{
    'name': 'Stock/Inventory Lock',
    'version': "14.0.1.0.0",
    'category': 'Stock',
    'author': 'Odoo Bangladesh',
    'summary': 'Each Month till 5th can modify only last inventory quantity adjustment',
    'description': 'Each Month till 5th can modify only last inventory quantity adjustment',
    'depends': ['stock'],
    'data': [
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}
