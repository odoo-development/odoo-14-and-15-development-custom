import dateutil.relativedelta
from odoo import _, api, exceptions, fields, models
from odoo.osv import osv


class StockInventory(models.Model):
    _inherit = "stock.inventory"

    def get_last_month_period(self):
        return True

    def build_ctx_periods_2(self, period_from_id, period_to_id):
        if self.period_from_id == self.period_to_id:
            return [period_from_id]
        period_from = self.browse(period_from_id)
        period_date_start = period_from.date_start
        company1_id = period_from.company_id.id
        period_to = self.browse(period_to_id)
        period_date_stop = period_to.date_stop
        company2_id = period_to.company_id.id
        if company1_id != company2_id:
            raise osv.except_osv(_('Error!'), _('You should choose the periods that belong to the same company.'))
        if period_date_start > period_date_stop:
            raise osv.except_osv(_('Error!'), _('Start period should precede then end period.'))

        # /!\ We do not include a criterion on the company_id field below, to allow producing consolidated reports
        # on multiple companies. It will only work when start/end periods are selected and no fiscal year is chosen.

        # for period from = january, we want to exclude the opening period (but it has same date_from, so we have to check if period_from is special or not to include that clause or not in the search).
        if period_from.special:
            return self.search([('date_start', '>=', period_date_start), ('date_stop', '<=', period_date_stop)])
        return self.search(
            [('date_start', '>=', period_date_start), ('date_stop', '<=', period_date_stop), ('special', '=', False)])

    @api.returns('self')
    def find_section(self):
        dt = None
        if not dt:
            dt = fields.date.context_today(self)
        args = [('date_start', '<=', dt), ('date_stop', '>=', dt)]
        if self.context.get('company_id', False):
            args.append(('company_id', '=', self.context['company_id']))
        else:
            company_id = self.env['res.users'].browse().company_id.id
            args.append(('company_id', '=', company_id))
        result = []
        if self.context.get('account_period_prefer_normal', True):
            # look for non-special periods first, and fallback to all if no result is found
            result = self.search(args + [('special', '=', False)])
        if not result:
            result = self.search(args)
        if not result:
            model, action_id = self.env['ir.model.data'].get_object_reference('account', 'action_account_period')
            msg = _('There is no period defined for this date: %s.\nPlease go to Configuration/Periods.') % dt
            raise exceptions.RedirectWarning(msg, action_id, _('Go to the configuration panel'))
        return result

    def create(self, vals):
        period_selected_id = vals.get('period_id')
        periods = self.env['account.period'].find()
        current_period_id = periods[0] if len(periods) > 0 else False

        today_date = fields.date.today()
        import datetime
        day_name = datetime.datetime.today().day
        previous_period_id = False

        allwoed_periods_list = []

        # day_name=6
        if 0 < day_name < 6:
            today_date = datetime.datetime.today()
            previous_month_date = today_date - dateutil.relativedelta.relativedelta(months=1)

            previous_periods = self.env['account.period'].find(dt=previous_month_date)
            previous_period_id = previous_periods[0] if len(previous_periods) > 0 else False

        if period_selected_id:
            if previous_period_id is not False:
                allwoed_periods_list.append(previous_period_id)
            if current_period_id is not False:
                allwoed_periods_list.append(current_period_id)
                if period_selected_id not in allwoed_periods_list:
                    raise osv.except_osv(_('Warning'), _('Your selected Period should be current Month'))
        else:
            raise osv.except_osv(_('Warning'), _('Please select Force Valuation Period'))

        res = super(StockInventory, self).create(vals)
        return res
