# Copyright 2021 Rocky

{
    "name": "Customer Company List View",
    "version": "14.0",
    "author": "Rocky",
    "category": "Sales Management",
    "license": "AGPL-3",
    "depends": ["sale"],
    "data": [
        "customer_company_list_view.xml"
    ],
    "installable": True,
}
