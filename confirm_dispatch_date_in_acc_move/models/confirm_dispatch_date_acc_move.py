from datetime import timedelta

from odoo import api, fields, models


class AccountMove(models.Model):
    _inherit = 'account.move'

    def confirm_date_from_so(self):

        if self.ids:

            query = """select sale_order.date_order, account_move.id from sale_order,account_move where sale_order.client_order_ref = account_move.ref and account_move.id IN %s"""
            self._cr.execute(query, (tuple(self.ids),))
            confirm_date_ids = self._cr.fetchall()

            self.so_confirm_date = ''

            for items in confirm_date_ids:
                if items[1] in self.ids:
                    move_id = self.env['account.move'].browse(items[1])
                    move_id.so_confirm_date = items[0]

    def dispatch_date_from_manifest(self):

        if self.ids:

            query = """select wms_manifest_process.confirm_time, wms_manifest_line.invoice_id from wms_manifest_process,wms_manifest_line where wms_manifest_process.id = wms_manifest_line.wms_manifest_id and wms_manifest_line.invoice_id IN %s"""
            self._cr.execute(query, (tuple(self.ids),))
            dispatch_date_ids = self._cr.fetchall()

            self.dispatch_date = ''

            for items in dispatch_date_ids:
                if items[1] in self.ids:
                    move_id = self.env['account.move'].browse(items[1])
                    move_id.dispatch_date = items[0]

    so_confirm_date = fields.Datetime(string='SO Confirm Date', compute='confirm_date_from_so', readonly=True, index=True)

    dispatch_date = fields.Datetime(string='Dispatch Date', compute='dispatch_date_from_manifest', readonly=True, index=True)

    customer_classification = fields.Char(related='commercial_partner_id.x_classification', string="Customer Classification", readonly=True, index=True, store=True)


