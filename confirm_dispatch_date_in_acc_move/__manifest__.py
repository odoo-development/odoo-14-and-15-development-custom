
{
    'name': 'Customer Classification, Confirm and Dispatch Date in Account Move',
    'version': '2.0.0.0',
    'summary': """Customer Classification, Confirm and Dispatch Date in Account Move
    """,
    'author': 'Sindabad',
    'website': 'http://www.sindabad.com',
    'depends': [
       'stock','account','partner_custom_fields'
    ],
    'data': [
        'views/confirm_dispatch_date_acc_move_view.xml'
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}