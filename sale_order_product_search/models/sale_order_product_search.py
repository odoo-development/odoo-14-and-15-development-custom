from operator import itemgetter

from odoo import models, fields


class SaleOrderProduct(models.Model):
    _inherit = 'sale.order'

    def _get_product(self):

        return True

    def _product_no_search(self, operator, value):

        # args = [('order_on', 'ilike', '1000026832')]
        operator = operator
        product_id = value

        sale_order_line = "SELECT DISTINCT order_id FROM sale_order_line WHERE product_id = '{0}'".format(
            product_id)
        self.env.cr.execute(sale_order_line)

        res = self.env.cr.fetchall()
        if not res:
            return [('id', '=', '0')]
        return [('id', 'in', res)]


    product_on = fields.Text(compute='_get_product', string='product name', search='_product_no_search', index=True)
