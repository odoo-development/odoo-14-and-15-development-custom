{
    'name': 'sale prder product search',
    'version': "14.0.1.0.0",
    'category': 'sale order',
    'author': 'Ashif',
    'summary': 'sale order Custom Search',
    'description': 'order search in picking Custom Search',
    'depends': ['base','product','sale', 'stock'],
    'installable': True,
    'application': True,
    'auto_install': False,
}
