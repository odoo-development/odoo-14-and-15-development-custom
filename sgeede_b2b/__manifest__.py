# {
#     'name': 'Back To Back Purchase Order from Sales order',
#     'version': '1.0',
#     'category': 'Tools',
#     'summary': """Generate Back to Back Purchase order in Sale order""",
#     'description': """Generate Back to Back Purchase order in Sale order""",
#     'author': 'Saad Bin Saif',
#     'website': 'http://www.bizzwit.com',
#     'depends': ['base','sale','purchase', 'odoo_magento_connect'],
#     'init_xml': [],
#     'update_xml': [
#          'wizard/back_to_back_order.xml',
#          'sale_view.xml',
#          'purchase_view.xml',
#
#     ],
#     'demo_xml': [],
#     'installable': True,
#     'active': False,
#     'certificate': '',
#     'price': 9.99,
#     'currency': 'EUR',
#     'images': ['images/main_screenshot.png',
#     'images/sgeede.png'
#
#     ],
# }
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:



# -*- coding: utf-8 -*-
{
    'name': 'Back to Back Purchase Order from Sales order',
    'summary': '''Generate Back to Back Purchase Order in Sale Order''',
    'description': '''
        Generate Back to Back Purchase Order in Sale Order
    ''',
    'author': 'SGEEDE',
    'website': 'http://www.sgeede.com',
    'category': 'Tools',
    'depends': ['base', 'sale', 'purchase', 'stock'],
    'data': [
        'security/ir.model.access.csv',
        'wizard/back_to_back_order_views.xml',
        'views/sale_views.xml',
        'views/purchase_views.xml',
    ],
    'demo': [],
    'installable': True,
    'license': 'LGPL-3',
    'images': ['static/description/icon.png']
}
