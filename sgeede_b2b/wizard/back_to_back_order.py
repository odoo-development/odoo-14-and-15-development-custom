from odoo import api, fields, models, tools, _
from odoo.exceptions import UserError
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT
from odoo.tools.float_utils import float_compare


class BackToBackOrder(models.TransientModel):
    _name = 'back.to.back.order'
    _description = 'Back to Back Orders'

    @api.model
    def _get_pricelist_id(self):
        sale = self.env['sale.order'].browse(self.env.context.get('active_id'))
        return sale.pricelist_id

    @api.onchange('picking_type_id')
    def onchange_picking_type_id(self):
        self.location_id = self.picking_type_id and self.picking_type_id.default_location_dest_id or False

    @api.model
    def default_get(self, fields):
        obj_data = self.env['ir.model.data']
        type_obj = self.env['stock.picking.type']
        user_obj = self.env['res.users']

        context = dict(self.env.context or {})
        res = super(BackToBackOrder, self).default_get(fields)
        order = self.env['sale.order'].browse(context.get('active_ids'))
        items = []
        location_id = False
        picking_type_id = False

        for line in order.order_line:
            item = (0, 0, {
                'product_id': line.product_id.id,
                'qty': line.product_uom_qty,
                'price': line.price_unit,
                'subtotal': line.price_subtotal
            })
            if line.product_id:
                items.append(item)

        types = type_obj.search([('code', '=', 'incoming'), ('warehouse_id.company_id', '=', self.env.company.id)],
                                limit=1)
        if not types:
            types = type_obj.search([('code', '=', 'incoming'), ('warehouse_id', '=', False)], limit=1)

        if types:
            picking_type_id = types.id
            location_id = types.default_location_dest_id and types.default_location_dest_id.id or False

            client_order_ref = order.client_order_ref

        res.update(line_ids=items, picking_type_id=picking_type_id, location_id=location_id, client_order_ref=client_order_ref)
        return res

    def action_confirm(self):
        vals = {}
        sale_obj = self.env['sale.order']
        purchase_obj = self.env['purchase.order']
        purchase_line_obj = self.env['purchase.order.line']
        product_uom = self.env['uom.uom']
        product_product = self.env['product.product']
        res_partner = self.env['res.partner']
        res = {}
        todo = []
        supplierinfo = False

        for po in self:
            vals = {
                'partner_id': po.partner_id.id,
                'date_order': po.date_order,
                'client_order_ref': po.client_order_ref,
                'picking_type_id': po.picking_type_id.id,
                'create_uid': self.env.uid,
                'x_credit_days': po.x_credit_days,
                'x_sale_classification_type': po.x_sale_classification_type
            }
            purchase_id = purchase_obj.create(vals)
            sale = sale_obj.browse(self.env.context.get('active_id'))
            if sale:
                sale.write({
                    'purchase_id': purchase_id,
                    'b2b_generate': True
                })

            context_partner = self.env.context.copy()
            if po.partner_id.id:
                lang = res_partner.browse(po.partner_id.id).lang
                context_partner.update({'lang': lang, 'partner_id': po.partner_id.id})

            for line in po.line_ids:
                if line.qty <= 0:
                    continue
                else:
                    product = product_product.with_context(context_partner).browse(line.product_id.id)
                    dummy, name = product_product.with_context(context_partner).browse(line.product_id.id).name_get()[0]

                    if product.description_purchase:
                        name += '\n' + product.description_purchase

                    precision = self.env['decimal.precision'].precision_get('Product Unit of Measure')
                    for supplier in product.seller_ids:
                        if po.partner_id.id and (supplier.name.id == po.partner_id.id):
                            supplierinfo = supplier
                            if supplierinfo.product_uom.id != line.product_uom.id:
                                res['warning'] = {'title': _('Warning!'),
                                                  'message': _('The selected supplier only sells this product by %s')
                                                             % supplierinfo.product_uom.name}

                            min_qty = supplierinfo.min_qty
                            if float_compare(min_qty, line.qty, precision_digits=precision) == 1:
                                if line.qty:
                                    raise UserError(
                                        _('The selected supplier has a minimal quanitity set to %s %s, you should not purchase less.') % (
                                        supplierinfo.min_qty, supplierinfo.product_uom.name))
                                line.qty = min_qty
                                return {'warning': '!!!'}

                    dt = purchase_line_obj._get_date_planned(supplierinfo).strftime(DEFAULT_SERVER_DATETIME_FORMAT)
                    values = {
                        'product_id': line.product_id.id,
                        'name': name,
                        'date_planned': dt,
                        'product_qty': line.qty,
                        'price_unit': line.price,
                        'price_subtotal': line.subtotal,
                        'order_id': purchase_id.id,
                        'product_uom': line.product_id.uom_po_id.id,
                    }
                    line = self.env['purchase.order.line'].with_context(self.env.context).create(values)
                    line.write({
                        'sale_order_id': self.env.context.get('active_id')
                    })

    partner_id = fields.Many2one('res.partner', 'Supplier')
    date_order = fields.Datetime('Date Order', default=fields.Datetime.now)
    line_ids = fields.One2many('back.to.back.order.line', 'back_order_id', 'Order Lines for the Wizard')
    location_id = fields.Many2one('stock.location', 'Destination')
    picking_type_id = fields.Many2one('stock.picking.type', 'Deliver To',
                                      help='This will determine picking type of incoming shipment')
    pricelist_id = fields.Many2one('product.pricelist', 'Pricelist', default=_get_pricelist_id,
                                   help='The pricelist sets the currenct used for this purchase order. It also computes the supplier price for the selected products/quantities.')
    client_order_ref = fields.Char(string='Customer Reference', copy=False)
    x_credit_days = fields.Integer(string="Credit Days")
    x_sale_classification_type = fields.Selection(
        [('b2r', 'B2R'),
         ('b2e', 'B2E'),
         ('b2c', 'B2C'),
         ('b2re', 'B2R & B2E'),
         ('undefined', 'Undefined')],
        string="Sale Classification Type", default='b2r')


class BackToBackOrderLine(models.TransientModel):
    _name = 'back.to.back.order.line'
    _description = 'Back to Back Order Lines'

    def _compute_amount_line(self):
        res = {}
        tax_obj = self.env['account.tax']
        for line in self:
            taxes = tax_obj.compute_all(price_unit=line.price, quantity=line.qty, product=line.product_id,
                                        partner=line.back_order_id.partner_id)
            currency = line.back_order_id.pricelist_id.currency_id
            line.subtotal = currency.round(taxes['total_included'])

    @api.model
    def _get_uom_id(self):
        try:
            proxy = self.env['ir.model.data']
            result = proxy.get_object_reference('product', 'product_uom_unit')
            return result[1]
        except Exception as ex:
            return False

    @api.onchange('product_id')
    def onchange_product_id(self):
        result = {}
        if not self.product_id:
            return result

        unit_cost = self.product_id.standard_price
        self.price = unit_cost

        return result

    @api.onchange('qty')
    def onchange_qty(self):
        result = {}
        if not self.product_id:
            return result

        total = self.price * self.qty
        self.subtotal = total

        return result

    product_id = fields.Many2one('product.product', 'Product')
    back_order_id = fields.Many2one('back.to.back.order', 'Back Order')
    qty = fields.Float('Quantity')
    price = fields.Float('Unit Price')
    subtotal = fields.Float('Subtotal', compute='_compute_amount_line')
    taxes_id = fields.Many2many('account.tax', 'purchase_order_taxes', 'ord_id', 'tax_id', 'Taxes')
    product_uom = fields.Many2one('uom.uom', 'Unit of Measure', default=_get_uom_id)
