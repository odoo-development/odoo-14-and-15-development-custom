# -*- coding: utf-8 -*-
from odoo import api, fields, models, tools, _


class PurchaseOrderLine(models.Model):
    _inherit = 'purchase.order'

    client_order_ref = fields.Char(string='Customer Reference', copy=False)


# class PurchaseOrderLine(models.Model):
#     _inherit = 'purchase.order.line'
#
#     sale_order_id = fields.Many2one('sale.order', 'Order')




# from openerp.osv import fields, osv
# from openerp.tools.translate import _
# import openerp.addons.decimal_precision as dp
#
# class purchase_order(osv.osv):
# 	_inherit = "purchase.order"
#
# 	_columns = {
# 		'client_order_ref': fields.char('Reference/Description'),
# 	}
#
#
# class purchase_order_line(osv.osv):
# 	_inherit = "purchase.order.line"
#
# 	_columns = {
# 			'sale_order_id': fields.many2one('sale.order', 'Order'),
# 		}


