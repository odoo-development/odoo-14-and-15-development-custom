# -*- coding: utf-8 -*-
from odoo import api, fields, models, tools, _


class SaleOrder(models.Model):
    _inherit = 'sale.order'

    b2b_generate = fields.Boolean(string='B2B Generate', default=False)
    purchase_id = fields.Many2one('purchase.order', string='B2B Purchase Order')
    purchase_line_ids = fields.One2many('purchase.order.line', 'sale_order_id', string='B2B Order Lines')




# import logging
# from datetime import datetime
# from dateutil.relativedelta import relativedelta
# from operator import itemgetter
# import time
#
# import openerp
# from openerp import SUPERUSER_ID
# from openerp import tools
# from openerp.osv import fields, osv, expression
# from openerp.tools.translate import _
# from openerp.tools.float_utils import float_round as round
#
#
# import openerp.addons.decimal_precision as dp
#
#
# class sale_order(osv.osv):
#     _inherit = "sale.order"
#
#     _columns = {
#             'purchase_id': fields.many2one('purchase.order', 'Purchase Order', select=True),
#             'purchase_line_ids': fields.one2many('purchase.order.line','sale_order_id', 'Order Lines')
#         }



