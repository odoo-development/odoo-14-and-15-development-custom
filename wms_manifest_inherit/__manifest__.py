{
     'name': 'WMS Manifest Inherit',
     'description': 'WMS Manifest Inherit',
     'author': 'Odoo Bangladesh',
     'depends': ['wms_manifest','ndr_process', 'send_sms_on_demand','wms_manifest_extends','cash_collection'],
     'data': [
         'report/report_unloading.xml',
         'report/report_menu.xml',
         'views/wms_manifest_view.xml',
         'views/wms_outbound_view.xml',
         'views/account_invoice_view.xml',
     ]

 }