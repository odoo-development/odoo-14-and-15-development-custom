import json
import datetime
from datetime import datetime

from odoo import api, fields, models, _
from odoo.exceptions import UserError, ValidationError, Warning
from odoo.osv import osv
from odoo.tools.translate import _


class wms_manifest_outbound(models.Model):
    _inherit = "wms.manifest.outbound"

    source = fields.Char('Source')
    delivery_done_by = fields.Many2one('res.users', 'Delivered Done By')
    delivery_done_datetime = fields.Datetime('Delivered Done Date')

    # ## Block code for v-14
    # _columns = {
    #     'source': fields.char('Source'),
    #     'delivery_done_by': fields.many2one('res.users', 'Delivered Done By'),
    #     'delivery_done_datetime': fields.datetime('Delivered Done Date'),
    # }
    #
    # _defaults = {
    #     'state': 'pending',
    #
    # }

    def receipt_return_stock(self):
        self.ensure_one()
        # try:

        if self.stock_return_id:
            raise ValidationError('Already Received')

        sales_order = self.env['sale.order'].search([('client_order_ref', '=', self.name),('state', '!=','cancel')], limit=1)
        # invoice_ids
        if not sales_order:
            raise ValidationError('Relevant sales order has not been found')

        if not sales_order.picking_ids:
            raise ValidationError('Relevant picking has not been found')

        if not sales_order.invoice_ids:
            raise ValidationError('Relevant invoice has not been found')

        invoice = self.env['account.move'].search([('id', '=', self.outbound_line[0].invoice_id), ('state', '!=', 'cancel')])

        stock_objects = sales_order.picking_ids
        invoice = invoice if invoice else self.outbound_line[0].invoice_id

        values = {'refund_method': 'refund', 'date_mode': 'custom'}
        account_reversal_form = self.env['account.move.reversal'].with_context(active_model='account.move',
                                                                               active_ids=[invoice.id],
                                                                               active_id=invoice.id).create(values)
        account_reversal_result = account_reversal_form.reverse_moves()


        if account_reversal_result and account_reversal_result.get('res_id', False):
            credit_note_id = account_reversal_result.get('res_id', False)
            credit_note = self.env['account.move'].browse(credit_note_id)
        else:
            raise ValidationError('Refund invoice has not generated. Please inform your system administrator')

        # reversed_move = credit_note._reverse_move_vals({}, False)
        new_invoice_line_ids = []
        new_invoice_line_id = []

        for line_vals in credit_note['line_ids']:
            if not line_vals['exclude_from_invoice_tab']:
                new_invoice_line_id.append((0, 0, line_vals))

        # credit_note.write({
        #     'invoice_line_ids': [(5, 0, 0)],
        #     'partner_bank_id': False,
        # })

        # product_dict = {}

        for model in new_invoice_line_id:
            new_dict = model[2].read(list(set(self.env['account.move.line']._fields)))
            new_dict[0].pop('id', 0)
            new_invoice_line_ids.append((0, 0, new_dict[0]))

        credit_note.write({
            'invoice_line_ids': [(5, 0, 0)],
            'partner_bank_id': False,
        })

        product_return_moves = [[5, False, False]]
        out_product_id=[]
        stock_object = None

        picking_type_out = "SELECT id from stock_picking_type WHERE code='outgoing'"
        self.env.cr.execute(picking_type_out)
        picking_type_out_id =[c_id[0] for c_id in self.env.cr.fetchall()]

        for items in self.outbound_line:
            for cmd, virtualid, line_vals in new_invoice_line_ids:
                if line_vals.get('product_id', False) and items.product_id == line_vals['product_id'][0] and line_vals['product_id'][0]:
                    if items.return_qty == 0:
                        new_invoice_line_ids.remove((cmd, virtualid, line_vals))
                    else:
                        if float(items.return_qty) == float(line_vals['quantity']):
                            five_vat = line_vals['five_vat']
                            price_subtotal_with_vat = line_vals['price_subtotal_with_vat']
                            total_profit_loss = line_vals['total_profit_loss']
                            x_total_discount = line_vals['x_total_discount']
                        else:
                            five_vat = round((items.return_qty * line_vals['vat_on_unit_profit_loss']), 2)
                            price_subtotal_with_vat = round((line_vals['price_unit_with_vat'] *items.return_qty), 2)
                            total_profit_loss = round((line_vals['unit_cost_profit_loss'] *items.return_qty), 2)
                            x_total_discount = round((line_vals['x_total_discount'] *items.return_qty), 2)
                        # invoice_line_id = self.update_invoice_data(quantity,line_vals)
                        line_vals.update({'quantity': items.return_qty,
                                          'five_vat': five_vat,
                                          'price_subtotal_with_vat':price_subtotal_with_vat,
                                          'total_profit_loss':total_profit_loss,
                                          'x_total_discount':x_total_discount,
                        })

            if items.return_qty >0:
                product_dict = {
                    'product_id': items.product_id,
                    # 'quantity': items.quantity - items.delivered,
                    'quantity': items.return_qty,
                    # 'lot_id': False
                }
                out_product_id.append(items.product_id)

                for stock_item in stock_objects:
                    if stock_item.picking_type_id.id in picking_type_out_id:
                        product_id_return = []
                        stock_object = stock_item
                        for st_items in stock_item.move_lines:
                            if st_items.product_id.id == items.product_id and st_items.product_uom_qty == items.quantity:
                                product_id_return.append(st_items.product_id.id)
                                product_dict['move_id'] = st_items.id
                                product_return_moves.append([0, False, product_dict])
                                product_dict = {}
                        if product_return_moves and len(out_product_id)==len(product_id_return):
                            stock_object = stock_item
                            break

        credit_note.write({'invoice_line_ids': new_invoice_line_ids})
        credit_note.action_post()

        ###########################################################################
        # Check suggested outstanding payments.
        to_reconcile_payments_widget_vals = json.loads(invoice.invoice_outstanding_credits_debits_widget)
        current_amounts = {vals['move_id']: vals['amount'] for vals in to_reconcile_payments_widget_vals['content']}

        # Reconcile
        pay_term_lines = invoice.line_ids \
            .filtered(lambda line: line.account_id.user_type_id.type in ('receivable', 'payable'))
        to_reconciles = self.env['account.move'].browse(list(current_amounts.keys())) \
            .line_ids \
            .filtered(lambda line: line.account_id == pay_term_lines.account_id)

        for to_reconcile in to_reconciles:
            if pay_term_lines.move_id.name in to_reconcile.move_id.ref:
                (pay_term_lines + to_reconcile).reconcile()
        ###########################################################################
        prepare_data = {'product_return_moves': product_return_moves,
                        # 'invoice_state': '2binvoiced',
                        'location_id': stock_object.location_id.id,
                        }

        return_picking = self.env['stock.return.picking'].with_context(active_model='stock.picking',
                                                                       active_ids=[stock_object.id],
                                                                       active_id=stock_object.id, refund=True).create(
            prepare_data)
        result = return_picking.with_context(active_id=stock_object.id).create_returns()

        if result and result.get('res_id', False):
            return_picking_id = result.get('res_id', False)

            # Immediate Transfer
            immediate_transfer_line_ids = [[0, False, {
                'picking_id': return_picking_id,
                'to_immediate': True
            }]]

            stock_immediate_trn = self.env['stock.immediate.transfer'].create({
                'pick_ids': [(4, return_picking_id)],
                'show_transfers': False,
                'immediate_transfer_line_ids': immediate_transfer_line_ids
            })

            stock_immediate_trn.with_context(button_validate_picking_ids=stock_immediate_trn.pick_ids.ids).process()
        else:
            raise ValidationError('Return picking has not been created, please contact your system administrator')

        self.write({
            'invoice_return_id': credit_note_id,
            'stock_return_id': return_picking_id,
            'state': 'confirm',
            'confirm_by': self.env.uid,
            'confirm': True
        })

        self.outbound_line.write({'invoice_return_id': credit_note_id})


class WmsManifestOutboundLine(models.Model):
    _inherit = "wms.manifest.outbound.line"

    invoice_return_id = fields.Integer('Invoice Return ID')


class accountinvoice(models.Model):
    _inherit = "account.move"

    ndr_inv_ids = fields.One2many('ndr.process', 'invoice_id', 'NDR')
    inv_ids = fields.One2many('wms.manifest.outbound.line', 'invoice_id', 'Return Info')
    return_inv_ids = fields.One2many('wms.manifest.outbound.line', 'invoice_return_id', 'Return')


class WmsManifestLine(models.Model):
    _inherit = "wms.manifest.line"

    source = fields.Char('Source')
    priority_customer = fields.Boolean('Priority Customer', index=True)
    delivery_done_by = fields.Many2one('res.users', 'Delivered Done By')
    delivery_done_datetime = fields.Datetime('Delivered Done Date')


class NdrProcess(models.Model):
    _inherit = "ndr.process"
    _description = "NDR Process"

    source = fields.Char('Source')
    delivery_done_by = fields.Many2one('res.users', 'Delivered Done By')
    delivery_done_datetime = fields.Datetime('Delivered Done Date')

