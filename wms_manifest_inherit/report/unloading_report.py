from odoo import api, models, _
from datetime import datetime

class PackingChallanReport(models.AbstractModel):

   _name= 'report.wms_manifest_inherit.report_unloading'
   _description = 'unloading report for NDR and return product'

   @api.model
   def _get_report_values(self, docids, data=None):
       docs = self.env['wms.manifest.process'].browse(docids[0])

       return {
           'doc_ids': docids,
           'doc_model': 'wms.manifest.process',
           'docs': docs,
           'data': data,

           'get_product_list': self.get_product_list,
       }

   def get_product_list(self,obj):

       try:
           order_numbers = [obj.split(',')[0]]
       except:
           order_numbers = [str(obj)]

       group_id = None

       p_list=[]
       reschedule_invoice_number=[]
       return_order_number=[]

       self.env.cr.execute("SELECT id FROM wms_manifest_process WHERE name=%s", (order_numbers))
       for item in self.env.cr.fetchall():
           group_id = [item[0]]

       if group_id is not None:
           order_data = self.env['wms.manifest.process'].browse(group_id)

           account_invoice_ids = order_data.picking_line

           for items in account_invoice_ids:
               if items.magento_no and items.reschedule:
                   reschedule_invoice_number.append(items.invoice_id)
               if items.magento_no and (items.full_return or items.partial_delivered):
                   return_order_number.append(items.magento_no)

           invoice_line_env = self.env['account.move.line']
           total_moveline_list = invoice_line_env.search([('move_id', 'in', reschedule_invoice_number),(
               'product_id', '!=', None),('price_subtotal_with_vat', '!=', None)])
           # total_moveline_list = invoice_line_env.browse([total_moveline_id_list])

           return_order_env = self.env['wms.manifest.outbound']
           total_retn_movline_list = return_order_env.search(
               [('man_name', '=', obj), ('name', 'in', return_order_number), ('has_return', '=', True)])
           # total_retn_movline_list= return_order_env.browse(total_retn_movline_id_list)

           product_id_lists =[]
           product_obj_lists=[]
           moves_list=[]
           retn_move_list=[]

           for att1 in total_moveline_list:
               product_id_lists.append(att1.product_id.id)
               product_obj_lists.append(att1.product_id)
               moves_list.append(att1)

           for retn_moveline_id_list in total_retn_movline_list:
               for att2 in retn_moveline_id_list.outbound_line:
                   product_id_lists.append(att2.product_id)
                   product_obj_lists.append(self.env['product.product'].browse(att2.product_id))
                   retn_move_list.append(att2)

           product_id_lists =list(set(product_id_lists))
           product_obj_lists=list(set(product_obj_lists))


           for product_id in product_id_lists:
               ndr_qty_count=0
               retn_qty_count=0
               product_info_dict={}
               for mov in moves_list:
                   if mov.product_id.id==product_id:
                       ndr_qty_count += mov.quantity
               for retn_mov in retn_move_list:
                  if retn_mov.product_id==product_id:
                    retn_qty_count += retn_mov.return_qty

               for p_id in product_obj_lists:
                 if p_id.id == product_id and ndr_qty_count >0 and product_id !=1:
                    product_info_dict['product_name']=str(p_id.default_code)+ ' '+str(p_id.name)
                    product_info_dict['default_code']=p_id.default_code
                    product_info_dict['mov']=p_id
                    product_info_dict['qty']=ndr_qty_count
                    product_info_dict['total_qty']=float(ndr_qty_count)+ float(retn_qty_count)
                    break
                 if p_id.id == product_id and retn_qty_count >0 and product_id !=1:
                    product_info_dict['product_name']=str(p_id.default_code)+ ''+ str(p_id.name)
                    product_info_dict['default_code']=p_id.default_code
                    product_info_dict['mov']=p_id
                    product_info_dict['qty']=ndr_qty_count
                    product_info_dict['retn_qty']=retn_qty_count
                    product_info_dict['total_qty']=float(ndr_qty_count) + float(retn_qty_count)
                    break
               if product_info_dict:
                   p_list.append(product_info_dict)

       return p_list



