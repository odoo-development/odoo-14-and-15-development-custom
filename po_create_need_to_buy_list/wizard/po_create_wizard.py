import logging

from odoo import models, fields
from odoo.addons import decimal_precision as dp

_logger = logging.getLogger(__name__)


class PoCreateNeedToBuyList(models.TransientModel):
    _name = "po.create.need.to.buy.list"
    _description = "PO Create From Need to Buy List"

    partner_id = fields.Many2one('res.partner', 'Supplier', required=True)
    date_order = fields.Datetime('Date Order', required=True, default=fields.Datetime.now)
    line_ids = fields.One2many('po.create.need.to.buy.list.line', 'line_id', 'Product Lines', required=True)
    location_id = fields.Many2one('stock.location', 'Destination', domain=[('usage', '<>', 'view')],
                                  default=lambda self: self.env[
                                      'res.users'].company_id.partner_id.property_stock_customer.id, )
    picking_type_id = fields.Many2one('stock.picking.type', 'Deliver To',
                                      help="This will determine picking type of incoming shipment")
    pricelist_id = fields.Many2one('product.pricelist', 'Pricelist',
                                   help="The pricelist sets the currency used for this purchase order. It also computes the supplier price for the selected products/quantities.")
    currency_id = fields.Many2one('res.currency', 'Currency', readonly=True,
                                  default=lambda self: self.env['res.users'].company_id.currency_id.id, )
    client_order_ref = fields.Char('Reference/Description')
    product_uom = fields.Char('Product Unit of Measure')

    def default_get(self, fields):
        res = super(PoCreateNeedToBuyList, self).default_get(fields)
        order = self.env['product.need.to.buy'].browse(self._context['active_ids'])

        items = []

        for line in order:
            remaining_qty = line.ordered_qty
            ### Chack Already Generated PO

            query = "select purchase_order_line.product_id,purchase_order.picking_type_id,sum(purchase_order_line.product_qty) as total_qty " \
                    "from purchase_order,purchase_order_line " \
                    "where purchase_order.id=purchase_order_line.order_id " \
                    "and purchase_order.state ='draft' " \
                    "and purchase_order.system_generated_po = True " \
                    "and purchase_order_line.product_id =%s " \
                    "group by purchase_order_line.product_id, purchase_order.picking_type_id"

            self.env.cr.execute(query, ([line.name.id]))

            prd_info = self.env.cr.fetchall()

            if len(prd_info) > 0:
                generated_qty = prd_info[0][1]

                remaining_qty = line.total_need_to_buy

            ## Ends Here
            if remaining_qty > 0:

                item = {
                    'product_id': line.name.id,
                    'qty': remaining_qty,
                    'product_uom': str(line.prod_uom),
                    'price': line.name.standard_price,
                    'subtotal': line.name.standard_price * remaining_qty,
                }

                if line.name:
                    items.append((0, 0, item))

        res.update(line_ids=items)

        return res

    def prepare_order_line_data(self, line, product_qty=0):
        return {
            'product_id': line.product_id.id,
            'name': line.product_id.display_name,
            'price_unit': line.price,
            'product_qty': product_qty,
            'date_planned': fields.datetime.now()
        }

    def po_create(self):
        po_data = {
            'partner_id': None,
            'date_order': None,
            'picking_type_id': None,
            # 'pricelist_id': None,
            'description': None,
            'order_line': None,
            'system_generated_po': True,
        }

        need_to_buy_obj = self.env['po.create.need.to.buy.list'].browse(self.ids)

        utt_order_line = []
        uttrt_order_line = []
        nodd_order_line = []
        noddrt_order_line = []
        moti_order_line = []
        motirt_order_line = []
        saved_po_ids = []

        for need_buy in self:
            po_data = {
                'partner_id': need_buy.partner_id.id,
                'date_order': fields.datetime.now(),
                'picking_type_id': None,
                # 'location_id': None,
                # 'pricelist_id': need_buy.pricelist_id.id if need_buy.pricelist_id else False,
                # 'description': None,
                'partner_ref': need_buy.client_order_ref,
                'order_line': None,
                'system_generated_po': True,
                'adv_po_snd': 'credit',
            }
            order_line = []

            # for o_line in need_buy.line_ids:
            #     need_to_buy_obj = self.env['product.need.to.buy']
            #
            #     need_ids = need_to_buy_obj.search([('name', '=', o_line.product_id.id)])
            #
            #     need_to_buy_list = need_to_buy_obj.browse(need_ids)[0]
            #
            #     if need_to_buy_list.uttwh_forcast_quantity > 0:
            #         utt_order_line.append([0, False, {
            #             'product_id': o_line.product_id.id,
            #             'name': o_line.product_id.name,
            #             'price_unit': o_line.price,
            #             'product_qty': need_to_buy_list.uttwh_forcast_quantity,
            #             'date_planned': fields.datetime.now()
            #         }])
            #
            #     if need_to_buy_list.uttwh_retail_forcast_quantity > 0:
            #         uttrt_order_line.append([0, False, {
            #             'product_id': o_line.product_id.id,
            #             'name': o_line.product_id.name,
            #             'price_unit': o_line.price,
            #             'product_qty': need_to_buy_list.uttwh_retail_forcast_quantity,
            #             'date_planned': fields.datetime.now()
            #         }])
            #
            #     if need_to_buy_list.nodda_forcast_quantity > 0:
            #         nodd_order_line.append([0, False, {
            #             'product_id': o_line.product_id.id,
            #             'name': o_line.product_id.name,
            #             'price_unit': o_line.price,
            #             'product_qty': need_to_buy_list.nodda_forcast_quantity,
            #             'date_planned': fields.datetime.now()
            #         }])
            #
            #     if need_to_buy_list.nodda_retail_forcast_quantity > 0:
            #         noddrt_order_line.append([0, False, {
            #             'product_id': o_line.product_id.id,
            #             'name': o_line.product_id.name,
            #             'price_unit': o_line.price,
            #             'product_qty': need_to_buy_list.nodda_retail_forcast_quantity,
            #             'date_planned': fields.datetime.now()
            #         }])
            #
            #     if need_to_buy_list.moti_forcast_quantity > 0:
            #         moti_order_line.append([0, False, {
            #             'product_id': o_line.product_id.id,
            #             'name': o_line.product_id.name,
            #             'price_unit': o_line.price,
            #             'product_qty': need_to_buy_list.moti_forcast_quantity,
            #             'date_planned': fields.datetime.now()
            #         }])
            #
            #     if need_to_buy_list.moti_retail_forcast_quantity > 0:
            #         motirt_order_line.append([0, False, {
            #             'product_id': o_line.product_id.id,
            #             'name': o_line.product_id.name,
            #             'price_unit': o_line.price,
            #             'product_qty': need_to_buy_list.moti_retail_forcast_quantity,
            #             'date_planned': fields.datetime.now()
            #         }])

        for line in self.line_ids:
            product = self.env['product.need.to.buy'].search([('name', '=', line.product_id.id)])
            if product.uttwh_forcast_quantity > 0:
                utt_order_line.append([0, False, self.prepare_order_line_data(line, product.uttwh_forcast_quantity)])

            if product.uttwh_retail_forcast_quantity > 0:
                uttrt_order_line.append([0, False, self.prepare_order_line_data(line, product.uttwh_retail_forcast_quantity)])

            if product.nodda_forcast_quantity > 0:
                nodd_order_line.append([0, False, self.prepare_order_line_data(line, product.nodda_forcast_quantity)])

            if product.nodda_retail_forcast_quantity > 0:
                noddrt_order_line.append([0, False, self.prepare_order_line_data(line, product.nodda_retail_forcast_quantity)])

            if product.moti_forcast_quantity > 0:
                moti_order_line.append([0, False, self.prepare_order_line_data(line, product.moti_forcast_quantity)])

            if product.moti_retail_forcast_quantity > 0:
                motirt_order_line.append([0, False, self.prepare_order_line_data(line, product.moti_retail_forcast_quantity)])


        ### For Uttara Warehouse
        if len(utt_order_line) > 0:
            picking_type_id = 1
            warehouse_id = 1
            self._create_po_based_on_warehouse(po_data, saved_po_ids, utt_order_line, picking_type_id, warehouse_id)

        ### For Uttara Retail Warehouse
        if len(uttrt_order_line) > 0:
            picking_type_id = 23
            warehouse_id = 6
            self._create_po_based_on_warehouse(po_data, saved_po_ids, uttrt_order_line, picking_type_id, warehouse_id)

        ### For Nodda Warehouse
        if len(nodd_order_line) > 0:
            picking_type_id = 7
            warehouse_id = 2
            self._create_po_based_on_warehouse(po_data, saved_po_ids, nodd_order_line, picking_type_id, warehouse_id)

        ### For Nodda Retail Warehouse
        if len(noddrt_order_line) > 0:
            picking_type_id = 13
            warehouse_id = 3
            self._create_po_based_on_warehouse(po_data, saved_po_ids, noddrt_order_line, picking_type_id, warehouse_id)

        ### For Motijheel Warehouse
        if len(moti_order_line) > 0:
            picking_type_id = 18
            warehouse_id = 4
            self._create_po_based_on_warehouse(po_data, saved_po_ids, moti_order_line, picking_type_id, warehouse_id)

        ### For Motijheel Retail Warehouse
        if len(motirt_order_line) > 0:
            picking_type_id = 28
            warehouse_id = 7
            self._create_po_based_on_warehouse(po_data, saved_po_ids, motirt_order_line, picking_type_id, warehouse_id)


        ##### Update The Quantity

        # it is what we have saved the data

        ### Ends here

        return True

    def _create_po_based_on_warehouse(self, po_data, saved_po_ids, utt_order_line, picking_type_id, warehouse_id):
        need_to_buy_obj = self.env['product.need.to.buy']
        purchase_obj = self.env['purchase.order']
        po_data['name'] = None
        po_data.pop("name", None)
        # po_data['location_id'] = 12
        po_data['picking_type_id'] = picking_type_id
        po_data['order_line'] = utt_order_line
        data = po_data
        res = purchase_obj.create(data)
        saved_id = res.id if res else None
        saved_po_ids.append(saved_id)
        for items in utt_order_line:
            p_id = items[2].get('product_id')
            need_ids = need_to_buy_obj.search([('name', '=', p_id)])

            if need_ids and need_ids[0] and need_ids[0].b2b_order_ids != 'None':
                need_to_buy_list = need_ids[0].b2b_order_ids
                need_to_buy_list = str(need_to_buy_list)
                need_to_buy_list = need_to_buy_list.replace(']', '')
                need_to_buy_list = need_to_buy_list.replace('[', '')
                need_to_buy_list = need_to_buy_list.split(',')
                for so_id in need_to_buy_list:
                    self.env.cr.execute("select * from sale_order where warehouse_id=%s and id =%s", (warehouse_id, so_id))
                    abc = self.env.cr.fetchall()
                    if len(abc) > 0:
                        self.env.cr.execute(
                            "INSERT INTO sale_b2b_ref (sale_id,po_id,product_id) VALUES ('{0}','{1}','{2}')".format(
                                so_id, saved_id, p_id))
                        # self.env.cr.commit()
        # self.env.cr.commit()


class PoCreateNeedToBuyListLine(models.TransientModel):
    _name = "po.create.need.to.buy.list.line"
    _description = "PO Create Need to Buy List Line"

    def _amount_line(self):
        # res = {}
        # cur_obj = self.env['res.currency']
        # tax_obj = self.env['account.tax']
        for line in self:
            cur = line.line_id.pricelist_id.currency_id if line.line_id.pricelist_id else self.env.company.currency_id
            taxes = line.taxes_id.compute_all(line.price, cur, 10, line.product_id, line.line_id.partner_id)
            # res[line.id] = cur_obj.round(cur, taxes['total'])
            line.update({'subtotal': cur.round(taxes['total_void'])})
            # res[line.id] = cur.round(taxes['total_void'])
        # return res

    product_id = fields.Many2one('product.product', 'Product')
    product_name = fields.Char(related='product_id.display_name', string="Product", readonly=True)
    line_id = fields.Many2one('po.create.need.to.buy.list', string='Product Lines')
    qty = fields.Float('Quantity')
    price = fields.Float('Unit Price')
    subtotal = fields.Float(compute='_amount_line', string='Subtotal', digits_compute=dp.get_precision('Account'))
    taxes_id = fields.Many2many('account.tax', 'purchase_order_taxe', 'ord_id', 'tax_id', 'Taxes')
    product_uom = fields.Char(related="product_id.uom_name", string="Unit of Measure")


class SaleToPoReference(models.Model):
    _name = "sale.b2b.ref"
    _description = "sale b2b ref"

    sale_id = fields.Many2one('sale.order', 'Sale Order')
    po_id = fields.Many2one('purchase.order', 'Purchase Order')
    product_id = fields.Many2one('product.product', 'Product')
