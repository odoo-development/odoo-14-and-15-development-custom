from odoo import api, fields, models, _

class Product(models.Model):
    _inherit = "product.product"

    def _compute_pending_qty(self):

        for record in self:
            order_ids_obj = self.env['sale.order.line'].search([('product_id', '=', record.id), (
                'state', 'in', ['draft', 'approval_pending', 'check_pending', 'new_cus_approval_pending', 'ceo_check_pending'])])
            # order_ids_obj = self.env['sale.order.line'].browse(order_ids)
            total_pending_qty = 0
            for items in order_ids_obj:
                total_pending_qty += items.product_uom_qty

            record.pending_qty = total_pending_qty

    pending_qty = fields.Float(string='Pending quantity for approval/confirmation',compute='_compute_pending_qty')
