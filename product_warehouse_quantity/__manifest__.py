
{
    "name": "Product Warehouse Quantity",
    "version": "1.0.0",
    "author": "Odoo Bangladesh",
    'category': 'Inventory/Inventory',
    "summary": """Real-time calculation and display available product's quantity from all warehouse""",
    "license": "AGPL-3",
    "website": "",
    "description": """""",
    "depends": ["sale_management", "stock", "product"],
    "data": ['views/product_view.xml'],
    "images": ["static/description/banner.png"],
    "installable": True,
    "application": False,
    "auto_install": False,
}
