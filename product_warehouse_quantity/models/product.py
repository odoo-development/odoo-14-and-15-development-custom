from odoo import api, fields, models, _


class ProductProduct(models.Model):
    _inherit = "product.product"

    # warehouse_quantity = fields.Char(string="Quantity per warehouse", compute='_get_warehouse_quantity')

    uttara_quantity = fields.Float(string="Uttara Quantity", compute='_compute_uttara_warehouse_quantity')
    nodda_quantity = fields.Float(string="Nodda Quantity",compute='_compute_nodda_warehouse_quantity')
    signboard_quantity = fields.Float(string="Signboard Quantity",compute='_compute_signboard_warehouse_quantity')
    badda_quantity = fields.Float(string="Badda Quantity",compute='_compute_badda_warehouse_quantity')
    savar_quantity = fields.Float(string="Savar Quantity",compute='_compute_savar_warehouse_quantity')
    badda_horeca_quantity = fields.Float(string="Badda HoReCa Quantity",compute='_compute_badda_horeca_warehouse_quantity')

    uttara_free_quantity = fields.Float(string="Uttara Free Quantity", compute='_get_uttara_free_quantity')
    nodda_free_quantity = fields.Float(string="Nodda Free Quantity", compute='_get_nodda_free_quantity')
    signboard_free_quantity = fields.Float(string="Signboard Free Quantity", compute='_get_signboard_free_quantity')
    badda_free_quantity = fields.Float(string="Badda Free Quantity", compute='_get_badda_free_quantity')
    savar_free_quantity = fields.Float(string="Savar Free Quantity", compute='_get_savar_free_quantity')
    badda_horeca_free_quantity = fields.Float(string="Badda HoReCa Free Quantity", compute='_get_badda_horeca_free_quantity')

    uttara_forecast_quantity = fields.Float(string="Uttara Forecast Quantity", compute='_get_uttara_forecast_quantity')
    nodda_forecast_quantity = fields.Float(string="Nodda Forecast Quantity", compute='_get_nodda_forecast_quantity')
    signboard_forecast_quantity = fields.Float(string="Signboard Forecast Quantity", compute='_get_signboard_forecast_quantity')
    badda_forecast_quantity = fields.Float(string="Badda Forecast Quantity", compute='_get_badda_forecast_quantity')
    savar_forecast_quantity = fields.Float(string="Savar Forecast Quantity", compute='_get_savar_forecast_quantity')
    badda_horeca_forecast_quantity = fields.Float(string="Badda HoReCa Forecast Quantity", compute='_get_badda_horeca_forecast_quantity')

    uttara_damage_quantity = fields.Float(string="Uttara Damage Quantity", compute='_get_uttara_damage_warehouse_quantity')
    nodda_damage_quantity = fields.Float(string="Nodda Damage Quantity", compute='_get_nodda_damage_warehouse_quantity')
    signboard_damage_quantity = fields.Float(string="Signboard Damage Quantity", compute='_get_signboard_damage_warehouse_quantity')
    badda_damage_quantity = fields.Float(string="Badda Damage Quantity", compute='_get_badda_damage_warehouse_quantity')
    savar_damage_quantity = fields.Float(string="Savar Damage Quantity", compute='_get_savar_damage_warehouse_quantity')

    total_damage_quantity = fields.Float(string="Total Damage Quantity", compute='_get_total_damage_quantity')

    def _get_uttara_free_quantity(self):
        loc_id = self.env["stock.warehouse"].get_warehouse_id('UttWH').lot_stock_id

        stock_in_hand = self._get_uttara_warehouse_quantity()
        forecasted_qty = self._get_fun_for_free_qty(loc_id.id)

        for k, v in stock_in_hand.items():
            stock_in_hand[k] = 0 if v is None else v
        for k, v in forecasted_qty.items():
            forecasted_qty[k] = 0 if v is None else v

        for record in self:
            res_count = stock_in_hand.get(record.id, 0) - forecasted_qty.get(record.id, 0)
            record.uttara_free_quantity = res_count if res_count > 0 else 0

    def _get_nodda_free_quantity(self):
        loc_id = self.env["stock.warehouse"].get_warehouse_id('NodWH').lot_stock_id

        stock_in_hand = self._get_nodda_warehouse_quantity()
        forecasted_qty = self._get_fun_for_free_qty(loc_id.id)

        for k, v in stock_in_hand.items():
            stock_in_hand[k] = 0 if v is None else v
        for k, v in forecasted_qty.items():
            forecasted_qty[k] = 0 if v is None else v

        for record in self:
            res_count = stock_in_hand.get(record.id, 0) - forecasted_qty.get(record.id, 0)
            record.nodda_free_quantity = res_count if res_count > 0 else 0

    def _get_signboard_free_quantity(self):
        loc_id = self.env["stock.warehouse"].get_warehouse_id('SinWH').lot_stock_id

        stock_in_hand = self._get_signboard_warehouse_quantity()
        forecasted_qty = self._get_fun_for_free_qty(loc_id.id)
        for k, v in stock_in_hand.items():
            stock_in_hand[k] = 0 if v is None else v
        for k, v in forecasted_qty.items():
            forecasted_qty[k] = 0 if v is None else v

        for record in self:
            res_count = stock_in_hand.get(record.id, 0) - forecasted_qty.get(record.id, 0)
            record.signboard_free_quantity = res_count if res_count > 0 else 0

    def _get_badda_free_quantity(self):
        loc_id = self.env["stock.warehouse"].get_warehouse_id('BddWH').lot_stock_id

        stock_in_hand = self._get_badda_warehouse_quantity()
        forecasted_qty = self._get_fun_for_free_qty(loc_id.id)
        for k, v in stock_in_hand.items():
            stock_in_hand[k] = 0 if v is None else v
        for k, v in forecasted_qty.items():
            forecasted_qty[k] = 0 if v is None else v

        for record in self:
            res_count = stock_in_hand.get(record.id, 0) - forecasted_qty.get(record.id, 0)
            record.badda_free_quantity = res_count if res_count > 0 else 0

    def _get_badda_horeca_free_quantity(self):
        loc_id = self.env["stock.warehouse"].get_warehouse_id('BdHoW').lot_stock_id

        stock_in_hand = self._get_badda_horeca_warehouse_quantity()
        forecasted_qty = self._get_fun_for_free_qty(loc_id.id)
        for k, v in stock_in_hand.items():
            stock_in_hand[k] = 0 if v is None else v
        for k, v in forecasted_qty.items():
            forecasted_qty[k] = 0 if v is None else v

        for record in self:
            res_count = stock_in_hand.get(record.id, 0) - forecasted_qty.get(record.id, 0)
            record.badda_horeca_free_quantity = res_count if res_count > 0 else 0

    def _get_savar_free_quantity(self):
        loc_id = self.env["stock.warehouse"].get_warehouse_id('SavWH').lot_stock_id

        stock_in_hand = self._get_savar_warehouse_quantity()
        forecasted_qty = self._get_fun_for_free_qty(loc_id.id)
        for k, v in stock_in_hand.items():
            stock_in_hand[k] = 0 if v is None else v
        for k, v in forecasted_qty.items():
            forecasted_qty[k] = 0 if v is None else v

        for record in self:
            res_count = stock_in_hand.get(record.id, 0) - forecasted_qty.get(record.id, 0)
            record.savar_free_quantity = res_count if res_count > 0 else 0

    ######### Free End ################

    # def _get_warehouse_quantity(self):
    #     for record in self:
    #         warehouse_quantity_text = ""
    #
    #         quant_ids = self.env["stock.quant"].search([("product_id", "=", record.id),
    #                                                     ("location_id.usage", "=", "internal")])
    #
    #         t_warehouses = {}
    #         for quant in quant_ids:
    #             if quant.location_id:
    #                 if quant.location_id.id not in t_warehouses:
    #                     t_warehouses.update({quant.location_id.id: 0})
    #                 t_warehouses[quant.location_id.id] += quant.quantity
    #
    #         tt_warehouses = {}
    #         for location in t_warehouses:
    #             warehouse = False
    #             location1 = location
    #
    #             while not warehouse and location1:
    #                 warehouse_id = self.env["stock.warehouse"].search([("lot_stock_id", "=", location1)])
    #                 if len(warehouse_id) > 0:
    #                     warehouse = True
    #                 else:
    #                     warehouse = False
    #                 # location1 = location1.location_id
    #             if warehouse_id:
    #                 # warehouse_id = self.env["stock.warehouse"].browse(warehouse_id)
    #                 if warehouse_id.name not in tt_warehouses:
    #                     tt_warehouses.update({warehouse_id.id: 0})
    #                 tt_warehouses[warehouse_id.id] += t_warehouses[location]
    #
    #         for item in tt_warehouses:
    #             if item == self.env["stock.warehouse"].get_warehouse_id('UttWH').id:
    #                 record.uttara_quantity = tt_warehouses[item]
    #             elif item == self.env["stock.warehouse"].get_warehouse_id('NodWH').id:
    #                 record.nodda_quantity = tt_warehouses[item]
    #             elif item == self.env["stock.warehouse"].get_warehouse_id('SinWH').id:
    #                 record.signboard_quantity = tt_warehouses[item]
    #             elif item == self.env["stock.warehouse"].get_warehouse_id('BddWH').id:
    #                 record.badda_quantity = tt_warehouses[item]
    #             elif item == self.env["stock.warehouse"].get_warehouse_id('BdHoW').id:
    #                 record.badda_horeca_quantity = tt_warehouses[item]
    #             elif item == self.env["stock.warehouse"].get_warehouse_id('SavWH').id:
    #                 record.savar_quantity = tt_warehouses[item]
    #
    #             if tt_warehouses[item] != 0:
    #                 warehouse_quantity_text = (
    #                         warehouse_quantity_text
    #                         + " ** "
    #                         + str(item)
    #                         + ": "
    #                         + str(tt_warehouses[item])
    #                 )
    #         # record.warehouse_quantity = warehouse_quantity_text

    def _get_onhand_quantity_by_warehouse(self, w_id):
        res = {}
        for record in self:
            quant_ids = self.env["stock.quant"].search([("product_id", "=", record.id),
                                                        ("location_id.usage", "=", "internal")])

            t_warehouses = {}
            for quant in quant_ids:
                if quant.location_id:
                    if quant.location_id.id not in t_warehouses:
                        t_warehouses.update({quant.location_id.id: 0})
                    t_warehouses[quant.location_id.id] += quant.quantity

            tt_warehouses = {}
            for location in t_warehouses:
                warehouse = False
                location1 = location

                while not warehouse and location1:
                    warehouse_id = self.env["stock.warehouse"].search([("lot_stock_id", "=", location1)])
                    if len(warehouse_id) > 0:
                        warehouse = True
                    else:
                        warehouse = False
                    # location1 = location1.location_id
                if warehouse_id:
                    # warehouse_id = self.env["stock.warehouse"].browse(warehouse_id)
                    if warehouse_id.name not in tt_warehouses:
                        tt_warehouses.update({warehouse_id.id: 0})
                    tt_warehouses[warehouse_id.id] += t_warehouses[location]

            for item in tt_warehouses:
                if item == w_id:
                    res[record.id] = tt_warehouses[item]

        return res

    def _get_uttara_warehouse_quantity(self):
        wh_id = self.env["stock.warehouse"].get_warehouse_id('UttWH').id
        return self._get_onhand_quantity_by_warehouse(wh_id)

    def _get_nodda_warehouse_quantity(self):
        wh_id = self.env["stock.warehouse"].get_warehouse_id('NodWH').id
        return self._get_onhand_quantity_by_warehouse(wh_id)

    def _get_signboard_warehouse_quantity(self):
        wh_id = self.env["stock.warehouse"].get_warehouse_id('SinWH').id
        return self._get_onhand_quantity_by_warehouse(wh_id)

    def _get_badda_warehouse_quantity(self):
        wh_id = self.env["stock.warehouse"].get_warehouse_id('BddWH').id
        return self._get_onhand_quantity_by_warehouse(wh_id)

    def _get_badda_horeca_warehouse_quantity(self):
        wh_id = self.env["stock.warehouse"].get_warehouse_id('BdHoW').id
        return self._get_onhand_quantity_by_warehouse(wh_id)

    def _get_savar_warehouse_quantity(self):
        wh_id = self.env["stock.warehouse"].get_warehouse_id('SavWH').id
        return self._get_onhand_quantity_by_warehouse(wh_id)

    def _compute_uttara_warehouse_quantity(self):
        wh_id = self.env["stock.warehouse"].get_warehouse_id('UttWH').id
        val = self._get_onhand_quantity_by_warehouse(wh_id)
        for k, v in val.items():
            val[k] = 0 if v is None else v

        for record in self:
            res_count = val.get(record.id, 0)
            record.uttara_quantity = res_count if res_count > 0 else 0

    def _compute_nodda_warehouse_quantity(self):
        wh_id = self.env["stock.warehouse"].get_warehouse_id('NodWH').id
        val = self._get_onhand_quantity_by_warehouse(wh_id)
        for k, v in val.items():
            val[k] = 0 if v is None else v

        for record in self:
            res_count = val.get(record.id, 0)
            record.nodda_quantity = res_count if res_count > 0 else 0

    def _compute_signboard_warehouse_quantity(self):
        wh_id = self.env["stock.warehouse"].get_warehouse_id('SinWH').id
        val = self._get_onhand_quantity_by_warehouse(wh_id)
        for k, v in val.items():
            val[k] = 0 if v is None else v

        for record in self:
            res_count = val.get(record.id, 0)
            record.signboard_quantity = res_count if res_count > 0 else 0

    def _compute_badda_warehouse_quantity(self):
        wh_id = self.env["stock.warehouse"].get_warehouse_id('BddWH').id
        val = self._get_onhand_quantity_by_warehouse(wh_id)
        for k, v in val.items():
            val[k] = 0 if v is None else v

        for record in self:
            res_count = val.get(record.id, 0)
            record.badda_quantity = res_count if res_count > 0 else 0

    def _compute_badda_horeca_warehouse_quantity(self):
        wh_id = self.env["stock.warehouse"].get_warehouse_id('BdHoW').id
        val = self._get_onhand_quantity_by_warehouse(wh_id)
        for k, v in val.items():
            val[k] = 0 if v is None else v

        for record in self:
            res_count = val.get(record.id, 0)
            record.badda_horeca_quantity = res_count if res_count > 0 else 0

    def _compute_savar_warehouse_quantity(self):
        wh_id = self.env["stock.warehouse"].get_warehouse_id('SavWH').id
        val = self._get_onhand_quantity_by_warehouse(wh_id)
        for k, v in val.items():
            val[k] = 0 if v is None else v

        for record in self:
            res_count = val.get(record.id, 0)
            record.savar_quantity = res_count if res_count > 0 else 0


    def _get_uttara_damage_warehouse_quantity(self):
        wh_id = self.env["stock.warehouse"].get_warehouse_id('UttDW').id
        val = self._get_onhand_quantity_by_warehouse(wh_id)
        for k, v in val.items():
            val[k] = 0 if v is None else v

        for record in self:
            res_count = val.get(record.id, 0)
            record.uttara_damage_quantity = res_count if res_count > 0 else 0


    def _get_nodda_damage_warehouse_quantity(self):
        wh_id = self.env["stock.warehouse"].get_warehouse_id('NodDW').id
        val = self._get_onhand_quantity_by_warehouse(wh_id)
        for k, v in val.items():
            val[k] = 0 if v is None else v

        for record in self:
            res_count = val.get(record.id, 0)
            record.nodda_damage_quantity = res_count if res_count > 0 else 0


    def _get_signboard_damage_warehouse_quantity(self):
        wh_id = self.env["stock.warehouse"].get_warehouse_id('SinDW').id
        val = self._get_onhand_quantity_by_warehouse(wh_id)
        for k, v in val.items():
            val[k] = 0 if v is None else v

        for record in self:
            res_count = val.get(record.id, 0)
            record.signboard_damage_quantity = res_count if res_count > 0 else 0

    def _get_badda_damage_warehouse_quantity(self):
        wh_id = self.env["stock.warehouse"].get_warehouse_id('BddDW').id
        val = self._get_onhand_quantity_by_warehouse(wh_id)
        for k, v in val.items():
            val[k] = 0 if v is None else v

        for record in self:
            res_count = val.get(record.id, 0)
            record.badda_damage_quantity = res_count if res_count > 0 else 0

    def _get_savar_damage_warehouse_quantity(self):
        wh_id = self.env["stock.warehouse"].get_warehouse_id('SavDW').id
        val = self._get_onhand_quantity_by_warehouse(wh_id)
        for k, v in val.items():
            val[k] = 0 if v is None else v

        for record in self:
            res_count = val.get(record.id, 0)
            record.savar_damage_quantity = res_count if res_count > 0 else 0





    def _get_fun_for_free_qty(self, location):
        res = {}
        ### v8.0
        ### query = "select sum(qty) from stock_quant where reservation_id NOTNULL and product_id = {0} and location_id = {1}"

        ### v14.0 third party code
        # query = "select sum(quantity) from stock_quant where reserved_quantity != 0 and product_id = {0} and location_id = {1}"

        query = "select sum(reserved_quantity) from stock_quant where reserved_quantity != 0 and product_id = {0} and location_id = {1}"
        for item in self.ids:
            self.env.cr.execute(query.format(item, location))
            result = self.env.cr.fetchall()
            if result[0][0] is not None:
                res[item] = result[0][0]
            else:
                res[item] = 0
        return res

    def _get_uttara_forecast_quantity(self):
        ctx = self.env.context
        wh_id = self.env["stock.warehouse"].get_warehouse_id('UttWH').id
        for product in self:
            product.uttara_forecast_quantity = product.with_context(warehouse=wh_id).virtual_available

        self.env.context = dict(ctx)

    def _get_nodda_forecast_quantity(self):
        ctx = self.env.context
        wh_id = self.env["stock.warehouse"].get_warehouse_id('NodWH').id
        for product in self:
            product.nodda_forecast_quantity = product.with_context(warehouse=wh_id).virtual_available

        self.env.context = dict(ctx)

    def _get_signboard_forecast_quantity(self):
        ctx = self.env.context
        wh_id = self.env["stock.warehouse"].get_warehouse_id('SinWH').id
        for product in self:
            product.signboard_forecast_quantity = product.with_context(warehouse=wh_id).virtual_available

        self.env.context = dict(ctx)

    def _get_badda_forecast_quantity(self):
        ctx = self.env.context
        wh_id = self.env["stock.warehouse"].get_warehouse_id('BddWH').id
        for product in self:
            product.badda_forecast_quantity = product.with_context(warehouse=wh_id).virtual_available

        self.env.context = dict(ctx)

    def _get_badda_horeca_forecast_quantity(self):
        ctx = self.env.context
        wh_id = self.env["stock.warehouse"].get_warehouse_id('BdHoW').id
        for product in self:
            product.badda_horeca_forecast_quantity = product.with_context(warehouse=wh_id).virtual_available

        self.env.context = dict(ctx)

    def _get_savar_forecast_quantity(self):
        ctx = self.env.context
        wh_id = self.env["stock.warehouse"].get_warehouse_id('SavWH').id
        for product in self:
            product.savar_forecast_quantity = product.with_context(warehouse=wh_id).virtual_available

        self.env.context = dict(ctx)

    def _get_total_damage_quantity(self):

        for record in self:
            record.total_damage_quantity = str(record.uttara_damage_quantity + record.nodda_damage_quantity + record.signboard_damage_quantity + record.badda_damage_quantity+ record.savar_damage_quantity)

            return record


