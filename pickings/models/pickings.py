from odoo.exceptions import UserError, ValidationError, Warning
from odoo import api, fields, models, _
from odoo import SUPERUSER_ID, api
from odoo.tools.translate import _
from datetime import datetime
from odoo.osv import osv


class StockPicking(models.Model):
    _inherit = 'stock.picking'

    picked = fields.Boolean('Picked')


class PickingList(models.Model):
    _name = 'picking.list'
    _description = "Picking List"
    _order = 'id desc'

    name = fields.Char('Order Reference', required=True, copy=False, readonly=True,
                       states={'draft': [('readonly', False)], 'sent': [('readonly', False)]}, index=True, default='/')
    picking_date = fields.Datetime('Date', required=True, readonly=True, index=True, copy=False,
                                   default=fields.Datetime.now)
    description = fields.Char('Picker Name')
    create_date = fields.Datetime('Creation Date', readonly=True, index=True,
                                  help="Date on which sales order is created.")
    date_confirm = fields.Datetime('Confirmation Date', readonly=True, index=True,
                                   help="Date on which sales order is confirmed.", copy=False)
    user_id = fields.Many2one('res.users', 'Assigned to', index=True, tracking=True)

    picking_line = fields.One2many('picking.list.line', 'picking_id', 'Picking List Lines', required=True)
    picking_products_line = fields.One2many('picking.products.line', 'picking_id', 'Picking Products Line',
                                            required=False)
    product_scan = fields.Char('Product Scan')
    generate_packed = fields.Boolean('Generate Packed', default=False, help="Generate Pack button hide after action")
    state = fields.Selection([
        ('pending', 'Waiting for Picking'),
        ('done', 'Done'),
        ('cancel', 'Cancelled'),
    ], 'Status', default='pending', readonly=True, copy=False, help="Gives the status of the quotation or sales order",
        index=True)

    def make_confirm(self):
        self.write({
            'state': 'done',
            'date_confirm': fields.Datetime.now(),
            'generate_packed': True,
        })

        self.picking_line.write({'state': 'done'})

        for pl in self:
            for pl_line in pl.picking_line:
                pl_line.stock_id.write({'picked': True})

    def make_cancel(self):
        id_list = self.ids
        stock_picking_ids = []
        for element in id_list:
            ids = [element]

            if ids is not None:
                self.env.cr.execute("update picking_list set state='cancel' where id=%s", (ids))
                self.env.cr.execute("update picking_list_line set state='cancel' where picking_id=%s", (ids))

            self.env.cr.execute("select stock_id from  picking_list_line where picking_id=%s", (ids))

            for item in self.env.cr.fetchall():
                for tuple_items in list(item):
                    stock_picking_ids.append(tuple_items)

        for items in stock_picking_ids:
            self.env.cr.execute("update stock_picking set picked= False where id=%s", ([items]))

        return True

    ## Stock Transfer Code is here

    def stock_transfer_from_picking(self, stock_list=[]):
        immediate_transfer_line_ids = []
        for picking_id in stock_list:
            immediate_transfer_line_ids.append([0, False, {
                'picking_id': picking_id[0],
                'to_immediate': True
            }])

        res = self.env['stock.immediate.transfer'].create({
            'pick_ids': [(4, p[0]) for p in stock_list],
            'show_transfers': False,
            'immediate_transfer_line_ids': immediate_transfer_line_ids
        })

        return res.with_context(button_validate_picking_ids=res.pick_ids.ids).process()

    ## Ends the Stock transfer Code

    def check_product_has_warranty_or_not(self):
        uid = 1
        # validating Product Warranty serials
        stock_id = None
        found_warranty_applicable = False
        if stock_id is not None:
            self.env.cr.execute(
                "select product_product.warranty_applicable from stock_move,product_product where stock_move.product_id=product_product.id and  stock_move.picking_id=%s",
                ([stock_id]))
            for w_info in self.env.cr.fetchall():
                if w_info[0] == True:
                    found_warranty_applicable = True
                    break

        return found_warranty_applicable

    def make_pack(self):
        for pl in self:
            packing_line_list = []
            stock_list = []
            order_list = []

            pack_data = {
                'description': 'System Generated',
                'picking_id': pl.id,
            }

            for line_items in pl.picking_line:
                if line_items.stock_id.state != 'cancel':
                    stock_list.append((line_items.stock_id.id, line_items.magento_no))
                    packing_line_list.append(
                        [0, False,
                         {'stock_id': line_items.stock_id.id, 'magento_no': line_items.magento_no, 'details': False,
                          'receive_date': datetime.now(), 'order_number': False, 'return_date': False}])

                    order_list.append(line_items.magento_no)

            self.stock_transfer_from_picking(stock_list)
            pack_data['packing_line'] = packing_line_list

            if len(packing_line_list) > 0:
                saved_id = self.env['packing.list'].create(pack_data)

            pl.write({'generate_packed': False})

            for order_row in self.env["sale.order"].search([("client_order_ref", "in", order_list)]):
                try:
                    data = {
                        'odoo_order_id': order_row.id,
                        'magento_id': str(order_row.client_order_ref),
                        'order_state': str('packed'),
                        'state_time': fields.datetime.now(),
                    }
                    new_r = self.env["order.status.synch.log"].sudo().create(data)
                except:
                    pass

        return True

    def create(self, data):
        # context = dict(context or {})

        stock_picking_list = []
        if data.get('picking_line'):
            picking_line = []
            for items in data.get('picking_line'):
                att_data = {}
                for att_data in items:
                    if type(att_data) is dict:
                        stock_id = att_data.get('stock_id')
                        stock_picking_list.append(stock_id)
                        stock_obj = self.env['stock.picking'].browse(stock_id)

                        # att_data['magento_no'] = " "
                        att_data['magento_no'] = stock_obj.mag_no
                        att_data['order_number'] = stock_obj.origin
                picking_line.append([0, False, att_data])

            data['picking_line'] = picking_line

        if stock_picking_list:
            abc = []
            for items in stock_picking_list:
                self.env.cr.execute("select picking_id from  picking_list_line where state !='cancel' and stock_id=%s",
                                    ([items]))

                abc += self.env.cr.fetchall()

            if len(abc) > 0:
                raise osv.except_osv(_('Message'), _("Already Pickied the numbers"))

        picking_id = super(PickingList, self).create(data)

        if picking_id:
            for items in data['picking_line']:
                for att in items:
                    if isinstance(att, dict):
                        self.env.cr.execute("update stock_picking set picked= True where id=%s",
                                            (att.get('stock_id'),))

        return picking_id

    @api.onchange('product_scan')
    def picking_products_scan(self):
        if self.product_scan:
            scanned_ean = self.product_scan

            if self.picking_products_line and scanned_ean != 'False':
                first_picking_products_line_id = self.picking_products_line[0].id
                picking_id_query = "SELECT picking_id FROM picking_products_line WHERE id={0}".format(
                    first_picking_products_line_id)
                self._cr.execute(picking_id_query)
                picking_id = self._cr.fetchone()[0]

                # picking_list = self.env['picking.list'].browse([picking_id])

                # picking_products_line
                qty_update = False
                # for product in picking_list.picking_products_line:
                for product in self.picking_products_line:

                    if product.product_ean == scanned_ean and product.total_challan_qty > product.picking_qty:
                        product.picking_qty += 1
                        qty_update = True
                        break

                if not qty_update:
                    # give waring msg
                    # EAN dose not exists with the product
                    pass

            self.product_scan = ""

            # return "xXxXxXxXxX"

    def write(self, vals):

        # only update only if (picking_qty is less or equal to total_challan_qty)
        update_permission = list()

        if vals.get('picking_products_line', False):
            for single_line in vals['picking_products_line']:
                if not not single_line[2]:
                    for picking_products_l in self.picking_products_line:
                        if picking_products_l.id == single_line[1]:
                            picking_qty = single_line[2]['picking_qty'] if single_line[2].has_key(
                                'picking_qty') else picking_products_l.picking_qty

                            if picking_qty > picking_products_l.picking_qty:
                                update_permission.append(False)
                            else:
                                update_permission.append(True)

        if False in update_permission:

            raise osv.except_osv(_('Picking line adjustment ERROR!'),
                                 _('Picking quantity should be less then or equal to (total challan quantity)!!!'))

        else:
            record = super(PickingList, self).write(vals)

            return record


class PickingListLine(models.Model):
    _name = 'picking.list.line'
    _description = "Picking Line List"

    picking_id = fields.Many2one('picking.list', 'Order Reference', required=True, ondelete='cascade', index=True,
                                 readonly=True)
    stock_id = fields.Many2one('stock.picking', string='Stock Picking')
    sequence = fields.Integer('Sequence', help="Gives the sequence order when displaying a list of sales order lines.")
    details = fields.Char('Details')
    receive_date = fields.Date('Receive Date', required=True, default=fields.Datetime.now)
    return_date = fields.Date('Return Date')
    order_number = fields.Char('Order Number')
    magento_no = fields.Char('Magento No')
    state = fields.Selection([
        ('pending', 'Waiting for Picking'),
        ('done', 'Done'),
        ('cancel', 'Cancelled'),

    ], 'Status', default='pending', readonly=True, copy=False, help="Gives the status of the quotation or sales order",
        index=True)

    # ## Block code for V-14
    #
    # _columns = {
    #     'picking_id': fields.many2one('picking.list', 'Order Reference', required=True, ondelete='cascade', index=True,
    #                                   readonly=True),
    #
    #     'stock_id': fields.many2one('stock.picking', string='Stock Picking'),
    #     'sequence': fields.integer('Sequence',
    #                                help="Gives the sequence order when displaying a list of sales order lines."),
    #     'details': fields.char('Details'),
    #     'receive_date': fields.date('Receive Date', required=True),
    #     'return_date': fields.date('Return Date'),
    #     'order_number': fields.char('Order Number'),
    #     'magento_no': fields.char('Magento No'),
    #     'state': fields.selection([
    #         ('pending', 'Waiting for Picking'),
    #         ('done', 'Done'),
    #         ('cancel', 'Cancelled'),
    #
    #     ], 'Status', readonly=True, copy=False, help="Gives the status of the quotation or sales order", index=True),
    # }
    #
    # _defaults = {
    #     'receive_date': fields.datetime.now,
    #     'state': 'pending',
    #
    # }


class PickingProducts(models.Model):
    _name = 'picking.products'
    _description = "Picking Products"

    picking = fields.Many2one('picking.list', 'Order Reference')
    state = fields.Selection([
        ('pending', 'Waiting for Picking'),
        ('done', 'Done'),
        ('cancel', 'Cancelled'),
    ], 'Status', readonly=True, copy=False, help="Gives the status of the quotation or sales order", index=True)
    pick_prod_line = fields.One2many('picking.products.line', 'pick_prod_id', 'Picking Products Lines', required=True)

    # ## Block code for V-14
    # _columns = {
    #     'picking':  fields.many2one('picking.list', 'Order Reference'),
    #     'state': fields.selection([
    #         ('pending', 'Waiting for Picking'),
    #         ('done', 'Done'),
    #         ('cancel', 'Cancelled'),
    #
    #     ], 'Status', readonly=True, copy=False, help="Gives the status of the quotation or sales order", index=True),
    #     'pick_prod_line': fields.one2many('picking.products.line', 'pick_prod_id', 'Picking Products Lines', required=True),
    # }


class PickingProductsLine(models.Model):
    _name = 'picking.products.line'
    _description = "Picking Products Line"

    pick_prod_id = fields.Many2one('picking.products', 'Picking products', required=True, ondelete='cascade',
                                   index=True, readonly=True)
    picking_id = fields.Many2one('picking.list', 'Picking ID', required=False,
                                 ondelete='cascade', index=True, readonly=True)
    product_id = fields.Many2one('product.product', 'Product', readonly=True)
    product = fields.Char('Product Name', readonly=True)
    product_ean = fields.Char('Product EAN', readonly=True)
    total_challan_qty = fields.Float('Total Challan Qty', readonly=True)
    picking_qty = fields.Float('Picking Qty')
    return_date = fields.Date('Return Date')
    state = fields.Selection([
        ('pending', 'Waiting for Picking'),
        ('done', 'Done'),
        ('cancel', 'Cancelled'),

    ], 'Status', readonly=True, copy=False, help="Gives the status of the quotation or sales order", index=True)

    # ## Block code for V-14
    #
    # _columns = {
    #     'pick_prod_id': fields.many2one('picking.products', 'Picking products', required=True, ondelete='cascade', index=True, readonly=True),
    #     'picking_id': fields.many2one('picking.list', 'Picking ID', required=False,
    #                              ondelete='cascade', index=True, readonly=True),
    #     'product_id': fields.many2one('product.product', 'Product', readonly=True),
    #     'product': fields.char('Product Name', readonly=True),
    #     'product_ean': fields.char('Product EAN', readonly=True),
    #     'total_challan_qty': fields.float('Total Challan Qty', readonly=True),
    #     'picking_qty': fields.float('Picking Qty'),
    #     'return_date': fields.date('Return Date'),
    #     'state': fields.selection([
    #         ('pending', 'Waiting for Picking'),
    #         ('done', 'Done'),
    #         ('cancel', 'Cancelled'),
    #
    #     ], 'Status', readonly=True, copy=False, help="Gives the status of the quotation or sales order", index=True),
    #
    # }
