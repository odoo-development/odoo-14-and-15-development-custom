
{
    'name': 'Picking List Generation',
    'version': "14.0.1.0.0",
    'category': 'Sales',
    'description': """
Generate Picking List from Delivery Challan / Sales Order   
==============================================================

""",
    'author': 'Odoo Bangladesh',
    'depends': ['stock',
                'packings',
                'wms_base',
                'wms_menulist',
                ### Commented By Matiar Rahman
                ### Logically depends on 'waarehouse_order'
                'warehouse_order',
                ],
    # 'depends': ['stock', 'packings', 'product_simple_sync'],
    'data': [
        'wizard/stock_picking_select_view.xml',
        'security/picking_security.xml',
        'security/ir.model.access.csv',
        'views/picking_view.xml',
        'report/picking.xml',
        'report/report_picking.xml',
    ],

    'installable': True,
    'auto_install': False,
}