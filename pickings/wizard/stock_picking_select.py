
from odoo import api, fields, models, _
from odoo.tools.translate import _
from datetime import date, datetime
from odoo.exceptions import UserError, ValidationError


class SaleMakeInvoice(models.Model):
    _name = "picking.list.multiple"
    _description = "Picking List"

    picking_date = fields.Datetime('Date', required=True, readonly=True, index=True, copy=False,  default=fields.Datetime.now)
    description = fields.Char('Reference/Description')
    create_date = fields.Datetime('Creation Date', readonly=True, index=True,
                                   help="Date on which sales order is created.")
    date_confirm = fields.Date('Confirmation Date', readonly=True, index=True,
                                help="Date on which sales order is confirmed.", copy=False)
    user_id = fields.Many2one('res.users', 'Assigned to', index=True, tracking=True, default=lambda self: self.env.user)

    picking_line = fields.One2many('picking.list.line', 'picking_id', 'Picking List Lines', required=True)

    # ## Block code for V-14
    #
    # _columns = {
    #     'picking_date': fields.datetime('Date', required=True, readonly=True, index=True, copy=False),
    #     'description': fields.char('Reference/Description'),
    #     'create_date': fields.datetime('Creation Date', readonly=True, index=True,
    #                                    help="Date on which sales order is created."),
    #     'date_confirm': fields.date('Confirmation Date', readonly=True, index=True,
    #                                 help="Date on which sales order is confirmed.", copy=False),
    #     'user_id': fields.many2one('res.users', 'Assigned to', index=True, track_visibility='onchange'),
    #
    #     'picking_line': fields.one2many('picking.list.line', 'picking_id', 'Picking List Lines', required=True),
    #
    # }
    #
    # _defaults = {
    #     'picking_date': fields.datetime.now,
    #     'user_id': lambda obj, cr, uid, context: uid,
    #
    # }

    def make_picking(self):
        picking_obj = self.env['picking.list']

        # if context is None:
        #     context = {}
        # get_data = self.read()[0]
        data ={}
        picking_line = []
        today_date = date.today()

        if len(self.env.context.get('active_ids')) > 20 :
            raise ValidationError( _("Max allowed order is 20. Please select only 20 order at a time."))

        data['description'] = self.description

        for items in self.env.context.get('active_ids'):
            picking_line.append([0, False, {
                'stock_id': items,
                'receive_date': today_date.strftime('%Y-%m-%d'),
                'details': False,
                'state': 'pending',
                'return_date': False
            }])

        data['picking_line'] = picking_line


        save_the_data = picking_obj.create(data)

        # create picking_products and picking_products_line
        # save_the_data is the id of the picking_list created

        pl_obj = save_the_data if save_the_data else False

        pp_data = {
            'picking': pl_obj.id,
            'state': 'pending',
        }

        pp_line_list = list()

        """
        product_dict = {"01234": 10}
        """
        product_dict = dict()
        for pick_line in pl_obj.picking_line:

            # 'stock_id': fields.many2one('stock.picking', string='Stock Picking')
            # if the product_dict does not contains product then add with 0 quantity
            for product in pick_line.stock_id.move_lines:
                product_id = product.product_id.id

                if product_dict.get(str(product_id)):
                    product_dict[str(product_id)] = float(product.product_qty) + product_dict[str(product_id)]
                else:
                    product_dict[str(product_id)] = float(product.product_qty)

        # create picking_product_line
        product_obj = self.env['product.product']
        for key, value in product_dict.items():
            if value != 0.00:
                prod = product_obj.browse([int(key)])
                pp_line_list.append([0, False,
                                      {
                                          'picking_id': save_the_data.id,
                                          'product_id': int(key),
                                          'product': prod.name,
                                          'product_ean': prod.barcode if prod.barcode else None,
                                          'total_challan_qty': value ,
                                          'picking_qty': value,
                                          'state': 'pending',
                                      }])

        pp_data['pick_prod_line'] = pp_line_list
        if pp_line_list:
            pick_pro_env = self.env['picking.products']
            saved_pick_pro_id = pick_pro_env.create(pp_data)
