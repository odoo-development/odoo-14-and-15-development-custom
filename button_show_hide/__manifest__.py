#-*- coding:utf-8 -*-
{
    'name': "Hide Create, Edit, Import, Export and other custom button",
    'version': '14.0.1.0.0',
    'description': "Hide create, edit, import, export and other custom button based on groups",
    'summary': 'This module helps to hide create, edit, import, export and other custom button from List, '
               'Form and Kanban view based on groups.',
    'author': 'Sindabad.com',
    'category': 'Web',
    'depends': ['web','base_import'],
    'data': [
        'security/groups.xml',
        'views/assets.xml',
    ],
    'qweb': [
        'static/src/xml/create_edit_btn.xml',
    ],
    'installable': True,
}
