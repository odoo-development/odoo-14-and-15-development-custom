# Copyright 2021 Rocky

from odoo import fields, models, api, _


class ResPartner(models.Model):
    _inherit = 'res.partner'

    x_classification = fields.Char(string="Classification")
    x_industry = fields.Char(string="Industry")

    x_cus_dob = fields.Char(string="Date of Birth")

    x_customer_source = fields.Char(string="Customer Source")
    x_created_at = fields.Datetime(string="Customer Since")

    company_code = fields.Char(string="Company Code")
    parent_code = fields.Char(string="Parent Company Code", compute='_get_parent_code', store=True)

    magento_company_id = fields.Integer(string="Magento Company ID")
    credit_days = fields.Integer(string="Credit Days")

    monthly_limit = fields.Boolean(string="Monthly Limit")
    x_exceed_limit = fields.Boolean(string="Exceed Limit")

    cus_on_demand = fields.Boolean(string="Customer On Demand")

    @api.depends('company_code', 'parent_code')
    def _get_parent_code(self):
        res = {}

        for cus in self:
            cus.parent_code = cus.parent_id.company_code if cus.parent_id.company_code else cus.company_code

        # return res
