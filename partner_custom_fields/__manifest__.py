# Copyright 2021 Rocky

{
    "name": "Partner Custom Fields",
    "version": "13.0.1",
    "author": "Rocky",
    "category": "Sales Management",
    "license": "AGPL-3",
    "depends": ['sale_management'],
    "data": [
         "views/partner_view.xml",
    ],
    "installable": True,
}
