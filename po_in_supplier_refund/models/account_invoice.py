from odoo import models, fields, _


class AccountMove(models.Model):
    _inherit = "account.move"

    def _get_po(self):
        try:

            self._cr.execute(
                "SELECT name FROM account_move WHERE id=%s", (str(self.id),)
            )

            account_invoice_data = self._cr.fetchall()
            if (
                    len(account_invoice_data) == 0
                    or account_invoice_data[0][0] is None
            ):
                account_invoice_ref = None
            else:
                account_invoice_ref = account_invoice_data[0][0]

            if str(account_invoice_ref).startswith("PO"):
                purchase_order_data = str(account_invoice_ref)
            else:

                if account_invoice_ref is not None:
                    self._cr.execute(
                        "SELECT name FROM purchase_order WHERE client_order_ref=%s",
                        (str(account_invoice_ref),),
                    )

                    purchase_order_data = self._cr.fetchall()[0][0]
                else:

                    purchase_order_data = "No PO Found"

            self.po_number = purchase_order_data

        except:
           pass

    po_number = fields.Char(compute="_get_po", string="PO Number")
