from odoo import api, models, _
from datetime import datetime


class MushakChallanReport(models.AbstractModel):
    _name = 'report.vat_calculation_custom.report_mushak_challan'
    _description = 'Mushak Challan Report'

    @api.model
    def _get_report_values(self, docids, data=None):
        docs = self.env['account.move'].browse(docids[0])

        so_ids = self.env['sale.order'].search([('name', '=', docs.invoice_origin)])

        # order_data = self.env['sale.order'].browse(so_ids)

        wh_id = so_ids.warehouse_id.id

        warehouse_map = {1: 'UttWH',2: 'NodWH',3: 'SinWH',7: 'BddWH', 10: 'BdHoW',11: 'SavWH'}

        if warehouse_map[wh_id] == 'UttWH':
            issue_address = 'House No-46, Block-B, Ward-04, Noyanagor, Main Road, Turag, Dhaka-1230'
        elif warehouse_map[wh_id] == 'SinWH':
            issue_address = 'Hatim supermarket Sanarpar main road,SignBoard , Shiddhirganj, Dhaka Narayanganj-1430'
        elif warehouse_map[wh_id] == 'BddWH':
            issue_address = 'Vatara Thana, badda main road, sayednogor, Badda, Dhaka-1212'
        elif warehouse_map[wh_id] == 'BdHoW':
            issue_address = 'Vatara Thana, badda main road, sayednogor, Badda, Dhaka-1212'
        elif warehouse_map[wh_id] == 'SavWH':
            issue_address = 'Boliapur, nagarkanda, Savar, Dhaka-1212'
        else:
            issue_address = 'Ka-61, 5/A, Pragati Sharani, Baridhara, Dhaka-1212'

        issue_address = issue_address

        # Customer Shipping address
        shipping_address = ''
        if so_ids.partner_shipping_id.name:
            shipping_address = so_ids.partner_shipping_id.name + ", "
        if so_ids.partner_shipping_id.street:
            shipping_address = shipping_address + ", " + so_ids.partner_shipping_id.street + ", "
        if so_ids.partner_shipping_id.street2:
            shipping_address = shipping_address + so_ids.partner_shipping_id.street2 + ", "
        if so_ids.partner_shipping_id.city:
            shipping_address = shipping_address + so_ids.partner_shipping_id.city + ", "
        if so_ids.partner_shipping_id.state_id:
            shipping_address = shipping_address + so_ids.partner_shipping_id.state_id.name + ", "
        if so_ids.partner_shipping_id.zip:
            shipping_address = shipping_address + so_ids.partner_shipping_id.zip + ", "
        if so_ids.partner_invoice_id.country_id:
            shipping_address = shipping_address + so_ids.partner_invoice_id.country_id.name

        shipping_address = shipping_address

        # Customer Billing address
        billing = so_ids.partner_id.parent_id if so_ids.partner_id.parent_id else so_ids.partner_id
        billing_address = ''
        if billing.street:
            billing_address = billing.street + ", "
        if billing.street2:
            billing_address = billing_address + billing.street2 + ", "
        if billing.city:
            billing_address = billing_address + billing.city + ", "
        if billing.state_id:
            billing_address = billing_address + billing.state_id.name + ", "
        if billing.zip:
            billing_address = billing_address + billing.zip + ", "
        if billing.country_id:
            billing_address = billing_address + billing.country_id.name

        billing_address = billing_address

        if str(docs.move_type) == 'out_invoice' and str(docs.state) != 'cancel':

            total_vat = 0.00
            total_subtotal = 0.00
            p_total_amount = 0.00
            total_service_vat = 0.00

            for items in docs.invoice_line_ids:

                total_amount = items.price_subtotal_with_vat
                service_vat = (items.five_vat * 100) / 5
                p_t_amount = (total_amount - service_vat) - items.five_vat

                total_vat += items.five_vat
                total_subtotal += total_amount
                p_total_amount += p_t_amount
                total_service_vat += service_vat

        return {
            'doc_ids': docids,
            'doc_model': 'account.move',
            'docs': docs,
            'data': data,

            'company_name': 'Sindabad.com Ltd.',
            'company_bin_no': '000064664-0101',
            'issue_address': issue_address,

            'partner_name': docs.partner_id.commercial_partner_id.name if docs.partner_id.commercial_partner_id.name else docs.partner_id.name,
            'partner_bin_no': '',
            'billing_address': billing_address,
            'shipping_address': shipping_address,

            'issued_date': datetime.now().strftime('%d-%m-%Y'),
            'issued_time': datetime.now().strftime('%H:%M:%S'),

            'total_vat':round(total_vat, 2),
            'total_subtotal':round(total_subtotal, 2),
            'p_total_amount':round(p_total_amount, 2),
            'total_service_vat':round(total_service_vat, 2),
        }
