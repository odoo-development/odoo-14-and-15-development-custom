# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2009 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

import time
from openerp.report import report_sxw
from openerp.osv import osv
from openerp.tools.amount_to_text_en import amount_to_text

class billcollectinvoicedata(report_sxw.rml_parse):
    def __init__(self, cr, uid, name, context):
        super(billcollectinvoicedata, self).__init__(cr, uid, name, context)
        self.localcontext.update({
            'get_all_invoices_data': self.get_all_invoices_data,
            'get_invoices_list': self.get_invoices_list,

        })

    def get_all_invoices_data(self, obj):

        try:
            order_numbers = [obj.split(',')[0]]
        except:
            order_numbers = [str(obj)]

        group_id = None
        data_list = []

        self.cr.execute("SELECT id FROM bill_collection_billing_process WHERE name=%s", (order_numbers))
        for item in self.cr.fetchall():
            group_id = [item[0]]

        if group_id is not None:

            order_data = self.pool.get('bill.collection.billing.process').browse(self.cr, self.uid, group_id)

            account_invoice_ids = order_data.order_id.invoice_ids

            product_id_list=[]
            return_product_id_list=[]

            for invoice in account_invoice_ids:

                if str(invoice.state) != 'cancel':
                    str_state = 'Received' if str(invoice.type) == 'out_invoice' else 'Return'

                    for it in invoice.invoice_line:

                        regular_price = it.x_discount + it.vat_on_unit_profit_loss + it.price_unit
                        if it.quantity >0.00:

                            if 'Shipping' not in str(it.name):
                                unit_cost = it.unit_cost
                                quantity = it.quantity
                                unit_cost_profit_loss = it.unit_cost_profit_loss
                                total_profit_loss = it.total_profit_loss
                                five_vat = it.five_vat
                                total_price_with_vat = it.total_price_with_vat
                                unit_price_with_vat = it.unit_price_with_vat if it.unit_price_with_vat>0 else it.price_unit
                                discount_per_unit = it.x_discount
                                vat_on_unit_profit_loss = it.vat_on_unit_profit_loss
                                unit_price = it.price_unit
                                price_subtotal = it.price_subtotal

                                result = {
                                      'name': 'cool',
                                      'product_name': it.product_id.product_name,
                                      'product_id': it.product_id.id,
                                      'quantity': "{0:.2f}".format(quantity),
                                      'unit_cost':  "{0:,.2f}".format(unit_cost),
                                      'unit_cost_profit_loss':"{0:,.2f}".format(unit_cost_profit_loss),
                                      'total_profit_loss': "{0:,.2f}".format(total_profit_loss),
                                      'five_vat': "{0:,.2f}".format(five_vat),
                                      'total_price_with_vat': "{0:,.2f}".format(total_price_with_vat),
                                      # 'unit_price_with_vat': "{0:,.2f}".format(unit_price_with_vat),
                                      'unit_price_with_vat': "{0:,.2f}".format(float(total_price_with_vat)/float(quantity)),
                                      'regular_price':  "{0:,.2f}".format(regular_price),
                                      'discount_per_unit': "{0:,.2f}".format(it.x_discount),
                                      'vat_on_unit_profit_loss': "{0:,.2f}".format(it.vat_on_unit_profit_loss),
                                      'unit_price': "{0:,.2f}".format(it.price_unit),
                                      'uom': it.product_id.prod_uom,
                                      'amount': "{0:,.2f}".format(price_subtotal),
                                      'status_data': str_state,
                                      'five_vat_sum': five_vat,
                                      'quantity_sum': quantity,
                                      'total_price_with_vat_sum': total_price_with_vat,
                                      'unit_price_with_vat_sum': float(total_price_with_vat)/float(quantity),
                                      'price_subtotal_sum': price_subtotal,
                                      }

                                if it.product_id.id in product_id_list and str(invoice.type) == 'out_invoice':
                                    for row in data_list:
                                        if row['product_id']== it.product_id.id and row['status_data'] == 'Received' :
                                            # unit_cost += it.unit_cost

                                            quantity = float(row['quantity_sum'])
                                            five_vat = float(row['five_vat_sum'])
                                            total_price_with_vat = float(row['total_price_with_vat_sum'])
                                            unit_price_with_vat = float(row['unit_price_with_vat_sum'])
                                            price_subtotal = float(row['price_subtotal_sum'])

                                            quantity += it.quantity
                                            unit_cost_profit_loss += unit_cost_profit_loss
                                            total_profit_loss += total_profit_loss
                                            five_vat += it.five_vat
                                            total_price_with_vat += it.total_price_with_vat
                                            # unit_price_with_vat += it.unit_price_with_vat if it.unit_price_with_vat>0 else it.price_unit
                                            discount_per_unit += it.x_discount
                                            vat_on_unit_profit_loss += vat_on_unit_profit_loss
                                            unit_price += unit_price
                                            price_subtotal += it.price_subtotal

                                            row['quantity']= "{0:.2f}".format(quantity)
                                            row['five_vat']= "{0:,.2f}".format(five_vat)
                                            row['total_price_with_vat']= "{0:,.2f}".format(total_price_with_vat)
                                            row['unit_price_with_vat']= "{0:,.2f}".format((total_price_with_vat/quantity))
                                            row['amount']= "{0:,.2f}".format(price_subtotal)

                                            row['quantity_sum'] = quantity
                                            row['five_vat_sum'] = five_vat
                                            row['total_price_with_vat_sum'] = total_price_with_vat
                                            row['unit_price_with_vat_sum'] = total_price_with_vat/quantity
                                            row['price_subtotal_sum'] = price_subtotal

                                else:
                                    if it.product_id.id in return_product_id_list and str(invoice.type) == 'out_refund':
                                        for row in data_list:
                                            if row['product_id'] == it.product_id.id and row['status_data'] == 'Return':
                                                quantity = float(row['quantity_sum'])
                                                five_vat = float(row['five_vat_sum'])
                                                total_price_with_vat = float(row['total_price_with_vat_sum'])
                                                unit_price_with_vat = float(row['unit_price_with_vat_sum'])
                                                price_subtotal = float(row['price_subtotal_sum'])

                                                quantity += it.quantity
                                                unit_cost_profit_loss += unit_cost_profit_loss
                                                total_profit_loss += total_profit_loss
                                                five_vat += it.five_vat
                                                total_price_with_vat += it.total_price_with_vat
                                                # unit_price_with_vat += it.unit_price_with_vat if it.unit_price_with_vat > 0 else it.price_unit
                                                discount_per_unit += it.x_discount
                                                vat_on_unit_profit_loss += vat_on_unit_profit_loss
                                                unit_price += unit_price
                                                price_subtotal += it.price_subtotal

                                                row['quantity'] = "{0:.2f}".format(quantity)
                                                row['five_vat'] = "{0:,.2f}".format(five_vat)
                                                row['total_price_with_vat'] = "{0:,.2f}".format(total_price_with_vat)
                                                row['unit_price_with_vat'] = "{0:,.2f}".format(
                                                    (total_price_with_vat / quantity))
                                                row['amount'] = "{0:,.2f}".format(price_subtotal)

                                                row['quantity_sum'] = quantity
                                                row['five_vat_sum'] = five_vat
                                                row['total_price_with_vat_sum'] = total_price_with_vat
                                                row['unit_price_with_vat_sum'] = total_price_with_vat / quantity
                                                row['price_subtotal_sum'] = price_subtotal

                                    else:
                                        data_list.append(result)

                                if str(invoice.type) == 'out_refund':
                                    return_product_id_list.append(it.product_id.id)

                                else:
                                    product_id_list.append(it.product_id.id)

        return data_list

    def get_invoices_list(self, obj):

        try:
            order_numbers = [obj.split(',')[0]]
        except:
            order_numbers = [str(obj)]

        group_id = None
        data_list = []
        data_dict = dict()

        invoices = ''
        date = ''

        delivered_amount_without_tax = ''
        delivered_amount_tax = 0.00
        delivered_amount=''
        total_delivered=''
        refund_amount_without_tax =''
        refund_amount_tax=0.00
        refund_amount=''
        total_return=''
        rm_name = ''
        rm_mobile_numbers =''
        service_vat = ''
        shipping_charge = ''
        order_date = ''


        self.cr.execute("SELECT id FROM bill_collection_billing_process WHERE name=%s", (order_numbers))
        for item in self.cr.fetchall():
            group_id = [item[0]]

        if group_id is not None:

            order_data = self.pool.get('bill.collection.billing.process').browse(self.cr, self.uid, group_id)

            parent = order_data.order_id.partner_id

            order_date = order_data.order_id.date_order.split(" ")[0]

            rm_mobile_numbers = ""
            rm_name = ""
            for i in range(5):
                if len(parent.parent_id) == 1:
                    parent = parent.parent_id
                else:
                    rm_mobile_numbers = parent.user_id.partner_id.phone
                    rm_name = parent.user_id.partner_id.name
                    # company_id = parent.id
                    break

            inv_obj = self.pool.get('account.invoice')
            inv_list = inv_obj.search(self.cr, self.uid, [('name','=', str(order_data.mag_name))])

            invoices = ''
            date = ''

            total_amount = 0.00
            total_amount_tax = 0.00
            net_total = 0.00

            delivered_amount_without_tax = 0.00
            delivered_amount_tax = 0.00
            delivered_amount = 0.00

            total_delivered = str(order_data.delivered_amount)

            refund_amount_without_tax = 0.00
            refund_amount_tax = 0.00
            refund_amount = 0.00
            service_vat = 0.00
            shipping_charge = 0.00

            total_return = str(order_data.return_amount)

            for inv in inv_obj.browse(self.cr, self.uid, inv_list):
                if str(inv.state) != 'draft' and str(inv.state != 'cancel'):
                    invoices += str(inv.number) + ", " if inv.number else ''
                    date += str(inv.date_invoice) + ", " if inv.date_invoice else ''

                    if str(inv.state) != 'cancel' and str(inv.type) == 'out_refund':

                        refund_amount_without_tax += inv.amount_untaxed
                        refund_amount_tax += inv.amount_tax
                        refund_amount += inv.amount_total
                    else:
                        total_amount += inv.amount_untaxed
                        total_amount_tax += inv.amount_tax
                        net_total += inv.amount_total
                        delivered_amount_without_tax += inv.amount_untaxed
                        delivered_amount_tax += inv.amount_tax
                        delivered_amount += inv.amount_total

                    invoice_line_ids = inv.invoice_line

                    for items in invoice_line_ids:
                        if 'Shipping' in str(items.name):
                            shipping_charge +=  items.price_subtotal

                        if 'Shipping' not in str(items.name):

                            srv_vat = (items.five_vat * 100) / 5

                            service_vat += srv_vat

        in_word_amount = amount_to_text((float(delivered_amount) - float(refund_amount)), currency='Taka')

        in_word_amount = in_word_amount.replace("Cents", "Paisas") if 'Cents' in in_word_amount else in_word_amount.replace("Cent", "Paisa")
        total_amount_tax = delivered_amount_tax - refund_amount_tax
        return [{
            'order_date': order_date,
            'invoices': invoices,
            'date': date,
            'net_value': "{0:,.2f}".format((delivered_amount_without_tax - refund_amount_without_tax)),
            'total_net_value': "{0:,.2f}".format((delivered_amount - refund_amount)),
            'delivered_amount_without_tax': "{0:,.2f}".format(delivered_amount_without_tax),
            'delivered_amount_tax': "{0:,.2f}".format(delivered_amount_tax),
            'delivered_amount': "{0:,.2f}".format(delivered_amount),
            'total_delivered': "{0:,.2f}".format(float(total_delivered)),
            'refund_amount_without_tax': "{0:,.2f}".format(refund_amount_without_tax),
            'refund_amount_tax': "{0:,.2f}".format(refund_amount_tax),
            'total_amount_tax': "{0:,.2f}".format(total_amount_tax),
            'refund_amount':  "{0:,.2f}".format(refund_amount),
            'refund_amnt':  refund_amount,
            'total_return':  "{0:,.2f}".format(float(total_return)),
            'in_word_amount': in_word_amount,
            'rm_info': "RM Name : " + rm_name + " and Phone : " + rm_mobile_numbers if rm_name and rm_mobile_numbers else False,
            'service_vat': "{0:,.2f}".format(service_vat),
            'shipping_charge': "{0:,.2f}".format(shipping_charge),
        }]

class report_final_invoice_layout(osv.AbstractModel):
    _name = 'report.sales_collection_billing_process.billing_invoice_report'
    _inherit = 'report.abstract_report'
    _template = 'sales_collection_billing_process.billing_invoice_report'
    _wrapped_report_class = billcollectinvoicedata

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
