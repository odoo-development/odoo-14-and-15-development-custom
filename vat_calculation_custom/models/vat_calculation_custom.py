import datetime
from odoo import _,api, fields, models
from odoo.tools.safe_eval import safe_eval
from odoo.exceptions import UserError, ValidationError
from odoo.osv import expression
import logging
_logger = logging.getLogger(__name__)


class AccountMove(models.Model):
    _inherit = 'account.move'

    warehouse_id = fields.Many2one('stock.warehouse', default=1, string="Warehouse")
    sales_representative_name = fields.Char(string="Salesperson Name")
    sales_representative_email = fields.Char(string="Salesperson Email")
    sales_representative_mobile = fields.Char(string="Salesperson Phone")

    @api.model_create_multi
    def create(self, vals):
        stock_move_ids = None
        for val in vals:
            rec = dict(val)
            move_type = rec.get('move_type')
            total_vat = 0
            total_profit_los = 0
            total_unit_price_without_vat = 0
            stock_move_ids = []

            if move_type and move_type is not False and ('out_invoice' in move_type or 'out_refund' in move_type):
                tax_obj = self.env['account.tax']
                tax_data = tax_obj.search([('active','=',True),('type_tax_use','=','sale')])

                invoice_origin = rec.get('invoice_origin')

                if 'invoice_line_ids' in rec:

                    for line in rec.get('invoice_line_ids'):
                        unit_cost = 0
                        line = line[2]
                        product_query = "select id from product_product WHERE default_code='Delivery_007'"
                        self.env.cr.execute(product_query)
                        product_objects = self.env.cr.dictfetchall()

                        csc_product_query = "select id from product_product WHERE default_code='CSC_007'"
                        self.env.cr.execute(csc_product_query)
                        csc_product_objects = self.env.cr.dictfetchall()

                        discount_product_query = "select id from product_product WHERE default_code='DS_007'"
                        self.env.cr.execute(discount_product_query)
                        discount_product_objects = self.env.cr.dictfetchall()

                        total_quantity = line.get('quantity')
                        unit_so_price = line.get('price_unit')
                        line['price_unit_with_vat'] = line.get('price_unit')
                        line['price_subtotal_with_vat'] = unit_so_price * total_quantity
                        line['unit_cost'] = unit_cost

                        if line.get('product_id') and product_objects[0].get('id') != line.get('product_id') \
                                and csc_product_objects[0].get('id') != line.get('product_id') \
                                and discount_product_objects[0].get('id') != line.get('product_id') \
                                and 'Free' not in line.get('name'):

                            purchase_query = "select unit_cost as total from stock_valuation_layer where stock_move_id =" \
                                             "(SELECT stock_move.id FROM stock_move WHERE origin = %s " \
                                             "AND stock_move.product_id = %s AND stock_move.product_uom_qty=%s AND " \
                                             "stock_move.state = 'done' AND stock_move.invoiced=FALSE )"

                            self.env.cr.execute(purchase_query, (invoice_origin, line.get('product_id'),line.get('quantity')))

                            objects = self.env.cr.dictfetchall()

                            stock_move_query = "SELECT stock_move.id FROM stock_move WHERE origin = %s " \
                                             "AND stock_move.product_id = %s AND stock_move.product_uom_qty=%s AND " \
                                               "stock_move.state = 'done' AND stock_move.invoiced=FALSE"

                            self.env.cr.execute(stock_move_query,
                                                (invoice_origin, line.get('product_id'), line.get('quantity')))

                            for stock_move_id in self.env.cr.dictfetchall():
                                stock_move_ids.append(stock_move_id.get('id'))

                            for items in objects:
                                unit_cost =items.get('total')

                            if unit_cost == 0:
                                raise ValidationError(_("Cost Price=0. Please contact with Administrator"))

                            sale_query = "select x_discount from sale_order_line where order_id =" \
                                             "(SELECT id  FROM sale_order WHERE origin = %s " \
                                             ") AND sale_order_line.product_id = %s AND sale_order_line.product_uom_qty=%s"

                            self.env.cr.execute(sale_query,
                                                (invoice_origin, line.get('product_id'), line.get('quantity')))
                            sale_objects = self.env.cr.dictfetchall()
                            sale_discount = 0
                            if sale_objects:
                                for sale_items in sale_objects:
                                    sale_discount =sale_items.get('x_discount')

                            total_profit_loss =round(((unit_so_price * total_quantity) - (unit_cost * total_quantity)),2)
                            line['unit_cost'] = unit_cost
                            line['x_discount'] = sale_discount
                            line['x_total_discount'] = sale_discount * total_quantity

                            if total_profit_loss > 0:
                                per_unit_vat = round(((unit_so_price - unit_cost) * 0.05),2)
                                unit_price_without_vat = unit_so_price - per_unit_vat
                                price_without_vat = unit_price_without_vat * total_quantity
                                five_vat = round((per_unit_vat * total_quantity),2)

                                line['unit_cost_profit_loss'] = round((unit_so_price - unit_cost),2)
                                line['total_profit_loss'] = round(total_profit_loss,2)
                                line['five_vat'] = five_vat
                                line['vat_on_unit_profit_loss'] = round(per_unit_vat,2)
                                line['price_unit_with_vat'] = unit_so_price
                                line['price_subtotal_with_vat'] = unit_so_price * total_quantity
                                line['price_unit'] = unit_price_without_vat
                                line['credit'] = unit_price_without_vat * total_quantity
                                line['tax_ids'] = [(6, 0, [tax_data.id])]
                                total_vat = total_vat + five_vat
                                total_profit_los = total_profit_los + total_profit_loss
                                total_unit_price_without_vat = total_unit_price_without_vat + price_without_vat
                        # else:
                        #     if total_profit_los > 0:
                        #         line['debit'] = total_unit_price_without_vat

        result = super(AccountMove, self).create(vals)

        if result.move_type and result.move_type is not False and ('out_invoice' in result.move_type or 'out_refund' in result.move_type):
            if stock_move_ids and 'out_invoice' in result.move_type:

                self.env.cr.execute('update stock_move set invoiced=TRUE where id in %s',[tuple(stock_move_ids)])

            order_query = "select warehouse_id, sales_representative_name,sales_representative_email," \
                          "sales_representative_mobile,x_delivery_slot,x_expected_delivery_date,x_delivery_comments" \
                          " from sale_order where name =%s"
            self.env.cr.execute(order_query,(result.invoice_origin,))
            sale_info_objects = self.env.cr.dictfetchall()

            for sale_info in sale_info_objects:
                result.warehouse_id = sale_info.get('warehouse_id') if sale_info.get('warehouse_id') else 1
                result.sales_representative_name = sale_info.get('sales_representative_name') if sale_info.get('sales_representative_name') else ''
                result.sales_representative_email = sale_info.get('sales_representative_email') if sale_info.get('sales_representative_email') else ''
                result.sales_representative_mobile = sale_info.get('sales_representative_mobile') if sale_info.get('sales_representative_mobile') else ''

                result.x_delivery_slot = sale_info.get('x_delivery_slot') if sale_info.get('x_delivery_slot') else ''
                result.x_expected_delivery_date = sale_info.get('x_expected_delivery_date') if sale_info.get(
                    'x_expected_delivery_date') else ''
                result.x_delivery_comments = sale_info.get('x_delivery_comments') if sale_info.get(
                    'x_delivery_comments') else ''

        return result


    def _recompute_tax_lines(self, recompute_tax_base_amount=False):
        ''' Compute the dynamic tax lines of the journal entry.

        :param lines_map: The line_ids dispatched by type containing:
            * base_lines: The lines having a tax_ids set.
            * tax_lines: The lines having a tax_line_id set.
            * terms_lines: The lines generated by the payment terms of the invoice.
            * rounding_lines: The cash rounding lines of the invoice.
        '''
        super(AccountMove, self)._recompute_tax_lines(recompute_tax_base_amount)
        self.ensure_one()
        in_draft_mode = self != self._origin

        def _serialize_tax_grouping_key(grouping_dict):
            ''' Serialize the dictionary values to be used in the taxes_map.
            :param grouping_dict: The values returned by '_get_tax_grouping_key_from_tax_line' or '_get_tax_grouping_key_from_base_line'.
            :return: A string representing the values.
            '''
            return '-'.join(str(v) for v in grouping_dict.values())

        def _compute_base_line_taxes(base_line):
            ''' Compute taxes amounts both in company currency / foreign currency as the ratio between
            amount_currency & balance could not be the same as the expected currency rate.
            The 'amount_currency' value will be set on compute_all(...)['taxes'] in multi-currency.
            :param base_line:   The account.move.line owning the taxes.
            :return:            The result of the compute_all method.
            '''
            move = base_line.move_id

            if move.is_invoice(include_receipts=True):
                handle_price_include = True
                sign = -1 if move.is_inbound() else 1
                quantity = base_line.quantity
                is_refund = move.move_type in ('out_refund', 'in_refund')
                # price_unit_wo_discount = sign * base_line.price_unit * (1 - (base_line.discount / 100.0))
                price_unit_wo_discount = sign * base_line.five_vat
            else:
                handle_price_include = False
                quantity = 1.0
                tax_type = base_line.tax_ids[0].type_tax_use if base_line.tax_ids else None
                is_refund = (tax_type == 'sale' and base_line.debit) or (tax_type == 'purchase' and base_line.credit)
                price_unit_wo_discount = base_line.amount_currency
                if tax_type == 'sale':
                    price_unit_wo_discount = base_line.five_vat

            balance_taxes_res = base_line.tax_ids._origin.with_context(force_sign=move._get_tax_force_sign()).compute_all(
                price_unit_wo_discount,
                currency=base_line.currency_id,
                quantity=quantity,
                product=base_line.product_id,
                partner=base_line.partner_id,
                is_refund=is_refund,
                handle_price_include=handle_price_include,
            )

            if move.move_type == 'entry':
                repartition_field = is_refund and 'refund_repartition_line_ids' or 'invoice_repartition_line_ids'
                repartition_tags = base_line.tax_ids.flatten_taxes_hierarchy().mapped(repartition_field).filtered(lambda x: x.repartition_type == 'base').tag_ids
                tags_need_inversion = (tax_type == 'sale' and not is_refund) or (tax_type == 'purchase' and is_refund)
                if tags_need_inversion:
                    balance_taxes_res['base_tags'] = base_line._revert_signed_tags(repartition_tags).ids
                    for tax_res in balance_taxes_res['taxes']:
                        tax_res['tag_ids'] = base_line._revert_signed_tags(self.env['account.account.tag'].browse(tax_res['tag_ids'])).ids

            return balance_taxes_res

        taxes_map = {}

        # ==== Add tax lines ====
        to_remove = self.env['account.move.line']
        for line in self.line_ids.filtered('tax_repartition_line_id'):
            grouping_dict = self._get_tax_grouping_key_from_tax_line(line)
            grouping_key = _serialize_tax_grouping_key(grouping_dict)
            if grouping_key in taxes_map:
                # A line with the same key does already exist, we only need one
                # to modify it; we have to drop this one.
                to_remove += line
            else:
                taxes_map[grouping_key] = {
                    'tax_line': line,
                    'amount': 0.0,
                    'tax_base_amount': 0.0,
                    'grouping_dict': False,
                }
        if not recompute_tax_base_amount:
            self.line_ids -= to_remove

        # ==== Mount base lines ====
        for line in self.line_ids.filtered(lambda line: not line.tax_repartition_line_id):
            # Don't call compute_all if there is no tax.
            if not line.tax_ids:
                if not recompute_tax_base_amount:
                    line.tax_tag_ids = [(5, 0, 0)]
                continue

            compute_all_vals = _compute_base_line_taxes(line)

            # Assign tags on base line
            if not recompute_tax_base_amount:
                line.tax_tag_ids = compute_all_vals['base_tags'] or [(5, 0, 0)]

            tax_exigible = True
            for tax_vals in compute_all_vals['taxes']:
                grouping_dict = self._get_tax_grouping_key_from_base_line(line, tax_vals)
                grouping_key = _serialize_tax_grouping_key(grouping_dict)

                tax_repartition_line = self.env['account.tax.repartition.line'].browse(tax_vals['tax_repartition_line_id'])
                tax = tax_repartition_line.invoice_tax_id or tax_repartition_line.refund_tax_id

                if tax.tax_exigibility == 'on_payment':
                    tax_exigible = False

                taxes_map_entry = taxes_map.setdefault(grouping_key, {
                    'tax_line': None,
                    'amount': 0.0,
                    'tax_base_amount': 0.0,
                    'grouping_dict': False,
                })
                taxes_map_entry['amount'] += tax_vals['amount']
                taxes_map_entry['tax_base_amount'] += self._get_base_amount_to_display(tax_vals['base'], tax_repartition_line, tax_vals['group'])
                taxes_map_entry['grouping_dict'] = grouping_dict
            if not recompute_tax_base_amount:
                line.tax_exigible = tax_exigible

        # ==== Process taxes_map ====
        for taxes_map_entry in taxes_map.values():
            # The tax line is no longer used in any base lines, drop it.
            if taxes_map_entry['tax_line'] and not taxes_map_entry['grouping_dict']:
                if not recompute_tax_base_amount:
                    self.line_ids -= taxes_map_entry['tax_line']
                continue

            currency = self.env['res.currency'].browse(taxes_map_entry['grouping_dict']['currency_id'])

            # Don't create tax lines with zero balance.
            if currency.is_zero(taxes_map_entry['amount']):
                if taxes_map_entry['tax_line'] and not recompute_tax_base_amount:
                    self.line_ids -= taxes_map_entry['tax_line']
                continue

            # tax_base_amount field is expressed using the company currency.
            tax_base_amount = currency._convert(taxes_map_entry['tax_base_amount'], self.company_currency_id, self.company_id, self.date or fields.Date.context_today(self))

            # Recompute only the tax_base_amount.
            if recompute_tax_base_amount:
                if taxes_map_entry['tax_line']:
                    taxes_map_entry['tax_line'].tax_base_amount = tax_base_amount
                continue

            balance = currency._convert(
                taxes_map_entry['amount'],
                self.company_currency_id,
                self.company_id,
                self.date or fields.Date.context_today(self),
            )
            to_write_on_line = {
                'amount_currency': taxes_map_entry['amount'],
                'currency_id': taxes_map_entry['grouping_dict']['currency_id'],
                'debit': balance > 0.0 and balance or 0.0,
                'credit': balance < 0.0 and -balance or 0.0,
                'tax_base_amount': tax_base_amount,
            }

            if taxes_map_entry['tax_line']:
                # Update an existing tax line.
                taxes_map_entry['tax_line'].update(to_write_on_line)
            else:
                create_method = in_draft_mode and self.env['account.move.line'].new or self.env['account.move.line'].create
                tax_repartition_line_id = taxes_map_entry['grouping_dict']['tax_repartition_line_id']
                tax_repartition_line = self.env['account.tax.repartition.line'].browse(tax_repartition_line_id)
                tax = tax_repartition_line.invoice_tax_id or tax_repartition_line.refund_tax_id
                taxes_map_entry['tax_line'] = create_method({
                    **to_write_on_line,
                    'name': tax.name,
                    'move_id': self.id,
                    'partner_id': line.partner_id.id,
                    'company_id': line.company_id.id,
                    'company_currency_id': line.company_currency_id.id,
                    'tax_base_amount': tax_base_amount,
                    'exclude_from_invoice_tab': True,
                    'tax_exigible': tax.tax_exigibility == 'on_invoice',
                    **taxes_map_entry['grouping_dict'],
                })

            if in_draft_mode:
                taxes_map_entry['tax_line'].update(taxes_map_entry['tax_line']._get_fields_onchange_balance(force_computation=True))



class AccountMoveLine(models.Model):
    _inherit = 'account.move.line'

    unit_cost = fields.Float(string='Unit Cost')
    unit_cost_profit_loss = fields.Float(string='Cost P/L')
    vat_on_unit_profit_loss = fields.Float(string='VAT Per Unit')
    total_profit_loss = fields.Float(string='Total P/L')
    five_vat = fields.Float(string='Total VAT')
    unit_price_without_vat = fields.Float(string='Price without VAT')
    price_unit_with_vat = fields.Float(string='Price Unit with VAT')
    price_subtotal_with_vat = fields.Float(string='Total Amount With VAT')
    # total_price_with_vat = fields.function(_total_price_with_vat, type='float', string='Total Price With VAT')
    x_discount = fields.Float(string="Discount")
    x_total_discount = fields.Float(string="Total Discount")

    @api.model_create_multi
    def create(self, vals):

        result = super(AccountMoveLine, self).create(vals)
        total_vat = 0
        total_unit_price_without_vat = 0
        for rec in result:
            # rec = dict(val)
            unit_cost = rec.unit_cost
            if unit_cost is not False and unit_cost:

                if rec.product_id.id:

                    if rec.five_vat > 0:
                        total_vat = total_vat + rec.five_vat
                        total_unit_price_without_vat = total_unit_price_without_vat + (
                        rec.unit_price_without_vat * rec.quantity)
                        rec.price_total = rec.price_subtotal

            account_id = rec.account_id.id if rec.account_id.id else rec.account_id

            if total_vat > 0 and account_id == 16:
                rec.credit = rec.credit
                rec.debit = rec.debit
                rec.price_unit = total_vat
                rec.price_subtotal = total_vat
                rec.price_total = total_vat
                rec.tax_base_amount = total_unit_price_without_vat

        return result

    @api.onchange('quantity')
    def _onchange_vat(self):
        self.five_vat = round((self.quantity * self.vat_on_unit_profit_loss),2)
        self.price_subtotal_with_vat = self.price_subtotal + self.five_vat
        self.total_profit_loss = round((self.quantity * self.unit_cost_profit_loss),2)
        self.x_total_discount = round((self.quantity * self.x_discount),2)


class StockMove(models.Model):
    _inherit = 'stock.move'

    invoiced = fields.Boolean(default=False,string="Invoiced")


