{
    'name': 'Custom Vat calculation with profit/loss',
    'version': '1.0',
    'category': 'VAT',
    'author': 'Sindabad',
    'summary': 'New Custom Vat calculation with profit/loss',
    'website': 'https://www.sindabad.com',
    'description': "Vat Calculation of Invoices",
    'depends': [
        'account'
    ],
    'data': [
        # 'report/billing_invoice_report_custom.xml',
        # 'report/final_invoice_report_custom.xml',
        # 'report/invoice_report_custom.xml',
        # 'report/report_vatinvoice_custom.xml',
        'report/mushak_challan_report.xml',

        'views/mushak_challan_menu.xml',

        'views/vat_calculation_custom.xml',
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}
