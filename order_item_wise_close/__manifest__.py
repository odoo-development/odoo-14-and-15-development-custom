{
    'name': 'Order item wise close',
    'version': "14.0.1.0.0",
    'category': 'Sales',
    'description': "Order item wise close",
    'author': 'Odoo Bangladesh',
    'depends': ['odoo_magento_connect', 'sale_management', 'purchase', 'account', 'stock', 'stock_account','send_sms_on_demand'],
    # 'depends': ['odoo_magento_connect','sale_management', 'purchase', 'account', 'stock', 'stock_account','sales_collection_billing_process','send_sms_on_demand'],
    'data': [
        'security/order_item_wise_clos_Security.xml',
        'security/ir.model.access.csv',
        'wizard/order_close.xml',
        'views/order_item_wise_close_view.xml',
        'views/sale_view.xml',
        'views/purchase_view.xml',
    ],
    'installable': True,
    'auto_install': False,
}
