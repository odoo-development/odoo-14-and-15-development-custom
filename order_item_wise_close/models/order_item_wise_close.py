import json
from odoo.exceptions import Warning
import requests
from odoo import api, fields, models, _
from odoo.http import request
from odoo.osv import osv
from ...odoo_to_magento_api_connect.api_connect import get_magento_token


# from ..odoo_to_magento_api_connect.api_connect import get_magento_token


class SaleOrder(models.Model):
    _inherit = "sale.order"

    # Received amount
    def _get_canceled_amount(self):

        for sale_obj in self:
            total_sum = 0.00

            # init_data = stock_move_obj.search([('origin', '=', sale_obj.name)])

            # for so_move in stock_move_obj.browse(init_data):
            for order in sale_obj.order_line:

                if order.closed_quantity > float(0):
                    total_sum = total_sum + (order.closed_quantity * order.price_unit)

            sale_obj.canceled_amount = total_sum

    canceled_amount = fields.Float(compute='_get_canceled_amount', string='Cancelled Amount')
    # order_close_reason = fields.Char('Order Close Reason', required=True)
    order_close_reason = fields.Char('Order Close Reason')
    sale_order_ids = fields.One2many('order.item.wise.close.line', 'sale_order_id', 'Order Lines')

    # ## Block code for V-14
    # _columns = {
    #     'canceled_amount': fields.function(_get_canceled_amount, string='Cancelled Amount', type='float'),
    #     'order_close_reason': fields.char('Order Close Reason', required=True),
    #     'sale_order_ids': fields.one2many('order.item.wise.close.line', 'sale_order_id', 'Order Lines')
    # }

    def manual_so_status_sync(self):
        ids = self.env.context['active_ids']
        self.env['order.item.wise.close'].manual_order_status_sync(ids)
        return True


class SaleOrderLine(models.Model):
    _inherit = "sale.order.line"

    closed_quantity = fields.Float('closed_quantity')

    # _columns = {
    #     'closed_quantity': fields.float('closed_quantity')
    # }


class PurchaseOrder(models.Model):
    _inherit = "purchase.order"

    # Received amount
    received_amount = fields.Float(compute='_get_received_amount', string='Received Amount')
    # order_close_reason = fields.Char('Purchase Order Close Reason', required=True)
    order_close_reason = fields.Char('Purchase Order Close Reason')

    # ## Block code for V-14
    # _columns = {
    #     'received_amount': fields.function(_get_received_amount, string='Received Amount', type='float'),
    #     'order_close_reason': fields.char('Purchase Order Close Reason', required=True),
    # }

    def _get_received_amount(self):

        for po in self:
            total_sum = 0.00
            for invoice in po.invoice_ids:
                if invoice.move_type == 'in_invoice' and invoice.state != 'cancel':
                    total_sum = total_sum + invoice.amount_total
                if invoice.move_type == 'in_refund' and invoice.state != 'cancel':
                    total_sum = total_sum - invoice.amount_total
            # res[po.id] = total_sum
            po.received_amount = total_sum

            # return res


class PurchaseOrderLine(models.Model):
    _inherit = "purchase.order.line"

    closed_quantity = fields.Float('closed_quantity')

    # _columns = {
    #     'closed_quantity': fields.float('closed_quantity')
    # }


class OrderItemWiseClose(models.TransientModel):
    _name = "order.item.wise.close"
    _description = "Order Item Wise Close"

    so_close_reason = fields.Selection([
        ("product_not_available", "Product Not Available"),
        ("product_not_available_customer_will_take_alternative_product",
         "Product not available, Customer will take alternative product"),
        ("partially_delivered", "Partially delivered"),
        ("reordered", "Reordered#10000"),
        ("payment_issue", "Payment issue"),
        ("cash_problem", "Cash problem"),
        ("damage_product", "Damage product"),
        ("double_ordered", "Double ordered"),
        ("sample_not_approved", "Sample not approved"),
        ("wrong_sku", "Wrong SKU"),
        ("not_interested", "Not Interested"),
        ("wrong_product", "Wrong Product"),
        ("quality_issue", "Quality issue"),
        ("delay_delivery", "Delay delivery"),
        ("sourcing_delay", "Sourcing Delay"),
        ("not_for_retail", "Not for Retail")
    ], 'Sale Order Close Reason', copy=False, help="Reasons", index=True)
    po_close_reason = fields.Char('Purchase Order Close Reason')
    order_name = fields.Char('Order Name')
    order_ref = fields.Char('Order Ref.')
    order_id = fields.Many2one('sale.order', 'Sale Order')
    purchase_order_id = fields.Many2one('purchase.order', 'Purchase Order')
    order_type = fields.Char('Type')
    sale = fields.Boolean('Sale')
    sync_done = fields.Boolean('Sync Done')
    purchase = fields.Boolean('Purchase')
    item_line_ids = fields.One2many("order.item.wise.close.line", "close_order_id", 'Order Line', required=True)

    # order_item_line = fields.One2many('order.item.wise.close.line', 'close_order_id', 'Order Line', required=True)

    # ## Block code for v-14
    # _columns = {
    #     'so_close_reason': fields.selection([
    #         ("product_not_available", "Product Not Available"),
    #         ("product_not_available_customer_will_take_alternative_product",
    #          "Product not available, Customer will take alternative product"),
    #         ("partially_delivered", "Partially delivered"),
    #         ("reordered", "Reordered#10000"),
    #         ("payment_issue", "Payment issue"),
    #         ("cash_problem", "Cash problem"),
    #         ("damage_product", "Damage product"),
    #         ("double_ordered", "Double ordered"),
    #         ("sample_not_approved", "Sample not approved"),
    #         ("wrong_sku", "Wrong SKU"),
    #         ("not_interested", "Not Interested"),
    #         ("wrong_product", "Wrong Product"),
    #         ("quality_issue", "Quality issue"),
    #         ("delay_delivery", "Delay delivery"),
    #         ("sourcing_delay", "Sourcing Delay")
    #     ], 'Sale Order Close Reason', copy=False, help="Reasons", index=True),
    #     'po_close_reason': fields.char('Purchase Order Close Reason'),
    #     'order_name': fields.char('Order Name'),
    #     'order_ref': fields.char('Order Ref.'),
    #     'order_id': fields.many2one('sale.order', 'Sale Order'),
    #     'purchase_order_id': fields.many2one('purchase.order', 'Purchase Order'),
    #     'order_type': fields.char('Type'),
    #     'sale': fields.boolean('Sale'),
    #     'sync_done': fields.boolean('Sync Done'),
    #     'purchase': fields.boolean('Purchase'),
    #     'order_item_line': fields.one2many('order.item.wise.close.line', 'close_order_id', 'Order Line',
    #                                        required=True),
    # }

    @api.model
    def create(self, vals):
        # context = dict(self.env.context or {})

        found_zero_qty = False
        order_id = vals.get('order_id')

        order_obj = self.env['sale.order'].browse([order_id])

        for items in vals.get("item_line_ids"):
            try:
                if items[2].get('quantity') == 0:
                    found_zero_qty = True
            except:
                pass

        if found_zero_qty == True:
            raise Warning(_('You can not close due to Product quantity is set Zero (0). Please Remove it.'))

        item_line_id = super(OrderItemWiseClose, self).create(vals)

        return item_line_id

    def default_get(self, fields):

        # context = dict(self.env.context)
        res = super(OrderItemWiseClose, self).default_get(fields)

        order_obj = None
        purchase_order_id = None
        sale_order_id = None
        if self.env.context['sale'] == True:
            order_obj = self.env['sale.order'].browse(self.env.context['active_id'])
            res['order_type'] = 'sale'
            res['sale'] = True
            res['order_ref'] = order_obj.client_order_ref
            res['order_id'] = self.env.context['active_id']
            order_item_wise_close_query = "SELECT id FROM order_item_wise_close WHERE order_id={0} ".format(
                self.env.context['active_id'])
            self.env.cr.execute(order_item_wise_close_query)

        elif self.env.context['purchase'] == True:
            order_obj = self.env['purchase.order'].browse(self.env.context['active_id'])
            res['order_type'] = 'purchase'
            res['purchase'] = True
            # res['order_ref'] = order_obj.client_order_ref
            res['purchase_order_id'] = self.env.context['active_id']

            order_item_wise_close_query = "SELECT id FROM order_item_wise_close WHERE purchase_order_id={0} ".format(
                self.env.context['active_id'])
            self.env.cr.execute(order_item_wise_close_query)
        else:
            pass

        items = []
        close_item_qty = []

        close_ids = [item_wise_close[0] for item_wise_close in self.env.cr.fetchall()]

        if close_ids:
            close_item_line_query = "SELECT product_id,sum(quantity) FROM order_item_wise_close_line WHERE close_order_id in %s GROUP BY product_id "
            self.env.cr.execute(close_item_line_query, (tuple(close_ids),))

            close_item_qty = [{sp_id[0]: sp_id[1]} for sp_id in self.env.cr.fetchall()]

        for lines in order_obj.order_line:
            if lines.product_id.type != 'service':
                delivered_qty = 0.00
                append_item = True
                if self.env.context['purchase'] == True:
                    lines.product_uom_qty = lines.product_qty
                    purchase_order_id = int(self.env.context['active_id'])

                if self.env.context['sale'] == True:
                    lines.product_uom_qty = lines.product_uom_qty
                    sale_order_id = int(self.env.context['active_id'])

                moves = lines.move_ids

                for move_ids in moves:
                    if move_ids.state == 'done' and move_ids.product_id == lines.product_id and move_ids.product_qty != lines.product_uom_qty:
                        delivered_qty = delivered_qty + move_ids.product_qty

                    elif move_ids.state == 'done' and move_ids.product_id == lines.product_id and move_ids.product_qty == lines.product_uom_qty:
                        append_item = False

                if lines.product_id.type == 'service':
                    append_item = False

                quantity = float(lines.product_uom_qty - delivered_qty)

                if close_item_qty:
                    for item_qty in close_item_qty:
                        if item_qty.get(lines.product_id.id) and quantity - item_qty.get(lines.product_id.id) == 0.00:
                            append_item = False
                        elif item_qty.get(lines.product_id.id) and quantity - item_qty.get(lines.product_id.id) > 0.00:
                            quantity = quantity - item_qty[lines.product_id.id]
                            append_item = True

                item = {

                    'order_id': int(self.env.context['active_id']),
                    'sale_order_id': sale_order_id,
                    'purchase_order_id': purchase_order_id,
                    'product_id': int(lines.product_id.id),
                    'quantity': float(quantity),
                    'order_quantity': float(quantity)
                }
                if append_item:
                    items.append((0, 0, item))

        res['order_name'] = str(order_obj.name)
        res.update(item_line_ids=items)

        return res

    def item_wise_close(self):
        get_data = self.read()[0]

        order_obj = None

        so_obj = self.env['sale.order']
        po_obj = self.env['purchase.order']

        if get_data['sale'] == True:
            # if self.sale == True:
            get_data['order_id'] = get_data['order_id'][0]
            order_obj = so_obj.browse(get_data['order_id'])[0]

            if get_data['so_close_reason'] is False:
                raise Warning(_('Please give the Order Close Reason!!!'))

        if get_data['purchase'] == True:
            get_data['purchase_order_id'] = get_data['purchase_order_id'][0]
            order_obj = po_obj.browse(get_data['purchase_order_id'])[0]

            if get_data['po_close_reason'] == ' ' or get_data['po_close_reason'] is False or get_data[
                'po_close_reason'] is None:
                raise Warning(_('Please give the Order Close Reason!!!'))

        stock_picking_query = "SELECT id FROM stock_picking WHERE origin='{0}' and state!='done'".format(
            str(get_data['order_name']))
        self.env.cr.execute(stock_picking_query)

        for sp_id in self.env.cr.fetchall():
            stock_picking_id = int(sp_id[0])

            # unreserve

            picking = [sp_id]
            stock_obj = self.env['stock.picking'].browse(sp_id)
            stock_obj.do_unreserve()

            if stock_obj.move_lines:
                for prod in stock_obj.move_lines:

                    for item_lines in self.item_line_ids:

                        if prod.product_id.id == item_lines.product_id.id and prod.product_qty == item_lines.quantity:

                            prod.write({'product_uom_qty': 0.00})

                        elif prod.product_id.id == item_lines.product_id.id and prod.product_qty != item_lines.quantity:

                            prod.write({
                                'product_uom_qty': item_lines.order_quantity - item_lines.quantity})

                for item_line in self.item_line_ids:
                    for line in order_obj.order_line:
                        if get_data['sale'] == True:
                            line.product_uom_qty = line.product_uom_qty

                        if get_data['purchase'] == True:
                            line.product_uom_qty = line.product_qty

                        if line.state != 'cancel' and line.product_id.id == item_line.product_id.id and line.product_uom_qty == item_line.quantity:
                            if get_data['sale'] == True:
                                line.write({'state': 'done',
                                            'closed_quantity': item_line.quantity})
                            if get_data['purchase'] == True:
                                line.write({'state': 'done',
                                            'closed_quantity': item_line.quantity}, )

                        elif line.state != 'cancel' and line.product_id.id == item_line.product_id.id and line.product_uom_qty != item_line.quantity:
                            if get_data['sale'] == True:
                                line.write({'closed_quantity': item_line.quantity + line.closed_quantity})

                            if get_data['purchase'] == True:
                                line.write({'closed_quantity': item_line.quantity + line.closed_quantity})

                if stock_obj.state == 'draft' or stock_obj.state == 'confirmed':
                    # unreserve
                    stock_obj.do_unreserve()
                    # self.env['stock.picking'].do_unreserve([sp_id[0]])

                    # action cancel
                    stock_obj.action_assign()
                # self.env['stock.picking'].action_assign([sp_id[0]])

                if stock_obj.state == 'waiting':
                    # Rereservation
                    stock_obj.rereserve_pick()
                    # self.env['stock.picking'].rereserve_pick([sp_id[0]])

                non_zero_moves_id_list = [mv.id for mv in stock_obj.move_lines if mv.product_qty != 0.00]

                if not non_zero_moves_id_list:
                    if prod.product_qty == 0.00:
                        prod.write({'state': 'done'})
                        # self.env['stock.move'].write([moves.id for moves in stock_obj.move_lines if
                        #                               moves.product_qty == 0.00], {'state': 'done'})

        stock_query = "SELECT state FROM stock_picking WHERE origin='{0}'".format(str(get_data['order_name']))
        self.env.cr.execute(stock_query)
        stock_picking_status = [stock[0] for stock in self.env.cr.fetchall()]

        state_count = 0
        if 'draft' in stock_picking_status:
            state_count += 1
        elif 'waiting' in stock_picking_status:
            state_count += 1
        elif 'confirmed' in stock_picking_status:
            state_count += 1
        elif 'assigned' in stock_picking_status:
            state_count += 1

        if state_count == 0:
            if get_data['sale'] == True:
                so = so_obj.browse(get_data['order_id'])
                so.action_done()
                so.write({'shipped': True,
                          'order_close_reason': get_data['so_close_reason']})

                # so_state_query = "UPDATE sale_order SET shipped=TRUE,order_close_reason='{0}' WHERE id={1}".format(
                #     get_data['so_close_reason'], get_data['order_id'])
                #
                # self.env.cr.execute(so_state_query)
                # self.env.cr.commit()

        if get_data['purchase'] == True:
            po = po_obj.browse(get_data['purchase_order_id'])
            po.button_done()
            po.write({'is_shipped': True,
                      'order_close_reason': get_data['po_close_reason']})


        # # for sms
        # if get_data['sale'] == True:
        #     try:
        #         so_data = so_obj.browse(get_data['order_id'])
        #         close_data = self.browse().item_line_ids
        #         close_product_name = ''
        #         close_product_list = []
        #         count = 1
        #         for close_product in close_data:
        #             if close_product.order_quantity == close_product.quantity:
        #                 close_product_name += str(count) + ". " + str(
        #                     close_product.product_id.name) + ", " if close_product.product_id else ''
        #                 close_product_list.append(close_product)
        #                 count += 1
        #
        #         parent = so_data.partner_id
        #
        #         for i in range(5):
        #             if len(parent.parent_id) == 1:
        #                 parent = parent.parent_id
        #             else:
        #                 parent = parent
        #
        #                 break
        #
        #         company_phone = str(parent.mobile) if parent.mobile else parent.phone
        #         phone_number = str(so_data.partner_id.mobile) if so_data.partner_id.mobile else company_phone
        #
        #         if not phone_number:
        #             phone_number = str(so_data.partner_id.phone)
        #         if phone_number and '+88' not in phone_number:
        #             phone_number = '+88' + phone_number
        #
        #         if close_product_list:
        #             if phone_number:
        #                 sms_text = "Dear Customer,\nOrder Number {0}.\nYour order product(s) '{1}' has been canceled due to unavailability ".format(
        #                     str(so_data.client_order_ref), close_product_name)
        #
        #                 name = "order.item.wise.close"
        #
        #                 self.env["send.sms.on.demand"].send_sms_on_demand(sms_text, phone_number)
        #     except:
        #         pass

        ### command by Keya
        ##### need to work
        #####  "get_magento_token" method missing, "cron_script" module acha
        ## for magento sync
        if get_data['sale'] == True:
            try:
                token, url = get_magento_token(self, [get_data['order_id']])
                if token:
                    token = token.replace('"', "")
                    so = so_obj.browse([get_data['order_id']])
                    cancle_prds = dict()

                    for line in so.order_line:
                        if line.closed_quantity > 0 and line.product_id.default_code:
                            cancle_prds[str(line.product_id.default_code).replace('"', '\"')] = line.closed_quantity

                    if cancle_prds:
                        # return_map_product_data = {'orderId': increment_id, 'itemData': return_itemData}
                        return_map_product_data = {'orderId': str(so.client_order_ref), 'itemData': cancle_prds}
                        map_option_return_url = url + "/index.php/rest/V1/odoomagentoconnect/OrderItemCancel"

                        cancelItem = dict()
                        cancelItem['cancelItem'] = return_map_product_data
                        cc = json.dumps(cancelItem)

                        userAgent = request.httprequest.environ.get('HTTP_USER_AGENT', '')
                        headers = {'Authorization': token,
                                   'Content-Type': 'application/json', 'User-Agent': userAgent}

                        try:
                            shipment = requests.post(
                                map_option_return_url, data=cc, headers=headers, verify=False)
                            mage_shipment = json.loads(shipment.text)
                            sync_done_query = "UPDATE order_item_wise_close SET sync_done=TRUE WHERE id={0}".format(
                                self.ids[0])

                            self.env.cr.execute(sync_done_query)
                            self.env.cr.commit()
                        except:
                            pass

                    status_text = 'processing'

                    if so.partially_invoiced_dispatch == True or so.partial_delivered == True:
                        status_text = 'partially_delivered'

                    if so.state == 'done':
                        status_text = 'delivered'

                    # if so.invoice_ids:
                    #
                    #     invoices = [inv.state for inv in so.invoice_ids if
                    #                 inv.type == 'out_invoice' and inv.state != 'cancel']
                    #     invoices_state = [inv_state for inv_state in invoices if inv_state != 'paid']
                    #     if not invoices_state:
                    #         status_text = 'complete'

                    # if so.invoice_ids:
                    #     refund_invoices = [inv.state for inv in so.invoice_ids if
                    #                 inv.type == 'out_refund' and inv.state != 'cancel']
                    # if int(so.invoiced_amount)  int(so.amount_total) and int(so.invoiced_amount) > 0:
                    #     status_text == 'delivered'
                    invoiced_amount = int(so.invoiced_amount) + int(so.canceled_amount)
                    if int(invoiced_amount) == int(so.amount_total) and int(invoiced_amount) > 0:
                        status_text = 'point_gain'

                    try:
                        if status_text == 'delivered' or status_text == 'partially_delivered' or status_text == 'complete' or status_text == 'point_gain':
                            self.order_wise_status_sync(token, url, status_text, so.client_order_ref)
                    except:
                        pass
            except:
                pass

        return True

    def order_wise_status_sync(self, token, url_root, status_text, order_number):
        try:
            url = url_root + "/index.php/rest/V1/odoomagentoconnect/OrderStatus"
            map_order_data = {'orderId': str(order_number), 'status': status_text}
            statusData = dict()
            statusData['statusData'] = map_order_data
            data = json.dumps(statusData)

            userAgent = request.httprequest.environ.get('HTTP_USER_AGENT', '')
            headers = {'Authorization': token,
                       'Content-Type': 'application/json', 'User-Agent': userAgent}

            res = requests.post(
                url, data=data, headers=headers, verify=False)

        except:
            pass

        return True

    def item_cancel_sync(self):

        close_data = self.browse()
        if close_data.sale == True:
            order_id = close_data.order_id.id

          ### command by Keya
        ##### need to work
        #####  "get_magento_token" method missing, "cron_script" module acha
            # try:
            #     token, url = get_magento_token(self, [order_id])
            #     if token:
            #         token = token.replace('"', "")
            #         so = self.env['sale.order'].browse([order_id])
            #
            #         cancle_prds = dict()
            #
            #         for line in so.order_line:
            #             if line.closed_quantity > 0 and line.product_id.default_code:
            #                 cancle_prds[str(line.product_id.default_code).replace('"', '\"')] = line.closed_quantity
            #
            #         if cancle_prds:
            #             # return_map_product_data = {'orderId': increment_id, 'itemData': return_itemData}
            #             return_map_product_data = {'orderId': str(so.client_order_ref), 'itemData': cancle_prds}
            #             map_option_return_url = url + "/index.php/rest/V1/odoomagentoconnect/OrderItemCancel"
            #
            #             cancelItem = dict()
            #             cancelItem['cancelItem'] = return_map_product_data
            #             cc = json.dumps(cancelItem)
            #
            #             userAgent = request.httprequest.environ.get('HTTP_USER_AGENT', '')
            #             headers = {'Authorization': token,
            #                        'Content-Type': 'application/json', 'User-Agent': userAgent}
            #
            #             try:
            #                 shipment = requests.post(
            #                     map_option_return_url, data=cc, headers=headers, verify=False)
            #                 mage_shipment = json.loads(shipment.text)
            #                 sync_done_query = "UPDATE order_item_wise_close SET sync_done=TRUE WHERE id={0}".format(
            #                     self.env.ids[0])
            #
            #                 self.env.cr.execute(sync_done_query)
            #                 self.env.cr.commit()
            #             except:
            #                 raise osv.except_osv(_('Connection ERROR!'),
            #                                      _('Can not connect with magento!!!'))
            #
            # except:
            #     raise osv.except_osv(_('Connection ERROR!'),
            #                          _('Can not connect with magento!!!'))
        return True

    def manual_order_status_sync(self, sale_order_id):

        so_obj = self.env['sale.order']
        ### command by Keya
        ##### need to work
        #####  "get_magento_token" method missing, "cron_script" module acha
        # token, url = get_magento_token(self, sale_order_id)
        # # token, url = sale_order_id
        # if token:
        
        
        #  sync work by shuvarthi
        try:

            token, url = get_magento_token(self, sale_order_id)
            if token:

                for so in so_obj.browse(sale_order_id):

                    status_text = 'processing'

                    if so.delivery_dispatch == True and so.partially_invoiced_dispatch == False:
                        status_text = 'delivered'
                    if so.partially_invoiced_dispatch == True or so.partial_delivered == True:
                        status_text = 'partially_delivered'
                    if so.state == 'done':
                        status_text = 'delivered'
                    if str(so.state) in ['draft', 'approval_pending', 'check_pending', 'new_cus_approval_pending']:
                        status_text = 'pending'
                    if so.state == 'cancel':
                        status_text = 'canceled'

                    if so.invoice_ids:

                        invoices = [inv.state for inv in so.invoice_ids if
                                    inv.move_type == 'out_invoice' and inv.state != 'cancel']
                        invoices_state = [inv_state for inv_state in invoices if inv_state != 'paid']

                        if not invoices_state:
                            status_text = 'complete'

                    invoiced_amount = int(so.invoiced_amount) + int(so.canceled_amount)
                    if int(invoiced_amount) == int(so.amount_total) and int(invoiced_amount)>0:
                        status_text = 'point_gain'

                    if so.invoice_ids:
                        cancel_invoices = [inv.state for inv in so.invoice_ids if
                                           inv.move_type == 'out_invoice' and inv.state == 'cancel'
                                           and inv.x_auto_cancel == True]

                        if cancel_invoices:
                            status_text = 'canceled'

                    try:
                        token = token.replace('"', "")
                        self.order_wise_status_sync(token,url, status_text, so.client_order_ref,)
                    except:
                        pass
        except:
            pass
#         end


class OrderItemWiseCloseLine(models.TransientModel):
    _name = "order.item.wise.close.line"
    _description = "Order Item Wise Close line"

    close_order_id = fields.Many2one('order.item.wise.close', 'Close Order ID', required=True,)

    order_id = fields.Integer('Order ID')
    sale_order_id = fields.Many2one('sale.order', 'Sale Order')
    purchase_order_id = fields.Many2one('purchase.order', 'Purchase Order')
    product_id = fields.Many2one('product.product', 'Product', required=True)
    quantity = fields.Float('Close Quantity')
    order_quantity = fields.Float('Update Quantity')

    # ## Block code for v-14
    # _columns = {
    #     'close_order_id': fields.many2one('order.item.wise.close', 'Close Order ID', required=True,
    #                                       ondelete='cascade', index=True, readonly=True),
    #
    #     'order_id': fields.integer('Order ID'),
    #     'sale_order_id': fields.many2one('sale.order', 'Sale Order'),
    #     'purchase_order_id': fields.many2one('purchase.order', 'Purchase Order'),
    #     'product_id': fields.many2one('product.product', 'Product', required=True),
    #     'quantity': fields.float('Close Quantity'),
    #     'order_quantity': fields.float('Update Quantity'),
    # }
