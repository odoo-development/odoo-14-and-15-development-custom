from lxml import etree

from odoo import models, fields, api, _
from odoo.tools import float_compare


class AccountInvoice(models.Model):
    _inherit = "account.move"

    ndr = fields.Boolean(string='Re-Attempt')
    ndr_count = fields.Integer(string='Re-Attempt COUNT', default=0)
