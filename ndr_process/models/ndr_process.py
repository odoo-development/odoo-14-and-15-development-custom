from odoo import api
from odoo import models, fields
from odoo.tools.translate import _
from odoo.exceptions import UserError, ValidationError, Warning


class NdrProcess(models.Model):
    _name = "ndr.process"
    _description = "NDR Process"

    invoice_number = fields.Char('Invoice Number')
    order_number = fields.Char('Order Number')
    magento_number = fields.Char('Magento Number')
    order_id = fields.Integer('Order ID')
    invoice_id = fields.Integer('Invoice ID')
    ndr_number = fields.Char('NDR Number')
    ndr_reason = fields.Text('NDR Reason', required=True)
    customer_name = fields.Char('Customer Name')
    assigned_to = fields.Char('Assigned To')
    scan = fields.Char('Scan')
    received_by = fields.Many2one('res.users', 'Received By')
    confirm = fields.Boolean('Confirm')
    confirm_by = fields.Many2one('res.users', 'Confirm By')
    confirm_time = fields.Datetime('Confirmation Time')
    cancel = fields.Boolean('Cancel')
    cancel_by = fields.Many2one('res.users', 'Cancel By')
    cancel_time = fields.Datetime('Cancel Time')
    delivery_date = fields.Date('Delivery Date')
    return_date = fields.Datetime('Return Date')
    state = fields.Selection([
        ('draft', 'Draft'),
        ('confirm', 'Confirmed'),
        ('cancel', 'Cancelled'),

    ], 'Status', default='draft', readonly=True, copy=False, help="Gives the status of the Mainfest", index=True)
    ndr_process_line = fields.One2many('ndr.process.line', 'ndr_process_id', 'NDR Process Line', required=True)

    cancel_ndr_process_date = fields.Datetime('Cancel NDR Process Date')
    cancel_ndr_process_by = fields.Many2one('res.users', 'Cancel NDR Process By')
    cancel_ndr_process_reason = fields.Text('Cancel NDR Process Reason')

    # ## Block code for V-14
    #
    # _columns = {
    #     'invoice_number': fields.char('Invoice Number'),
    #     'order_number': fields.char('Order Number'),
    #     'magento_number': fields.char('Magento Number'),
    #     'order_id': fields.integer('Order ID'),
    #     'invoice_id': fields.integer('Invoice ID'),
    #     'ndr_number': fields.char('NDR Number'),
    #     'ndr_reason': fields.text('NDR Reason', required=True),
    #     'customer_name': fields.char('Customer Name'),
    #     'assigned_to': fields.char('Assigned To'),
    #     'scan': fields.char('Scan'),
    #     'received_by': fields.many2one('res.users', 'Received By'),
    #     'confirm': fields.boolean('Confirm'),
    #     'confirm_by': fields.many2one('res.users', 'Confirm By'),
    #     'confirm_time': fields.datetime('Confirmation Time'),
    #     'cancel': fields.boolean('Cancel'),
    #     'cancel_by': fields.many2one('res.users', 'Cancel By'),
    #     'cancel_time': fields.datetime('Cancel Time'),
    #     'delivery_date': fields.date('Delivery Date'),
    #     'return_date': fields.datetime('Return Date'),
    #     'state': fields.selection([
    #         ('draft', 'Draft'),
    #         ('confirm', 'Confirmed'),
    #         ('cancel', 'Cancelled'),
    #
    #     ], 'Status', readonly=True, copy=False, help="Gives the status of the Mainfest", index=True),
    #     'ndr_process_line': fields.one2many('ndr.process.line', 'ndr_process_id', 'NDR Process Line', required=True),
    #
    #     'cancel_ndr_process_date': fields.datetime('Cancel NDR Process Date'),
    #     'cancel_ndr_process_by': fields.many2one('res.users', 'Cancel NDR Process By'),
    #     'cancel_ndr_process_reason': fields.text('Cancel NDR Process Reason'),
    # }
    #
    # _defaults = {
    #     'user_id': lambda obj, cr, uid, context: uid,
    #     'state': 'draft',
    # }

    def check_already_scaned(self, invoice_id):

        already_scanned = False

        inv_env = self.env['ndr.process']
        inv_obj = inv_env.search([('invoice_id', '=', invoice_id)])

        if len(inv_obj) >= 1:
            reversed_inv = inv_obj.sorted(key=lambda r: r.create_date, reverse=True)
            inv_obj = reversed_inv[0]

            from datetime import date
            import time

            create_time = time.strptime(inv_obj.create_date, "%Y-%m-%d %H:%M:%S")

            today_date = date.today()
            create_date = date(create_time.tm_year, create_time.tm_mon, create_time.tm_mday)

            date_diff = today_date - create_date

            if date_diff.days == 0:
                already_scanned = True
            else:
                already_scanned = False

        return already_scanned

    @api.onchange('scan')
    def so_return_invoice(self):
        if self.scan:
            invoice_id = str(self.scan)

            if invoice_id != 'False' and self.invoice_number is False:

                inv_env = self.env['account.move']
                inv_obj = inv_env.search([('id', '=', invoice_id)])

                if self.check_already_scaned(invoice_id):
                    self.scan = ""
                else:

                    so_env = self.env['sale.order']
                    so_obj = so_env.search([('client_order_ref', '=', str(inv_obj.name)),('state', '!=','cancel')])

                    # self.name = inv_obj.name
                    self.invoice_number = inv_obj.name
                    # self.invoice = inv_obj
                    self.magento_number = inv_obj.name
                    self.order_number = so_obj.name
                    self.order_id = so_obj.id

                    inv_line_list = list()

                    for invoice_line in inv_obj.invoice_line_ids:
                        inv_line_list.append((0, 0, {
                            'product': str(invoice_line.name),
                            'product_id': int(invoice_line.product_id.id),
                            'product_ean': '',
                            # 'product_ean': str(invoice_line.product_id.ean13) if invoice_line.product_id.ean13 else '',
                            'quantity': invoice_line.quantity,
                            # 'received_quantity': 0,

                        }))

                    self.ndr_process_line = inv_line_list
                    self.scan = ""

    #     try:
    #
    #         if invoice_id != 'False' and self.invoice_number is False:
    #
    #             inv_env = self.env['account.move']
    #             inv_obj = inv_env.search([('id', '=', invoice_id)])
    #
    #             if self.check_already_scaned(invoice_id):
    #                 self.scan = ""
    #             else:
    #
    #                 so_env = self.env['sale.order']
    #                 so_obj = so_env.search([('client_order_ref', '=', str(inv_obj.name))])
    #
    #                 # self.name = inv_obj.name
    #                 self.invoice_number = inv_obj.number
    #                 self.invoice = inv_obj
    #                 self.magento_number = inv_obj.name
    #                 self.order_number = so_obj.name
    #                 self.order_id = so_obj.id
    #
    #                 inv_line_list = list()
    #
    #                 for invoice_line in inv_obj.invoice_line:
    #                     inv_line_list.append({
    #                         'product': str(invoice_line.name),
    #                         'product_id': int(invoice_line.product_id.id),
    #                         'product_ean': str(invoice_line.product_id.ean13) if invoice_line.product_id.ean13 else '',
    #                         'quantity': invoice_line.quantity,
    #                         # 'received_quantity': 0,
    #
    #                     })
    #
    #                 self.ndr_process_line = inv_line_list
    #                 self.scan = ""
    #     except:
    #         self.scan = ""
    #         pass
    #
    #     return "xXxXxXxXxX"

    def confirm_ndr(self):

        data = self.read()[0]

        invoice_id = None

        invoice_id = data.get('invoice_id')

        if invoice_id is not None:
            self.env.cr.execute("select ndr_count from account_move where id=%s", ([invoice_id]))
            invoice_data = self.env.cr.fetchall()
            inv_obj = None

            if len(invoice_data) > 0:
                inv_obj = invoice_data[0][0] if invoice_data[0][0] else 0
                inv_obj = inv_obj + 1
                release_invoice_query = "UPDATE account_move SET assigned=False,x_loading_assign=FALSE,delivered=False,ndr=True, ndr_count='{0}' WHERE id={1}".format(
                    inv_obj, invoice_id)
                self.env.cr.execute(release_invoice_query)
                self.env.cr.commit()

        for single_id in self.ids:
            confirm_ndr_query = "UPDATE ndr_process SET state='confirm', confirm_time='{0}' WHERE id={1}".format(
                str(fields.datetime.now()), single_id)
            self.env.cr.execute(confirm_ndr_query)
            self.env.cr.commit()

            confirm_ndr_query_line = "UPDATE ndr_process_line SET state='confirm', confirm_time='{0}' WHERE ndr_process_id={1}".format(
                str(fields.datetime.now()), single_id)
            self.env.cr.execute(confirm_ndr_query_line)
            self.env.cr.commit()

        return True

    def cancel_ndr(self):

        for single_id in self.ids:
            cancel_ndr_query = "UPDATE ndr_process SET state='cancel', cancel_time='{0}' WHERE id={1}".format(
                str(fields.datetime.now()), single_id)
            self.env.cr.execute(cancel_ndr_query)
            self.env.cr.commit()

            cancel_ndr_query_line = "UPDATE ndr_process_line SET state='cancel', cancel_time='{0}' WHERE ndr_process_id={1}".format(
                str(fields.datetime.now()), single_id)
            self.env.cr.execute(cancel_ndr_query_line)
            self.env.cr.commit()

        return True

    @api.model
    def create(self, vals):

        inv_env = self.env['account.move']
        inv_obj = inv_env.search([('name', '=', str(vals['invoice_number']))])

        for inv in inv_obj:
            if inv.delivered or inv.move_type == 'out_refund':
                if inv.move_type == 'out_refund':
                    raise Warning(_('This order is already returned.'))
                else:
                    raise Warning(_('This order is already delivered.'))

        record = super(NdrProcess, self).create(vals)

        so_env = self.env['sale.order']
        so_obj = so_env.search([('client_order_ref', '=', str(record.magento_number)),('state', '!=','cancel')])

        ndr_number = "NDR0" + str(record.id)
        order_id = so_obj.id

        record.invoice_id = inv_obj.id

        record.ndr_number = ndr_number
        record.order_number = so_obj.name

        record.order_id = order_id
        # record.delivery_date = so_obj.magetno_delivery_at
        record.return_date = fields.datetime.now()
        record.received_by = self._uid

        return record


class NdrProcessLine(models.Model):
    _name = "ndr.process.line"
    _description = "NDR Process Line"

    ndr_process_id = fields.Many2one('ndr.process', 'NDR Process ID', required=True,
                                     ondelete='cascade', index=True, readonly=True)
    product = fields.Char('Product')
    product_id = fields.Integer('Product ID')
    product_ean = fields.Char('Product EAN')

    confirm_time = fields.Datetime('Confirmation Time')
    cancel_time = fields.Datetime('Cancel Time')

    quantity = fields.Float('Quantity')
    state = fields.Selection([
        ('draft', 'Draft'),
        ('confirm', 'Confirmed'),
        ('cancel', 'Cancelled'),
    ], 'Status', help="Gives the status of the IRN control line", index=True)

    # ##Block code for V-14
    # _columns = {
    #     'ndr_process_id': fields.many2one('ndr.processs', 'NDR Process ID', required=True,
    #                                       ondelete='cascade', index=True, readonly=True),
    #     'product': fields.char('Product', readonly=True),
    #     'product_id': fields.integer('Product ID', readonly=True),
    #     'product_ean': fields.char('Product EAN'),
    #
    #     'confirm_time': fields.datetime('Confirmation Time'),
    #     'cancel_time': fields.datetime('Cancel Time'),
    #
    #     'quantity': fields.float('Quantity', readonly=True),
    #     # 'delivered_quantity': fields.float('Delivered Qty'),
    #     'state': fields.selection([
    #         ('draft', 'Draft'),
    #         ('confirm', 'Confirmed'),
    #         ('cancel', 'Cancelled'),
    #     ], 'Status', help="Gives the status of the IRN control line", index=True),
    # }


class CancelNdrProcessReason(models.Model):
    _name = "cancel.ndr.process.reason"
    _description = "Cancel NDR Process Reason"

    cancel_ndr_process_date = fields.Datetime('Cancel NDR Process Date')
    cancel_ndr_process_by = fields.Many2one('res.users', 'Cancel NDR Process By')
    cancel_ndr_process_reason = fields.Text('Cancel NDR Process Reason', required=True)

    # ## Block code for V-14
    # _columns = {
    #     'cancel_ndr_process_date': fields.datetime('Cancel NDR Process Date'),
    #     'cancel_ndr_process_by': fields.many2one('res.users', 'Cancel NDR Process By'),
    #     'cancel_ndr_process_reason': fields.text('Cancel NDR Process Reason', required=True),
    # }

    def cancel_ndr_process_reason_def(self):
        ids = self.env.context['active_ids']
        cancel_ndr_process_reason = str(self.env.context['cancel_ndr_process_reason'])
        cancel_ndr_process_by = self.env.uid
        cancel_ndr_process_date = fields.datetime.now()

        ndr_obj = self.env['ndr.process']

        for s_id in ids:
            cancel_ndr_proces_query = "UPDATE ndr_process SET cancel_ndr_process_reason='{0}', cancel_ndr_process_by={1}, cancel_ndr_process_date='{2}' WHERE id={3}".format(
                cancel_ndr_process_reason, cancel_ndr_process_by, cancel_ndr_process_date, s_id)
            self.env.cr.execute(cancel_ndr_proces_query)
            self.env.cr.commit()

            ndr_obj.cancel_ndr()

        return True
