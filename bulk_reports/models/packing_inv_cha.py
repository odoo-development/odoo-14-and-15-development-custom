from odoo import models, fields, api, _
from odoo.tools.translate import _
from odoo.exceptions import UserError, ValidationError, Warning


class PackingList(models.Model):
    _inherit = "packing.list"

    def print_packing_invoice_report_button(self):

        return self.env.ref("bulk_reports.action_packing_invoice_report").report_action(self)



    def print_packing_challan_report_button(self):

        return self.env.ref("bulk_reports.action_packing_challan_report").report_action(self)



    def print_packing_mushak_report_button(self):

        return self.env.ref("bulk_reports.action_packing_mushak_report").report_action(self)



    def print_packing_warranty_report_button(self):

        return self.env.ref("bulk_reports.action_packing_warranty_report").report_action(self)

