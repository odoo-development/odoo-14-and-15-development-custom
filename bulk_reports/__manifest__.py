{
    'name': 'SO Invoices, Delivery Challans and Mushak report Generate from Packings',
    'version': '14.0.0',
    'category': 'WMS',
    'description': """
Generate the   
==============================================================

""",
    'author': 'Sindabad',
    'depends': ['product','packings','order_delivery_dispatch','account'],
    'data': [
        'report/report_packing_invoice_layout.xml',
        'report/report_packing_challan_layout.xml',
        'report/report_packing_mushak_layout.xml',
        # 'report/report_invoice_warranty_layout.xml',
        # 'report/report_packing_warranty_layout.xml',

        'views/report_menu.xml',
        'views/packing_inherit_view.xml',
    ],

    'installable': True,
    'auto_install': False,
}

