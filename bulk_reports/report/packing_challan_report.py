from odoo import api, models, _
from datetime import datetime


class PackingChallanReport(models.AbstractModel):
    _name = 'report.bulk_reports.report_packing_challan_layout'
    _description = 'Packing Delivery Challan Report'

    @api.model
    def _get_report_values(self, docids, data=None):
        docs = self.env['packing.list'].browse(docids[0])

        return {
            'doc_ids': docids,
            'doc_model': 'packing.list',
            'docs': docs,
            'data': data,

            'get_packing_slip': self.get_packing_slip,
            'get_purchase_ref': self.get_purchase_ref,
            'get_payment_method': self.get_payment_method,

            'get_client_invoice_address': self.get_client_invoice_address,
            'get_client_shipping_address': self.get_client_shipping_address,
        }

    def get_packing_slip(self, obj):
        order_numbers = [str(obj)]
        pack_ref_cus = None

        self.env.cr.execute("SELECT x_pack_ref_cus FROM sale_order WHERE name=%s", (order_numbers))
        for item in self.env.cr.fetchall():
            pack_ref_cus = item[0]

        return pack_ref_cus if pack_ref_cus else '--'

    def get_purchase_ref(self, obj):
        order_numbers = [str(obj)]
        purchase_ref_cus = None
        self.env.cr.execute("SELECT x_puchase_ref_cus FROM sale_order WHERE name=%s", (order_numbers))
        for item in self.env.cr.fetchall():
            purchase_ref_cus = item[0]

        return purchase_ref_cus if purchase_ref_cus else '---'

    def get_payment_method(self, obj):

        order_numbers = [str(obj)]
        so_obj = self.env['sale.order'].browse(order_numbers[0])

        return so_obj

    def get_client_invoice_address(self, obj):

        order_numbers = [str(obj)]

        partner_invoice_id_query = "SELECT partner_invoice_id FROM sale_order WHERE name='{0}'".format(order_numbers[0])
        self.env.cr.execute(partner_invoice_id_query)
        partner_invoice_id = 0
        for item in self.env.cr.fetchall():
            partner_invoice_id = int(item[0])

        res_partner_obj = self.env['res.partner']
        res_partner = res_partner_obj.browse([partner_invoice_id])

        return res_partner if res_partner else '-'

    def get_client_shipping_address(self, obj):

        order_numbers = [str(obj)]

        partner_shipping_id_query = "SELECT partner_shipping_id FROM sale_order WHERE name='{0}'".format(
            order_numbers[0])
        self.env.cr.execute(partner_shipping_id_query)
        for item in self.env.cr.fetchall():
            partner_shipping_id = int(item[0])

        res_partner_obj = self.env['res.partner']
        res_partner = res_partner_obj.browse([partner_shipping_id])

        return res_partner if res_partner else '-'
