from odoo import api, models, _
from datetime import datetime


class PackingInvoiceReport(models.AbstractModel):
    _name = 'report.bulk_reports.report_packing_invoice_layout'
    _description = 'Packing Invoice Report'

    @api.model
    def _get_report_values(self, docids, data=None):
        docs = self.env['packing.list'].browse(docids[0])

        return {
            'doc_ids': docids,
            'doc_model': 'packing.list',
            'docs': docs,
            'data': data,
            'get_client_shipping_address': self.get_client_shipping_address,
        }

    def get_client_shipping_address(self, obj):

        origin = [str(obj)]
        picking_origin = None

        if origin is not None:
            self.env.cr.execute("SELECT origin FROM stock_picking WHERE name=%s", (origin))

            for item in self.env.cr.fetchall():
                picking_origin = item[0]

        order_numbers = [picking_origin]

        partner_shipping_id_query = "SELECT partner_shipping_id FROM sale_order WHERE name='{0}'".format(order_numbers[0])
        self.env.cr.execute(partner_shipping_id_query)
        for item in self.env.cr.fetchall():
            partner_shipping_id = int(item[0])

        res_partner_obj = self.env['res.partner']
        res_partner = res_partner_obj.browse([partner_shipping_id])

        return res_partner if res_partner else '-'

