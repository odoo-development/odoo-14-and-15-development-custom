{
    'name': 'Partial Delivery Order Close',
    'version': "14.0.1.0.0",
    'category': 'Sales',
    'description': "Partial Delivery Order Close",
    'author': 'Odoo Bangladesh',
    'depends': ['odoo_magento_connect', 'sale_management', 'account', 'stock', 'stock_account'],
    'data': [
            'security/partial_delivery_order_close_security.xml',
            'security/ir.model.access.csv',
            'wizard/order_close.xml',
            'views/sale_view.xml'
    ],
    'installable': True,
    'auto_install': False,
}
