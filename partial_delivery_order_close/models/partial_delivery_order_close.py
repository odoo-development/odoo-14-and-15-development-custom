from odoo import api, fields, models, _


class SaleOrder(models.Model):
    _inherit = "sale.order"

    # Received amount
    def _get_canceled_amount(self):

        for sale_obj in self:
            total_sum = 0.00

            # init_data = stock_move_obj.search([('origin', '=', sale_obj.name)])

            # for so_move in stock_move_obj.browse(init_data):
            for order in sale_obj.order_line:

                if order.closed_quantity > float(0):
                    total_sum = total_sum + (order.closed_quantity * order.price_unit)

            sale_obj.canceled_amount = total_sum


    canceled_amount = fields.Float(string='Cancelled Amount', compute='_get_canceled_amount')
    # order_close_reason = fields.Char('Order Close Reason', required=True)
    order_close_reason = fields.Char('Order Close Reason')

    # ## Block code for V-14
    # _columns = {
    #     'canceled_amount': fields.function(_get_canceled_amount, string='Cancelled Amount', type='float'),
    #     'order_close_reason': fields.char('Order Close Reason', required=True),
    # }


class PartiallyDeliveredOrderClose(models.Model):
    _name = "partially.delivered.order.close"
    _description = "Partially delivered order close"

    order_close_reason = fields.Selection([
        ("product_not_available", "Product Not Available"),
        ("product_not_available_customer_will_take_alternative_product",
         "Product not available, Customer will take alternative product"),
        ("partially_delivered", "Partially delivered"),
        ("reordered", "Reordered#10000"),
        ("payment_issue", "Payment issue"),
        ("cash_problem", "Cash problem"),
        ("damage_product", "Damage product"),
        ("double_ordered", "Double ordered"),
        ("sample_not_approved", "Sample not approved"),
        ("wrong_sku", "Wrong SKU"),
        ("not_interested", "Not Interested"),
        ("wrong_product", "Wrong Product"),
        ("quality_issue", "Quality issue"),
        ("delay_delivery", "Delay delivery"),
        ("sourcing_delay", "Sourcing Delay")
    ], 'Order Close Reason', copy=False, help="Reasons", index=True)

    order_name = fields.Char('Order Name')
    order_ref = fields.Char('Order Ref.')

    def partial_delivery_close(self):

        ids = self._context['active_ids']

        close_order_tbl_ids = ids
        order_close_reason = self._context['order_close_reason']
        so_obj = self.env['sale.order']

        for so in so_obj.browse(ids):

            stock_picking_query = "SELECT id FROM stock_picking WHERE origin='{0}' and state!='done'".format(
                str(so.name))
            self.env.cr.execute(stock_picking_query)

            for sp_id in self.env.cr.fetchall():
                # unreserve
                stock_obj = self.env['stock.picking'].browse(sp_id)
                stock_obj.do_unreserve()
                # self.env['stock.picking'].do_unreserve(int(sp_id))

                # action cancel
                stock_obj.action_cancel()

                for stock_move in stock_obj.move_lines:

                    sol_update_query = "UPDATE sale_order_line SET closed_quantity='{0}' WHERE id='{1}'".format(
                        stock_move.product_uom_qty,stock_move.sale_line_id.id )

                    self.env.cr.execute(sol_update_query)
                    self.env.cr.commit()

                # self.env['stock.picking'].action_cancel(int(sp_id))

        # Action Done
            so.action_cancel()

        for so_id in ids:
            so_state_query = "UPDATE sale_order SET shipped=TRUE, waiting_availability=FALSE , partially_available=FALSE, partial_delivered=FALSE, order_close_reason='{0}' WHERE id='{1}'".format(
                order_close_reason, so_id)

            self.env.cr.execute(so_state_query)
            self.env.cr.commit()

            for so in so_obj.browse(so_id):
                order_name = str(so.name)
                order_ref = str(so.client_order_ref)

                for co_id in close_order_tbl_ids:
                    so_state_query = "UPDATE partially_delivered_order_close SET order_name='{0}', order_ref='{1}' WHERE id='{2}'".format(
                        order_name, order_ref, co_id)

                    self.env.cr.execute(so_state_query)
                    self.env.cr.commit()
            self.env['order.item.wise.close'].manual_order_status_sync([so_id])
        # return True
