{
    'name': 'Product COGS Correction',
    'version': "14.0.1.0.0",
    'category': 'Sales',
    'description': "Product COGS Correction",
    'author': 'Odoo Bangladesh',
    'depends': ['stock', 'stock_account'],
    'data': [

    ],
    'installable': True,
    'auto_install': False,
}
