import logging

from odoo import fields, models
from odoo.tools.float_utils import float_round

_logger = logging.getLogger(__name__)


class StockQuant(models.Model):
    """
    Quants are the smallest unit of stock physical instances
    """
    _inherit = "stock.quant"

    def _get_inventory_value(self, cr, uid, quant, context=None):

        total_cost = 0

        if quant.total_calculated_cost > 0:
            return quant.total_calculated_cost
        else:
            for items in quant.history_ids:
                all_ok = True

                if items.origin is False or items.origin is None:
                    all_ok = False
                if 'Transit' in items.location_id.name or 'Transit' in items.location_dest_id.name:
                    all_ok = False

                if items.product_id.id == quant.product_id.id and str(items.state) == str('done') and all_ok == True:
                    total_cost = total_cost + (items.price_unit * quant.qty)
                    break

        if total_cost > 0:
            self.env.cr.execute("update stock_quant set total_calculated_cost=%s where id=%s", ([total_cost, quant.id]))
            self.env.cr.commit()
            return total_cost
        else:

            return quant.product_id.standard_price * quant.qty

    total_calculated_cost = fields.Float('Total Cost Calculated')

    ### Block code for V-14
    # _columns = {
    #     'total_calculated_cost': fields.float('Total Cost Calculated'),
    # }


class ProductProduct(models.Model):
    _inherit = "product.product"

    # Commented By Matiar Rahman
    # This is a base function, but didn't call the super, it may violate default behavior
    # def _product_available(self, field_names=None, arg=False):
    #     context = context or {}
    #     field_names = field_names or []
    #
    #     domain_products = [('product_id', 'in', ids)]
    #     domain_quant, domain_move_in, domain_move_out = [], [], []
    #     domain_quant_loc, domain_move_in_loc, domain_move_out_loc = self._get_domain_locations(cr, uid, ids,
    #                                                                                            context=context)
    #     domain_move_in += self._get_domain_dates(cr, uid, ids, context=context) + [
    #         ('state', 'not in', ('done', 'cancel', 'draft'))] + domain_products
    #     domain_move_out += self._get_domain_dates(cr, uid, ids, context=context) + [
    #         ('state', 'not in', ('done', 'cancel', 'draft')), ('origin', 'ilike', 'SO')] + domain_products
    #     domain_quant += domain_products
    #
    #     if context.get('lot_id'):
    #         domain_quant.append(('lot_id', '=', context['lot_id']))
    #     if context.get('owner_id'):
    #         domain_quant.append(('owner_id', '=', context['owner_id']))
    #         owner_domain = ('restrict_partner_id', '=', context['owner_id'])
    #         domain_move_in.append(owner_domain)
    #         domain_move_out.append(owner_domain)
    #     if context.get('package_id'):
    #         domain_quant.append(('package_id', '=', context['package_id']))
    #
    #     domain_move_in += domain_move_in_loc
    #     domain_move_out += domain_move_out_loc
    #     moves_in = self.env['stock.move'].read_group(cr, uid, domain_move_in, ['product_id', 'product_qty'],
    #                                                       ['product_id'], context=context)
    #     moves_out = self.env['stock.move'].read_group(cr, uid, domain_move_out, ['product_id', 'product_qty'],
    #                                                        ['product_id'], context=context)
    #
    #     domain_quant += domain_quant_loc
    #     quants = self.env['stock.quant'].read_group(cr, uid, domain_quant, ['product_id', 'qty'], ['product_id'],
    #                                                      context=context)
    #     quants = dict(map(lambda x: (x['product_id'][0], x['qty']), quants))
    #
    #     moves_in = dict(map(lambda x: (x['product_id'][0], x['product_qty']), moves_in))
    #     moves_out = dict(map(lambda x: (x['product_id'][0], x['product_qty']), moves_out))
    #     res = {}
    #     for product in self.browse(cr, uid, ids, context=context):
    #         id = product.id
    #         qty_available = float_round(quants.get(id, 0.0), precision_rounding=product.uom_id.rounding)
    #         incoming_qty = float_round(moves_in.get(id, 0.0), precision_rounding=product.uom_id.rounding)
    #         outgoing_qty = float_round(moves_out.get(id, 0.0), precision_rounding=product.uom_id.rounding)
    #         virtual_available = float_round(quants.get(id, 0.0) + moves_in.get(id, 0.0) - moves_out.get(id, 0.0),
    #                                         precision_rounding=product.uom_id.rounding)
    #         res[id] = {
    #             'qty_available': qty_available,
    #             'incoming_qty': incoming_qty,
    #             'outgoing_qty': outgoing_qty,
    #             'virtual_available': virtual_available,
    #         }
    #     return res
