from odoo import models, fields


class MenuList(models.Model):
    _name = 'wms.menulist'
    _description = "Menu List"

    name = fields.Char(string="Name", required=True)