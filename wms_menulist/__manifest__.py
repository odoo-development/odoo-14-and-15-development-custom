{
    'name': "WMS Menu List",
    'version': "14.0.1.0.0",
    'category': 'Information Technology',
    'author': "Odoo Bangladesh",
    'description': """
WMS menu list for warehouse people.
    """,
    'website': "",
    'data': [
        'security/wms_security.xml',
        'security/ir.model.access.csv',
        'views/wms_view.xml',
        # 'views/wms_inbound_view.xml',
        'views/reporting_view.xml',
        'views/menulist_view.xml'
    ],
    'depends': [
        'wms_base',
        'sale',
        'purchase',
        # 'sale_management', 'stock', 'account', 'purchase', 'pickings', 'packings', 'wms_inbound',
    ],
    'installable': True,
    'auto_install': False,
}
