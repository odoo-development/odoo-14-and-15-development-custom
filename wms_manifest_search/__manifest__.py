{
    'name': 'WMS Manifest Custom Search',
    'version': "14.0.1.0.0",
    'category': 'WMS',
    'author': 'Odoo Bangladesh',
    'summary': 'WMS Manifest Custom Search',
    'description': 'WMS Manifest Custom Search',
    'depends': ['base', 'wms_manifest'],
    'installable': True,
    'application': True,
    'auto_install': False,
}
