from operator import itemgetter

from odoo import models, fields


class WmsManifestCustomSearch(models.Model):
    _inherit = 'wms.manifest.process'

    def _get_order_no(self):

        return True

    def _order_no_search(self, operator, value):

        # args = [('order_on', 'ilike', '1000026832')]
        operator = operator
        order_no = value

        wms_manifest_line_query = "SELECT DISTINCT wms_manifest_id FROM wms_manifest_line WHERE magento_no = '{0}'".format(
            order_no)
        self.env.cr.execute(wms_manifest_line_query)

        res = self.env.cr.fetchall()
        if not res:
            return [('id', '=', '0')]
        return [('id', 'in', res)]

    def _get_invoice_no(self):

        return True

    def _invoice_no_search(self, operator, value):
        # operator = [item for item in args][0][1]
        invoice_no = value

        account_invoice_id_list = self.env['account.move'].search([('name', '=', invoice_no)])

        if account_invoice_id_list:
            wms_manifest_line_query = "SELECT DISTINCT wms_manifest_id FROM wms_manifest_line WHERE invoice_id = '{0}'".format(
                account_invoice_id_list[0].id)
            self.env.cr.execute(wms_manifest_line_query)

            res = self.env.cr.fetchall()
        else:
            res = None

        # SALJ/2019/10148

        if not res:
            return [('id', '=', '0')]
        return [('id', 'in', res)]

    def _get_priority_customer(self):

        return True

    def _priority_customer_search(self, operator, value):

        # args = [('order_on', 'ilike', '1000026832')]
        operator = operator
        order_no = value

        wms_manifest_line_query = "SELECT DISTINCT wms_manifest_id FROM wms_manifest_line WHERE priority_customer = '{0}'".format(
            order_no)
        self.env.cr.execute(wms_manifest_line_query)

        res = self.env.cr.fetchall()
        if not res:
            return [('id', '=', '0')]
        return [('id', 'in', res)]

    order_on = fields.Text(compute='_get_order_no', string='Order Number', search='_order_no_search', index=True)
    invoice_on = fields.Text(compute='_get_invoice_no', string='Invoice Number', search='_invoice_no_search')
    priority_customer_on = fields.Boolean(compute='_get_priority_customer', string='Priority Customer',
                                          search='_priority_customer_search' )

    # ## Block code for v-14
    # _columns = {
    #     'order_on': fields.function(_get_order_no, string='Order Number', fnct_search=_order_no_search, type='text'),
    #     'invoice_on': fields.function(_get_invoice_no, string='Invoice Number', fnct_search=_invoice_no_search,
    #                                   type='text'),
    #     'priority_customer_on': fields.function(_get_priority_customer, string='Priority Customer',
    #                                             fnct_search=_priority_customer_search,
    #                                             type='boolean'),
    # }
