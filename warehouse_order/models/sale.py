from odoo import api, fields, models, _
from odoo.exceptions import ValidationError
from odoo.osv import osv

class purchase_order(models.Model):
    _inherit = "stock.picking"

    # def _mag_no(self):
    #     for obj in self:
    #         if obj.origin:
    #             if 'S' in obj.origin:
    #                 try:
    #                     sale_order = self.env['sale.order'].search([('name', '=', obj.origin)], limit=1)
    #                     obj.mag_no = sale_order.client_order_ref if sale_order else obj.origin
    #                 except Exception as e:
    #                     obj.mag_no = obj.origin
    #             else:
    #                 obj.mag_no = ''

    def _mag_no(self):
        for obj in self:
            obj.mag_no = ''

            if obj.origin is not False:
                if 'S' in obj.origin:
                    try:
                        sale_ids = self.env['sale.order'].search([('name', '=', obj.origin)], limit=1)
                        obj.mag_no = sale_ids.client_order_ref
                    except:
                        obj.mag_no = obj.origin

                else:
                    obj.mag_no = ''

    def _magento_number_search(self, args):
        if not len(args):
            return []

        filtered_ids = [('id', '=', 0)]

        for tuple_items in args:
            searched_data = tuple_items[2]

            obj_data = self.env['sale.order'].search([('client_order_ref', 'like', searched_data),('state', '!=','cancel')])
            # obj_data = self.env['sale.order'].browse(sale_ids)

            order_list = [items.name for items in obj_data]

            picking_ids = self.env['stock.picking'].search([('origin', 'in', order_list)])
            filtered_ids = [('id', 'in', [ids for ids in picking_ids])]

        return filtered_ids

    def _shipment_search(self, args):
        selection_dict = dict()
        selection_dict[2029] = '5 Star'
        selection_dict[2028] = 'Aramex'
        selection_dict[2051] = 'In House'
        selection_dict[2026] = 'S A Paribahan'
        selection_dict[2025] = 'Sundarban'
        selection_dict[3645] = 'GoFetch'
        selection_dict[3647] = 'eCourier'
        selection_dict[3700] = 'GoGo'
        selection_dict[3730] = 'Biddyut'
        selection_dict[3731] = 'Pathao'
        selection_dict[0] = 'Not Set'
        selection_dict[2027] = 'Not Yet Defined'

        if not len(args):
            return []

        filtered_ids = [('id', '=', 0)]
        shipment_ids = []

        for tuple_items in args:
            searched_data = tuple_items[2]
            for k, v in selection_dict.iteritems():
                if searched_data in v:
                    shipment_ids.append(k)

            sale_ids = self.env['sale.order'].search([('magetno_shipment_by', 'in', shipment_ids)])
            obj_data = self.env['sale.order'].browse(sale_ids)

            order_list = [items.name for items in obj_data]

            picking_ids = self.env['stock.picking'].search([('origin', 'in', order_list)])
            filtered_ids = [('id', 'in', [ids for ids in picking_ids])]

        return filtered_ids

    def _shipping_name(self):
        res = {}
        stock_objects_id = self.env['stock.picking'].browse()
        selection_dict = dict()
        selection_dict[2029] = '5 Star'
        selection_dict[2028] = 'Aramex'
        selection_dict[2051] = 'In House'
        selection_dict[2026] = 'S A Paribahan'
        selection_dict[2025] = 'Sundarban'
        selection_dict[3645] = 'GoFetch'
        selection_dict[3647] = 'eCourier'
        selection_dict[3700] = 'GoGo'
        selection_dict[3730] = 'Biddyut'
        selection_dict[3731] = 'Pathao'
        selection_dict[0] = 'Not Set'
        selection_dict[2027] = 'Not Yet Defined'
        try:
            for obj in stock_objects_id:
                if 'SO' in obj.origin:
                    try:
                        sale_ids = self.env['sale.order'].search([('name', '=', obj.origin)])

                        obj_data = self.env['sale.order'].browse(sale_ids)

                        if int(obj_data.magetno_shipment_by) in selection_dict.keys():
                            res[obj.id] = selection_dict[int(obj_data.magetno_shipment_by)]
                    except:
                        res[obj.id] = ''
                else:
                    res[obj.id] = ''
        except:
            pass

        return res

    # v-14 ta  fnct_search=_magento_number_search nai,

    mag_no = fields.Char(related='sale_id.client_order_ref', string="Magento Number", index=True, store=True)
    shipment_by = fields.Char(string='Shipment By', compute ='_shipping_name')


    # ## Block code for V-14
    # _columns = {
    #
    #     'mag_no': fields.function(_mag_no, string='Magento Number', fnct_search=_magento_number_search, type='char'),
    #     'shipment_by': fields.function(_shipping_name, string='Shipment By', fnct_search=_shipment_search, type='char'),
    #
    # }
