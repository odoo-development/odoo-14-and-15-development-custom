
{
    'name': 'Warehouse Tree view magento order no, shipment by',
    'version': "1.0.0",
    'category': 'Tools',
    'summary': """ It will add two fields Magento Order No and Shipment By name""",
    'description': """After Installing add 2 fields in stock picking tree view""",
    'author': 'Odoo Bangladesh',
    'website': '',
    'depends': ['stock', 'sale'],
    "data": [
        "views/stock_picking_view.xml",
    ],
    'installable': True,
    'active': False,
}
