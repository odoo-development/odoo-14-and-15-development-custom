# Author Rocky

from odoo import fields, models, api, _


class POEndToEnd(models.Model):
    _name = "po.end.to.end"
    _description = "PO Approval End-to-End"

    STATE_SELECTION = [
        ('draft', 'RFQ'),
        ('sent', 'RFQ Sent'),

        ('category_head_approval_pending', 'Category Head Approval Pending'),
        ('category_vp_approval_pending', 'Category VP Approval Pending'),
        ('coo_approval_pending', 'COO Approval Pending'),
        ('ceo_approval_pending', 'CEO Approval Pending'),

        ('to approve', 'To Approve'),
        ('purchase', 'Purchase Order'),
        ('done', 'Locked'),
        ('cancel', 'Cancelled'),
    ]

    po_number = fields.Many2one('purchase.order', string='PO Number')
    po_id = fields.Integer('Purchase ID')
    po_date_create = fields.Datetime('PO Created Date')

    category_head_approve_date = fields.Text('Category Head Approve Date')
    category_head_approve_by = fields.Text('Category Head Approve By')

    category_vp_approve_date = fields.Text('Category VP Approve Date')
    category_vp_approve_by = fields.Text('Category VP Approve By')

    coo_approve_date = fields.Text('COO Approve Date')
    coo_approve_by = fields.Text('COO Approve By')

    ceo_approve_date = fields.Text('CEO Approve Date')
    ceo_approve_by = fields.Text('CEO Approve By')

    state = fields.Selection(STATE_SELECTION, 'Status', readonly=True,
                              help="The status of the purchase order or the quotation request. "
                                   "A request for quotation is a purchase order in a 'Draft' status. "
                                   "Then the order has to be confirmed by the user, the status switch "
                                   "to 'Confirmed'. Then the supplier must confirm the order to change "
                                   "the status to 'Approved'. When the purchase order is paid and "
                                   "received, the status becomes 'Done'. If a cancel action occurs in "
                                   "the invoice or in the receipt of goods, the status becomes "
                                   "in exception.",
                              select=True, copy=False)

    def po_data_create(self, po_number, po_id, value):

        po_count = self.search_count([('po_number','=',po_number), ('po_id', '=', po_id)])

        if int(po_count) == 0:
            created_id = self.create({
                'po_number': po_number,
                'po_id': po_id,
                'po_date_create': value['po_date_create'],
                'state': value['state'],
            })

        return True

    def po_confirm_update(self, po_number, po_id, value):

        po_etoe_id = self.search([('po_number', '=', po_number), ('po_id', '=', po_id)])

        po_etoe_id.write({
            'category_head_approve_date': value['category_head_approve_date'],
            'category_head_approve_by': value['category_head_approve_by'],
            'category_vp_approve_date': value['category_vp_approve_date'],
            'category_vp_approve_by': value['category_vp_approve_by'],
            'coo_approve_date': value['coo_approve_date'],
            'coo_approve_by': value['coo_approve_by'],
            'state': value['state'],
        })

        return True



