# -*- coding: utf-8 -*-
# Author Rocky 2021

from odoo import api, fields, models, _


class POApprovalAmountRange(models.Model):
    _name = "po.approval.amount.range"
    _inherit = ['mail.thread']

    head_range1 = fields.Integer('Category Head Amount Ranges From')
    head_range2 = fields.Integer('Category Head Amount Ranges To')
    vp_range1 = fields.Integer('Category VP Amount Ranges From')
    vp_range2 = fields.Integer('Category VP Amount Ranges To')
    coo_range1 = fields.Integer('COO Amount Ranges From')
    coo_range2 = fields.Integer('COO Amount Ranges To')
    ceo_range1 = fields.Integer('CEO Amount Ranges From')
    ceo_range2 = fields.Integer('CEO Amount Ranges To')


class PurchaseOrder(models.Model):
    _inherit = "purchase.order"

    STATE_SELECTION = [
        ('draft', 'RFQ'),
        ('sent', 'RFQ Sent'),

        ('category_head_approval_pending', 'Category Head Approval Pending'),
        ('category_vp_approval_pending', 'Category VP Approval Pending'),
        ('coo_approval_pending', 'COO Approval Pending'),
        ('ceo_approval_pending', 'CEO Approval Pending'),

        ('to approve', 'To Approve'),
        ('purchase', 'Purchase Order'),
        ('done', 'Locked'),
        ('cancel', 'Cancelled'),
    ]

    category_head_approve_date = fields.Datetime('Category Head Approve Date')
    category_head_approve_by = fields.Many2one('res.users', 'Category Head Approve By')

    category_vp_approve_date = fields.Datetime('Category VP Approve Date')
    category_vp_approve_by = fields.Many2one('res.users', 'Category VP Approve By')

    coo_approve_date = fields.Datetime('COO Approve Date')
    coo_approve_by = fields.Many2one('res.users', 'COO Approve By')

    ceo_approve_date = fields.Datetime('CEO Approve Date')
    ceo_approve_by = fields.Many2one('res.users', 'CEO Approve By')

    state = fields.Selection(STATE_SELECTION, 'Status', readonly=True,
                             help="The status of the purchase order or the quotation request. "
                                  "A request for quotation is a purchase order in a 'Draft' status. "
                                  "Then the order has to be confirmed by the user, the status switch "
                                  "to 'Confirmed'. Then the supplier must confirm the order to change "
                                  "the status to 'Approved'. When the purchase order is paid and "
                                  "received, the status becomes 'Done'. If a cancel action occurs in "
                                  "the invoice or in the receipt of goods, the status becomes "
                                  "in exception.",
                             select=True, copy=False)

    # paid = fields.Boolean('Paid')
    # paid_amount = fields.Float('Paid Amount')
    # description = fields.Text('Description')
    # paid_date = fields.Datetime('Date')

    def category_head_approval(self):

        for order in self:
            try:
                order.write({
                    'state': 'category_head_approval_pending',
                    'category_head_approve_date': fields.datetime.now(),
                    'category_head_approve_by': self.user_id
                })

                order.order_line.write({
                    'state': 'category_head_approval_pending',
                    'category_head_approve_date': fields.datetime.now(),
                    'category_head_approve_by': self.user_id,
                })

                self.message_post(body=_("RFQ Approved By - %s", self.user_id.name))
            except:
                pass

        self.env['po.end.to.end'].po_data_create(po_number=self.id,
                                                 po_id=self.id,
                                                 value={'po_date_create': self.create_date,
                                                        'state': self.state},
                                                 )

        return True

    def po_confirm_order(self):

        range_value = self.env['po.approval.amount.range'].search([])

        po_amount = self.amount_total

        range_head = range_value.head_range1 <= po_amount <= range_value.head_range2
        range_vp = range_value.vp_range1 <= po_amount <= range_value.vp_range2
        range_coo = range_value.coo_range1 <= po_amount <= range_value.coo_range2
        range_ceo = range_value.ceo_range1 <= po_amount

        if range_head or str(self.client_order_ref).startswith("10000") :

            self.button_approve()

            self.message_post(body=_("RFQ Confirmed By - %s", self.user_id.name))

            self.env['po.end.to.end'].po_confirm_update(self.id, self.id, value={
                'category_head_approve_date': self.category_head_approve_date if self.category_head_approve_date else None,
                'category_head_approve_by': str(self.category_head_approve_by.name) if (
                    self.category_head_approve_by.name) else None,
                'category_vp_approve_date': self.category_vp_approve_date if self.category_vp_approve_date else None,
                'category_vp_approve_by': str(self.category_vp_approve_by.name) if (
                    self.category_vp_approve_by.name) else None,
                'coo_approve_date': self.coo_approve_date if self.coo_approve_date else None,
                'coo_approve_by': str(self.coo_approve_by.name) if self.coo_approve_by.name else None,
                'state': self.state})

        if range_vp:

            if str(self.state) == 'category_head_approval_pending':
                self.write({
                    'state': 'category_vp_approval_pending',
                    'category_vp_approve_date': fields.datetime.now(),
                    'category_vp_approve_by': self.user_id
                })

                self.order_line.write({
                    'state': 'category_vp_approval_pending',
                    'category_vp_approve_date': fields.datetime.now(),
                    'category_vp_approve_by': self.user_id,
                })

                self.message_post(body=_("RFQ Approved By - %s", self.user_id.name))

                self.env['po.end.to.end'].po_confirm_update(self.id, self.id, value={
                    'category_head_approve_date': self.category_head_approve_date if self.category_head_approve_date else None,
                    'category_head_approve_by': str(self.category_head_approve_by.name) if (
                        self.category_head_approve_by.name) else None,
                    'category_vp_approve_date': self.category_vp_approve_date if self.category_vp_approve_date else None,
                    'category_vp_approve_by': str(self.category_vp_approve_by.name) if (
                        self.category_vp_approve_by.name) else None,
                    'coo_approve_date': self.coo_approve_date if self.coo_approve_date else None,
                    'coo_approve_by': str(self.coo_approve_by.name) if self.coo_approve_by.name else None,
                    'state': self.state})

                return {
                    'type': 'ir.actions.client',
                    'tag': 'reload',
                }

            if str(self.state) == 'category_vp_approval_pending':

                self.button_approve()

                self.message_post(body=_("RFQ Confirmed By - %s", self.user_id.name))

                self.env['po.end.to.end'].po_confirm_update(self.id, self.id, value={
                    'category_head_approve_date': self.category_head_approve_date if self.category_head_approve_date else None,
                    'category_head_approve_by': str(self.category_head_approve_by.name) if (
                        self.category_head_approve_by.name) else None,
                    'category_vp_approve_date': self.category_vp_approve_date if self.category_vp_approve_date else None,
                    'category_vp_approve_by': str(self.category_vp_approve_by.name) if (
                        self.category_vp_approve_by.name) else None,
                    'coo_approve_date': self.coo_approve_date if self.coo_approve_date else None,
                    'coo_approve_by': str(self.coo_approve_by.name) if self.coo_approve_by.name else None,
                    'state': self.state})

        if range_coo:

            if str(self.state) == 'category_head_approval_pending':
                self.write({
                    'state': 'category_vp_approval_pending',
                    'category_vp_approve_date': fields.datetime.now(),
                    'category_vp_approve_by': self.user_id
                })

                self.order_line.write({
                    'state': 'category_vp_approval_pending',
                    'category_vp_approve_date': fields.datetime.now(),
                    'category_vp_approve_by': self.user_id,
                })

                self.message_post(body=_("RFQ Approved By - %s", self.user_id.name))

                self.env['po.end.to.end'].po_confirm_update(self.id, self.id, value={
                    'category_head_approve_date': self.category_head_approve_date if self.category_head_approve_date else None,
                    'category_head_approve_by': str(self.category_head_approve_by.name) if (
                        self.category_head_approve_by.name) else None,
                    'category_vp_approve_date': self.category_vp_approve_date if self.category_vp_approve_date else None,
                    'category_vp_approve_by': str(self.category_vp_approve_by.name) if (
                        self.category_vp_approve_by.name) else None,
                    'coo_approve_date': self.coo_approve_date if self.coo_approve_date else None,
                    'coo_approve_by': str(self.coo_approve_by.name) if self.coo_approve_by.name else None,
                    'state': self.state})

                return {
                    'type': 'ir.actions.client',
                    'tag': 'reload',
                }

            if str(self.state) == 'category_vp_approval_pending':
                self.write({
                    'state': 'coo_approval_pending',
                    'coo_approve_date': fields.datetime.now(),
                    'coo_approve_by': self.user_id
                })

                self.order_line.write({
                    'state': 'coo_approval_pending',
                    'coo_approve_date': fields.datetime.now(),
                    'coo_approve_by': self.user_id,
                })

                self.message_post(body=_("RFQ Approved By - %s", self.user_id.name))

                self.env['po.end.to.end'].po_confirm_update(self.id, self.id, value={
                    'category_head_approve_date': self.category_head_approve_date if self.category_head_approve_date else None,
                    'category_head_approve_by': str(self.category_head_approve_by.name) if (
                        self.category_head_approve_by.name) else None,
                    'category_vp_approve_date': self.category_vp_approve_date if self.category_vp_approve_date else None,
                    'category_vp_approve_by': str(self.category_vp_approve_by.name) if (
                        self.category_vp_approve_by.name) else None,
                    'coo_approve_date': self.coo_approve_date if self.coo_approve_date else None,
                    'coo_approve_by': str(self.coo_approve_by.name) if self.coo_approve_by.name else None,
                    'state': self.state})

                return {
                    'type': 'ir.actions.client',
                    'tag': 'reload',
                }

            if str(self.state) == 'coo_approval_pending':

                self.button_approve()

                self.message_post(body=_("RFQ Confirmed By - %s", self.user_id.name))

                self.env['po.end.to.end'].po_confirm_update(self.id, self.id, value={
                    'category_head_approve_date': self.category_head_approve_date if self.category_head_approve_date else None,
                    'category_head_approve_by': str(self.category_head_approve_by.name) if (
                        self.category_head_approve_by.name) else None,
                    'category_vp_approve_date': self.category_vp_approve_date if self.category_vp_approve_date else None,
                    'category_vp_approve_by': str(self.category_vp_approve_by.name) if (
                        self.category_vp_approve_by.name) else None,
                    'coo_approve_date': self.coo_approve_date if self.coo_approve_date else None,
                    'coo_approve_by': str(self.coo_approve_by.name) if self.coo_approve_by.name else None,
                    'state': self.state})

        return True


class PurchaseOrderLine(models.Model):
    _inherit = "purchase.order.line"

    STATE_SELECTION = [
        ('draft', 'RFQ'),
        ('sent', 'RFQ Sent'),

        ('category_head_approval_pending', 'Category Head Approval Pending'),
        ('category_vp_approval_pending', 'Category VP Approval Pending'),
        ('coo_approval_pending', 'COO Approval Pending'),
        ('ceo_approval_pending', 'CEO Approval Pending'),

        ('to approve', 'To Approve'),
        ('purchase', 'Purchase Order'),
        ('done', 'Locked'),
        ('cancel', 'Cancelled'),
    ]

    category_head_approve_date = fields.Datetime('Category Head Approve Date')
    category_head_approve_by = fields.Many2one('res.users', 'Category Head Approve By')

    category_vp_approve_date = fields.Datetime('Category VP Approve Date')
    category_vp_approve_by = fields.Many2one('res.users', 'Category VP Approve By')

    coo_approve_date = fields.Datetime('COO Approve Date')
    coo_approve_by = fields.Many2one('res.users', 'COO Approve By')

    ceo_approve_date = fields.Datetime('CEO Approve Date')
    ceo_approve_by = fields.Many2one('res.users', 'CEO Approve By')

    state = fields.Selection(STATE_SELECTION, 'Status', readonly=True,
                             help="The status of the purchase order or the quotation request. "
                                  "A request for quotation is a purchase order in a 'Draft' status. "
                                  "Then the order has to be confirmed by the user, the status switch "
                                  "to 'Confirmed'. Then the supplier must confirm the order to change "
                                  "the status to 'Approved'. When the purchase order is paid and "
                                  "received, the status becomes 'Done'. If a cancel action occurs in "
                                  "the invoice or in the receipt of goods, the status becomes "
                                  "in exception.",
                             select=True, copy=False)


# class AdvancePOPaid(models.Model):
#     _name = "advance.po.paid"
#
#     paid = fields.Boolean('Paid')
#     paid_amount = fields.Float('Paid Amount', compute='_get_po_amount', store=True)
#     description = fields.Text('Paid Description')
#     paid_date = fields.Datetime(string='Paid Date', default=fields.Datetime.now())
#     purchase_id = fields.Many2one('purchase.order', 'Purchase ID')
#
#     @api.depends('purchase_id')
#     def _get_po_amount(self):
#         purchase_obj = self.env['purchase.order']
#         po_obj = purchase_obj.browse(self.env.context.get('active_id'))
#         po_amount = po_obj.amount_total if po_obj.amount_total else 0
#
#         return po_amount
#
#     def po_paid_def(self):
#         purchase_obj = self.env['purchase.order']
#
#         purchase_obj.write(self.env.context['active_id'])
#
#         return True
