import datetime
from odoo import _,api, fields, models
from odoo.exceptions import UserError, ValidationError
from odoo.osv import expression


class ConnectorOrderMapping(models.Model):
    _inherit = "connector.order.mapping"

    @api.model
    def create(self, vals):
        warehouse_dict ={}
        try:
            found_retail = False
            odoo_order_id = vals['odoo_order_id']
            order_info = self.env['sale.order'].search([('id','=',odoo_order_id)])

            partner_classification = str(order_info.partner_id.x_classification)

            retail_string = 'Retail'
            horeca_string = 'HORECA'
            general_string = 'General'
            ananta_string = 'Ananta'
            corporate_string = 'Corporate'
            warehouse = self.env['stock.warehouse']

            if order_info.x_delivery_slot == '' or order_info.x_delivery_slot == False:

                warehouse_info = warehouse.get_warehouse_id('UttWH')

                wh_id= warehouse_info.id

                if retail_string.lower() not in partner_classification.lower():
                    # if horeca_string.lower() not in partner_classification.lower():

                        wh_id = warehouse.get_warehouse_id('BddWH').id

                ## For Signboard, Comilla Warehouse selection Code , it work on delivery address
                # The following code for Area wise Warhouse change Auto
                  # Here key is the Narayanganj warehouse ID

                if retail_string.lower() in partner_classification.lower():
                    wh_area = self.env['warehouse.area']
                    wh_area_browse = wh_area.search([('code', '=', 'SinWH')])
                    # badda_wh_area_browse = wh_area.search([('code', '=', 'BddWH')])
                    savar_wh_area_browse = wh_area.search([('code', '=', 'SavWH')])

                    areas = wh_area_browse.areas
                    # badda_areas = badda_wh_area_browse.areas
                    savar_areas = savar_wh_area_browse.areas

                    signboard_area_list = areas.split(',')
                    signboard_list = [a.lstrip() for a in signboard_area_list]

                    # badda_area_list = badda_areas.split(',')
                    # badda_list = [b.lstrip() for b in badda_area_list]

                    savar_area_list = savar_areas.split(',')
                    savar_list = [sav.lstrip() for sav in savar_area_list]

                    sin_wh_id = warehouse.get_warehouse_id('SinWH').id
                    # badda_wh_id = warehouse.get_warehouse_id('BddWH').id
                    savar_wh_id = warehouse.get_warehouse_id('SavWH').id

                    warehouse_dict[sin_wh_id] = signboard_list
                    # warehouse_dict[badda_wh_id] = badda_list
                    warehouse_dict[savar_wh_id] = savar_list
                    try:
                        for k, v in warehouse_dict.items():

                            city_name = order_info.partner_shipping_id.city
                            state_name = order_info.partner_shipping_id.state_id.name

                            for item in v:
                                if state_name.upper() == item.upper():
                                    wh_id = k
                                    break

                            # if 'narayanganj' == city_name.lower() or 'narayanganj' in state_name.lower():
                            #     wh_id = sin_wh_id  ## In live Narayanganj wh id
                            #
                            # if 'comilla' == city_name.lower() or 'comilla' in state_name.lower():
                            #     wh_id = sin_wh_id  ## In live Comilla wh id

                    except:
                        city_name = ''
                        state_name = ''

                ## horeca
                if horeca_string.lower() in partner_classification.lower():
                    wh_id = warehouse_info.id

                ## General
                if general_string.lower() in partner_classification.lower():
                    wh_id = warehouse_info.id
                    ## Ends Here

                if wh_id is not None:
                    update_so_query_area = "UPDATE sale_order SET warehouse_id='{0}' WHERE id='{1}'".format(
                        wh_id, str(odoo_order_id))
                    self.env.cr.execute(update_so_query_area)
                    self.env.cr.commit()

            ### Ends here area wise warehouse selection

            if vals.get('odoo_order_id') and order_info.x_prepaid == False and found_retail == False:

                cust_so_count_query = "SELECT COUNT(id) FROM sale_order WHERE partner_id='{0}'".format(str(order_info.partner_id.id))
                self.env.cr.execute(cust_so_count_query)
                cust_so_count = 0
                for c in self.env.cr.fetchall():
                    cust_so_count = int(c[0])

                credit_limit_exceed = False
                state = 'draft'
                if cust_so_count > 1:
                    new_customer = False
                else:
                    new_customer = True

                advance_paid_or_cash_on_delivery = False

                if 'PAID' in order_info.note or 'Cash On Delivery' in order_info.note or 'Credit/Debit Cards' in order_info.note or 'Mobile Banking' in order_info.note:
                    advance_paid_or_cash_on_delivery=True

                # if customer_info.credit_limit > 0 and advance_paid_or_cash_on_delivery == False:
                if advance_paid_or_cash_on_delivery == False and corporate_string.lower() in partner_classification.lower():
                    # credit_limit_exceed = True
                    if order_info.partner_id.parent_id == True:
                        company_id = order_info.partner_id.parent_id.id
                        customer_info = order_info.partner_id.parent_id
                    else:
                        company_id = order_info.partner_id.id
                        customer_info = order_info.partner_id

                    total_receivable_amount = order_info.credit + order_info.amount_total

                    if customer_info.credit_limit == 0 and customer_info.credit_days == 0 and \
                                    "Corporate" in str(customer_info.x_classification):
                        credit_limit_exceed = True

                    if customer_info.credit_limit > 0:

                        if total_receivable_amount > order_info.credit_limit:
                            credit_limit_exceed = True

                        # if order_info.x_credit:
                        #     customer_info = self.env['res.partner'].search(id company_id])

                    credit_days_exceed = self.check_credit_days_exceed(customer_info)
                    if credit_days_exceed:
                        credit_limit_exceed = True

                    state = 'approval_pending'

                # if new_customer and state == 'approval_pending':
                #     state = 'approval_pending'
                if new_customer:
                    state = 'new_cus_approval_pending'

                if ananta_string.lower() in partner_classification.lower():
                    state = 'approval_pending'
                    # credit_limit_exceed = True

                update_so_query = "UPDATE sale_order SET new_customer='{0}', credit_limit_exceed='{1}', state='{2}' WHERE id='{3}'".format(new_customer, credit_limit_exceed, state, str(odoo_order_id))
                self.env.cr.execute(update_so_query)
                self.env.cr.commit()
        except:
            pass
        # ----------------------------------------------

        return super(ConnectorOrderMapping, self).create(vals)

    def check_credit_days_exceed(self, parent_id):

        credit_days_exceed = False

        try:

            first_inv_date = ''
            first_inv_date_query = "SELECT delivery_date FROM account_move WHERE commercial_partner_id = {0} and " \
                                   "state='posted' and delivered = TRUE ORDER BY delivery_date LIMIT 1".format(parent_id.id)

            self.env.cr.execute(first_inv_date_query)
            for fiv in self.env.cr.fetchall():
                first_inv_date = str(fiv[0])

            parent_customer_info = parent_id
            credit_days = int(parent_customer_info.credit_days)

            today_date = datetime.date.today()

            date_1 = datetime.datetime.strptime(first_inv_date, '%Y-%m-%d')
            limit_date = date_1 + datetime.timedelta(days=credit_days)

            if today_date > limit_date.date():
                credit_days_exceed = True
            else:
                credit_days_exceed = False
        except:
            pass

        return credit_days_exceed


class SaleOrder(models.Model):
    _inherit = 'sale.order'

    credit_limit_exceed = fields.Boolean('Credit Limit Exceed')
    new_customer = fields.Boolean('New Customer')
    new_customer_approve_date = fields.Datetime('New Customer Approve Date')
    new_customer_approve_by =  fields.Many2one('res.users', 'New Customer Approve By')
    new_customer_approve_reason = fields.Text('New Customer Approve Reason')
    new_customer_cancel_date = fields.Datetime('New Customer Cancel Date')
    new_customer_cancel_by = fields.Many2one('res.users', 'New Customer Cancel By')
    new_customer_cancel_reason = fields.Text('New Customer Cancel Reason')
    cl_approve_date = fields.Datetime('CL Approve Date')
    cl_approve_by = fields.Many2one('res.users', 'CL Approve By')
    cl_approve_reason = fields.Text('CL Approve Reason')

    check_pending_approve_date = fields.Datetime('CL Pending Approve Date')
    check_pending_approve_by = fields.Many2one('res.users', 'CL Pending Approve By')
    check_pending_approve_reason = fields.Text('CL Approve Reason')
    check_pending_cancel_date = fields.Datetime('CL Pending Cancel Date')
    check_pending_cancel_by = fields.Many2one('res.users', 'CL Pending Cancel By')
    check_pending_cancel_reason = fields.Text('CL Cancel Reason')

    cl_cancel_date = fields.Datetime('CL Cancel Date')
    cl_cancel_by = fields.Many2one('res.users', 'CL Cancel By')
    cl_cancel_reason = fields.Text('CL Cancel Reason')
    credit_days_exceed = fields.Boolean('Credit Days Exceed')
    state = fields.Selection([
        ('draft', 'Quotation'),
        ('sent', 'Quotation Sent'),
        ('sale', 'Sales Order'),
        ('done', 'Locked'),
        ('cancel', 'Cancelled'),
        ('approval_pending', 'Approval Pending'),
        ('check_pending', 'Check Pending'),
        ('ceo_check_pending', 'CEO Check Pending'),
        ('new_cus_approval_pending', 'New Customer Approval Pending'),
        ], 'Status', readonly=True, copy=False, help="Gives the status of the quotation or sales order.\
                  \nThe exception status is automatically set when a cancel operation occurs \
                  in the invoice validation (Invoice Exception) or in the picking list process (Shipping Exception).\nThe 'Waiting Schedule' status is set when the invoice is confirmed\
                   but waiting for the scheduler to run on the order date.", select=True)
    credit = fields.Float(compute='_compute_credit', string='Total Receivable')
    credit_days = fields.Integer(compute='_compute_credit_day', string='Credit Days')
    credit_limit = fields.Float(compute='_compute_credit_limit', string='Credit Limit')

    def _compute_credit_day(self):
        for sale in self:
            sale.credit_days = sale.partner_id.parent_id.credit_days if sale.partner_id.parent_id else sale.partner_id.parent_id.credit_days

    def _compute_credit_limit(self):
        for sale in self:
            sale.credit_limit = sale.partner_id.parent_id.credit_limit if sale.partner_id.parent_id else sale.partner_id.parent_id.credit_limit

    def _compute_credit(self):
        for sale in self:
            sale.credit = sale.partner_id.parent_id.credit if sale.partner_id.parent_id else sale.partner_id.parent_id.credit


# ------------------- CREDIT LIMIT EXCEED ---------------------
class ApproveLimitExceed(models.Model):
    _name = "approve.limit.exceed"
    _description = "Approve Limit Exceed"

    approve_date = fields.Datetime('CL Approve Date', default=fields.Datetime.now)
    approve_by = fields.Many2one('res.users', 'CL Approve By')
    approve_reason = fields.Text('CL Approve Reason')

    # Approve CL
    def so_approve_limit_exceed(self):
        approve_date = self.env.context['approve_date']
        approve_by = self.env.uid
        approve_reason = self.env.context['approve_reason']
        ids = self.env.context['active_ids']  # [15526]
        try:

            for id in ids:
                sale = self.env['sale.order'].browse(id)
                if sale.state == 'ceo_check_pending':
                    approve_query = "UPDATE sale_order SET state='draft' WHERE id='{0}'".format(id)
                    self.env.cr.execute(approve_query)
                    self.env.cr.commit()
                else:
                    state = 'check_pending'
                    approve_query = "UPDATE sale_order SET cl_approve_date='{0}', cl_approve_by='{1}', cl_approve_reason='{2}', state='{4}' WHERE id='{3}'".format(
                        approve_date, approve_by, approve_reason, id, state)
                    self.env.cr.execute(approve_query)
                    self.env.cr.commit()
                    try:
                        data_range = self.env['so.approval.amount.range'].search([('id','=',1)])
                        if float(sale.amount_total) < float(data_range.head_range2):
                            approve_query = "UPDATE sale_order SET state='draft' WHERE id='{0}'".format(id)
                            self.env.cr.execute(approve_query)
                            self.env.cr.commit()
                    except:
                        pass

                if sale.credit_limit_exceed:
                    approve_query = "UPDATE sale_order SET cl_approve_date='{0}', cl_approve_by='{1}', cl_approve_reason='{2}', state='check_pending' WHERE id='{3}'".format(
                        approve_date, approve_by, approve_reason, id)
                    self.env.cr.execute(approve_query)
                    self.env.cr.commit()
        except:
            pass

        return True


class CancelLimitExceed(models.Model):
    _name = "cancel.limit.exceed"
    _description = "Cancel Limit Exceed"

    cancel_date = fields.Datetime('CL Cancel Date', default=fields.Datetime.now)
    cancel_by = fields.Many2one('res.users', 'CL Cancel By')
    cancel_reason = fields.Selection([
                        ("product_unavailability","Product unavailability"),
                        ("delay_delivery","Delay delivery"),
                        ("pricing_issue","Pricing issue"),
                        ("wrong_order","Wrong order"),
                        ("cash_problem","Cash problem"),
                        ("damage_product","Damage product"),
                        ("double_ordered","Double ordered"),
                        ("sample_not_approved","Sample not approved"),
                        ("wrong_sku","Wrong SKU"),
                        ("not_interested","Not interested"),
                        ("wrong_product","Wrong product"),
                        ("quality_issue","Quality issue"),
                        ("test_order","Test Order"),
                        ("payment_not_done","Payment not done"),
                        ("apnd","Advance payment not done"),
                        ("invalid_information","Invalid Information"),
                        ("customer_unreachable","Customer unreachable"),
                        ("same_day_delivery","Same day delivery"),
                        ("fake_order","Fake Order"),
                        ("sourcing_delay","Sourcing Delay"),
                        ("reorder","Reorder"),
                        ("discount_issue","Discount Issue"),
                        ("product_not_available_customer_will_take_alternative_product", "Product not available, Customer will take alternative product"),


                   ], 'CL Cancel Reason', readonly=True, copy=False, help="Reasons", select=True)


    def so_cancel_limit_exceed(self):

        cancel_date = self.env.context['cancel_date']
        cancel_by = self.env.uid
        cancel_reason = self.env.context['cancel_reason']
        ids = self.env.context['active_ids']

        for id in ids:

            try:
                if cancel_reason:

                    cancel_approve_query = "UPDATE sale_order SET cl_cancel_date='{0}', cl_cancel_by='{1}', cl_cancel_reason='{2}' WHERE id='{3}'".format(cancel_date, cancel_by, cancel_reason, id)
                    self.env.cr.execute(cancel_approve_query)
                    self.env.cr.commit()
                    self.env['sale.order'].browse(id).action_cancel()
                # else:
                #     raise except_orm('Cancel Reason', 'Can not submit blank reason!!!')

            except:
                pass

        return True


class SOApprovalAmountRange(models.Model):

    _name = "so.approval.amount.range"

    head_range1 = fields.Integer('Head of Sales Ranges From')
    head_range2 = fields.Integer('Head of Sales Ranges To')
    cfo_range1 = fields.Integer('CFO Ranges From')
    cfo_range2 = fields.Integer('CFO Ranges To')
    ceo_range1 = fields.Integer('CEO Ranges From')
    ceo_range2 = fields.Integer('CEO Ranges To')


# ------------------- CREDIT LIMIT EXCEED CHECK PENDING ---------------------
class ApproveLimitExceedCheckPending(models.Model):
    _name = "approve.limit.exceed.check.pending"
    _description = "Approve Limit Exceed Check Pending"

    approve_date = fields.Datetime('CL Approve Date', default=fields.Datetime.now)
    approve_by = fields.Many2one('res.users', 'CL Approve By')
    approve_reason = fields.Text('CL Approve Reason')

    def so_approve_limit_exceed_check_pending(self):
        approve_date = self.env.context['approve_date']
        approve_by = self.env.uid
        approve_reason = self.env.context['approve_reason']
        ids = self.env.context['active_ids']  # [15526]

        for id in ids:
            try:
                data_range = self.env['so.approval.amount.range'].search([('id','=',1)])

                sale = self.env['sale.order'].browse(id)
                nxt_state='draft'

                if float(sale.amount_total) > float(data_range.cfo_range2):
                    if sale.state =='ceo_check_pending':
                        nxt_state='draft'
                    else:
                        nxt_state = 'ceo_check_pending'

                approve_query = "UPDATE sale_order SET check_pending_approve_date='{0}', check_pending_approve_by='{1}', check_pending_approve_reason='{2}', state='{3}' WHERE id='{4}'".format(approve_date, approve_by, approve_reason,nxt_state, id)
                self.env.cr.execute(approve_query)
                self.env.cr.commit()

            except:

                pass

        return True


class CancelLimitExceedCheckPending(models.Model):
    _name = "cancel.limit.exceed.check.pending"
    _description = "Cancel Limit Exceed Check Pending"


    cancel_date = fields.Datetime('CL Cancel Date', default=fields.Datetime.now)
    cancel_by = fields.Many2one('res.users', 'CL Cancel By')
    cancel_reason = fields.Selection([
                        ("product_unavailability","Product unavailability"),
                        ("delay_delivery","Delay delivery"),
                        ("pricing_issue","Pricing issue"),
                        ("wrong_order","Wrong order"),
                        ("cash_problem","Cash problem"),
                        ("damage_product","Damage product"),
                        ("double_ordered","Double ordered"),
                        ("sample_not_approved","Sample not approved"),
                        ("wrong_sku","Wrong SKU"),
                        ("not_interested","Not interested"),
                        ("wrong_product","Wrong product"),
                        ("quality_issue","Quality issue"),
                        ("test_order","Test Order"),
                        ("payment_not_done","Payment not done"),
                        ("apnd","Advance payment not done"),
                        ("invalid_information","Invalid Information"),
                        ("customer_unreachable","Customer unreachable"),
                        ("same_day_delivery","Same day delivery"),
                        ("fake_order","Fake Order"),
                        ("sourcing_delay","Sourcing Delay"),
                        ("reorder","Reorder"),
                        ("discount_issue","Discount Issue"),
                        ("product_not_available_customer_will_take_alternative_product", "Product not available, Customer will take alternative product"),
                   ], 'CL Cancel Reason', readonly=True, copy=False, help="Reasons", select=True)


    def so_cancel_limit_exceed_check_pending(self):

        cancel_date = self.env.context['cancel_date']
        cancel_by = self.env.uid
        cancel_reason = self.env.context['cancel_reason']
        ids = self.env.context['active_ids']

        for id in ids:

            try:

                if cancel_reason:
                    """
                    'check_pending_cancel_date': fields.datetime('CL Pending Cancel Date'),
                    'check_pending_cancel_by': fields.many2one('res.users', 'CL Pending Cancel By'),
                    'check_pending_cancel_reason': fields.text('CL Cancel Reason'),
                    """
                    cancel_query = "UPDATE sale_order SET check_pending_cancel_date='{0}', check_pending_cancel_by='{1}', check_pending_cancel_reason='{2}' WHERE id='{3}'".format(
                        cancel_date, cancel_by, cancel_reason, id)
                    self.env.cr.execute(cancel_query)
                    self.env.cr.commit()
                    self.env['sale.order'].browse(id).action_cancel()
                else:
                    raise UserError(_('Cancel Reason.Can not submit blank reason!!!'))

            except:

                pass

        return True


# ------------------- NEW CUSTOMER ---------------------
class ApproveNewCustomer(models.Model):
    _name = "approve.new.customer"
    _description = "Approve New Customer"

    approve_date = fields.Datetime('New Customer Approve Date', default=fields.Datetime.now)
    approve_by = fields.Many2one('res.users', 'New Customer Approve By')
    approve_reason = fields.Text('New Customer Approve Reason')

    def so_approve_new_customer(self):

        approve_date = self.env.context['approve_date']
        approve_by = self.env.uid
        approve_reason = self.env.context['approve_reason']
        ids = self.env.context['active_ids']  # [15526]

        for id in ids:

            try:

                cancel_approve_query = "UPDATE sale_order SET new_customer_approve_date='{0}', new_customer_approve_by='{1}', new_customer_approve_reason='{2}', state='draft' WHERE id='{3}'".format(approve_date, approve_by, approve_reason, id)
                self.env.cr.execute(cancel_approve_query)
                self.env.cr.commit()

            except:
                pass

        return True


class CancelNewCustomer(models.Model):
    _name = "cancel.new.customer"
    _description = "Cancel New Customer"

    cancel_date = fields.Datetime('CL Cancel Date', default=fields.Datetime.now)
    cancel_by = fields.Many2one('res.users', 'CL Cancel By')
    cancel_reason =fields.Selection([

                    ("product_unavailability","Product unavailability"),
                    ("delay_delivery","Delay delivery"),
                    ("pricing_issue","Pricing issue"),
                    ("wrong_order","Wrong order"),
                    ("cash_problem","Cash problem"),
                    ("damage_product","Damage product"),
                    ("double_ordered","Double ordered"),
                    ("sample_not_approved","Sample not approved"),
                    ("wrong_sku","Wrong SKU"),
                    ("not_interested","Not interested"),
                    ("wrong_product","Wrong product"),
                    ("quality_issue","Quality issue"),
                    ("test_order","Test Order"),
                    ("payment_not_done","Payment not done"),
                    ("apnd","Advance payment not done"),
                    ("invalid_information","Invalid Information"),
                    ("customer_unreachable","Customer unreachable"),
                    ("same_day_delivery","Same day delivery"),
                    ("fake_order","Fake Order"),
                    ("sourcing_delay","Sourcing Delay"),
                    ("reorder","Reorder"),
                    ("discount_issue","Discount Issue"),
                    ("product_not_available_customer_will_take_alternative_product", "Product not available, Customer will take alternative product"),
               ], 'CL Cancel Reason', readonly=True, copy=False, help="Reasons", select=True)

    def so_cancel_new_customer(self):

        cancel_date = self.env.context['cancel_date']
        cancel_by = self.env.uid
        cancel_reason = self.env.context['cancel_reason']
        ids = self.env.context['active_ids']

        for id in ids:

            try:
                if cancel_reason:

                    cancel_approve_query = "UPDATE sale_order SET new_customer_cancel_date='{0}', new_customer_cancel_by='{1}', new_customer_cancel_reason='{2}' WHERE id='{3}'".format(cancel_date, cancel_by, cancel_reason, id)
                    self.env.cr.execute(cancel_approve_query)
                    self.env.cr.commit()
                    self.env['sale.order'].browse(id).action_cancel()
                else:
                    raise UserError(_('Cancel Reason . Can not submit blank reason!!!'))

            except:

                pass

        return True


class StockWarehouse(models.Model):
    _inherit = 'stock.warehouse'

    def get_warehouse_id(self,code):

        warehouse_info = self.search([('code', '=', code)])
        wh_id = warehouse_info

        return wh_id
