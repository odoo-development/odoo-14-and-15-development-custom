{
    "name": "payslip detailes with festival bouns",
    "version": "15.0.0",
    "author": "Ashif",
    "category": "Human Resources",
    "license": "AGPL-3",
    "depends": [
        'bi_hr_payroll',
        'hr_contract'
    ],
    "data": [

        "report/payslip_detailes_layout.xml",
        "views/payslip_report_detailes_menu.xml",
        "views/payslip_report_detailes.xml",
    ],
    "installable": True,
    "auto_install": False,
}