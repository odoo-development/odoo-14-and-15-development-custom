from odoo import api, models, _
from datetime import date


class payslipdata(models.AbstractModel):
    _name = 'report.payslip_detailes.report_payslip_detailes_layout'
    _description = 'payslip report'

    @api.model
    def _get_report_values(self, docids, data=None):
        docs = self.env['hr.payslip.run'].browse(docids[0])

        return {
            'doc_ids': docids,
            'doc_model': 'hr.payslip.run',
            'docs': docs,
            'data': data,
            'get_information': self.get_information
        }

    def get_information(self, obj):

        result = []
        ids = []
        for id in range(len(obj)):
            ids.append(obj.id)
        if obj.id:

            self.env.cr.execute(
    '''select hr_payslip.name,hr_payslip.employee_id,hr_contract.name as emp_name,
    hr_contract.date_start as emp_joining_date, hr_contract.employee_identification_code,hr_contract.gross_salary, 
    hr_contract.cash_allowance, hr_payslip.fest_bonus_sin, (select hr_department.name from hr_department 
    where hr_contract.department_id = hr_department.id) as department, (select hr_job.name from hr_job 
    where hr_job.id = hr_contract.job_id) as deg, (select res_partner_bank.acc_number from res_partner_bank 
    where hr_employee.bank_account_id = res_partner_bank.id) as Acc_number from hr_payslip,hr_contract,hr_employee 
    where payslip_run_id = %s and hr_payslip.employee_id = hr_contract.employee_id 
    and hr_contract.employee_id = hr_employee.id order by department''',(obj.id,))

            objects_dict = {}
            emp_ids = []

            import itertools
            object = self.env.cr.dictfetchall()
            objects = sorted(object, key=lambda i: i['employee_id'])
            an_iterator = itertools.groupby(objects, lambda y: y['employee_id'])

            for key, group in an_iterator:
                key_and_group = {key: list(group)}
                objects_dict.update(key_and_group)
                emp_ids.append(key)

            for val in emp_ids:
                data = {}
                for x in objects_dict[val]:
                    cash_allowance = x['cash_allowance'] if x['cash_allowance'] else 0

                    data.update({
                        'acc_number': x['acc_number'],
                        'department': x['department'],
                        'deg': x['deg'],
                        'emp_joining_date': x['emp_joining_date'].strftime("%d-%m-%Y"),
                        'emp_name': x['emp_name'],
                        'employee_id': x['employee_id'],
                        'employee_identification_code': x['employee_identification_code'],
                        'gross': x['gross_salary'],
                        'cash_allowance': x['cash_allowance'],
                        'net_salary' : x['gross_salary'] + cash_allowance,
                        'fest_bonus_sin':x['fest_bonus_sin']
                    })


                result.append(data)
        final_data = sorted(result, key=lambda i: i['department'])

        return final_data
