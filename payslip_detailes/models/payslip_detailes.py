from odoo import models, fields, api, _
from odoo.tools.translate import _
from odoo.exceptions import UserError, ValidationError, Warning

class PayslipRun(models.Model):
    _inherit = "hr.payslip.run"


    def payslip_detailes_report(self):
        return self.env.ref("payslip_detailes.action_payslip_report").report_action(self)
