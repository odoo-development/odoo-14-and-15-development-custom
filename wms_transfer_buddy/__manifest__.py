{
    "name": "WMS Transfer Buddy",
    "version": "14.0.1.0.0",
    "category": "WMS Transfer Buddy",
    "author": "Odoo Bangladesh",
    "summary": "WMS, Inventory, Logistic, Storage",
    "description": "WMS Transfer Buddy",
    "depends": ["stock"],
    "installable": True,
    "application": True,
    "auto_install": False,
}
