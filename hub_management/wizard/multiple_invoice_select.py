import json
from odoo import api, fields, models, _
from odoo.tools.translate import _
from datetime import date, datetime
from odoo.exceptions import UserError, ValidationError


class MultipleInvoiceSelectAccept(models.TransientModel):
    _name = "multiple.invoice.select.accept"
    _description = "Multiple Invoice Select For Accept"

    date_accept = fields.Datetime('Date', required=True, readonly=True, index=True, copy=False,  default=fields.Datetime.now)
    description = fields.Char('Reference/Description')

    def accept_hub_invoice(self):
        data = dict()
        data['description'] = self.description
        self.env.cr.execute("UPDATE hub_wise_invoice SET description =%s,state='accepted' WHERE id IN %s", (data['description'],tuple(self.env.context.get('active_ids'))))
        self.env.cr.commit()


class MultipleInvoiceSelectShipped(models.TransientModel):
    _name = "multiple.invoice.select.shipped"
    _description = "Multiple Invoice Select For Shipped"

    date_shipped = fields.Datetime('Date', required=True, readonly=True, index=True, copy=False,  default=fields.Datetime.now)
    description = fields.Char('Reference/Description')
    delivery_man = fields.Many2one('delivery.man.info',string='Delivery Man')
    delivery_vehicle_number = fields.Char(string='Vehicle Number')
    user_id = fields.Many2one('res.users', 'Assigned to', index=True)

    def default_get(self, fields):

        res = super(MultipleInvoiceSelectShipped, self).default_get(fields)

        for items in self.env['hub.wise.invoice'].browse(self.env.context.get('active_ids')):
            if items.state != 'accepted':
                raise UserError(_("First accept the order."))

        return res

    def shipped_hub_invoice(self):
        data = dict()
        data['delivery_man'] = self.delivery_man.id
        data['delivery_vehicle_number'] = self.delivery_vehicle_number
        data['date_shipped'] = self.date_shipped
        data['description'] = self.description
        self.env.cr.execute("UPDATE hub_wise_invoice SET delivery_man = %s,"
                            "delivery_vehicle_number = %s,description =%s,state='shipped' WHERE id IN %s", (data['delivery_man'],data['delivery_vehicle_number'],data['description'],tuple(self.env.context.get('active_ids'))))
        self.env.cr.commit()


class MultipleInvoiceSelectReject(models.TransientModel):
    _name = "multiple.invoice.select.reject"
    _description = "Multiple Invoice Select For Reject"

    date_reject = fields.Datetime('Date', required=True, readonly=True, index=True, copy=False,  default=fields.Datetime.now)
    description = fields.Char('Reference/Description')
    return_delivery_man = fields.Many2one('delivery.man.info',string='Return Delivery Man')
    return_vehicle_number = fields.Char(string='Retn. Vehicle Number')
    reject_reason = fields.Char(string='Reject Reason')
    user_id = fields.Many2one('res.users', 'Assigned to', index=True)

    def default_get(self, fields):

        res = super(MultipleInvoiceSelectReject, self).default_get(fields)

        for items in self.env['hub.wise.invoice'].browse(self.env.context.get('active_ids')):
            if items.state != 'draft':
                raise UserError(_("Only Draft invoice can be rejected"))

        return res

    def reject_hub_invoice(self):
        data = dict()
        data['return_delivery_man'] = self.return_delivery_man.id
        data['return_vehicle_number'] = self.return_vehicle_number
        data['date_reject'] = self.date_reject
        data['description'] = self.description
        data['reject_reason'] = self.reject_reason
        try:
            self.env.cr.execute("SELECT manifest_line_id FROM hub_wise_invoice WHERE id IN %s",[tuple(self.env.context.get('active_ids'))])
            manifest_line_id_list = self.env.cr.fetchall()

            for items in manifest_line_id_list:
                self.env["wms.manifest.line"].reschedule_invoice_delivery(items[0])

            self.env.cr.execute("UPDATE hub_wise_invoice SET date_reject = %s,return_delivery_man = %s,"
                                "return_vehicle_number = %s,description = %s,reject_reason = %s,state='rejected',"
                                "rescheduled= TRUE WHERE id IN %s",(data['date_reject'],data['return_delivery_man'],
                                                  data['return_vehicle_number'],data['description'],
                                                  data['reject_reason'],
                                                  tuple(self.env.context.get('active_ids'))))
            self.env.cr.commit()

        except:
            raise UserError(_("Something went wrong. Please talk with administration"))


class MultipleInvoiceSelectDelivered(models.TransientModel):
    _name = "multiple.invoice.select.delivered"
    _description = "Multiple Invoice Select For Delivered"

    description = fields.Char('Reference/Description')
    delivery_date = fields.Date('Delivery Date', default=fields.Datetime.now())
    deliver_time = fields.Selection([
        ('07 AM - 08 AM', '07 AM - 08 AM'),
        ('08 AM - 09 AM', '08 AM - 09 AM'),
        ('09 AM - 10 AM', '09 AM - 10 AM'),
        ('10 AM - 11 AM', '10 AM - 11 AM'),
        ('11 AM - 12 PM', '11 AM - 12 PM'),
        ('12 PM - 01 PM', '12 PM - 01 PM'),
        ('01 PM - 02 PM', '01 PM - 02 PM'),
        ('02 PM - 03 PM', '02 PM - 03 PM'),
        ('03 PM - 04 PM', '03 PM - 04 PM'),
        ('04 PM - 05 PM', '04 PM - 05 PM'),
        ('05 PM - 06 PM', '05 PM - 06 PM'),
        ('06 PM - 07 PM', '06 PM - 07 PM'),
        ('07 PM - 08 PM', '07 PM - 08 PM'),
        ('08 PM - 09 PM', '08 PM - 09 PM'),
        ('09 PM - 10 PM', '09 PM - 10 PM'),
    ], 'Deliver Time', default='10 AM - 11 AM', index=True)

    def default_get(self, fields):

        res = super(MultipleInvoiceSelectDelivered, self).default_get(fields)

        for items in self.env['hub.wise.invoice'].browse(self.env.context.get('active_ids')):
            if items.state != 'shipped':
                raise UserError(_("First shipped the order."))

        return res

    def delivered_hub_invoice(self):
        context = dict(self.env.context)
        context['delivery_date'] = self.delivery_date
        context['delivery_time'] = self.deliver_time
        context['source'] = self.env.context.get('source') if self.env.context.get('source',False) else 'Web'
        try:
            for items in self.env['hub.wise.invoice'].browse(self.env.context.get('active_ids')):
                if items.state =='shipped':
                    items.manifest_line_id.with_context(context).action_invoice_full_delivered()

                    self.env.cr.execute("UPDATE hub_wise_invoice SET description = %s,state='fully_delivered',delivery_amount=%s WHERE id = %s",(self.description,items.invoice_amount,items.id))
                    self.env.cr.commit()
                else:
                    raise UserError(_("First shipped the order."))
        except:
            raise UserError(_("Something went wrong. Please talk with administration"))


class MultipleInvoiceSelectReschedule(models.TransientModel):
    _name = "multiple.invoice.select.reschedule"
    _description = "Multiple Invoice Select For Reschedule"

    date_reschedule = fields.Datetime('Date', required=True, readonly=True, index=True, copy=False,  default=fields.Datetime.now)
    description = fields.Char('Reference/Description')
    return_delivery_man = fields.Many2one('delivery.man.info',string='Return Delivery Man')
    return_vehicle_number = fields.Char(string='Retn. Vehicle Number')
    ndr = fields.Selection([
        ('cash_problems', 'Cash Problems'),
        ('customer_did_not_ordered', 'Customer Did Not Ordered'),
        ('customer_not_interested', 'Customer Not Interested'),
        ('customer_not_reachable', 'Customer Not Reachable'),
        ('customer_wants_full_delivery', 'Customer Wants Full Delivery'),
        ('customer_wants_high_quality', 'Customer Wants High Quality'),
        ('customer_wants_low_quality', 'Customer Wants Low Quality'),
        ('fake_order', 'Fake Order'),
        ('late_delivery', 'Late Delivery'),
        ('less_qty_required', 'Less Qty Required'),
        ('more_qty_required', 'More Qty Required'),
        ('office_closed', 'Office Closed'),
        ('pack_size_issue', 'Pack Size Issue'),
        ('price_issue', 'Price Issue'),
        ('product_damaged', 'Product Damaged'),
        ('product_mismatch', 'Product Mismatch'),
        ('returned_due_to_other_products', 'Returned Due to Other Products'),
        ('wrong_address', 'Wrong Address'),
        ('wrong_order_placed', 'Wrong Order Placed'),
        ('wrongly_packed', 'Wrongly Packed'),
    ], 'Not Delivery Reason', index=True)

    def default_get(self, fields):

        res = super(MultipleInvoiceSelectReschedule, self).default_get(fields)

        for items in self.env['hub.wise.invoice'].browse(self.env.context.get('active_ids')):
            if items.state != 'shipped':
                raise UserError(_("Only shipped invoice can be re-scheduled"))

        return res

    def reschedule_hub_invoice(self):
        data = dict()
        data['return_delivery_man'] = self.return_delivery_man.id
        data['return_vehicle_number'] = self.return_vehicle_number
        data['description'] = self.description
        data['ndr'] = self.ndr

        for items in self.env['hub.wise.invoice'].browse(self.env.context.get('active_ids')):
            self.env["wms.manifest.line"].reschedule_invoice_delivery(items.manifest_line_id.id)

            self.env.cr.execute("UPDATE hub_wise_invoice SET return_delivery_man = %s,"
                                "return_vehicle_number = %s,description = %s,ndr = %s,state='reschedule',"
                                "rescheduled= TRUE WHERE id = %s",(data['return_delivery_man'],
                                                  data['return_vehicle_number'],data['description'],
                                                  data['ndr'],items.id))
            self.env.cr.commit()


class MultipleInvoiceSelectReturn(models.TransientModel):
    _name = "multiple.invoice.select.return"
    _description = "Multiple Invoice Select For Full Return"

    date_return = fields.Datetime('Date', required=True, readonly=True, index=True, copy=False,  default=fields.Datetime.now)
    description = fields.Char('Reference/Description')
    return_delivery_man = fields.Many2one('delivery.man.info',string='Return Delivery Man')
    return_vehicle_number = fields.Char(string='Retn. Vehicle Number')
    return_reason = fields.Selection([
        ('cash_problems', 'Cash Problems'),
        ('customer_did_not_ordered', 'Customer Did Not Ordered'),
        ('customer_not_interested', 'Customer Not Interested'),
        ('customer_not_reachable', 'Customer Not Reachable'),
        ('customer_wants_full_delivery', 'Customer Wants Full Delivery'),
        ('customer_wants_high_quality', 'Customer Wants High Quality'),
        ('customer_wants_low_quality', 'Customer Wants Low Quality'),
        ('fake_order', 'Fake Order'),
        ('late_delivery', 'Late Delivery'),
        ('less_qty_required', 'Less Qty Required'),
        ('more_qty_required', 'More Qty Required'),
        ('office_closed', 'Office Closed'),
        ('pack_size_issue', 'Pack Size Issue'),
        ('price_issue', 'Price Issue'),
        ('product_damaged', 'Product Damaged'),
        ('product_mismatch', 'Product Mismatch'),
        ('returned_due_to_other_products', 'Returned Due to Other Products'),
        ('wrong_address', 'Wrong Address'),
        ('wrong_order_placed', 'Wrong Order Placed'),
        ('wrongly_packed', 'Wrongly Packed'),
    ], 'Return Reason', index=True)

    def default_get(self, fields):

        res = super(MultipleInvoiceSelectReturn, self).default_get(fields)

        for items in self.env['hub.wise.invoice'].browse(self.env.context.get('active_ids')):
            if items.state != 'shipped':
                raise UserError(_("Only shipped invoice can be returned"))

        return res

    def full_return_hub_invoice(self):
        data = dict()
        data['return_delivery_man'] = self.return_delivery_man.id
        data['return_vehicle_number'] = self.return_vehicle_number
        data['description'] = self.description
        data['return_reason'] = self.return_reason

        for items in self.env['hub.wise.invoice'].browse(self.env.context.get('active_ids')):
            self.invoice_with_full_return(items)

            self.env.cr.execute("UPDATE hub_wise_invoice SET return_delivery_man = %s,"
                                "return_vehicle_number = %s,description = %s,return_reason = %s,state='full_return',delivery_amount=0, return_amount=%s WHERE id = %s",(data['return_delivery_man'],
                                                  data['return_vehicle_number'],data['description'],
                                                  data['return_reason'],items.invoice_amount,
                                                  items.id))
            self.env.cr.commit()

    def invoice_with_full_return(self,hub_obj):
        context = dict(self.env.context)
        context['source'] = self.env.context.get('source') if self.env.context.get('source') else 'Web'
        hub_inv_obj = hub_obj
        man_line = hub_inv_obj.manifest_line_id

        if len(man_line) > 1:
            raise ValidationError('Multiple manifest line has been created using same invoice.')

        if self.return_reason:
            out_data = {
                'name': hub_inv_obj.name,
                'man_name': str(man_line.wms_manifest_id.name),
                'delivery_date': datetime.now().strftime('%Y-%m-%d'),
                'source': context.get('source') if context.get('source') else 'Web',
                'delivery_done_by': self.env.uid,
                'delivery_done_datetime': fields.Date.today(),
            }

            outbound_line_list = list()

            for line in hub_inv_obj.hub_wise_delivery_line:
                if float(line.inv_quantity) > 0.00:
                    outbound_line_list.append([0, False, {
                        'invoice_id': hub_inv_obj.invoice_id.id,
                        'magento_no': hub_inv_obj.name,
                        'description': str(hub_inv_obj.description),
                        'product_id': line.product_id.id,
                        'amount': float(line.inv_amount),
                        'quantity': float(line.inv_quantity),
                        'delivered': 0,
                        'return_reason': str(self.return_reason),
                    }])

            out_data['outbound_line'] = outbound_line_list
            context['manifest_line_id'] = man_line.id

            wms_man_out = self.env["wms.manifest.outbound"]
            man_out_id = wms_man_out.create(out_data)

            source = context['source']
            delivery_done_by = self.env.uid
            delivery_done_datetime = fields.Date.today()
            context['full_return'] = True

            man_line.write({'manifest_out_status': 'assigned', 'full_return': True, 'source': source,
                            'delivery_done_by': delivery_done_by,
                            'delivery_done_datetime': delivery_done_datetime})

            # --------------------------------------------------
            data = dict()
            data['name'] = str(man_line.magento_no)
            data['invoice_id'] = hub_inv_obj.invoice_id.id
            data['delivery_date'] = datetime.today().date().strftime("%Y-%m-%d")
            data['delivery_time'] = ''
            self.env['wms.manifest.process'].process_confirm(data)
            # --------------------------------------------------

        else:
            # pop up error
            raise Warning(_('Reason can not be blank!!!'))


class HubInvoicePartialDelivery(models.Model):
    _name = "hub.invoice.partial.delivery"
    _description = "Hub Invoice Partial Delivery"

    description = fields.Char('Reference/Description')
    return_delivery_man = fields.Many2one('delivery.man.info', string='Return Delivery Man')
    return_vehicle_number = fields.Char(string='Retn. Vehicle Number')
    partial_delivery_line = fields.One2many('hub.invoice.partial.delivery.line', 'partial_deliver_line_id', 'Delivery Line',
                                    required=True)

    return_reason = fields.Selection([
        ('sku_full_received', 'SKU Full Received'),
        ('cash_problems', 'Cash Problems'),
        ('customer_did_not_ordered', 'Customer Did Not Ordered'),
        ('customer_not_interested', 'Customer Not Interested'),
        ('customer_not_reachable', 'Customer Not Reachable'),
        ('customer_wants_full_delivery', 'Customer Wants Full Delivery'),
        ('customer_wants_high_quality', 'Customer Wants High Quality'),
        ('customer_wants_low_quality', 'Customer Wants Low Quality'),
        ('fake_order', 'Fake Order'),
        ('late_delivery', 'Late Delivery'),
        ('less_qty_required', 'Less Qty Required'),
        ('more_qty_required', 'More Qty Required'),
        ('office_closed', 'Office Closed'),
        ('pack_size_issue', 'Pack Size Issue'),
        ('price_issue', 'Price Issue'),
        ('product_damaged', 'Product Damaged'),
        ('product_mismatch', 'Product Mismatch'),
        ('returned_due_to_other_products', 'Returned Due to Other Products'),
        ('wrong_address', 'Wrong Address'),
        ('wrong_order_placed', 'Wrong Order Placed'),
        ('wrongly_packed', 'Wrongly Packed'),
    ], 'Return Reason', index=True)

    def default_get(self, fields):

        res = super(HubInvoicePartialDelivery, self).default_get(fields)

        man_line = self.env['hub.wise.invoice'].browse(self.env.context.get('active_ids'))

        for items in man_line:
            if items.state != 'shipped':
                raise UserError(_("Only shipped invoice can be returned"))

        items = list()

        for line in man_line.hub_wise_delivery_line:
            if line.inv_quantity > 0.00:
                item = {
                    'product_id': line.product_id.id,
                    'description': line.product_id.default_code,
                    'quantity': line.inv_quantity,
                    'product_unit_price': float(line.product_unit_price),
                    'amount': float(line.inv_amount),
                    'delivered_qty': line.inv_quantity
                }

                if line.product_id.default_code and line.product_id.default_code != '':
                    items.append((0, 0, (item)))
                else:
                    raise ValidationError("The relevant product doesn't have any internal reference. Please set the "
                                          "internal reference, and try again")
        abc = json.dumps(items)

        res.update(partial_delivery_line=items)

        return res

    def partial_delivery_hub_invoice(self):
        data = dict()
        data['return_delivery_man'] = self.return_delivery_man.id
        data['return_vehicle_number'] = self.return_vehicle_number
        data['description'] = self.description
        data['return_reason'] = self.return_reason
        try:

            for items in self.env['hub.wise.invoice'].browse(self.env.context.get('active_ids')):
                self.invoice_with_partial_return(items)

                delivery_amount = items.invoice_amount - items.return_amount

                self.env.cr.execute("UPDATE hub_wise_invoice SET return_delivery_man = %s,"
                                    "return_vehicle_number = %s,description = %s,return_reason = %s,state='partial_delivered',delivery_amount=%s WHERE id = %s",
                                    (data['return_delivery_man'],
                                     data['return_vehicle_number'], data['description'],
                                     data['return_reason'], delivery_amount,items.id
                                     ))
                self.env.cr.commit()

        except:
            raise UserError(_("Only shipped invoice can be returned"))

    def invoice_with_partial_return(self,hub_inv_obj):
        context = dict(self.env.context)

        delivery_return = self
        context['source'] = self.env.context.get('source') if self.env.context.get('source', False) else 'Web'

        if not self.return_reason:
            raise Warning(_('Reason can not be blank!!!'))

        for line in delivery_return.partial_delivery_line:
            if line.delivered_qty < 0 or line.delivered_qty > line.quantity:
                raise Warning(_('Delivered quantity should be within ordered quantity!!!'))

        man_line_list = hub_inv_obj.manifest_line_id
        man_line = hub_inv_obj


        if len(man_line) > 1:
            raise ValidationError('Multiple manifest line has been created using same invoice.')

        out_data = {
            'name': hub_inv_obj.name,
            'man_name': str(man_line.manifest_id.name),
            'delivery_date': datetime.now().strftime('%Y-%m-%d'),
            'source': context.get('source') if context.get('source') else 'Web',
            'delivery_done_by': self.env.uid,
            'delivery_done_datetime': fields.Date.today(),
        }

        outbound_line_list = list()

        for line in delivery_return.partial_delivery_line:

            if float(line.quantity) > 0.00:

                if self.env["product.product"].browse([line.product_id]).type == 'service':
                    delivered = float(1)
                    quantity = float(1)
                else:
                    delivered = float(line.delivered_qty)
                    quantity = float(line.quantity)

                outbound_line_list.append([0, False, {
                    'invoice_id': hub_inv_obj.invoice_id.id,
                        'magento_no': hub_inv_obj.name,
                        'description': str(hub_inv_obj.description),
                        'product_id': line.product_id,
                        'amount': float(line.amount),
                        'quantity': quantity,
                        'delivered': delivered,
                        'return_reason': str(self.return_reason),
                }])

        out_data['outbound_line'] = outbound_line_list

        context['manifest_line_id'] = man_line.id

        wms_man_out = self.env["wms.manifest.outbound"]
        man_out_id = wms_man_out.create(out_data)

        returned_qty = 0
        returned_amount = 0
        for line_return in delivery_return.partial_delivery_line:
            if line_return.returned_qty > 0:
                returned_qty += line_return.returned_qty
                returned_amount += line_return.returned_amount

        source = context['source']
        delivery_done_by = self.env.uid
        delivery_done_datetime = datetime.today()
        if man_out_id and returned_qty > 0:

            man_line_list.write({'manifest_out_status': 'assigned', 'partial_delivered': True,
                                 'source': source, 'delivery_done_by': delivery_done_by,
                                 'delivery_done_datetime': delivery_done_datetime
                                 })
        else:
            man_line_list.write({'manifest_out_status': 'assigned', 'full_delivered': True,
                                 'source': source, 'delivery_done_by': delivery_done_by,
                                 'delivery_done_datetime': delivery_done_datetime})

        # --------------------------------------------------

        data = dict()
        data['name'] = str(man_line.name)
        data['invoice_id'] = man_line.invoice_id.id
        data['delivery_date'] = datetime.today().date().strftime("%Y-%m-%d")
        data['delivery_time'] = ''
        self.env['wms.manifest.process'].process_confirm(data)
        # --------------------------------------------------
        self.env.cr.execute("UPDATE hub_wise_invoice SET return_amount=%s WHERE id = %s",(returned_amount,hub_inv_obj.id))
        self.env.cr.commit()

        return True


class HubInvoicePartialDeliveryLine(models.Model):
    _name = "hub.invoice.partial.delivery.line"
    _description = "Hub Invoice Partial Delivery Line"

    product_id = fields.Integer('Product ID')
    description = fields.Char('Description')
    quantity = fields.Float('Quantity')
    delivered_qty = fields.Float('Delivered')
    returned_qty = fields.Float('Return')
    amount = fields.Float('Total Amount')
    product_unit_price = fields.Float(string='Price Unit')
    returned_amount = fields.Float('Returned Amount')
    delivered_amount = fields.Float('Delivered Amount')
    partial_deliver_line_id = fields.Many2one('hub.invoice.partial.delivery', 'Delivery Line', required=True, ondelete='cascade',
                                      index=True, readonly=True)

    @api.onchange('delivered_qty')
    def _onchange_return_qty(self):
        self.returned_qty = self.quantity - self.delivered_qty if (self.quantity - self.delivered_qty) > 0 else 0
        self.returned_amount = round(self.returned_qty * self.product_unit_price,2) if (self.returned_qty * self.product_unit_price) > 0 else 0
