{
    'name': 'Hub Management',
    'version': '14.0.1',
    'summary': 'Warehouse Hub Area Mapping or Hub management system area wise and all',
    'category': 'Inventory',
    'author': 'Ashif Raihun',
    'website': 'https://www.sindabad.com',
    'depends': ['base','product','sale','wms_manifest_modify'],
    'data': [
        'security/hub_management_security.xml',
        'security/ir.model.access.csv',
        'wizard/multiple_invoice_select_accept.xml',
        'wizard/multiple_invoice_select_reject.xml',
        'wizard/multiple_invoice_select_shipped.xml',
        'wizard/multiple_invoice_select_delivered.xml',
        'wizard/multiple_invoice_select_reschedule.xml',
        'wizard/multiple_invoice_select_full_return.xml',
        'wizard/hub_invoice_select_partial_delivery.xml',
        'views/warehouse_hub_area_mapping.xml',
        'views/wms_manifest_inherit_view.xml',
        'views/hub_wise_invoice.xml',
        'views/loading_master_inherit_view.xml'

    ],
    'installable': True,
    'application': False,
    'auto_install': False,
}
