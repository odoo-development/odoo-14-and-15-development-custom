from odoo import api, fields, models, _
from odoo.tools.translate import _
from odoo.exceptions import except_orm, Warning, RedirectWarning, UserError


class WarehouseHubArea(models.Model):
    _name = "warehouse.hub.area"
    _description = "Warehouse Hub Area"

    name=fields.Char(string="Hub Name")
    address=fields.Char(string="Hub address")
    hub_short_code=fields.Char(string="Hub Code")
    warehouse_id=fields.Many2one('stock.warehouse', string='Warehouse Name')
    hub_manager=fields.Char(string="Hub Manager")
    area_line_ids = fields.One2many('hub.area.line', 'area_hub_id', 'Area Details', required=True)
    active=fields.Boolean('Active', default=True)


class HubAreaLine(models.Model):
    _name = "hub.area.line"
    _description = "Hub Area Line"

    area_hub_id = fields.Many2one('warehouse.hub.area', 'Hub Name', required=True, ondelete='cascade', index=True)
    state_id = fields.Many2one('res.country.state', string="State Name", copy=False, store=True)
    code=fields.Char(string="Area Code")
    serial_number = fields.Char('Serial Number')
    country_id=fields.Many2one('res.country', string= 'Country Name')

    @api.onchange('state_id')
    def _onchange_state_id(self):
        if self.state_id:
            self.country_id = self.state_id.country_id and self.state_id.country_id.id or False
            self.code = self.state_id.code


class HubWiseInvoice(models.Model):
    _name = "hub.wise.invoice"
    _description = "Hub Wise Delivery"

    name=fields.Char(string='Order Number')
    oder_id=fields.Many2one('sale.order', string='Order Number')
    invoice_id=fields.Many2one('account.move', string= 'Invoice Number')
    challan_id=fields.Many2one('stock.picking', string='Challan Number')
    warehouse_id=fields.Many2one('stock.warehouse', string='Warehouse')
    hub_id=fields.Many2one('warehouse.hub.area', string='Hub')
    invoice_amount=fields.Float(string='Invoice Amount')
    delivery_amount=fields.Float(string='Delivery Amount')
    return_amount = fields.Float(string='Return Amount')
    delivery_address=fields.Char(string='Delivery Address')
    delivery_area=fields.Char(string='Delivery Area')
    delivery_vehicle_number=fields.Char(string='Vehicle Number', tracking=True)
    delivery_man = fields.Many2one('delivery.man.info', string='Delivery Man', tracking=True)
    manifest_delivery_man = fields.Char(string='Manifest Delivery Man', tracking=True)
    manifest_vehicle_number = fields.Char(string='Manifest Vehicle Number', tracking=True)
    return_delivery_man = fields.Many2one('delivery.man.info',string='Return Delivery Man', tracking=True)
    return_vehicle_number =fields.Char(string='Return Vehicle Number', tracking=True)
    manifest_id=fields.Many2one('wms.manifest.process', string='Manifest No')
    manifest_line_id = fields.Many2one('wms.manifest.line', string='Manifest line id')
    remark = fields.Text(string='Remarks', tracking=True)
    description = fields.Text(string='Description', tracking=True)
    reject_reason = fields.Char(string='Reject Reason', tracking=True)

    rescheduled = fields.Boolean(string='Reschedule')

    date_accept = fields.Datetime('Accept Date',readonly=True, index=True, copy=False)
    date_reject = fields.Datetime('Reject Date', readonly=True, index=True, copy=False)
    state=fields.Selection([
        ('draft', 'Draft'),
        ('accepted', 'Accepted'),
        ('rejected', 'Rejected'),
        ('shipped', 'Shipped'),
        ('fully_delivered', 'Delivered'),
        ('partial_delivered', 'Partial Delivered'),
        ('full_return', 'Return'),
        ('reschedule', 'Re-schedule')],default = 'draft',
        string='Status')

    ndr = fields.Selection([
        ('cash_problems', 'Cash Problems'),
        ('customer_did_not_ordered', 'Customer Did Not Ordered'),
        ('customer_not_interested', 'Customer Not Interested'),
        ('customer_not_reachable', 'Customer Not Reachable'),
        ('customer_wants_full_delivery', 'Customer Wants Full Delivery'),
        ('customer_wants_high_quality', 'Customer Wants High Quality'),
        ('customer_wants_low_quality', 'Customer Wants Low Quality'),
        ('fake_order', 'Fake Order'),
        ('late_delivery', 'Late Delivery'),
        ('less_qty_required', 'Less Qty Required'),
        ('more_qty_required', 'More Qty Required'),
        ('office_closed', 'Office Closed'),
        ('pack_size_issue', 'Pack Size Issue'),
        ('price_issue', 'Price Issue'),
        ('product_damaged', 'Product Damaged'),
        ('product_mismatch', 'Product Mismatch'),
        ('returned_due_to_other_products', 'Returned Due to Other Products'),
        ('wrong_address', 'Wrong Address'),
        ('wrong_order_placed', 'Wrong Order Placed'),
        ('wrongly_packed', 'Wrongly Packed'),
    ], 'Not Delivery Reason', index=True)

    return_reason = fields.Selection([
        ('cash_problems', 'Cash Problems'),
        ('customer_did_not_ordered', 'Customer Did Not Ordered'),
        ('customer_not_interested', 'Customer Not Interested'),
        ('customer_not_reachable', 'Customer Not Reachable'),
        ('customer_wants_full_delivery', 'Customer Wants Full Delivery'),
        ('customer_wants_high_quality', 'Customer Wants High Quality'),
        ('customer_wants_low_quality', 'Customer Wants Low Quality'),
        ('fake_order', 'Fake Order'),
        ('late_delivery', 'Late Delivery'),
        ('less_qty_required', 'Less Qty Required'),
        ('more_qty_required', 'More Qty Required'),
        ('office_closed', 'Office Closed'),
        ('pack_size_issue', 'Pack Size Issue'),
        ('price_issue', 'Price Issue'),
        ('product_damaged', 'Product Damaged'),
        ('product_mismatch', 'Product Mismatch'),
        ('returned_due_to_other_products', 'Returned Due to Other Products'),
        ('wrong_address', 'Wrong Address'),
        ('wrong_order_placed', 'Wrong Order Placed'),
        ('wrongly_packed', 'Wrongly Packed'),
    ], 'Return Reason', index=True)

    active = fields.Boolean('Active', default=True, tracking=True)
    hub_wise_delivery_line = fields.One2many('hub.delivery.product.line', 'hub_wise_delivery_line_id', 'Product Line', required=True)

    def accept(self):
        pass

    def reject(self):
        pass

    def returned(self):
        pass

    def delivered(self):
        pass

    def partial_delivered(self):
        pass

    def reschedule(self):
        pass


class HubDeliveryProductLine(models.Model):
    _name = "hub.delivery.product.line"
    _description = "Delivery Line"

    hub_wise_delivery_line_id = fields.Many2one('hub.wise.invoice', 'Hub Wise Delivery Line ID', required=True, ondelete='cascade', index=True,readonly=True)
    product_id = fields.Many2one('product.product', string="Product Name", copy=False, store=True)
    inv_quantity=fields.Float(string='Quantity')
    delivered_quantity = fields.Float(string='Delivered Quantity')
    delivered_amount=fields.Float(string='Delivery Amount')
    product_unit_price=fields.Float(string='Price Unit')
    inv_amount=fields.Float(string='Amount')
    return_quantity=fields.Float(string='Return Quantity')
    return_amount=fields.Float(string= 'Return Amount')



class WmsManifestProcess(models.Model):
    _inherit = "wms.manifest.process"

    hub_id = fields.Many2one('warehouse.hub.area', string='Hub Name')

    def confirm_wms_manifest(self):
        super(WmsManifestProcess, self).confirm_wms_manifest()
        inv_obj = self.env['account.move']
        hub_invoice_obj = self.env['hub.wise.invoice']
        if self.hub_id:
            for items in self.picking_line:
                magento_no = str(items.magento_no)
                sales = self.env['sale.order'].search([('client_order_ref', '=', magento_no),('state', '!=','cancel')])

                out_data = {
                    'name': items.magento_no,
                    'oder_id': sales.id,
                    'invoice_id': items.invoice_id,
                    'warehouse_id': self.warehouse_id.id,
                    'hub_id': self.hub_id.id,
                    'delivery_address': items.address,
                    'delivery_area': items.area,
                    'manifest_id': self.id,
                    'manifest_line_id': items.id,
                    'manifest_delivery_man': self.assigned_to,
                    'manifest_vehicle_number': self.vehicle_number,
                    'invoice_amount': items.amount
                }

                hub_wise_delivery_line = list()

                for inv_line in inv_obj.browse(items.invoice_id).invoice_line_ids:
                    hub_wise_delivery_line.append([0, False, {
                        'product_id': inv_line.product_id.id,
                        'product_unit_price': float(inv_line.price_unit_with_vat),
                        'inv_amount': float(inv_line.price_subtotal_with_vat),
                        'inv_quantity': float(inv_line.quantity),
                    }])

                out_data['hub_wise_delivery_line'] = hub_wise_delivery_line
                hub_invoice = hub_invoice_obj.create(out_data)
        return True


class WmsManifestLine(models.Model):
    _inherit = "wms.manifest.line"

    hub_id = fields.Many2one('warehouse.hub.area', string='Hub Name')
    hub_invoice = fields.Boolean(string='Hub Invoice')


class LoadingList(models.Model):
    _inherit = "loading.master"

    hub_id = fields.Many2one('warehouse.hub.area', string='Hub Name')


class LoadingListLine(models.Model):
    _inherit = "loading.master.line"

    hub_id = fields.Many2one('warehouse.hub.area', string='Hub Name')
    hub_invoice = fields.Boolean(string='Hub Invoice')


