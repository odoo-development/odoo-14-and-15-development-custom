# -*- coding: utf-8 -*-
{
    'name': "Supplier Portal",
    'summary': """ This module will help you to Supplier Portal based on model """,
    'description': """ Supplier Portal based on model """,
    'author': "Sindabad",
    'category': 'Customise',
    'depends': ['base','sale','order_approval_process'],
    'license': 'AGPL-3',
    'data': [
        'views/supplier_portal_view.xml',
    ],
    'installable': True,
    'application': True,
}
