from odoo import api, fields, models, _


class SaleOrder(models.Model):
    _inherit = 'sale.order'

    state = fields.Selection(selection_add=[('draft_sp', 'Pending Supplier Order'),
                                            ('processing_sp', 'Processing Supplier Order'),
                                            ('confirmed_sp', 'Confirmed Supplier Order'),
                                            ('shipped_sp', 'Shipped Supplier Order'),
                                            ('delivered_sp', 'Delivered Supplier Order')
                                            ])
    seller_order = fields.Boolean(string="Seller Order", default = False)
    seller_id = fields.Many2one('res.partner', string='Seller', tracking=True)

    @api.model
    def seller_order_cancel(self, vals):
        odoo_order_id = vals.get('odoo_order_id')
        so_obj = self.env['sale.order'].browse(odoo_order_id)
        if so_obj:
            so_obj.action_cancel()
            data = {
                'status_code': 200,
                'status': 'success'
            }
        else:
            data = {
                'status_code': 404,
                'status': 'error'
            }
        return data

    @api.model
    def seller_order_status(self, vals):
        odoo_order_id = vals.get('odoo_order_id')
        seller_status = vals.get('order_status')
        so_obj = self.env['sale.order'].browse(odoo_order_id)
        if so_obj:
            status = None
            state = str('processing_sp')
            if seller_status == 'confirmed':
                status = str('processing')
                state = str('confirmed_sp')
                self.send_sms_sp('confirmed_sp', so_obj)

            if seller_status == 'shipped':
                status = str('dispatched')
                state = str('shipped_sp')
                self.send_sms_sp('shipped_sp', so_obj)

            if seller_status == 'delivered':
                status = str('delivered')
                state = str('delivered_sp')
                self.send_sms_sp('delivered_sp', so_obj)

            try:
                status_data = {
                    'odoo_order_id': so_obj.id,
                    'magento_id': str(so_obj.client_order_ref),
                    'order_state': status,
                    'state_time': fields.datetime.now()
                }
                if status:
                    self.env["order.status.synch.log"].sudo().create(status_data)

                so_obj.write({'state': state})

            except:
                pass

            data = {
                'status_code': 200,
                'status': 'success'
            }
        else:
            data = {
                'status_code': 404,
                'status': 'error'
            }
        return data

    def send_sms_sp(self,state,send_sms_sp):
        # super(SaleOrder, self).seller_order_status(self)
        so = send_sms_sp
        try:
            # so = self.browse()
            parent = so.partner_id
            for i in range(5):
                if len(parent.parent_id) == 1:
                    parent = parent.parent_id
                else:
                    parent = parent
                    break
            company_phone = parent.mobile
            phone_number = str(so.partner_id.mobile) if so.partner_id.mobile else company_phone
            if not phone_number:
                phone_number = str(so.partner_id.phone)

            state=''

            if phone_number and  state=='confirmed_sp' :

                sms_text = "Your order #{0} has been confirmed. You will get updates once items has been shipped.".format(
                    str(so.client_order_ref))

                self.env["send.sms.on.demand"].send_sms_on_demand(sms_text, phone_number, str(parent.name))
            if phone_number and  state=='shipped_sp':

                sms_text= "Your order {0} has been shipped and will be delivered today by Sindabad Wish Master {1}.".format(
                            str(so.client_order_ref))
                self.env["send.sms.on.demand"].send_sms_on_demand(sms_text, phone_number, str(parent.name))
            if phone_number and  state== 'delivered_sp':
                sms_text_delivered = "Your order {0} has been delivered successfully. Thank you for staying with us. Try our mobile app to get offers at https://rebrand.ly/tar7z".format(
                    so.client_order_ref)
                self.env["send.sms.on.demand"].send_sms_on_demand(sms_text_delivered, phone_number, str(parent.name))

        except:
            pass

        return state


class Product(models.Model):
    _inherit = 'product.product'

    seller_product = fields.Boolean(string="Seller Product",default = False)
    seller_id = fields.Many2one('res.partner', string='Seller', tracking =True)
    seller_portal_product_id = fields.Integer(string="Seller Portal Product ID")

