# -*- coding: utf-8 -*-

{

    'name':'Inherit PDF Report',
    'version':'14.0.1.2',
    'category':'sales/CRM',
    'summary':'create Prodject from leads',
    'description': "",
    'website':"",
    'depends':[
        'base','sale','account','purchase','stock',
    ],
    'data':[

        'report/delivery_report_inherit.xml',
        'report/invoice_report_inherit.xml',
        'report/purchas_order_report_inherit.xml',


    ],


    'demo':[],
    'installable': True,
    'application': False,
    'auto_install': False,



}