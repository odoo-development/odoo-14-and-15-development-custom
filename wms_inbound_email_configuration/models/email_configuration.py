import logging

from odoo import fields, models

_logger = logging.getLogger(__name__)


class PoEmailConfiguration(models.Model):
    _name = "po.email.configuration"
    _description = "PO EMAIL Configuration"
    _order = 'id desc'

    name = fields.Char("Configuration Name", required=True)
    template_id = fields.Many2one('mail.template', 'Email Template',
                                  domain=[('model', 'in', ['purchase.order', 'product.product'])])
    active = fields.Boolean('Active', required=False)

    # ### Block code for V-14
    # _columns = {
    #     # 'email_to': fields.char('TO',required=True),
    #     'name': fields.char("Configuration Name", required=True),
    #     'template_id': fields.many2one('email.template', 'Email Template', domain=[('model', 'in', ['purchase.order','product.product'])]),
    #     'active': fields.boolean('Active', required=False),
    #
    # }

    _sql_constraints = [
        ('template_id_unique', 'unique(template_id)', 'Can\'t be duplicate value for this field!')
    ]

    def email_trigger_with_conditional_po(self):

        self.env.cr.execute("select name, template_id, active from po_email_configuration")
        email_conf_data = self.env.cr.fetchall()
        id = self.env['purchase.order'].search([])[-1]

        if len(email_conf_data) > 0:
            for query_data in email_conf_data:
                name = query_data[0]
                # print 'length, name=', len(email_conf_data), name, '*'*10
                template_id = query_data[1]
                active = query_data[2]
                if template_id and active is True:
                    try:
                        # print 'id=', id, 'template_id=', template_id
                        self.env['email.template'].send_mail(template_id, id, force_send=True)
                    except Exception as e:
                        _logger.warning('Email failed with following exception: %s', e.message)
                else:
                    _logger.warning('Status of %s is %s', name, active)

        return True

    def email_trigger_with_conditional_product(self):
        self.env.cr.execute("select name, template_id, active from po_email_configuration")

        email_conf_data = self.env.cr.fetchall()
        id = self.env['product.product'].search([])[-1]

        if len(email_conf_data) > 0:
            for query_data in email_conf_data:
                name = query_data[0]
                # print 'length, name=', len(email_conf_data), name, '*'*10
                template_id = query_data[1]
                active = query_data[2]
                if template_id and active is True:
                    try:
                        # print 'id=', id, 'template_id=', template_id
                        self.env['email.template'].send_mail(template_id, id, force_send=True)
                    except Exception as e:
                        _logger.warning('Email failed with following exception: %s', e.message)
                else:
                    _logger.warning('Status of %s is %s', name, active)

        return True
