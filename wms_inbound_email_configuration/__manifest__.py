{
    'name': 'WMS Inbound Email Configuration',
    'version': "14.0.1.0.0",
    'category': 'Accounts',
    'description': """
WMS Inbound Email Configuration   
==============================================================

""",
    'author': 'Odoo Bangladesh',
    'depends': ['account', 'base', 'purchase', 'product', 'wms_inbound', 'wms_menulist'],
    'data': [
        'security/ir.model.access.csv',
        'views/email_configuration_menu_vew.xml',
        'views/email_configuration_view.xml',
    ],
    'installable': True,
    'auto_install': False,
}
