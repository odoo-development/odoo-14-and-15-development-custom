from odoo import models, fields, api, _

class AccountMove(models.Model):
    _inherit = "account.move"

    rtv_id = fields.Many2one('rtv.process', string='RTV Number',required=False, tracking=True)
    rtv = fields.Boolean(string='RTV')
