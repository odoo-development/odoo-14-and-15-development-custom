import datetime
from time import gmtime, strftime

from odoo import models, fields, api
from odoo.exceptions import Warning
from odoo.tools.translate import _


class StockPicking(models.Model):
    _inherit = 'stock.picking'

    rtv = fields.Boolean('RTV')


class StockMove(models.Model):
    _inherit = "stock.move"

    def action_done(self):
        context = None
        if self.env.context.get('rtv', False):
            res = super(StockMove, self).action_done()
            for move in self:

                for rtv_data in self.env['rtv.process'].browse(self.env.context.get('rtv_id')).rtv_process_line:
                    if rtv_data.unit_cost > float(0) and move.product_id == rtv_data.product_id:
                        self.write([move.id], {'price_unit': rtv_data.unit_cost})

        else:
            res = super(StockMove, self).action_done()

        return res


class SrnProcess(models.Model):
    _name = "srn.process"
    _description = "SRN Process"

    name = fields.Char('SRN Number')
    warehouse_id = fields.Many2one('stock.warehouse', string='Warehouse', readonly=True, copy=False, index=True)

    # warehouse_id = fields.Selection([
    #     (1, 'Central Warehouse Uttara'),
    #     (2, 'Nodda Warehouse'),
    #     (3, 'Nodda Retail Warehouse'),
    #     (4, 'Motijheel Warehouse'),
    #     (6, 'Uttara Retail Warehouse'),
    #     (7, 'Motijheel Retail Warehouse'),
    #     (8, 'Damage Warehouse'),
    #
    # ], 'Warehouse', readonly=True, copy=False, help="Gives the List of the Warehouses", index=True)

    vendor = fields.Many2one('res.partner', 'Supplier')
    vendor_address = fields.Char('Address')
    vendor_contact = fields.Char('Contact Number')
    remark = fields.Text('Remark')
    delivered = fields.Boolean('Delivered')

    received = fields.Boolean('Received')
    received_by = fields.Many2one('res.users', 'Received By')
    received_time = fields.Datetime('Received Time')

    not_received = fields.Boolean('Not Received')
    not_received_by = fields.Many2one('res.users', 'Not Received By')
    not_received_time = fields.Datetime('Not Received Time')

    confirmed_by = fields.Many2one('res.users', 'Confirmed By')
    confirmed_time = fields.Datetime('Confirmed Time')

    requested_by = fields.Many2one('res.users', 'Requested By')
    requested_time = fields.Datetime('Requested Time')

    resolved = fields.Boolean('Resolved')
    resolved_by = fields.Many2one('res.users', 'Resolved By')
    resolved_time = fields.Datetime('Resolved Time')

    cancel = fields.Boolean('Cancel')
    cancel_by = fields.Many2one('res.users', 'Cancel By')
    cancel_time = fields.Datetime('Cancel Time')

    state = fields.Selection([
        ('pending', 'Pending'),
        ('confirmed', 'Confirmed'),
        ('received', 'Received'),
        ('not_received', 'Not Received'),
        ('partial_received', 'Partial Received'),
        ('cancel', 'Cancelled'),

    ], 'Status', default='pending', readonly=True, copy=False, help="Gives the status of the SRN Process", index=True)

    srn_line = fields.One2many('srn.rtv.line', 'srn_rtv_line_id', 'SRN Line ID')
    srn_product_line = fields.One2many('srn.rtv.product', 'srn_rtv_product_id', 'SRN RTV Product', required=True)

    stock_id = fields.Many2one('stock.picking', 'Stock Number')

    pqc = fields.Boolean('PQC')
    pqc_id = fields.Many2one("pending.quality.control", 'PQC ID')
    pqc_number = fields.Char('PQC Number')

    cancel_reason = fields.Text('Cancel Reason')

    not_receive_reason_date = fields.Datetime('Not Receive Reason Date')
    not_receive_reason_by = fields.Many2one('res.users', 'Not Receive Reason By')

    not_receive_reason = fields.Selection([
        ("Shop off", "Shop off"),
        ("Vendor unreachable", "Vendor unreachable"),
        ("Vendor Rescheduled", "Vendor Rescheduled"),
        ("Does not agree to take", "Does not agree to take"),
        ("Not received due to quality", "Not received due to quality"),
        ("Vendor is not aware of", "Vendor is not aware of")

    ], 'Not Receive Reason', copy=False, help="Reason", index=True)

    cancel_srn_rtv_date = fields.Datetime('Cancel SRN RTV Date')
    cancel_srn_rtv_by = fields.Many2one('res.users', 'Cancel SRN RTV By')
    cancel_srn_rtv_reason = fields.Text('Cancel SRN RTV Reason')

    # ### Block code for V-14
    # _columns = {
    #     'name': fields.char('SRN Number'),
    #     'warehouse_id': fields.selection([
    #         (1, 'Central Warehouse Uttara'),
    #         (2, 'Nodda Warehouse'),
    #         (3, 'Nodda Retail Warehouse'),
    #         (4, 'Motijheel Warehouse'),
    #         (6, 'Uttara Retail Warehouse'),
    #         (7, 'Motijheel Retail Warehouse'),
    #         (8, 'Damage Warehouse'),
    #
    #     ], 'Warehouse', readonly=True, copy=False, help="Gives the List of the Warehouses", index=True),
    #
    #     'vendor': fields.many2one('res.partner', 'Supplier', domain="[('supplier','=',True)]"),
    #     'vendor_address': fields.char('Address'),
    #     'vendor_contact': fields.char('Contact Number'),
    #     'remark': fields.text('Remark'),
    #     'delivered': fields.boolean('Delivered'),
    #
    #     'received': fields.boolean('Received'),
    #     'received_by': fields.many2one('res.users', 'Received By'),
    #     'received_time': fields.datetime('Received Time'),
    #
    #     'not_received': fields.boolean('Not Received'),
    #     'not_received_by': fields.many2one('res.users', 'Not Received By'),
    #     'not_received_time': fields.datetime('Not Received Time'),
    #
    #     'confirmed_by': fields.many2one('res.users', 'Confirmed By'),
    #     'confirmed_time': fields.datetime('Confirmed Time'),
    #
    #     'requested_by': fields.many2one('res.users', 'Requested By'),
    #     'requested_time': fields.datetime('Requested Time'),
    #
    #     'resolved': fields.boolean('Resolved'),
    #     'resolved_by': fields.many2one('res.users', 'Resolved By'),
    #     'resolved_time': fields.datetime('Resolved Time'),
    #
    #     'cancel': fields.boolean('Cancel'),
    #     'cancel_by': fields.many2one('res.users', 'Cancel By'),
    #     'cancel_time': fields.datetime('Cancel Time'),
    #
    #     'state': fields.selection([
    #         ('pending', 'Pending'),
    #         ('confirmed', 'Confirmed'),
    #         ('received', 'Received'),
    #         ('not_received', 'Not Received'),
    #         ('partial_received', 'Partial Received'),
    #         ('cancel', 'Cancelled'),
    #
    #     ], 'Status', readonly=True, copy=False, help="Gives the status of the SRN Process", index=True),
    #
    #     'srn_line': fields.one2many('srn.rtv.line', 'srn_rtv_line_id', 'SRN Line ID'),
    #     'srn_product_line': fields.one2many('srn.rtv.product', 'srn_rtv_product_id', 'SRN RTV Product', required=True),
    #
    #     'stock_id': fields.many2one('stock.picking', 'Stock Number'),
    #
    #     'pqc': fields.boolean('PQC'),
    #     'pqc_id': fields.many2one("pending.quality.control", 'PQC ID'),
    #     'pqc_number': fields.char('PQC Number'),
    #
    #     'cancel_reason': fields.text('Cancel Reason'),
    #
    #     'not_receive_reason_date': fields.datetime('Not Receive Reason Date'),
    #     'not_receive_reason_by': fields.many2one('res.users', 'Not Receive Reason By'),
    #     # 'not_receive_reason': fields.text('Not Receive SRN RTV Reason'),
    #
    #     # 'not_receive_reason': fields.selection([
    #     #     ("Vendor was unreachable", "Vendor was unreachable"),
    #     #     ("Vendor refused to provide", "Vendor refused to provide"),
    #     #     ("Unable to reach", "Unable to reach"),
    #     #     ("Vendors place was closed", "Vendors place was closed"),
    #     #     ("Cash problem", "Cash problem"),
    #     #     ("Vendor rescheduled", "Vendor rescheduled"),
    #     #     ("Wrong address given", "Wrong address given"),
    #     #     ("Unable to reach on Time", "Unable to reach on Time"),
    #     #     ("Quality issue", "Quality issue"),
    #     #     ("Due to traffic issue", "Due to traffic issue")
    #     #
    #     # ], 'Not Receive Reason', copy=False, help="Reason", index=True),
    #
    #     'not_receive_reason': fields.selection([
    #         ("Shop off", "Shop off"),
    #         ("Vendor unreachable", "Vendor unreachable"),
    #         ("Vendor Rescheduled", "Vendor Rescheduled"),
    #         ("Does not agree to take", "Does not agree to take"),
    #         ("Not received due to quality", "Not received due to quality"),
    #         ("Vendor is not aware of", "Vendor is not aware of")
    #
    #     ], 'Not Receive Reason', copy=False, help="Reason", index=True),
    #
    #     'cancel_srn_rtv_date': fields.datetime('Cancel SRN RTV Date'),
    #     'cancel_srn_rtv_by': fields.many2one('res.users', 'Cancel SRN RTV By'),
    #     'cancel_srn_rtv_reason': fields.text('Cancel SRN RTV Reason'),
    #
    # }
    #
    # _defaults = {
    #     'user_id': lambda obj, cr, uid, context: uid,
    #     'state': 'pending',
    # }

    @api.model
    def create(self, vals):
        record = super(SrnProcess, self).create(vals)

        srn_number = "SRN0" + str(record.id)
        record.name = srn_number

        return record

    def confirm_srn(self):
        for srn in self:
            srn.write({
                'state': 'confirmed',
                'confirmed_by': self.env.uid,
                'confirmed_time': fields.Datetime.now()
            })

            srn.srn_line.write({
                'state': 'confirmed',
                'confirmed_by': self.env.uid,
                'confirmed_time': fields.Datetime.now()
            })

            srn.srn_product_line.write({
                'state': 'confirmed',
                'confirmed_time': fields.Datetime.now()
            })

        # for single_id in self.ids:
        #     confirmed_srn_query = "UPDATE srn_process SET state='confirmed', confirmed_by={0},confirmed_time='{1}' WHERE id={2}".format(str(fields.datetime.now()), single_id)
        #     self.env.cr.execute(confirmed_srn_query)
        #     self.env.cr.commit()
        #
        #     confirmed_srn_rtv_line_query = "UPDATE srn_rtv_line SET state='confirmed', confirmed_by={0}, confirmed_time='{1}' WHERE srn_rtv_line_id={2}".format(
        #         str(fields.datetime.now()), single_id)
        #     self.env.cr.execute(confirmed_srn_rtv_line_query)
        #     self.env.cr.commit()
        #
        #     confirmed_srn_rtv_product_query = "UPDATE srn_rtv_product SET state='confirmed', confirmed_time='{0}' WHERE srn_rtv_product_id={1}".format(
        #         str(fields.datetime.now()), single_id)
        #     self.env.cr.execute(confirmed_srn_rtv_product_query)
        #     self.env.cr.commit()
        #
        # return True

    def transfer_stock(self, rtv_id, reverse=False):
        # call transfer
        rtv_data = self.env['rtv.process'].browse(rtv_id)

        for rtv_obj in rtv_data:

            if reverse is True:
                stock_picking_id = rtv_obj.reverse_stock_id.id
            else:
                stock_picking_id = rtv_obj.stock_id.id

            stock_picking = self.env['stock.picking'].do_enter_transfer_details([stock_picking_id])

            if str(rtv_obj.stock_id.state) == 'draft' or str(rtv_obj.stock_id.state) == 'confirmed':
                #     # unreserve
                self.env['stock.picking'].do_unreserve(int(stock_picking_id))

                # action cancel
                self.env['stock.picking'].action_assign(int(stock_picking_id))

            if str(rtv_obj.stock_id.state) == 'waiting':
                # Rereservation
                self.env['stock.picking'].rereserve_pick([stock_picking_id])

            ##  stock.transfer_details model nai v-14
            # trans_obj = self.env['stock.transfer_details']
            # trans_search = trans_obj.search([('picking_id', '=', stock_picking_id)])
            #
            # trans_search = [trans_search[len(trans_search) - 1]] if len(trans_search) > 1 else trans_search
            #
            # trans_browse = self.env['stock.transfer_details'].browse(trans_search)
            #
            # trans_browse.do_detailed_transfer()

            if reverse is False:
                rtv_obj.write({
                    'state': 'received',
                    'delivered': True,
                    'received_by': self.env.uid,
                    'received_time': str(fields.Datetime.now())
                })

                rtv_obj.rtv_process_line.write({
                    'state': 'received',
                    'received_time': str(fields.Datetime.now())
                })

                # received_rtv_query = "UPDATE rtv_process SET state='received',delivered = TRUE ,received_time='{0}', received_by='{1}' WHERE id={2}".format(
                #     str(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')), receiver, rtv_obj.id)
                # self.env.cr.execute(received_rtv_query)
                # self.env.cr.commit()
                #
                # received_rtv_query_line = "UPDATE rtv_process_line SET state='received', received_time='{0}' WHERE rtv_process_id={1}".format(
                #     str(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')), rtv_obj.id)
                # self.env.cr.execute(received_rtv_query_line)
                # self.env.cr.commit()

                self.env['rtv.end.to.end'].rtv_received_update(rtv_obj.name, rtv_obj.id, 'received')

                self.env['rtv.process'].send_email(rtv_obj.id)

        # return True

    ### ai nichar method ta kothao use kora painai ####
    def rtv_to_aqc(self):

        aqc_line = []

        rtv_obj = self.env['rtv.process'].browse()

        for rtv_data in rtv_obj:

            pqc_obj = self.env['pending.quality.control'].browse(self.env['pending.quality.control'].search([
                (
                    'pqc_number',
                    '=',
                    str(
                        rtv_data.pqc_number))]))

            put_data = data = {'po_id': pqc_obj.po_id,
                               'po_number': str(pqc_obj.po_number),
                               'irn_number': rtv_data.pqc_id.irn_number,
                               'irn_id': rtv_data.pqc_id.irn_id,
                               'qc_id': rtv_data.pqc_id.qc_id,
                               'warehouse_id': rtv_data.warehouse_id,
                               'vendor': rtv_data.vendor_id.id,
                               'area': rtv_data.area.id,
                               'state': 'confirm',
                               'qc_number': pqc_obj.qc_number,
                               'received_by': self.env.uid,
                               'pqc_number': str(rtv_data.pqc_number),
                               'date_confirm': strftime("%Y-%m-%d %H:%M:%S", gmtime()),
                               'pqc_id': rtv_data.pqc_id
                               }

            for rtv_product in rtv_data.rtv_process_line:
                for pqc_data in pqc_obj.pqc_line:
                    if rtv_product.product_id == pqc_data.product_id:
                        aqc_line.append([0, False, {
                            'rtv_id': rtv_data.id,
                            'rtv_name': rtv_data.name,
                            'qc_id': pqc_obj.qc_id,
                            'product': rtv_product.product_id.name,
                            'product_id': rtv_product.product_id.id,
                            'purchase_quantity': pqc_data.purchase_quantity,
                            'received_quantity': rtv_product.return_qty,
                            'accepted_quantity': rtv_product.return_qty,
                            'pending_quantity': rtv_product.requested_qty,
                            'state': 'confirm'
                        }])

            data['aqc_line'] = aqc_line

            if len(aqc_line) > 0:
                # Create aqc
                save_the_data = self.env['accepted.quality.control'].create(data)

                # Generate Put away Process
                put_line_list = []

                for rtv_product in rtv_data.rtv_process_line:
                    if rtv_product.return_qty > 0:
                        put_line_list.append([0, False,
                                              {
                                                  'product_id': rtv_product.product_id.id,
                                                  'qc_id': save_the_data,
                                                  'location': '',
                                                  'product': rtv_product.product_id.name,
                                                  'remarks': '',
                                                  'quantity': rtv_product.return_qty,
                                                  'po_number': str(pqc_obj.po_number)

                                              }])

                put_data['aqc_id'] = save_the_data
                put_data['state'] = 'pending'
                put_data['putaway_line_view'] = put_line_list
                put_env = self.env['putaway.process']
                saved_put_id = put_env.create(put_data)

                rtv_obj.write({
                    'state': 'received',
                    'delivered': True,
                    'received_by': self.env.uid,
                    'received_time': str(fields.Datetime.now())
                })

                rtv_obj.rtv_process_line.write({
                    'state': 'received',
                    'received_time': str(fields.Datetime.now())
                })

        #         received_rtv_query = "UPDATE rtv_process SET state='received',delivered = TRUE ,received_time='{0}', received_by='{1}' WHERE id={2}".format(
        #             str(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')), rtv_obj.id)
        #         self.env.cr.execute(received_rtv_query)
        #         self.env.cr.commit()
        #
        #         received_rtv_query_line = "UPDATE rtv_process_line SET state='received', received_time='{0}' WHERE rtv_process_id={1}".format(
        #             str(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')), rtv_obj.id)
        #         self.env.cr.execute(received_rtv_query_line)
        #         self.env.cr.commit()
        #
        # return True

    def rtv_pqc_status_change(self, rtv_id):

        rtv_obj = self.env['rtv.process'].browse(rtv_id)
        for rtv in rtv_obj:
            rtv.write({
                'state': 'received',
                'delivered': True,
                'received_by': self.env.uid,
                'received_time': str(fields.Datetime.now()),
                # 'id': rtv.id,
            })

            rtv.rtv_process_line.write({
                'state': 'received',
                'received_time': str(fields.Datetime.now()),
            })

            # received_rtv_query = "UPDATE rtv_process SET state='received',delivered = TRUE ,received_time='{0}', received_by='{1}' WHERE id={2}".format(
            #     str(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')), singel_id.id)
            # self.env.cr.execute(received_rtv_query)
            # self.env.cr.commit()
            #
            # received_rtv_query_line = "UPDATE rtv_process_line SET state='received', received_time='{0}' WHERE rtv_process_id={1}".format(
            #     str(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')), singel_id.id)
            # self.env.cr.execute(received_rtv_query_line)
            # self.env.cr.commit()

            self.env['rtv.end.to.end'].rtv_received_update(rtv.name, rtv.id, 'received')

            self.env['rtv.process'].send_email(rtv.id)

        return True

    def receive_srn(self):
        if self.state == 'confirmed':
            for srn_data in self:
                srn_rtv_ids = self.srn_line
                stock_rtv = [i.rtv_id.id for i in srn_rtv_ids if not i.pqc and i.delivered == False]
                pqc_rtv = [i.rtv_id.id for i in srn_rtv_ids if i.pqc and i.delivered == False]

                if len(stock_rtv) > 0:
                    self.create_invoice(stock_rtv, nrr='')
                if len(pqc_rtv) > 0:
                    self.rtv_pqc_status_change(pqc_rtv)

                rtv_ids = stock_rtv + pqc_rtv
                if len(rtv_ids) > 1:
                    srn_data.srn_line.write({
                        'state': 'received',
                        'delivered': True,
                        'received_by': self.env.uid,
                        'received_time': str(fields.Datetime.now()),
                        'rtv_id': rtv_ids,
                    })

                    srn_data.srn_product_line.write({
                        'state': 'received',
                        'received_time': str(fields.Datetime.now()),
                        'rtv_id': rtv_ids,
                    })

                    # received_srn_rtv_line_query = "UPDATE srn_rtv_line SET state='received', delivered=TRUE ,received_by={0}, received_time='{1}' WHERE srn_rtv_line_id={2} and rtv_id IN {3}".format(
                    #     str(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')), single_id, tuple(rtv_ids))
                    # self.env.cr.execute(received_srn_rtv_line_query)
                    # self.env.cr.commit()
                    #
                    # received_srn_rtv_product_query = "UPDATE srn_rtv_product SET state='received', received_time='{0}' WHERE srn_rtv_product_id={1} and rtv_id IN {2}".format(
                    #     str(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')), single_id, tuple(rtv_ids))
                    # self.env.cr.execute(received_srn_rtv_product_query)
                    # self.env.cr.commit()

                else:

                    srn_data.srn_line.write({
                        'state': 'received',
                        'delivered': True,
                        'received_by': self.env.uid,
                        'received_time': str(fields.Datetime.now()),
                        'rtv_id': rtv_ids,
                    })

                    srn_data.srn_product_line.write({
                        'state': 'received',
                        'received_time': str(fields.Datetime.now()),
                        'rtv_id': rtv_ids,
                    })
                    srn_data.write({
                        'state': 'received',
                        'delivered': True,
                        'received_by': self.env.uid,
                        'received_time': str(fields.Datetime.now()),
                    })
                #     received_srn_rtv_line_query = "UPDATE srn_rtv_line SET state='received',delivered=TRUE , received_by={0}, received_time='{1}' WHERE srn_rtv_line_id={2} and rtv_id = {3}".format(
                #         str(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')), single_id, rtv_ids[0])
                #     self.env.cr.execute(received_srn_rtv_line_query)
                #     self.env.cr.commit()
                #
                #     received_srn_rtv_product_query = "UPDATE srn_rtv_product SET state='received', received_time='{0}' WHERE srn_rtv_product_id={1} and rtv_id = {2}".format(
                #         str(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')), single_id, rtv_ids[0])
                #     self.env.cr.execute(received_srn_rtv_product_query)
                #     self.env.cr.commit()
                #
                # received_srn_query = "UPDATE srn_process SET state='received',delivered = TRUE ,received_by={0},received_time='{1}' " \
                #                      "WHERE id={2}".format(str(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')), single_id)
                # self.env.cr.execute(received_srn_query)
                # self.env.cr.commit()
        else:
            raise Warning(_('This SRN already processed.'))
        return True

    def not_receive_srn(self, rtv_id, not_receive_reason, ):
        rtv_data = self.env['rtv.process'].browse(rtv_id)

        for rtv_obj in rtv_data:
            rtv_data.write({
                'state': 'not_received',
                'not_receive_reason': not_receive_reason,
                'not_received_by': self.env.uid,
                'not_received_time': str(fields.Datetime.now()),
                'srn_rtv_number': '',
            })

            rtv_data.rtv_process_line.write({
                'state': 'not_received',
                'not_received_time': str(fields.Datetime.now())
            })

            # received_rtv_query = "UPDATE rtv_process SET state='not_received', not_receive_reason='{0}', not_received_time='{1}', not_received_by='{2}',srn_rtv_number= NULL WHERE id={3}".format(
            #     self.not_receive_reason,
            #     str(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')), uid, rtv_obj.id)
            # self.env.cr.execute(received_rtv_query)
            # self.env.cr.commit()
            #
            # received_rtv_query_line = "UPDATE rtv_process_line SET state='not_received', not_received_time='{0}' WHERE rtv_process_id={1}".format(
            #     str(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')), rtv_obj.id)
            # self.env.cr.execute(received_rtv_query_line)
            # self.env.cr.commit()

            self.env['rtv.end.to.end'].rtv_not_received_update(rtv_obj.name, rtv_obj.id, 'not_received')

            self.email_template(rtv_obj.id)

        # return True

    def cancel_srn(self):
        for srn in self:
            srn.write({
                'state': 'cancel',
                'cancel_by': self.env.uid,
                'cancel_time': fields.Datetime.now()
            })

            srn.srn_line.write({
                'state': 'cancel',
                'cancel_by': self.env.uid,
                'cancel_time': fields.Datetime.now()
            })

            srn.srn_product_line.write({
                'state': 'cancel',
                'cancel_time': fields.Datetime.now()
            })

        # for single_id in self.ids:
        #     cancel_srn_query = "UPDATE srn_process SET state='cancel', cancel_by={0},cancel_time='{1}' WHERE id={2}".format(
        #         str(fields.datetime.now()), single_id)
        #     self.env.cr.execute(cancel_srn_query)
        #     self.env.cr.commit()
        #
        #     cancel_srn_rtv_line_query = "UPDATE srn_rtv_line SET state='cancel', cancel_by={0}, cancel_time='{1}' WHERE srn_rtv_line_id={2}".format(
        #         str(fields.datetime.now()), single_id)
        #     self.env.cr.execute(cancel_srn_rtv_line_query)
        #     self.env.cr.commit()
        #
        #     cancel_srn_rtv_product_query = "UPDATE srn_rtv_product SET state='cancel', cancel_time='{0}' WHERE srn_rtv_product_id={1}".format(
        #         str(fields.datetime.now()), single_id)
        #     self.env.cr.execute(cancel_srn_rtv_product_query)
        #     self.env.cr.commit()
        #
        # return True

    def create_invoice(self, rtv_id, nrr=None):
        for rtv_odj in self.env['rtv.process'].browse(rtv_id):
            if rtv_odj.state != 'cancel':
                if rtv_odj.stock_id and rtv_odj.stock_id.state == "assigned":
                    picking = [rtv_odj.stock_id.id]

                    if nrr != '':
                        for rtv_line in rtv_odj.rtv_process_line:
                            for stock_move_line in rtv_odj.stock_id.move_line_ids:
                                if rtv_line.product_id.id == stock_move_line.product_id.id:
                                    stock_move_line.qty_done = rtv_line.return_qty

                        rtv_odj.stock_id.with_context(skip_backorder=True, picking_ids_not_to_backorder=rtv_odj.stock_id.ids) \
                            .button_validate()
                    else:
                        rtv_odj.stock_id.automatic_stock_picking_validate()

                    if rtv_odj.stock_id.state == "done":
                        # Create Supplier Refund Invoice
                        invoice_line_ids = []
                        for rtv_line in rtv_odj.rtv_process_line:
                            if rtv_line.return_qty > 0:
                                invoice_line_ids.append((0, None, {
                                    'product_id': rtv_line.product_id.id,
                                    'quantity': rtv_line.return_qty,
                                    'price_unit': rtv_line.unit_cost,
                                }))

                        invoice = self.env['account.move'].create({
                            'move_type': 'in_refund',
                            'partner_id': rtv_odj.vendor_id.id,
                            'invoice_date': fields.Datetime.now(),
                            'currency_id': self.env.company.currency_id.id,
                            'ref': rtv_odj.name,
                            'invoice_line_ids': invoice_line_ids,
                            'rtv': True,
                            'rtv_id': rtv_odj.id,
                        })

                        invoice.action_post()

                        rtv_odj.write({
                            'state': 'received',
                            'delivered': True,
                            'received_by': self.env.uid,
                            'received_time': fields.Datetime.now(),
                            'not_receive_reason': nrr,
                            'invoice_id': invoice.id
                        })
                        rtv_odj.rtv_process_line.write({
                            'state': 'received',
                            'received_time': fields.Datetime.now()
                        })
                        self.env['rtv.end.to.end'].rtv_received_update(rtv_odj.name, rtv_odj.id, 'received')
                        self.email_template(rtv_odj.id)

                        remaining_stock_id = self.env['stock.picking'].search([('backorder_id', '=', picking[0])])
                        if remaining_stock_id:
                            remaining_stock_id.do_unreserve()

                            remaining_stock_id.action_cancel()

                else:
                    raise Warning(_("The related picking %s is not in assigned state", rtv_odj.stock_id.name))

                # created_id = self.env['stock.transfer_details'].create(
                #     {'picking_id': len(picking) and picking[0] or False})
                #
                # if created_id:
                #     created_id = self.env['stock.transfer_details'].browse(created_id)
                #     if created_id.picking_id.state in ['assigned', 'partially_available']:
                #         processed_ids = []
                #
                #         # Create new and update existing rtv operations
                #         for lstits in [created_id.item_ids, created_id.packop_ids]:
                #             for prod in lstits:
                #                 for rtv_line in rtv_odj.rtv_process_line:
                #                     if prod.product_id.id == rtv_line.product_id.id:
                #                         pack_datas = {
                #                             'product_id': prod.product_id.id,
                #                             'product_uom_id': prod.product_uom_id.id,
                #                             'product_qty': rtv_line.return_qty,
                #                             'package_id': prod.package_id.id,
                #                             'lot_id': prod.lot_id.id,
                #                             'location_id': prod.sourceloc_id.id,
                #                             'location_dest_id': prod.destinationloc_id.id,
                #                             'result_package_id': prod.result_package_id.id,
                #                             'date': prod.date if prod.date else datetime.datetime.now(),
                #                             'owner_id': prod.owner_id.id,
                #                         }
                #                         if prod.packop_id:
                #                             prod.packop_id.with_context(no_recompute=True).write(pack_datas)
                #                             processed_ids.append(prod.packop_id.id)
                #                         else:
                #                             pack_datas['picking_id'] = created_id.picking_id.id
                #                             packop_id = created_id.env['stock.pack.operation'].create(pack_datas)
                #                             processed_ids.append(packop_id.id)
                #         # Delete the others
                #         packops = created_id.env['stock.pack.operation'].search(
                #             ['&', ('picking_id', '=', created_id.picking_id.id), '!', ('id', 'in', processed_ids)])
                #         packops.unlink()
                #         # Execute the transfer of the picking
                #         created_id.picking_id.do_transfer()
                #
                #     if created_id.picking_id.invoice_state != 'invoiced':
                #         # Now Create the Invoice for this picking
                #         picking_pool = self.env['stock.picking']
                #         from datetime import date
                #         context['date_inv'] = date.today()
                #         context['inv_type'] = 'in_refund'
                #         active_ids = [rtv_odj.stock_id.id]
                #         res = picking_pool.action_invoice_create(active_ids,
                #                                                  journal_id=1136,
                #                                                  group=True,
                #                                                  type='in_refund', )
                #         invoice_line = []
                #         total = 0
                #         if res:
                #             invoice_data = self.env['account.move'].browse(res[0])[0]
                #             invoice_data.signal_workflow('invoice_open')
                #
                #             # Following Query flag that invoice and transfer will set boolean value
                #             invoice_data.write({
                #                 'rtv': True,
                #                 'rtv_id': rtv_odj.id,
                #             })
                #             rtv_odj.write({
                #                 'state': 'received',
                #                 'delivered': True,
                #                 'received_by': self.env.uid,
                #                 'received_time': str(fields.Datetime.now()),
                #                 'not_receive_reason': nrr,
                #                 'invoice_id': res[0]
                #             })
                #
                #             rtv_odj.rtv_process_line.write({
                #                 'state': 'received',
                #                 'received_time': str(fields.Datetime.now())
                #             })
                #
                #             # self.env.cr.execute("update account_invoice set rtv_id=%s where id=%s", ([rtv_odj.id, res[0]]))
                #             # self.env.cr.execute("update account_invoice set rtv='True' where id=%s", ([res[0]]))
                #             #
                #             # received_rtv_query = "UPDATE rtv_process SET state='received',delivered = TRUE ,received_time='{0}', received_by='{1}',not_receive_reason='{2}',invoice_id={3} WHERE id={4}".format(
                #             #     str(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')), receiver,nrr, res[0],rtv_odj.id)
                #             # self.env.cr.execute(received_rtv_query)
                #             # self.env.cr.commit()
                #             #
                #             # received_rtv_query_line = "UPDATE rtv_process_line SET state='received', received_time='{0}' WHERE rtv_process_id={1}".format(
                #             #     str(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')), rtv_odj.id)
                #             # self.env.cr.execute(received_rtv_query_line)
                #             # self.env.cr.commit()
                #
                #             self.env['rtv.end.to.end'].rtv_received_update(rtv_odj.name, rtv_odj.id, 'received')
                #             self.email_template(rtv_odj.id)
                #

            else:
                raise Warning(_("The RTV %s is in cancel state", rtv_odj.name))

        # return True

    def email_template(self, id):
        self.env['rtv.process'].send_email(id)
        return True


class SrnRtvLine(models.Model):
    _name = "srn.rtv.line"
    _description = "SRN RTV Line"

    srn_rtv_line_id = fields.Many2one('srn.process', 'SRN Process ID')

    stock_id = fields.Many2one('stock.picking', 'Stock Number')

    reverse_stock_id = fields.Many2one('stock.picking', 'Reverse Stock Number')

    pqc = fields.Boolean('PQC')
    pqc_id = fields.Many2one("pending.quality.control", 'PQC ID')
    pqc_number = fields.Char('PQC Number')

    rtv_name = fields.Char('RTV Number')
    rtv_id = fields.Many2one('rtv.process', 'RTV Process ID')
    delivered = fields.Boolean('Delivered')

    received = fields.Boolean('Received')
    received_by = fields.Many2one('res.users', 'Received By')
    received_time = fields.Datetime('Received Time')

    not_received = fields.Boolean('Not Received')
    not_received_by = fields.Many2one('res.users', 'Not Received By')
    not_received_time = fields.Datetime('Not Received Time')
    confirmed_by = fields.Many2one('res.users', 'Confirmed By')
    confirmed_time = fields.Datetime('Confirmed Time')

    requested_by = fields.Many2one('res.users', 'Requested By')
    requested_time = fields.Datetime('Requested Time')

    resolved = fields.Boolean('Resolved')
    resolved_by = fields.Many2one('res.users', 'Resolved By')
    resolved_time = fields.Datetime('Resolved Time')

    cancel = fields.Boolean('Cancel')
    cancel_by = fields.Many2one('res.users', 'Cancel By')
    cancel_time = fields.Datetime('Cancel Time')
    state = fields.Selection([
        ('pending', 'Pending'),
        ('confirmed', 'Confirmed'),
        ('received', 'Received'),
        ('not_received', 'Not Received'),
        ('partial_received', 'Partial Received'),
        ('cancel', 'Cancelled'),
    ], 'Status', readonly=True, copy=False, help="Gives the status of the SRN Process", index=True)

    cancel_reason = fields.Text('Cancel Reason')

    not_receive_reason_date = fields.Datetime('Not Receive Reason Date')
    not_receive_reason_by = fields.Many2one('res.users', 'Not Receive Reason By')

    not_receive_reason = fields.Selection([
        ("Shop off", "Shop off"),
        ("Vendor unreachable", "Vendor unreachable"),
        ("Vendor Rescheduled", "Vendor Rescheduled"),
        ("Does not agree to take", "Does not agree to take"),
        ("Not received due to quality", "Not received due to quality"),
        ("Vendor is not aware of", "Vendor is not aware of")

    ], 'Not Receive Reason', copy=False, help="Reason", index=True)

    cancel_srn_rtv_date = fields.Datetime('Cancel SRN RTV Date')
    cancel_srn_rtv_by = fields.Many2one('res.users', 'Cancel SRN RTV By')
    cancel_srn_rtv_reason = fields.Text('Cancel SRN RTV Reason')
    srn_rtv_number = fields.Char('Challan Number')

    #### Block code for V-14
    # _columns = {
    #
    #     'srn_rtv_line_id': fields.many2one('srn.process', 'SRN Process ID'),
    #
    #     'stock_id': fields.many2one('stock.picking', 'Stock Number'),
    #
    #     'reverse_stock_id': fields.many2one('stock.picking', 'Reverse Stock Number'),
    #
    #     'pqc': fields.boolean('PQC'),
    #     'pqc_id': fields.many2one("pending.quality.control", 'PQC ID'),
    #     'pqc_number': fields.char('PQC Number'),
    #
    #     'rtv_name': fields.char('RTV Number'),
    #     'rtv_id': fields.many2one('rtv.process', 'RTV Process ID'),
    #     'delivered': fields.boolean('Delivered'),
    #
    #     'received': fields.boolean('Received'),
    #     'received_by': fields.many2one('res.users', 'Received By'),
    #     'received_time': fields.datetime('Received Time'),
    #
    #     'not_received': fields.boolean('Not Received'),
    #     'not_received_by': fields.many2one('res.users', 'Not Received By'),
    #     'not_received_time': fields.datetime('Not Received Time'),
    #
    #     'confirmed_by': fields.many2one('res.users', 'Confirmed By'),
    #     'confirmed_time': fields.datetime('Confirmed Time'),
    #
    #     'requested_by': fields.many2one('res.users', 'Requested By'),
    #     'requested_time': fields.datetime('Requested Time'),
    #
    #     'resolved': fields.boolean('Resolved'),
    #     'resolved_by': fields.many2one('res.users', 'Resolved By'),
    #     'resolved_time': fields.datetime('Resolved Time'),
    #
    #     'cancel': fields.boolean('Cancel'),
    #     'cancel_by': fields.many2one('res.users', 'Cancel By'),
    #     'cancel_time': fields.datetime('Cancel Time'),
    #
    #     'state': fields.selection([
    #         ('pending', 'Pending'),
    #         ('confirmed', 'Confirmed'),
    #         ('received', 'Received'),
    #         ('not_received', 'Not Received'),
    #         ('partial_received', 'Partial Received'),
    #         ('cancel', 'Cancelled'),
    #
    #     ], 'Status', readonly=True, copy=False, help="Gives the status of the SRN Process", index=True),
    #
    #     'cancel_reason': fields.text('Cancel Reason'),
    #
    #     'not_receive_reason_date': fields.datetime('Not Receive Reason Date'),
    #     'not_receive_reason_by': fields.many2one('res.users', 'Not Receive Reason By'),
    #     # 'not_receive_reason': fields.text('Not Receive SRN RTV Reason'),
    #
    #     # 'not_receive_reason': fields.selection([
    #     #     ("Vendor was unreachable", "Vendor was unreachable"),
    #     #     ("Vendor refused to provide", "Vendor refused to provide"),
    #     #     ("Unable to reach", "Unable to reach"),
    #     #     ("Vendors place was closed", "Vendors place was closed"),
    #     #     ("Cash problem", "Cash problem"),
    #     #     ("Vendor rescheduled", "Vendor rescheduled"),
    #     #     ("Wrong address given", "Wrong address given"),
    #     #     ("Unable to reach on Time", "Unable to reach on Time"),
    #     #     ("Quality issue", "Quality issue"),
    #     #     ("Due to traffic issue", "Due to traffic issue")
    #     #
    #     # ], 'Not Receive Reason', copy=False, help="Reason", index=True),
    #
    #     'not_receive_reason': fields.selection([
    #         ("Shop off", "Shop off"),
    #         ("Vendor unreachable", "Vendor unreachable"),
    #         ("Vendor Rescheduled", "Vendor Rescheduled"),
    #         ("Does not agree to take", "Does not agree to take"),
    #         ("Not received due to quality", "Not received due to quality"),
    #         ("Vendor is not aware of", "Vendor is not aware of")
    #
    #     ], 'Not Receive Reason', copy=False, help="Reason", index=True),
    #
    #     'cancel_srn_rtv_date': fields.datetime('Cancel SRN RTV Date'),
    #     'cancel_srn_rtv_by': fields.many2one('res.users', 'Cancel SRN RTV By'),
    #     'cancel_srn_rtv_reason': fields.text('Cancel SRN RTV Reason'),
    #     'srn_rtv_number': fields.char('Challan Number'),
    #
    # }
    ### ai method kothao use/call hoynai ###
    def not_receive_rtv_srn(self):
        for srn_line in self:
            srn_line.write({
                'state': 'not_received',
                'not_received_by': self.env.uid,
                'not_received_time': fields.Datetime.now()
            })

            srn_line.srn_rtv_line_id.write({
                'state': 'not_received',
                'not_received_by': self.env.uid,
                'not_received_time': fields.Datetime.now()
            })

        # for single_id in self.ids:
        #     not_received_srn_query = "UPDATE srn_process SET state='not_received', not_received_by={0}, not_received_time='{1}' WHERE id={2}".format(
        #         str(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')), single_id)
        #     self.env.cr.execute(not_received_srn_query)
        #     self.env.cr.commit()
        #
        #     not_received_srn_rtv_line_query = "UPDATE srn_rtv_line SET state='not_received', not_received_by={0}, not_received_time='{1}' WHERE srn_rtv_line_id={2}".format(
        #         str(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')), single_id)
        #     self.env.cr.execute(not_received_srn_rtv_line_query)
        #     self.env.cr.commit()
        #
        #     not_received_srn_rtv_product_query = "UPDATE srn_rtv_product SET state='not_received', not_received_time='{0}' WHERE srn_rtv_product_id={1}".format(
        #         str(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')), single_id)
        #     self.env.cr.execute(not_received_srn_rtv_product_query)
        #     self.env.cr.commit()
        #
        # return True


class SrnRtvProduct(models.Model):
    _name = "srn.rtv.product"
    _description = "SRN RTV Product"

    srn_rtv_product_id = fields.Many2one('srn.process', 'SRN RTV Product', required=True, index=True, readonly=True)

    stock_id = fields.Many2one('stock.picking', 'Stock Number')
    pqc = fields.Boolean('PQC')
    pqc_id = fields.Many2one("pending.quality.control", 'PQC ID')
    pqc_number = fields.Char('PQC Number')

    rtv_name = fields.Char('RTV Number')
    rtv_id = fields.Many2one('rtv.processs', 'RTV Process ID')

    product_id = fields.Many2one('product.product', 'Product')
    cancel_time = fields.Datetime('Cancel Time')
    resolved_time = fields.Datetime('Resolved Time')
    received_time = fields.Datetime('Received Time')
    not_received_time = fields.Datetime('Not Received Time')
    requested_time = fields.Datetime('Requested Time')
    confirmed_time = fields.Datetime('Confirmed Time')
    return_qty = fields.Float('Return Qty')
    remaining_qty = fields.Float('Remaining Qty')
    available_qty = fields.Float('Free Qty')
    requested_qty = fields.Float('Request Qty')
    unit_cost = fields.Float('Unit Rate')
    amount = fields.Float('Amount')
    cal_amount = fields.Float('')
    state = fields.Selection([
        ('pending', 'Pending'),
        ('confirmed', 'Confirmed'),
        ('received', 'Received'),
        ('not_received', 'Not Received'),
        ('partial_received', 'Partial Received'),
        ('cancel', 'Cancelled'),

    ], 'Status', readonly=True, copy=False, help="Gives the status of the SRN Process", index=True)

    not_receive_reason_date = fields.Datetime('Not Receive Reason Date')
    not_receive_reason_by = fields.Many2one('res.users', 'Not Receive Reason By')

    not_receive_reason = fields.Selection([
        ("Shop off", "Shop off"),
        ("Vendor unreachable", "Vendor unreachable"),
        ("Vendor Rescheduled", "Vendor Rescheduled"),
        ("Does not agree to take", "Does not agree to take"),
        ("Not received due to quality", "Not received due to quality"),
        ("Vendor is not aware of", "Vendor is not aware of")

    ], 'Not Receive Reason', copy=False, help="Reason", index=True)

    cancel_srn_rtv_date = fields.Datetime('Cancel SRN RTV Date')
    cancel_srn_rtv_by = fields.Many2one('res.users', 'Cancel SRN RTV By')
    cancel_srn_rtv_reason = fields.Text('Cancel SRN RTV Reason')

    srn_rtv_number = fields.Char('Challan Number')

    # ## Block code for V-14
    # _columns = {
    #     # 'srn_process_id': fields.many2one('srn.process', 'SRN Process ID', ondelete='cascade', index=True, readonly=True),
    #
    #     'srn_rtv_product_id': fields.many2one('srn.process', 'SRN RTV Product', required=True, ondelete='cascade',
    #                                           index=True, readonly=True),
    #
    #     'stock_id': fields.many2one('stock.picking', 'Stock Number'),
    #     'pqc': fields.boolean('PQC'),
    #     'pqc_id': fields.many2one("pending.quality.control", 'PQC ID'),
    #     'pqc_number': fields.char('PQC Number'),
    #
    #     'rtv_name': fields.char('RTV Number'),
    #     'rtv_id': fields.many2one('rtv.processs', 'RTV Process ID'),
    #
    #     'product_id': fields.many2one('product.product', 'Product'),
    #     'cancel_time': fields.datetime('Cancel Time'),
    #     'resolved_time': fields.datetime('Resolved Time'),
    #     'received_time': fields.datetime('Received Time'),
    #     'not_received_time': fields.datetime('Not Received Time'),
    #     'requested_time': fields.datetime('Requested Time'),
    #     'confirmed_time': fields.datetime('Confirmed Time'),
    #     'return_qty': fields.float('Return Qty'),
    #     'remaining_qty': fields.float('Remaining Qty'),
    #     'available_qty': fields.float('Free Qty'),
    #     'requested_qty': fields.float('Request Qty'),
    #     'unit_cost': fields.float('Unit Rate'),
    #     'amount': fields.float('Amount'),
    #     'cal_amount': fields.float(''),
    #     'state': fields.selection([
    #         ('pending', 'Pending'),
    #         ('confirmed', 'Confirmed'),
    #         ('received', 'Received'),
    #         ('not_received', 'Not Received'),
    #         ('partial_received', 'Partial Received'),
    #         ('cancel', 'Cancelled'),
    #
    #     ], 'Status', readonly=True, copy=False, help="Gives the status of the SRN Process", index=True),
    #
    #     'not_receive_reason_date': fields.datetime('Not Receive Reason Date'),
    #     'not_receive_reason_by': fields.many2one('res.users', 'Not Receive Reason By'),
    #     # 'not_receive_reason': fields.text('Not Receive SRN RTV Reason'),
    #
    #     # 'not_receive_reason': fields.selection([
    #     #     ("Vendor was unreachable", "Vendor was unreachable"),
    #     #     ("Vendor refused to provide", "Vendor refused to provide"),
    #     #     ("Unable to reach", "Unable to reach"),
    #     #     ("Vendors place was closed", "Vendors place was closed"),
    #     #     ("Cash problem", "Cash problem"),
    #     #     ("Vendor rescheduled", "Vendor rescheduled"),
    #     #     ("Wrong address given", "Wrong address given"),
    #     #     ("Unable to reach on Time", "Unable to reach on Time"),
    #     #     ("Quality issue", "Quality issue"),
    #     #     ("Due to traffic issue", "Due to traffic issue")
    #     #
    #     # ], 'Not Receive Reason', copy=False, help="Reason", index=True),
    #
    #     'not_receive_reason': fields.selection([
    #         ("Shop off", "Shop off"),
    #         ("Vendor unreachable", "Vendor unreachable"),
    #         ("Vendor Rescheduled", "Vendor Rescheduled"),
    #         ("Does not agree to take", "Does not agree to take"),
    #         ("Not received due to quality", "Not received due to quality"),
    #         ("Vendor is not aware of", "Vendor is not aware of")
    #
    #     ], 'Not Receive Reason', copy=False, help="Reason", index=True),
    #
    #     'cancel_srn_rtv_date': fields.datetime('Cancel SRN RTV Date'),
    #     'cancel_srn_rtv_by': fields.many2one('res.users', 'Cancel SRN RTV By'),
    #     'cancel_srn_rtv_reason': fields.text('Cancel SRN RTV Reason'),
    #
    #     'srn_rtv_number': fields.char('Challan Number'),
    #
    # }


class NotReceiveSrnRtvReason(models.Model):
    _name = "not.receive.srn.rtv.reason"
    _description = "Not Receive Reason"

    not_receive_reason_date = fields.Datetime('Not Receive Reason Date')
    not_receive_reason_by = fields.Many2one('res.users', 'Not Receive Reason By')
    not_receive_reason = fields.Selection([
        ("Shop off", "Shop off"),
        ("Vendor unreachable", "Vendor unreachable"),
        ("Vendor Rescheduled", "Vendor Rescheduled"),
        ("Does not agree to take", "Does not agree to take"),
        ("Not received due to quality", "Not received due to quality"),
        ("Vendor is not aware of", "Vendor is not aware of")

    ], 'Not Receive Reason', copy=False, help="Reason", index=True)

    # ### Block code for V-14
    # _columns = {
    #     'not_receive_reason_date': fields.datetime('Not Receive Reason Date'),
    #     'not_receive_reason_by': fields.many2one('res.users', 'Not Receive Reason By'),
    #
    #     # 'not_receive_reason': fields.selection([
    #     #     ("Vendor was unreachable", "Vendor was unreachable"),
    #     #     ("Vendor refused to provide", "Vendor refused to provide"),
    #     #     ("Unable to reach", "Unable to reach"),
    #     #     ("Vendors place was closed", "Vendors place was closed"),
    #     #     ("Cash problem", "Cash problem"),
    #     #     ("Vendor rescheduled", "Vendor rescheduled"),
    #     #     ("Wrong address given", "Wrong address given"),
    #     #     ("Unable to reach on Time", "Unable to reach on Time"),
    #     #     ("Quality issue", "Quality issue"),
    #     #     ("Due to traffic issue", "Due to traffic issue")
    #     #
    #     # ], 'Not Receive Reason', copy=False, help="Reason", index=True),
    #
    #     'not_receive_reason': fields.selection([
    #         ("Shop off", "Shop off"),
    #         ("Vendor unreachable", "Vendor unreachable"),
    #         ("Vendor Rescheduled", "Vendor Rescheduled"),
    #         ("Does not agree to take", "Does not agree to take"),
    #         ("Not received due to quality", "Not received due to quality"),
    #         ("Vendor is not aware of", "Vendor is not aware of")
    #
    #     ], 'Not Receive Reason', copy=False, help="Reason", index=True),
    #
    # }

    def not_receive_reason_def(self):
        ids = self.env.context['active_ids']
        not_receive_reason = str(self.env.context['not_receive_reason'])
        # not_receive_reason_by = self.env.uid
        # not_receive_reason_date = str(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'))

        srn_pro_obj = self.env['srn.process']
        srn_obj = srn_pro_obj.browse(ids)

        if srn_obj[0].state == 'confirmed':

            rtv_ids = [i.rtv_id.id for i in srn_obj.srn_line if i.delivered == False]

            for s_id in srn_obj:
                s_id.write({
                    'state': 'not_received',
                    'not_receive_reason': not_receive_reason,
                    'not_receive_reason_by': self.env.uid,
                    'not_receive_reason_date': fields.Datetime.now()
                })

                s_id.srn_line.write({
                    'state': 'not_received',
                    'not_receive_reason': not_receive_reason,
                    'not_receive_reason_by': self.env.uid,
                    'not_receive_reason_date': fields.Datetime.now()
                })

                s_id.srn_product_line.write({
                    'state': 'not_received',
                    'not_receive_reason': not_receive_reason,
                    'not_receive_reason_by': self.env.uid,
                    'not_receive_reason_date': fields.Datetime.now()
                })

                # for srn_line_id in srn_obj.srn_line:
                #     not_receive_srn_rtv_query = "UPDATE srn_rtv_line SET state='not_received', not_receive_reason='{0}', not_receive_reason_by={1},                                                 not_receive_reason_date='{2}' WHERE srn_rtv_line_id={3} and id={4}".format(
                #                                 not_receive_reason, not_receive_reason_by, not_receive_reason_date, s_id.id, srn_line_id.id)
                #     self.env.cr.execute(not_receive_srn_rtv_query)
                #     self.env.cr.commit()
                #
                # for srn_product_line_id in srn_obj.srn_product_line:
                #     not_receive_srn_product_query = "UPDATE srn_rtv_product SET state='not_received',not_receive_reason='{0}', not_receive_reason_by={1},                                                not_receive_reason_date='{2}' WHERE srn_rtv_product_id={3} and id={4}".format(
                #         not_receive_reason, not_receive_reason_by, not_receive_reason_date, s_id.id, srn_product_line_id.id)
                #     self.env.cr.execute(not_receive_srn_product_query)
                #     self.env.cr.commit()
                #
                # not_receive_srn_query = "UPDATE srn_process SET state='not_received', not_receive_reason='{0}', not_receive_reason_by={1},                                                   not_receive_reason_date='{2}' WHERE id={3}".format(
                #     not_receive_reason, not_receive_reason_by, not_receive_reason_date, s_id.id)
                # self.env.cr.execute(not_receive_srn_query)
                # self.env.cr.commit()

            srn_pro_obj.not_receive_srn(rtv_ids, not_receive_reason)
        else:
            raise Warning(_('This SRN already processed.'))

        # return True


class NotReceiveRTVReason(models.Model):
    _name = "not.receive.rtv.reason"
    _description = "Not Receive Reason"

    not_receive_reason_date = fields.Datetime('Not Receive Reason Date')
    not_receive_reason_by = fields.Many2one('res.users', 'Not Receive Reason By')

    not_receive_reason = fields.Selection([
        ("Shop off", "Shop off"),
        ("Vendor unreachable", "Vendor unreachable"),
        ("Vendor Rescheduled", "Vendor Rescheduled"),
        ("Does not agree to take", "Does not agree to take"),
        ("Not received due to quality", "Not received due to quality"),
        ("Vendor is not aware of", "Vendor is not aware of")

    ], 'Not Receive Reason', copy=False, help="Reason", index=True)

    srn_id = fields.Integer('SRN ID')
    rtv_id = fields.Integer('RTV ID')
    rtv_srn_id = fields.Integer(' SRN RTV Line ID')

    # ### Block code for V-14
    # _columns = {
    #     'not_receive_reason_date': fields.datetime('Not Receive Reason Date'),
    #     'not_receive_reason_by': fields.many2one('res.users', 'Not Receive Reason By'),
    #
    #     # 'not_receive_reason': fields.selection([
    #     #     ("Vendor was unreachable", "Vendor was unreachable"),
    #     #     ("Vendor refused to provide", "Vendor refused to provide"),
    #     #     ("Unable to reach", "Unable to reach"),
    #     #     ("Vendors place was closed", "Vendors place was closed"),
    #     #     ("Cash problem", "Cash problem"),
    #     #     ("Vendor rescheduled", "Vendor rescheduled"),
    #     #     ("Wrong address given", "Wrong address given"),
    #     #     ("Unable to reach on Time", "Unable to reach on Time"),
    #     #     ("Quality issue", "Quality issue"),
    #     #     ("Due to traffic issue", "Due to traffic issue")
    #     #
    #     # ], 'Not Receive Reason', copy=False, help="Reason", index=True),
    #
    #     'not_receive_reason': fields.selection([
    #         ("Shop off", "Shop off"),
    #         ("Vendor unreachable", "Vendor unreachable"),
    #         ("Vendor Rescheduled", "Vendor Rescheduled"),
    #         ("Does not agree to take", "Does not agree to take"),
    #         ("Not received due to quality", "Not received due to quality"),
    #         ("Vendor is not aware of", "Vendor is not aware of")
    #
    #     ], 'Not Receive Reason', copy=False, help="Reason", index=True),
    #
    #     'srn_id': fields.integer('SRN ID'),
    #     'rtv_id': fields.integer('RTV ID'),
    #     'rtv_srn_id': fields.integer(' SRN RTV Line ID'),
    #
    # }

    def default_get(self, fields):

        res = super(NotReceiveRTVReason, self).default_get(fields)
        srn_rtv = self.env['srn.rtv.line'].browse(self.env.context['active_id'])

        res['srn_id'] = int(srn_rtv.srn_rtv_line_id)
        res['rtv_id'] = int(srn_rtv.rtv_id)
        res['rtv_srn_id'] = self.env.context['active_id']

        return res

    def rtv_wise_not_receive(self):

        get_data = self.read()[0]
        nrr = self.not_receive_reason

        srn_pro_obj = self.env['srn.process']

        srn_obj = srn_pro_obj.browse(int(get_data['srn_id']))

        if nrr != ' ':
            srn_pro_obj.not_receive_srn([int(get_data['rtv_id'])], nrr)

            for srl in self.env['srn.rtv.line'].browse(int(get_data['rtv_srn_id'])):
                srl.write({
                    'state': 'not_received',
                    'not_receive_reason': nrr,
                    'received_by': self.env.uid,
                    'received_time': fields.Datetime.now(),
                })

            for srp in self.env['srn.rtv.product'].browse(int(get_data['rtv_id'])):
                srp.write({
                    'state': 'not_received',
                    'received_time': fields.Datetime.now(),
                })

            # received_srn_rtv_line_query = "UPDATE srn_rtv_line SET state='not_received', not_receive_reason='{0}', received_by={1}, received_time='{2}' WHERE srn_rtv_line_id={3} and id = {4}".
            # format(nrr, uid, str(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')), int(get_data['srn_id']), int(get_data['rtv_srn_id']))
            # self.env.cr.execute(received_srn_rtv_line_query)
            # self.env.cr.commit()
            #
            # received_srn_rtv_product_query = "UPDATE srn_rtv_product SET state='not_received', received_time='{0}' WHERE srn_rtv_product_id={1} and rtv_id = {2}".
            # format(str(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')), int(get_data['srn_id']), int(get_data['rtv_id']))
            # self.env.cr.execute(received_srn_rtv_product_query)
            # self.env.cr.commit()

            res = [srn_line.state for srn_line in srn_obj.srn_line]

            if len(list(set(res))) == 1:
                srn_obj.write({
                    'state': 'not_received',
                    'delivered': True,
                    'received_by': self.env.uid,
                    'received_time': fields.Datetime.now(),
                })

                # not_received_srn_query = "UPDATE srn_process SET state='not_received', delivered = TRUE , received_by={0}, received_time='{1}' WHERE id={2}".format(
                #     str(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')), int(get_data['srn_id']))
                # self.env.cr.execute(not_received_srn_query)
                # self.env.cr.commit()

            elif 'confirmed' in res:
                srn_obj.write({
                    'state': 'confirmed',
                    'delivered': True,
                    'received_by': self.env.uid,
                    'received_time': fields.Datetime.now(),
                })

                # not_received_srn_query = "UPDATE srn_process SET state='confirmed', delivered = TRUE , received_by={0}, received_time='{1}' WHERE id={2}".format(
                #     str(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')), int(get_data['srn_id']))
                # self.env.cr.execute(not_received_srn_query)
                # self.env.cr.commit()

            else:
                srn_obj.write({
                    'state': 'partial_received',
                    'delivered': True,
                    'received_by': self.env.uid,
                    'received_time': fields.Datetime.now(),
                })

                # partial_received_srn_query = "UPDATE srn_process SET state='partial_received', delivered = TRUE , received_by={0}, received_time='{1}' WHERE id={2}".format(
                #     str(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')), int(get_data['srn_id']))
                # self.env.cr.execute(partial_received_srn_query)
                # self.env.cr.commit()


        else:
            raise Warning(_('Please, Given actual reason.'))
        # return True


class CancelSrnRtvReason(models.Model):
    _name = "cancel.srn.rtv.reason"
    _description = "Cancel SRN RTV Reason"

    cancel_srn_rtv_date = fields.Datetime('Cancel SRN RTV Date')
    cancel_srn_rtv_by = fields.Many2one('res.users', 'Cancel SRN RTV By')
    cancel_srn_rtv_reason = fields.Text('Cancel SRN RTV Reason', required=True)

    # _columns = {
    #     'cancel_srn_rtv_date': fields.datetime('Cancel SRN RTV Date'),
    #     'cancel_srn_rtv_by': fields.many2one('res.users', 'Cancel SRN RTV By'),
    #     'cancel_srn_rtv_reason': fields.text('Cancel SRN RTV Reason', required=True),
    #
    # }

    def cancel_srn_rtv_reason_def(self):
        ids = self.env.context['active_ids']
        cancel_srn_rtv_reason = str(self.env.context['cancel_srn_rtv_reason'])

        srn_pro_obj = self.env['srn.process']

        srn_obj = self.env['srn.process'].browse(ids)

        srn_obj.write({
            'cancel_srn_rtv_reason': cancel_srn_rtv_reason,
            'cancel_srn_rtv_by': self.env.uid,
            'cancel_srn_rtv_date': fields.datetime.now(),
        })
        srn_obj.srn_line.write({
            'cancel_srn_rtv_reason': cancel_srn_rtv_reason,
            'cancel_srn_rtv_by': self.env.uid,
            'cancel_srn_rtv_date': fields.datetime.now(),
        })
        srn_obj.srn_product_line.write({
            'cancel_srn_rtv_reason': cancel_srn_rtv_reason,
            'cancel_srn_rtv_by': self.env.uid,
            'cancel_srn_rtv_date': fields.datetime.now(),
        })

        srn_obj.cancel_srn()

        # for s_id in ids:
        # cancel_srn_rtv_query = "UPDATE srn_process SET cancel_srn_rtv_reason='{0}', cancel_srn_rtv_by={1}, cancel_srn_rtv_date='{2}' WHERE id={3}".format(
        #     cancel_srn_rtv_reason, cancel_srn_rtv_by, cancel_srn_rtv_date, s_id)
        # self.env.cr.execute(cancel_srn_rtv_query)
        # self.env.cr.commit()

        # for srn_line_id in srn_obj.srn_line:
        #     cancel_srn_rtv_query = "UPDATE srn_rtv_line SET cancel_srn_rtv_reason='{0}', cancel_srn_rtv_by={1}, cancel_srn_rtv_date='{2}' WHERE srn_rtv_line_id={3} and id={4}".format(
        #         cancel_srn_rtv_reason, cancel_srn_rtv_by, cancel_srn_rtv_date, srn_line_id.id, s_id)
        #     self.env.cr.execute(cancel_srn_rtv_query)
        #     self.env.cr.commit()
        #
        #     self.env['rtv.process'].cancel_rtv([srn_line_id.rtv_id.id])
        #
        # for srn_product_line_id in srn_obj.srn_product_line:
        #     cancel_srn_rtv_query = "UPDATE srn_rtv_product SET cancel_srn_rtv_reason='{0}', cancel_srn_rtv_by={1}, cancel_srn_rtv_date='{2}' WHERE srn_rtv_product_id={3} and id={4}".format(
        #         cancel_srn_rtv_reason, cancel_srn_rtv_by, cancel_srn_rtv_date, srn_product_line_id.id, s_id)
        #     self.env.cr.execute(cancel_srn_rtv_query)
        #     self.env.cr.commit()

        # return True


class ReceiveSrnRtnWizard(models.Model):
    _name = "receive.srn.rtv.wizard"
    _description = "Receive Srn Rtn Wizard"

    name = fields.Char('RTV Number')
    warehouse_id = fields.Many2one('stock.warehouse', string='Warehouse', readonly=True, copy=False, index=True)

    # warehouse_id = fields.Selection([
    #     (1, 'Central Warehouse Uttara'),
    #     (2, 'Nodda Warehouse'),
    #     (3, 'Nodda Retail Warehouse'),
    #     (4, 'Motijheel Warehouse'),
    #     (6, 'Uttara Retail Warehouse'),
    #     (7, 'Motijheel Retail Warehouse'),
    #     (8, 'Damage Warehouse'),
    #
    # ], 'Warehouse', readonly=True, copy=False, help="Gives the List of the Warehouses", index=True)

    not_receive_reason = fields.Selection([
        ("Shop off", "Shop off"),
        ("Vendor unreachable", "Vendor unreachable"),
        ("Vendor Rescheduled", "Vendor Rescheduled"),
        ("Does not agree to take", "Does not agree to take"),
        ("Not received due to quality", "Not received due to quality"),
        ("Vendor is not aware of", "Vendor is not aware of")

    ], 'Not Receive Reason', copy=False, help="Reason", index=True)

    rtv_type = fields.Selection([
        ('pqc', 'PQC'),
        ('stock', 'Stock')

    ], 'RTV Type', readonly=True, copy=False, help="Gives the RTV Type ", index=True)
    vendor_id = fields.Integer('Vendor Id')
    rtv_srn_id = fields.Integer('')
    rtv_id = fields.Integer('')
    srn_rtv_line_id = fields.Integer('')
    vendor_name = fields.Char('Vendor')
    area = fields.Char('Area')
    remark = fields.Text('Remark')
    ref_po_number = fields.Char('Ref PO Number')
    pqc = fields.Boolean('PQC')
    pqc_id = fields.Many2one("pending.quality.control", 'PQC ID')
    pqc_number = fields.Char('PQC Number')
    state = fields.Selection([
        ('pending', 'Pending'),
        ('requested', 'Requested'),
        ('resolved', 'Resolved'),
        ('confirmed', 'Ready for Return'),
        ('create_srn', 'SRN'),
        ('received', 'Received'),
        ('cancel', 'Cancelled'),

    ], 'Status', readonly=True, copy=False, help="Gives the status of the RTV Process", index=True)

    stock_id = fields.Many2one('stock.picking', 'Stock Number')

    resolve_rtv_process_date = fields.Datetime('Resolve rtv Process Date')
    resolve_rtv_process_by = fields.Many2one('res.users', 'Resolve rtv Process By')
    resolve_rtv_process_reason = fields.Text('Resolve rtv Process Reason')

    cancel_rtv_process_date = fields.Datetime('Cancel rtv Process Date')
    cancel_rtv_process_by = fields.Many2one('res.users', 'Cancel rtv Process By')
    cancel_rtv_process_reason = fields.Text('Cancel rtv Process Reason')

    rtv_product_line_ids = fields.One2many('rtv.product.line.wizard', 'rtv_process_id', 'Delivery Line',
                                           required=True)

    # ### Block code for V-14
    #
    # _columns = {
    #     'name': fields.char('RTV Number'),
    #     'warehouse_id': fields.selection([
    #         (1, 'Central Warehouse Uttara'),
    #         (2, 'Nodda Warehouse'),
    #         (3, 'Nodda Retail Warehouse'),
    #         (4, 'Motijheel Warehouse'),
    #         (6, 'Uttara Retail Warehouse'),
    #         (7, 'Motijheel Retail Warehouse'),
    #         (8, 'Damage Warehouse'),
    #
    #     ], 'Warehouse', readonly=True, copy=False, help="Gives the List of the Warehouses", index=True),
    #
    #     # 'not_receive_reason': fields.selection([
    #     #
    #     #     ("Vendor was unreachable", "Vendor was unreachable"),
    #     #     ("Vendor refused to provide", "Vendor refused to provide"),
    #     #     ("Unable to reach", "Unable to reach"),
    #     #     ("Vendors place was closed", "Vendors place was closed"),
    #     #     ("Cash problem", "Cash problem"),
    #     #     ("Vendor rescheduled", "Vendor rescheduled"),
    #     #     ("Wrong address given", "Wrong address given"),
    #     #     ("Unable to reach on Time", "Unable to reach on Time"),
    #     #     ("Quality issue", "Quality issue"),
    #     #     ("Due to traffic issue", "Due to traffic issue"),
    #     #     ("Invoice/Cash Memo not provided", "Invoice/Cash Memo not provided")
    #     #
    #     # ], 'Not Received Reason', copy=False, help="Reason", index=True),
    #
    #     'not_receive_reason': fields.selection([
    #         ("Shop off", "Shop off"),
    #         ("Vendor unreachable", "Vendor unreachable"),
    #         ("Vendor Rescheduled", "Vendor Rescheduled"),
    #         ("Does not agree to take", "Does not agree to take"),
    #         ("Not received due to quality", "Not received due to quality"),
    #         ("Vendor is not aware of", "Vendor is not aware of")
    #
    #     ], 'Not Receive Reason', copy=False, help="Reason", index=True),
    #
    #     'rtv_type': fields.selection([
    #         ('pqc', 'PQC'),
    #         ('stock', 'Stock')
    #
    #     ], 'RTV Type', readonly=True, copy=False, help="Gives the RTV Type ", index=True),
    #     'vendor_id': fields.integer('Vendor Id'),
    #     'rtv_srn_id': fields.integer(''),
    #     'rtv_id': fields.integer(''),
    #     'srn_rtv_line_id': fields.integer(''),
    #     'vendor_name': fields.char('Vendor'),
    #     'area': fields.char('Area'),
    #     'remark': fields.text('Remark'),
    #     'ref_po_number': fields.char('Ref PO Number'),
    #     'pqc': fields.boolean('PQC'),
    #     'pqc_id': fields.many2one("pending.quality.control", 'PQC ID'),
    #     'pqc_number': fields.char('PQC Number'),
    #     'state': fields.selection([
    #         ('pending', 'Pending'),
    #         ('requested', 'Requested'),
    #         ('resolved', 'Resolved'),
    #         ('confirmed', 'Ready for Return'),
    #         ('create_srn', 'SRN'),
    #         ('received', 'Received'),
    #         ('cancel', 'Cancelled'),
    #
    #     ], 'Status', readonly=True, copy=False, help="Gives the status of the RTV Process", index=True),
    #     'rtv_product_line': fields.one2many('rtv.product.line.wizard', 'rtv_process_id', 'RTV Process Line',
    #                                         required=True),
    #     'stock_id': fields.many2one('stock.picking', 'Stock Number'),
    #
    #     'resolve_rtv_process_date': fields.datetime('Resolve rtv Process Date'),
    #     'resolve_rtv_process_by': fields.many2one('res.users', 'Resolve rtv Process By'),
    #     'resolve_rtv_process_reason': fields.text('Resolve rtv Process Reason'),
    #
    #     'cancel_rtv_process_date': fields.datetime('Cancel rtv Process Date'),
    #     'cancel_rtv_process_by': fields.many2one('res.users', 'Cancel rtv Process By'),
    #     'cancel_rtv_process_reason': fields.text('Cancel rtv Process Reason'),
    # }

    def default_get(self, fields):

        res = super(ReceiveSrnRtnWizard, self).default_get(fields)
        srn_rtv = self.env['srn.rtv.line'].browse(self.env.context['active_id'])
        rtv_obj = self.env['rtv.process'].browse(srn_rtv.rtv_id.id)
        items = []
        for lines in rtv_obj.rtv_process_line:
            line = lines[0]
            if float(line.requested_qty) > 0.00:
                item = {
                    'rtv_product_id': int(line.id),
                    'product_id': int(line.product_id.id),
                    'requested_qty': float(line.requested_qty),
                    'return_qty': float(line.return_qty),
                    'remaining_qty': float(line.remaining_qty),
                    'amount': float(line.amount),
                    'cal_amount': float(line.cal_amount),
                    'unit_cost': float(line.unit_cost),
                }

                for product_line in self.env['srn.process'].browse(srn_rtv.srn_rtv_line_id.id,
                                                                   ).srn_product_line:
                    if line.product_id.id == product_line.product_id.id:
                        item['srn_rtv_product_line_id'] = int(product_line.id)

                items.append((0, 0, (item)))

        res['rtv_type'] = str(rtv_obj.rtv_type)
        res['warehouse_id'] = int(rtv_obj.warehouse_id)
        res['name'] = str(rtv_obj.name)
        res['pqc'] = (rtv_obj.pqc)
        res['vendor_id'] = int(rtv_obj.vendor_id.id)
        res['vendor_name'] = str(rtv_obj.vendor_id.name)
        res['pqc_id'] = int(rtv_obj.pqc_id.id) if rtv_obj.pqc_id else ''
        res['pqc_number'] = str(rtv_obj.pqc_number) if rtv_obj.pqc_number else ''
        res['area'] = str(rtv_obj.area.name) if rtv_obj.area else ''
        res['remark'] = str(rtv_obj.remark) if rtv_obj.remark else ''
        # res['po_number'] = str(rtv_obj.po_number) if rtv_obj.po_number else ''
        res['stock_id'] = (rtv_obj.stock_id.id) if rtv_obj.stock_id else ''
        res['pqc'] = rtv_obj.pqc
        res['srn_rtv_line_id'] = int(srn_rtv.srn_rtv_line_id)
        res['rtv_id'] = int(srn_rtv.rtv_id)
        res['rtv_srn_id'] = self.env.context['active_id']

        res.update(rtv_product_line_ids=items)

        return res

    def rtv_wise_receive(self):
        # get_data = self.read()[0]
        sum_rem_qty = sum([line.requested_qty - line.return_qty for line in self.rtv_product_line_ids])
        sum_req_qty = sum([line.requested_qty for line in self.rtv_product_line_ids])
        sum_return_qty = sum([line.return_qty for line in self.rtv_product_line_ids])
        nrr = self.not_receive_reason

        # for line in self.rtv_product_line_ids:
        #     # result = self.env['rtv.product.line.wizard'].browse(i)
        #     remaining_qty = line.requested_qty - line.return_qty
        #     sum_rem_qty += remaining_qty
        #     sum_req_qty += line.requested_qty
        #     sum_return_qty += line.return_qty
        #
        #     if remaining_qty < 0.00:
        #         raise Warning(_('Return quantity should be less then or equal to (requested quantity)!!!'))

        if sum_req_qty == sum_rem_qty:
            raise Warning(_('Return quantity should be less then or equal to (requested quantity)!!!'))

        elif sum_return_qty < 1.00:
            raise Warning(_('Return quantity should be greater then or equal to 1 !!!'))

        elif sum_rem_qty > 0.00 and (nrr == ' ' or nrr is False):
            raise Warning(_('Please give the Not Receive Reason!!!'))

        else:
            for line in self.rtv_product_line_ids:
                if (line.requested_qty - line.return_qty) < 0.00:
                    raise Warning(_('Return quantity should be less then or equal to (requested quantity)!!!'))
                # rtv_product = self.env['rtv.product.line.wizard'].browse(row)
                # result_remaining_quantity = rtv_product.requested_qty - rtv_product.return_qty
                # rtv_product.remaining_qty = result_remaining_quantity
                # rtv_product.cal_amount = line.return_qty * line.unit_cost
                # rtv_product.amount = rtv_product.return_qty * rtv_product.unit_cost

                for rpl in self.env['rtv.process.line'].search([('rtv_process_id', '=', self.rtv_id),
                                                                ('product_id', '=', line.product_id.id)], limit=1):
                    rpl.write({
                        'return_qty': line.return_qty,
                        'remaining_qty': line.requested_qty - line.return_qty,
                        'cal_amount': line.return_qty * line.unit_cost,
                        'amount': line.return_qty * line.unit_cost,
                    })

                # update_query_rtv_process_line = "UPDATE rtv_process_line SET return_qty='{0}'" \
                #                                 ",remaining_qty = '{1}',cal_amount='{2}',amount= '{3}' WHERE " \
                #                                 "rtv_process_id = {4} and id={5}".format(
                #     rtv_product.return_qty, rtv_product.remaining_qty,rtv_product.cal_amount,rtv_product.amount,
                #     int(get_data['rtv_id']), rtv_product.rtv_product_id)
                # cr.execute(update_query_rtv_process_line)
                # cr.commit()

                for srp in self.env['srn.rtv.product'].search([('srn_rtv_product_id', '=', self.srn_rtv_line_id),
                                                               ('rtv_id', '=', self.rtv_id),
                                                               ('id', '=', line.srn_rtv_product_line_id)]):
                    srp.write({
                        'state': 'received',
                        'return_qty': line.return_qty,
                        'remaining_qty': line.requested_qty - line.return_qty,
                        'received_time': fields.Datetime.now(),
                    })

                # srn_rtv_product_update_query = "UPDATE srn_rtv_product SET return_qty='{0}'" \
                #                                ",remaining_qty = '{1}' WHERE srn_rtv_product_id={2} and rtv_id = {3} and id={4}".format(
                #     rtv_product.return_qty, rtv_product.remaining_qty,
                #     int(get_data['srn_rtv_line_id']), int(get_data['rtv_id']), int(rtv_product.srn_rtv_product_line_id))
                # cr.execute(srn_rtv_product_update_query)
                # cr.commit()

            if self.pqc is False:
                self.env['srn.process'].create_invoice([self.rtv_id], nrr=nrr)
            else:
                self.env['srn.process'].rtv_pqc_status_change([self.rtv_id])
                if sum_rem_qty > 0.00:
                    self.rtv_to_pqc(self.rtv_id)

            for srl in self.env['srn.rtv.line'].browse(int(self.rtv_srn_id)):
                srl.write({
                    'state': 'received',
                    'delivered': True,
                    'received_by': self.env.uid,
                    'received_time': fields.Datetime.now(),
                })

            # received_srn_rtv_line_query = "UPDATE srn_rtv_line SET state='received',delivered=TRUE, received_by={0}, received_time='{1}' WHERE srn_rtv_line_id={2} and id = {3}".format(
            #     str(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')), int(get_data['srn_rtv_line_id']), int(get_data['rtv_srn_id']))
            # self.env.cr.execute(received_srn_rtv_line_query)
            # self.env.cr.commit()

            # received_srn_rtv_product_query = "UPDATE srn_rtv_product SET state='received', received_time='{0}' WHERE srn_rtv_product_id={1} and rtv_id = {2}".format(str(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')), int(get_data['srn_rtv_line_id']), int(get_data['rtv_id']))
            # self.env.cr.execute(received_srn_rtv_product_query)
            # self.env.cr.commit()

            srn_obj = self.env['srn.process'].browse(int(self.srn_rtv_line_id))
            res = [srn_line.state for srn_line in srn_obj.srn_line]

            if len(list(set(res))) == 1:
                srn_obj.write({
                    'state': 'received',
                    'delivered': True,
                    'received_by': self.env.uid,
                    'received_time': fields.Datetime.now(),
                })
                # received_srn_query = "UPDATE srn_process SET state='received', delivered = TRUE , received_by={0}, received_time='{1}' WHERE id={2}".format(
                #     str(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')), int(get_data['srn_rtv_line_id']))
                # self.env.cr.execute(received_srn_query)
                # self.env.cr.commit()
            elif 'confirmed' in res:
                srn_obj.write({
                    'state': 'confirmed',
                })
                # received_srn_query = "UPDATE srn_process SET state='confirmed' WHERE id={0}".format(
                #     str(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')), int(get_data['srn_rtv_line_id']))
                # self.env.cr.execute(received_srn_query)
                # self.env.cr.commit()
            else:
                srn_obj.write({
                    'state': 'partial_received',
                    'delivered': True,
                    'received_by': self.env.uid,
                    'received_time': fields.Datetime.now(),
                })
                # received_srn_query = "UPDATE srn_process SET state='partial_received',delivered = TRUE , received_by={0}, received_time='{1}' WHERE id={2}".format(
                #     str(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')), int(get_data['srn_rtv_line_id']))
                # self.env.cr.execute(received_srn_query)
                # self.env.cr.commit()

            # return True

    def reverse_stock_transfer(self, rtv_id, stock_id):

        id = rtv_id
        rtv_data = self.env['rtv.process'].browse(id)

        stock_picking_type_ids = self.env['stock.picking.type'].search([('name', 'like', 'Receipts')], )
        stock_picking_type_data = self.env['stock.picking.type'].browse(stock_picking_type_ids, )
        location_id = None
        location_dest_id = None
        picking_type_id = None

        for items in stock_picking_type_data:

            if items.warehouse_id.id == rtv_data.warehouse_id:
                location_dest_id = items.default_location_dest_id.id
                picking_type_id = items.id
                location_id = self.env['stock.location'].search([('name', '=', 'Suppliers')])[0]
        move_line = []

        stock_data = {}

        if location_id is not None and location_dest_id is not None:
            stock_data = {'origin': stock_id[1],
                          'message_follower_ids': False,
                          'carrier_tracking_ref': False,
                          'number_of_packages': 0,
                          'date_done': False,
                          'carrier_id': False,
                          'write_uid': False,
                          'partner_id': rtv_data.vendor_id.id,
                          'message_ids': False,
                          'note': False,
                          'picking_type_id': picking_type_id,
                          'move_type': 'one',
                          'company_id': 1,
                          'priority': '1',
                          'picking_type_code': False,
                          'owner_id': False,
                          'min_date': False,
                          'date': strftime("%Y-%m-%d %H:%M:%S", gmtime()),
                          'pack_operation_ids': [],
                          'carrier_code': 'custom',
                          'invoice_state': '2binvoiced'}
            tmp_dict = {}
            for rtv_line in rtv_data.rtv_process_line:
                move_line.append([0, False,
                                  {'product_uos_qty': rtv_line.requested_qty,
                                   'date_expected': strftime("%Y-%m-%d %H:%M:%S", gmtime()),
                                   'date': strftime("%Y-%m-%d %H:%M:%S", gmtime()),
                                   'product_id': rtv_line.product_id.id,
                                   'product_uom': 1,
                                   'picking_type_id': picking_type_id,
                                   'product_uom_qty': rtv_line.remaining_qty,
                                   'price_unit': rtv_line.unit_cost,
                                   'invoice_state': '2binvoiced',
                                   'product_tmpl_id': False,
                                   'product_uos': False,
                                   'reserved_quant_ids': [],
                                   'location_dest_id': location_dest_id,
                                   'procure_method': 'make_to_stock',
                                   'product_packaging': False,
                                   'group_id': False,
                                   'location_id': location_id,
                                   'name': str(rtv_line.product_id.name)}])

            stock_data['move_lines'] = move_line

        stock_obj = self.env['stock.picking']
        save_the_data = stock_obj.create(stock_data)

        stock_obj_confirmation = stock_obj.action_confirm([save_the_data])
        stock_obj = stock_obj.action_assign([save_the_data])

        rtv_data.write({'reverse_stock_id': save_the_data})

        # requested_rtv_query = "UPDATE rtv_process SET reverse_stock_id='{0}' WHERE id={1}".format(save_the_data, id)
        # self.env.cr.execute(requested_rtv_query)
        # self.env.cr.commit()

        self.env['srn.process'].transfer_stock([id], reverse=True)

        return save_the_data

    def rtv_to_pqc(self, rtv_id):

        pqc_line_list = []

        rtv_data = self.env['rtv.process'].browse(rtv_id)

        pqc_obj = self.env['pending.quality.control'].search([('pqc_number', '=', str(rtv_data.pqc_number))])

        pqc_data_dict = {
            'po_id': pqc_obj.po_id,
            'po_number': str(pqc_obj.po_number),
            'irn_number': rtv_data.pqc_id.irn_number,
            'irn_id': rtv_data.pqc_id.irn_id,
            'qc_id': rtv_data.pqc_id.qc_id,
            'warehouse_id': rtv_data.warehouse_id.id,
            # 'vendor': rtv_data.vendor.id,
            # 'area': rtv_data.area.id,
            'state': 'pending',
            'qc_number': pqc_obj.qc_number,
            'received_by': self.env.uid,
            'pqc_number': str(rtv_data.pqc_number),
            'date_confirm': strftime("%Y-%m-%d %H:%M:%S", gmtime()),
            # 'pqc_id': rtv_data.pqc_id

        }
        for rtv_product in rtv_data.rtv_process_line:
            for pqc_data in pqc_obj.pqc_line:
                if rtv_product.product_id == pqc_data.product_id:
                    pqc_line_list.append([0, False, {
                        'rtv_id': rtv_data.id,
                        'rtv_name': rtv_data.name,
                        'qc_id': pqc_obj.qc_id,
                        'product': rtv_product.product_id.name,
                        'product_id': rtv_product.product_id.id,
                        'purchase_quantity': pqc_data.purchase_quantity,
                        'received_quantity': rtv_product.remaining_qty,
                        'accepted_quantity': float(0),
                        'pqc_reject_quantity': rtv_product.remaining_qty,
                        'pending_quantity': rtv_product.remaining_qty
                    }])
        pqc_data_dict['pqc_line'] = pqc_line_list

        if len(pqc_line_list) > 0:
            # Create pqc
            pqc_env = self.env['pending.quality.control']
            saved_pqc_id = pqc_env.create(pqc_data_dict)

            rtv_data.write({'reverse_pqc_id': saved_pqc_id})

            # rtv_query = "UPDATE rtv_process SET reverse_pqc_id='{0}' WHERE id={1}".format(
            #     saved_pqc_id, self.rtv_id)
            # self.env.cr.execute(rtv_query)
            # self.env.cr.commit()

            return saved_pqc_id
        else:
            return True


class RtvProductLineWizard(models.Model):
    _name = "rtv.product.line.wizard"
    _description = "RTV Process Line Wizard"

    rtv_process_id = fields.Many2one('receive.srn.rtv.wizard', 'RTV Process ID', required=True,
                                     ondelete='cascade', index=True, readonly=True)

    product_id = fields.Many2one('product.product', 'Product', required=True)
    po_number = fields.Char('Ref PO Number')
    pqc = fields.Boolean('PQC')
    rtv_product_id = fields.Integer('RTV Product ID')
    srn_rtv_product_line_id = fields.Integer('SRN RTV Product Line ID')
    return_qty = fields.Float('Return Qty')
    remaining_qty = fields.Float('Remaining Qty')
    available_qty = fields.Float('Free Qty')
    requested_qty = fields.Float('Request Qty')
    unit_cost = fields.Float('Unit Rate')
    amount = fields.Float('Amount')
    cal_amount = fields.Float('')
    state = fields.Selection([
        ('pending', 'Pending'),
        ('requested', 'Requested'),
        ('resolved', 'Resolved'),
        ('confirmed', 'Ready for Return'),
        ('create_srn', 'SRN'),
        ('received', 'Received'),
        ('cancel', 'Cancelled'),

    ], 'Status', readonly=True, copy=False, help="Gives the status of the RTV Process", index=True)

    # ## block code for V-14
    #
    # _columns = {
    #     'rtv_process_id': fields.many2one('receive.srn.rtv.wizard', 'RTV Process ID', required=True,
    #                                       ondelete='cascade', index=True, readonly=True),
    #
    #     'product_id': fields.many2one('product.product', 'Product', required=True),
    #     'po_number': fields.char('Ref PO Number'),
    #     'pqc': fields.boolean('PQC'),
    #     'rtv_product_id': fields.integer('RTV Product ID'),
    #     'srn_rtv_product_line_id': fields.integer('SRN RTV Product Line ID'),
    #     'return_qty': fields.float('Return Qty'),
    #     'remaining_qty': fields.float('Remaining Qty'),
    #     'available_qty': fields.float('Free Qty'),
    #     'requested_qty': fields.float('Request Qty'),
    #     'unit_cost': fields.float('Unit Rate'),
    #     'amount': fields.float('Amount'),
    #     'cal_amount': fields.float(''),
    #     'state': fields.selection([
    #         ('pending', 'Pending'),
    #         ('requested', 'Requested'),
    #         ('resolved', 'Resolved'),
    #         ('confirmed', 'Ready for Return'),
    #         ('create_srn', 'SRN'),
    #         ('received', 'Received'),
    #         ('cancel', 'Cancelled'),
    #
    #     ], 'Status', readonly=True, copy=False, help="Gives the status of the RTV Process", index=True),
    # }
    @api.onchange('requested_qty', 'return_qty')
    def onchange_remaining_qty(self):
        values = {}
        if self.requested_qty and self.return_qty:
            values['remaining_qty'] = float(self.requested_qty) - float(self.return_qty)
        else:
            values['remaining_qty'] = 0.00
        return {'value': values}
