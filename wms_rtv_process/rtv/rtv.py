import itertools
from odoo import api, fields, models, _
from odoo.exceptions import UserError, ValidationError, Warning
from odoo.osv import osv
from odoo.tools.translate import _
from odoo import api
from time import gmtime, strftime
from odoo.exceptions import except_orm, Warning, RedirectWarning


class RtvProcess(models.Model):
    _name = "rtv.process"
    _description = "RTV Process"

    @api.depends('invoice_id')
    def _count_all(self):
        for rtv in self:
            rtv.invoice_count = len(rtv.invoice_id) if rtv.invoice_id else 0

    name = fields.Char('RTV Number')
    warehouse_id = fields.Many2one('stock.warehouse', string='Warehouse', readonly=True, copy=True, index=True)
    # warehouse_id = fields.Selection([
    #     (1, 'Central Warehouse Uttara'),
    #     (2, 'Nodda Warehouse'),
    #     (3, 'Nodda Retail Warehouse'),
    #     (4, 'Motijheel Warehouse'),
    #     (6, 'Uttara Retail Warehouse'),
    #     (7, 'Motijheel Retail Warehouse'),
    #     (8, 'Damage Warehouse'),
    #     (19, 'Comilla Warehouse'),
    # ], 'Warehouse', readonly=True, copy=False, help="Gives the List of the Warehouses", index=True)
    warehouse_name = fields.Char('Warehouse Name')
    rtv_type = fields.Selection([
        ('pqc', 'PQC'),
        ('stock', 'Stock')
    ], 'RTV Type', default='stock', readonly=True, copy=False, help="Gives the RTV Type ", select=True)
    vendor_id = fields.Many2one('res.partner', 'Supplier')
    vendor_address = fields.Char('Address')
    vendor_contact = fields.Char('Contact Number')
    area = fields.Many2one("res.country.state", 'Area')
    remark = fields.Text('Remark')
    po_number = fields.Char('Ref PO Number')
    po_id = fields.Many2one("purchase.order", 'PO ID')
    pqc = fields.Boolean('PQC')
    pqc_id = fields.Many2one("pending.quality.control", 'PQC ID')
    pqc_number = fields.Char('Ref PQC Number')
    pqc_processed = fields.Boolean('PQC Processed')
    delivered = fields.Boolean('Delivered')
    reattempted = fields.Boolean('Reattempted')
    reattempt_count = fields.Integer(string="Re-attempt Count")
    reattempt_by = fields.Many2one('res.users', 'Reattempt By')
    reattempt_time = fields.Datetime('Reattempt Time')
    reattempted_cancel = fields.Boolean('Reattempted Cancel')
    received_by = fields.Many2one('res.users', 'Received By')
    received_time = fields.Datetime('Received Time')
    confirmed_by = fields.Many2one('res.users', 'Confirmed By')
    confirmed_time = fields.Datetime('Confirmed Time')
    requested_by = fields.Many2one('res.users', 'Requested By')
    requested_time = fields.Datetime('Requested Time')
    resolved = fields.Boolean('Resolved')
    resolved_by = fields.Many2one('res.users', 'Resolved By')
    resolved_time = fields.Datetime('Resolved Time')
    approved_by = fields.Many2one('res.users', 'Approved By')
    approved_time = fields.Datetime('Approved Time')
    damage = fields.Boolean('Damage')
    cancel = fields.Boolean('Cancel')
    cancel_by = fields.Many2one('res.users', 'Cancel By')
    cancel_time = fields.Datetime('Cancel Time')
    state = fields.Selection([
        ('pending', 'Pending'),
        ('requested', 'Requested'),
        ('resolved', 'Resolved'),
        ('confirmed', 'Ready for Return'),
        ('create_srn', 'SRN'),
        ('received', 'Received'),
        ('not_received', 'Not Received'),
        ('pending_approval', 'Pending Approval'),
        ('approved', 'Approved'),
        ('cancel', 'Cancelled'),
    ], 'Status', default='pending', readonly=True, copy=False, help="Gives the status of the RTV Process", index=True)

    rtv_process_line = fields.One2many('rtv.process.line', 'rtv_process_id', 'RTV Process Line', required=True)
    stock_id = fields.Many2one('stock.picking', 'Stock Number')
    invoice_id = fields.Many2one('account.move', 'Invoice Number')
    aqc_id = fields.Many2one('accepted.quality.control', 'AQC Number')
    reverse_stock_id = fields.Many2one('stock.picking', 'Reverse Stock Number')
    reverse_pqc_id = fields.Many2one('pending.quality.control', 'Reverse PQC Number')

    resolve_rtv_process_date = fields.Datetime('Resolve rtv Process Date')
    resolve_rtv_process_by = fields.Many2one('res.users', 'Resolve rtv Process By')
    resolve_rtv_process_reason = fields.Text('Resolve rtv Process Reason')

    cancel_rtv_process_date = fields.Datetime('Cancel rtv Process Date')
    cancel_rtv_process_by = fields.Many2one('res.users', 'Cancel rtv Process By')
    cancel_rtv_process_reason = fields.Text('Cancel rtv Process Reason')

    not_received_by = fields.Many2one('res.users', 'Not Received By')
    not_received_time = fields.Datetime('Not Received Time')
    not_receive_reason = fields.Selection([
        ("Shop off", "Shop off"),
        ("Vendor unreachable", "Vendor unreachable"),
        ("Vendor Rescheduled", "Vendor Rescheduled"),
        ("Does not agree to take", "Does not agree to take"),
        ("Not received due to quality", "Not received due to quality"),
        ("Vendor is not aware of", "Vendor is not aware of")
    ], 'Not Receive Reason', copy=False, help="Reason", index=True)

    srn_number = fields.Char('SRN Number')
    srn_rtv_number = fields.Char('Challan Number')
    invoice_count = fields.Integer(compute='_count_all', string='Invoices')

    @api.onchange("vendor_id")
    def onchange_vendor_id(self, vendor_id=False,):
        values = {'vendor_address': ''}
        if self.vendor_id is not False:
            vendor = self.env['res.partner'].search([('id', '=', self.vendor_id.id)], limit=1)

            if vendor.street:
                values['vendor_address'] = values['vendor_address'] + vendor.street + ", "
            if vendor.street2:
                values['vendor_address'] = values['vendor_address'] + vendor.street2 + ", "
            if vendor.city:
                values['vendor_address'] = values['vendor_address'] + vendor.city + ", "
            if vendor.state_id:
                values['vendor_address'] = values['vendor_address'] + vendor.state_id.name + ", "
            if vendor.zip:
                values['vendor_address'] = values['vendor_address'] + vendor.zip + ", "
            if vendor.country_id:
                values['vendor_address'] = values['vendor_address'] + vendor.country_id.name
            if vendor.mobile:
                values['vendor_contact'] = vendor.mobile if vendor.mobile else vendor.phone

        return {'value': values}

    def action_view_invoice(self):
        invoices = self.mapped('invoice_id')
        action = self.env["ir.actions.actions"]._for_xml_id("account.action_move_out_invoice_type")
        if len(invoices) > 1:
            action['domain'] = [('id', 'in', invoices.ids)]
        elif len(invoices) == 1:
            form_view = [(self.env.ref('account.view_move_form').id, 'form')]
            if 'views' in action:
                action['views'] = form_view + [(state,view) for state,view in action['views'] if view != 'form']
            else:
                action['views'] = form_view
            action['res_id'] = invoices.id
        else:
            action = {'type': 'ir.actions.act_window_close'}

        context = {
            'default_move_type': 'out_invoice',
        }
        if len(self) == 1:
            context.update({
                # 'default_partner_id': self.partner_id.id,
                # 'default_partner_shipping_id': self.partner_shipping_id.id,
                # 'default_invoice_payment_term_id': self.payment_term_id.id or self.partner_id.property_payment_term_id.id or self.env['account.move'].default_get(['invoice_payment_term_id']).get('invoice_payment_term_id'),
                'default_invoice_origin': self.mapped('name'),
                # 'default_user_id': self.user_id.id,
            })
        action['context'] = context
        return action

    def invoice_open(self):
        mod_obj = self.env['ir.model.data']
        act_obj = self.env['ir.actions.act_window']

        result = mod_obj.get_object_reference('account.view_move_form')
        id = result and result[1] or False
        result = act_obj.read([id])[0]

        for rtv in self:
            inv_ids = rtv.invoice_id.id
            if not inv_ids:
                raise Warning(_('Please create Invoices.'))

            else:
                res = mod_obj.get_object_reference('account', 'invoice_supplier_form')
                result['views'] = [(res and res[1] or False, 'form')]
                result['res_id'] = inv_ids or False
        return result

    def request_rtv(self):
        for rtv_data in self:
            if not rtv_data.vendor_id:
                raise Warning(_('You are missing vendor name. Please give vendor name!!!'))
            if not rtv_data.warehouse_id:
                raise Warning(_('You are missing warehouse name. Please give warehouse name!!!'))
            if not rtv_data.pqc:
                stock_picking_type_data = self.env['stock.picking.type'].search([('name', 'like', 'Delivery Orders')])
                # stock_picking_type_data = self.env['stock.picking.type'].browse(stock_picking_type_ids)
                location_id = None
                location_dest_id = None
                picking_type_id = None

                for items in stock_picking_type_data:
                    if items.warehouse_id.id == rtv_data.warehouse_id.id:
                        location_id = items.default_location_src_id.id
                        picking_type_id = items.id
                        location_dest_id = self.env['stock.location'].search([('name', '=', 'Vendors')], limit=1)
                move_line = []

                stock_data = {}

                if location_id is not None and location_dest_id is not None:
                    stock_data = {'origin': rtv_data.name,
                                  'message_follower_ids': False,
                                  'carrier_tracking_ref': False,
                                  # 'number_of_packages': 0,
                                  'date_done': False,
                                  'carrier_id': False,
                                  'write_uid': False,
                                  'partner_id': rtv_data.vendor_id.id,
                                  'message_ids': False,
                                  'note': False,
                                  'picking_type_id': picking_type_id,
                                  'move_type': 'one',
                                  'company_id': self.env.company.id,
                                  'priority': '1',
                                  'picking_type_code': False,
                                  'owner_id': False,
                                  'scheduled_date': False,
                                  'date': strftime("%Y-%m-%d %H:%M:%S", gmtime()),
                                  # 'pack_operation_ids': [],
                                  'rtv': True,
                                  'carrier_code': 'custom',
                                  'location_id': location_id,
                                  'location_dest_id': location_dest_id.id,
                                  # 'invoice_state': '2binvoiced'
                                  }
                    tmp_dict = {}
                    for rtv_line in rtv_data.rtv_process_line:
                        if rtv_line.requested_qty == float(0):
                            raise Warning(_('Request quantity for this RTV is not provided, please check!!!'))
                        else:
                            move_line_data = {
                                'origin': rtv_data.name,
                                # 'product_uos_qty': rtv_line.requested_qty,
                                # 'date_expected': strftime("%Y-%m-%d %H:%M:%S", gmtime()),
                                'date': strftime("%Y-%m-%d %H:%M:%S", gmtime()),
                                'product_id': rtv_line.product_id.id,
                                'product_uom': rtv_line.product_id.uom_id.id,
                                'picking_type_id': picking_type_id,
                                'product_uom_qty': rtv_line.requested_qty,
                                # 'invoice_state': '2binvoiced',
                                'product_tmpl_id': rtv_line.product_id.product_tmpl_id.id,
                                # 'product_uos': False,
                                # 'reserved_quant_ids': [],
                                'location_dest_id': location_dest_id.id,
                                'procure_method': 'make_to_stock',
                                # 'product_packaging': False,
                                'group_id': False,
                                'location_id': location_id,
                                'price_unit': rtv_line.unit_cost,
                                'name': str(rtv_line.product_id.name)}
                            # if rtv_line.unit_cost > float(0):
                            #     move_line_data.update({'price_unit':rtv_line.unit_cost})
                            move_line.append([0, False, move_line_data])

                    stock_data['move_lines'] = move_line

                stock_obj = self.env['stock.picking']
                save_the_data = stock_obj.create(stock_data)
                if save_the_data:
                    save_the_data.action_confirm()
                    save_the_data.action_assign()

                rtv_data.write({
                    'state': 'requested',
                    'requested_by': self.env.uid,
                    'requested_time': str(fields.Datetime.now()),
                    'stock_id': save_the_data.id if save_the_data else False
                })

                rtv_data.rtv_process_line.write({
                    'state': 'requested',
                    'confirmed_time': str(fields.Datetime.now())
                })
            else:
                rtv_data.write({
                    'state': 'requested',
                    'requested_by': self.env.uid,
                    'requested_time': str(fields.Datetime.now())
                })

                rtv_data.rtv_process_line.write({
                    'state': 'requested',
                    'confirmed_time': str(fields.Datetime.now())
                })

            self.env['rtv.end.to.end'].rtv_request_update(rtv_data.name, rtv_data.id, 'requested')
            rtv_data.email_template('RTV Ticket')

    def create_aqc(self):
        # if context is None:
        #     context = {}
        aqc_line = []

        # rtv_obj = self.env['rtv.process'].browse(cr, uid, ids, context=context)

        for rtv_data in self:

            pqc_obj = self.env['pending.quality.control'].search([('pqc_number', '=', rtv_data.pqc_number)], limit=1)

            data = {'po_id': pqc_obj.po_id,
                    'po_number': pqc_obj.po_number,
                    'irn_number': rtv_data.pqc_id.irn_number,
                    'irn_id': rtv_data.pqc_id.irn_id,
                    'qc_id': rtv_data.pqc_id.qc_id,
                    'warehouse_id': rtv_data.warehouse_id.id,
                    # 'vendor': rtv_data.vendor_id.id,
                    # 'area': rtv_data.area.id,
                    'state': 'confirm',
                    'qc_number': pqc_obj.qc_number,
                    'received_by': self.env.uid,
                    'pqc_number': str(rtv_data.pqc_number),
                    'date_confirm': fields.Datetime.now(),
                    'pqc_id': rtv_data.pqc_id
                    }

            put_data = {'warehouse_id': rtv_data.warehouse_id.id,
                        # 'state': 'confirm',
                        # 'received_by': self.env.uid,
                        # 'pqc_number': str(rtv_data.pqc_number),
                        # 'date_confirm': fields.Datetime.now(),
                        # 'pqc_id': rtv_data.pqc_id.id
                        }

            for rtv_product in rtv_data.rtv_process_line:
                for pqc_data in pqc_obj.pqc_line:
                    if rtv_product.product_id == pqc_data.product_id:
                        aqc_line.append([0, False, {
                            # 'rtv_id': rtv_data.id,
                            # 'rtv_name': rtv_data.name,
                            'qc_id': pqc_obj.qc_id,
                            'product': rtv_product.product_id.name,
                            'product_id': rtv_product.product_id.id,
                            'purchase_quantity': pqc_data.purchase_quantity,
                            'received_quantity': rtv_product.return_qty,
                            'accepted_quantity': rtv_product.return_qty,
                            'pending_quantity': rtv_product.requested_qty,
                            'state': 'confirm'
                        }])

            data['aqc_line'] = aqc_line

            if len(aqc_line) > 0:
                # Create aqc
                save_the_data = self.env['accepted.quality.control'].create(data)

                # Generate Put away Process
                put_line_list = []

                for rtv_product in rtv_data.rtv_process_line:
                    if rtv_product.return_qty > 0:
                        put_line_list.append([0, False,
                                              {
                                                  'product_id': rtv_product.product_id.id,
                                                  'qc_id': save_the_data.id,
                                                  'location': '',
                                                  'product': rtv_product.product_id.name,
                                                  'remarks': '',
                                                  'quantity': rtv_product.return_qty,
                                                  'po_number': str(pqc_obj.po_number)

                                              }])

                put_data['aqc_id'] = save_the_data.id
                put_data['state'] = 'pending'
                put_data['putaway_line_view'] = put_line_list
                put_env = self.env['putaway.process']
                saved_put_id = put_env.create(put_data)

                rtv_data.write({
                    'aqc_id': save_the_data.id
                })

                # rtv_query = "UPDATE rtv_process SET aqc_id='{0}' WHERE id={1}".format(
                #     save_the_data, rtv_data.id)
                # self.env.cr.execute(rtv_query)
                # self.env.cr.commit()

    def resolve_rtv(self):
        for rtv_obj in self:
            if not rtv_obj.pqc:
                if rtv_obj.stock_id.id is not None or False:
                    # Cancel Action
                    rtv_obj.stock_id.do_unreserve()
                    rtv_obj.stock_id.action_cancel()
            else:
                rtv_obj.create_aqc()

            rtv_obj.write({
                'state': 'resolved',
                'resolved': True,
                'stock_id': None,
                'resolved_by': self.env.uid,
                'resolved_time': str(fields.Datetime.now())
            })

            rtv_obj.rtv_process_line.write({
                'state': 'resolved',
                'resolved_time': str(fields.Datetime.now())
            })

            self.env['rtv.end.to.end'].rtv_resolve_update(rtv_obj.name, rtv_obj.id, 'resolved')
            rtv_obj.email_template('RTV Ticket')

    def confirm_rtv(self):
        for rtv_obj in self:
            rtv_obj.write({
                'state': 'confirmed',
                'confirmed_by': self.env.uid,
                'confirmed_time': fields.Datetime.now()
            })

            rtv_obj.rtv_process_line.write({
                'state': 'confirmed',
                'confirmed_time': fields.Datetime.now()
            })

        self.env['rtv.end.to.end'].rtv_confirm_update(rtv_obj.name, rtv_obj.id, 'confirmed')
        rtv_obj.email_template('RTV Ticket')

    def action_srn_rtv(self):
        self.srn_rtv()

    def srn_rtv(self):
        dict_data = [{'warehouse_id': row.warehouse_id.id,
                      'vendor_id': row.vendor_id.id,
                      'rtv_id': row.id} for row in self]

        groupby_warehouse_vendor = []
        i = 0
        for key, group in itertools.groupby(dict_data, key=lambda x: (x['warehouse_id'], x['vendor_id'])):
            groupby_warehouse_vendor.append({i: list(group)})
            i += 1

        z = 0
        for row in groupby_warehouse_vendor:
            data = {}
            srn_line = []
            srn_product_line = []

            rtv_datas = self.search([('warehouse_id', '=', row[z][0]['warehouse_id']),
                                     ('vendor_id', '=', row[z][0]['vendor_id']),
                                     ('id', 'in', self.ids)])
            for rtv_data in rtv_datas:
                srn_number = rtv_data.srn_number if rtv_data.srn_number else 'False'
                if rtv_data.state != 'confirmed':
                    raise Warning(_('Please confirm the RTV'))
                # if rtv_data.vendor:
                data['vendor'] = rtv_data.vendor_id.id
                data['vendor_address'] = rtv_data.vendor_address if rtv_data.vendor_address else ''
                data['vendor_contact'] = rtv_data.vendor_contact if rtv_data.vendor_contact else ''

                data['warehouse_id'] = rtv_data.warehouse_id.id

                # if rtv_data.area:
                #     data['area'] = rtv_data.area.name

                srn_line.append([0, False, {
                    'rtv_name': rtv_data.name,
                    'rtv_id': rtv_data.id,
                    'stock_id': rtv_data.stock_id.id if rtv_data.stock_id else False,
                    'pqc': int(rtv_data.pqc) if rtv_data.pqc else False,
                    'pqc_id': rtv_data.pqc_id.id if rtv_data.pqc_id else '',
                    'pqc_number': rtv_data.pqc_number if rtv_data.pqc_number else '',
                    'state': 'pending'
                }])

                for rtv_product in rtv_data.rtv_process_line:
                    srn_product_line.append([0, False, {
                        'rtv_id': rtv_data.id,
                        'rtv_name': rtv_data.name,
                        'stock_id': int(rtv_data.stock_id.id) if rtv_data.stock_id.id else '',
                        'pqc': int(rtv_data.pqc) if rtv_data.pqc else False,
                        'pqc_id': int(rtv_data.pqc_id.id) if rtv_data.pqc_id.id else '',
                        'pqc_number': str(rtv_data.pqc_number) if rtv_data.pqc_number else '',
                        'product_id': rtv_product.product_id.id,
                        'return_qty': rtv_product.return_qty,
                        'requested_qty': rtv_product.requested_qty,
                        'remaining_qty': rtv_product.remaining_qty,
                        'state': 'pending',
                        'amount': rtv_product.amount,
                        'unit_cost': rtv_product.unit_cost,
                    }])

                data['srn_product_line'] = srn_product_line
            data['srn_line'] = srn_line
            save_the_data = self.env['srn.process'].create(data)
            z += 1

            srn_obj = save_the_data

            srn = srn_obj.name if srn_number == 'False' else srn_number + "," + srn_obj.name
            for srn_line_rtv in srn_obj.srn_line:
                # warehouse_code = {1: 'UttWH',
                #                   2: 'NodWH',
                #                   3: 'SinWH',
                #                   7: 'BddWH',
                #                   4: 'UttDW',
                #                   5: 'NodDW',
                #                   6: 'SinDW',
                #                   9: 'BddDW'}

                srn_rtv_number = srn_obj.name + '/' + srn_obj.warehouse_id.code + '/' + srn_line_rtv.rtv_id.name
                srn_line_rtv.write({
                    'srn_rtv_number': srn_rtv_number
                })
                # update_srn_number = "UPDATE srn_rtv_line SET srn_rtv_number='{0}' WHERE id ={1}".format(
                #     srn_rtv_number, srn_line_rtv.id)
                # self.env.cr.execute(update_srn_number)
                # self.env.cr.commit()

            srn_obj.srn_product_line.write({'srn_rtv_number': srn_rtv_number})
            # update_srn_number_product = "UPDATE srn_rtv_product SET srn_rtv_number='{0}' WHERE rtv_id ={1} and srn_rtv_product_id={2}".format(
            #     srn_rtv_number, srn_line_rtv.rtv_id.id, srn_obj.id)
            # self.env.cr.execute(update_srn_number_product)
            # self.env.cr.commit()

            rtv_datas.write({
                'state': 'create_srn',
                'srn_number': srn,
                'srn_rtv_number': srn_rtv_number
            })

            for rtv_data in rtv_datas:
                rtv_data.rtv_process_line.write({
                    'state': 'create_srn'
                })
                # update_query_srn = "UPDATE rtv_process SET srn_number='{0}',srn_rtv_number='{1}' WHERE id ={2}".format(
                #     srn, srn_rtv_number, srn_line_rtv.rtv_id.id)
                # self.env.cr.execute(update_query_srn)
                # self.env.cr.commit()
                self.env['rtv.end.to.end'].rtv_srn_update(rtv_data.name, rtv_data.id, srn,
                                                          srn_rtv_number, 'create_srn')

    def cancel_rtv(self):
        for rtv_obj in self:
            if not rtv_obj.pqc:
                if rtv_obj.stock_id.id is not None or False:
                    # Cancel Action
                    rtv_obj.stock_id.do_unreserve()
                    rtv_obj.stock_id.action_cancel()

            rtv_obj.write({
                'state': 'cancel',
                'cancel_by': self.env.uid,
                'cancel_time': fields.Datetime.now()
            })

            rtv_obj.rtv_process_line.write({
                'state': 'cancel',
                'cancel_time': fields.Datetime.now()
            })

            self.env['rtv.end.to.end'].rtv_cancel_update(rtv_obj.name, rtv_obj.id, 'cancel')

    def cancel_pending_rtv(self):
        for rtv in self:
            rtv.write({
                'state': 'cancel',
                'cancel_by': self.env.uid,
                'cancel_time': fields.Datetime.now()
            })

            rtv.rtv_process_line.write({
                'state': 'cancel',
                'cancel_time': fields.Datetime.now()
            })

            self.env['rtv.end.to.end'].rtv_cancel_update(rtv.name, rtv.id, 'cancel')
        # rtv_obj = self.browse(self.ids[0])
        # for single_id in self.ids:
        #     cancel_rtv_query = "UPDATE rtv_process SET state='cancel', cancel_by={0}, cancel_time='{1}' WHERE id={2}".format(
        #         str(fields.datetime.now()), single_id)
        #     self.env.cr.execute(cancel_rtv_query)
        #     self.env.cr.commit()
        #
        #     cancel_rtv_query_line = "UPDATE rtv_process_line SET state='cancel', cancel_time='{0}' WHERE rtv_process_id={1}".format(
        #         str(fields.datetime.now()), single_id)
        #     self.env.cr.execute(cancel_rtv_query_line)
        #     self.env.cr.commit()
        # return True

    def _reattempt_count(self):
        for rtv in self:
            rtv.reattempt_count = (rtv.reattempt_count + 1) if rtv.reattempt_count else 1
        # rtv_obj = self.env['rtv.process'].browse()
        # reattempt = rtv_obj.reattempt_count if rtv_obj.reattempt_count else 0
        # reattempted = reattempt + 1
        #
        # return reattempted

    def reattempt_rtv(self):
        for rtv in self:
            rtv._reattempt_count()
        # single_id = self.ids[0]
        # reattempt = self._reattempt_count(single_id)
        # rtv_obj = self.browse(self.ids[0])

            if rtv.reattempt_count < 3:
                state = 'pending'
            else:
                state = 'pending_approval'

            rtv.write({
                'reattempted': True,
                'state': state,
                'reattempt_by': self.env.uid,
                'reattempt_time': fields.Datetime.now()
            })

            rtv.rtv_process_line.write({
                'state': state
            })

            # reattempt_srn_query = "UPDATE rtv_process SET reattempted=True,state='{0}', reattempt_by={1},reattempt_time='{2}',reattempt_count={3} WHERE id={4}".format(
            #     state, uid, str(fields.datetime.now()), reattempt, single_id)
            # self.env.cr.execute(reattempt_srn_query)
            # self.env.cr.commit()

            # reattempt_srn_rtv_line_query = "UPDATE rtv_process_line SET state='{0}' WHERE rtv_process_id={1}".format(
            #     state, single_id)
            # self.env.cr.execute(reattempt_srn_rtv_line_query)
            # self.env.cr.commit()

            self.env['rtv.end.to.end'].rtv_reattempt_update(rtv.name, rtv.id, state)
            rtv.email_template('RTV Reattempted')

    def approved_reattempt_rtv(self):
        for rtv in self:
            if not rtv.pqc:
                if rtv.stock_id is not None or False:
                    # Cancel Action
                    rtv.stock_id.do_unreserve()
                    rtv.stock_id.action_cancel()
            else:
                rtv.create_aqc()

            rtv.write({
                'state': "approved",
                'approved_by': self.env.uid,
                'approved_time': fields.Datetime.now()
            })

            rtv.rtv_process_line.write({
                'state': "approved"
            })

            self.env['rtv.end.to.end'].rtv_approval_update(rtv.name, rtv.id, 'approved')
            rtv.email_template('RTV Ticket')

    def send_rtv_to_damage(self):
        # id = self.ids[0] if len(self.ids) > 0 else None
        # rtv_obj = self.browse(self.ids[0])
        # uid = 1

        for rtv_obj in self:
            if not rtv_obj.pqc:
                if rtv_obj.stock_id.id is not None or False:
                    # Cancel Action
                    rtv_obj.stock_id.do_unreserve()
                    rtv_obj.stock_id.action_cancel()

                stock_picking_type_data = self.env['stock.picking.type'].search([('name', 'like', 'Delivery Orders')])
                # stock_picking_type_data = self.env['stock.picking.type'].browse(stock_picking_type_ids)
                location_id = None
                location_dest_id = None
                picking_type_id = None

                for items in stock_picking_type_data:
                    if items.warehouse_id.id == rtv_obj.warehouse_id.id:
                        location_id = items.default_location_src_id.id
                        picking_type_id = items.id
                        location_dest_id = 68

                move_line = []

                stock_data = {}

                if location_id is not None and location_dest_id is not None:
                    stock_data = {'origin': False, 'message_follower_ids': False, 'carrier_tracking_ref': False,
                                  'number_of_packages': 0, 'date_done': False, 'carrier_id': False, 'write_uid': False,
                                  'partner_id': False, 'message_ids': False, 'note': False,
                                  'picking_type_id': picking_type_id,
                                  'move_type': 'one', 'company_id': 1, 'priority': '1', 'picking_type_code': False,
                                  'owner_id': False, 'min_date': False, 'date': strftime("%Y-%m-%d %H:%M:%S", gmtime()),
                                  'pack_operation_ids': [],
                                  'carrier_code': 'custom', 'invoice_state': 'none'}
                    tmp_dict = {}
                    for rtv_line in rtv_obj.rtv_process_line:
                        move_line.append([0, False,
                                          {'product_uos_qty': rtv_line.requested_qty,
                                           'date_expected': strftime("%Y-%m-%d %H:%M:%S", gmtime()),
                                           'date': strftime("%Y-%m-%d %H:%M:%S", gmtime()),
                                           'product_id': rtv_line.product_id.id, 'product_uom': 1,
                                           'picking_type_id': picking_type_id, 'product_uom_qty': rtv_line.requested_qty,
                                           'price_unit': rtv_line.unit_cost,
                                           'invoice_state': 'none', 'product_tmpl_id': False, 'product_uos': False,
                                           'reserved_quant_ids': [], 'location_dest_id': location_dest_id,
                                           'procure_method': 'make_to_stock',
                                           'product_packaging': False, 'group_id': False, 'location_id': location_id,
                                           'name': str(rtv_line.product_id.name)}])

                    stock_data['move_lines'] = move_line

                # stock_obj = self.env['stock.picking']
                save_the_data = self.env['stock.picking'].create(stock_data)
                if save_the_data:
                    stock_obj_confirmation = save_the_data.action_confirm()
                    stock_obj = save_the_data.action_assign()

                    # call transfer
                    stock_picking = save_the_data.do_enter_transfer_details()

                    ### Commented By Matiar Rahman
                    ### stock.transfer_details model is not exist in Odoo 14.0
                    # trans_obj = self.env['stock.transfer_details']
                    # trans_search = trans_obj.search([('picking_id', '=', save_the_data)])
                    #
                    # trans_search = [trans_search[len(trans_search) - 1]] if len(trans_search) > 1 else trans_search
                    #
                    # trans_browse = self.env['stock.transfer_details'].browse(trans_search)
                    #
                    # trans_browse.do_detailed_transfer()

                    rtv_obj.write({
                        'state': 'approved',
                        'damage': True,
                        'stock_id': save_the_data.id,
                        'approved_by': self.env.uid,
                        'approved_time': fields.Datetime.now()
                    })

                    rtv_obj.rtv_process_line.write({
                        'state': 'approved'
                    })

                    # for single_id in self.ids:
                    #     resolve_rtv_query = "UPDATE rtv_process SET state='approved',damage=TRUE ,stock_id='{0}',approved_by={1},approved_time='{2}' WHERE id={3}".format(
                    #         save_the_data, uid, str(fields.datetime.now()), single_id)
                    #     self.env.cr.execute(resolve_rtv_query)
                    #     self.env.cr.commit()
                    #
                    #     resolve_rtv_query_line = "UPDATE rtv_process_line SET state='approved'"
                    #     self.env.cr.execute(resolve_rtv_query_line)
                    #     self.env.cr.commit()

                    self.env['rtv.end.to.end'].rtv_approval_update(rtv_obj.name, rtv_obj.id, 'approved')
                    rtv_obj.email_template('RTV Ticket')
                return True
            else:
                raise osv.except_osv(_('Warning!'),
                                     _('You have to receive products in PO W/H.'
                                       'Please follow rtv process to transfer to Damage W/H'))

    def email_template(self, template_name):
        self.send_email(template_name)
        return True

    def send_email(self, template_name):
        uid = 1
        email_template_obj = self.env['mail.template']

        template_ids = email_template_obj.search([('name', '=', template_name)], limit=1)

        if template_ids:
            try:
                email_template_obj.send_mail(template_ids[0], self.ids, force_send=True)
            except:
                pass
        return True

    @api.model
    def create(self, vals):
        warehouse_id = vals['warehouse_id']

        new_rtv_line = []

        if vals.get('rtv_process_type') != 'po':
            if vals.get("rtv_process_line", False):
                for items in vals.get('rtv_process_line'):

                    product_id = items[2]['product_id']
                    prod_browse = self.env['product.product'].search([('id', '=', product_id)])

                    if warehouse_id == 1:  # Uttara
                        items[2]['available_qty'] = prod_browse.uttara_free_quantity
                    elif warehouse_id == 2:  # Nodda
                        items[2]['available_qty'] = prod_browse.nodda_free_quantity
                    elif warehouse_id == 3:  # Signboard
                        items[2]['available_qty'] = prod_browse.signboard_free_quantity
                    elif warehouse_id == 7:  # Badda
                        items[2]['available_qty'] = prod_browse.badda_free_quantity
                    elif warehouse_id == 4:  # Uttara Damage
                        items[2]['available_qty'] = prod_browse.uttara_damage_quantity
                    elif warehouse_id == 5:  # Nodda Damage
                        items[2]['available_qty'] = prod_browse.nodda_damage_quantity
                    elif warehouse_id == 6:  # Signboard Damage
                        items[2]['available_qty'] = prod_browse.signboard_damage_quantity
                    elif warehouse_id == 9:  # Badda Damage
                        items[2]['available_qty'] = prod_browse.badda_damage_quantity
                    elif warehouse_id == 10:  # Badda horeca Damage
                        items[2]['available_qty'] = prod_browse.badda_horeca_free_quantity
                    elif warehouse_id == 11:  # Savar
                        items[2]['available_qty'] = prod_browse.savar_free_quantity
                    elif warehouse_id == 12:  # Savar Damage
                        items[2]['available_qty'] = prod_browse.savar_damage_quantity

                    items[2]['amount'] = items[2]['cal_amount']
                    items[2]['free_qty'] = items[2]['available_qty']

                    if items[2]['available_qty'] == float(0):
                        raise Warning(_("This Product is not available in Stock"))

                    if items[2].get('requested_qty') > items[2]['available_qty']:
                        raise Warning(_("Requested Quantity can not greater than Available Quantity"))
                    else:
                        items[2]['return_qty'] = items[2].get('requested_qty')

                    if items[2].get('requested_qty') == float(0):
                        raise Warning(_("Please, give requested quantity!!!"))

        else:

            if vals.get("rtv_process_line", False):
                for items in vals.get('rtv_process_line'):
                    product_id = items[2]['product_id']
                    prod_browse = self.env['product.product'].search([('id', '=', product_id)])

                    if warehouse_id == 1:  # Uttara
                        items[2]['available_qty'] = prod_browse.uttara_free_quantity
                    elif warehouse_id == 2:  # Nodda
                        items[2]['available_qty'] = prod_browse.nodda_free_quantity
                    elif warehouse_id == 3:  # Signboard
                        items[2]['available_qty'] = prod_browse.signboard_free_quantity
                    elif warehouse_id == 7:  # Badda
                        items[2]['available_qty'] = prod_browse.badda_free_quantity
                    elif warehouse_id == 4:  # Uttara Damage
                        items[2]['available_qty'] = prod_browse.uttara_damage_quantity
                    elif warehouse_id == 5:  # Nodda Damage
                        items[2]['available_qty'] = prod_browse.nodda_damage_quantity
                    elif warehouse_id == 6:  # Signboard Damage
                        items[2]['available_qty'] = prod_browse.signboard_damage_quantity
                    elif warehouse_id == 9:  # Badda Damage
                        items[2]['available_qty'] = prod_browse.badda_damage_quantity
                    elif warehouse_id == 10:  # Badda horeca Damage
                        items[2]['available_qty'] = prod_browse.badda_horeca_free_quantity
                    elif warehouse_id == 11:  # Savar
                        items[2]['available_qty'] = prod_browse.savar_free_quantity
                    elif warehouse_id == 12:  # Savar Damage
                        items[2]['available_qty'] = prod_browse.savar_damage_quantity

                    items[2]['amount'] = items[2]['cal_amount']
                    items[2]['free_qty'] = items[2]['available_qty']

                    if items[2].get('free_qty') > float(0) and items[2].get('requested_qty') > float(0) and items[2].get('free_qty') >= items[2].get('requested_qty') and items[2].get('grn_receive_quantity') > items[2].get('requested_qty'):
                        new_rtv_line.append(items)

                    elif items[2]['free_qty'] == float(0):
                        raise Warning(_("This Product is not available in Stock"))
                    elif items[2].get('requested_qty') > items[2]['free_qty']:
                        raise Warning(_("Requested Quantity can not greater than Available Quantity"))
                    elif items[2].get('requested_qty') > items[2].get('grn_receive_quantity'):
                        raise Warning(_("Requested Quantity can not greater than GRN Quantity"))
                    # elif items[2].get('requested_qty') == float(0):
                    #     raise Warning(_("Please, give requested quantity!!!"))

        if len(new_rtv_line) > 0:
            vals["rtv_process_line"] = new_rtv_line

        if vals['po_number']:
            po_number = vals['po_number']
            po_obj = self.env['purchase.order'].search([('name', '=', str(po_number))], limit=1)
            vals['po_id'] = po_obj.id

        if vals['vendor_id']:
            warehouse_id = vals['warehouse_id']
            vals['warehouse_id'] = warehouse_id
            vendor_id = vals['vendor_id']
            vals['vendor_id'] = vendor_id
            vendor_address = vals['vendor_address']
            vals['vendor_address'] = vendor_address
            vendor_contact = vals['vendor_contact']
            vals['vendor_contact'] = vendor_contact
            area = vals['area']
            vals['area'] = area

        record = super(RtvProcess, self).create(vals)

        rtv_number = "RTV0" + str(record.id)
        record.name = rtv_number

        warehouse_code = {1: 'UttWH',
                          2: 'NodWH',
                          3: 'SinWH',
                          7: 'BddWH',
                          4: 'UttDW',
                          5: 'NodDW',
                          6: 'SinDW',
                          9: 'BddDW',
                          10: "BdHoW",
                          11: "SavWH",
                          12: "SavDW"
                          }
        record.warehouse_name = warehouse_code[warehouse_id]

        # self.env['rtv.end.to.end'].rtv_data_create(rtv_number, record.id,)

        self.env['rtv.end.to.end'].rtv_data_create(rtv_number, record.id,
                                                   value={'rtv_date_create': record.create_date,
                                                          'vendor_name': record.vendor_id.name,
                                                          'warehouse_name': warehouse_code[warehouse_id],
                                                          'ref_po_number': record.po_number,
                                                          'ref_pqc_number': record.pqc_number,
                                                          'state': record.state})
        return record

    def write(self, vals):

        update_permission = list()
        if vals.get('rtv_process_line', False):
            for single_line in vals['rtv_process_line']:
                if not not single_line[2]:
                    for req_l in self.rtv_process_line:

                        if req_l.id == single_line[1]:

                            request_quantity = single_line[2]['requested_qty'] if single_line[2].get('requested_qty') else req_l.requested_qty

                            if single_line[2]['amount'] == req_l['cal_amount']:
                                req_l.amount = req_l['cal_amount']
                            else:
                                req_l.amount = single_line[2]['amount']

                            if request_quantity > req_l.free_qty:
                                update_permission.append(False)
                            else:
                                update_permission.append(True)
                        else:
                            pass
                            # single_line[2]['free_qty'] = req_l['available_qty']
                            # single_line[2]['amount'] = req_l['cal_amount']
                            # request_quantity = single_line[2]['requested_qty'] if single_line[2].get(
                            #     'requested_qty') else req_l.requested_qty
                            #
                            # if request_quantity >= req_l.free_qty:
                            #     update_permission.append(False)
                            # else:
                            #     update_permission.append(True)

        if False in update_permission:
            raise Warning(_('Request quantity should be less then or equal to (available quantity)!!!'))

        else:
            record = super(RtvProcess, self).write(vals)

            return record


class RtvProcessLine(models.Model):
    _name = "rtv.process.line"
    _description = "rtv Process Line"

    rtv_process_id = fields.Many2one('rtv.process', 'RTV Process ID', required=True, ondelete='cascade', index=True, )

    product_id = fields.Many2one('product.product', 'Product')
    po_number = fields.Char('Ref PO Number')
    pqc = fields.Boolean('PQC')
    pqc_processed = fields.Boolean('PQC Processed')
    cancel_time = fields.Datetime('Cancel Time')
    resolved_time = fields.Datetime('Resolved Time')
    received_time = fields.Datetime('Received Time')
    requested_time = fields.Datetime('Requested Time')
    confirmed_time = fields.Datetime('Confirmed Time')
    return_qty = fields.Float('Return Qty', store=True)
    remaining_qty = fields.Float('Remaining Qty')
    available_qty = fields.Float('')
    free_qty = fields.Float('Available Qty')
    requested_qty = fields.Float('Request Qty')
    unit_cost = fields.Float('Unit Rate')
    amount = fields.Float('Amount', store=True)
    cal_amount = fields.Float('')
    state = fields.Selection([
        ('pending', 'Pending'),
        ('requested', 'Requested'),
        ('resolved', 'Resolved'),
        ('confirmed', 'Ready for Return'),
        ('create_srn', 'SRN'),
        ('received', 'Received'),
        ('not_received', 'Not Received'),
        ('pending_approval', 'pending_approval'),
        ('approved', 'Approved'),
        ('cancel', 'Cancelled'),
    ], 'Status', default='pending', readonly=True, copy=False, help="Gives the status of the RTV Process", index=True)

    not_received_by = fields.Many2one('res.users', 'Not Received By')
    not_received_time = fields.Datetime('Not Received Time')

    not_receive_reason = fields.Selection([
        ("Shop off", "Shop off"),
        ("Vendor unreachable", "Vendor unreachable"),
        ("Vendor Rescheduled", "Vendor Rescheduled"),
        ("Does not agree to take", "Does not agree to take"),
        ("Not received due to quality", "Not received due to quality"),
        ("Vendor is not aware of", "Vendor is not aware of")
    ], 'Not Receive Reason', copy=False, help="Reason", index=True),

    srn_number = fields.Char("SRN Number")

    # ### Block code for V-14
    #
    # _columns = {
    #     'rtv_process_id': fields.many2one('rtv.processs', 'RTV Process ID', required=True,
    #                                       ondelete='cascade', index=True, readonly=True),
    #
    #     'product_id': fields.many2one('product.product', 'Product'),
    #     'po_number': fields.char('Ref PO Number'),
    #     'pqc': fields.boolean('PQC'),
    #     'pqc_processed': fields.boolean('PQC Processed'),
    #     'cancel_time': fields.datetime('Cancel Time'),
    #     'resolved_time': fields.datetime('Resolved Time'),
    #     'received_time': fields.datetime('Received Time'),
    #     'requested_time': fields.datetime('Requested Time'),
    #     'confirmed_time': fields.datetime('Confirmed Time'),
    #     'return_qty': fields.float('Return Qty'),
    #     'remaining_qty': fields.float('Remaining Qty'),
    #     'available_qty': fields.float(''),
    #     'free_qty': fields.float('Available Qty'),
    #     'requested_qty': fields.float('Request Qty'),
    #     'unit_cost': fields.float('Unit Rate'),
    #     'amount': fields.float('Amount'),
    #     'cal_amount': fields.float(''),
    #     'state': fields.selection([
    #         ('pending', 'Pending'),
    #         ('requested', 'Requested'),
    #         ('resolved', 'Resolved'),
    #         ('confirmed', 'Ready for Return'),
    #         ('create_srn', 'SRN'),
    #         ('received', 'Received'),
    #         ('not_received', 'Not Received'),
    #         ('pending_approval', 'pending_approval'),
    #         ('approved', 'Approved'),
    #         ('cancel', 'Cancelled'),
    #
    #     ], 'Status', readonly=True, copy=False, help="Gives the status of the RTV Process", index=True),
    #
    #     'not_received_by': fields.many2one('res.users', 'Not Received By'),
    #     'not_received_time': fields.datetime('Not Received Time'),
    #
    #     # 'not_receive_reason': fields.selection([
    #     #     ("Vendor was unreachable", "Vendor was unreachable"),
    #     #     ("Vendor refused to provide", "Vendor refused to provide"),
    #     #     ("Unable to reach", "Unable to reach"),
    #     #     ("Vendors place was closed", "Vendors place was closed"),
    #     #     ("Cash problem", "Cash problem"),
    #     #     ("Vendor rescheduled", "Vendor rescheduled"),
    #     #     ("Wrong address given", "Wrong address given"),
    #     #     ("Unable to reach on Time", "Unable to reach on Time"),
    #     #     ("Quality issue", "Quality issue"),
    #     #     ("Due to traffic issue", "Due to traffic issue")
    #     #
    #     # ], 'Not Receive Reason', copy=False, help="Reason", index=True),
    #
    #     'not_receive_reason': fields.selection([
    #         ("Shop off", "Shop off"),
    #         ("Vendor unreachable", "Vendor unreachable"),
    #         ("Vendor Rescheduled", "Vendor Rescheduled"),
    #         ("Does not agree to take", "Does not agree to take"),
    #         ("Not received due to quality", "Not received due to quality"),
    #         ("Vendor is not aware of", "Vendor is not aware of")
    #
    #     ], 'Not Receive Reason', copy=False, help="Reason", index=True),
    #
    #     'srn_number': fields.char("SRN Number"),
    # }
    #
    # _defaults = {
    #     'state': 'pending'
    # }

    @api.onchange('product_id')
    def onchange_product_id(self):
        # product_id = False
        # context = None
        warehouse_id = self._context.get('warehouse_id')

        if not warehouse_id:
            raise Warning(_("You must first select a Destination Warehouse"))
        values = {}

        if self.product_id:
            # prod_obj = self.env['product.product']
            # uid = 1
            # prod_search = prod_obj.search([('id', '=', product_id)])
            # prod_browse = self.env['product.product'].browse(prod_search)
            prod_browse = self.env["product.product"].search([("id", "=", self.product_id.id)])

            if warehouse_id == 1:  # Uttara
                values['available_qty'] = prod_browse.uttara_free_quantity
            elif warehouse_id == 2:  # Nodda
                values['available_qty'] = prod_browse.nodda_free_quantity
            elif warehouse_id == 3:  # Signboard
                values['available_qty'] = prod_browse.signboard_free_quantity
            elif warehouse_id == 7:  # Badda
                values['available_qty'] = prod_browse.badda_free_quantity
            elif warehouse_id == 4:  # Uttara Damage
                values['available_qty'] = prod_browse.uttara_damage_quantity
            elif warehouse_id == 5:  # Nodda Damage
                values['available_qty'] = prod_browse.nodda_damage_quantity
            elif warehouse_id == 6:  # Signboard Damage
                values['available_qty'] = prod_browse.signboard_damage_quantity
            elif warehouse_id == 9:  # Badda Damage
                values['available_qty'] = prod_browse.badda_damage_quantity
            elif warehouse_id == 10: # Badda horeca
                values['available_qty'] = prod_browse.badda_horeca_free_quantity
            elif warehouse_id == 11: # Savar
                values['available_qty'] = prod_browse.savar_free_quantity
            elif warehouse_id == 12: # Savar Damage
                values['available_qty'] = prod_browse.savar_damage_quantity

            values['free_qty'] = values['available_qty']

            if values['available_qty'] == float(0):
                raise Warning(_("This Product is not available in Stock"))

            self.unit_cost = prod_browse.standard_price

        return {'value': values}

    @api.onchange('requested_qty', 'unit_cost')
    def onchange_product_amount_cal(self):
        if self.requested_qty and self.unit_cost is not False:
            self.amount = float(self.requested_qty) * float(self.unit_cost)
            self.cal_amount = float(self.requested_qty) * float(self.unit_cost)
            self.return_qty = float(self.requested_qty)
        else:
            self.amount = 0.00
            self.cal_amount = 0.00
            self.return_qty = 0.00


class ResolveRtvProcessReason(models.Model):
    _name = "resolve.rtv.process.reason"
    _description = "Resolve rtv Process Reason"

    resolve_rtv_process_date = fields.Datetime('Resolve rtv Process Date')
    resolve_rtv_process_by = fields.Many2one('res.users', 'Resolve rtv Process By')
    resolve_rtv_process_reason = fields.Text('Resolve rtv Process Reason', required=True)

    # ## Block code for V-14
    # _columns = {
    #     'resolve_rtv_process_date': fields.datetime('Resolve rtv Process Date'),
    #     'resolve_rtv_process_by': fields.many2one('res.users', 'Resolve rtv Process By'),
    #     'resolve_rtv_process_reason': fields.text('Resolve rtv Process Reason', required=True),
    #
    # }

    def resolve_rtv_process_reason_def(self):
        self.ensure_one()
        if self._context.get('active_ids', False):
            for rtv in self.env['rtv.process'].search([('id', 'in', self._context.get('active_ids', False))]):
                rtv.write({
                    'resolve_rtv_process_reason': self.resolve_rtv_process_reason or "",
                    'resolve_rtv_process_by': self.env.uid,
                    'resolve_rtv_process_date': fields.Datetime.now()
                })

                rtv.resolve_rtv()


class CancelRtvProcessReason(models.Model):
    _name = "cancel.rtv.process.reason"
    _description = "Cancel rtv Process Reason"

    cancel_rtv_process_date = fields.Datetime('Cancel rtv Process Date')
    cancel_rtv_process_by = fields.Many2one('res.users', 'Cancel rtv Process By')
    cancel_rtv_process_reason = fields.Text('Cancel RTV Process Reason', required=True)

    # ## Block code for V-14
    # _columns = {
    #     'cancel_rtv_process_date': fields.datetime('Cancel rtv Process Date'),
    #     'cancel_rtv_process_by': fields.many2one('res.users', 'Cancel rtv Process By'),
    #     'cancel_rtv_process_reason': fields.text('Cancel RTV Process Reason', required=True),
    #
    # }

    def cancel_rtv_process_reason_def(self):
        self.ensure_one()
        if self._context.get('active_ids', False):
            for rtv in self.env['rtv.process'].search([('id', 'in', self._context.get('active_ids', False))]):
                rtv.write({
                    'cancel_rtv_process_reason': self.cancel_rtv_process_reason or "",
                    'cancel_rtv_process_by': self.env.uid,
                    'cancel_rtv_process_date': fields.Datetime.now()
                })

                rtv.cancel_rtv()


class CancelPendingRtvProcessReason(models.Model):
    _name = "cancel.pending.rtv.process.reason"
    _description = "Cancel rtv Process Reason"

    cancel_rtv_process_date = fields.Datetime('Cancel rtv Process Date')
    cancel_rtv_process_by = fields.Many2one('res.users', 'Cancel rtv Process By')
    cancel_rtv_process_reason = fields.Text('Cancel RTV Process Reason', required=True)

    # ## Block code for v-14
    # _columns = {
    #     'cancel_rtv_process_date': fields.datetime('Cancel rtv Process Date'),
    #     'cancel_rtv_process_by': fields.many2one('res.users', 'Cancel rtv Process By'),
    #     'cancel_rtv_process_reason': fields.text('Cancel RTV Process Reason', required=True),
    #
    # }

    def cancel_pending_rtv_process_reason_def(self):
        self.ensure_one()
        if self._context.get('active_ids', False):
            for rtv in self.env['rtv.process'].search([('id', 'in', self._context.get('active_ids', False))]):
                rtv.write({
                    'cancel_rtv_process_reason': self.cancel_rtv_process_reason or "",
                    'cancel_rtv_process_by': self.env.uid,
                    'cancel_rtv_process_date': fields.Datetime.now()
                })

                rtv.cancel_pending_rtv()


class PendingQualityControl(models.Model):
    _inherit = "pending.quality.control"
    _description = 'Pending Quality Control'

    stock_id = fields.Many2one('stock.picking', 'Stock Number')
    rtv_name = fields.Char('RTV Number')
    rtv_id = fields.Many2one('rtv.processs', 'RTV Process ID')

    def wms_inbound_pqc_rtv(self):
        for pqc in self:
            data = {}
            rtv_process_line = []
            if pqc.po_number:
                po_obj = self.env['purchase.order'].search([('name', '=', pqc.po_number)], limit=1)
                data['po_id'] = po_obj.id
                data['vendor_id'] = po_obj.partner_id.id
                vendor_data = self.env['rtv.process'].onchange_vendor_id(po_obj.partner_id.id)
                data['vendor_address'] = vendor_data['value']['vendor_address'] if vendor_data['value'].get(
                    'vendor_address', False) else ''
                data['vendor_contact'] = vendor_data['value']['vendor_contact'] if vendor_data['value'].get(
                    'vendor_contact', False) else ''
                data['area'] = po_obj.partner_id.state_id.id if po_obj.partner_id.state_id else None

            data['warehouse_id'] = pqc.warehouse_id.id
            data['rtv_type'] = 'pqc'
            data['pqc'] = True
            data['pqc_id'] = pqc.id
            data['pqc_number'] = pqc.pqc_number
            data['po_number'] = pqc.po_number
            data['pqc_processed'] = pqc.pqc_processed
            data['remark'] = ''
            data['state'] = 'pending'

            for pqc_line_obj in pqc.pqc_line:
                if not pqc_line_obj.pqc_processed:
                    if pqc_line_obj.pqc_reject_quantity > float(0):
                        pqc_reject_quantity = pqc_line_obj.pqc_reject_quantity
                    else:
                        pqc_reject_quantity = pqc_line_obj.pending_quantity

                    rtv_process_line.append([0, False, {
                        # 'rtv_id': pqc_data.rtv_id,
                        # 'rtv_name': pqc_data.rtv_name,
                        # 'stock_id': pqc.stock_id.id,
                        # 'product': pqc_line_obj.product,
                        'product_id': pqc_line_obj.product_id.id,
                        'return_qty': pqc_reject_quantity,
                        'available_qty': pqc_reject_quantity,
                        # 'rtv_process_id': pqc_data.id,
                        'amount': 0,
                        'unit_cost': 0,
                        'requested_qty': pqc_reject_quantity,
                        'cal_amount': 0,
                        'state': 'pending',
                    }])

            if len(rtv_process_line) > 0:
                data['rtv_process_line'] = rtv_process_line

                save_the_data = self.env['rtv.process'].create(data)
                save_the_data.email_template('RTV Ticket')
                pqc.write({
                    'state': 'return_to_vendor',
                    'pqc_processed': True,
                    'rtv_id': save_the_data.id
                })

                for pqc_line_obj in pqc.pqc_line:
                    if not pqc_line_obj.pqc_processed:
                        pqc_line_obj.write({
                            'state': 'return_to_vendor',
                            'pqc_processed': True,
                            'rtv_id': save_the_data.id
                        })


class PendingQualityControlLine(models.Model):
    _inherit = "pending.quality.control.line"
    _description = 'Pending Quality Control Line'

    stock_id = fields.Many2one('stock.picking', 'Stock Number')
    rtv_name = fields.Char('RTV Number')
    rtv_id = fields.Many2one('rtv.processs', 'RTV Process ID')

    def wms_inbound_pqc_line_rtv(self):
        data = {}
        rtv_process_line = []

        for pqcl in self:
            pqc_line_obj = pqcl
            # pqc_obj = self.env['pending.quality.control']
            pqc = pqcl.pqc_id

            if pqc.po_number:
                po_obj = self.env['purchase.order'].search([('name', '=', pqc.po_number)], limit=1)
                data['po_id'] = po_obj.id
                data['vendor_id'] = po_obj.partner_id.id
                vendor_data = self.env['rtv.process'].onchange_vendor_id(po_obj.partner_id.id)
                data['vendor_address'] = vendor_data['value']['vendor_address'] if vendor_data['value'].get(
                    'vendor_address', False) else ''
                data['vendor_contact'] = vendor_data['value']['vendor_contact'] if vendor_data['value'].get(
                    'vendor_contact', False) else ''
                data['area'] = po_obj.partner_id.state_id.id if po_obj.partner_id.state_id else None

            data['warehouse_id'] = pqc.warehouse_id.id
            data['rtv_type'] = 'pqc'
            data['pqc'] = True
            data['pqc_id'] = pqc.id
            data['pqc_number'] = pqc.pqc_number
            data['po_number'] = pqc.po_number
            data['pqc_processed'] = pqc.pqc_processed
            data['remark'] = ''
            data['state'] = 'pending'

            if pqc_line_obj.pqc_reject_quantity > float(0):
                pqc_reject_quantity = pqc_line_obj.pqc_reject_quantity
            else:
                pqc_reject_quantity = pqc_line_obj.pending_quantity

            rtv_process_line.append([0, False, {
                # 'rtv_id': pqc_data.rtv_id,
                # 'rtv_name': pqc_data.rtv_name,
                # 'stock_id': pqc.stock_id.id,
                # 'product': pqc_line_obj.product,
                'product_id': pqc_line_obj.product_id.id,
                'return_qty': pqc_reject_quantity,
                'available_qty': pqc_reject_quantity,
                # 'rtv_process_id': pqc_data.id,
                'amount': 0,
                'unit_cost': 0,
                'requested_qty': pqc_reject_quantity,
                'cal_amount': 0,
                'state': 'pending',
            }])

            data['rtv_process_line'] = rtv_process_line

            save_the_data = self.env['rtv.process'].create(data)
            save_the_data.email_template('RTV Ticket')

            pqcl.write({
                'state': 'return_to_vendor',
                'pqc_processed': True,
                'rtv_id': save_the_data.id
            })
