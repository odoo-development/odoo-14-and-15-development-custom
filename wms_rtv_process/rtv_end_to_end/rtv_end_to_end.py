from odoo import api, fields, models, _

class RtvEndToEnd(models.Model):
    _name = "rtv.end.to.end"
    _description = "Inbound End-to-End"

    rtv_number = fields.Char('RTV Number')
    rtv_id = fields.Integer('RTV ID')
    warehouse_name = fields.Char('Warehouse')
    vendor_name = fields.Char('Vendor')
    ref_po_number = fields.Char('Ref PO Number')
    ref_pqc_number = fields.Char('Ref PO Number')
    stock_number = fields.Char('Stock Number')
    rev_stock_number = fields.Char('Reverse Stock Number')
    aqc_number = fields.Char('Stock')
    rev_pqc_number = fields.Char('Reverse PQC Number')
    rtv_date_create = fields.Text('Created Date')
    rtv_date_requested = fields.Text('Requested Date')
    rtv_date_resolved = fields.Text('Resolved Date')
    rtv_date_received = fields.Text('Received Date')
    rtv_date_reattempt = fields.Text('Reattempt Date')
    rtv_date_confirm = fields.Text('Ready for Return Date')
    rtv_date_not_received = fields.Text('Not Received Date')
    srn_number = fields.Char('SRN Numbers')
    challan_number = fields.Char('Challan Numbers')
    srn_date_create = fields.Text('SRN Created Date')
    rtv_date_cancel = fields.Text('Cancel Date')
    rtv_date_approval = fields.Text('Approval Date')
    srn_date_cancel = fields.Text('SRN Cancel Date')
    state = fields.Selection([
        ('pending', 'Pending'),
        ('requested', 'Requested'),
        ('resolved', 'Resolved'),
        ('confirmed', 'Ready for Return'),
        ('create_srn', 'Create SRN'),
        ('received', 'Received'),
        ('not_received', 'Not Received'),
        ('pending_approval', 'Pending Approval'),
        ('approved', 'Approved'),
        ('cancel', 'Cancelled'),
    ], 'Status', readonly=True, copy=False, help="Gives the status of the RTV Process", index=True)

    # ###Block code for V-14
    #
    # _columns = {
    #     'rtv_number': fields.char('RTV Number'),
    #     'rtv_id': fields.integer('RTV ID'),
    #     'warehouse_name': fields.char('Warehouse'),
    #     'vendor_name': fields.char('Vendor'),
    #     'ref_po_number': fields.char('Ref PO Number'),
    #     'ref_pqc_number': fields.char('Ref PO Number'),
    #     'stock_number': fields.char('Stock Number'),
    #     'rev_stock_number': fields.char('Reverse Stock Number'),
    #     'aqc_number': fields.char('Stock'),
    #     'rev_pqc_number': fields.char('Reverse PQC Number'),
    #     'rtv_date_create': fields.text('Created Date'),
    #     'rtv_date_requested': fields.text('Requested Date'),
    #     'rtv_date_resolved': fields.text('Resolved Date'),
    #     'rtv_date_received': fields.text('Received Date'),
    #     'rtv_date_reattempt': fields.text('Reattempt Date'),
    #     'rtv_date_confirm': fields.text('Ready for Return Date'),
    #     'rtv_date_not_received': fields.text('Not Received Date'),
    #     'srn_number': fields.char('SRN Numbers'),
    #     'challan_number': fields.char('Challan Numbers'),
    #     'srn_date_create': fields.text('SRN Created Date'),
    #     'rtv_date_cancel': fields.text('Cancel Date'),
    #     'rtv_date_approval': fields.text('Approval Date'),
    #     'srn_date_cancel': fields.text('SRN Cancel Date'),
    #     'state': fields.selection([
    #         ('pending', 'Pending'),
    #         ('requested', 'Requested'),
    #         ('resolved', 'Resolved'),
    #         ('confirmed', 'Ready for Return'),
    #         ('create_srn', 'Create SRN'),
    #         ('received', 'Received'),
    #         ('not_received', 'Not Received'),
    #         ('pending_approval', 'Pending Approval'),
    #         ('approved', 'Approved'),
    #         ('cancel', 'Cancelled'),
    #
    #     ], 'Status', readonly=True, copy=False, help="Gives the status of the RTV Process", index=True),
    # }

    def rtv_data_create(self, rtv_number, rtv_id, value):

        rtv_count = self.search_count([('rtv_number', '=', rtv_number), ('rtv_id', '=', rtv_id)])

        if int(rtv_count) == 0:
            created_id = self.create({
                'rtv_number': rtv_number,
                'rtv_id': rtv_id,
                'vendor_name': value['vendor_name'],
                'warehouse_name': value['warehouse_name'],
                'rtv_date_create': value['rtv_date_create'],
                'ref_po_number': value['ref_po_number'] if value['ref_po_number'] else '',
                'ref_pqc_number': value['ref_pqc_number'] if value['ref_pqc_number'] else '',
                'state': value['state']
            })
        return created_id

    def rtv_confirm_update(self, rtv_number, rtv_id, state=None):
        for etoe in self.search([('rtv_number', '=', rtv_number), ('rtv_id', '=', rtv_id)], limit=1):
            etoe.write({
                'rtv_date_confirm': str(fields.Datetime.now()) if etoe.rtv_date_confirm else str(etoe.rtv_date_confirm) + "," + str(fields.Datetime.now()),
                'state': state
            })

    def rtv_request_update(self, rtv_number, rtv_id, state=None):
        for etoe in self.search([('rtv_number', '=', rtv_number), ('rtv_id', '=', rtv_id)], limit=1):
            etoe.write({
                'rtv_date_requested': str(fields.Datetime.now()) if etoe.rtv_date_requested else str(etoe.rtv_date_requested) + "," + str(fields.Datetime.now()),
                'state': state
            })

    def rtv_resolve_update(self, rtv_number, rtv_id, state=None):
        for etoe in self.search([('rtv_number', '=', rtv_number), ('rtv_id', '=', rtv_id)], limit=1):
            etoe.write({
                'rtv_date_resolved': str(fields.Datetime.now()) if etoe.rtv_date_resolved else str(etoe.rtv_date_resolved) + "," + str(fields.Datetime.now()),
                'state': state
            })

    def rtv_reattempt_update(self, rtv_number, rtv_id, state=None):
        for etoe in self.search([('rtv_number', '=', rtv_number), ('rtv_id', '=', rtv_id)], limit=1):
            etoe.write({
                'rtv_date_reattempt': str(fields.Datetime.now()) if str(etoe.rtv_date_reattempt) == 'False' else str(etoe.rtv_date_reattempt) + "," + str(fields.Datetime.now()),
                'state': state
            })

    def rtv_approval_update(self, rtv_number, rtv_id, state=None):
        for etoe in self.search([('rtv_number', '=', rtv_number), ('rtv_id', '=', rtv_id)], limit=1):
            etoe.write({
                'rtv_date_approval': str(fields.Datetime.now()) if str(etoe.rtv_date_approval) == 'False' else str(etoe.rtv_date_approval) + "," + str(fields.Datetime.now()),
                'state': state
            })

    def rtv_received_update(self, rtv_number, rtv_id, state=None):
        for etoe in self.search([('rtv_number', '=', rtv_number), ('rtv_id', '=', rtv_id)], limit=1):
            etoe.write({
                'rtv_date_received': str(fields.Datetime.now()) if str(etoe.rtv_date_received) == 'False' else str(etoe.rtv_date_received) + "," + str(fields.Datetime.now()),
                'state': state
            })

    def rtv_not_received_update(self, rtv_number, rtv_id, state=None):
        for etoe in self.search([('rtv_number', '=', rtv_number), ('rtv_id', '=', rtv_id)], limit=1):
            etoe.write({
                'rtv_date_not_received': str(fields.Datetime.now()) if str(etoe.rtv_date_not_received) == 'False' else str(etoe.rtv_date_not_received) + "," + str(fields.Datetime.now()),
                'state': state
            })

    def rtv_srn_update(self, rtv_number, rtv_id, srn_number, srn_rtv_number, state):
        for etoe in self.search([('rtv_number', '=', rtv_number),
                                 ('rtv_id', '=', rtv_id)], limit=1):
            etoe.write({
                'srn_number': srn_number,
                'challan_number': srn_rtv_number if not etoe.challan_number else str(etoe.challan_number) + "," + str(srn_rtv_number),
                'state': str(state),
                'srn_date_create': str(fields.Datetime.now()) if not etoe.srn_date_create else str(etoe.srn_date_create) + "," + str(fields.Datetime.now()),
            })

    def rtv_cancel_update(self, rtv_number, rtv_id, state):
        for etoe in self.search([('rtv_number', '=', rtv_number), ('rtv_id', '=', rtv_id)], limit=1):
            etoe.write({
                'rtv_date_cancel': str(fields.Datetime.now()) if etoe.rtv_date_cancel else str(etoe.rtv_date_cancel) + "," + str(fields.Datetime.now()),
                'state': state
            })

    def srn_cancel_update(self, rtv_number, rtv_id, state=None):
        for etoe in self.search([('rtv_number', '=', rtv_number), ('rtv_id', '=', rtv_id)], limit=1):
            etoe.write({
                'srn_date_cancel': str(fields.Datetime.now()) if str(etoe.srn_date_cancel) == 'False' else str(etoe.srn_date_cancel) + "," + str(fields.Datetime.now()),
                'state': state
            })


