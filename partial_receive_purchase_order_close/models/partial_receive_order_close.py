import odoo

from odoo import fields, models, _


class PurchaseOrder(models.Model):
    _inherit = "purchase.order"

    received_amount = fields.Float(string='Received Amount',compute='_get_received_amount')
    # order_close_reason = fields.Char('Purchase Order Close Reason', required=True)
    order_close_reason = fields.Char('Purchase Order Close Reason')

    # ## Block code for V-14
    # _columns = {
    #     'received_amount': fields.function(_get_received_amount, string='Received Amount', type='float'),
    #     'order_close_reason': fields.char('Purchase Order Close Reason', required=True),
    # }
    # Received amount
    def _get_received_amount(self):

        for po in self:
            total_sum = 0.00
            for invoice in po.invoice_ids:
                if invoice.move_type == 'in_invoice' and invoice.state!='cancel':
                    total_sum = total_sum + invoice.amount_total
                if invoice.move_type == 'in_refund' and invoice.state!='cancel':
                    total_sum = total_sum - invoice.amount_total
            # res[po.id] = total_sum
            po.received_amount = total_sum

        # return res

class PartiallyReceivedOrderClose(models.Model):
    _name = "partially.received.order.close"
    _description = "Partially received order close"

    order_close_reason = fields.Char('Order Close Reason', required=True)
    order_name = fields.Char('Order Name')
    order_ref = fields.Char('Order Ref.')

    ### Block code for V-14
    # _columns = {
    #
    #     'order_close_reason': fields.char('Order Close Reason', required=True),
    #     'order_name': fields.char('Order Name'),
    #     'order_ref': fields.char('Order Ref.'),
    # }

    def partial_receive_close(self):

        close_order_tbl_ids = self.ids
        context = []
        ids = context['active_ids']
        order_close_reason = context['order_close_reason']
        po_obj = self.env['purchase.order']

        for po in po_obj.browse(ids, context=context):

            stock_picking_query = "SELECT id FROM stock_picking WHERE origin='{0}' and state!='done'".format(
                str(po.name))
            self.env.cr.execute(stock_picking_query)

            for sp_id in self.env.cr.fetchall():
                # unreserve
                self.env['stock.picking'].do_unreserve(int(sp_id[0]))

                # action cancel
                self.env['stock.picking'].action_cancel(int(sp_id[0]))

        # Action Done
        po_obj.wkf_po_done()

        for po_id in ids:
            po_state_query = "UPDATE purchase_order SET shipped=TRUE, order_close_reason='{0}' WHERE id='{1}'".format(
                order_close_reason, po_id)

            self.env.cr.execute(po_state_query)
            self.env.cr.commit()

            for po in po_obj.browse(po_id):
                order_name = str(po.name)
                order_ref = str(po.client_order_ref)

                for co_id in close_order_tbl_ids:
                    po_state_query = "UPDATE partially_received_order_close SET order_name='{0}', order_ref='{1}' WHERE id='{2}'".format(
                        order_name, order_ref, co_id)

                    self.env.cr.execute(po_state_query)
                    self.env.cr.commit()

        return True
