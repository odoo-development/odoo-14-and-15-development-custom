{
    'name': 'Send SMS on Demand',
    'version': "14.0.1.0.0",
    'author': "Odoo Bangladesh",
    'category': 'SMS',
    'summary': 'Send SMS on Demand',
    'depends': ['odoo_magento_connect', 'account', 'sale_management', 'purchase', 'wms_manifest', 'order_cancel_reasons',
                'sms_gateway_custom'],
    'data': [
        'security/head_mapping_security.xml',
        'security/ir.model.access.csv',
        'views/c_head_mapping_view.xml',
        'views/w_head_mapping_view.xml',
        'views/head_mapping_menu.xml',
        'views/ch_wh_com_sync_scheduler.xml',

    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}
