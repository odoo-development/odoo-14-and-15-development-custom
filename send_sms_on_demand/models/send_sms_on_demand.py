
import logging
import re
from datetime import datetime
from odoo import api, fields, models, _

_logger = logging.getLogger(__name__)


class SendSmsOnDemand(models.Model):
    _name = "send.sms.on.demand"
    _description = 'Send Sms On Demand'

    def send_sms_on_demand(self, sms_text, mobile_numbers, name):
        try:
            # commited by shuvarthi
            new_r = self.env['sms.api'].send_sms_by_ssl_api(mobile_numbers,sms_text)
        except:
            pass

        return True

    def get_customer_mobile(self, so):
        mobile = ''

        # res_partner_obj = self.env["res.partner"]
        # parent = res_partner_obj.browse(cr, uid, customer_id, context=context)

        parent = so.partner_id
        for i in range(5):
            if len(parent.parent_id) == 1:
                parent = parent.parent_id
            else:
                parent = parent
                break
        company_phone = parent.mobile
        mobile = so.partner_id.mobile if so.partner_id.mobile else company_phone
        if not mobile:
            mobile = so.partner_id.phone

        return mobile


class WmsManifestProcess(models.Model):
    _inherit = "wms.manifest.process"

    def confirm_wms_manifest(self):
        resp = super(WmsManifestProcess, self).confirm_wms_manifest()
        # send sms
        # Out for Delivery : Hi [First Name], Your order #[Order ID]  has now been dispatched and will be delivered today by Sindabad Wish Master. Please keep Tk [Amount]  ready.Helpline: 09612-002244


        # wms_man = self.env["wms.manifest.process"].browse()
        wms_man = self
        # account_inv = self.env["account.move"]
        so_obj = self.env["sale.order"]
        res_partner_obj = self.env["res.partner"]
        try:
            for line in wms_man.picking_line:

                customer_name = line.customer_name
                name = "wms.manifest.process." + str(wms_man.id)
                order = line.magento_no
                mobile_numbers = line.phone
                amount = line.amount
                so = so_obj.search([('client_order_ref', '=', str(order)),('state', '!=','cancel')])
                # so = so_obj.browse(so_list)
                sms_text = ""
                sms_text_rm = ""
                rm_mobile_numbers = ''

                company_id = ""
                parent = so.partner_id
                classification = str(so.customer_classification)
                email = ''

                for i in range(5):
                    if len(parent.parent_id) == 1:
                        parent = parent.parent_id
                    else:
                        rm_mobile_numbers = so.sales_representative_mobile
                        email = parent.email
                        # company_id = parent.id
                        break

                order_warehouse = str(so.warehouse_id.name)
                # Partial delivery messages
                total_inv_amount = 0

                for inv in so.invoice_ids:
                    total_inv_amount += inv.amount_total

                if wms_man.assigned_to:
                    assigned_to = str(wms_man.assigned_to)
                else:
                    assigned_to = ''

                if total_inv_amount < so.amount_total:

                    try:
                        # compare_date_obj = datetime.datetime.now() + datetime.timedelta(days=7)
                        # expected_shipment_date = compare_date_obj.strftime('%Y-%m-%d')

                        if 'Corporate' in classification :
                            partial_delivery_msg = "Your order {0} has been shipped partially and will be delivered today by Sindabad Wish Master {1}, the rest will be shipped soon. Sorry for your inconveniences.".format(order, assigned_to)

                        else:

                            partial_delivery_msg = "Dear {3} Your order {0} has been shipped partially and will be delivered today by {1}, the rest will be shipped soon. Sorry for your inconveniences. Your total bill is Tk {2}. Thank you shopping with us.".format(order,assigned_to, format(int(round(total_inv_amount)), ','), customer_name)

                        self.env["send.sms.on.demand"].send_sms_on_demand(partial_delivery_msg, mobile_numbers,
                                                                              name)
                    except:
                        pass

                elif "Cash On Delivery" in str(so.note):

                    if 'Corporate' in classification:

                        sms_text = "Dear {2} Your order {0} has been shipped and will be delivered today by {1}.".format(order, assigned_to, customer_name)


                    else:

                        sms_text = "Dear {3} Your order {0} has been shipped and will be delivered today by {1}. Your total bill is Tk {2}. Thank you shopping with us.".format(order, assigned_to, format(int(round(amount)), ','), customer_name)

                else:

                    if 'Corporate' in classification:

                        sms_text = "Dear {2} Your order {0} has been shipped and will be delivered today by {1}.Thank you shopping with us.".format(order, assigned_to, customer_name)

                    else:

                        sms_text = "Dear {3} Your order {0} has been shipped and will be delivered today by {1}. Your total bill is Tk {2}.Thank you shopping with us.".format(order, assigned_to, format(int(round(amount)), ','), customer_name)

                sms_text_rm = "Dear RM, Order {0} of your client {1} has been shipped with Value Tk {2} from {3}.".format(order, customer_name, format(int(round(amount)), ','), order_warehouse)

                # only retail customer will get the SMS
                # if "Retail" in str(so.customer_classification):
                self.env["send.sms.on.demand"].send_sms_on_demand(sms_text, mobile_numbers, name)

                if rm_mobile_numbers:
                    self.env["send.sms.on.demand"].send_sms_on_demand(sms_text_rm, rm_mobile_numbers, name)

                try:
                    # send email
                    if not email[0].isdigit() and sms_text!='':
                        # tmp_data = e_log_obj.browse(cr, uid, email_log_id, context=context)
                        # send email
                        if re.search("^(\d+)@sindabad.com$", email):
                            sin_email_match = re.search("^(\d+)@sindabad.com$", email)
                        elif re.search("^sin(\d+)@sindabad.com$", email):
                            sin_email_match = re.search("^sin(\d+)@sindabad.com$", email)
                        elif re.search("@sindabadretail.com$", email):
                            sin_email_match = re.search("@sindabadretail.com$", email)
                        else:
                            sin_email_match = None

                        if sin_email_match is None:
                            # e_log_obj = self.env['send.email.on.demand.log']
                            #
                            # email_data = {
                            #     'mail_text': sms_text,
                            #     'model': "sale.order",
                            #     'so_id': so.id,
                            #     'email': email
                            # }
                            #
                            # email_log_id = e_log_obj.create(email_data)
                            email_template_obj = self.env['mail.template']
                            template_ids = email_template_obj.sudo().search([('name', '=', 'Shipped')])
                            template_ids.send_mail(so.id, force_send=True)
                except:
                    pass

                # try:
                #     status = 'dispatched'
                #     self.env['order.status.synch.log'].offer_notification_api(order, status)
                # except:
                #     pass
        except:
            pass

        return True


class WmsManifestOutbound(models.Model):
    _inherit = "wms.manifest.outbound"

    def create(self, vals):

        outbound = super(WmsManifestOutbound, self).create(vals)
        try:
            outbound.deliverd_dispatched_msg()
        except:
            pass
        return outbound


    def deliverd_dispatched_msg(self):

        resp = self
        try:

            """
            Delivered: Your order #[Order ID] has been delivered successfully . Thanks for staying with us at sindabad.com. Helpline: 09612-002244
            Return: Your shipment of [Tk amount] is being returned due to [Reason]. For any query please call us on helpline: 09612-002244
            
            Partial Delivery: Hi <Customer First Name>, your order <order number> has been dispatched partially amounting <amount>. Rest item/s will be dispatched within few days. You will be notified before dispatch. A list of items which could not be shipped are already shared in your email. We are sorry for any inconveniences for partial delivery. To enquire more, please call 09612-002244
            """

            wms_out = self
            so_obj = self.env["sale.order"]

            sms_obj = self.env["send.sms.on.demand"]
            order_id = wms_out.name
            invoice_id = wms_out.outbound_line[0].invoice_id if wms_out.outbound_line else False

            if invoice_id:
                try:
                    inv = self.env["account.move"].browse([int(invoice_id)])

                    mobile_numbers = inv.partner_id.phone
                    name = "wms.manifest.outbound." + str(wms_out.id)
                    so = so_obj.search([('client_order_ref', '=', str(order_id)),('state', '!=','cancel')])
                    # so = so_obj.browse(so_list)
                    mobile_numbers = self.env["send.sms.on.demand"].get_customer_mobile(so)

                    order = so.client_order_ref
                    amount = inv.amount_total
                    customer_name = ''
                    rm_mobile_numbers = ''
                    parent = so.partner_id
                    classification = str(so.customer_classification)
                    email = ''
                    rm_name = ''

                    for i in range(5):
                        if len(parent.parent_id) == 1:
                            parent = parent.parent_id
                        else:
                            rm_mobile_numbers = so.sales_representative_mobile
                            customer_name = parent.name
                            rm_name = str(so.sales_representative_name)
                            email = str(so.sales_representative_email)
                            # company_id = parent.id
                            break

                    # only retail customer will get the SMS
                    sms_text_delivered = "Your order {0} has been delivered successfully. Thank you for staying with us.".format(
                        order_id)
                    sms_text_rm = "Dear {4}, Order {0} of your client {1} - Mob- {2} has been delivered with Value Tk {3}.".format(
                        order, customer_name, mobile_numbers, format(int(round(amount)), ','), rm_name)

                    # Partial delivery messages
                    total_inv_amount = 0

                    for inv in so.invoice_ids:
                        total_inv_amount += inv.amount_total

                    if total_inv_amount < so.amount_total:

                        try:
                            # compare_date_obj = datetime.now() + datetime.timedelta(days=7)
                            # expected_shipment_date = compare_date_obj.strftime('%Y-%m-%d')

                            if 'Corporate' in classification:
                                partial_delivery_msg = "Your order {0} has been shipped partially, the rest will be shipped soon. Sorry for your inconveniences.".format(order)
                            else:

                                partial_delivery_msg = "Your order {0} has been shipped partially, the rest will be shipped soon. Sorry for your inconveniences. Please keep Tk {1} ready.".format(order, format(int(round(total_inv_amount)), ','))

                            sms_obj.send_sms_on_demand(partial_delivery_msg, mobile_numbers, name)
                        except:
                            pass

                    # if "Retail" in str(so.customer_classification):
                    try:
                        try:
                            if total_inv_amount >= so.amount_total:
                                sms_obj.send_sms_on_demand(sms_text_delivered, mobile_numbers, name)
                                sms_obj.send_sms_on_demand(sms_text_rm, rm_mobile_numbers, name)
                        except:
                            pass

                        try:
                            if not email[0].isdigit():
                                if re.search("^(\d+)@sindabad.com$", email):
                                    sin_email_match = re.search("^(\d+)@sindabad.com$", email)
                                elif re.search("^sin(\d+)@sindabad.com$", email):
                                    sin_email_match = re.search("^sin(\d+)@sindabad.com$", email)
                                elif re.search("@sindabadretail.com$", email):
                                    sin_email_match = re.search("@sindabadretail.com$", email)
                                else:
                                    sin_email_match = None

                                if sin_email_match is None:

                                    email_template_obj = self.env['mail.template']
                                    template_ids = email_template_obj.sudo().search([('name', '=', 'Order Delivery')])
                                    template_ids.send_mail(so.id, force_send=True)
                        except:
                            pass

                        returned_amount = 0
                        for line in wms_out.outbound_line:
                            if line.return_qty > 0:
                                single_price = line.amount / line.quantity
                                returned_amount += line.return_qty * single_price

                        sms_text_return = "Your shipment of Tk {0} of order {1} has been returned. For any query please call us on Helpline: 09612002244".format(
                            format(int(round(returned_amount)), ','), order)

                        # only retail customer will get the SMS
                        # if returned_amount > 0 and "Retail" in str(so.customer_classification):
                        # if returned_amount > 0:
                        #     sms_obj.send_sms_on_demand(cr, uid, ids, sms_text_return, mobile_numbers, name, context=context)
                    except:
                        pass

                except:
                    pass

            # try:
            #
            #     status = 'delivered'
            #     self.env['order.status.synch.log'].offer_notification_api(order_id, status)
            # except:
            #     pass
        except:
            pass
        return True


# (a) Welcome Message:
"""
Hello
Welcome to Sindabad.com Ltd! We are happy to have you on
board.
Please visit Sindabad.com to discover thousands of products.
"""


# class ResPartner(models.Model):
#     _inherit = 'res.partner'
#
#     @api.model
#     def create(self, vals):
#
#         try:
#             if vals.has_key('mobile'):
#                 vals['phone'] = vals['mobile']
#             elif vals.has_key('phone'):
#                 vals['mobile'] = vals['phone']
#         except:
#             pass
#         partner = super(ResPartner, self).create(vals)
#
#         try:
#             phone_number = ''
#             if vals.has_key('mobile'):
#                 phone_number = vals['mobile']
#             if not phone_number:
#                 phone_number = vals['phone']
#             name = vals['name']
#
#             email = ''
#             if vals.has_key('email'):
#                 email = vals['email']
#             """
#             sms_text = ''
#             if phone_number:
#                 sms_text = "Hello,\n Welcome to Sindabad.com Ltd! We are happy to have you on board. Please visit Sindabad.com to discover thousands of products."
#
#                 self.env["send.sms.on.demand"].send_sms_on_demand(self._cr, self._uid, self._ids, sms_text, phone_number, name, context=self._context)
#
#             # send email
#             if email:
#                 e_log_obj = self.env['send.email.on.demand.log']
#
#                 email_data = {
#                     'mail_text': sms_text,
#                     'model': "res.partner",
#                     # 'so_id': ids[0],
#                     'email': email
#                 }
#
#                 email_log_id = e_log_obj.create(self._cr, self._uid, email_data, context=self._context)
#                 # tmp_data = e_log_obj.browse(cr, uid, email_log_id, context=context)
#
#                 # send email
#                 email_template_obj = self.env['mail.template']
#                 template_ids = email_template_obj.search(self._cr, self._uid, [('name', '=', 'SO process mail')], context=self._context)
#
#                 email_template_obj.send_mail(self._cr, self._uid, template_ids[0], email_log_id, force_send=True, context=self._context)
#             """
#         except:
#             pass
#
#         return partner


# (b) Order hold for due Message
"""
Dear Valued Customer,
Your credit amount 'amount' has exceed the limit. Please
clear the previous dues so that process the order. For
further details, please contact your relationship manager.
"""
# implemented in order_approval_process

# (c) Order Confirmation Message
"""
Hello "Company Name" ,
Your order "order number" has been placed on "Sunday, date
and time" via "Cash On Delivery/on credit". You will be
updated with another email after your item(s) has been
dispatched.
"""


class SaleOrder(models.Model):
    _inherit = "sale.order"

    def action_cancel(self):
        super(SaleOrder, self).action_cancel()
        so = self
        try:
            # so = self.browse()
            parent = so.partner_id
            for i in range(5):
                if len(parent.parent_id) == 1:
                    parent = parent.parent_id
                else:
                    parent = parent
                    break
            company_phone = parent.mobile
            phone_number = str(so.partner_id.mobile) if so.partner_id.mobile else company_phone
            # so_cancel_date = datetime.datetime.now().strftime('%A, %d-%m-%Y')
            if not phone_number:
                phone_number = str(so.partner_id.phone)

            email = str(so.partner_id.email)
            cancel_reason = ""
            if so.cancel_co_reason:
                cancel_reason += str(so.cancel_co_reason).replace("_", " ")
            if so.cancel_quotation_reason:
                cancel_reason += str(so.cancel_quotation_reason).replace("_", " ")

            if cancel_reason == 'apnd':
                cancel_reason = "Advance payment not done"

            sms_text = ''
            if phone_number and cancel_reason:
                sms_text = "Hello {0}, we are sorry to inform that your order {1} has been canceled due to '{2}'".format(
                    str(parent.name), str(so.client_order_ref), cancel_reason)

                self.env["send.sms.on.demand"].send_sms_on_demand(sms_text, phone_number, str(parent.name))

            # send email
            if email and cancel_reason:
                # send email
                if re.search("^(\d+)@sindabad.com$", email):
                    sin_email_match = re.search("^(\d+)@sindabad.com$", email)
                elif re.search("^sin(\d+)@sindabad.com$", email):
                    sin_email_match = re.search("^sin(\d+)@sindabad.com$", email)
                elif re.search("@sindabadretail.com$", email):
                    sin_email_match = re.search("@sindabadretail.com$", email)
                else:
                    sin_email_match = None

                if sin_email_match is None and cancel_reason:
                    email_template_obj = self.env['mail.template']
                    template_ids = email_template_obj.sudo().search([('name', '=', 'Order Canceled')])
                    template_ids.send_mail(so.id, force_send=True)
        except:
            pass
        try:
            product_ids =[order_line.product_id.id for order_line in so.order_line if order_line.product_id.default_code not in 'Delivery_007' and \
                                order_line.product_id.default_code not in 'CSC_007' and \
                                order_line.product_id.default_code not in 'DS_007']

            self.env['product.product'].stock_synch(self.env['product.product'].browse(product_ids))

        except:
            pass

        return True


# Order Dispatch and Delivery Message
"""
Same as email and SMS send to retail customer
"""

# (d) Order Cancelled message
"""
Hello "Company Name" ,
Your order "order number" has been cancelled on "Sunday,
date and time" due to "cancel reason".
"""
# implemented in def action_cancel(self, cr, uid, ids, context=None):


# Delay Message (Category Head)
"""
Order number "Number" is pending for 2/4 days
due to unavailability of product "Product name".
"""


class CategoryHeadMapping(models.Model):
    _name = "category.head.mapping"
    _description = 'Category Head Mapping'

    ch_mapping = fields.Text("Category Head Mapping {'category_id':user_id,'category_id':user_id}")

    # _columns = {
    #     'ch_mapping': fields.text("Category Head Mapping {'category_id':user_id,'category_id':user_id}"),
    # }

    def product_unavailable_order_process_ch(self):
        uid = 1

        try:
            cat_head_obj = self.env['category.head.mapping']
            cat_head = cat_head_obj.browse(uid, [1])
            # mapping_data = json.load(cat_head.mapping)
            mapping_data = eval(str(cat_head.ch_mapping))

            comm_heads = dict()

            date_4 = datetime.datetime.now() - datetime.timedelta(days=4)
            date_2 = datetime.datetime.now() - datetime.timedelta(days=2)

            date_before_4 = date_4.strftime('%Y-%m-%d')
            date_before_2 = date_2.strftime('%Y-%m-%d')

            so_obj = self.env["sale.order"]
            sos = so_obj.search([("date_order", ">", date_before_4), ("date_order", "<", date_before_2),
                                              ("state", "!=", "cancel"), ("state", "!=", "done")])
            # sos = so_obj.browse(so_list)

            users_obj = self.env["res.users"]
            for order in sos:
                for line in order.order_line:
                    sms_text = "Order number %23{0} is pending for 2/4 days due to unavailability of product {1}.".format(
                        str(order.client_order_ref), str(line.product_id.product_name))
                    categ_id = str(line.product_id.categ_id.id)
                    categ_head_id = mapping_data[categ_id]

                    # if not comm_heads.has_key(categ_id) and mapping_data.has_key(categ_id):
                    #    comm_heads[categ_id] = mapping_data[categ_id]

                    partner = users_obj.browse(categ_head_id)

                    phone_number = str(partner.partner_id.mobile)
                    email = str(partner.partner_id.email)

                    # send sms
                    if phone_number:
                        self.env["send.sms.on.demand"].send_sms_on_demand(sms_text, phone_number,
                                                                          str(partner.partner_id.name))

                    # send email
                    if email:
                        e_log_obj = self.env['send.email.on.demand.log']

                        email_data = {
                            'mail_text': sms_text,
                            'model': "sale.order",
                            'so_id': order.id,
                            'email': email
                        }

                        email_log_id = e_log_obj.create(email_data)
                        # tmp_data = e_log_obj.browse(cr, uid, email_log_id, context=context)

                        # send email
                        email_template_obj = self.env['mail.template']
                        template_ids = email_template_obj.search([('name', '=', 'SO process mail')])

                        email_template_obj.send_mail(template_ids[0], email_log_id, force_send=True)
            """
            res_user_obj = self.env['res.users']
            for key in comm_heads:
                cat_heads = res_user_obj.browse(cr, uid, [comm_heads[key]], context=context).partner_id

                # sms & email through cat_heads
            """
        except:
            pass

        return True


# Delay Message (Finance) (hold it for as discussed with Salman vai)
"""
Order number "Number" is pending for 2/4 days.
Please Pay PO "Number" to get the products.
"""

# Delay Message (Warehouse)
"""
Order number "number" is pending for 2/4 days
and ready to deliver. Please deliver the order.
"""


class WarehouseHeadMapping(models.Model):
    _name = "warehouse.head.mapping"
    _description = 'Warehouse Head Mapping'

    wh_mapping = fields.Text("Warehouse Head Mapping {'warehouse_id':user_id,'warehouse_id':user_id}")

    # _columns = {
    #     'wh_mapping': fields.text("Warehouse Head Mapping {'warehouse_id':user_id,'warehouse_id':user_id}"),
    # }

    def product_unavailable_order_process_wh(self):
        uid = 1
        try:
            wh_head_obj = self.env['warehouse.head.mapping']
            wh_head = wh_head_obj.browse()
            # mapping_data = json.loads(str(wh_head.wh_mapping))
            mapping_data = eval(str(wh_head.wh_mapping))

            comm_heads = dict()

            date_4 = datetime.datetime.now() - datetime.timedelta(days=4)
            date_2 = datetime.datetime.now() - datetime.timedelta(days=2)

            date_before_4 = date_4.strftime('%Y-%m-%d')
            date_before_2 = date_2.strftime('%Y-%m-%d')

            so_obj = self.env["sale.order"]
            sos = so_obj.search([("date_order", ">", date_before_4), ("date_order", "<", date_before_2),
                                              ("state", "!=", "cancel"), ("state", "!=", "done")])
            # sos = so_obj.browse(so_list)

            users_obj = self.env["res.users"]
            for order in sos:
                wh_id = str(order.warehouse_id.id)
                wh_head_id = mapping_data[wh_id]

                sms_text = "Order number %23{0} is pending for 2/4 days and ready to deliver. Please deliver the order.".format(
                    str(order.client_order_ref))

                partner = users_obj.browse(wh_head_id)

                phone_number = str(partner.partner_id.mobile)
                email = str(partner.partner_id.email)

                # send sms
                if phone_number:
                    self.env["send.sms.on.demand"].send_sms_on_demand(sms_text, phone_number,
                                                                      str(partner.partner_id.name))

                # send email
                if email:
                    e_log_obj = self.env['send.email.on.demand.log']

                    email_data = {
                        'mail_text': sms_text,
                        'model': "sale.order",
                        'so_id': order.id,
                        'email': email
                    }

                    email_log_id = e_log_obj.create(email_data)
                    # tmp_data = e_log_obj.browse(cr, uid, email_log_id, context=context)

                    # send email
                    email_template_obj = self.env['mail.template']
                    template_ids = email_template_obj.search([('name', '=', 'SO process mail')])

                    email_template_obj.send_mail(template_ids[0], email_log_id, force_send=True,)
        except:
            pass

        return True


class SendEmailOnDemandLog(models.Model):
    _name = "send.email.on.demand.log"
    _description = "Send Email on Demand Log"

    mail_text = fields.Text('Email text)')
    model = fields.Char("Model")
    so_id = fields.Integer("Model Obj ID")
    email = fields.Char("Email")


    # ### Block code for v-14
    # _columns = {
    #     'mail_text': fields.text('Email text)'),
    #     'model': fields.char("Model"),
    #     'so_id': fields.integer("Model Obj ID"),
    #     'email': fields.char("Email"),
    # }

#### ai model nai v-14
# class BridgeBackbone(models.Model):
#     _inherit = "bridge.backbone"
#
#     def order_cancel(self, order_id):
#
#         try:
#             so_obj = self.env["sale.order"]
#
#             so = so_obj.browse(order_id)
#             parent = so.partner_id
#             email = ''
#             parent_name = ''
#
#             for i in range(5):
#                 if len(parent.parent_id) == 1:
#                     parent = parent.parent_id
#                 else:
#                     parent_name = parent.name
#                     email = parent.email
#                     # company_id = parent.id
#                     break
#
#             if email:
#                 suborder_date = datetime.datetime.now().strftime('%A, %d-%m-%Y')
#
#                 """
#                 Hello Zeeshan, Your order 1000090122 has been cancelled and a suborder has been created on Tuesday, 21-07-2020 due to modification in your order.  You will shortly receive an email with the new order id
#                 """
#                 cancel_reason = ""
#                 if so.cancel_co_reason:
#                     cancel_reason += str(so.cancel_co_reason).replace("_", " ")
#                 if so.cancel_quotation_reason:
#                     cancel_reason += str(so.cancel_quotation_reason).replace("_", " ")
#
#                 if not cancel_reason:
#                     cancel_reason = "modification in your order"
#
#                 msg_text = "Hello {0}, we are sorry to inform that your order #{1} has been canceled due to {2}.".format(
#                     parent_name, so.client_order_ref, cancel_reason)
#
#                 e_log_obj = self.env['send.email.on.demand.log']
#
#                 email_data = {
#                     'mail_text': msg_text,
#                     'model': "sale.order",
#                     'so_id': so.id,
#                     'email': email
#                 }
#
#                 email_log_id = e_log_obj.create(email_data)
#                 # tmp_data = e_log_obj.browse(cr, uid, email_log_id, context=context)
#
#                 # send email
#                 email_template_obj = self.env['mail.template']
#                 template_ids = email_template_obj.search([('name', '=', 'Order Canceled')])
#
#                 email_template_obj.send_mail(template_ids[0], so.id, force_send=True)
#
#         except:
#             pass
#
#         super(BridgeBackbone, self).order_cancel(order_id)
#
#         return False

class PackingList(models.Model):

    _inherit = 'packing.list'

    def make_confirm(self):
        resp = super(PackingList, self).make_confirm()
        so_env = self.env['sale.order']

        # pack = self.env["packing.list").browse()
        pack = self
        sms_obj = self.env["send.sms.on.demand"]

        for line in pack.packing_line:
            try:
                so = so_env.search([('client_order_ref', '=', str(line.magento_no)),('state', '!=','cancel')])
                # so = so_env.browse(cr, uid, so_id, context=context)
                mobile_numbers = so.partner_shipping_id.mobile
                if not mobile_numbers:
                    mobile_numbers = so.partner_shipping_id.phone

                name = "packing.list." + str(pack.id)

                order = so.client_order_ref

                sms_text = "Your order {0} is being packed and will be delivered within next 24 hours.".format(order)

                try:
                    if mobile_numbers:
                        sms_obj.send_sms_on_demand(sms_text, mobile_numbers, name)

                except:
                    pass

            except:
                pass
        return True
