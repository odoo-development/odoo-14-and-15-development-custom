from odoo import api, fields, models, _
from odoo.osv import osv
from odoo.exceptions import UserError, ValidationError, Warning


class LoadingList(models.Model):
    _name = "loading.master"
    _description = "Loading List"
    _order = "id desc"

    name = fields.Char("Order Reference", required=False, copy=False, readonly=True, default="/",
                       states={"draft": [("readonly", False)], "sent": [("readonly", False)]}, index=True)
    loading_date = fields.Datetime("Date", required=False, readonly=True, index=True, copy=False,
                                   default=fields.Datetime.now)
    date_confirm = fields.Datetime("Confirmation Date", readonly=True, index=True,
                                   help="Date on which Loading is confirmed.", copy=False, )
    vehicle_number = fields.Char("Vehicle Number")
    assign_to = fields.Char("Assign To")
    description = fields.Char("Remarks")
    total_amount = fields.Float("Total loading Amount")
    invoice_scan = fields.Char("Invoice Scan")
    manifest_id = fields.Many2one("wms.manifest.process", "Manifest ID")
    loading_line = fields.One2many("loading.master.line", "loading_id", "Loading List Lines", required=False, )
    loading_products_line = fields.One2many("loading.products.line", "loading_id", "Loading Products Line",
                                            required=False, )
    state = fields.Selection(
        [("pending", "Waiting for Loading"), ("done", "Loaded and Manifested"), ("cancel", "Cancelled")],
        "Status", default='pending', readonly=True, copy=False, help="Gives the status of the Loading Master", )

    def make_confirm(self):
        self.ensure_one()
        if self.state == "done":
            raise Warning(_("This sheet has been confirmed and manifest has been generated."))

        if self.state == "cancel":
            raise Warning(_("This sheet can not be confirmed."))

        ## Generate Manifiest Process
        manifest_id = ""

        inv_lines = []

        for inv_items in self.loading_line:
            tmp = {
                "status": inv_items.status,
                "area": inv_items.area,
                "invoice_id": inv_items.invoice_id.id,
                "phone": inv_items.phone,
                "amount": inv_items.amount,
                "payment_type": inv_items.payment_type,
                "address": inv_items.address,
                "warehouse_id": inv_items.warehouse_id.id,
                "hub_id": inv_items.hub_id.id if inv_items.hub_id else None,
                "hub_invoice": True if inv_items.hub_id else False,
                "magento_no": inv_items.magento_no,
                "customer_name": inv_items.customer_name,
            }

            inv_lines.append([0, False, tmp])

        manifest_vals = {
            "remark": self.description,
            "vehicle_from": 0,
            "vehicle_number": self.vehicle_number,
            "scan": False,
            "courier_name": self.name,
            "courier_wh": self.courier_wh,
            "warehouse_id": self.warehouse_id.id,
            "hub_id": self.hub_id.id if self.hub_id else None,
            "hub_invoice": True if self.hub_id else False,
            "vehicle_to": 0,
            "assigned_to": self.assign_to,
            "picking_line": inv_lines,
        }

        manifest_id = self.env["wms.manifest.process"].create(manifest_vals)

        ## Ends here Manifest Process

        self.write({'state': 'done',
                    'manifest_id': manifest_id.id
                    })

    def make_cancel(self):
        if self.state == "done":
            raise Warning(_("This sheet has been confirmed and manifest has been generated."))

        self.write({'state': 'cancel'})
        for line in self.loading_line:
            self.env.cr.execute("UPDATE account_move SET x_loading_assign=FALSE WHERE id = %s",
                       (line.invoice_id.id,))
            self.env.cr.commit()

    def _check_packing_line_for_same_invoice(self, packing_line, invoice_id):
        same_invoice = False

        for line in packing_line:
            try:
                if line['invoice_id'].id == invoice_id:
                    same_invoice = True
            except Exception as e:
                pass

        return same_invoice

    # def check_invoice_delivered(self, invoice_id):
    #     delivered = False
    #     mov_obj = self.env['account.move'].search([("id", "=", str(invoice_id))])
    #     ## error jonno off rakachi
    #     check_delivered_query = (
    #         "SELECT delivered FROM account_move WHERE id={0}".format(invoice_id)
    #     )
    #     self._cr.execute(check_delivered_query)
    #
    #     for a in self._cr.fetchall():
    #         inv_delivered = a[0]
    #         if inv_delivered == True:
    #             delivered = True
    #         else:
    #             delivered = False
    #
    #     return delivered

    # def check_invoice_return(self, invoice_id):
    #     returned = False
    #
    #     check_return_query = (
    #         "SELECT type FROM account_move WHERE id={0}".format(invoice_id)
    #     )
    #     self._cr.execute(check_return_query)
    #
    #     for a in self._cr.fetchall():
    #         inv_returned = a[0]
    #         if inv_returned == "out_refund":
    #             returned = True
    #         else:
    #             returned = False
    #
    #     return returned

    # def check_invoice_assigned(self, invoice_id):
    #     assigned = False
    #
    #     check_assigned_query = (
    #         "SELECT assigned FROM account_move WHERE id={0}".format(
    #             invoice_id
    #         )
    #     )
    #     self._cr.execute(check_assigned_query)
    #
    #     for a in self._cr.fetchall():
    #         inv_assigned = a[0]
    #         if inv_assigned == True:
    #             assigned = True
    #         else:
    #             assigned = False
    #
    #     return assigned

    @api.onchange("invoice_scan")
    def loading_invoice_scan(self):
        # try:
        if self.invoice_scan:
            outbound_line_list = list()
            try:
                if int(self.invoice_scan):
                    invoice = self.env['account.move'].search([('id', '=', int(self.invoice_scan)),('state','!=','cancel')], limit=1)
            except Exception as e:
                invoice = self.env['account.move'].search([('name', '=', self.invoice_scan),('state','!=','cancel')], limit=1)


            if invoice:
                invoice_id = invoice.id

                if invoice.warehouse_id != self.warehouse_id:
                    self.invoice_scan = ""
                    raise Warning(_("This invoice is not %s, please scan same warehouse invoices." % self.warehouse_id.name))
                elif invoice.delivered:
                    self.invoice_scan = ""
                    raise ValidationError(_("This invoice is already delivered."))
                elif invoice.move_type == 'out_refund':
                    self.invoice_scan = ""
                    raise Warning(_("This invoice is already returned."))
                elif invoice.assigned:
                    self.invoice_scan = ""
                    raise Warning(_("This invoice is already assigned another manifest"))

                if invoice.x_loading_assign:
                    self.invoice_scan=""
                    raise Warning( _('This invoice already assigned another loading sheet'))

                invoice_obj = invoice

                new_picking_line = []

                if self.hub_id:
                    hub_area_list_obj = self.env['hub.area.line'].search([('area_hub_id', '=', self.hub_id.id)])
                    area_list = [hub_area_name.state_id.name for hub_area_name in hub_area_list_obj]
                    delivery_area = invoice.partner_shipping_id.state_id.name
                    if delivery_area not in area_list:
                        raise Warning(
                            _("This invoice is not %s, please scan same hub invoices." % self.hub_id.name))

                if invoice_obj:

                    if self._check_packing_line_for_same_invoice(self.loading_line, invoice_id):
                        self.invoice_scan = ""
                    else:

                        if self.loading_products_line:

                            for items in self.loading_products_line:
                                if not items.id:
                                    outbound_line_list.append((0, 0, {
                                        "invoice_id": items["invoice_id"],
                                        "product_id": items["product_id"],
                                        "loading_qty": items["loading_qty"]}))
                        deli_area = invoice_obj.partner_shipping_id.state_id.name or ""
                        deli_address = invoice_obj.partner_shipping_id.street or ""
                        deli_phone = invoice_obj.partner_shipping_id.mobile or invoice_obj.partner_shipping_id.phone or ""

                        new_picking_line.append((0, 0, {
                            "invoice_id": invoice_id,
                            "invoice_number": invoice_obj.name,
                            # "magento_no": str(invoice_obj.name),
                            "warehouse_id": invoice_obj.warehouse_id.id,
                            "hub_id": self.hub_id.id if self.hub_id.id else None,
                            "hub_invoice": True if self.hub_id.id else False,
                            "magento_no": invoice_obj.ref,
                            "priority_customer": '',
                            # "priority_customer": invoice_obj.x_priority_customer,
                            "customer_name": str(invoice_obj.partner_id.name),
                            "phone": str(deli_phone),
                            "address": str(deli_address),
                            # "payment_type": invoice_obj.payment_reference,
                            "payment_type": str(invoice_obj.narration).replace("Payment Information:-", ""),
                            "amount": float(invoice_obj.amount_total),
                            "status": str(invoice_obj.state),
                            "area": str(deli_area),
                        }))
                        self.loading_line = new_picking_line

                        self.invoice_scan = ""
                else:
                    self.invoice_scan = ""
            else:
                raise Warning(_("This invoice is already canceled."))

    # except Exception as e:
    #     self.invoice_scan = ""

    @api.model
    def create(self, vals):
        outbound_line_list = []

        for inv in vals["loading_line"]:
            invoice_id = inv[2].get("invoice_id")
            # invoice_line_env = self.env["account.move.line"]
            invoice_line_obj = self.env["account.move.line"].search(
                [("move_id", "=", invoice_id),
                 ('exclude_from_invoice_tab', '=', False)]
            )

            invoice = self.env["account.move"].browse(invoice_id)
            if invoice.warehouse_id.id != vals["warehouse_id"]:
                raise Warning(_("This sheet warehouse has not set properly, please check!"))

            for invoice_line in invoice_line_obj:
                prod_name = invoice_line.product_id.name
                if prod_name != "Shipping" and prod_name != "shipping":
                    outbound_line_list.append(
                        [
                            0,
                            False,
                            {
                                "invoice_id": invoice_id,
                                "product_id": invoice_line.product_id.id,
                                "loading_qty": invoice_line.quantity,
                            },
                        ]
                    )

            self.env['account.move'].browse(invoice_id).write({'x_loading_assign': True})
        vals["loading_products_line"] = outbound_line_list

        record = super(LoadingList, self).create(vals)
        loading_number = "LoadSheet-0" + str(record.id)
        record.name = loading_number
        # self.env['account.move'].browse(invoice_id).write({'assigned': True})

        return record

    # @api.model
    def write(self, vals):

        if str(self.state) == str("done"):
            raise Warning(_("This sheet has been confirmed and manifest has been generated."))

        get_deleted_line_ids = []
        get_deleted_invs_ids = []
        if vals.get("loading_line"):
            for items in vals.get("loading_line"):
                if items[0] == 2:
                    get_deleted_line_ids.append(items[1])
                    for sav_items in self.loading_line:
                        if sav_items.id == items[1]:
                            # get_deleted_invs_ids.append(
                            #     [
                            #         0,
                            #         False,
                            #         {
                            #             sav_items.invoice_id.id
                            #         }
                            #     ]
                            # )
                            get_deleted_invs_ids.append(sav_items.invoice_id.id)
                            break

        record = super(LoadingList, self).write(vals)

        if len(get_deleted_invs_ids) > 0:
            self.env.cr.execute(
                "DELETE FROM loading_products_line WHERE invoice_id in %s",
                (tuple(get_deleted_invs_ids),),
            )
            self.env.cr.commit()

            self.env.cr.execute("UPDATE account_move SET x_loading_assign=FALSE WHERE id in %s",
                             (tuple(get_deleted_invs_ids),))
            self.env.cr.commit()

        # return record


class LoadingListLine(models.Model):
    _name = "loading.master.line"
    _description = "Loading Line List"

    loading_id = fields.Many2one("loading.master", "Loading Reference", required=False, ondelete="cascade", index=True,
                                 readonly=True)
    invoice_id = fields.Many2one("account.move", string="Invoice Number")
    invoice_number = fields.Char("Invoice Number")
    magento_no = fields.Char("Order No")
    customer_name = fields.Char("Customer Name")
    phone = fields.Char("Phone")
    address = fields.Char("Address")
    payment_type = fields.Char("Payment Type")
    amount = fields.Float("Amount")
    status = fields.Char("Status")
    area = fields.Char("Area")
    priority_customer = fields.Boolean("Priority Customer")

    ###  Block code for V-14
    # _columns = {
    #     "loading_id": fields.many2one(
    #         "loading.master",
    #         "Loading Reference",
    #         required=False,
    #         ondelete="cascade",
    #         index=True,
    #         readonly=True,
    #     ),
    #     "invoice_id": fields.many2one(
    #         "account.invoice", string="Invoice Number"
    #     ),
    #     "invoice_number": fields.char("Invoice Number"),
    #     "magento_no": fields.char("Order No"),
    #     "customer_name": fields.char("Customer Name"),
    #     "phone": fields.char("Phone"),
    #     "address": fields.char("Address"),
    #     "payment_type": fields.char("Payment Type"),
    #     "amount": fields.float("Amount"),
    #     "status": fields.char("Status"),
    #     "area": fields.char("Area"),
    #     "priority_customer": fields.boolean("Priority Customer"),
    # }


class LoadingProductsLine(models.Model):
    _name = "loading.products.line"
    _description = "Loading Products Line"

    load_prod_id = fields.Many2one("loading.products", "Loading products", required=False, ondelete="cascade",
                                   index=True, readonly=True, )
    loading_id = fields.Many2one("loading.master", "Loading ID", required=False, ondelete="cascade", index=True,
                                 readonly=True)
    invoice_id = fields.Many2one("account.move", "Invoice Number", readonly=True)
    product_id = fields.Many2one("product.product", "Product", readonly=True)
    product = fields.Char("Product Name", readonly=True)
    product_ean = fields.Char("Product EAN", readonly=True)
    loading_qty = fields.Float("Qty")
    state = fields.Selection([("pending", "Waiting for Loading"), ("done", "Done"), ("cancel", "Cancelled")], "Status",
                             readonly=True, copy=False, help="Gives the status of the quotation or sales order",
                             index=True)

    ### Block code for V-14
    # _columns = {
    #     "load_prod_id": fields.many2one(
    #         "loading.products",
    #         "Loading products",
    #         required=False,
    #         ondelete="cascade",
    #         index=True,
    #         readonly=True,
    #     ),
    #     "loading_id": fields.many2one(
    #         "loading.master",
    #         "Loading ID",
    #         required=False,
    #         ondelete="cascade",
    #         index=True,
    #         readonly=True,
    #     ),
    #     "invoice_id": fields.many2one(
    #         "account.invoice", "Invoice Number", readonly=True
    #     ),
    #     "product_id": fields.many2one(
    #         "product.product", "Product", readonly=True
    #     ),
    #     "product": fields.char("Product Name", readonly=True),
    #     "product_ean": fields.char("Product EAN", readonly=True),
    #     "loading_qty": fields.float("Qty"),
    #     "state": fields.selection(
    #         [
    #             ("pending", "Waiting for Loading"),
    #             ("done", "Done"),
    #             ("cancel", "Cancelled"),
    #         ],
    #         "Status",
    #         readonly=True,
    #         copy=False,
    #         help="Gives the status of the quotation or sales order",
    #         index=True,
    #     ),
    # }
