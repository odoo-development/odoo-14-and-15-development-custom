from odoo import api, fields, models, _


class AccountMove(models.Model):
    _inherit = 'account.move'

    x_loading_assign = fields.Boolean("Assigned to Loading")
