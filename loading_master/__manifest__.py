
{
    "name": "Loading Master Generation Process",
    "version": "1.0.0",
    "category": "Warehouse (WMS)",
    "description": """

""",
    "author": "Odoo Bangladesh",
    "depends": ["account",
                'wms_menulist',
                'wms_manifest'],
    "data": [
        "security/loading_master_security.xml",
        "security/ir.model.access.csv",
        "report/report_loading.xml",
        "report/report_menu.xml",
        "views/loading_view.xml",
        "views/loading_menu.xml",
    ],
    "installable": True,
    "auto_install": False,
}
