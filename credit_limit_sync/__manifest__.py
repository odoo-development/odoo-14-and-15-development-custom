{
    'name': 'Credit Limit Sync',
    'version': '14.0',
    'category': 'Credit Limit Sync',
    'author': 'SIndabad',
    'summary': 'Credit Limit Sync with Magento',
    'description': 'Credit Limit Sync with Magento',
    'depends': ['sale', 'stock', 'odoo_to_magento_api_connect', 'order_approval_process'],
    'data': [
        'views/outstanding_sync_from_so.xml',
        # 'outstanding_sync_scheduler.xml',
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}
