from odoo import api, fields, models, _
from ...odoo_to_magento_api_connect.api_connect import get_magento_token, submit_request
import json
import requests
import datetime
from datetime import date
# from ...customer_statement.report.partner_statement import PartnerStatement


class ResPartner(models.Model):
    _inherit = "res.partner"

    other_outstanding = fields.Float(string='Other Outstanding', default=0)

    def statement_balance(self, ids):
        res = 0
        for partner in ids:
            res = partner.debit - partner.credit
        return res

    def company_pending_order_amount(self,odoo_company_id):
        company_child_id_list = self.compary_child(odoo_company_id)
        pending_order_query = "Select sum(sale_order.amount_total) from sale_order where id not in (Select id from sale_order where name in (select invoice_origin from account_move) and sale_order.state!= 'cancel') and sale_order.partner_id in %s and sale_order.state!= 'cancel' group by partner_id"
        self.env.cr.execute(pending_order_query, (tuple(company_child_id_list),))
        company_pending_order_amount_list = [res[0] for res in self.env.cr.fetchall()]
        if company_pending_order_amount_list:

            company_pending_order_total_amount = sum(company_pending_order_amount_list)
            if company_pending_order_total_amount >0 :
                company_pending_order_total_amount = company_pending_order_total_amount *(-1)
        else:
            company_pending_order_total_amount = 0
        return company_pending_order_total_amount

    def compary_child(self,ids):
        odoo_company_id = ids

        company_child_query = "SELECT res_partner.id FROM res_partner JOIN res_partner parent ON res_partner.parent_id = parent.id WHERE res_partner.active= TRUE and res_partner.parent_id =%s"
        self.env.cr.execute(company_child_query, ([odoo_company_id]))
        company_child_id_list = [res[0] for res in self.env.cr.fetchall()]
        company_child_id_list.append(odoo_company_id)
        return company_child_id_list

    def company_statement_balance(self, ids):
        odoo_company_id = ids
        balance = 0
        company_child_id_list = self.compary_child(odoo_company_id)
        companies_list = self.search([('id', 'in', company_child_id_list), ('active', '=', True)])
        if company_child_id_list:
            for company_list in companies_list:
                outstanding_balance = round((float(company_list.debit - company_list.credit)),2)
                balance += outstanding_balance

        return balance

    def company_outstanding_balance_undelivered(self,ids):
        odoo_company_id = ids

        company_child_id_list = self.compary_child(odoo_company_id)

        invoices = self.env['account.move'].search([('commercial_partner_id', 'in', company_child_id_list),
                                                    ('move_type', '=', 'out_invoice'),
                                                    ('delivered', '=', True), ('state', '=', 'posted'),
                                                    ('payment_state', 'in', ['not_paid', 'partial'])])
        due = 0
        for inv in invoices:
            due = due + inv.amount_residual

        return round((float(due)),2)

    def company_sales_value(self,compnay_id):

        company_sales_value_query = "SELECT sum(amount_total) as total,state FROM sale_order WHERE (partner_id in (SELECT res_partner.id FROM res_partner JOIN res_partner parent ON res_partner.parent_id = parent.id WHERE res_partner.active=TRUE and res_partner.parent_id =%s) or partner_id = %s) and state!='cancel' GROUP BY state"

        self.env.cr.execute(company_sales_value_query, ([compnay_id,compnay_id]))
        objects = self.env.cr.dictfetchall()

        confirm_sales_value = 0
        pending_sales_value = 0

        for val in objects:
            if val['state'] in ['sale','done']:
                confirm_sales_value += val['total']
            else:
                pending_sales_value += val['total']

        data = {
            'confirm_sales_value': confirm_sales_value,
            'pending_sales_value': pending_sales_value
        }
        return data

    def company_invoices_value(self, compnay_id):

        company_invoices_value_query = "SELECT count(*) as count,sum(amount_total) as total,sum(amount_residual) as due,move_type,payment_state,delivered FROM account_move WHERE (commercial_partner_id in (SELECT res_partner.id FROM res_partner JOIN res_partner parent ON res_partner.parent_id = parent.id WHERE res_partner.parent_id =%s) or commercial_partner_id = %s) and move_type in ('out_invoice', 'out_refund') and state!='cancel' GROUP BY move_type,payment_state,delivered"

        self.env.cr.execute(company_invoices_value_query, ([compnay_id,compnay_id]))
        objects = self.env.cr.dictfetchall()

        invoice_value = 0
        invoice_count = 0
        refund_value = 0
        refund_count = 0
        delivered_paid_invoice_count = 0
        delivered_paid_invoice_value = 0
        delivered_unpaid_invoice_value = 0
        delivered_unpaid_invoice_count = 0
        nondelivered_unpaid_invoice_count = 0
        nondelivered_unpaid_invoice_value = 0

        for val in objects:
            if val['delivered'] is True:
                if val['move_type'] == 'out_invoice':
                    if val['payment_state'] in ['not_paid','partial']:
                        delivered_unpaid_invoice_value = delivered_unpaid_invoice_value + val['due']
                        delivered_unpaid_invoice_count = delivered_unpaid_invoice_count + val['count']
                    else:
                        delivered_paid_invoice_value = delivered_paid_invoice_value + val['total']
                        delivered_paid_invoice_count = delivered_paid_invoice_count + val['count']

                    invoice_value = invoice_value + val['total']
                    invoice_count = invoice_count + val['count']

                else:
                    refund_value = refund_value + val['total']
                    refund_count = refund_count + val['count']
            else:
                if val['move_type'] == 'out_invoice':
                    if val['payment_state'] in ['not_paid','partial']:
                        nondelivered_unpaid_invoice_value = nondelivered_unpaid_invoice_value + val['due']
                        nondelivered_unpaid_invoice_count = nondelivered_unpaid_invoice_count + val['count']
                    else:
                        delivered_paid_invoice_value = delivered_paid_invoice_value + val['total']
                        delivered_unpaid_invoice_count = delivered_unpaid_invoice_count + val['count']

                    invoice_value = invoice_value + val['total']
                    invoice_count = invoice_count + val['count']
                else:
                    refund_value = refund_value + val['total']
                    refund_count = refund_count + val['count']

        data = {
            'invoice_value': invoice_value,
            'invoice_count': invoice_count,
            'refund_value': refund_value,
            'refund_count': refund_count,
            'delivered_unpaid_invoice_value': delivered_unpaid_invoice_value,
            'delivered_unpaid_invoice_count': delivered_unpaid_invoice_count,
            'delivered_paid_invoice_value': delivered_paid_invoice_value,
            'delivered_paid_invoice_count': delivered_paid_invoice_count,
            'nondelivered_unpaid_invoice_value': nondelivered_unpaid_invoice_value,
            'nondelivered_unpaid_invoice_count': nondelivered_unpaid_invoice_count,
        }
        return data

    def outstanding_sync(self, partner_id, token, url_root):

        rs_p_obj = self.env['res.partner']
        parent_id = partner_id

        list_of_company_dicts = []

        for partner in parent_id:

            try:
                monthly_limit= False
                partner_exceed_limit=True
                if partner.x_credit_status:
                    monthly_limit=partner.monthly_limit
                    partner_exceed_limit=partner.x_exceed_limit
                    balance = float(self.statement_balance(partner)) + float(self.company_pending_order_amount(partner.id))

                    map_company_data ={
                        "company_id":partner.magento_company_id,
                        "company_credit":{
                            "credit_limit": partner.credit_limit,
                            # "total_balance": self.company_statement_balance(partner.id),
                            # "balance": self.company_outstanding_balance_undelivered(partner.id),
                            "balance": balance,
                            "credit_days": partner.credit_days,
                            "monthly_limit": monthly_limit,
                            "exceed_limit": partner_exceed_limit
                        }
                    }

                    if partner.magento_company_id != 0:
                        list_of_company_dicts.append(map_company_data)
            except:
                pass
        
        magento_data = {"companyCredits": list_of_company_dicts}
        data = json.dumps(magento_data)
        try:
            if token:
                token = token.replace('"', "")
                headers = {'Authorization': token, 'Content-Type': 'application/json'}

                post_url = url_root + "/index.php/rest/V1/odoomagentoconnect/companyCreditBulk"

                post_resp = requests.post(post_url, data=data, headers=headers)
        except:
            pass

        return True

    def action_partner_sync_credit_and_outstanding(self):


        ids = self.id
        # try:
        #     with open('/var/log/odoo/cron-run-time.log', 'a+') as f:
        #         right_now = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        #         f.write("Outstanding Sync Scheduler: Start time: " + right_now + "\n")
        # except:
        #     pass

        so_obj = self.env['sale.order']
        sale_order_id = so_obj.search([('client_order_ref', '!=', None)], limit=3)
        sale_order_ids = sale_order_id.mapped('id')

        token, url_root = get_magento_token(self,sale_order_ids)

        rs_p_obj = self.env['res.partner']

        companies_list = list()
        # if ids is None:

        # companies_list = self.search(['&', ('is_company', '=', True), ('parent_id', '=', None),
        #                                                ('credit_limit', '>', 0), ('magento_company_id', '!=', 0)])

        companies_list = self.search(['&', ('is_company', '=', True), ('parent_id', '=', None),
                                      ('x_credit_status', '=', True), ('magento_company_id', '!=', 0)])
        if ids:
            companies_list = self

        # for partner_id in companies_list:
        #
        #     # rs_p_obj.credit_limit_sync(cr, uid, [partner_id], token, url_root, vals=None, context=context)

        rs_p_obj.outstanding_sync(companies_list, token, url_root)

        # try:
        #     with open('/var/log/odoo/cron-run-time.log', 'a+') as f:
        #         right_now = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        #         f.write("Outstanding Sync Scheduler: End time: " + right_now + "\n")
        # except:
        #     pass

        return True

    @api.model
    def cmp_company_invoice_details(self, vals):
        odoo_company_id = vals.get('odoo_id')
        # companies_list = self.search([('is_company', '=', True), ('id', '=', odoo_company_id),])
        company_child_query = "SELECT res_partner.id FROM res_partner JOIN res_partner parent ON res_partner.parent_id = parent.id WHERE res_partner.active= TRUE and res_partner.parent_id =%s"
        self.env.cr.execute(company_child_query, ([odoo_company_id]))
        company_child_id_list = [res[0] for res in self.env.cr.fetchall()]
        company_child_id_list.append(odoo_company_id)

        data = []
        if company_child_id_list:
            invoices = self.env['account.move'].search([('commercial_partner_id','in',company_child_id_list),
                                                        ('move_type','=','out_invoice'),
                                                        ('delivered','=',True),('state','=','posted'),
                                                        ('payment_state','in',['not_paid','partial'])])

            for inv in invoices:
                data.append({
                    'type': 'success',
                    'inv_number': inv.name,
                    'inv_id': inv.id,
                    'order_number': inv.ref,
                    'invoice_date': inv.invoice_date,
                    'delivery_date': inv.delivery_date,
                    'amount_residual': inv.amount_residual,
                    'amount_total': inv.amount_total,
                    'payment_method': str(inv.narration).replace("Payment Information:- ", ""),
                })

        else:
            data.append({
                'type': 'error',
                'msg': 'Company not found'
            })

        return data

    @api.model
    def cmp_company_statement_balance(self, vals):
        odoo_company_id = vals.get('odoo_id')
        company_pending_order_amount = 0
        outstanding_balance = 0
        balance = 0

        # company_child_query = "SELECT res_partner.id FROM res_partner JOIN res_partner parent ON res_partner.parent_id = parent.id WHERE res_partner.active= TRUE and res_partner.parent_id =%s"
        # self.env.cr.execute(company_child_query, ([odoo_company_id]))
        # company_child_id_list = [res[0] for res in self.env.cr.fetchall()]
        # company_child_id_list.append(odoo_company_id)
        if odoo_company_id:
            companies_list = self.search([('is_company', '=', True),('active', '=', True), ('id', '=', odoo_company_id)])
            for company_list in companies_list:
                outstanding_balance = float(self.statement_balance([company_list]))
                company_pending_order_amount = float(self.company_pending_order_amount(company_list.id))

                balance = outstanding_balance + company_pending_order_amount
            data = {
                'type': 'success',
                'odoo_company_id': odoo_company_id,
                'outstanding_balance': balance,
                'account_receivable': outstanding_balance,
                'company_pending_order_amount': company_pending_order_amount,
            }
        else:
            data ={
                'type': 'error',
                'msg': 'Company not found'
            }
        return data

    @api.model
    def cmp_company_summary(self, vals):
        odoo_company_id = vals.get('odoo_id')
        companies_list = self.search([('is_company', '=', True), ('id', '=', odoo_company_id),('active','=',True)])
        data ={}
        if companies_list:
            registration_date =  companies_list.create_date + datetime.timedelta(hours=6)

            data.update({'registration':registration_date.date().strftime("%d-%m-%Y"),
                         'sales_representative_name':companies_list.sales_representative_name,
                         'sales_representative_email' :companies_list.sales_representative_email ,
                         'sales_representative_mobile': companies_list.sales_representative_mobile})
            data.update(self.company_sales_value(odoo_company_id))
            data.update(self.company_invoices_value(odoo_company_id))

        else:
            data ={
                'type': 'error',
                'msg': 'Company not found'
            }
        return data

    @api.model
    def cmp_credit_company_value(self):
        company_child_query = "SELECT count(company_code),sum(credit_limit) FROM res_partner WHERE active= TRUE and x_credit_status = TRUE and is_company = TRUE"
        self.env.cr.execute(company_child_query)
        data = self.env.cr.fetchone()
        if data:
            total_credit_company = data[0]
            total_credit_value = data[1]
        else:
            total_credit_company = 0
            total_credit_value = 0
        result = {
            'total_credit_company':total_credit_company,
            'total_credit_value':total_credit_value
        }
        return result

    # @api.model
    # def cmp_credit_company_value(self):
    #     company_child_query = "SELECT sum(amount_due) FROM res_partner WHERE active= TRUE and x_credit_status = TRUE and is_company = TRUE"
    #     self.env.cr.execute(company_child_query)
    #     data = self.env.cr.fetchone()
    #     if data:
    #         total_credit_company = data[0]
    #         total_credit_value = data[1]
    #     else:
    #         total_credit_company = 0
    #         total_credit_value = 0
    #
    #     result = {
    #         'total_credit_company':total_credit_company,
    #         'total_credit_value':total_credit_value
    #     }
    #     return result
    #
    # @api.model
    # def cmp_credit_company_value(self):
    #
    #     company_child_query = "SELECT count(company_code),sum(credit_limit) FROM res_partner WHERE active= TRUE and x_credit_status = TRUE and is_company = TRUE"
    #     self.env.cr.execute(company_child_query)
    #     data = self.env.cr.fetchone()
    #     if data:
    #         total_credit_company = data[0]
    #         total_credit_value = data[1]
    #     else:
    #         total_credit_company = 0
    #         total_credit_value = 0
    #     result = {
    #         'total_credit_company': total_credit_company,
    #         'total_credit_value': total_credit_value
    #     }
    #     return result

    @api.model
    def cmp_company_due_value(self):
        cod = 0
        sin_credit = 0
        card = 0
        paid = 0
        cheque = 0
        other = 0

        company_child_query = "SELECT sum(amount_residual) as total,narration FROM account_move WHERE delivered=True and move_type = 'out_invoice' and payment_state in ('not_paid','partial') and state!='cancel' GROUP BY narration"
        self.env.cr.execute(company_child_query)
        data = self.env.cr.dictfetchall()
        for row in data:
            payment_method =str(row['narration']).replace("Payment Information:- ", "").split("/", 1)[0]

            if 'Cash On Delivery' in payment_method:
                cod = cod + row['total']
            elif 'Sindabad Credit' in payment_method:
                sin_credit = sin_credit + row['total']
            elif 'Credit/Debit Cards' in payment_method:
                card = card + row['total']
            elif 'PAID' in payment_method.upper():
                paid = paid + row['total']
            elif 'Cheque' in payment_method:
                cheque = cheque + row['total']
            else:
                other = other + row['total']

        result = {
            'cash_on_delivery': cod,
            'sin_credit': sin_credit,
            'credit_debit_cards': card,
            'cheque': cheque,
            'paid': paid,
            'other': other
        }
        return result