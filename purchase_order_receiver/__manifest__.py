{
    'name': 'Purchase Order Receiver Name',
    "version": "14.0.1.0.0",
    "category": "Purchase",
    "author": "Odoo Bangladesh",
    "summary": "Purchase",
    "description": "Product receiver name fix",
    'depends': ['purchase', 'stock', 'stock_location_in_moveline', ],
    # 'depends': ['purchase', 'stock', 'purchase_order_line_state', 'stock_location_in_moveline', ],
    "installable": True,
    "application": True,
    "auto_install": False,
}
