from odoo import api, fields, models, _
import requests



class SMSMailServer(models.Model):
    _name = "sms.mail.server"
    _description = "SMS Mail Server"

    @api.model
    def get_reference_type(self):
        return ['SSL Wireless', 'Route Mobile']

    @api.model
    def _get_mob_no(self):
        user_obj = self.env['res.users'].browse([self._uid])
        return user_obj.mob_number

    @api.model
    def _set_mob_no(self):
        user_obj = self.env['res.users'].browse([self._uid])
        user_obj.write({'mob_number': self.user_mobile_no})

    description = fields.Char(string="Description", required=True)

    sequence = fields.Integer(string='Priority', help="Default Priority will be 0.")
    sms_debug = fields.Boolean(string="Debugging",
                               help="If enabled, the error message of sms gateway will be written to the log file")
    user_mobile_no = fields.Char(string="Mobile No.", help="Eleven digit mobile number with country code(e.g +88)")
    # gateway = fields.Selection('get_reference_type')
    gateway = fields.Char(string="Gateway", required=True)

    ssl_url = fields.Char(string="URL", widget="url", required=True)
    ssl_user_name = fields.Char(string="User Name", required=True)
    ssl_password = fields.Char(string="Password", required=True)
    ssl_sid = fields.Selection([("Sindabad", "Sindabad"), ("Kiksha", "Kiksha")],
                               string="Sender Id", required=True)

    def test_conn_ssl(self):
        self.ensure_one()
        user_obj = self.env['res.users'].browse(self._uid)
        mobile_number = self.user_mobile_no

        mob_numbers_list = mobile_number.split(",")
        sms_text = "Odoo 14: SMS Test Connection Successful....!!!"
        data = {
            "user": self.ssl_user_name,
            "pass": self.ssl_password,
            "sid": self.ssl_sid,
        }


        try:

            send_urls = self.ssl_url
            dlr = 1
            smstype = 0
            mob_no = ''
            source = 'Sindabad'
            for mob_no in mob_numbers_list:

                if len(mob_no) >= 13:
                    mob_no = mob_no
                else:
                    tmp_mob = mob_no[-11:]
                    mob_no = str('+88') + str(tmp_mob)

                request_url = '{0}?username={1}&password={2}&type={3}&dlr={4}&destination={5}&source={6}&message={7}'.format(
                    send_urls, self.ssl_user_name, self.ssl_password, smstype, dlr, mob_no, source, sms_text)

                resp = requests.post(request_url)
                response_content = resp._content.decode('utf-8')

        except Exception as e:
            print(e)

        return True


class SmsApi(models.AbstractModel):
    _inherit = 'sms.api'

    """
    @api.model
    def _send_sms(self, numbers, message):

        response = self.custom_send_sms_api(numbers, message)
        status = response.status_code

        sms_response = {
                        'res_id': message.get('res_id'),
                        'state': 'success' if status == 200 else 'server_error',
                        'credit': 1
                        }

        return sms_response
    """

    @api.model
    def _send_sms_batch(self, messages):
        """ Send SMS using IAP in batch mode

        :param messages: list of SMS to send, structured as dict [{
            'res_id':  integer: ID of sms.sms,
            'number':  string: E164 formatted phone number,
            'content': string: content to send
        }]

        :return: return of /iap/sms/1/send controller which is a list of dict [{
            'res_id': integer: ID of sms.sms,
            'state':  string: 'insufficient_credit' or 'wrong_number_format' or 'success',
            'credit': integer: number of credits spent to send this SMS,
        }]

        :raises: normally none
        """

        response_list = list()

        for message in messages:
            number = str('+88') + str(message.get('number'))[-11:]
            sms_text = message.get('content')

            response = self.send_sms_by_ssl_api(number, sms_text)
            status = response.status_code

            response_list.append({
                'res_id': message.get('res_id'),
                'state': 'success' if status == 200 else 'server_error',
                'credit': 1
            })

        return response_list

    def send_sms_by_ssl_api(self, numbers, message):

        # sms_server = self.env['sms.mail.server'].browse(1)
        sms_server = self.env["sms.mail.server"].sudo().search([("gateway", "=", "SSL Wireless")])

        send_urls = sms_server.ssl_url
        user_name = sms_server.ssl_user_name
        password = sms_server.ssl_password
        dlr = 1
        smstype = 0
        source = 'Sindabad'
        try:
            number = numbers.split(",")
            for mob_no in number:
                mob_no = mob_no.replace("-","") if mob_no is not False else mob_no
                mob_no = mob_no.replace(" ", "") if mob_no is not False else mob_no

                if len(mob_no) >= 13:
                    mob_no = mob_no
                else:
                    tmp_mob = mob_no[-11:]
                    mob_no = str('+88') + str(tmp_mob)

                request_url = '{0}?username={1}&password={2}&type={3}&dlr={4}&destination={5}&source={6}&message={7}'.format(
                    send_urls, user_name,password, smstype, dlr, mob_no, source, message)

                resp = requests.post(request_url)
                response_content = resp._content.decode('utf-8')

        except Exception as e:
            pass
        return True

