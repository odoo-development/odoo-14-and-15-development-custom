
{
    'name': 'SMS gateway custom',
    'version': '14.0',
    'category': 'Tools',
    'summary': 'SMS Gateway Configuration',
    'author': 'Sindabad',
    'description': """
    
    This module helps to configure custom SMS gateway
    
    """,
    'depends': ['base_setup', 'mail', 'sms'],
    'data': [
        # 'security/sms_mail_security.xml',
        'views/sms_mail_server.xml',
        'security/ir.model.access.csv',
    ],
    'installable': True,
    'auto_install': True,
}
