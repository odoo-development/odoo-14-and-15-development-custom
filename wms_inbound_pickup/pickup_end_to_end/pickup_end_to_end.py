
from odoo import api, fields, models, _
from odoo.exceptions import ValidationError
from odoo.osv import osv


class InbEndToEnd(models.Model):
    _inherit = "inb.end.to.end"

    pickup_id = fields.Char('Pickup IDs')
    pickup_number = fields.Char('Pickup Number')
    pickup_request_date_create = fields.Text('Pickup Requst Created Date')
    pickup_request_date_confirm = fields.Text('Pickup Request Confirm Date')
    manifest_id = fields.Char('Pickup IDs')
    manifest_number = fields.Char('Manifest Number')
    manifest_date_create = fields.Text('Manifest Created Date')
    manifest_date_confirm = fields.Text('Manifest Confirm Date')
    manifest_date_dispatch = fields.Text('Manifest Dispatch Date')

    # ## Block code for V-14
    # _columns = {
    #     'pickup_id': fields.char('Pickup IDs'),
    #     'pickup_number': fields.char('Pickup Number'),
    #     'pickup_request_date_create': fields.text('Pickup Requst Created Date'),
    #     'pickup_request_date_confirm': fields.text('Pickup Request Confirm Date'),
    #     'manifest_id': fields.char('Pickup IDs'),
    #     'manifest_number': fields.char('Manifest Number'),
    #     'manifest_date_create': fields.text('Manifest Created Date'),
    #     'manifest_date_confirm': fields.text('Manifest Confirm Date'),
    #     'manifest_date_dispatch': fields.text('Manifest Dispatch Date')
    # }


    def pickup_data_create(self, pickup):
        po_count = self.search_count([('po_number', '=', pickup.po_number), ('po_id', '=', pickup.po_id)])

        if int(po_count) == 0:
            created_id = self.create({
                'po_number': pickup.po_number,
                'po_id': pickup.po_id,
                'pickup_id': pickup.id,
                'pickup_number': pickup.name,
                'pickup_request_date_create': pickup.create_date.strftime("%Y-%m-%d"),
            })
        else:
            inb_etoe_id = self.search([('po_number', '=', pickup.po_number), ('po_id', '=', pickup.po_id)], limit=1)
            inb_etoe_id.write({
                'pickup_id': str(pickup.id) if str(inb_etoe_id.pickup_id) == 'False' else (str(inb_etoe_id.pickup_id) + "," + str(pickup.id)),
                'pickup_number': str(pickup.name) if str(inb_etoe_id.pickup_number) == 'False' else str(inb_etoe_id.pickup_number) + "," + str(pickup.name),
                'pickup_request_date_create': str(pickup.create_date) if str(
                    inb_etoe_id.pickup_request_date_create) == 'False' else str(inb_etoe_id.pickup_request_date_create) + "," +
                                                                            pickup.create_date.strftime("%Y-%m-%d"),
            })

        return True


    def pickup_request_confirm_update(self, po_number, po_id):
        # browse and then update
        for etoe in self.search([('po_number', '=', po_number), ('po_id', '=', po_id)]):
            etoe.write({'pickup_request_date_confirm': str(fields.Datetime.now()) if etoe.pickup_request_date_confirm == False else str(etoe.pickup_request_date_confirm) + "," + str(fields.Datetime.now())})

    def manifest_create(self, po_number, po_id):

        # inb_etoe_id = self.search([('po_number', '=', self.po_number), ('po_id', '=', self.po_id)])

        # browse and then update
        for etoe in self.search([('po_number', '=', po_number), ('po_id', '=', po_id)]):
            etoe.write({
                'manifest_id': str(etoe.manifest_id) if str(etoe.manifest_id) == 'False' else str(etoe.manifest_id) + "," + str(self.manifest_id),
                'manifest_number': str(etoe.manifest_number) if str(etoe.manifest_number) == 'False' else str(etoe.manifest_number) + "," + str(self.manifest_number),
                'manifest_date_create': str(fields.Datetime.now()) if str(etoe.manifest_date_create) == 'False' else str(etoe.manifest_date_create) + "," + str(fields.Datetime.now()),
            })

        return True

    def manifest_confirm_update(self):

        inb_etoe_id = self.search([('po_number', '=', self.po_number), ('po_id', '=', self.po_id)])

        # browse and then update
        for etoe in self.browse(inb_etoe_id):
            self.write(inb_etoe_id, {
                'manifest_date_confirm': str(self.manifest_date_confirm) if str(etoe.manifest_date_confirm) == 'False' else str(etoe.manifest_date_confirm) + "," + str(self.manifest_date_confirm),
            })

        return True

    def manifest_dispatch_update(self):

        inb_etoe_id = self.search([('po_number', '=', self.po_number), ('po_id', '=', self.po_id)])

        # browse and then update
        for etoe in self.browse(inb_etoe_id):
            self.write(inb_etoe_id, {
                'manifest_date_dispatch': str(self.manifest_date_dispatch) if str(etoe.manifest_date_dispatch) == 'False' else str(etoe.manifest_date_dispatch) + "," + str(self.manifest_date_dispatch),
            })

        return True