
from odoo import api, fields, models, _


class PurchaseOrder(models.Model):
    _inherit = 'purchase.order'

    pickup_request_line = fields.One2many(comodel_name='pickup.request.line', inverse_name='po_id', string='Pickup Request')

    reattempt_number = fields.Integer(string="Re-attempt Count")
    vendor_pickup = fields.Selection([('yes', 'Yes'), ('no', 'No')], 'Vendor Pickup', default='no')

    # ## Block code for V-14
    # _columns = {
    #     'pickup_request_line': fields.one2many('pickup.request.line', 'po_id', 'Pickup Request', required=False),
    #
    #     'reattempt_number': fields.integer(string="Re-attempt Count"),
    #
    #     'vendor_pickup': fields.selection([
    #         ('yes', 'Yes'),
    #         ('no', 'No')
    #     ], 'Vendor Pickup'),
    # }
    #
    # _defaults = {
    #     'vendor_pickup': 'no',
    # }

#### ai clas close rakachi 'sgeede_b2b' module a depends
# class BackToBackOrder(models.Model):
#     _inherit = "back.to.back.order"
#
#     vendor_pickup = fields.Selection([('yes', 'Yes'), ('no', 'No')], 'Vendor Pickup', default='no')

    # Block code for V-14
    # _columns = {
    #     'vendor_pickup': fields.selection([
    #         ('yes', 'Yes'),
    #         ('no', 'No')
    #     ], 'Vendor Pickup'),
    # }
    #
    # _defaults = {
    #     'vendor_pickup': 'no',
    # }

