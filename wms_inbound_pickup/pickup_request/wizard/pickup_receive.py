from odoo.exceptions import UserError, ValidationError, Warning
from odoo.osv import osv
import datetime
from odoo import api, fields, models, _


class PickupSuccess(models.Model):
    _name = "pickup.success"
    _description = 'Pickup Success'

    warehouse_id = fields.Many2one('stock.warehouse', string='Warehouse Location')
    pickup_name = fields.Char('Pickup Number')
    pickup_req_id = fields.Char('Pickup Request ID')
    po_number = fields.Char('PO Number')
    po_id = fields.Integer('Purchase ID')
    pickup_manifest_id = fields.Integer('Pickup Manifest ID')
    pickup_manifest_line_id = fields.Integer('Pickup Manifest Line ID')
    partner_id = fields.Many2one('res.partner', 'Vendor', change_default=True, tracking=True)
    partner_address = fields.Char('Vendor Address')
    area = fields.Char('Area')
    npr = fields.Selection([
        ("Vendor was unreachable", "Vendor was unreachable"),
        ("Vendor refused to provide", "Vendor refused to provide"),
        ("Unable to reach", "Unable to reach"),
        ("Vendors place was closed", "Vendors place was closed"),
        ("Cash problem", "Cash problem"),
        ("Vendor rescheduled", "Vendor rescheduled"),
        ("Wrong address given", "Wrong address given"),
        ("Unable to reach on Time", "Unable to reach on Time"),
        ("Quality issue", "Quality issue"),
        ("Due to traffic issue", "Due to traffic issue"),
        ("Invoice/Cash Memo not provided", "Invoice/Cash Memo not provided")
    ], 'No Pick UP Reason', copy=False, help="Reason", index=True)

    remark = fields.Char('Remark')
    date_pickup = fields.Date('Pick Up Date')
    pickup_product_line = fields.One2many('pickup.product.line', 'pickup_product_line_id', 'Pickup Product Line')


    # ## Block code for V-14
    # _columns = {
    #     'warehouse_id': fields.many2one('stock.warehouse', string='Warehouse Location'),
    #     'pickup_name': fields.char('Pickup Number'),
    #     'pickup_req_id': fields.char('Pickup Request ID'),
    #     'po_number': fields.char('PO Number'),
    #     'po_id': fields.integer('Purchase ID'),
    #     'pickup_manifest_id': fields.integer('Pickup Manifest ID'),
    #     'pickup_manifest_line_id': fields.integer('Pickup Manifest Line ID'),
    #     'partner_id': fields.many2one('res.partner', 'Vendor',
    #                                   change_default=True, track_visibility='always'),
    #     'partner_address': fields.char('Vendor Address'),
    #     'area': fields.char('Area'),
    #     # 'npr': fields.text('No Pick UP Reason'),
    #     'npr': fields.selection([
    #
    #         ("Vendor was unreachable", "Vendor was unreachable"),
    #         ("Vendor refused to provide", "Vendor refused to provide"),
    #         ("Unable to reach", "Unable to reach"),
    #         ("Vendors place was closed", "Vendors place was closed"),
    #         ("Cash problem", "Cash problem"),
    #         ("Vendor rescheduled", "Vendor rescheduled"),
    #         ("Wrong address given", "Wrong address given"),
    #         ("Unable to reach on Time", "Unable to reach on Time"),
    #         ("Quality issue", "Quality issue"),
    #         ("Due to traffic issue", "Due to traffic issue"),
    #         ("Invoice/Cash Memo not provided", "Invoice/Cash Memo not provided")
    #
    #     ], 'No Pick UP Reason', copy=False, help="Reason", index=True),
    #
    #     'remark': fields.char('Remark'),
    #     'date_pickup': fields.date('Pick Up Date'),
    #     'pickup_product_line': fields.one2many('pickup.product.line', 'pickup_product_line_id', 'Pickup Product Line'),
    # }
    def default_get(self, fields):
        res = super(PickupSuccess, self).default_get(fields)
        manifest_pickup = self.env['pickup.manifest.line'].browse(self.env.context['active_id'])
        pickup_req = self.env['pickup.request'].browse(manifest_pickup.pickup_id)
        items = []

        for line in pickup_req.pickup_request_line:
            if float(line.request_quantity) > 0.00:
                item = {
                    'pickup_req_product_id': int(line.id),
                    'product': str(line.product),
                    'product_id': int(line.product_id.id),
                    'request_quantity': float(line.request_quantity),
                    'received_quantity': float(line.request_quantity),
                }
                items.append((0, 0, (item)))

        res['po_number'] = str(pickup_req.po_number)
        res['po_id'] = int(pickup_req.po_id)
        res['pickup_name'] = str(pickup_req.name)
        res['pickup_req_id'] = manifest_pickup.pickup_id
        res['partner_id'] = int(pickup_req.partner_id.id)
        res['partner_address'] = str(pickup_req.partner_address)
        res['area'] = str(pickup_req.area)
        res['date_pickup'] = pickup_req.date_pickup
        res['warehouse_id'] = int(pickup_req.warehouse_id.id)
        res['pickup_manifest_id'] = int(manifest_pickup.pickup_manifest_id)
        res['pickup_manifest_line_id'] = self.env.context['active_id']

        res.update(pickup_product_line=items)

        return res


    # def default_get(self, fields):
    #
    #     res = super(PickupSuccess, self).default_get(fields)
    #     manifest_pickup = self.env['pickup.manifest.line'].browse(self.env. context['active_id'])
    #     pickup_req = self.env['pickup.request'].browse(manifest_pickup.pickup_id)
    #     items = []
    #
    #     for line in pickup_req.pickup_request_line:
    #         if float(line.request_quantity) > 0.00:
    #             item = {
    #                 'pickup_req_product_id': int(line.id),
    #                 'product': str(line.product),
    #                 'product_id': int(line.product_id.id),
    #                 'request_quantity': float(line.request_quantity),
    #                 'received_quantity': float(line.request_quantity),
    #             }
    #             items.append(item)
    #
    #     res['po_number'] = str(pickup_req.po_number)
    #     res['po_id'] = int(pickup_req.po_id)
    #     res['pickup_name'] = str(pickup_req.name)
    #     res['pickup_req_id'] = manifest_pickup.pickup_id
    #     res['partner_id'] = int(pickup_req.partner_id.id)
    #     res['partner_address'] = str(pickup_req.partner_address)
    #     res['area'] = str(pickup_req.area)
    #     res['date_pickup'] = pickup_req.date_pickup
    #     res['warehouse_id'] = int(pickup_req.warehouse_id.id)
    #     res['pickup_manifest_id'] = int(manifest_pickup.pickup_manifest_id)
    #     res['pickup_manifest_line_id'] = self.env.context['active_id']
    #
    #     res.update(pickup_product_line=items)
    #
    #     return res

    def _check_pickup_request_reattempt(self):
        po_obj = self.env['purchase.order'].browse(self.po_id)
        reattempt = po_obj.reattempt_number if po_obj.reattempt_number else 0
        reattempt_number = reattempt +1

        return reattempt_number

    def pickup_receive_process(self):
        get_data = self.read()[0]
        data = {}
        irn_line = []
        sum_rem_qty = 0.0
        sum_req_qty = 0.0
        sum_remaining_quantity = 0.00

        pickup_obj = self.env['pickup.request'].browse(int(get_data['pickup_req_id']))

        for j in get_data.get('pickup_product_line'):
            result = self.env['pickup.product.line'].browse(j)
            remaining_quantity = result.request_quantity - result.received_quantity
            sum_rem_qty += remaining_quantity
            sum_req_qty += result.request_quantity

            if remaining_quantity < 0.00:
                raise Warning(_('1Pick Up product line adjustment ERROR!   '
                                'Received quantity should be less then or equal to (requested quantity)!!!'))
        if sum_req_qty == sum_rem_qty:
            raise Warning(_('2Pick Up product line adjustment ERROR!   '
                            'Received quantity should be less then or equal to (requested quantity)!!!'))
        elif sum_rem_qty > 0.00 and (self.npr == ' ' or self.npr is False):
            raise Warning(_('No Pick Up Reason ERROR!   '
                            'Please give the No Pick Up Reason!!!'))
        else:
            for row in get_data.get('pickup_product_line'):
                pickup_product = self.env['pickup.product.line'].browse(row)
                result_remaining_quantity = pickup_product.request_quantity - pickup_product.received_quantity
                pickup_product.remaining_quantity = result_remaining_quantity

                if pickup_product.remaining_quantity > 0.00:
                    received_quantity = pickup_product.received_quantity
                    remaining_quantity = pickup_product.remaining_quantity
                    state = 'unsuccessful'
                    pickup_type = 'partial_received'

                else:
                    received_quantity = pickup_product.received_quantity
                    remaining_quantity = pickup_product.remaining_quantity
                    state = 'done'
                    pickup_type = 'full_received'

                ### Update Pickup Request Line ###

                update_query_pickup_req_line = "UPDATE pickup_request_line SET received_quantity='{0}'" \
                                               ", remaining_quantity = '{1}',state='{2}',pickup_type ='{3}' WHERE " \
                                               "pickup_request_line_id = {4} and id={5}".format(
                    received_quantity, remaining_quantity, state, pickup_type,
                    int(get_data['pickup_req_id']), pickup_product.pickup_req_product_id)
                self.env.cr.execute(update_query_pickup_req_line)
                self.env.cr.commit()

        for pickup_line in pickup_obj.pickup_request_line:
            irn_line.append([0, False, {
                    'po_id': pickup_obj.po_id,
                    'product': pickup_line.product,
                    'product_id': pickup_line.product_id.id,
                    'ordered_quantity': pickup_line.ordered_quantity,
                    'received_quantity': pickup_line.received_quantity,
                    }])
        data['irn_line'] = irn_line
        data['po_number'] = pickup_obj.po_number
        data['po_id'] = pickup_obj.po_id
        data['warehouse_id'] = pickup_obj.po_warehouse_id.id

        irn_obj = self.env['irn.control']
        if len(irn_line) > 0:
            save_the_data = irn_obj.create(data)

            for i in pickup_obj.pickup_request_line:
                sum_remaining_quantity += i.remaining_quantity

            ### Update Pickup Manifest Line ###
            for pml in self.env['pickup.manifest.line'].browse(int(self.pickup_manifest_line_id)):
                pml.write({'state': 'close'})

            ### Update Pickup Manifest ###
            for manifest_line_obj in self.env['pickup.manifest'].browse(int(self.pickup_manifest_id)):
                if len(manifest_line_obj.pickup_line) == 1:
                    manifest_line_obj.write({'state': 'done'})

            if sum_remaining_quantity == float(0):
                pickup_state = 'done'
                pick_type = 'full_received'
                npr = ''
            else:
                pickup_state = 'unsuccessful'
                pick_type = 'partial_received'

            ### Update Pickup Request ###
            for pickup_obj in self.env['pickup.request'].browse(int(self.pickup_req_id)):
                values = {'state': pickup_state,
                          'pickup_type': pick_type,
                          'npr': self.npr,
                          'received_by': self.env.uid,
                          'remark': self.remark,
                          }
                pickup_obj.write(values)

            try:
                if sum_remaining_quantity != float(0):
                    self.env['pickup.request'].send_email(1, 'NPR - Pickup Process', int(self.pickup_req_id))
            except:
                pass

            return save_the_data

        else:
            raise Warning(_('Please, Given actual reason for Cancel Pickup Request.'))

     ### below code dorkar nai
    # def pickup_receive_process(self, fields):
    #
    #     get_data = self.read()[0]
    #     data = {}
    #     irn_line = []
    #     sum_rem_qty = 0.0
    #     sum_req_qty = 0.0
    #     sum_remaining_quantity = 0.00
    #     npr = fields['npr']
    #     remark = fields['remark']
    #
    #     pickup_obj = self.env['pickup.request'].browse(int(get_data['pickup_req_id']))
    #
    #     for j in get_data.get('pickup_product_line'):
    #         result = self.env['pickup.product.line'].browse(j)
    #         remaining_quantity = result.request_quantity - result.received_quantity
    #         sum_rem_qty += remaining_quantity
    #         sum_req_qty += result.request_quantity
    #
    #         if remaining_quantity < 0.00:
    #             raise osv.except_osv(_('Pick Up product line adjustment ERROR!'),
    #                                  _('Received quantity should be less then or equal to (requested quantity)!!!'))
    #
    #     if sum_req_qty == sum_rem_qty:
    #         raise osv.except_osv(_('Pick Up product line adjustment ERROR!'),
    #                          _('Received quantity should be less then or equal to (requested quantity)!!!'))
    #
    #     elif sum_rem_qty > 0.00 and (npr == ' ' or npr is False):
    #         raise osv.except_osv(_('No Pick Up Reason ERROR!'),
    #                              _('Please give the No Pick Up Reason!!!'))
    #
    #     else:
    #         for row in get_data.get('pickup_product_line'):
    #
    #             pickup_product = self.env['pickup.product.line'].browse(row)
    #             result_remaining_quantity = pickup_product.request_quantity - pickup_product.received_quantity
    #             pickup_product.remaining_quantity = result_remaining_quantity
    #
    #             if pickup_product.remaining_quantity > 0.00 :
    #                 received_quantity = pickup_product.received_quantity
    #                 remaining_quantity = pickup_product.remaining_quantity
    #                 state = 'unsuccessful'
    #                 pickup_type = 'partial_received'
    #
    #             else:
    #                 received_quantity = pickup_product.received_quantity
    #                 remaining_quantity = pickup_product.remaining_quantity
    #                 state = 'done'
    #                 pickup_type = 'full_received'
    #
    #             update_query_pickup_req_line = "UPDATE pickup_request_line SET received_quantity='{0}'" \
    #                                            ", remaining_quantity = '{1}',state='{2}',pickup_type ='{3}' WHERE " \
    #                                            "pickup_request_line_id = {4} and id={5}".format(
    #                         received_quantity, remaining_quantity,state,pickup_type,
    #                         int(get_data['pickup_req_id']),pickup_product.pickup_req_product_id)
    #             self.env.cr.execute(update_query_pickup_req_line)
    #             self.env.cr.commit()
    #
    #     for pickup_line in pickup_obj.pickup_request_line:
    #         # if pickup_line.received_quantity > 0.0:
    #         irn_line.append([0, False, {
    #             'po_id': pickup_obj.po_id,
    #             'product': pickup_line.product,
    #             'product_id': pickup_line.product_id.id,
    #             'ordered_quantity': pickup_line.ordered_quantity,
    #             'received_quantity': pickup_line.received_quantity,
    #         }])
    #     data['irn_line'] = irn_line
    #     data['po_number'] = pickup_obj.po_number
    #     data['po_id'] = pickup_obj.po_id
    #     data['warehouse_id'] = pickup_obj.po_warehouse_id.id
    #
    #     irn_obj = self.env['irn.control']
    #
    #     if len(irn_line) >0:
    #         save_the_data = irn_obj.create(data)
    #
    #         for i in pickup_obj.pickup_request_line:
    #             sum_remaining_quantity += i.remaining_quantity
    #
    #         update_query_manifest_line = "UPDATE pickup_manifest_line SET state='done'" \
    #                                        "WHERE pickup_manifest_id = {0} and  pickup_id = {1} and id ={2}".format(
    #                                     int(get_data['pickup_manifest_id']),int(get_data['pickup_req_id']),
    #                                     int(get_data['pickup_manifest_line_id']))
    #
    #         self.env.cr.execute(update_query_manifest_line)
    #         self.env.cr.commit()
    #
    #         manifest_line_obj = self.env['pickup.manifest'].browse(int(get_data['pickup_manifest_id']))
    #         res=[]
    #         for manifest_line in manifest_line_obj.pickup_line:
    #             res.append(manifest_line.state)
    #
    #         if len(list(set(res))) == 1:
    #             update_query_manifest =  "UPDATE pickup_manifest SET state='done',close_time='{0}'" \
    #                                          "WHERE id ={1}".format(str(datetime.datetime.now()),int(get_data['pickup_manifest_id']))
    #
    #             self.env.cr.execute(update_query_manifest)
    #             self.env.cr.commit()
    #
    #
    #         if sum_remaining_quantity == float(0):
    #             pickup_state = 'done'
    #             pick_type = 'full_received'
    #             npr = ''
    #         else:
    #             pickup_state = 'unsuccessful'
    #             pick_type = 'partial_received'
    #
    #         update_query = "UPDATE pickup_request SET state='{0}', pickup_type='{1}',npr='{2}',remark='{3}'," \
    #                        "received_by={4},pickup_not_receive_date='{5}' WHERE id ={6}".format(
    #             pickup_state, pick_type,npr,remark,str(datetime.datetime.now()),int(get_data['pickup_req_id']))
    #         self.env.cr.execute(update_query)
    #         self.env.cr.commit()
    #
    #         try:
    #             if sum_remaining_quantity != float(0):
    #                 self.env['pickup.request'].send_email(1,'NPR - Pickup Process', int(get_data['pickup_req_id']))
    #         except:
    #             pass
    #
    #         return save_the_data
    #
    #     else:
    #         raise osv.except_osv(_('Warning!'),
    #                              _('Please, Given proper quantity'))


class PickupProductLine(models.Model):
    _name = "pickup.product.line"
    _description = 'pickup product line'

    pickup_product_line_id = fields.Many2one('pickup.success', 'pickup Product Line ID', ondelete='cascade', index=True)
    pickup_req_product_id = fields.Integer('Pickup Req Product ID')
    product = fields.Char('Product')
    product_id = fields.Integer('Product ID')
    request_quantity = fields.Float('Request Qty')
    received_quantity = fields.Float('Received Qty')
    remaining_quantity = fields.Float('Remaining Qty')
    remark = fields.Char('Remark')
    request_type = fields.Selection([
        ('fully_received', 'Fully Received'),
        ('partially_received', 'Partially Received')
    ], 'Request Type', help="Gives the status of the pickup request line", index=True)

    state = fields.Selection([
        ('confirmed', 'Confirmed'),
        ('cancel', 'Cancel')
    ], 'Status', help="Gives the status of the pickup request line", index=True)

    #### Block code for V-14
    #
    # _columns = {
    #     'pickup_product_line_id': fields.many2one('pickup.success', 'pickup Product Line ID',
    #                                               ondelete='cascade', index=True),
    #     'pickup_req_product_id': fields.integer('Pickup Req Product ID'),
    #     'product': fields.char('Product'),
    #     'product_id': fields.integer('Product ID'),
    #     'request_quantity': fields.float('Request Qty'),
    #     'received_quantity': fields.float('Received Qty'),
    #     'remaining_quantity': fields.float('Remaining Qty'),
    #     'remark': fields.char('Remark'),
    #     'request_type': fields.selection([
    #         ('fully_received', 'Fully Received'),
    #         ('partially_received', 'Partially Received')
    #     ], 'Request Type', help="Gives the status of the pickup request line", index=True),
    #
    #     'state': fields.selection([
    #         ('confirmed', 'Confirmed'),
    #         ('cancel', 'Cancel')
    #     ], 'Status', help="Gives the status of the pickup request line", index=True)
    # }

