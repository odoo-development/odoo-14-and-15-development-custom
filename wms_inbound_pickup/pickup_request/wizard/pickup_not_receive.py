from odoo import api, fields, models, _
from odoo.tools.translate import _
import datetime
from odoo.osv import osv


class PickupNotReceiveReson(models.Model):
    _name = "pickup.not.receive.reason"
    _description = "Pickup Not Receive Reason"

    pickup_not_receive_date = fields.Datetime('Cancel Pickup Request Date')
    pickup_not_receive_by = fields.Many2one('res.users', 'Cancel Pickup Request By')
    npr = fields.Selection([
        ("Vendor was unreachable", "Vendor was unreachable"),
        ("Vendor refused to provide", "Vendor refused to provide"),
        ("Unable to reach", "Unable to reach"),
        ("Vendors place was closed", "Vendors place was closed"),
        ("Cash problem", "Cash problem"),
        ("Vendor rescheduled", "Vendor rescheduled"),
        ("Wrong address given", "Wrong address given"),
        ("Unable to reach on Time", "Unable to reach on Time"),
        ("Quality issue", "Quality issue"),
        ("Due to traffic issue", "Due to traffic issue"),
        ("Invoice/Cash Memo not provided", "Invoice/Cash Memo not provided")
    ], 'No Pick UP Reason', copy=False, help="Reason", index=True, required=True)

    pickup_req_id = fields.Char('Pickup Request ID')
    po_id = fields.Integer('Purchase ID')
    pickup_manifest_id = fields.Integer('Pickup Manifest ID')
    pickup_manifest_line_id = fields.Integer('Pickup Manifest Line ID')

    def default_get(self, fields):
        res = super(PickupNotReceiveReson, self).default_get(fields)
        manifest_pickup = self.env['pickup.manifest.line'].browse(self.env.context['active_id'])
        pickup_req = self.env['pickup.request'].browse(manifest_pickup.pickup_id)

        res['po_id'] = int(pickup_req.po_id)
        res['pickup_req_id'] = manifest_pickup.pickup_id
        res['pickup_manifest_id'] = int(manifest_pickup.pickup_manifest_id)
        res['pickup_manifest_line_id'] = self.env.context['active_id']
        return res

    def pickup_not_receive(self):
        if self.npr != ' ':
            # state = 'unsuccessful'
            # pickup_type = 'not_received'
            #
            # for i in pickup_obj.pickup_request_line:
            #     requested_quantity = i.request_quantity
            #     update_query_pickup_req_line = "UPDATE pickup_request_line SET state='{0}', pickup_type='{1}',received_quantity=0.00, " \
            #                                    "remaining_quantity={2} WHERE pickup_request_line_id = {3} and id ={4} ".format(state,
            #                                                                                             pickup_type,
            #                                                                                             requested_quantity,
            #                                                                                             int(get_data[
            #                                                                                                     'pickup_req_id']),
            #                                                                                             int(i.id))
            #     self.env.cr.execute(update_query_pickup_req_line)
            #     self.env.cr.commit()

            for pickup_obj in self.env['pickup.request'].browse(int(self.pickup_req_id)):
                values = {'state': 'unsuccessful',
                          'pickup_type': 'not_received',
                          'npr': self.npr,
                          'received_by': self.env.uid,
                          'pickup_not_receive_date': fields.Datetime.now()
                          }
                pickup_obj.write(values)
                for prl in pickup_obj.pickup_request_line:
                    values = {'state': 'unsuccessful',
                              'pickup_type': 'not_received',
                              'received_quantity': 0.00,
                              'remaining_quantity': prl.request_quantity
                              }
                    prl.write(values)

            # update_query_manifest_line = "UPDATE pickup_manifest_line SET state='done'" \
            #                              "WHERE pickup_manifest_id = {0} and  pickup_id = {1} and id ={2}".format(
            #     int(get_data['pickup_manifest_id']), int(get_data['pickup_req_id']),
            #     int(get_data['pickup_manifest_line_id']))
            #
            # self.env.cr.execute(update_query_manifest_line)
            # self.env.cr.commit()

            ### Update Pickup Manifest Line ###
            for pml in self.env['pickup.manifest.line'].browse(int(self.pickup_manifest_line_id)):
                pml.write({'state': 'close'})

            # manifest_line_obj = self.env['pickup.manifest'].browse(int(get_data['pickup_manifest_id']))
            # res = []
            # for manifest_line in manifest_line_obj.pickup_line:
            #     res.append(manifest_line.state)
            #
            # if len(list(set(res))) == 1:
            #     update_query_manifest = "UPDATE pickup_manifest SET state='done'" \
            #                             "WHERE id ={0}".format(
            #                                                    int(get_data['pickup_manifest_id']))
            #
            #     self.env.cr.execute(update_query_manifest)
            #     self.env.cr.commit()

            ### Update Pickup Manifest ###
            for manifest_line_obj in self.env['pickup.manifest'].browse(int(self.pickup_manifest_id)):
                if len(manifest_line_obj.pickup_line) == 1:
                    manifest_line_obj.write({'state': 'done'})

            # update_query = "UPDATE pickup_request SET state='{0}', pickup_type='{1}' , npr='{2}', " \
            #                "received_by={3},pickup_not_receive_date='{4}' WHERE id ={5}".format(
            #     state, pickup_type, npr, pickup_not_receive_by, (datetime.datetime.now()), int(get_data['pickup_req_id']))
            # self.env.cr.execute(update_query)
            # self.env.cr.commit()

            try:
                self.env['pickup.request'].send_email(1, 'NPR - Pickup Process', int(self.pickup_req_id))
            except:
                pass

        else:
            raise osv.except_osv(_('Warning!'),
                                 _('Please, Given actual reason for Cancel Pickup Request.'))
