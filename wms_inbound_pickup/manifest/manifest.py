from datetime import datetime, timedelta
from odoo import api, fields, models, _
from odoo.osv import osv


class PickupManifest(models.Model):
    _name = "pickup.manifest"
    _description = "Pickup Manifest Process"
    _order = 'id desc'

    vehicle_type = fields.Char(string='Vehicle Type', required=True)
    name = fields.Char(string='Manifest Number')
    pickup_boy = fields.Char(string='Pickup Boy', required=True)
    warehouse_id = fields.Many2one('stock.warehouse', string='Warehouse Location', required=True)
    date_pickup = fields.Date('Pick Up Date', required=True)
    remark = fields.Text('Remark')
    scan = fields.Char('Scan')
    pickup_line = fields.One2many('pickup.manifest.line', 'pickup_manifest_id', 'Assign to Courier', required=True)

    approved_time = fields.Datetime('Approved Date', help="Date on which manifest is approved.")
    approved_by = fields.Many2one('res.users', 'Approved By')

    dispatch_time = fields.Datetime('Dispatch Date', help="Date on which manifest is dispatch.")
    dispatch_by = fields.Many2one('res.users', 'Dispatch By')

    close_time = fields.Datetime('Close Date', help="Date on which manifest is closeled.")
    close_by = fields.Many2one('res.users', 'Close By')

    cancel_time = fields.Datetime('Cancel Date', help="Date on which manifest is closeled.")
    cancel_by = fields.Many2one('res.users', 'Cancel By')
    npr = fields.Selection([
        ("Vendor was unreachable", "Vendor was unreachable"),
        ("Vendor refused to provide", "Vendor refused to provide"),
        ("Unable to reach", "Unable to reach"),
        ("Vendors place was closed", "Vendors place was closed"),
        ("Cash problem", "Cash problem"),
        ("Vendor rescheduled", "Vendor rescheduled"),
        ("Wrong address given", "Wrong address given"),
        ("Unable to reach on Time", "Unable to reach on Time"),
        ("Quality issue", "Quality issue"),
        ("Due to traffic issue", "Due to traffic issue")
    ], 'No Pick UP Reason', copy=False, help="Reason", index=True)

    state = fields.Selection([
        ('pending', 'Pending'),
        ('approved', 'Approve'),
        ('dispatch', 'Dispatch'),
        ('done', 'Done'),
        ('cancel', 'Cancel'),
    ], 'State', default='pending', readonly=True, copy=False, help="Gives the status of the Pickup Manifest", index=True)

    # ## Block code for V-14
    # _columns = {
    #     # 'manifest_close_status': fields.function(_close_status_update, string="Manifest", type="many2many", relation='pickup.manifest'),
    #     'vehicle_type': fields.char(string='Vehicle Type', required=True),
    #     'name': fields.char(string='Manifest Number'),
    #     'pickup_boy': fields.char(string='Pickup Boy', required=True),
    #     'warehouse_id': fields.many2one('stock.warehouse', string='Warehouse Location', required=True),
    #     'date_pickup': fields.date('Pick Up Date', required=True),
    #     'remark': fields.text('Remark'),
    #     'scan': fields.char('Scan'),
    #     'pickup_line': fields.one2many('pickup.manifest.line', 'pickup_manifest_id', 'Assign to Courier', required=True),
    #
    #     'approved_time': fields.datetime('Approved Date', help="Date on which manifest is approved."),
    #     'approved_by': fields.many2one('res.users', 'Approved By'),
    #
    #     'dispatch_time': fields.datetime('Dispatch Date', help="Date on which manifest is dispatch."),
    #     'dispatch_by': fields.many2one('res.users', 'Dispatch By'),
    #
    #     'close_time': fields.datetime('Close Date', help="Date on which manifest is closeled."),
    #     'close_by': fields.many2one('res.users', 'Close By'),
    #
    #     'cancel_time': fields.datetime('Cancel Date', help="Date on which manifest is closeled."),
    #     'cancel_by': fields.many2one('res.users', 'Cancel By'),
    #
    #     # 'npr': fields.char('npr'),
    #     'npr': fields.selection([
    #
    #         ("Vendor was unreachable", "Vendor was unreachable"),
    #         ("Vendor refused to provide", "Vendor refused to provide"),
    #         ("Unable to reach", "Unable to reach"),
    #         ("Vendors place was closed", "Vendors place was closed"),
    #         ("Cash problem", "Cash problem"),
    #         ("Vendor rescheduled", "Vendor rescheduled"),
    #         ("Wrong address given", "Wrong address given"),
    #         ("Unable to reach on Time", "Unable to reach on Time"),
    #         ("Quality issue", "Quality issue"),
    #         ("Due to traffic issue", "Due to traffic issue")
    #
    #     ], 'No Pick UP Reason', copy=False, help="Reason", index=True),
    #
    #     'state': fields.selection([
    #         ('pending', 'Pending'),
    #         ('approved', 'Approve'),
    #         ('dispatch', 'Dispatch'),
    #         ('done', 'Done'),
    #         ('cancel', 'Cancel'),
    #
    #     ], 'State', readonly=True, copy=False, help="Gives the status of the Pickup Manifest", index=True)
    # }
    #
    # _defaults = {self
    #     'user_id': lambda obj, cr, uid, context: uid,
    #     'state': 'pending'
    # }


    def print_report(self):
        '''This function prints the pickup manifest'''
        context = dict(self.env.context or {}, active_ids=self.ids)
        return self.env["report"].get_action(cr, uid, ids, 'wms_inbound_pickup.report_pickup_manifest_layout', context=context)


    def approved_pickup_manifest(self):
        uid = self.env.uid

        try:
            manifest_env = self.env['pickup.manifest']
            manifest_obj = manifest_env.browse()

            for single_id in self.ids:
                approved_pickup_query = "UPDATE pickup_manifest SET state='approved', approved_by='{0}', approved_time='{1}' WHERE id='{2}'".format(
                    uid, str(fields.datetime.now()), single_id)
                self.env.cr.execute(approved_pickup_query)
                self.env.cr.commit()

                approved_pickup_query_line = "UPDATE pickup_manifest_line SET state='approved', approved_by='{0}', approved_time='{1}' WHERE pickup_manifest_id = {2}".format(
                    uid, str(fields.datetime.now()), single_id)
                self.env.cr.execute(approved_pickup_query_line)
                self.env.cr.commit()
            for row in manifest_obj.pickup_line:
                self.env['inb.end.to.end'].manifest_confirm_update(str(row.po_number), row.po_id,
                                                               str(fields.datetime.now()))
        except:
            pass

        return True

    def dispatch_pickup_manifest(self):
        uid = self.env.uid
        try:
            manifest_env = self.env['pickup.manifest']
            manifest_obj = manifest_env.browse()
            for single_id in self.ids:
                dispatch_pickup_query = "UPDATE pickup_manifest SET state='dispatch', dispatch_by='{0}', dispatch_time='{1}' WHERE id='{2}'".format(
                    uid, str(fields.datetime.now()), single_id)
                self.env.cr.execute(dispatch_pickup_query)
                self.env.cr.commit()

                dispatch_pickup_query_line = "UPDATE pickup_manifest_line SET state='dispatch', dispatch_by='{0}', dispatch_time='{1}' WHERE pickup_manifest_id = {2}".format(
                    uid, str(fields.datetime.now()), single_id)
                self.env.cr.execute(dispatch_pickup_query_line)
                self.env.cr.commit()

            for row in manifest_obj.pickup_line:
                self.env['inb.end.to.end'].manifest_dispatch_update(str(row.po_number), row.po_id, str(fields.datetime.now()))
        except:
            pass

        return True

    def cancel_pickup_manifest(self):
        uid = self.env.uid
        try:
            for single_id in self.ids:
                close_pickup_query = "UPDATE pickup_manifest SET state='cancel', cancel_by='{0}', cancel_time='{1}' WHERE id='{2}'".format(
                    uid, str(fields.datetime.now()), single_id)
                self.env.cr.execute(close_pickup_query)
                self.env.cr.commit()

                close_pickup_query_line = "UPDATE pickup_manifest_line SET state='cancel', cancel_by='{0}', cancel_time='{1}' WHERE pickup_manifest_id = {2}".format(
                    uid, str(fields.datetime.now()), single_id)
                self.env.cr.execute(close_pickup_query_line)
                self.env.cr.commit()

                pickup_manifest = self.env['pickup.manifest'].browse(single_id)

                for row in pickup_manifest.pickup_line:
                    update_query = "UPDATE pickup_request SET state ='pending', cancel_roll_back_pending_time ='{0}' WHERE id={1}".format(
                        str(fields.datetime.now()), row.pickup_id)
                    self.env.cr.execute(update_query)
                    self.env.cr.commit()

                    update_query_pickup_req_line = "UPDATE pickup_request_line SET state='pending', cancel_roll_back_pending_time ='{0}' WHERE pickup_request_line_id={1}".format(
                        str(fields.datetime.now()), row.pickup_id)
                    self.env.cr.execute(update_query_pickup_req_line)
                    self.env.cr.commit()
        except:
            pass

        return True

    @api.model
    def create(self, vals):

        # generate manifest number
        record = super(PickupManifest, self).create(vals)

        manifest_number = "MF0" + str(record.id)
        record.name = manifest_number
        pickup_line = vals['pickup_line'][0][2]

        self.env['inb.end.to.end'].manifest_create(pickup_line['po_number'], pickup_line['po_id'])

        return record


class PickupManifestLine(models.Model):
    _name = "pickup.manifest.line"
    _description = "Manifest Line"

    pickup_manifest_id = fields.Many2one('pickup.manifest', 'Manifest Line ID', required=True, ondelete='cascade', index=True, readonly=True)
    pickup_number = fields.Char('Pickup Number')
    pickup_id = fields.Integer('PIckup ID')
    po_number = fields.Char('PO Number')
    npr = fields.Selection([
        ("Vendor was unreachable", "Vendor was unreachable"),
        ("Vendor refused to provide", "Vendor refused to provide"),
        ("Unable to reach", "Unable to reach"),
        ("Vendors place was closed", "Vendors place was closed"),
        ("Cash problem", "Cash problem"),
        ("Vendor rescheduled", "Vendor rescheduled"),
        ("Wrong address given", "Wrong address given"),
        ("Unable to reach on Time", "Unable to reach on Time"),
        ("Quality issue", "Quality issue"),
        ("Due to traffic issue", "Due to traffic issue")
    ], 'No Pick UP Reason', copy=False, help="Reason", index=True)

    po_id = fields.Integer('Purchase ID')
    partner_id = fields.Many2one('res.partner', 'Vendor', required=True,
                                  change_default=True, tracking=True)
    partner_address = fields.Char('Vendor Address', required=True)
    area = fields.Char('Area', required=True)
    warehouse_id = fields.Many2one('stock.warehouse', string='Warehouse Location', required=True)

    approved_time = fields.Datetime('Approved Date', help="Date on which manifest is approved.")
    approved_by = fields.Many2one('res.users', 'Approved By')

    dispatch_time = fields.Datetime('Dispatch Date', help="Date on which manifest is dispatch.")
    dispatch_by = fields.Many2one('res.users', 'Dispatch By')

    close_time = fields.Datetime('Close Date', help="Date on which manifest is closeled.")
    close_by = fields.Many2one('res.users', 'Close By')

    cancel_time = fields.Datetime('Cancel Date', help="Date on which manifest is closeled.")
    cancel_by = fields.Many2one('res.users', 'Cancel By')

    state = fields.Selection([
        ('pending', 'Pending'),
        ('approved', 'Approve'),
        ('dispatch', 'Dispatch'),
        ('close', 'Close'),
    ], 'Status', readonly=True, copy=False, help="Gives the status of the Pickup Manifest", index=True)

    # ## Block code for V-14
    #
    # _columns = {
    #     'pickup_manifest_id': fields.many2one('pickup.manifest', 'Manifest Line ID', required=True, ondelete='cascade',
    #                                           index=True, readonly=True),
    #     'pickup_number': fields.char('Pickup Number'),
    #     'pickup_id': fields.integer('PIckup ID'),
    #     'po_number': fields.char('PO Number'),
    #     # 'npr': fields.char('npr'),
    #     'npr': fields.selection([
    #
    #         ("Vendor was unreachable", "Vendor was unreachable"),
    #         ("Vendor refused to provide", "Vendor refused to provide"),
    #         ("Unable to reach", "Unable to reach"),
    #         ("Vendors place was closed", "Vendors place was closed"),
    #         ("Cash problem", "Cash problem"),
    #         ("Vendor rescheduled", "Vendor rescheduled"),
    #         ("Wrong address given", "Wrong address given"),
    #         ("Unable to reach on Time", "Unable to reach on Time"),
    #         ("Quality issue", "Quality issue"),
    #         ("Due to traffic issue", "Due to traffic issue")
    #
    #     ], 'No Pick UP Reason', copy=False, help="Reason", index=True),
    #
    #     'po_id': fields.integer('Purchase ID'),
    #     'partner_id': fields.many2one('res.partner', 'Vendor', required=True,
    #                                   change_default=True, track_visibility='always'),
    #     'partner_address': fields.char('Vendor Address', required=True),
    #     'area': fields.char('Area', required=True),
    #     'warehouse_id': fields.many2one('stock.warehouse', string='Warehouse Location', required=True),
    #
    #     'approved_time': fields.datetime('Approved Date', help="Date on which manifest is approved."),
    #     'approved_by': fields.many2one('res.users', 'Approved By'),
    #
    #     'dispatch_time': fields.datetime('Dispatch Date', help="Date on which manifest is dispatch."),
    #     'dispatch_by': fields.many2one('res.users', 'Dispatch By'),
    #
    #     'close_time': fields.datetime('Close Date', help="Date on which manifest is closeled."),
    #     'close_by': fields.many2one('res.users', 'Close By'),
    #
    #     'cancel_time': fields.datetime('Cancel Date', help="Date on which manifest is closeled."),
    #     'cancel_by': fields.many2one('res.users', 'Cancel By'),
    #
    #     'state': fields.selection([
    #         ('pending', 'Pending'),
    #         ('approved', 'Approve'),
    #         ('dispatch', 'Dispatch'),
    #         ('close', 'Close'),
    #
    #     ], 'Status', readonly=True, copy=False, help="Gives the status of the Pickup Manifest", index=True),
    #
    # }
