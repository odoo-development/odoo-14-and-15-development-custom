{
    "name": "Warehouse Access Control",
    "summary": """
         Warehouse and Stock Location Access on Users.""",
    "description": """
        This Module Restricts the User from Accessing Warehouse and Process Stock Moves other than allowed to Warehouses and Stock Locations.
    """,
    "author": "Odoo Bangladesh",
    'category': 'Inventory/Inventory',
    "version": "1.0.0",
    "depends": ["stock", "sale_management"],
    "data": [
        "views/users_view.xml",
        "security/security.xml",
    ],
}
