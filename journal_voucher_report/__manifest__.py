# -*- coding: utf-8 -*-

{

    'name': 'Journal Voucher Report',
    'version': '14.0.1.2',
    'category': 'Accounting',
    'summary': 'Journal Voucher PDF Report',
    'description': "",
    'website': "sindabad.com",
    'depends': [
        'account'
    ],
    'data': [
        'report/journal_voucher_report.xml',
        'views/report_menu.xml',
    ],
    'demo': [],
    'installable': True,
    'application': True,
    'auto_install': False,
}
