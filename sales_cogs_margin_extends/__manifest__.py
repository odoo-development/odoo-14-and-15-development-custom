{
    'name': 'Sales Cogs Margin Extends',
    'version': "14.0.1.0.0",
    'author': "Odoo Bangladesh",
    'category': 'Sales',
    'summary': 'SO to Delivery summary',
    'depends': ['sale_management', 'sindabad_customization'],
    ### Block for V-14
    # 'depends': ['sale', 'report_xls', 'sales_cogs_margin', 'sindabad_customization'],
    'data': [
        'security/sales_cogs_margin_extend_security.xml',
        'security/ir.model.access.csv',
        'views/order_cogs_more_menu.xml',
        'views/order_cogs.xml'
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}
