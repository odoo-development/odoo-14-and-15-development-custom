{
    'name': "Reserve Date for stock product",
    'summary': """
        This module will create stock reserve date """,
    'description': """
        It will create stock reserve date when someone hit check availability button
    """,
    'author': "Odoo Bangladesh",
    'website': " ",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/openerp/addons/base/module/module_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': "14.0.1.0.0",

    # any module necessary for this one to work correctly
    'depends': [
            'stock',
    ],

    # always loaded
    'data': [
        'security/stock_move_details_security.xml',
        'security/ir.model.access.csv',
        "views/move_history_view.xml",
        # 'views/stock_move_history_view.xml',
    ],

}
