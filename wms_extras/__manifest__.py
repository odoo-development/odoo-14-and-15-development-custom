{
    'name': 'WMS Extras',
    'version': "14.0.1.0.0",
    'category': 'WMS Extras',
    'author': 'Odoo Bangladesh',
    'summary': 'WMS Extras',
    'description': 'WMS Extras',
    'depends': ['partner_custom_fields', 'wms_menulist'],
    "data": [
        "views/stock_picking_view.xml",
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}
