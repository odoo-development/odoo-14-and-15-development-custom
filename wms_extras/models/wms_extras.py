from odoo import api, fields, models, _


class StockPicking(models.Model):
    _inherit = 'stock.picking'

    def write(self, vals):

        return super(StockPicking, self).write(vals)

    def create(self, vals):

        classification = ''
        area = ''
        new_customer = False
        try:

            order_origin = vals.get('origin')
            if str(order_origin).startswith("S"):
                so_obj = self.env['sale.order']

                so_list = so_obj.search([('name', '=', str(order_origin))])
                # so = so_obj.browse(so_list)
                area = str(so_list.partner_shipping_id.state_id.name)
                classification = str(so_list.customer_classification)
                new_customer = so_list.new_customer
        except:
            pass

        vals['area'] = area
        vals['classification'] = classification
        vals['x_priority_customer'] = new_customer
        return super(StockPicking, self).create(vals)

    def _get_customer_classification(self):

        for pick in self:

            pick.classification = False
            if str(pick.origin).startswith("S"):
                so_obj = self.env['sale.order']

                so_list = so_obj.search([('name', '=', str(pick.origin))])
                so = so_obj.browse(so_list)

                parent = so_list.partner_id

                for i in range(5):
                    if len(parent.parent_id) == 1:
                        parent = parent.parent_id
                    else:
                        classification = parent.x_classification
                        break

                if classification:
                    pick.classification = str(classification)

    def _get_customer_area(self):
        for pick in self:
            pick.area = False
            if pick.partner_id.state_id:
                pick.area = str(pick.partner_id.state_id.name)

    classification = fields.Char('Classification', compute='_get_customer_classification')
    area = fields.Char('Area', compute='_get_customer_area')

    # ## Block code for V-14
    # _columns = {
    #     'classification': fields.char('Classification'),
    #     'area': fields.char('Area'),
    # }
