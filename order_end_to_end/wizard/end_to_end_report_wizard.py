from odoo import api, fields, models, _


class EndToEndReportWizard(models.TransientModel):
    _name = "end.to.end.report.wizard"
    _description = "End To End Report"

    date_from = fields.Date(string="Date From", default=fields.Datetime.now())
    date_to = fields.Date(string="Date To", default=fields.Datetime.now())
    category = fields.Selection([('All', 'All'),
                                 ('Pending for Procurement', 'Pending for Procurement'),
                                 ('Delivred Order', 'Delivred Order'),
                                 ('STN', 'STN')], string='Category')

    warehouse_id = fields.Many2one('stock.warehouse', string='Warehouse')

    def print_report_xls(self):
        # order_ids = self.env['account.move'].search([('state', '!=', 'cancel')])
        data = {
            'date_from': self.date_from,
            'date_to': self.date_to,
            'warehouse_id': self.warehouse_id,
            'category': self.category,

            # 'form_data': self.read()[0],
            # 'order_list': order_ids
        }
        return self.env.ref('order_end_to_end.action_end_to_end_report_xlsx').report_action(self, data=data)

    

