import base64
import io
from odoo import models


class EndToEndReportXlsx(models.AbstractModel):
    _name = 'report.order_end_to_end.end_to_end_report_xls'
    _description = 'Order End To End Report'
    _inherit = 'report.report_xlsx.abstract'

    def generate_xlsx_report(self, workbook, data, objects):
        domain = [('state', '!=', 'cancel')]

        if data.get('date_from'):
            domain.append(('date', '>=', data.get('date_from')))
        if data.get('date_to'):
            domain.append(('date', '<=', data.get('date_to')))
        if data.get('category'):
            domain.append(('category', '=', data.get('category')))
        if data.get('warehouse_id'):
            domain.append(('warehouse_id', '=', data.get('warehouse_id')))

        report_name = "Order End To End Report"
        sheet = workbook.add_worksheet(report_name)
        bold = workbook.add_format({'bold': True, 'align': 'center', 'bg_color': '#fffbed', 'border': True})
        title = workbook.add_format({'bold': True, 'align': 'center', 'bg_color': '#f2eee4', 'border': True})
        header_row_style = workbook.add_format({'bold': True, 'align': 'center', 'border': True})

        sheet.merge_range('A1:F1', 'Order End To  End Report', title)

        invoices = self.env['stock.move'].search(domain)
        row = 3
        col = 0

        sheet.write(row, col, 'Date Range: ', header_row_style)
        sheet.write(row, col+1, data.get('date_from'), header_row_style)
        sheet.write(row, col+2, 'To', header_row_style)
        sheet.write(row, col+3, data.get('date_to'), header_row_style)

        row += 2

        # Header Row
        sheet.set_column(0, 5, 18)
        sheet.write(row, col, 'Name', header_row_style)
        sheet.write(row, col+1, 'Reference', header_row_style)

        row += 2

        for inv in invoices:

            for line in inv.invoice_line_ids:
                sheet.write(row, col, inv.name, bold)
                sheet.write(row, col+1, line.ref)

                row += 1
