{
    'name': 'Order End To End Report',
    'version': '14.0.1',
    'author': "Sindabad",
    'category': 'Sales',
    'summary': 'Details Summary of Delievred quantity with profit and Loss Statement',
    'depends': ['base', 'sale', 'report_xlsx',],
    'data': [
        'security/sales_end_to_end_security.xml',
        'security/ir.model.access.csv',
        'wizard/end_to_end_report_wizard_view.xml',
        'report/action_end_to_end_report.xml',
        'views/end_to_end_report_menu.xml',
        'views/action_manager.xml',
    ],
}
