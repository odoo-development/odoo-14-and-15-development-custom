{
    'name': 'outbound picking search',
    'version': "14.0.1.0.0",
    'category': 'outbound',
    'author': 'Ashif',
    'summary': 'outnound picking Custom Search',
    'description': 'order search in picking Custom Search',
    'depends': ['base', 'stock','pickings', 'wms_base'],
    'installable': True,
    'application': True,
    'auto_install': False,
}
