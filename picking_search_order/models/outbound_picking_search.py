from operator import itemgetter

from odoo import models, fields


class OutboundPicking(models.Model):
    _inherit = 'picking.list'

    def _get_order_no(self):

        return True

    def _order_no_search(self, operator, value):

        # args = [('order_on', 'ilike', '1000026832')]
        operator = operator
        order_no = value

        outbound_picking_list_line = "SELECT DISTINCT picking_id FROM picking_list_line WHERE magento_no = '{0}'".format(
            order_no)
        self.env.cr.execute(outbound_picking_list_line)

        res = self.env.cr.fetchall()
        if not res:
            return [('id', '=', '0')]
        return [('id', 'in', res)]

    def _get_invoice_no(self):

        return True

    order_on = fields.Text(compute='_get_order_no', string='Order Number', search='_order_no_search', index=True)
