from odoo import api, fields, models, _
from odoo.exceptions import ValidationError
from odoo.osv import osv


class AccountMove(models.Model):
    _inherit = "account.move"

    def active_delivery_dispatch(self):
        self.ensure_one()
        # ids = self._context['active_ids']
        # ai_obj = self.browse(ids)
        so = self.env['sale.order'].search([('name', '=', self.invoice_origin)], limit=1)
        if len(so) > 0:
            so.write({'delivery_dispatch': True})

        ### Commented By Matiar Rahman
        ### Need to change the business logic

        # for acc_in in self:
        #     delivery_dispatch_query = "UPDATE sale_order SET delivery_dispatch=TRUE WHERE client_order_ref='{0}'".format(str(acc_in.name))
        #
        #     self.env.cr.execute(delivery_dispatch_query)
        #     self.env.cr.commit()
        #
        # return True


class SaleOrder(models.Model):
    _inherit = 'sale.order'

    @api.depends('procurement_group_id')
    def _get_shipped(self):
        for sale in self:
            if sale.procurement_group_id:
                sale.shipped = all([proc.state in ['cancel', 'done'] for proc in sale.procurement_group_id.stock_move_ids])
            else:
                sale.shipped = False

    def _get_orders_procurements(self):
        res = set()
        for proc in self.env['procurement.order'].browse():
            if proc.state == 'done' and proc.sale_line_id:
                res.add(proc.sale_line_id.order_id.id)
        return list(res)


    delivery_dispatch = fields.Boolean('Delivered')
    invoiced_dispatch = fields.Boolean('Dispatched')
    shipped = fields.Boolean(compute='_get_shipped', string='Order Invoiced', store=True)

