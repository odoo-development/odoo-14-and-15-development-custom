{
    'name': 'Order Delivery Dispatch',
    'version': "14.0.1.0.0",
    'category': 'Order Delivery Dispatch',
    'author': 'Odoo Bangladesh',
    'summary': 'Sales Order, Delivery, Dispatch',
    'description': 'Order Delivery Dispatch',
    'depends': ['sale_management',
                # 'account',
                # 'delivery_customer_name'
                ],
    'data': [
        'views/account_invoice.xml',
    ],
    'installable': True,
    'application': False,
    'auto_install': False,
}
