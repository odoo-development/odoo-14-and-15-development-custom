{
    'name': 'Auto stock selection in Back to Back PO',
    "version": "14.0.1.0.0",
    "category": "Stock",
    "author": "Odoo Bangladesh",
    "summary": "Stock",
    "description": "In Back to Back PO if a Warehouse operation is selected then perspective stock will be selected automatically",
    # 'depends': ['sgeede_b2b'],
    "installable": True,
    "application": True,
    "auto_install": False,
}
