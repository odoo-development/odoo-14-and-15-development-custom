from odoo import api, models


class AutoStockSelection(models.Model):
    _inherit = "back.to.back.order"

    @api.onchange('picking_type_id')
    def stock_location_onchange(self):
        default_stock = self.env['stock.picking.type'].browse(self._cr, self._uid, int(self.picking_type_id),
                                                              context=self._context)

        default_stock_id = default_stock.default_location_dest_id

        self.location_id = default_stock_id

        return "True"
