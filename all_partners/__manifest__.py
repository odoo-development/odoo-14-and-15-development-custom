{
    'name': 'All Partners',
    'version': '1.0',
    'category': 'Sale',
    'author': 'Sindabad',
    'summary': 'All Partners information are includes here.',
    'description': 'All Partners information are includes here.',
    'depends': [
        'base',
    ],
    'data': [
        'security/all_partners_security.xml',
        'views/all_partners_view.xml',
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}
