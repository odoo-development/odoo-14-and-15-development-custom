
from odoo import api, fields, models


class IrnControl(models.Model):
    _inherit = "irn.control"

    def confirm_wms_inbound_irn(self):
        self.ensure_one()
        # irn_env = self.env['irn.control']
        # irn_obj = irn_env.browse()

        # check if already irn pushed

        if not self.qc_pushed:

            qc_data = {
                'irn_number': self.irn_number,
                'irn_id': self.id,
                'warehouse_id': self.warehouse_id.id,
                'po_number': self.po_number,
                'po_id': self.po_id,
                'remark': self.remark,
                'received_by': self.received_by.id,
            }

            qc_line_list = list()

            for line_item in self.irn_line:
                qc_line_list.append([0, False,
                                     {
                                         'product': line_item.product,
                                         'product_id': line_item.product_id.id,
                                         'product_ean': line_item.product_ean if line_item.product_ean else "",
                                         'description': line_item.remark if line_item.remark else "",
                                         'purchase_quantity': line_item.ordered_quantity,
                                         'received_quantity': line_item.received_quantity,
                                         'accepted_quantity': 0,
                                         'pending_quantity': line_item.received_quantity,
                                     }])

            qc_data['qc_line'] = qc_line_list

            # qc_env = self.env['quality.control']
            saved_qc_id = self.env['quality.control'].create(qc_data)

            values = {'qc_pushed': True,
                      'date_confirm': fields.Datetime.now()}
            self.write(values)
            self.irn_line.write({'qc_pushed': True})
            po = self.env['purchase.order'].browse([self.po_id])
            po.write({'irn_created': True})

            # for single_id in self.ids:
            #     qc_pushed_update_query = "UPDATE IrnControl SET qc_pushed=TRUE, date_confirm='{0}' WHERE id={1}".format(
            #         str(fields.datetime.now()), single_id)
            #     self.env.cr.execute(qc_pushed_update_query)
            #     self.env.cr.commit()
            #
            #     qc_pushed_update_query_irn_line = "UPDATE IrnControlLine SET qc_pushed=TRUE WHERE irn_line_id={0}".format(
            #         single_id)
            #     self.env.cr.execute(qc_pushed_update_query_irn_line)
            #     self.env.cr.commit()
            #
            #     irn_status_update_query = "UPDATE PurchaseOrder SET irn_created=TRUE WHERE id={0}".format(irn_obj.po_id)
            #     self.env.cr.execute(irn_status_update_query)
            #     self.env.cr.commit()

            # update irn end-to-end
            self.env['inb.end.to.end'].irn_confirm_update(po_number=self.po_number, po_id=self.po_id)

        return True

