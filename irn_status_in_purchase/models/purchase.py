from odoo import fields, models
from collections import OrderedDict


class PurchaseOrder(models.Model):
    _inherit = 'purchase.order'

    irn_created = fields.Boolean('IRN Created', default=False)

    def irn_status_for_old_po(self):
        # get_po_number_from_irn = "SELECT po_id FROM irn_control WHERE qc_pushed = TRUE"
        # self.env.cr.execute(get_po_number_from_irn)
        # query_fetch = self.env.cr.fetchall()
        # po_list = [line[0] for line in query_fetch]

        po_ids = [irn.po_id for irn in self.env['irn.control'].search([('qc_pushed', '=', True)])]
        self.browse(list(OrderedDict.fromkeys(po_ids))).write({'irn_created': True})

        # if len(po_list) > 0:
        #     update_query = "UPDATE purchase_order SET irn_created = TRUE WHERE id IN {0}".format(tuple(po_list))
        #     self.env.cr.execute(update_query)
        #     self.env.cr.commit()
