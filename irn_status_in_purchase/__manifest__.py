{
     'name': 'IRN Status in Purchase',
     "version": "14.0.1.0.0",
     "category": "Purchase",
     'description': 'IRN Status in Purchase',
     'author': 'Odoo Bangladesh',
     'depends': ['wms_inbound', 'purchase'],
     'data': ['views/irn_status_view_po.xml']

 }