# Author Rocky 2021

from odoo import fields, models, api


class SaleOrder(models.Model):
    _inherit = "sale.order"

    so_inside_outside = fields.Selection([('inside', 'Inside'), ('outside', 'Outside')],
                                         string='Address Zone')

    @api.model
    def create(self, vals):

        so = super(SaleOrder, self).create(vals)

        # order_info = self.browse([so].id)
        # import pdb;pdb.set_trace()

        if 'Dhaka' in so.partner_shipping_id.city:
            so_inside_outside = 'inside'

        else:
            so_inside_outside = 'outside'

        so.write({'so_inside_outside': so_inside_outside})

        return so

