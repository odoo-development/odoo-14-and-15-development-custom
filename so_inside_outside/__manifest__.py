# Author Rocky 2021

{
    "name": "SO Delivery Inside Outside Field",
    "version": "14.0",
    "category": "Sales Management",
    "author": "Rocky",
    "summary": "SO Delivery Inside Outside Field",
    "description": "SO Delivery Inside Outside Field",
    "depends": ["sale"],
    "data": ["so_inside_outside_view.xml"],
    "installable": True,
    "application": True,
    "auto_install": False,
}
