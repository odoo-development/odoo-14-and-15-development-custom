from odoo import api, fields, models, _
from odoo.tools.translate import _
from odoo.exceptions import except_orm, Warning, RedirectWarning, UserError


class DeliveryManInfo(models.Model):
    _name = "delivery.man.info"
    _description = "Delivery Man Info"

    name=fields.Char(string="Delivery Man Name")
    delivery_man_mobile=fields.Char(string="Mobile Number")
    warehouse_name=fields.Selection( [
        ('uttwh', 'Uttara Warehouse'),
        ('bddwh', 'Badda Warehouse')],
        string="Warehouse Name")
    status=fields.Boolean('Active', default=True)
    sailor_id=fields.Integer(String="Sailor ID")

    # @api.model
    # def create(self,vals):
    #     return super(DeliveryManInfo,self).create(vals)



class WmsManifestProcess(models.Model):
    _inherit = "wms.manifest.process"

    assigned_delivery_man_ids = fields.Many2one('delivery.man.info', string='Assigned Delivery Man')




