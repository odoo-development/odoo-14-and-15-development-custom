{
    'name': 'Delivery APP',
    'version': '14.0.1',
    'summary': 'Insert data from new from from app on a new table and from  and store the delivery boys data.',
    'category': 'Inventory',
    'author': 'Ashif Raihun',
    'website': 'https://www.sindabad.com',
    'depends': ['base', 'wms_manifest', 'wms_manifest_inherit'
    ],
    'data': [
        'security/delivery_app_security.xml',
        'security/ir.model.access.csv',
        'views/wms_manifest_process_inherit_view.xml',
        'views/delivery_man_info_form.xml',
    ],
    'installable': True,
    'application': False,
    'auto_install': False,
}
