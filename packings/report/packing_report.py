from odoo import api, models, _
from datetime import datetime


class PackingReport(models.AbstractModel):
    _name = "report.packings.report_packinglayout"
    _description = 'Packing Report'

    @api.model
    def _get_report_values(self, docids, data=None):
        docs = self.env['packing.list'].browse(docids[0])

        return {
            'doc_ids': docids,
            'doc_model': 'packing.list',
            'docs': docs,
            'data': data,

            # "get_magento_order_numbers": self.get_magento_order_numbers,
            # "get_customer_name": self.get_customer_name,
            "get_product_list": self.get_product_list,

            'get_client_invoice_address': self.get_client_invoice_address,
            'get_client_shipping_address': self.get_client_shipping_address,

            'get_purchase_ref': self.get_purchase_ref,
            'get_payment_method': self.get_payment_method,
        }

    def get_magento_order_numbers(self, obj):

        stock_packing_ids = [it.stock_id for it in obj.packing_line]
        magento_numbers = ""

        for items in stock_packing_ids:
            if items.mag_no:
                magento_numbers = magento_numbers + str(items.mag_no) + ", "

        return magento_numbers

    def get_customer_name(self, obj):

        stock_picking = self.pool.get("stock.picking").browse(
            obj._cr, obj._uid, [int(obj.picking_id)], context=obj._context
        )

        customer_name = str(stock_picking.partner_id.name)

        return customer_name

    def get_product_list(self, obj):

        stock_packing_ids = [it.stock_id for it in obj.packing_line]
        total_moveline_list = [items.move_lines for items in stock_packing_ids]
        product_id_lists = []
        moves_list = []
        for items in total_moveline_list:
            for att in items:
                product_id_lists.append(att.product_id.id)
                moves_list.append(att)

        return moves_list

    def get_client_invoice_address(self, obj):

        order_numbers = [str(obj)]

        partner_invoice_id_query = "SELECT partner_invoice_id FROM sale_order WHERE name='{0}'".format(order_numbers[0])
        self.env.cr.execute(partner_invoice_id_query)
        partner_invoice_id = 0
        for item in self.env.cr.fetchall():
            partner_invoice_id = int(item[0])

        res_partner_obj = self.env['res.partner']
        res_partner = res_partner_obj.browse([partner_invoice_id])

        return res_partner if res_partner else '-'

    def get_client_shipping_address(self, obj):

        order_numbers = [str(obj)]

        partner_shipping_id_query = "SELECT partner_shipping_id FROM sale_order WHERE name='{0}'".format(
            order_numbers[0])
        self.env.cr.execute(partner_shipping_id_query)
        for item in self.env.cr.fetchall():
            partner_shipping_id = int(item[0])

        res_partner_obj = self.env['res.partner']
        res_partner = res_partner_obj.browse([partner_shipping_id])

        return res_partner if res_partner else '-'

    def get_purchase_ref(self, obj):
        order_numbers = [str(obj)]
        purchase_ref_cus = None
        self.env.cr.execute("SELECT x_puchase_ref_cus FROM sale_order WHERE name=%s", (order_numbers))
        for item in self.env.cr.fetchall():
            purchase_ref_cus = item[0]

        return purchase_ref_cus if purchase_ref_cus else '---'

    def get_payment_method(self, obj):

        order_numbers = [str(obj)]
        so_obj = self.env['sale.order'].browse(order_numbers[0])

        return so_obj
