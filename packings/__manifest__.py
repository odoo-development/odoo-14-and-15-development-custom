{
    "name": "Packing List Generation",
    "version": "14.0.1.0.0",
    "category": "Sales",
    "description": """
Generate Packing List from Delivery Challan / Sales Order   
==============================================================

""",
    "author": "Odoo Bangladesh",
    "depends": ["stock",
                'account',
                'wms_base',
                ],
    "data": [
        "wizard/stock_packing_select_view.xml",
        "security/packing_security.xml",
        "security/ir.model.access.csv",
        "views/packing_view.xml",
        "report/report_packing.xml",
        "views/packing.xml",
    ],
    "installable": True,
    "auto_install": False,
}
