from datetime import date

from odoo import models, fields


class SaleMakeInvoice(models.TransientModel):
    _name = "packing.list.multiple"
    _description = "Packing List"

    packing_date = fields.Datetime('Date', required=True, readonly=True, index=True, copy=False,
                                   default=fields.Datetime.now())
    description = fields.Char('Reference/Description')
    # Commented by Matiar Rahman
    # create_date = fields.Datetime("Creation Date", readonly=True, index=True,
    #                               help="Date on which sales order is created."),
    date_confirm = fields.Datetime("Confirmation Date", readonly=True, index=True,
                                   help="Date on which sales order is confirmed.", copy=False),
    user_id = fields.Many2one("res.users", "Assigned to", index=True, tracking=True, default=lambda self: self.env.user)
    packing_line = fields.One2many("packing.list.line", "packing_id", "Packing List Lines", required=True)

    ### Block for V-14
    # _columns = {
    #     "packing_date": fields.datetime("Date", required=True, readonly=True, index=True, copy=False),
    #     "description": fields.char("Reference/Description"),
    #     "create_date": fields.datetime("Creation Date", readonly=True, index=True,
    #                                    help="Date on which sales order is created."),
    #     "date_confirm": fields.date("Confirmation Date", readonly=True, index=True,
    #                                 help="Date on which sales order is confirmed.", copy=False),
    #     "user_id": fields.many2one("res.users", "Assigned to", index=True, track_visibility="onchange"),
    #     "packing_line": fields.one2many("packing.list.line", "packing_id", "Packing List Lines", required=True),
    # }
    #
    # _defaults = {
    #     "packing_date": fields.datetime.now,
    #     "user_id": lambda obj, cr, uid, context: uid,
    # }

    def make_packing(self, context=None):
        packing_obj = self.env["packing.list"]

        if context is None:
            context = {}
        get_data = self.read(ids)[0]
        data = {}
        packing_line = []
        today_date = date.today()

        data["description"] = get_data.get("description")

        for items in context.get("active_ids"):
            packing_line.append(
                [
                    0,
                    False,
                    {
                        "stock_id": items,
                        "receive_date": today_date.strftime("%Y-%m-%d"),
                        "details": False,
                        "state": "pending",
                        "return_date": False,
                    },
                ]
            )

        data["packing_line"] = packing_line

        save_the_data = packing_obj.create(data, context=context)

        return save_the_data
