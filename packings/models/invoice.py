from odoo import fields, models


class AccountMove(models.Model):
    _inherit = "account.move"

    pack_id = fields.Many2one(
        "packing.list",
        string="Packing",
        required=False,
        tracking=True
    )
    from_packed = fields.Boolean(string="Packed")
