from odoo import models, fields, api, _
from odoo.tools.translate import _
from odoo.exceptions import UserError, ValidationError, Warning
from odoo.osv import osv


class StockPacking(models.Model):
    _inherit = "stock.picking"

    packed = fields.Boolean('Packed')


class PackingList(models.Model):
    _name = "packing.list"
    _description = "Packing List"

    packing_date = fields.Datetime('Date', default=fields.Datetime.now())
    description = fields.Char('Packer Name')
    picking_id = fields.Char('Picking', required=False)
    create_date = fields.Datetime('Creation Date', readonly=True, index=True,
                                  help="Date on which sales order is created.", )
    date_confirm = fields.Datetime("Confirmation Date", readonly=True, index=True,
                                   help="Date on which sales order is confirmed.", copy=False, )
    user_id = fields.Many2one("res.users", "Assigned to", index=True, tracking=True, default=lambda self: self.env.user)
    packing_line = fields.One2many("packing.list.line", "packing_id", "Packing List Lines", required=True, )
    generate_invoice = fields.Boolean('Generate Invoice', default=False, help="Generate Invoice button hide after action")

    state = fields.Selection([
        ("pending", "Waiting for Packing"),
        ("done", "Done"),
        ("cancel", "Cancelled"),
    ], string="State", default='pending', readonly=True, copy=False,
        help="Gives the status of the quotation or sales order", index=True, )

    def create_invoice(self):
        for pl in self:
            packing_id = pl.id
            for picking_id in pl.packing_line:
                order_id = picking_id.magento_no
                sale_object = self.env["sale.order"].search([("client_order_ref", "=", order_id)], limit=1)

                if sale_object and sale_object.invoice_status == 'to invoice':
                    invoice = sale_object._create_invoices(final=True)
                    invoice.action_post() if invoice else None
                    picking_id.write({
                        'invoice_id': invoice.id,
                        # 'invoice_id': invoice.display_name,
                    })

                    try:
                        data = {
                            'odoo_order_id': sale_object.id,
                            'magento_id': str(order_id),
                            'order_state': str('invoiced'),
                            'state_time': fields.datetime.now(),
                        }
                        new_r = self.env["order.status.synch.log"].sudo().create(data)
                    except:
                        pass

                pl.write({'generate_invoice': False})

        return True

    def test_create_invoice(self):
        context = dict(self.env.context or {})
        picking_pool = self.env["stock.picking"]
        data = self.browse(self.ids[0])
        journal2type = {
            "sale": "out_invoice",
            "purchase": "in_invoice",
            "sale_refund": "out_refund",
            "purchase_refund": "in_refund",
        }
        context["date_inv"] = data.invoice_date
        acc_journal = self.env["account.journal"]
        inv_type = journal2type.get(data.journal_type) or "out_invoice"
        context["inv_type"] = inv_type

        active_ids = context.get("active_ids", [])
        res = picking_pool.action_invoice_create(
            active_ids,
            journal_id=data.journal_id.id,
            group=data.group,
            type=inv_type,)
        return res

    def make_confirm(self):
        self.write({
            'state': 'done',
            'date_confirm': fields.Datetime.now(),
            'generate_invoice': True,
        })

        self.packing_line.write({'state': 'done'})

        for pl in self:
            for pl_line in pl.packing_line:
                pl_line.stock_id.write({'packed': True})

    def make_cancel(self):
        id_list = self.ids
        stock_packing_ids = []
        for element in id_list:
            ids = [element]

            if ids is not None:
                self.env.cr.execute(
                    "update packing_list set state='cancel' where id=%s", (ids)
                )
                self.env.cr.execute(
                    "update packing_list_line set state='cancel' where packing_id=%s",
                    (ids),
                )

            self.env.cr.execute(
                "select stock_id from  packing_list_line where packing_id=%s",
                (ids),
            )

            for item in self.env.cr.fetchall():
                for tuple_items in list(item):
                    stock_packing_ids.append(tuple_items)

        for items in stock_packing_ids:
            self.env.cr.execute(
                "update stock_picking set packed='False' where id=%s",
                ([items]),
            )

        return "True"

    def create(self, vals):
        # context = dict(context or {})

        stock_packing_list = []
        if vals.get("packing_line"):
            packing_line = []
            for items in vals.get("packing_line"):
                att_data = {}
                for att_data in items:
                    if type(att_data) is dict:
                        stock_id = att_data.get("stock_id")
                        stock_packing_list.append(stock_id)
                        stock_obj = self.env["stock.picking"].browse(stock_id)

                        att_data["magento_no"] = stock_obj.mag_no
                        att_data["order_number"] = stock_obj.origin
                packing_line.append([0, False, att_data])

            vals["packing_line"] = packing_line

        if stock_packing_list:
            abc = []
            for items in stock_packing_list:
                self.env.cr.execute(
                    "select packing_id from  packing_list_line where state !='cancel' and stock_id=%s",
                    ([items]),
                )

                abc += self.env.cr.fetchall()

            if len(abc) > 0:
                raise Warning(_("Already Packed the numbers"))

        packing_id = super(PackingList, self).create(vals)

        if packing_id:
            for items in vals["packing_line"]:
                for att in items:
                    if isinstance(att, dict):
                        self.env.cr.execute(
                            "update stock_picking set packed='True' where id=%s",
                            (att.get("stock_id"),),
                        )

        return packing_id

    # def write(self, vals):
    #     if isinstance(self.ids, (int, self.long)):
    #         ids = [self.ids]
    #     res = super(PackingList, self).write(ids, vals)
    #     return res

    ####  Block for V-14
    # _columns = {
    #     "packing_date": fields.datetime(
    #         "Date", required=True, readonly=True, index=True, copy=False
    #     ),
    #     "description": fields.char("Packer Name"),
    #     "picking_id": fields.char("Picking", required=False),
    #     "create_date": fields.datetime(
    #         "Creation Date",
    #         readonly=True,
    #         index=True,
    #         help="Date on which sales order is created.",
    #     ),
    #     "date_confirm": fields.datetime(
    #         "Confirmation Date",
    #         readonly=True,
    #         index=True,
    #         help="Date on which sales order is confirmed.",
    #         copy=False,
    #     ),
    #     "user_id": fields.many2one(
    #         "res.users",
    #         "Assigned to",
    #         index=True,
    #         track_visibility="onchange",
    #     ),
    #     "packing_line": fields.one2many(
    #         "packing.list.line",
    #         "packing_id",
    #         "Packing List Lines",
    #         required=True,
    #     ),
    #     "state": fields.selection(
    #         [
    #             ("pending", "Waiting for Packing"),
    #             ("done", "Done"),
    #             ("cancel", "Cancelled"),
    #         ],
    #         "Status",
    #         readonly=True,
    #         copy=False,
    #         help="Gives the status of the quotation or sales order",
    #         index=True,
    #     ),
    # }
    #
    # _defaults = {
    #     "packing_date": fields.datetime.now,
    #     "user_id": lambda obj, cr, uid, context: uid,
    #     "state": "pending",
    # }


class PackingListLine(models.Model):
    _name = "packing.list.line"
    _description = "Packing Line List"

    packing_id = fields.Many2one("packing.list", "Order Reference", required=True, ondelete="cascade", index=True,
                                 readonly=True)
    stock_id = fields.Many2one("stock.picking", string="Stock Packing")
    invoice_id = fields.Many2one("account.move", string="Invoice")
    # invoice_id = fields.Char(string="Invoice")
    sequence = fields.Integer("Sequence", help="Gives the sequence order when displaying a list of sales order lines."),
    details = fields.Char("Details")
    receive_date = fields.Date("Receive Date", required=True, default=fields.Datetime.now())
    return_date = fields.Date("Return Date")
    order_number = fields.Char("Order Number")
    magento_no = fields.Char("Magento No")

    state = fields.Selection([
        ("pending", "Waiting for Packing"),
        ("done", "Done"),
        ("cancel", "Cancelled"),
    ], string="State", default='pending', readonly=True, copy=False,
        help="Gives the status of the quotation or sales order", index=True, )

    ### Block for V-14
    # _columns = {
    #     "packing_id": fields.many2one(
    #         "packing.list",
    #         "Order Reference",
    #         required=True,
    #         ondelete="cascade",
    #         index=True,
    #         readonly=True,
    #     ),
    #     "stock_id": fields.many2one("stock.picking", string="Stock Packing"),
    #     "invoice_id": fields.many2one("account.invoice", string="Invoice"),
    #     "sequence": fields.integer(
    #         "Sequence",
    #         help="Gives the sequence order when displaying a list of sales order lines.",
    #     ),
    #     "details": fields.char("Details"),
    #     "receive_date": fields.date("Receive Date", required=True),
    #     "return_date": fields.date("Return Date"),
    #     "order_number": fields.char("Order Number"),
    #     "magento_no": fields.char("Magento No"),
    #     "state": fields.selection(
    #         [
    #             ("pending", "Waiting for Packing"),
    #             ("done", "Done"),
    #             ("cancel", "Cancelled"),
    #         ],
    #         "Status",
    #         readonly=True,
    #         copy=False,
    #         help="Gives the status of the quotation or sales order",
    #         index=True,
    #     ),
    # }
    #
    # _defaults = {
    #     "receive_date": fields.datetime.now,
    #     "state": "pending",
    # }
