from odoo import api, fields, models, _
from odoo.tools.translate import _
from odoo import api
from odoo.exceptions import except_orm, Warning, RedirectWarning, UserError


class PaymentPlan(models.Model):
    _name = 'payment.plan'
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _description = 'Payment Plan Process'

    name = fields.Char('Name')
    sequence = fields.Integer(string='Sequence')

    date = fields.Datetime(string="Date", default=fields.Datetime.now, required=True, copy=False, store=True)
    note = fields.Text(string="Note", copy=False, store=True)
    state = fields.Selection([
        ('draft', 'Pending'),
        ('request', 'Requested'),
        ('approved', 'Approved'),
        ('reject', 'Reject'),
    ], string='Status', readonly=True, copy=False, index=True, tracking=True, default='draft', store=True)

    request_date = fields.Datetime('Request Date', tracking=True, copy=False, store=True)
    request_by = fields.Many2one('res.users', 'Request By', tracking=True, copy=False, store=True)
    approve_date = fields.Datetime('Approve Date', tracking=True, copy=False, store=True)
    approve_by = fields.Many2one('res.users', 'Approve By', tracking=True, copy=False, store=True)
    reject_date = fields.Datetime('Rejected Date', tracking=True, copy=False, store=True)
    reject_by = fields.Many2one('res.users', 'Rejected By', tracking=True, copy=False, store=True)

    b2c_collection = fields.Float(String='B2C Collection', copy=False, store=True)
    b2r_collection = fields.Float(String='B2R Collection', copy=False, store=True)
    b2e_collection = fields.Float(String='B2E Collection', copy=False, store=True)
    horeca_collection = fields.Float(String='Horeca Collection', copy=False, store=True)

    total_disbursement = fields.Float(compute='_get_total_disbursement', string='Total Disbursement')
    total_collection = fields.Float(compute='_get_total_collection', string='Total Collection')

    pp_line = fields.One2many('payment.plan.line', 'pp_line_id', string="Payment Plan Line", required=True, copy=False)

    @api.model
    def create(self, vals):
        if vals.get("pp_line", False):
            for items in vals.get('pp_line'):
                if items[2].get('amount_due') < items[2]['proposal_amount']:
                    raise Warning(_("Requested proposal amount can not greater than amount due."))

        record = super(PaymentPlan, self).create(vals)
        pp_name = self.env['ir.sequence'].next_by_code('payment.plan')
        record.name = pp_name

        return record

    def write(self, vals):
        update_permission = list()
        if vals.get("pp_line", False):
            for single_line in vals.get('pp_line'):
                if not not single_line[2]:
                    for req_l in self.pp_line:
                        if req_l.id == single_line[1]:
                            req_proposal_amount = single_line[2]['proposal_amount'] if single_line[2].get('proposal_amount') else req_l.proposal_amount
                            req_tds_amount = single_line[2]['tds'] if single_line[2].get('tds') else req_l.tds
                            req_vat_amount = single_line[2]['vat'] if single_line[2].get('vat') else req_l.vat
                            req_approve_amount = single_line[2]['approve_amount'] if single_line[2].get('approve_amount') else req_l.approve_amount

                            if req_tds_amount > req_proposal_amount:
                                raise Warning(_("TDS Amount can not greater than Proposal Amount."))

                            elif req_vat_amount > req_proposal_amount:
                                raise Warning(_("VDS Amount can not greater than Proposal Amount."))

                            elif req_approve_amount > req_proposal_amount:
                                raise Warning(_("Approve Amount can not greater than Proposal Amount."))

                            elif req_proposal_amount > req_l.amount_due:
                                update_permission.append(False)
                            else:
                                update_permission.append(True)
                        else:
                            pass

        if False in update_permission:
            raise Warning(_("Requested proposal amount can not greater than amount due."))
        else:
            record = super(PaymentPlan, self).write(vals)
            return record

    def pp_request(self):
        self.write({
            'state': 'request',
            'request_by': self.env.uid,
            'request_date': fields.datetime.now(),
        })
        self.pp_line.write({
            'state': 'request',
            'request_by': self.env.uid,
            'request_date': fields.datetime.now(),
        })

    def pp_approved(self):
        self.write({
            'state': 'approved',
            'approve_by': self.env.uid,
            'approve_date': fields.datetime.now(),
        })
        self.pp_line.write({
            'state': 'approved',
            'approve_by': self.env.uid,
            'approve_date': fields.datetime.now(),
        })

        sum_line = float(0)
        for line in self.pp_line:
            sum_line = sum_line+line.approve_amount
        if sum_line == float(0):
            raise Warning(_('Approve amount should not be zero!!!'))

    def pp_reject(self):
        self.write({
            'state': 'reject',
            'reject_by': self.env.uid,
            'reject_date': fields.datetime.now(),
        })
        self.pp_line.write({
            'state': 'approved',
            'approve_by': self.env.uid,
            'approve_date': fields.datetime.now(),
        })

    def as_it_is(self):
        for line in self.pp_line:
            line.approve_amount = line.proposal_amount

    def _get_total_disbursement(self):

        for record in self:
            total_amount = 0

            for pp in record.pp_line:
                total_amount = total_amount + pp.proposal_amount

            record.total_disbursement = total_amount

    @api.onchange('b2c_collection', 'b2r_collection', 'b2e_collection', 'horeca_collection')
    def _get_total_collection(self):

        for record in self:
            record.total_collection = float(record.b2c_collection) + float(record.b2r_collection) + float(record.b2e_collection) + float(record.horeca_collection)



class PaymentPlanLine(models.Model):
    _name = "payment.plan.line"
    _description = "Payment Plan Line"

    pp_line_id = fields.Many2one('payment.plan', 'Payment Plan ID', required=True, ondelete='cascade', index=True)
    
    po_id = fields.Many2one('purchase.order', string="PO Number", copy=False, store=True)
    partner_id = fields.Many2one('res.partner', string="Company", domain=[('is_supplier', '=', True)], copy=False, store=True)
    receivable_amount = fields.Float(string="Receivable Amount", copy=False, store=True)
    payable_amount = fields.Float(string="Payable Amount", copy=False, store=True)

    po_date = fields.Datetime(string="PO Date", copy=False, store=True)
    po_amount = fields.Float(string="PO Amount", copy=False, store=True)

    bill_amount = fields.Float(String='Bill Amount', copy=False, store=True)
    amount_due = fields.Float(String='Amount Due', copy=False, store=True)
    paid_amount = fields.Float(String='Paid Amount', copy=False, store=True)

    tds = fields.Float(String='TDS', copy=False, store=True)
    vat = fields.Float(String='VAT', copy=False, store=True)

    payment_status = fields.Char(String='Payment Status', copy=False, store=True)
    credit_days = fields.Integer(String='Credit Days', copy=False, store=True)
    classification_type = fields.Char(String='Classification', copy=False, store=True)

    net_payable = fields.Float(string="Net Payable", copy=False, store=True)
    proposal_amount = fields.Float(string="Proposal Amount", copy=False, store=True)
    approve_amount = fields.Float(string="Approve Amount", copy=False, store=True)
    proposal_approve = fields.Boolean(string="Proposal Approve", default=False, copy=False, store=True)

    remark = fields.Char(string="Remark", copy=False, store=True)

    

    state = fields.Selection([
        ('draft', 'Pending'),
        ('request', 'Requested'),
        ('approved', 'Approved'),
        ('reject', 'Reject'),
    ], string='Status', readonly=True, copy=False, index=True, tracking=True, default='draft', store=True)

    request_date = fields.Datetime('Request Date', tracking=True, copy=False, store=True)
    request_by = fields.Many2one('res.users', 'Request By', tracking=True, copy=False, store=True)
    approve_date = fields.Datetime('Approve Date', tracking=True, copy=False, store=True)
    approve_by = fields.Many2one('res.users', 'Approve By', tracking=True, copy=False, store=True)
    reject_date = fields.Datetime('Rejected Date', tracking=True, copy=False, store=True)
    reject_by = fields.Many2one('res.users', 'Rejected By', tracking=True, copy=False, store=True)

    @api.onchange('po_id')
    def po_details(self):
        po_id = self.po_id
        if po_id:
            if po_id.amount_total == po_id.po_paid_amount:
                raise Warning(_('This PO is already paid. Please Chcek details!!!'))
            elif po_id.amount_total < po_id.po_paid_amount:
                raise Warning(_('This PO amount has some problem. Please Chcek details!!!'))
            else:
                self.partner_id = po_id.partner_id.id
                self.receivable_amount = po_id.partner_id.credit
                self.payable_amount = po_id.partner_id.debit
                self.po_date = po_id.date_order if po_id.date_order else None
                self.po_amount = po_id.amount_total if po_id.amount_total else None
                self.payment_status = po_id.adv_po_snd.upper()
                self.credit_days = po_id.x_credit_days
                self.classification_type = po_id.x_sale_classification_type.upper()
                po_amount_due = po_id.amount_total - po_id.po_paid_amount
                self.amount_due = po_amount_due if po_amount_due else None
                self.paid_amount = po_id.po_paid_amount if po_id.po_paid_amount else None

    @api.onchange('proposal_amount', 'tds', 'vat')
    def onchange_net_payable(self):
        values = {}
        if self.proposal_amount or self.tds or self.vat:
            values['net_payable'] = float(self.proposal_amount) - float(self.tds) - float(self.vat)
        else:
            values['net_payable'] = 0.00
        return {'value': values}
    
    

