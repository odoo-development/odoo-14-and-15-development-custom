import itertools, collections

from odoo import api, models, _
from datetime import datetime


class PaymentPlanReport(models.AbstractModel):
    _name = 'report.payment_plan.report_payment_plan_summary_layout'
    _description = 'Payment Plan Summary Report'

    @api.model
    def _get_report_values(self, docids, data=None):
        docs = self.env['payment.plan'].browse(docids[0])

        return {
            'doc_ids': docids,
            'doc_model': 'payment.plan',
            'docs': docs,
            'data': data,
            'get_partner_data': self.get_partner_data,
        }

    def get_partner_data(self, obj):
        result = []

        objects_dict = {}
        emp_ids = []

        object = [{'partner_id': row.partner_id.id,
                   'proposal_amount': row.proposal_amount,
                   'tds': row.tds,
                   'vat': row.vat,
                   'net_payable': row.proposal_amount - row.tds - row.vat,
                   'classification_type': row.classification_type,
                   'remark': row.remark,
                   'payment_plan_id': row.id,
                   } for row in obj.pp_line]

        objects = sorted(object, key=lambda i: i['partner_id'])

        an_iterator = itertools.groupby(objects, lambda y: y['partner_id'])

        for key, group in an_iterator:
            key_and_group = {key: list(group)}
            objects_dict.update(key_and_group)
            emp_ids.append(key)

        common_dict = []
        for val in emp_ids:
            data = {}
            proposal_amount = 0
            tds = 0
            vat = 0
            net_payable = 0
            b2r_amount = 0
            b2c_amount = 0
            b2e_amount = 0
            horeca_amount = 0
            con_remark = ''

            for x in objects_dict[val]:

                res_partner_obj = self.env['res.partner']
                res_partner = res_partner_obj.browse([x['partner_id']])

                proposal_amount = float(proposal_amount) + float(x['proposal_amount'])
                tds = float(tds) + float(x['tds'])
                vat = float(vat) + float(x['vat'])
                net_payable = float(net_payable) + float(x['net_payable'])

                if x['classification_type'] == 'b2r':
                    b2r_amount = float(b2r_amount) + float(x['proposal_amount'])
                if x['classification_type'] == 'b2c':
                    b2c_amount = float(b2c_amount) + float(x['proposal_amount'])
                if x['classification_type'] == 'b2e':
                    b2e_amount = float(b2e_amount) + float(x['proposal_amount'])
                if x['classification_type'] == 'b2re':
                    horeca_amount = float(horeca_amount) + float(x['proposal_amount'])

                if x['remark']:
                    con_remark = con_remark + str(x['remark']) + ', '
                else:
                    con_remark = ''

                data.update({
                    'partner_id': res_partner.name,
                    'proposal_amount': proposal_amount,
                    'tds': tds,
                    'vat': vat,
                    'net_payable': net_payable,
                    'classification_type': x['classification_type'],
                    'remark': str(con_remark),
                    'b2r': b2r_amount,
                    'b2c': b2c_amount,
                    'b2e': b2e_amount,
                    'b2re': horeca_amount,
                })

            result.append(data)

        return result
