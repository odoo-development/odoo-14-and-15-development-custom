#  auto confirm sync
import datetime
import json
import logging
from datetime import datetime, date
import requests
from datetime import datetime, timedelta, date
import dateutil.parser
from odoo import api, fields, models, _
from ...odoo_to_magento_api_connect.api_connect import get_magento_token


class auto_confirmation(models.Model):
    _name = "auto.confirmation"

    name = fields.Char(String='Name')

    def auto_confirmation(self):
        SUPERUSER_ID = 1
        sales_order = self.env['sale.order']
        sale_ids = sales_order.search([('state', '=', 'draft'),('amount_total', '>', 0)], limit=50)
        for so_items in sale_ids:

            classification = so_items.customer_classification
            order_amount = so_items.amount_total
            payment_type = so_items.note
            order_id = so_items.id
            # next_date = dateutil.parser.parse(so_items.create_date).date() + timedelta(days=1)
            zone = so_items.so_inside_outside
            shipping_state_name = so_items.partner_shipping_id.state_id.name
            shipping_state = shipping_state_name.replace('-',' ')
            state_name = shipping_state.split()
            magento_amount = so_items.x_sin_total_magento
            gap_amount = round((magento_amount - order_amount), 2)
            # b2c_auto_confirm_area_list = [
            #     'Banani','Badda','Bashundhara','Baridhara','Gulshan','Mirpur','Mohakhali',
            #     'Uttara','Baridhara Dohs','Dakshinkhan','Kuril','Mirpur', 'Nikunja','Uttar Khan','Tejgaon','Vatara',
            #     'Shewrapara','Satarkul','Notun Bazar','Dhaur','Dhanmondi','Mohammadpur','Banasree','Tejgaon',
            #     'Aftabnagar','Rampura','Malibag','Moghbazar','New Market'
            # ]

            # b2c_auto_confirm_area = [confirm_area.upper() for confirm_area in b2c_auto_confirm_area_list]

            try:
                if 'Retail' in classification or 'SME' in classification or 'SOHO' in classification or \
                                'HORECA' in classification:
                    if 'Grocery' not in classification :
                        if 'Cash' in payment_type:
                            if order_amount > 900:
                                if gap_amount != 0:
                                    so_items.order_re_sync()
                                so_items.action_confirm()
                                # update_so_query = "UPDATE sale_order SET magetno_delivery_at='{0}' WHERE id='{1}'".format(next_date, str(order_id))
                                # self.env.cr.execute(update_so_query)
                                # self.env.cr.commit()

                # Call Order confirmation
                # if 'General' in classification or classification == '' or classification is None or 'Grocery' in classification:
                    # if zone == 'inside' and 'Cash' in payment_type and state_name[0].upper() in b2c_auto_confirm_area:
                if 'General' in classification or classification == '' or classification is None:
                    if zone == 'inside' and 'Cash' in payment_type :
                        if order_amount < 10000:
                            if gap_amount != 0:
                                so_items.order_re_sync()
                            so_items.action_confirm()
                            # update_so_query = "UPDATE sale_order SET magetno_delivery_at='{0}' WHERE id='{1}'".format(next_date, str(order_id))
                            # self.env.cr.execute(update_so_query)
                            # self.env.cr.commit()
            except:
                pass
        return True
