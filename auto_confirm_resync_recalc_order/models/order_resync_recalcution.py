#  auto confirm sync
import datetime
import json
import logging
from datetime import datetime, date
import requests
from datetime import datetime, timedelta, date
import dateutil.parser
from odoo import api, fields, models, _
from ...odoo_to_magento_api_connect.api_connect import get_magento_token


class SaleOrder(models.Model):
    _inherit = 'sale.order'

    re_sync = fields.Boolean(string="Re Sync")
    product_missing_ids = fields.One2many('product.missing', 'order_id', 'Product Missing IDs')

    def order_re_sync(self):
        so_obj = self.env['sale.order']
        order_id = self.id
        # so = so_obj.browse(order_id)
        ### requesting from magento
        if order_id:
            try:
                token, url_root = get_magento_token(self, [order_id])
            except:
                token = None
                url_root = None
        if token and url_root:
            token = token.replace('"', "")
            headers = {'Authorization': token, 'Content-Type': 'application/json'}

            get_url = url_root + "/rest/V1/odoomagentoconnect/GetOrderDetails/" + str(self.client_order_ref)
            get_resp = requests.get(get_url, headers=headers)
            order_data = json.loads(get_resp.text)

            # mag_no = order_data["increment_id"]
            mag_total = order_data["grand_total"]
            mag_items = order_data["items"]

            so_amount_total = self.amount_total
            if so_amount_total != mag_total:

                # email_template_obj = self.env['email.template']
                # template_ids = email_template_obj.search([('name', '=', 'Unsync Product')])
                prod_obj = self.env['product.product']
                # i = 0
                prod_miss_list = []
                for counter in range(5):
                    odoo_line_item_list = []
                    missing_item_list = []
                    item_list = []
                    ### odoo order product item line
                    for line in self.order_line:
                        odoo_line_item_list.append(str(line.default_code))

                    ### magento order product item line
                    for prod_list in mag_items:
                        if str(prod_list.get('sku')) not in odoo_line_item_list:
                            tmp_dict = {}
                            tmp_dict[str(prod_list.get('sku'))] = prod_list
                            missing_item_list.append(tmp_dict)
                        else:
                            tmp_dict = {}
                            tmp_dict[str(prod_list.get('sku'))] = prod_list
                            item_list.append(tmp_dict)
                    # vora
                    if missing_item_list:
                        for mag_item in missing_item_list:
                            sku = list(mag_item.keys())[0]
                            prod_search = prod_obj.search([('default_code', '=', str(sku))])
                            if not prod_search:
                                ## email###
                                uid = 1
                                # try to sync product from magento --- start

                                post_url = url_root + "/rest/V1/odoomagentoconnect/productSyncBulk/"
                                productIds = [mag_item[sku]['product_id']]
                                miss_prods = json.dumps({"productIds": productIds})

                                miss_prod_resp = requests.post(post_url, data=miss_prods, headers=headers)
                                miss_prod_data = json.loads(miss_prod_resp.text)
                                miss_prod_dict = dict()

                                for m_p in miss_prod_data:
                                    miss_prod_dict[m_p[list(m_p.keys())[0]]['sku']] = m_p[list(m_p.keys())[0]][
                                        'success']

                            else:
                                sol_obj = self.env['sale.order.line']

                                x_discount = (mag_item[sku]['discount_amount']) / (mag_item[sku]['qty_ordered'])
                                price_unit = (mag_item[sku]['price']) - x_discount

                                sol_data = {
                                    'product_id': prod_search.id,
                                    'state': 'draft',
                                    'name': mag_item[sku]['name'],
                                    'default_code': mag_item[sku]['sku'],
                                    'price_unit': price_unit,
                                    'product_uom_qty': mag_item[sku]['qty_ordered'],
                                    'order_id': self.id,
                                    'x_discount': x_discount,
                                    # 'total_discount': mag_item[sku]['discount_amount'],
                                    'x_original_price_unit': mag_item[sku]['price'],
                                }

                                sol_obj.create(sol_data)

                    else:
                        self.action_re_calculate_so_amount()

                    so_amount_total = so_obj.browse(order_id)

                    if so_amount_total.amount_total == mag_total:
                        re_sync_update_query = "UPDATE sale_order SET re_sync= TRUE WHERE id={0}".format(
                            so_amount_total.id)
                        self.env.cr.execute(re_sync_update_query)
                        self.env.cr.commit()

                        break

                for pm in prod_miss_list:

                    pm_obj = self.env['product.missing']

                    for item in missing_item_list:

                        if str(list(item.keys())[0]) == str(pm):
                            sku = list(item.keys())[0]

                            pm_data = {
                                'order_id': order_id,
                                'miss_name': item[sku]['name'],
                                'miss_sku': str(pm),
                                'miss_qty': item[sku]['qty_ordered'],
                            }

                            pm_obj.create(pm_data)

                ########## Email and warning #######
                # if prod_miss_list:
                #
                #     email_template_obj.send_mail(cr, uid, template_ids[0], ids[0], force_send=True, context=context)
                #
                #     raise osv.except_osv(_('Warning!!!'), _(
                #     'This products are not enlisted in product table. Please this product sync manually \'%s\'.') % str(prod_miss_list))

                ########## End ###############

                return {
                    'type': 'ir.actions.client',
                    'tag': 'reload',
                }


            else:

                re_sync_update_query = "UPDATE sale_order SET re_sync= TRUE WHERE id={0}".format(self.id)
                self.env.cr.execute(re_sync_update_query)
                self.env.cr.commit()

                # return {
                #     'type': 'ir.actions.client',
                #     'tag': 'reload',
                # }
                #
                # raise osv.except_osv(_('Successful!!!'), _('This order already sync successfully.'))

        return True

    def action_re_calculate_so_amount(self):
        # so_obj = self.env["sale.order"]
        so = self
        so_amount = so.amount_total
        magento_amount = so.x_sin_total_magento
        gap_amount = 0
        p_found = False
        already_adjusted_amount = 0
        line_id = 0
        gap_amount = round((magento_amount - so_amount), 2)
        for so_items in so.order_line:
            if so_items.product_id.name == '':
                p_found = True
                already_adjusted_amount = so_items.price_unit
                line_id = so_items.id
                break

        discount_product_query = "select id from product_product WHERE default_code='DS_007'"
        self.env.cr.execute(discount_product_query)
        product_objects = self.env.cr.dictfetchall()
        discount_id = product_objects[0].get('id')

        if p_found == False and gap_amount != 0:
            vals = {'order_line':
                [
                    [0, False,
                     {'product_id': discount_id, 'product_uom': 1,
                      'discount': 0, 'price_unit': gap_amount, 'product_uom_qty': 1,
                      'name': 'Discount Sales'}]

                ]
            }
            record = super(SaleOrder, self).write(vals)
        elif p_found == True and gap_amount != 0 and already_adjusted_amount != gap_amount:

            vals = {'order_line': [[1, line_id, {'price_unit': gap_amount}]]}
            record = super(SaleOrder, self).write(vals)
        return True


class ProductMissing(models.Model):
    _name = 'product.missing'

    order_id = fields.Many2one('sale.order', string="Order ID")
    miss_name = fields.Char(string="Product Name")
    miss_sku = fields.Char(string="SKU")
    miss_qty = fields.Float(string="Quantity")
