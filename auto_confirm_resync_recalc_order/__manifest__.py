{
    'name': 'Auto confirmation, Re-Sync, Re-Calculation in SO',
    'version': '1.0',
    'category': 'WMS',
    'author': 'Rocky',
    'summary': 'Auto confirmation, Re-Sync, Re-Calculation in SO',
    'description': 'Custom Sync Module',
    'depends': ['odoo_magento_connect','sale', 'stock', 'product'],
    'data': [
        'security/ir.model.access.csv',
        'views/sale_order_view.xml',
        'views/auto_confirmation_scheduler_view.xml',
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}
