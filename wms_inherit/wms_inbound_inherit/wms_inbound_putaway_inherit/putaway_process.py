from odoo import fields, models
from odoo.exceptions import except_orm, Warning, RedirectWarning
from odoo.osv import osv

from odoo.tools.translate import _


class PutawayProcess(models.Model):
    _inherit = 'putaway.process'

    def put_confirm(self):
        self.ensure_one()
        put_data = self

        if put_data.state == 'confirmed':
            raise Warning(_('This Put Away already confirmed'))
        if put_data.state == 'cancel':
            raise Warning(_('This Put Away already Cancelled. You can not confirm it. Please create new one.'))

        for item in put_data.putaway_line_view:
            if len(item.location) < 2:
                raise Warning(_('You have to specify the location.'))

        product_dict = {}

        for item in put_data.putaway_line_view:
            product_dict[item.product_id] = [item.quantity, item.location]

        purchase_order = self.env['purchase.order'].search([('name', '=', put_data.aqc_id.po_number)], limit=1)
        picking_obj = purchase_order.picking_ids.filtered((lambda p: p.state == 'assigned'))[0]

        if picking_obj:
            for move in picking_obj.move_lines:
                for p in product_dict:
                    if move.product_id.id == p:
                        self.env['stock.move.line'].create({
                            'picking_id': move.picking_id.id,
                            'move_id': move.id,
                            'product_id': move.product_id.id,
                            'product_uom_id': move.product_uom.id,
                            'qty_done': product_dict[p][0],
                            'location_id': move.location_id.id,
                            'location_dest_id': move.location_dest_id.id
                        })
            picking_obj.with_context(cancel_backorder=False)._action_done()

            purchase_order.action_create_invoice()
            vendor_bill = purchase_order.invoice_ids.filtered(lambda o: o.state == 'draft')
            vendor_bill.invoice_date = fields.Datetime.now()
            vendor_bill.action_post()

            # for po in purchase_order:
            #     for picking in po.picking_ids:
            #         if picking.state in ['assigned', 'partially_available']:
            #             picking_ids.append(picking.id)

            # if len(picking_ids) > 0:
            #     picking = [picking_ids[0]]
            # changing invoice control of picking
            # picking_obj = self.env['stock.picking']
            # operation_id = picking_obj.browse(picking_ids[0])
            # picking_obj.write([operation_id.id], {'invoice_state': '2binvoiced'})

            # context.update({
            #
            #     'active_model': 'stock.picking',
            #     'active_ids': picking,
            #     'active_id': len(picking) and picking[0] or False
            # })
            # created_id = self.env['stock.transfer_details'].create({
            #     'picking_id': len(picking) and picking[0] or False})
            #
            # if created_id is not None:
            #     break

        # if created_id:
        #     created_id = self.env['stock.transfer_details'].browse(created_id)
        #     if created_id.picking_id.state in ['assigned', 'partially_available']:
        #         # raise Warning(_('You cannot transfer a picking in state \'%s\'.') % created_id.picking_id.state)
        #
        #         processed_ids = []
        #
        #         # Create new and update existing pack operations
        #         for lstits in [created_id.item_ids, created_id.packop_ids]:
        #             for prod in lstits:
        #                 if product_dict.get(prod.product_id.id):
        #
        #                     pack_datas = {
        #                         'product_id': prod.product_id.id,
        #                         'product_uom_id': prod.product_uom_id.id,
        #                         'product_qty': product_dict.get(prod.product_id.id)[0],
        #                         'package_id': prod.package_id.id,
        #                         'lot_id': prod.lot_id.id,
        #                         'location_id': prod.sourceloc_id.id,
        #                         'location_dest_id': prod.destinationloc_id.id,
        #                         'result_package_id': prod.result_package_id.id,
        #                         'date': prod.date if prod.date else datetime.now(),
        #                         'owner_id': prod.owner_id.id,
        #                         'product_location': product_dict.get(prod.product_id.id)[1]
        #                     }
        #
        #                     if prod.packop_id:
        #                         prod.packop_id.with_context(no_recompute=True).write(pack_datas)
        #                         processed_ids.append(prod.packop_id.id)
        #                     else:
        #                         pack_datas['picking_id'] = created_id.picking_id.id
        #                         packop_id = created_id.env['stock.pack.operation'].create(pack_datas)
        #                         processed_ids.append(packop_id.id)
        #         # Delete the others
        #         packops = created_id.env['stock.pack.operation'].search(
        #             ['&', ('picking_id', '=', created_id.picking_id.id), '!', ('id', 'in', processed_ids)])
        #         packops.unlink()
        #
        #         # Execute the transfer of the picking
        #         abc = created_id.picking_id.do_transfer()
        #
        #         ## Get quant IDs and assign the date id it exists
        #         quant_id_list = []
        #         exp_date = ''
        #         for put_item in put_data.putaway_line_view:
        #             exp_date = put_item.product_exp_date
        #             for move_item in created_id.picking_id.move_lines:
        #                 for moved_data in move_item.quant_ids:
        #                     if put_item.product_id == moved_data.product_id.id:
        #                         quant_id_list.append({'quant_id': moved_data.id,
        #                                               'exp_date': exp_date})
        #
        #         try:
        #             for q_list in quant_id_list:
        #                 update_expiry_date = "UPDATE stock_quant SET product_exp_date='{0}' WHERE id='{1}'".format(
        #                     q_list.get('exp_date'), q_list.get('quant_id'))
        #                 self.env.cr.execute(update_expiry_date)
        #                 self.env.cr.commit()
        #         except:
        #             pass
        #
        #         ## Ends Here
        #
        #         if created_id.picking_id.invoice_state != 'invoiced':
        #             # Now Create the Invoice for this picking
        #             picking_pool = self.env['stock.picking']
        #             from datetime import date
        #             context['date_inv'] = date.today()
        #             context['inv_type'] = 'in_invoice'
        #             active_ids = [picking[0]]
        #             res = picking_pool.action_invoice_create(active_ids,
        #                                                      journal_id=1135,
        #                                                      group=True,
        #                                                      type='in_invoice')
        #             if res:
        #                 invoice_data = self.env['account.move'].browse(res[0], context=context)[0]
        #
        #                 invoice_data.signal_workflow('invoice_open')

        self.write({
            'state': 'confirmed',
            'confirmation_date': fields.Datetime.now()
        })
        self.env['inb.end.to.end'].putaway_confirm_update(po_number=str(put_data.aqc_id.po_number),
                                                          po_id=put_data.aqc_id.po_id)
