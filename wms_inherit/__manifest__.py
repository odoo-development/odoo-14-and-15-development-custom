{
    'name': 'WMS Inherit',
    'version': "14.0.1.0.0",
    'category': 'WMS',
    'author': 'Odoo Bangladesh',
    'summary': 'WMS all process inherit here',
    'description': 'WMS all process inherit here',
    'depends': ['base', 'wms_inbound'],
    'data': [
        # 'wms_inbound_inherit/wms_inbound_aqc_inherit/report/report_sticker_printing_inherit.xml'
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}
