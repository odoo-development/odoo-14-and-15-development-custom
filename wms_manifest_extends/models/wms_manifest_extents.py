import datetime
import json
from datetime import date
from odoo.exceptions import UserError, ValidationError, Warning

from odoo import api
from odoo import fields, models
from odoo.tools.translate import _
from odoo.osv import osv


class WmsManifestOutbound(models.Model):
    _inherit = "wms.manifest.outbound"

    man_name = fields.Char('Manifest No')
    delivery_time = fields.Char('Delivery Time')


class WmsManifestOutboundLine(models.Model):
    _inherit = "wms.manifest.outbound.line"

    def _get_return_amount_fnc(self):
        res = {}

        """
        for partner in self.browse(cr, uid, ids, context=context):
            res[partner.id] = self._display_address(cr, uid, partner, context=context)
        """

        return res

    return_reason = fields.Char('Return Reason')


class WmsManifestProcess(models.Model):
    _inherit = "wms.manifest.process"

    name = fields.Char('Manifest No')
    assigned_user_ids = fields.Many2one('res.users', string='Assigned User')


    @api.model
    def create(self, vals):

        record = super(WmsManifestProcess, self).create(vals)
        name = "MANIFEST0" + str(record.id)
        record.name = name

        return record

    def create_delivery_from_manifest(self, invoice_ids):
        context = dict(self.env.context)
        inv_obj = self.env["account.move"]
        wms_man_out = self.env["wms.manifest.outbound"]

        # invoice_id = ids
        inv = inv_obj.browse(invoice_ids)

        name = self.env.context.get('name')  # Magento Ref
        man_name = self.env.context.get('man_name')  # Manifest No
        delivery_date = context['delivery_date']

        if inv.date > delivery_date:
            raise Warning(_('Delivery date can not less than invoice date!'))
        out_data = {
            'name': name,
            'man_name': man_name,
            'delivery_date': delivery_date,
            'delivery_time': self.env.context['delivery_time'],
            'state': 'confirm',
            'confirm': True,
            'confirm_by': self.env.uid,
            'source': self.env.context.get('source') if self.env.context.get('source', False) else 'Web',
            'delivery_done_by': self.env.uid,
            'delivery_done_datetime': fields.Date.today(),
        }

        outbound_line_list = list()

        for inv_line in inv.invoice_line_ids:
            if float(inv_line.quantity) > 0.00:
                outbound_line_list.append([0, False, {
                    'invoice_id': invoice_ids[0],
                    'magento_no': name,
                    'description': str(inv_line.name),
                    'product_id': inv_line.product_id.id,
                    'amount': float(inv_line.price_subtotal),
                    'quantity': float(inv_line.quantity),
                    'delivered': float(inv_line.quantity)
                }])

        out_data['outbound_line'] = outbound_line_list

        wms_man_out = wms_man_out.create(out_data)
        if wms_man_out:
            wms_man_out.write({'state': 'confirm', 'confirm': True})

    def process_confirm(self, data):

        magento_no = data['name']
        sales = self.env['sale.order'].search([('client_order_ref', '=', magento_no),('state', '!=','cancel')])

        if len(sales) > 0:
            # sales = self.env['sale.order'].browse(query_for_SO)[0]
            # sale_order = self.env["sale.order"]
            for so_item in sales:
                if so_item.state != 'cancel':
                    if so_item.delivered_amount == so_item.amount_total:
                        so_item.write({'invoiced_dispatch': True})
                        # dispatch_wms_query = "UPDATE sale_order SET invoiced_dispatch=TRUE  WHERE id='{1}'".format(uid,
                        #                                                                                            so_item.id)
                        # self.env.cr.execute(dispatch_wms_query)
                        # self.env.cr.commit()
                        # sale_order.action_status(cr, uid, [so_item.id, ], 'dispatched', context=context)
                    else:
                        so_item.write({'partially_invoiced_dispatch': True})
                        # dispatch_wms_query = "UPDATE sale_order SET partially_invoiced_dispatch=TRUE  WHERE id='{1}'".format(
                        #     so_item.id)
                        # self.env.cr.execute(dispatch_wms_query)
                        # self.env.cr.commit()
                        # sale_order.action_status(cr, uid, [so_item.id, ], 'partially_dispatched', context=context)
        # --------------------------------------------------

        # if the last invoice then set state confirm
        man_pro_obj = self.env["wms.manifest.line"]
        # inv_obj = self.env["account.move"]
        man_process_line = man_pro_obj.search([('invoice_id', '=', data['invoice_id']), ('reschedule', '=', False)])
        # man_process_line = man_pro_obj.browse(man_pro_line_list)

        man_process = man_process_line.wms_manifest_id

        if man_process.id is False and data.get('wms_manifest_id', False):
            man_process = data['wms_manifest_id']

        total_invoices = len(man_process.picking_line)
        processed_inv = 0

        for man_p in man_process.picking_line:
            if str(man_p.manifest_out_status) == "assigned":
                processed_inv += 1

        if total_invoices == processed_inv:

            # call confirm. otherwise "sms" will not go
            # self.env['wms.manifest.process'].confirm_wms_manifest(cr, uid, [man_process.id], context=context)

            try:
                man_process.write(
                    {'state': 'confirm', 'confirm_by': self.env.uid, 'confirm_time': fields.Datetime.now()})
                # confirm_wms_query = "UPDATE wms_manifest_process SET state='confirm', confirm_by='{0}', confirm_time='{1}' WHERE id='{2}'".format(
                #     str(fields.datetime.now()), man_process.id)
                # self.env.cr.execute(confirm_wms_query)
                # self.env.cr.commit()

                man_process_line.write({
                    'delivery_date': data['delivery_date'],
                    'deliver_time': data['delivery_time']
                })
                # wms_man_lin_query = "UPDATE wms_manifest_line SET delivery_date='{0}', deliver_time='{1}' WHERE invoice_id='{2}' AND magento_no='{3}'".format(
                #     data['delivery_date'], data['delivery_time'], data['invoice_id'], data['name'])
                # self.env.cr.execute(wms_man_lin_query)
                # self.env.cr.commit()

            except:
                pass
        else:
            man_process_line.write({
                'delivery_date': data['delivery_date'],
                'deliver_time': data['delivery_time']
            })
            # wms_man_lin_query = "UPDATE wms_manifest_line SET delivery_date='{0}', deliver_time='{1}' WHERE invoice_id='{2}' AND magento_no='{3}'".format(
            #     data['delivery_date'], data['delivery_time'], data['invoice_id'], data['name'])
            # self.env.cr.execute(wms_man_lin_query)
            # self.env.cr.commit()


class WmsManifestLine(models.Model):
    _inherit = "wms.manifest.line"

    manifest_out_status = fields.Char('Manifest Out Status')
    delivery_date = fields.Date('Delivery Date')
    deliver_time = fields.Char('Delivery Time')
    reschedule = fields.Boolean('Reschedule')
    rm_name = fields.Char('RM Name')
    rm_phone = fields.Char('RM Phone')
    full_delivered = fields.Boolean('Full Delivered')
    partial_delivered = fields.Boolean('Partial Delivered')
    full_return = fields.Boolean('Full Return')

    @api.model
    def create(self, vals):

        record = super(WmsManifestLine, self).create(vals)

        try:

            so_obj = self.env["sale.order"]
            so_list = so_obj.search([('client_order_ref', '=', record.magento_no)])

            for so in so_list:

                if so.sales_representative_name:
                    record.rm_name = so.sales_representative_name
                    record.rm_phone = so.sales_representative_mobile
                record.priority_customer = so.new_customer
        except:
            pass

        return record

    def reschedule_invoice_delivery(self, ids):

        wms_man_line = self.browse(ids)

        ndr_pro_obj = self.env["ndr.process"]
        wms_man_pro = self.env["wms.manifest.process"]
        man_line_obj = self.env['wms.manifest.line']
        inv_obj = self.env["account.move"]
        so_obj = self.env["sale.order"]
        inv_list = inv_obj.search([('id', '=', wms_man_line.invoice_id)])
        inv = inv_obj.browse(inv_list)
        so_list = so_obj.search([('client_order_ref', '=', str(inv_list.ref)),('state', '!=','cancel')])
        so = so_obj.browse(so_list)

        if ndr_pro_obj.search([('invoice_id', '=', inv_list.id)]) and wms_man_line.reschedule:
            raise Warning(_('NDR Process Already done!!! Can not do again!'))

        if inv_list.name:
            ndr_data = dict()
            ndr_data['invoice_number'] = inv_list.name
            # ndr_data['invoice'] = inv_list
            ndr_data['magento_number'] = str(inv_list.ref)
            ndr_data['order_number'] = so_list.name
            ndr_data['order_id'] = so.id
            ndr_data['state'] = 'draft'
            ndr_data['ndr_reason'] = 'Created from outbound'
            # ndr_data['source'] = self.env.context['source']

            inv_line_list = list()

            for invoice_line in inv_list.invoice_line_ids:
                if invoice_line.quantity > 0.00:
                    inv_line_list.append([0, False, {
                        'product': str(invoice_line.name),
                        'product_id': int(invoice_line.product_id.id),
                        # 'product_ean': str(invoice_line.product_id.ean13) if invoice_line.product_id.ean13 else '',
                        'product_ean': '',
                        'quantity': invoice_line.quantity,

                    }])

            ndr_data['ndr_process_line'] = inv_line_list

            # ndr.process
            ndr_pro_id = ndr_pro_obj.create(ndr_data)
            if ndr_pro_id and man_line_obj.browse(ids).hub_invoice != True:
                ndr_pro_id.confirm_ndr()

            # status assigned
            data = dict()
            data['invoice_id'] = inv_list.id
            data['name'] = str(inv_list.ref)
            data['delivery_date'] = datetime.datetime.now().strftime('%Y-%m-%d')
            data['delivery_time'] = ''
            data['wms_manifest_id'] = man_line_obj.browse(ids).wms_manifest_id

            source = self.env.context.get('source') if self.env.context.get('source') else 'Web'
            delivery_done_by = self.env.uid
            delivery_done_datetime = fields.Datetime.now()

            wms_man_line.write({'manifest_out_status': 'assigned',
                                'reschedule': True,
                                'source': source,
                                'delivery_done_by': delivery_done_by,
                                'delivery_done_datetime': delivery_done_datetime})

            wms_man_pro.process_confirm(data)

        else:
            raise Warning(_('The invoice is not validate yet!!! Please validate the invoice'))

        # return True

    def action_invoice_full_delivered(self):
        for manifest_line in self:
            context = dict(self.env.context)
            context['name'] = str(manifest_line.magento_no)
            context['man_name'] = str(manifest_line.wms_manifest_id.name)
            context['invoice_id'] = manifest_line.invoice_id
            context['magento_no'] = str(manifest_line.magento_no)
            context['manifest_line_id'] = manifest_line.id
            context['source'] = context.get('source') if context.get('source', False) else 'Web'

            self.env["wms.manifest.process"].with_context(context).create_delivery_from_manifest(
                [manifest_line.invoice_id])

            manifest_line.write({'manifest_out_status': 'assigned',
                                 'full_delivered': True,
                                 'source': context['source'],
                                 'delivery_done_by': self.env.uid,
                                 'delivery_done_datetime': fields.Date.today()
                                 })

            # --------------------------------------------------
            data = dict()
            data['name'] = str(manifest_line.magento_no)
            data['invoice_id'] = manifest_line.invoice_id
            data['delivery_date'] = context['delivery_date']
            data['delivery_time'] = context['delivery_time']
            self.env['wms.manifest.process'].process_confirm(data)
            # --------------------------------------------------


class ManifestWithFullReturn(models.Model):
    _name = "manifest.with.full.return"
    _description = "Manifest with full return"

    invoice_id = fields.Integer('Invoice ID')
    order_no = fields.Char('Order No')
    delivery_line_ids = fields.One2many('manifest.with.full.return.line', 'line_id', 'Delivery Line',
                                        required=True)
    return_reason = fields.Selection([
        ('cash_problems', 'Cash Problems'),
        ('customer_did_not_ordered', 'Customer Did Not Ordered'),
        ('customer_not_interested', 'Customer Not Interested'),
        ('customer_not_reachable', 'Customer Not Reachable'),
        ('customer_wants_full_delivery', 'Customer Wants Full Delivery'),
        ('customer_wants_high_quality', 'Customer Wants High Quality'),
        ('customer_wants_low_quality', 'Customer Wants Low Quality'),
        ('fake_order', 'Fake Order'),
        ('late_delivery', 'Late Delivery'),
        ('less_qty_required', 'Less Qty Required'),
        ('more_qty_required', 'More Qty Required'),
        ('office_closed', 'Office Closed'),
        ('pack_size_issue', 'Pack Size Issue'),
        ('price_issue', 'Price Issue'),
        ('product_damaged', 'Product Damaged'),
        ('product_mismatch', 'Product Mismatch'),
        ('returned_due_to_other_products', 'Returned Due to Other Products'),
        ('wrong_address', 'Wrong Address'),
        ('wrong_order_placed', 'Wrong Order Placed'),
        ('wrongly_packed', 'Wrongly Packed'),
    ], 'Return Reason', index=True)

    def default_get(self, fields):
        # product_pricelist = self.env['product.pricelist']
        # if context is None:
        #     context = {}
        res = super(ManifestWithFullReturn, self).default_get(fields)

        man_line_obj = self.env["wms.manifest.line"]
        man_line = man_line_obj.browse(self.env.context['active_id'])

        res['invoice_id'] = man_line.invoice_id
        res['order_no'] = man_line.magento_no

        inv_obj = self.env["account.move"]
        inv = inv_obj.browse([man_line.invoice_id])

        items = list()

        for line in inv.invoice_line_ids:
            if line.quantity > 0.00:
                item = {
                    'product_id': line.product_id.id,
                    'description': line.name,
                    'quantity': line.quantity,
                    'amount': float(line.price_subtotal),
                    'delivered': line.quantity
                }

                if line.product_id.default_code and line.product_id.default_code != '':
                    items.append((0, 0, item))
                else:
                    raise ValidationError("The relevant product doesn't have any internal reference. Please set the "
                                          "internal reference, and try again")

        res.update(delivery_line_ids=items)

        return res

    def create_deliver_with_full_return(self):
        context = dict(self.env.context)
        delivery_return = self.browse(self.ids)
        context['source'] = self.env.context.get('source') if self.env.context.get('source') else 'Web'

        man_line_obj = self.env['wms.manifest.line']
        man_line = man_line_obj.search([('invoice_id', '=', delivery_return.invoice_id),
                                        ('reschedule', '=', False)])

        if len(man_line) > 1:
            raise ValidationError('Multiple manifest line has been created using same invoice.')

        if delivery_return.return_reason:
            out_data = {
                'name': delivery_return.order_no,
                'man_name': str(man_line.wms_manifest_id.name),
                'delivery_date': datetime.datetime.now().strftime('%Y-%m-%d'),
                'source': context.get('source') if context.get('source') else 'Web',
                'delivery_done_by': self.env.uid,
                'delivery_done_datetime': fields.Date.today(),
            }

            outbound_line_list = list()

            for line in delivery_return.delivery_line_ids:
                if float(line.quantity) > 0.00:
                    outbound_line_list.append([0, False, {
                        'invoice_id': delivery_return.invoice_id,
                        'magento_no': delivery_return.order_no,
                        'description': str(line.description),
                        'product_id': line.product_id,
                        'amount': float(line.amount),
                        'quantity': float(line.quantity),
                        'delivered': 0,
                        'return_reason': str(delivery_return.return_reason),
                    }])

            out_data['outbound_line'] = outbound_line_list
            context['manifest_line_id'] = man_line.id

            wms_man_out = self.env["wms.manifest.outbound"]
            man_out_id = wms_man_out.create(out_data)

            returned_qty = 0
            man_out = man_out_id
            for line_return in man_out.outbound_line:
                if line_return.return_qty > 0:
                    returned_qty += line_return.return_qty

            source = context['source']
            delivery_done_by = self.env.uid
            delivery_done_datetime = fields.Date.today()

            if man_out_id and returned_qty > 0:
                context['full_return'] = True
                # man_out.receipt_return()

                man_line.write({'manifest_out_status': 'assigned', 'full_return': True, 'source': source,
                                'delivery_done_by': delivery_done_by,
                                'delivery_done_datetime': delivery_done_datetime})

            # --------------------------------------------------
            data = dict()
            data['name'] = str(man_line.magento_no)
            data['invoice_id'] = delivery_return.invoice_id
            data['delivery_date'] = datetime.datetime.today().date().strftime("%Y-%m-%d")
            data['delivery_time'] = ''
            self.env['wms.manifest.process'].process_confirm(data)
            # --------------------------------------------------

        else:
            # pop up error
            raise Warning(_('Reason can not be blank!!!'))

        return True


class ManifestWithFullReturnLine(models.Model):
    _name = "manifest.with.full.return.line"
    _description = "Manifest with full return line"

    line_id = fields.Many2one('manifest.with.full.return', 'Delivery Line', required=True, ondelete='cascade',
                              index=True, readonly=True)
    product_id = fields.Integer('Product ID')
    description = fields.Char('Description')
    quantity = fields.Float('Quantity')
    delivered = fields.Float('Delivered')
    return_quantity = fields.Float('Return')
    amount = fields.Float('Amount')
    return_reason = fields.Selection([
        ('cash_problems', 'Cash Problems'),
        ('customer_did_not_ordered', 'Customer Did Not Ordered'),
        ('customer_not_interested', 'Customer Not Interested'),
        ('customer_not_reachable', 'Customer Not Reachable'),
        ('customer_wants_full_delivery', 'Customer Wants Full Delivery'),
        ('customer_wants_high_quality', 'Customer Wants High Quality'),
        ('customer_wants_low_quality', 'Customer Wants Low Quality'),
        ('fake_order', 'Fake Order'),
        ('late_delivery', 'Late Delivery'),
        ('less_qty_required', 'Less Qty Required'),
        ('more_qty_required', 'More Qty Required'),
        ('office_closed', 'Office Closed'),
        ('pack_size_issue', 'Pack Size Issue'),
        ('price_issue', 'Price Issue'),
        ('product_damaged', 'Product Damaged'),
        ('product_mismatch', 'Product Mismatch'),
        ('returned_due_to_other_products', 'Returned Due to Other Products'),
        ('wrong_address', 'Wrong Address'),
        ('wrong_order_placed', 'Wrong Order Placed'),
        ('wrongly_packed', 'Wrongly Packed'),
    ], 'Return Reason', index=True)


class ManifestWithReturn(models.Model):
    _name = "manifest.with.return"
    _description = "Manifest with return"

    invoice_id = fields.Integer('Invoice ID')
    order_no = fields.Char('Order No')
    delivery_date = fields.Date('Delivery Date')
    delivery_line = fields.One2many('manifest.with.return.line', 'deliver_line_id', 'Delivery Line',
                                    required=True)
    delivery_time = fields.Selection([
        ('07 AM - 08 AM', '07 AM - 08 AM'),
        ('08 AM - 09 AM', '08 AM - 09 AM'),
        ('09 AM - 10 AM', '09 AM - 10 AM'),
        ('10 AM - 11 AM', '10 AM - 11 AM'),
        ('11 AM - 12 PM', '11 AM - 12 PM'),
        ('12 PM - 01 PM', '12 PM - 01 PM'),
        ('01 PM - 02 PM', '01 PM - 02 PM'),
        ('02 PM - 03 PM', '02 PM - 03 PM'),
        ('03 PM - 04 PM', '03 PM - 04 PM'),
        ('04 PM - 05 PM', '04 PM - 05 PM'),
        ('05 PM - 06 PM', '05 PM - 06 PM'),
        ('06 PM - 07 PM', '06 PM - 07 PM'),
        ('07 PM - 08 PM', '07 PM - 08 PM'),
        ('08 PM - 09 PM', '08 PM - 09 PM'),
        ('09 PM - 10 PM', '09 PM - 10 PM'),
    ], 'Deliver Time', index=True)

    # ## Block code for v-14
    # _columns = {
    #     'invoice_id': fields.integer('Invoice ID'),
    #     'order_no': fields.char('Order No'),
    #     'delivery_date': fields.date('Delivery Date'),
    #     'delivery_line': fields.one2many('manifest.with.return.line', 'deliver_line_id', 'Delivery Line',
    #                                      required=True),
    #     'delivery_time': fields.selection([
    #         ('07 AM - 08 AM', '07 AM - 08 AM'),
    #         ('08 AM - 09 AM', '08 AM - 09 AM'),
    #         ('09 AM - 10 AM', '09 AM - 10 AM'),
    #         ('10 AM - 11 AM', '10 AM - 11 AM'),
    #         ('11 AM - 12 PM', '11 AM - 12 PM'),
    #         ('12 PM - 01 PM', '12 PM - 01 PM'),
    #         ('01 PM - 02 PM', '01 PM - 02 PM'),
    #         ('02 PM - 03 PM', '02 PM - 03 PM'),
    #         ('03 PM - 04 PM', '03 PM - 04 PM'),
    #         ('04 PM - 05 PM', '04 PM - 05 PM'),
    #         ('05 PM - 06 PM', '05 PM - 06 PM'),
    #         ('06 PM - 07 PM', '06 PM - 07 PM'),
    #         ('07 PM - 08 PM', '07 PM - 08 PM'),
    #         ('08 PM - 09 PM', '08 PM - 09 PM'),
    #         ('09 PM - 10 PM', '09 PM - 10 PM'),
    #     ], 'Deliver Time', index=True),
    # }

    def default_get(self, fields):
        # product_pricelist = self.env['product.pricelist']
        # if context is None:
        #     context = {}
        res = super(ManifestWithReturn, self).default_get(fields)

        man_line_obj = self.env["wms.manifest.line"]
        man_line = man_line_obj.browse(self.env.context['active_id'])

        res['invoice_id'] = man_line.invoice_id
        res['order_no'] = man_line.magento_no
        res['delivery_date'] = date.today()
        # res['delivery_date'] = datetime.datetime.today().date().strftime("%Y-%m-%d")

        inv_obj = self.env["account.move"]
        inv = inv_obj.browse([man_line.invoice_id])

        items = list()

        for line in inv.invoice_line_ids:
            if line.quantity > 0.00:
                item = {
                    'product_id': line.product_id.id,
                    'description': line.name,
                    'quantity': line.quantity,
                    'amount': float(line.price_subtotal),
                    'delivered': line.quantity
                }

                if line.product_id.default_code and line.product_id.default_code != '':
                    items.append((0, 0, (item)))
                else:
                    raise ValidationError("The relevant product doesn't have any internal reference. Please set the "
                                          "internal reference, and try again")
                    # items.append(item)
        abc = json.dumps(items)
        # res['line_list'] = abc

        res.update(delivery_line=items)

        return res

    def create_deliver_with_return(self):
        context = dict(self.env.context)

        delivery_return = self.browse(self.ids)
        context['source'] = self.env.context.get('source') if self.env.context.get('source', False) else 'Web'

        for line in delivery_return.delivery_line:
            if line.delivered < 0 or line.delivered > line.quantity:
                raise Warning(_('Delivered quantity should be within ordered quantity!!!'))

        man_line_obj = self.env['wms.manifest.line']
        man_line_list = man_line_obj.search([('invoice_id', '=', delivery_return.invoice_id),
                                             ('reschedule', '=', False)])
        man_line = man_line_list

        if len(man_line) > 1:
            raise ValidationError('Multiple manifest line has been created using same invoice.')

        out_data = {
            'name': delivery_return.order_no,
            'man_name': str(man_line_list.wms_manifest_id.name),
            'delivery_date': delivery_return.delivery_date,
            'delivery_time': delivery_return.delivery_time,
            'source': context.get('source') if context.get('source') else 'Web',
            'delivery_done_by': self.env.uid,
            'delivery_done_datetime': fields.Date.today(),

        }

        outbound_line_list = list()

        for line in delivery_return.delivery_line:
            if not line.return_reason and (float(line.quantity) - float(line.delivered)) > 0:
                raise Warning(_('Reason can not be blank!!!'))

            if float(line.quantity) > 0.00:

                if self.env["product.product"].browse([line.product_id]).type == 'service':
                    delivered = float(1)
                    quantity = float(1)
                else:
                    delivered = float(line.delivered)
                    quantity = float(line.quantity)

                outbound_line_list.append([0, False, {
                    'invoice_id': delivery_return.invoice_id,
                    'magento_no': delivery_return.order_no,
                    'description': str(line.description),
                    'product_id': line.product_id,
                    'amount': float(line.amount),
                    'quantity': quantity,
                    'delivered': delivered,
                    'return_reason': str(line.return_reason),
                }])

        out_data['outbound_line'] = outbound_line_list

        context['manifest_line_id'] = man_line.id

        wms_man_out = self.env["wms.manifest.outbound"]
        man_out_id = wms_man_out.create(out_data)

        returned_qty = 0
        man_out = man_out_id
        for line_return in man_out_id.outbound_line:
            if line_return.return_qty > 0:
                returned_qty += line_return.return_qty

        source = context['source']
        delivery_done_by = self.env.uid
        delivery_done_datetime = datetime.datetime.today()
        if man_out_id and returned_qty > 0:
            # man_out_id.receipt_return()

            man_line_list.write({'manifest_out_status': 'assigned', 'partial_delivered': True,
                                 'source': source, 'delivery_done_by': delivery_done_by,
                                 'delivery_done_datetime': delivery_done_datetime
                                 })
        else:
            man_line_list.write({'manifest_out_status': 'assigned', 'full_delivered': True,
                                 'source': source, 'delivery_done_by': delivery_done_by,
                                 'delivery_done_datetime': delivery_done_datetime})

        # --------------------------------------------------
        data = dict()
        data['name'] = str(man_line.magento_no)
        data['invoice_id'] = delivery_return.invoice_id
        data['delivery_date'] = delivery_return.delivery_date
        data['delivery_time'] = delivery_return.delivery_time
        self.env['wms.manifest.process'].process_confirm(data)
        # --------------------------------------------------

        return True


class ManifestWithReturnLine(models.Model):
    _name = "manifest.with.return.line"
    _description = "Manifest with return line"

    product_id = fields.Integer('Product ID')
    description = fields.Char('Description')
    quantity = fields.Float('Quantity')
    delivered = fields.Float('Delivered')
    returned_qty = fields.Float('Return')
    return_manifest = fields.Float('Return')
    amount = fields.Float('Amount')
    return_reason = fields.Selection([
        ('sku_full_received', 'SKU Full Received'),
        ('cash_problems', 'Cash Problems'),
        ('customer_did_not_ordered', 'Customer Did Not Ordered'),
        ('customer_not_interested', 'Customer Not Interested'),
        ('customer_not_reachable', 'Customer Not Reachable'),
        ('customer_wants_full_delivery', 'Customer Wants Full Delivery'),
        ('customer_wants_high_quality', 'Customer Wants High Quality'),
        ('customer_wants_low_quality', 'Customer Wants Low Quality'),
        ('fake_order', 'Fake Order'),
        ('late_delivery', 'Late Delivery'),
        ('less_qty_required', 'Less Qty Required'),
        ('more_qty_required', 'More Qty Required'),
        ('office_closed', 'Office Closed'),
        ('pack_size_issue', 'Pack Size Issue'),
        ('price_issue', 'Price Issue'),
        ('product_damaged', 'Product Damaged'),
        ('product_mismatch', 'Product Mismatch'),
        ('returned_due_to_other_products', 'Returned Due to Other Products'),
        ('wrong_address', 'Wrong Address'),
        ('wrong_order_placed', 'Wrong Order Placed'),
        ('wrongly_packed', 'Wrongly Packed'),
    ], 'Return Reason', index=True)
    deliver_line_id = fields.Many2one('manifest.with.return', 'Delivery Line', required=True, ondelete='cascade',
                                      index=True, readonly=True)

    # ## Block code for v-14
    # _columns = {
    #     'product_id': fields.integer('Product ID'),
    #     'description': fields.char('Description'),
    #     'quantity': fields.float('Quantity'),
    #     'delivered': fields.float('Delivered'),
    #     'returned_qty': fields.float('Return'),
    #     'return': fields.float('Return'),
    #     'amount': fields.float('Amount'),
    #     # 'return_reason': fields.char('Return Reason'),
    #     'return_reason': fields.selection([
    #         ('sku_full_received', 'SKU Full Received'),
    #         ('cash_problems', 'Cash Problems'),
    #         ('customer_did_not_ordered', 'Customer Did Not Ordered'),
    #         ('customer_not_interested', 'Customer Not Interested'),
    #         ('customer_not_reachable', 'Customer Not Reachable'),
    #         ('customer_wants_full_delivery', 'Customer Wants Full Delivery'),
    #         ('customer_wants_high_quality', 'Customer Wants High Quality'),
    #         ('customer_wants_low_quality', 'Customer Wants Low Quality'),
    #         ('fake_order', 'Fake Order'),
    #         ('late_delivery', 'Late Delivery'),
    #         ('less_qty_required', 'Less Qty Required'),
    #         ('more_qty_required', 'More Qty Required'),
    #         ('office_closed', 'Office Closed'),
    #         ('pack_size_issue', 'Pack Size Issue'),
    #         ('price_issue', 'Price Issue'),
    #         ('product_damaged', 'Product Damaged'),
    #         ('product_mismatch', 'Product Mismatch'),
    #         ('returned_due_to_other_products', 'Returned Due to Other Products'),
    #         ('wrong_address', 'Wrong Address'),
    #         ('wrong_order_placed', 'Wrong Order Placed'),
    #         ('wrongly_packed', 'Wrongly Packed'),
    #     ], 'Return Reason', index=True),
    #     'deliver_line_id': fields.many2one('manifest.with.return', 'Delivery Line', required=True, ondelete='cascade',
    #                                        index=True, readonly=True),
    # }

    @api.onchange('delivered')
    def _onchange_return_qty(self):
        self.returned_qty = self.quantity - self.delivered if self.quantity - self.delivered > 0 else 0


class ManifestWithFullDelivered(models.Model):
    _name = "manifest.with.full.delivered"
    _description = "Manifest with full Delivered"

    invoice_id = fields.Char('Invoice ID')
    order_no = fields.Char('Order No')
    delivery_date = fields.Date('Delivery Date', default=fields.Datetime.now())
    deliver_time = fields.Selection([
        ('07 AM - 08 AM', '07 AM - 08 AM'),
        ('08 AM - 09 AM', '08 AM - 09 AM'),
        ('09 AM - 10 AM', '09 AM - 10 AM'),
        ('10 AM - 11 AM', '10 AM - 11 AM'),
        ('11 AM - 12 PM', '11 AM - 12 PM'),
        ('12 PM - 01 PM', '12 PM - 01 PM'),
        ('01 PM - 02 PM', '01 PM - 02 PM'),
        ('02 PM - 03 PM', '02 PM - 03 PM'),
        ('03 PM - 04 PM', '03 PM - 04 PM'),
        ('04 PM - 05 PM', '04 PM - 05 PM'),
        ('05 PM - 06 PM', '05 PM - 06 PM'),
        ('06 PM - 07 PM', '06 PM - 07 PM'),
        ('07 PM - 08 PM', '07 PM - 08 PM'),
        ('08 PM - 09 PM', '08 PM - 09 PM'),
        ('09 PM - 10 PM', '09 PM - 10 PM'),
    ], 'Deliver Time', default='10 AM - 11 AM', index=True)

    def default_get(self, fields):
        res = super(ManifestWithFullDelivered, self).default_get(fields)

        man_line_obj = self.env["wms.manifest.line"]
        man_line = man_line_obj.browse(self.env.context['active_id'])

        res['invoice_id'] = man_line.invoice_id
        res['order_no'] = man_line.magento_no

        return res

    def create_full_delivery(self):
        context = dict(self.env.context)
        context['delivery_date'] = self.delivery_date
        context['delivery_time'] = self.deliver_time
        context['source'] = self.env.context.get('source') if self.env.context.get('source', False) else 'Web'

        man_lines = self.env["wms.manifest.line"].search([('invoice_id', '=', self.invoice_id),
                                                          ('magento_no', '=', self.order_no),
                                                          ('reschedule', '=', False)])

        if len(man_lines) > 0:
            man_lines.with_context(context).action_invoice_full_delivered()


class ManifestReschedule(models.Model):
    _name = "manifest.reschedule"
    _description = "Manifest Reschedule"

    invoice_id = fields.Char('Invoice ID')
    manifest_line_id = fields.Integer('Manifest Line ID')
    ndr = fields.Selection([
        ('cash_problems', 'Cash Problems'),
        ('customer_did_not_ordered', 'Customer Did Not Ordered'),
        ('customer_not_interested', 'Customer Not Interested'),
        ('customer_not_reachable', 'Customer Not Reachable'),
        ('customer_wants_full_delivery', 'Customer Wants Full Delivery'),
        ('customer_wants_high_quality', 'Customer Wants High Quality'),
        ('customer_wants_low_quality', 'Customer Wants Low Quality'),
        ('fake_order', 'Fake Order'),
        ('late_delivery', 'Late Delivery'),
        ('less_qty_required', 'Less Qty Required'),
        ('more_qty_required', 'More Qty Required'),
        ('office_closed', 'Office Closed'),
        ('pack_size_issue', 'Pack Size Issue'),
        ('price_issue', 'Price Issue'),
        ('product_damaged', 'Product Damaged'),
        ('product_mismatch', 'Product Mismatch'),
        ('returned_due_to_other_products', 'Returned Due to Other Products'),
        ('wrong_address', 'Wrong Address'),
        ('wrong_order_placed', 'Wrong Order Placed'),
        ('wrongly_packed', 'Wrongly Packed'),
    ], 'Not Delivery Reason', index=True)

    def default_get(self, fields):
        # if context is None:
        #     context = {}
        context = None
        res = super(ManifestReschedule, self).default_get(fields)

        man_line_obj = self.env["wms.manifest.line"]
        man_line = man_line_obj.browse(self.env.context['active_id'])

        res['invoice_id'] = man_line.invoice_id
        res['manifest_line_id'] = self.env.context['active_id']

        return res

    def reschedule_invoice(self):
        context = dict(self.env.context)
        try:
            data = self.read()[0]
        except:
            data = self.read()
        context['source'] = context.get('source') if context.get('source') else 'Web'

        self.env["wms.manifest.line"].reschedule_invoice_delivery(data['manifest_line_id'])
        # return True
