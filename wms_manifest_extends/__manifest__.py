{
    'name': 'WMS Manifest Extends',
    'version': "14.0.1.0.0",
    'author': "Odoo Bangladesh",
    'category': 'SMS',
    'summary': 'WMS Manifest Extends',
    'depends': ['base', 'odoo_magento_connect', 'account', 'sale_management', 'purchase', 'wms_manifest', 'send_sms_on_demand'],
    'data': [
        'security/manifest_extends.xml',
        'security/ir.model.access.csv',
        #
        'wizard/manifest_with_full_delivered.xml',
        'wizard/manifest_with_return.xml',
        'wizard/manifest_with_full_return.xml',
        'wizard/manifest_reschedule.xml',
        'views/wms_manifest_view_extend.xml',
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}
