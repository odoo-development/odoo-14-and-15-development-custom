{
    'name': 'Product Barcode Generator',
    'version': '14.0.1',
    'summary': 'Generates EAN13 Standard Barcode for Product.',
    'category': 'Inventory',
    'author': 'Sindabad.com',
    'website': 'https://www.sindabad.com',
    'depends': ['stock',
                'product',
                'stock_sync',
                'wms_inbound'
                ],
    'data': [
        'views/product_action_menu.xml',
        'views/product_form_view.xml',
        'views/irn_wizard_form_view.xml',
    ],
    'installable': True,
    'application': False,
    'auto_install': False,
}
