import math, re
import random
from odoo import api, models


class ProductAutoBarcode(models.Model):
    _inherit = 'product.product'

    def product_barcode_generator(self):
        self.barcode_generator()

    def barcode_generator(self):

        for record in self:

            if not record.barcode:
                # record.barcode = random.randrange(1111111111111, 9999999999999, record.id)
                record.barcode = self.env['barcode.nomenclature'].sudo().sanitize_ean(str(83)+str(random.randint(11111111111, 99999999999)))


