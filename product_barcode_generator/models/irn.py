from odoo import api, fields, models, _
from odoo.tools.translate import _
from odoo.exceptions import except_orm, Warning, RedirectWarning, UserError


class IrnWithEan(models.Model):
    _inherit = "irn.with.ean"

    generate_barcode = fields.Boolean(string="Auto Generate Barcode")

    def save_irn_with_product_ean(self):
        context = None
        product_id = self.env.context['product_id']
        product_ean = self.env.context['product_ean']

        if not self.generate_barcode:
            if product_ean == False or len('product_ean') > 13 in product_ean:
                raise Warning(_('Not a valid EAN number!!!'))
            else:
                for pp in self.env['product.product'].browse(product_id):
                    if not pp.barcode:
                        pp.write({'barcode': product_ean})

                irn_line_query = "UPDATE irn_control_line SET is_ean=TRUE, product_ean='{0}' WHERE product_id='{1}'".format(pp.barcode, product_id)
                self.env.cr.execute(irn_line_query)
                self.env.cr.commit()
        else:
            for pp in self.env['product.product'].browse(product_id):
                if not pp.barcode:
                    pp.barcode_generator()

            irn_line_query = "UPDATE irn_control_line SET is_ean=TRUE, product_ean='{0}' WHERE product_id='{1}'".format(pp.barcode, product_id)
            self.env.cr.execute(irn_line_query)
            self.env.cr.commit()

            self.env['product.product'].product_barcode_sync_to_magento(product_id)

        return {
            'type': 'ir.actions.client',
            'tag': 'reload',
        }

