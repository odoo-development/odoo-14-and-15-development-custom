# Author Rocky 2021

from odoo import fields, models


class SaleOrderLine(models.Model):
    _inherit = "sale.order.line"

    default_code = fields.Char(string="SKU", related="product_id.default_code", tracking=True)

