# Copyright 2021 Rocky

{
    "name": "Product SKU in sale order line",
    "version": "14.0",
    "author": "Sindabad",
    "category": "Sales Management",
    "license": "AGPL-3",
    "depends": ["sale"],
    "data": [
        "views/sale_order_line_sku_view.xml"
    ],
    "installable": True,
}
