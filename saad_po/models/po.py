from odoo import api, fields, models, _


class PO(models.Model):

    _inherit = 'purchase.order'

    adv_po_snd = fields.Selection([('credit', 'Credit'), ('advance', 'Advance'), ('cash', 'By Cash')],
                                   'Payment Status', required=False,
                                   states={'draft': [('required', True)], 'sent': [('required', True)]},
                                   change_default=True, tracking=True)

    # ## Block code for V-14
    # _columns = {
    #     #'adv_po_snd': fields.selection([ ('credit', 'Credit'),('advance', 'Advance'),], 'Payment Status', change_default='true', track_visibility='always'),
    #     'adv_po_snd': fields.selection([ ('credit', 'Credit'),('advance', 'Advance'),('cash','By Cash')], 'Payment Status', required=False, states={'draft': [('required', True)], 'sent': [('required', True)]}, change_default=True, track_visibility='always'),
    # }