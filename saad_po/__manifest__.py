
{
    'name': 'PO Payment Status Extra Field',
    'version': "1.0.0",
    'author': 'Odoo Bangladesh',
    'category': 'Tools',
    'depends': ['purchase'],
    'data': [
        'views/po_view.xml'
    ],
    'demo': [],
    'installable': True,
}
