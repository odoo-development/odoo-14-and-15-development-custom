# Copyright 2021 Rocky

from odoo import fields, models, api, _
from odoo.exceptions import UserError, ValidationError


class StockPicking(models.Model):
    _inherit = 'stock.picking'

    x_priority_customer = fields.Boolean(string="Priority Customer")

    def write(self, vals):

        record = super(StockPicking, self).write(vals)
        if 'move_line_ids_without_package' in vals:
            move_line_id_without_package = self.move_line_ids_without_package

            for val in move_line_id_without_package:
                if val.qty_done > val.product_uom_qty:
                    raise ValidationError(_("Done quantity less then or equal to Reserved quantity"))

        return record

