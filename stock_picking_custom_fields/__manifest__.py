# Copyright 2021 Rocky

{
    "name": "Stock Picking Custom Fields",
    "version": "14.0",
    "author": "Rocky",
    "category": "Inventory/Inventory",
    "license": "AGPL-3",
    "depends": ['stock'],
    "data": [
        "views/stock_picking_view.xml",
    ],
    "installable": True,
}
