{
    "name": "Damage Sale Menus",
    "version": "14.0",
    "author": "Sindabad",
    "category": "Sales Management",
    "license": "AGPL-3",
    "depends": ['sale'],
    "data": [
        'security/damage_sale_sa.xml',
        'views/damage_sale_menu.xml',
    ],
    "installable": True,
}
