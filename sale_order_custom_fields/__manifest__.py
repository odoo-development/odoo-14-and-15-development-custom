# Copyright 2021 Rocky

{
    "name": "Sales Order Custom Fields",
    "version": "14.0",
    "author": "Rocky",
    "category": "Sales Management",
    "license": "AGPL-3",
    "depends": ['sale', 'so_inside_outside'],
    "data": [
        "views/sale_order_view.xml",
    ],
    "installable": True,
}
