# Copyright 2021 Rocky

from odoo import api, fields, models, SUPERUSER_ID, _
from odoo.tools import float_is_zero, float_compare
from odoo import fields, models


class SaleOrder(models.Model):
    _inherit = 'sale.order'

    x_coupon_code = fields.Char(string="Coupon Code")
    x_delivery_option = fields.Char(string="Delivery Options")
    x_discount_amount = fields.Float(string="Discount Amount")
    x_discount_description = fields.Text(string="Discount Description")
    x_offer_key = fields.Char(string="Offer Key")
    x_order_source = fields.Char(string="Order Source")
    x_pack_ref_cus = fields.Char(string="Pack Ref")
    x_prepaid = fields.Boolean(string="Prepaid")
    x_puchase_ref_cus = fields.Char(string="Customer Purchase ref")
    x_purchase_ref_cus = fields.Char(string="Customer Purchase ref")
    x_sin_total_magento = fields.Float(string="Total In Magento")
    payment_code = fields.Char(string="Payment Code")
    customer_ref_id = fields.Many2one('res.partner', 'Customer reference on Order')
    warehouse_id = fields.Many2one('stock.warehouse', default=1, string="Warehouse")
    x_app_installation_id = fields.Char(string="app information")

    sales_representative_name = fields.Char(string="Salesperson Name")
    sales_representative_email = fields.Char(string="Salesperson Email")
    sales_representative_mobile = fields.Char(string="Salesperson Phone")
    is_company_customer = fields.Boolean(string="Is Company Customer")

    invoiced_amount = fields.Monetary("Invoiced Amount", compute='_compute_invoiced_amount',
                                      currency_field='currency_id',
                                      help="Total amount to invoice on the sales order, including all items "
                                           "(services, storables, expenses, ...)")

    order_state = fields.Selection([
        ('no invoice', 'Not Invoiced'),
        ('invoiced', 'Fully Invoiced'),
        ('returned', 'Fully Returned'),
        ('partial invoice', 'Partially Invoiced'),
        ('partial return', 'Partially Returned'),
    ], string='Invoice State', compute='_get_order_process_state', store=True, default='no invoice')

    def _compute_invoiced_amount(self):

        for sale in self:
            invoice_id = self.env['account.move'].search(
                ['&', ('invoice_origin', '=', sale.name), '|', ('state', '=', 'draft'), ('state', '=', 'posted'),
                 ('payment_state', 'not in', ['reversed', 'invoicing_legacy'])])
            total = 0

            for inv_items in invoice_id:
                if str(inv_items.state) == 'posted':
                    if str(inv_items.move_type) == 'out_invoice':
                        total += inv_items.amount_total
                    elif str(inv_items.move_type) == 'out_refund':
                        total -= inv_items.amount_total

            sale.invoiced_amount = total

    @api.depends('state', 'order_line.order_state')
    def _get_order_process_state(self):

        unconfirmed_orders = self.filtered(lambda so: so.state not in ['sale', 'done'])
        unconfirmed_orders.order_state = 'no invoice'
        confirmed_orders = self - unconfirmed_orders
        if not confirmed_orders:
            return
        line_order_process_state_all = [(d['order_id'][0], d['order_state']) for d in
                                        self.env['sale.order.line'].read_group(
                                            [('order_id', 'in', confirmed_orders.ids)], ['order_id', 'order_state'],
                                            ['order_id', 'order_state'], lazy=False)]

        # line_order_process_state_all = [(d['order_id'][0], d['order_state']) for d in self.env['sale.order.line'].search([('order_id','=',confirmed_orders.ids)])]

        for order in confirmed_orders:
            line_order_process_state = [d[1] for d in line_order_process_state_all if d[0] == order.id]

            if order.state in ('sale', 'done'):
                if any(order_state == 'partial invoice' for order_state in line_order_process_state):
                    order.order_state = 'partial invoice'

                elif any(order_state in ('partial return', 'returned') for order_state in line_order_process_state):
                    order.order_state = 'partial return'

                elif line_order_process_state and all(order_state == 'invoiced' for order_state in
                                                      line_order_process_state):
                    order.order_state = 'invoiced'

                elif line_order_process_state and all(order_state == 'returned' for order_state in
                                                      line_order_process_state):
                    order.order_state = 'returned'
                else:
                    order.order_state = 'no invoice'
            else:
                order.order_state = 'no invoice'


class SaleOrderLine(models.Model):
    _inherit = "sale.order.line"

    x_before_custom_price = fields.Float(string="Before Custom Price")
    x_custom_discount = fields.Float(string="Custom Discount Amount")
    x_discount = fields.Float(string="Discount")
    x_discount_gap = fields.Float(string="Discount Gap with magento")
    x_original_price_unit = fields.Float(string="Regular Price")
    x_saad_test = fields.Char(string="UoM")

    order_state = fields.Selection([
        ('no invoice', 'Not Invoiced'),
        ('invoiced', 'Fully Invoiced'),
        ('returned', 'Fully Returned'),
        ('partial invoice', 'Partially Invoiced'),
        ('partial return', 'Partially Returned')

    ], string='Invoice State', compute='_compute_order_process_state', store=True, default='no invoice')

    total_discount = fields.Float(string="Total Discount", compute='_total_discount_cals', store=True)

    @api.depends('invoice_lines.move_id.state', 'invoice_lines.quantity', 'untaxed_amount_to_invoice')
    def _get_invoice_qty(self):
        """
        Compute the quantity invoiced. If case of a refund, the quantity invoiced is decreased. Note
        that this is the case only if the refund is generated from the SO and that is intentional: if
        a refund made would automatically decrease the invoiced quantity, then there is a risk of reinvoicing
        it automatically, which may not be wanted at all. That's why the refund has to be created from the SO
        """
        for line in self:
            qty_invoiced = 0.0

            for invoice_line in line.invoice_lines:
                if invoice_line.move_id.state != 'cancel':
                    if invoice_line.move_id.move_type == 'out_invoice':
                        qty_invoiced += invoice_line.product_uom_id._compute_quantity(invoice_line.quantity,
                                                                                      line.product_uom)
                    # elif invoice_line.move_id.move_type == 'out_refund':
                    #     if not line.is_downpayment or line.untaxed_amount_to_invoice == 0:
                    #         qty_invoiced -= invoice_line.product_uom_id._compute_quantity(invoice_line.quantity,
                    #                                                                       line.product_uom)
            line.qty_invoiced = qty_invoiced

    @api.depends('state', 'order_state', 'product_uom_qty', 'qty_delivered', 'qty_to_invoice', 'qty_invoiced')
    def _compute_order_process_state(self):

        precision = self.env['decimal.precision'].precision_get('Product Unit of Measure')

        for line in self:
            invoice_type_refund = [inv_items.move_type for inv_items in line.order_id.invoice_ids if
                                   inv_items.move_type ==
                                   'out_refund']
            if line.state in ('sale', 'done'):

                if float_compare(line.qty_invoiced, line.product_uom_qty, precision_digits=precision) >= 0:

                    if invoice_type_refund:
                        if line.qty_invoiced == 0:
                            line.order_state = 'returned'

                        else:
                            line.order_state = 'partial return'
                    else:
                        line.order_state = 'invoiced'
                elif float_compare(line.qty_invoiced, line.product_uom_qty, precision_digits=precision) < 0:

                    if invoice_type_refund:
                        if line.qty_return == line.qty_invoiced:
                            line.order_state = 'returned'

                        else:
                            line.order_state = 'partial return'
                    else:
                        if line.qty_invoiced == 0:
                            line.order_state = 'no invoice'
                        else:
                            line.order_state = 'partial invoice'

                        if line.closed_quantity > 0:
                            line.order_state = 'invoiced'
            else:
                line.order_state = 'no invoice'

    @api.depends('x_discount', 'product_uom_qty')
    def _total_discount_cals(self):

        for line in self:
            line.total_discount = round(float((line.x_discount) * float(line.product_uom_qty)), 2)



