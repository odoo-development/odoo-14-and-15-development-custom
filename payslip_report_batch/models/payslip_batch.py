from odoo import models, fields, api, _
from odoo.tools.translate import _
from odoo.exceptions import UserError, ValidationError, Warning

class PayslipBatchRun(models.Model):
    _inherit = "hr.payslip.run"


    def print_batch_payslip_report(self):
        return self.env.ref("payslip_report_batch.action_batch_payslip_report").report_action(self)
