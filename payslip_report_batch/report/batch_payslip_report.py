from odoo import api, models, _
from datetime import date


class batchpayslipdata(models.AbstractModel):
    _name = 'report.payslip_report_batch.report_batch_payslip_layout'
    _description = 'Batch payslip report'

    @api.model
    def _get_report_values(self, docids, data=None):
        docs = self.env['hr.payslip.run'].browse(docids[0])

        return {
            'doc_ids': docids,
            'doc_model': 'hr.payslip.run',
            'docs': docs,
            'data': data,
            'get_details_by_data': self.get_details_by_data,
            'top_chart_data':self.top_chart_data,
            'get_department_wise_data':self.get_department_wise_data,
        }


    def get_details_by_data(self, obj,department):

        result = []
        ids = []
        for id in range(len(obj)):
            ids.append(obj.id)
        if obj.id:
            self.env.cr.execute(
                '''select id,code from hr_salary_rule where code in ('TOTAL/ADDITION', 'TOTAL/DED', 'NET') ''')

            rule_dict= {rule['code']: rule['id'] for rule in self.env.cr.dictfetchall()}
            self.env.cr.execute(
    '''select salary_rule_id, amount, hr_payslip.name,hr_payslip.employee_id,hr_contract.name as emp_name,
    hr_contract.date_start as emp_joining_date, hr_contract.employee_identification_code, hr_contract.gross_salary,
     hr_payslip.fest_bonus_sin, (select hr_department.name from hr_department 
     where hr_contract.department_id = hr_department.id) as department, (select hr_job.name 
     from hr_job where hr_job.id = hr_contract.job_id) as deg, (select res_partner_bank.acc_number 
     from res_partner_bank where hr_employee.bank_account_id = res_partner_bank.id) as Acc_number 
     from hr_payslip, hr_payslip_line,hr_contract,hr_employee where hr_payslip.id= hr_payslip_line.slip_id 
     and payslip_run_id = %s and salary_rule_id in (select id from hr_salary_rule 
     where code in ('TOTAL/ADDITION','TOTAL/DED','NET')) and hr_payslip.employee_id = hr_contract.employee_id 
     and hr_contract.employee_id = hr_employee.id and hr_contract.department_id= %s''',(obj.id,department))

            objects_dict = {}
            emp_ids = []

            import itertools
            object = self.env.cr.dictfetchall()
            objects = sorted(object, key=lambda i: i['employee_id'])
            an_iterator = itertools.groupby(objects, lambda y: y['employee_id'])

            for key, group in an_iterator:
                key_and_group = {key: list(group)}
                objects_dict.update(key_and_group)
                emp_ids.append(key)

            for val in emp_ids:
                data = {}
                for x in objects_dict[val]:
                    data.update({
                        'acc_number': x['acc_number'],
                        'department': x['department'],
                        'deg': x['deg'],
                        'emp_joining_date': x['emp_joining_date'].strftime("%d-%m-%Y"),
                        'emp_name': x['emp_name'],
                        'employee_id': x['employee_id'],
                        'employee_identification_code': x['employee_identification_code'],
                        'gross': x['gross_salary'],
                        # 'cash_allowance': x['cash_allowance'],
                        'fest_bonus_sin': x['fest_bonus_sin']
                    })
                    if x['salary_rule_id'] == rule_dict['TOTAL/ADDITION']:
                        data['total_addition'] = x['amount']
                    if x['salary_rule_id'] == rule_dict['TOTAL/DED']:
                        data['total_dedaction'] = x['amount']
                    if x['salary_rule_id'] == rule_dict['NET']:
                        data['net_salary'] = x['amount']
                result.append(data)
            # final_data = sorted(result, key=lambda i: i['department'])

        return result


    def top_chart_data(self,obj):
        if obj.id:
            self.env.cr.execute('''select sum(amount) as net_salary,count(*),sum(gross_salary) as gross ,sum(cash_allowance) as additional,(select hr_department.name from hr_department where hr_contract.department_id = hr_department.id) as department from hr_payslip, hr_payslip_line,hr_contract,hr_employee where hr_payslip.id= hr_payslip_line.slip_id and payslip_run_id= %s and salary_rule_id = (select id from hr_salary_rule where code = 'NET') and hr_payslip.employee_id = hr_contract.employee_id and hr_contract.employee_id = hr_employee.id group by department''', (obj.id,))
            objects = self.env.cr.dictfetchall()
            for x in objects:
                x['total']= float(x['net_salary']) + float(x['additional'])

            return objects


    def get_department_wise_data(self, obj):

        self.env.cr.execute(
            '''select hr_contract.department_id  as department_id
            from hr_payslip, hr_payslip_line,hr_contract,hr_employee 
            where hr_payslip.id= hr_payslip_line.slip_id and payslip_run_id= %s 
            and hr_payslip.employee_id = hr_contract.employee_id and hr_contract.employee_id = hr_employee.id 
            group by hr_contract.department_id order by hr_contract.department_id''',
            (obj.id,))
        objects = self.env.cr.dictfetchall()

        get_department_ids = [dep['department_id'] for dep in objects]

        return get_department_ids











