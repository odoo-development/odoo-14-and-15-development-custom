{
    "name": "Batch payslip report with Dep wise expense and top chart",
    "version": "15.0.0",
    "author": "Ashif",
    "category": "Human Resources",
    "license": "AGPL-3",
    "depends": [
        'bi_hr_payroll',
        'hr_contract'
    ],
    "data": [
         "report/report_batch_payslip_layout.xml",
         "views/batch_payslip_menu.xml",
         "views/batch_inherit_view.xml",
    ],
    "installable": True,
    "auto_install": False,
}
