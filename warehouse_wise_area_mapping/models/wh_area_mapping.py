from odoo import api, fields, models


class WarehouseArea(models.Model):
    _name = "warehouse.area"
    _description = "Warehouse area"
    _order = "id desc"

    code = fields.Char("Code")
    areas = fields.Text("Areas")

    """ Relational Fields """
    warehouse_id = fields.Many2one("stock.warehouse", string="Warehouse Name")

    @api.onchange('warehouse_id')
    def _onchange_warehouse_id(self):
        """ Finds code id for changed warehouse. """
        if self.warehouse_id:
            self.code = self.warehouse_id.code
        else:
            self.code = False
