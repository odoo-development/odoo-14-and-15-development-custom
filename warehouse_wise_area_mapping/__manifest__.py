{
    "name": "Warehouse wise area mapping",
    "version": "1.0.0",
    'category': 'Inventory/Inventory',
    "author": "Odoo Bangladesh",
    "summary": """
         Warehouse wise area mapping for orders""",
    "description": """
        Warehouse wise area mapping for orders.
    """,
    "depends": [
        "stock",
    ],
    "data": [
        "security/wh_area_security.xml",
        "security/ir.model.access.csv",
        "views/wh_area_mapping_view.xml",
        "views/wh_area_mapping_menu.xml",
    ],
    "installable": True,
    "application": False,
    "auto_install": False,
}
