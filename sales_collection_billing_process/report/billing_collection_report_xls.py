# Author Mufti Muntasir Ahmed 25-04-2018


import xlwt
import datetime
from openerp.osv import orm
from openerp.report import report_sxw
from openerp.addons.report_xls.report_xls import report_xls
from openerp.addons.report_xls.utils import rowcol_to_cell, _render
from openerp.tools.translate import translate, _
import logging
_logger = logging.getLogger(__name__)


_ir_translation_name = 'billing.collection.xls'


class billing_coll_xls_parser(report_sxw.rml_parse):

    def __init__(self, cr, uid, name, context):
        super(billing_coll_xls_parser, self).__init__(
            cr, uid, name, context=context)
        move_obj = self.pool.get('bill.collection.billing.process')
        self.context = context
        wanted_list = move_obj._report_xls_fields(cr, uid, context)
        template_changes = move_obj._report_xls_template(cr, uid, context)
        self.localcontext.update({
            'datetime': datetime,
            'wanted_list': wanted_list,
            'template_changes': template_changes,
            '_': self._,
        })

    def _(self, src):
        lang = self.context.get('lang', 'en_US')
        return translate(self.cr, _ir_translation_name, 'report', lang, src) \
            or src

    def set_context(self, objects, data, ids, report_type=None):
        ids = self.context.get('active_ids')
        bill_obj = self.pool['bill.collection.billing.process']
        context = self.context

        self.localcontext.update({
            # 'docs': docs,
            # 'time': time,
            # 'getLines': self._lines_get,
            # 'due': due,
            # 'paid': paid,
            # 'prev_balance': init_balance,
            # 'date_range': str(st_date) + ' to ' + str(end_date),
            # 'mat': mat,
            # 'text':text_output
        })
        self.context = context

        return super(billing_coll_xls_parser, self).set_context(objects, data, ids, report_type)


class billing_coll_xls(report_xls):

    def __init__(self, name, table, rml=False, parser=False, header=True,
                 store=False):
        super(billing_coll_xls, self).__init__(
            name, table, rml, parser, header, store)

        # Cell Styles
        _xs = self.xls_styles
        # header
        rh_cell_format = _xs['bold'] + _xs['fill'] + _xs['borders_all']
        self.rh_cell_style = xlwt.easyxf(rh_cell_format)
        self.rh_cell_style_center = xlwt.easyxf(rh_cell_format + _xs['center'])
        self.rh_cell_style_right = xlwt.easyxf(rh_cell_format + _xs['right'])
        # lines
        aml_cell_format = _xs['borders_all']
        self.aml_cell_style = xlwt.easyxf(aml_cell_format)
        self.aml_cell_style_center = xlwt.easyxf(
            aml_cell_format + _xs['center'])
        self.aml_cell_style_date = xlwt.easyxf(
            aml_cell_format + _xs['left'],
            num_format_str=report_xls.date_format)
        self.aml_cell_style_decimal = xlwt.easyxf(
            aml_cell_format + _xs['right'],
            num_format_str=report_xls.decimal_format)
        # totals
        rt_cell_format = _xs['bold'] + _xs['fill'] + _xs['borders_all']
        self.rt_cell_style = xlwt.easyxf(rt_cell_format)
        self.rt_cell_style_right = xlwt.easyxf(rt_cell_format + _xs['right'])
        self.rt_cell_style_decimal = xlwt.easyxf(
            rt_cell_format + _xs['right'],
            num_format_str=report_xls.decimal_format)

        # XLS Template
        self.col_specs_template = {

            'customer_name': {
                'header': [1, 20, 'text', _render("_('Customer Name')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'company_code': {
                'header': [1, 20, 'text', _render("_('Company Code')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'credit_limit': {
                'header': [1, 20, 'text', _render("_('Credit Limit')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'credit_days': {
                'header': [1, 20, 'text', _render("_('Credit Days')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'classification': {
                'header': [1, 20, 'text', _render("_('Classification')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'salesperson': {
                'header': [1, 20, 'text', _render("_('RM Name')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'assigned_to': {
                'header': [1, 20, 'text', _render("_('Assigned To')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'client_order_ref': {
                'header': [1, 20, 'text', _render("_('Order No')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'warehouse_name': {
                'header': [1, 20, 'text', _render("_('Warehouse Name')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            # 'po_ref': {
            #     'header': [1, 20, 'text', _render("_('PO Ref')")],
            #     'lines': [1, 0, 'text', _render("''")],
            #     'totals': [1, 0, 'text', None]},

            # 'date_order': {
            #     'header': [1, 20, 'text', _render("_('Order Placing Date')")],
            #     'lines': [1, 0, 'text', _render("''")],
            #     'totals': [1, 0, 'text', None]},

            'date_confirm': {
                'header': [1, 20, 'text', _render("_('Order Confirmation Date')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'amount_total': {
                'header': [1, 20, 'text', _render("_('Order Value / Amount')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'final_delivery_date': {
                'header': [1, 20, 'text', _render("_('Final Delivery Date')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'delivery_challan_uploaded': {
                'header': [1, 20, 'text', _render("_('Delivery Challan uploaded')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'invoice_nos': {
                'header': [1, 20, 'text', _render("_('Invoice No')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'invoice_dates': {
                'header': [1, 20, 'text', _render("_('Invoice Dates')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'invoice_values': {
                'header': [1, 20, 'text', _render("_('Invoice Values')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'delivery_dates': {
                'header': [1, 20, 'text', _render("_('Delivery Dates')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'final_inv_date': {
                'header': [1, 20, 'text', _render("_('Final Summary Date')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'final_inv_no': {
                'header': [1, 20, 'text', _render("_('Final Summary No')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'delivered_amount': {
                'header': [1, 20, 'text', _render("_('Delivered Amount')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'return_amount': {
                'header': [1, 20, 'text', _render("_('Return Amount')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'order_status': {
                'header': [1, 20, 'text', _render("_('Order Status')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'bill_sub_target_date': {
                'header': [1, 20, 'text', _render("_('Bill Submission Target Date')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'actual_sub_date': {
                'header': [1, 20, 'text', _render("_('Actual Submitted Date')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'sub_comment': {
                'header': [1, 20, 'text', _render("_('Submission Comment')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'billing_pattern': {
                'header': [1, 20, 'text', _render("_('Billing Pattern')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'bill_sub_date': {
                            'header': [1, 20, 'text', _render("_('System Input Date')")],
                            'lines': [1, 0, 'text', _render("''")],
                            'totals': [1, 0, 'text', None]},

            'bill_rec_copy_uploaded': {
                'header': [1, 20, 'text', _render("_('Bill Received Copy Uploaded')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'collection_aging': {
                'header': [1, 20, 'text', _render("_('Collection Aging')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'final_amount': {
                'header': [1, 20, 'text', _render("_('Final Amount')")],
                'lines': [1, 0, 'text', _render("''")],
                'totals': [1, 0, 'text', None]},

            'expected_payment_date': {
                            'header': [1, 20, 'text', _render("_('Expected Payment Date')")],
                            'lines': [1, 0, 'text', _render("''")],
                            'totals': [1, 0, 'text', None]},

            'total_paid_amount': {
                            'header': [1, 20, 'text', _render("_('Total Paid Amount')")],
                            'lines': [1, 0, 'text', _render("''")],
                            'totals': [1, 0, 'text', None]},

            'unpaid_amount': {
                            'header': [1, 20, 'text', _render("_('Unpaid Amount')")],
                            'lines': [1, 0, 'text', _render("''")],
                            'totals': [1, 0, 'text', None]},


            'paid_date': {
                            'header': [1, 20, 'text', _render("_('Paid Date')")],
                            'lines': [1, 0, 'text', _render("''")],
                            'totals': [1, 0, 'text', None]},

            'paid_amount': {
                            'header': [1, 20, 'text', _render("_('Paid Amount')")],
                            'lines': [1, 0, 'text', _render("''")],
                            'totals': [1, 0, 'text', None]},

            'cq_number': {
                            'header': [1, 20, 'text', _render("_('CQ Number')")],
                            'lines': [1, 0, 'text', _render("''")],
                            'totals': [1, 0, 'text', None]},

            'cq_money_receipt_attach': {
                            'header': [1, 20, 'text', _render("_('CQ & Money Receipt File Attach Status')")],
                            'lines': [1, 0, 'text', _render("''")],
                            'totals': [1, 0, 'text', None]},

            'partial_paid_amount': {
                            'header': [1, 20, 'text', _render("_('Partial Paid Amount')")],
                            'lines': [1, 0, 'text', _render("''")],
                            'totals': [1, 0, 'text', None]},

            'billing_status': {
                            'header': [1, 20, 'text', _render("_('Billing Status')")],
                            'lines': [1, 0, 'text', _render("''")],
                            'totals': [1, 0, 'text', None]},

            'payment_type': {
                            'header': [1, 20, 'text', _render("_('Payment Type')")],
                            'lines': [1, 0, 'text', _render("''")],
                            'totals': [1, 0, 'text', None]},

            'cancel_date': {
                            'header': [1, 20, 'text', _render("_('Cancel Date')")],
                            'lines': [1, 0, 'text', _render("''")],
                            'totals': [1, 0, 'text', None]},

            'cancel_amount': {
                            'header': [1, 20, 'text', _render("_('Cancel Amount')")],
                            'lines': [1, 0, 'text', _render("''")],
                            'totals': [1, 0, 'text', None]},

            'cancel_reason': {
                            'header': [1, 20, 'text', _render("_('Cancel Reason')")],
                            'lines': [1, 0, 'text', _render("''")],
                            'totals': [1, 0, 'text', None]},
        }

    def generate_xls_report(self, _p, _xs, data, objects, wb):

        wanted_list = _p.wanted_list
        self.col_specs_template.update(_p.template_changes)
        _ = _p._

        context = self.context
        st_date = data.get('form').get('date_from')
        end_date = data.get('form').get('date_to')
        context['start_date'] = st_date
        context['end_date'] = end_date
        self.context = context
        # date_range = _("Date: %s to %s" % (st_date, end_date))

        # report_name = objects[0]._description or objects[0]._name
        report_name = _("Billing Collection Report")
        ws = wb.add_sheet(report_name[:31])
        ws.panes_frozen = True
        ws.remove_splits = True
        ws.portrait = 0  # Landscape
        ws.fit_width_to_pages = 1
        row_pos = 0

        # set print header/footer
        ws.header_str = self.xls_headers['standard']
        ws.footer_str = self.xls_footers['standard']

        # Title
        cell_style = xlwt.easyxf(_xs['xls_title'])
        c_specs = [
            ('report_name', 4, 3, 'text', report_name),
        ]
        row_data = self.xls_row_template(c_specs, ['report_name'])
        row_pos = self.xls_write_row(
            ws, row_pos, row_data, row_style=cell_style)
        row_pos += 1

        cell_style = xlwt.easyxf(_xs['xls_title'])
        """
        c_specs = [
            ('date_range', 4, 3, 'text', date_range),
        ]
        """
        # row_data = self.xls_row_template(c_specs, ['date_range'])
        row_pos = self.xls_write_row(
            ws, row_pos, row_data, row_style=cell_style)
        row_pos += 1

        # Column headers
        c_specs = map(lambda x: self.render(
            x, self.col_specs_template, 'header', render_space={'_': _p._}),
            wanted_list)
        row_data = self.xls_row_template(c_specs, [x[0] for x in c_specs])
        row_pos = self.xls_write_row(
            ws, row_pos, row_data, row_style=self.rh_cell_style,
            set_column_size=True)
        ws.set_horz_split_pos(row_pos)

        odj_ids = self.pool("bill.collection.billing.process").search(self.cr, self.uid,
                                                           [
                                                            ('create_date', '>=', st_date +' 00:00:00'),
                                                            ('create_date', '<=', end_date + ' 23:59:59')], context=context)

        objects = self.pool("bill.collection.billing.process").browse(self.cr, self.uid, odj_ids,
                                                         context=context)

        for order in objects:

            c_specs = map(
                lambda x: self.render(x, self.col_specs_template, 'lines'),
                wanted_list)

            for list_data in c_specs:

                # if str(list_data[0]) == str('date_order'):
                #     list_data[4] = str(order.order_id.date_order)

                if str(list_data[0]) == str('date_confirm'):
                    list_data[4] = str(order.order_id.date_confirm)

                if str(list_data[0]) == str('client_order_ref'):
                    list_data[4] = str(order.order_id.client_order_ref)

                if str(list_data[0]) == str('warehouse_name'):

                    list_data[4] = str(order.order_id.warehouse_id.name)

                """
                if str(list_data[0]) == str('odoo_so'):
                    list_data[4] = str(order.order_id.name)
                """
                # if str(list_data[0]) == str('po_ref'):
                #     list_data[4] = str(order.order_id.x_puchase_ref_cus)

                # finding customer info
                parent = order.order_id.partner_id

                for i in range(5):
                    if len(parent.parent_id) == 1:
                        parent = parent.parent_id
                    else:
                        parent = parent
                        break

                if str(list_data[0]) == str('customer_name'):
                    list_data[4] = str(parent.name)

                if str(list_data[0]) == str('company_code'):

                    if parent.company_code:
                        list_data[4] = str(parent.company_code)
                    else:
                        list_data[4] = ""

                if str(list_data[0]) == str('credit_limit'):
                    list_data[4] = str(parent.credit_limit)

                if str(list_data[0]) == str('credit_days'):
                    list_data[4] = str(parent.credit_days)

                if str(list_data[0]) == str('classification'):

                    if order.order_id.customer_classification:
                        list_data[4] = str(order.order_id.customer_classification)
                    else:
                        list_data[4] = ""

                if str(list_data[0]) == str('salesperson'):

                    if parent.user_id:
                        list_data[4] = str(parent.user_id.partner_id.name)
                    else:
                        list_data[4] = ''

                if str(list_data[0]) == str('assigned_to'):

                    if order.assigned_rm.partner_id.name:
                        list_data[4] = str(order.assigned_rm.partner_id.name)
                    else:
                        list_data[4] = ""

                if str(list_data[0]) == str('amount_total'):
                    list_data[4] = str(order.order_id.amount_total)

                if str(list_data[0]) == str('order_status'):
                    list_data[4] = str(order.order_id.state)


                if str(list_data[0]) == str('final_delivery_date'):

                    # date_done
                    stock_obj = self.pool.get('stock.picking')
                    stock_list = stock_obj.search(self.cr, self.uid, [('origin', '=', str(order.order_id.name))])
                    stock_list.sort(reverse=True)
                    if stock_list:
                        stock = stock_obj.browse(self.cr, self.uid, stock_list[0], self.context)
                    if stock.date_done:
                        list_data[4] = str(stock.date_done)
                    else:
                        list_data[4] = ''

                if str(list_data[0]) == str('delivery_challan_uploaded'):
                    # challan_uploded
                    inv_obj = self.pool.get('account.invoice')
                    inv_list = inv_obj.search(self.cr, self.uid, [('name', '=', str(order.order_id.client_order_ref))])

                    challan_uploaded = False
                    for inv in inv_obj.browse(self.cr, self.uid, inv_list, context=context):
                        if inv.challan_uploded:
                            challan_uploaded = True
                        else:
                            challan_uploaded = False
                            break

                    list_data[4] = str(challan_uploaded)

                invoices = ''
                if str(list_data[0]) == str('invoice_nos'):
                    acc_inv_obj = self.pool.get('account.invoice')
                    acc_inv_list = [inv.invoice.id for inv in order.final_invoice]
                    acc_invs = acc_inv_obj.browse(self.cr, self.uid, acc_inv_list, context=self.context)

                    for inv in acc_invs:
                        if str(inv.state) != 'draft' and str(inv.state != 'cancel'):
                            invoices += str(inv.number) + ", " if inv.number else ''

                    list_data[4] = str(invoices)

                invoice_dates = ''
                if str(list_data[0]) == str('invoice_dates'):

                    acc_inv_obj = self.pool.get('account.invoice')
                    acc_inv_list = [inv.invoice.id for inv in order.final_invoice]
                    acc_invs = acc_inv_obj.browse(self.cr, self.uid, acc_inv_list, context=self.context)

                    for inv in acc_invs:
                        if str(inv.state) != 'draft' and str(inv.state != 'cancel'):

                            invoice_dates += str(inv.date_invoice) + ", " if inv.date_invoice else ''
                    list_data[4] = str(invoice_dates)

                invoice_values = ''
                if str(list_data[0]) == str('invoice_values'):

                    acc_inv_obj = self.pool.get('account.invoice')
                    acc_inv_list = [inv.invoice.id for inv in order.final_invoice]
                    acc_invs = acc_inv_obj.browse(self.cr, self.uid, acc_inv_list, context=self.context)

                    for inv in acc_invs:
                        if str(inv.state) != 'draft' and str(inv.state != 'cancel'):
                            invoice_values += str(inv.amount_total) + ", " if inv.amount_total else ''

                    list_data[4] = str(invoice_values)

                delivery_dates = ''
                if str(list_data[0]) == str('delivery_dates'):

                    acc_inv_obj = self.pool.get('account.invoice')
                    acc_inv_list = [inv.invoice.id for inv in order.final_invoice]
                    acc_invs = acc_inv_obj.browse(self.cr, self.uid, acc_inv_list, context=self.context)

                    for inv in acc_invs:
                        if str(inv.state) != 'draft' and str(inv.state != 'cancel'):
                            delivery_dates += str(inv.delivery_date) + ", " if inv.delivery_date else ''

                    list_data[4] = str(delivery_dates)

                if str(list_data[0]) == str('final_inv_date'):
                    list_data[4] = str(order.create_date)

                if str(list_data[0]) == str('final_inv_no'):
                    list_data[4] = str(order.name)

                if str(list_data[0]) == str('delivered_amount'):
                    list_data[4] = str(order.delivered_amount)

                if str(list_data[0]) == str('unpaid_amount'):
                    unpaid_amount = order.billing_amount -order.paid_amount
                    list_data[4] = str(unpaid_amount)

                if str(list_data[0]) == str('total_paid_amount'):
                    list_data[4] = str(order.paid_amount)

                if str(list_data[0]) == str('return_amount'):
                    list_data[4] = str(order.return_amount)

                if str(list_data[0]) == str('bill_sub_target_date'):
                    exp_sub_date = ''
                    for assignee in order.assignee:
                        if assignee.exp_sub_date:
                            exp_sub_date = str(assignee.exp_sub_date)

                    list_data[4] = exp_sub_date

                # attachment info
                attachment = self.pool.get('ir.attachment')
                attach_list = attachment.search(self.cr, self.uid, [('res_model', '=', 'bill.collection.billing.process'), ('res_id', '=', order.id)])

                if str(list_data[0]) == str('actual_sub_date'):
                    list_data[4] = str(order.actual_sub_date)

                if str(list_data[0]) == str('sub_comment'):

                    sub_comment = ''
                    if order.assignee:
                        sub_comment = order.assignee[0].comment
                        list_data[4] = str(sub_comment)
                    else:
                        list_data[4] = sub_comment


                if str(list_data[0]) == str('bill_sub_date'):
                    # bill submitted date = bill received copy upload
                    att_file_date = ''
                    if attach_list:
                        for attach in attach_list:
                            att = attachment.browse(self.cr, self.uid, attach, context=self.context)
                            att_file_date += str(att.create_date) + ", " if att.create_date else ''

                        list_data[4] = str(att_file_date)
                    else:
                        list_data[4] = ''

                if str(list_data[0]) == str('bill_rec_copy_uploaded'):
                    # bill submitted date = bill received copy upload

                    if attach_list:
                        list_data[4] = 'True'
                    else:
                        list_data[4] = 'False'

                attach_col_aging = ''
                if str(list_data[0]) == str('collection_aging'):
                    unpaid_amount = order.billing_amount - order.paid_amount

                    if unpaid_amount!=0.00:
                    #     for attach in attach_list:
                    #         att = attachment.browse(self.cr, self.uid, attach, context=self.context)
                        if order.actual_sub_date:
                            col_aging = datetime.datetime.today() - datetime.datetime.strptime(order.actual_sub_date, '%Y-%m-%d')
                            attach_col_aging += str(col_aging.days) + "days, " if col_aging else ''
                        else:
                            attach_col_aging = ''
                            list_data[4] = str(attach_col_aging)
                        list_data[4] = str(attach_col_aging)
                    else:
                        list_data[4] = 'Done'

                attach_cq = False
                if str(list_data[0]) == str('cq_money_receipt_attach'):
                    if attach_list:
                        for attach in attach_list:
                            att = attachment.browse(self.cr, self.uid, attach, context=self.context)
                            attach_file_name = att.name
                            if 'CQ' in attach_file_name or 'Money' in attach_file_name:
                                attach_cq = True
                                break
                            else:
                                attach_cq = False

                        list_data[4] = str(attach_cq)
                    else:
                        list_data[4] = ''

                if str(list_data[0]) == str('final_amount'):
                    list_data[4] = str(order.billing_amount)

                if str(list_data[0]) == str('billing_pattern'):
                    if order.order_id.so_billing_pattern == 'on_demand':
                        billing_pattern = 'On Demand'
                    elif order.order_id.so_billing_pattern == 'on_delivery':
                        billing_pattern = 'On Delivery'
                    else:
                        billing_pattern = ''

                    list_data[4] = str(billing_pattern)


                if str(list_data[0]) == str('expected_payment_date'):
                    expected_payment_date = str(order.exp_pay_date)
                    list_data[4] = str(expected_payment_date.replace("False", ""))


                paid_date=''
                if str(list_data[0]) == str('paid_date'):
                    for payment_dt in order.payment:
                        paid_date += str(payment_dt.paid_date) + ", " if payment_dt.paid_date else ''

                    list_data[4] = str(paid_date)

                paid_amount = ''
                if str(list_data[0]) == str('paid_amount'):
                    for paid_amnt in order.payment:
                        paid_amount += str(paid_amnt.paid_amount) + ", " if paid_amnt.paid_amount else ''
                    list_data[4] = str(paid_amount)

                memo_number=''
                if str(list_data[0]) == str('cq_number'):
                    for memo_num in order.payment:
                        memo_number += str(memo_num.memo) + ", " if memo_num.memo else ''
                    list_data[4] = str(memo_number)


                if str(list_data[0]) == str('partial_paid_amount'):
                    if str(order.state) == 'partially_paid':
                        list_data[4] = str(order.paid_amount)


                if str(list_data[0]) == str('billing_status'):
                    list_data[4] = str(order.state)

                payment_type=''
                if str(list_data[0]) == str('payment_type'):
                    for payment_types in order.payment:
                        payment_type += str(payment_types.type) + ", " if payment_types.type else ''
                    list_data[4] = str(payment_type)


                if str(list_data[0]) == str('cancel_date'):
                    cancel_date = str(order.cancel_time)
                    list_data[4] = str(cancel_date.replace("False", ""))


                if str(list_data[0]) == str('cancel_amount'):
                    if str(order.state) == 'cancel':
                        list_data[4] = str(order.billing_amount)

                if str(list_data[0]) == str('cancel_reason'):
                    cancel_reason = str(order.cancel_bill_process_reason)
                    list_data[4] = str(cancel_reason.replace("False", ""))

            row_data = self.xls_row_template(c_specs, [x[0] for x in c_specs])
            row_pos = self.xls_write_row(
                ws, row_pos, row_data, row_style=self.aml_cell_style)


billing_coll_xls('report.billing.collection.xls',
                    'bill.collection.billing.process',
                    parser=billing_coll_xls_parser)
