from datetime import datetime, timedelta
from odoo import api, fields, models, _ , exceptions
import operator
from operator import itemgetter, attrgetter
import datetime


class SaleOrder(models.Model):

    _inherit = 'sale.order'

    ass_for_bill_coll = fields.Boolean(string="Assigned for Billing Collection")
    so_billing_pattern = fields.Selection([('on_delivery', 'On Delivery'), ('on_demand', 'On Demand')],
                                          'Billing Pattern')

    @api.model
    def create(self,vals):
        order_info = super(SaleOrder, self).create(vals)

        parent = order_info.partner_id
        # company = False

        for i in range(5):
            if len(parent.parent_id) == 1:
                parent = parent.parent_id

            else:
                parent = parent
                break

        if parent:

            if parent.cus_on_demand or parent.cus_on_demand == True:
                so_billing_pattern = 'on_demand'

            else:
                so_billing_pattern = 'on_delivery'

            order_info.write({'so_billing_pattern': so_billing_pattern})

        return order_info


    def generate_final_summary_action(self):

        ids = self.env.context['active_ids']
        bill_pro_obj = self.env['bill.collection.billing.process']
        acc_inv_obj = self.env['account.move']
        stock_obj = self.env['stock.picking']

        # first check if any one of the so final invoice stored in "bill.collection.billing.process"
        # if so, do not proceed. show warning
        try:
            for so in self:
                if so.client_order_ref:
                    so.client_order_ref = so.client_order_ref
                else:
                    so.client_order_ref = so.id

                bill_pro_list = bill_pro_obj.search([('mag_name', '=', str(so.client_order_ref)), ('state', '!=', 'cancel')])
                if bill_pro_list:
                    raise exceptions.ValidationError(_('Final invoice already been created for {0}. Can not create again!!!'
                                                       ).format(str(so.client_order_ref)))

                else:
                    # create final invoice
                    bill_pro_data = {
                        'name': str(so.name),
                        'mag_name': str(so.client_order_ref),
                        'warehouse_name': str(so.warehouse_id.name),
                        'order_amount': str(so.amount_total),
                        'billing_amount': str(so.invoiced_amount),
                        'order_id': str(so.id),
                        'return_amount': 0.0,
                        'delivered_amount': 0.0,
                        'unpaid_amount': str(so.amount_total),
                        'paid_amount': 0.0,
                        'state': 'draft',
                        'partner_id': so.partner_id.id,
                    }

                    stock_list = stock_obj.search([('origin', '=', str(so.name)),('state', '!=', 'cancel'),
                                                  ('date_done', '!=', None)])
                    # stock_list.sorted(reverse=True)
                    stock_list = sorted(stock_list, key=attrgetter('date_done'), reverse=True)

                    if stock_list:
                        for stock in stock_list:
                            if stock.date_done:
                                att_file_date = stock.date_done.date()
                                assigned_time = stock.date_done
                                break

                    else:
                        att_file_date = datetime.datetime.today().date()
                        assigned_time = datetime.datetime.today()


                    if so.so_billing_pattern == 'on_delivery':
                        bill_pro_data.update({
                            'state': 'submitted',
                            'assigned_rm': so.user_id.id,
                            'assigned_time': assigned_time,
                            'exp_sub_date': att_file_date,
                            'actual_sub_date':att_file_date,
                            'exp_pay_date': (att_file_date +
                                             timedelta(days=int(so.partner_id.credit_days))),
                            'submitted_by': self._uid,
                            'submitted_time':att_file_date,
                        })

                    invoice_line_list = list()

                    # acc_inv_list = acc_inv_obj.search(cr, uid, [('name', '=', str(so.client_order_ref))])
                    acc_inv_list = [inv.id for inv in so.invoice_ids]
                    acc_invs = acc_inv_obj.browse(acc_inv_list)

                    returned_amount = 0
                    delivered_amount = 0

                    for inv in acc_invs:
                        inv_type = 'delivered'
                        if str(inv.move_type) in ('out_invoice','out_refund'):

                            if str(inv.move_type) == str('out_invoice'):
                                inv_type = 'delivered'
                                delivered_amount = delivered_amount + inv.amount_total
                            elif str(inv.move_type) == str('out_refund'):
                                inv_type = 'return'
                                returned_amount = returned_amount + inv.amount_total

                            for line_item in inv.line_ids:
                                product_id = line_item.product_id.id if line_item.product_id else False

                                if "Free Shipping" not in str(line_item.name) and "Discount" not in str(line_item.name) \
                                        and "Discount" not in str(line_item.name) and product_id and \
                                        self.env['product.product'].search([('id','=',product_id)]) \
                                        and line_item.price_subtotal_with_vat>0:

                                    invoice_line_list.append([0, False,{
                                             'type': inv_type, # delivered / return
                                             'product_id': line_item.product_id.id,
                                             'invoice': inv.id,
                                             'invoice_no': inv.name,
                                             'description': str(line_item.name),
                                             'date': line_item.create_date,
                                             'quantity': line_item.quantity,
                                             'uom': str(line_item.product_uom_id.name),
                                             'unit_price': line_item.price_unit,
                                             'discount': line_item.discount,
                                             'tax':line_item.five_vat,
                                             'amount': line_item.price_subtotal_with_vat,
                                             'state': 'draft' if so.so_billing_pattern == 'on_demand' else 'submitted',

                                         }])

                    bill_pro_data['return_amount'] = returned_amount
                    bill_pro_data['delivered_amount'] = delivered_amount

                    bill_pro_data['final_invoice'] = invoice_line_list

                    billing_id = bill_pro_obj.create(bill_pro_data)

                    if billing_id:
                        res1 = billing_id.id
                        number_st = 'FS-10' + str(res1)
                        self._cr.execute("update sale_order set ass_for_bill_coll=TRUE where id=%s", ([so.id]))
                        self._cr.commit()
                        self._cr.execute("update bill_collection_billing_process set name=%s where id=%s", ([number_st, res1]))
                        self._cr.commit()

                        # insert into log list view
                        bill_log_obj = self.env['coll.bill.process.log']
                        bill_log_data = {
                            'name': number_st,
                            'mag_name': str(so.client_order_ref),
                            'warehouse_name': str(so.warehouse_id.name),
                            'order_amount':  str(so.amount_total),
                            'billing_amount': str(so.invoiced_amount),
                            'return_amount': str(returned_amount),
                            'delivered_amount': str(delivered_amount),
                            'unpaid_amount': str(so.invoiced_amount),
                            'state': 'draft',
                            'bill_process_id': res1,
                            'order_id': so.id,
                        }
                        bill_log_obj.create(bill_log_data)

                    # search the selected orders "open" invoices and create final invoice.
                    # Final invoice stored in "bill.collection.billing.process"
                    # SO37415

                    if so.so_billing_pattern == 'on_delivery':
                        ass_obj = self.env["bill.assignee.line"]
                        assignee_data = {
                            'col_bill_pro_id': res1,
                            'assigned_date': assigned_time,
                            'assigned_rm': so.user_id.id,
                            'actual_sub_date': att_file_date,
                            'exp_sub_date': att_file_date,
                            'exp_pay_date': (att_file_date + timedelta(days=int(so.partner_id.credit_days))),
                            'assignee_status': True,
                            'state': 'assigned'
                        }

                        ass_id = ass_obj.sudo().create(assignee_data)
                        # create in payment line
                        payment_data = {
                            'col_bill_pro_id': res1,
                            'assignee_line_id': ass_id.id,
                            'paid_amount': 0.00,
                        }

                        payment_line = self.env["bill.payment.line"]
                        pay_id = payment_line.sudo().create(payment_data)
        except:
            self._cr.rollback()


class CollectionBillingProcess(models.Model):
    _name = "bill.collection.billing.process"
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _description = "Collection Billing Process"

    name = fields.Char('Name')
    mag_name = fields.Char('Magento Order No')
    order_amount = fields.Float('Order Amount')
    warehouse_name = fields.Char('Warehouse Name')
    billing_amount = fields.Float('Final Amount')
    return_amount = fields.Float('Total Return Amount')
    delivered_amount = fields.Float('Total Delivered Amount')
    unpaid_amount = fields.Float('Unpaid Amount')
    paid_amount = fields.Float('Total Paid Amount')
    final_invoice = fields.One2many('bill.final.invoice.line', 'col_bill_pro_id', 'Final Invoice') # foreign model
    payment = fields.One2many('bill.payment.line', 'col_bill_pro_id', 'Payment')  # foreign model
    assignee = fields.One2many('bill.assignee.line', 'col_bill_pro_id', 'Assignee')  # foreign model
    # 're_attempt': fields.one2many('bill.re.attempt.line', 'col_bill_pro_id', 'Re-attempt'),  # foreign model
    order_id = fields.Many2one('sale.order','Sale Order')
    # so_salesperson = fields.related('order_id', 'custom_salesperson', 'name', type='char', string='Sales Person')
    so_salesperson = fields.Many2one('res.users', 'Sales Person')
    partner_id = fields.Many2one('res.partner','Customer')
    assigned_rm = fields.Many2one('res.users', 'Assigned RM')
    actual_sub_date = fields.Date('Actual Submitted Date')
    exp_sub_date = fields.Date('Expected Submission Date')
    exp_pay_date = fields.Date('Expected Payment Date')
    payment_assigned_rm = fields.Many2one('res.users', 'Assigned RM (Payment)')
    state = fields.Selection([
        ('draft', 'Draft'),
        ('assigned', 'Assigned'),
        ('submitted', 'Submitted'),
        ('pending_payment', 'Pending Payment'),
        ('partially_paid', 'Partially Paid'),
        ('paid', 'Paid'),
        ('cancel', 'Cancel'),
    ], 'Status', select=True)
    assigned_by = fields.Many2one('res.users', 'Assigned By')
    assigned_time = fields.Datetime('Assigned Date')
    submitted_by = fields.Many2one('res.users', 'Submitted By')
    submitted_time = fields.Datetime('Submitted Date')
    pending_payment_by = fields.Many2one('res.users', 'Pending Payment By')
    pending_payment_time = fields.Datetime('Pending Payment Date')
    paid_by = fields.Many2one('res.users', 'Paid By')
    paid_time = fields.Datetime('Paid Date')
    cancel_by = fields.Many2one('res.users', 'Cancel By')
    cancel_time = fields.Datetime('Cancel Date')
    partially_paid_by = fields.Many2one('res.users', 'Partially Paid By')
    partially_paid_time = fields.Datetime('Partially Paid Date')

    cancel_bill_process_date = fields.Datetime('Cancel bill Process Date')
    cancel_bill_process_by = fields.Many2one('res.users', 'Cancel bill Process By')
    cancel_bill_process_reason = fields.Text('Cancel bill Process Reason')

    def cancel_bill(self,single_id):

        cancel_bill_query = "UPDATE bill_collection_billing_process SET state='cancel', cancel_by={0}, " \
                            "cancel_time='{1}' WHERE id={2}".format(self._uid,
            str(fields.datetime.now()), single_id)
        self._cr.execute(cancel_bill_query)
        self._cr.commit()

        so_obj = self.env["sale.order"]

        so_obj.browse(self.order_id).write({"ass_for_bill_coll": False})

        return True

    def rm_unique(self,ids):
        assigned_rm=[]
        bill_pro = self.browse(ids)
        for fs_bill in bill_pro:
            for row in fs_bill.assignee:
                if row.assignee_status == True:
                    assigned_rm.append(row.assigned_rm.id)

        assigned_rm=set(assigned_rm)

        return assigned_rm


class BillFinalInvoice(models.Model):
    _name = "bill.final.invoice.line"
    _description = "Final Summary"

    col_bill_pro_id = fields.Many2one('bill.collection.billing.process', 'Billing Collection ID',
                                      ondelete='cascade', select=True, readonly=True)
    type = fields.Char('Type')  # delivered / return
    product_id = fields.Many2one('product.product', 'Product', readonly=True)
    invoice = fields.Many2one('account.invoice', 'Invoice', readonly=True)
    invoice_no = fields.Char('Invoice No')
    description = fields.Text('Memo')
    date = fields.Date('Date')
    quantity = fields.Float('Quantity')
    uom = fields.Char('UOM')
    unit_price = fields.Float('Unit Price')
    discount = fields.Float('Discount')
    tax = fields.Float('Tax')
    amount = fields.Float('Amount')
    state = fields.Selection([
        ('draft', 'Draft'),
        ('assigned', 'Assigned'),
        ('submitted', 'Submitted'),
        ('pending_payment', 'Pending Payment'),
        ('partially_paid', 'Partially Paid'),
        ('paid', 'Paid'),
        ('cancel', 'Cancel'),
    ], 'Status', select=True)


class PaymentLine(models.Model):
    _name = "bill.payment.line"
    _description = "Payment Line"

    col_bill_pro_id = fields.Many2one('bill.collection.billing.process', 'Billing Collection ID', ondelete='cascade', select=True, readonly=True)
    assignee_line_id = fields.Many2one('bill.assignee.line', 'Assignee', ondelete='cascade', select=True, readonly=True)
    paid_amount =  fields.Float('Paid Amount')
    memo = fields.Text('Memo')
    paid_date = fields.Date('Paid Date')
    type = fields.Selection([
        ('cash', 'Cash'),
        ('check', 'Check'),
    ], 'Type', select=True)
    state = fields.Selection([
        ('draft', 'Draft'),
        ('assigned', 'Assigned'),
        ('submitted', 'Submitted'),
        ('pending_payment', 'Pending Payment'),
        ('partially_paid', 'Partially Paid'),
        ('paid', 'Paid'),
        ('cancel', 'Cancel'),
    ], 'Status', select=True)


class AssigneeLine(models.Model):
    _name = "bill.assignee.line"
    _description = "Assignee Line"

    col_bill_pro_id = fields.Many2one('bill.collection.billing.process', 'Billing Collection ID', ondelete='cascade', select=True, readonly=True)
    payment = fields.One2many('bill.payment.line', 'assignee_line_id', 'Payment')
    assigned_rm = fields.Many2one('res.users', 'Assigned RM')
    assigned_date = fields.Date('Assigned Date')
    actual_sub_date = fields.Date('Actual Submitted Date')
    exp_sub_date = fields.Date('Expected Submission Date')
    exp_pay_date = fields.Date('Expected Payment Date')
    assignee_status = fields.Boolean("Assignee Status")
    comment = fields.Text('Memo')
    state = fields.Selection([
        ('draft', 'Draft'),
        ('assigned', 'Assigned'),
        ('submitted', 'Submitted'),
        ('pending_payment', 'Pending Payment'),
        ('partially_paid', 'Partially Paid'),
        ('paid', 'Paid'),
        ('cancel', 'Cancel'),
    ], 'Status', select=True)


class BillWizAssignrm(models.Model):
    _name = "bill.wiz.assign.rm"
    _description = "Assign RM Wizard"

    name = fields.Char('Order No')
    mag_name = fields.Char('Magento Order No')
    order_amount = fields.Float('Order Amount')
    assigned_date = fields.Date('Assigned Date')
    assigned_rm = fields.Many2one('res.users', 'Assigned RM')
    actual_sub_date = fields.Date('Actual Submitted Date')
    exp_sub_date = fields.Date('Expected Submission Date')
    state = fields.Selection([
        ('draft', 'Draft'),
        ('assigned', 'Assigned'),
        ('submitted', 'Submitted'),
        ('pending_payment', 'Pending Payment'),
        ('partially_paid', 'Partially Paid'),
        ('paid', 'Paid'),
        ('cancel', 'Cancel'),
    ], 'Status', select=True)

    @api.model
    def default_get(self, fields):

        record = super(BillWizAssignrm, self).default_get(fields)

        ids = self.env.context['active_ids']
        bill_pro_obj = self.env['bill.collection.billing.process']
        bill_pro = bill_pro_obj.browse(ids)
        assign_rm_unique = bill_pro_obj.rm_unique(ids)
        if len(assign_rm_unique)>0:
            raise exceptions.ValidationError(
                                 _('Already assign RM.Please re-assign RM.'))

        if len(bill_pro)==1:
            record['name'] = str(bill_pro.name)
            record['mag_name'] = str(bill_pro.mag_name)
            record['order_amount'] = bill_pro.order_amount
        record['assigned_date'] = datetime.datetime.today().date()
        record['state'] = 'assigned'

        return record

    def save_assign_rm(self):
        ass_obj = self.env["bill.assignee.line"]
        save_assign = self

        for fs_id in self.env.context['active_ids']:
            assignee_data = {
                'col_bill_pro_id': fs_id,
                'assigned_date': save_assign.assigned_date,
                'assigned_rm': save_assign.assigned_rm.id,
                'exp_sub_date': save_assign.exp_sub_date,
                'assignee_status': True,
                'state': save_assign.state
            }

            ass_id = ass_obj.create(assignee_data)

            state_update_query = "UPDATE bill_collection_billing_process SET state='assigned', assigned_by={0}, " \
                                 "assigned_time='{1}', assigned_rm='{2}',exp_sub_date='{3}' WHERE id={4}".\
                format(self._uid, str(fields.datetime.now()), save_assign.assigned_rm.id,save_assign.exp_sub_date, fs_id)
            self._cr.execute(state_update_query)
            self._cr.commit()

            bill_log_obj = self.env['coll.bill.process.log']
            log_id = bill_log_obj.search([('bill_process_id', '=', fs_id)])
            bill_log_data = {
                'assigned_to': save_assign.assigned_rm.id,
                'assigned_time': save_assign.exp_sub_date,
                'state': 'assigned',
            }
            log_id.write(bill_log_data)

            bill_pro_data = {
                'exp_sub_date': save_assign.exp_sub_date
            }
            bill_pro_obj = self.env["bill.collection.billing.process"]
            bill_pro_obj.browse(fs_id).write( bill_pro_data)

        return True


class BillWizReAssignrm(models.Model):
    _name = "bill.wiz.re.assign.rm"
    _description = "Re-assign RM Wizard"

    name = fields.Char('Order No')
    mag_name = fields.Char('Magento Order No')
    order_amount = fields.Float('Order Amount')
    assigned_date = fields.Date('Assigned Date')
    assigned_rm = fields.Many2one('res.users', 'Assigned RM')
    actual_sub_date = fields.Date('Actual Submitted Date')
    exp_sub_date = fields.Date('Expected Submission Date')

    @api.model
    def default_get(self,fields):

        record = super(BillWizReAssignrm, self).default_get(fields)

        ids = self.env.context['active_ids']
        bill_pro_obj = self.env['bill.collection.billing.process']
        bill_pro = bill_pro_obj.browse(ids)
        assign_rm_unique = bill_pro_obj.rm_unique(ids)
        if len(assign_rm_unique)==0:
            raise exceptions.ValidationError(
                                 _('Please assign RM.'))

        if len(bill_pro) == 1:
            record['name'] = str(bill_pro.name)
            record['mag_name'] = str(bill_pro.mag_name)
            record['order_amount'] = bill_pro.order_amount
        record['assigned_date'] = datetime.datetime.today().date()

        return record

    def save_re_assign_rm(self):

        ass_obj = self.env["bill.assignee.line"]
        for fs_id in self.env.context['active_ids']:
            re_assignee_data = {
                'col_bill_pro_id': fs_id,
                'assigned_date': self.exp_sub_date,
                'assigned_rm': self.assigned_rm.id,
                'exp_sub_date': self.exp_sub_date,
                'assignee_status': True
            }

            ass_line_list = ass_obj.search([('col_bill_pro_id', '=', fs_id)])
            for line in ass_line_list:
                ass_obj.browse(line.id).write({'assignee_status': False})

            ass_obj.create(re_assignee_data)

        return True


class BillWizAssignrmSubmission(models.Model):
    _name = "bill.wiz.assign.rm.submission"
    _description = "Assign RM Submission Wizard"

    name = fields.Char('Order No')
    mag_name = fields.Char('Magento Order No')
    order_amount = fields.Float('Order Amount')
    assigned_date = fields.Date('Assigned Date')
    assigned_rm = fields.Many2one('res.users', 'Assigned RM')
    actual_sub_date = fields.Date('Actual Submitted Date')
    exp_sub_date = fields.Date('Expected Submission Date')
    exp_pay_date = fields.Date('Expected Payment Date')
    comment = fields.Text('Comment')
    state = fields.Selection([
        ('draft', 'Draft'),
        ('assigned', 'Assigned'),
        ('submitted', 'Submitted'),
        ('pending_payment', 'Pending Payment'),
        ('partially_paid', 'Partially Paid'),
        ('paid', 'Paid'),
        ('cancel', 'Cancel'),
    ], 'Status', select=True)

    @api.model
    def default_get(self ,fields):

        record = super(BillWizAssignrmSubmission, self).default_get(fields)

        ids = self.env.context['active_ids']
        bill_pro_obj = self.env['bill.collection.billing.process']
        bill_pro_data = bill_pro_obj.browse(ids)
        assign_rm_unique = bill_pro_obj.rm_unique( ids)
        if len(assign_rm_unique)==1:
            for bill_pro in bill_pro_data:

                for line in bill_pro.assignee:
                    if line.assignee_status:
                        record['name'] = str(bill_pro.name)
                        record['assigned_rm'] = line.assigned_rm.id
                        record['mag_name'] = str(bill_pro.mag_name)
                        record['order_amount'] = bill_pro.order_amount
                        record['actual_sub_date'] = line.actual_sub_date
                        record['exp_sub_date'] = line.exp_sub_date
                record['name'] = str(bill_pro.name)
                record['mag_name'] = str(bill_pro.mag_name)
                record['order_amount'] = bill_pro.order_amount
                record['state'] = 'submitted'

        elif len(assign_rm_unique)==0:
            raise exceptions.ValidationError(
                                 _('Please assign the RM'))
        else:
            raise exceptions.ValidationError(
                                 _('Please select the same RM order'))

        return record

    def save_assign_rm_submission(self):

        # rm_submission = self.browse(cr, uid, ids, context=context)

        rm_sub_data = {
            'actual_sub_date': self.actual_sub_date,
            'exp_sub_date': self.exp_sub_date,
            'exp_pay_date': self.exp_pay_date,
            'assigned_rm': self.assigned_rm.id,
            'comment': self.comment,
            'state': self.state
        }

        bill_pro_ids = self.env.context['active_ids']
        bill_pro_obj = self.env['bill.collection.billing.process']
        bill_pro_data = bill_pro_obj.browse(bill_pro_ids)

        assignee_line_obj = self.env["bill.assignee.line"]
        for bill_pro in bill_pro_data:
            assignee_id = assignee_line_obj.search([('col_bill_pro_id', '=', bill_pro.id), ('assignee_status', '=', True)])
            assignee_id.write(rm_sub_data)

            # create in payment line
            payment_data = {
                'col_bill_pro_id': bill_pro.id,
                'assignee_line_id': assignee_id.id,
                'paid_amount': 0.00,
            }

            payment_line = self.env["bill.payment.line"]
            pay_id = payment_line.create(payment_data)

            state_update_query = "UPDATE bill_collection_billing_process SET state='submitted', submitted_by={0}," \
                                 "submitted_time='{1}', payment_assigned_rm='{2}' WHERE id={3}".\
                format(self._uid, str(fields.datetime.now()), self.assigned_rm.id, bill_pro.id)
            self._cr.execute(state_update_query)
            self._cr.commit()

            bill_log_obj = self.env['coll.bill.process.log']
            log_id = bill_log_obj.search([('bill_process_id', '=', bill_pro.id)])
            bill_log_data = {
                'submitted_to': self.assigned_rm.id,
                'actual_sub_date': self.actual_sub_date,
                'submitted_time': self.exp_pay_date,
                'state': 'submitted',

            }
            log_id.write(bill_log_data)

            bill_pro_data1 = {
                'exp_pay_date': self.exp_pay_date,
                'actual_sub_date': self.actual_sub_date
            }
            bill_pro_obj.browse(bill_pro.id).write(bill_pro_data1)

        return True


class BillWizAssignrmRescheduleSubmission(models.Model):
    _name = "bill.wiz.assign.rm.reschedule.submission"
    _description = "Assign RM Reschedule Submission Wizard"

    name = fields.Char('Order No')
    mag_name = fields.Char('Magento Order No')
    order_amount = fields.Float('Order Amount')
    assigned_date = fields.Date('Assigned Date')
    assigned_rm = fields.Many2one('res.users', 'Assigned RM')
    actual_sub_date = fields.Date('Actual Submitted Date')
    exp_sub_date = fields.Date('Expected Submission Date')
    exp_pay_date = fields.Date('Expected Payment Date')
    comment = fields.Text('Comment')

    @api.model
    def default_get(self,fields):

        record = super(BillWizAssignrmRescheduleSubmission, self).default_get(fields)

        ids = self.env.context['active_ids']
        bill_pro_obj = self.env['bill.collection.billing.process']
        bill_pro_data = bill_pro_obj.browse(ids)
        assign_rm_unique = bill_pro_obj.rm_unique(ids)
        if len(assign_rm_unique) == 1:
            for bill_pro in bill_pro_data:
                record['name'] = str(bill_pro.name)
                record['mag_name'] = str(bill_pro.mag_name)
                record['order_amount'] = bill_pro.order_amount

        elif len(assign_rm_unique) == 0:
            raise exceptions.ValidationError(
                                 _('Please assign the RM'))
        else:
            raise exceptions.ValidationError(
                                 _('Please select the same RM order'))

        return record

    def save_assign_rm_reschedule_submission(self):

        # rm_submission = self.browse(cr, uid, ids, context=context)

        rm_re_sub_data = {
            'col_bill_pro_id': self.env.context['active_ids'][0],
            'exp_sub_date': self.exp_sub_date,
            'exp_pay_date': self.exp_pay_date,
            'assigned_rm': self.assigned_rm.id,
            'comment': self.comment,
            'state': 'submitted',
            'assignee_status': True
        }

        bill_pro_ids = self.env.context['active_ids']
        bill_pro_obj = self.env['bill.collection.billing.process']
        bill_pro_data = bill_pro_obj.browse(bill_pro_ids)

        assignee_line_obj = self.env["bill.assignee.line"]
        for bill_pro in bill_pro_data:

            assignee_id = assignee_line_obj.search(['&', ('col_bill_pro_id', '=', bill_pro.id), ('assignee_status', '=', True)])

            assignee_id.write({'assignee_status': False})

            ass_line = assignee_line_obj.create(rm_re_sub_data)

            # create in payment line
            payment_data = {
                'col_bill_pro_id': bill_pro.id,
                'assignee_line_id': assignee_id.id,
                'paid_amount': 0.00,
            }

            payment_line = self.env["bill.payment.line"]
            pay_id = payment_line.create( payment_data)

            state_update_query = "UPDATE bill_collection_billing_process SET state='submitted', submitted_by={0}," \
                                 "submitted_time='{1}', payment_assigned_rm='{2}' WHERE id={3}".format(
                self._uid, str(fields.datetime.now()), self.assigned_rm.id, bill_pro.id)
            self._cr.execute(state_update_query)
            self._cr.commit()

            bill_log_obj = self.env['coll.bill.process.log']
            log_id = bill_log_obj.search([('bill_process_id', '=', bill_pro.id)])
            bill_log_data = {
                'submitted_to': self.assigned_rm.id,
                'submitted_time': self.exp_pay_date,
                'state': 'submitted',

            }
            log_id.write(bill_log_data)

            bill_pro_data1 = {
                'exp_sub_date': self.exp_sub_date,
                'exp_pay_date': self.exp_pay_date,
            }
            bill_pro_obj.browse(bill_pro.id).write(bill_pro_data1)

        return True


class BillWizPaymentCollection(models.Model):
    _name = "bill.wiz.payment.collection"
    _description = "Payment Collection Wizard"

    paid_amount = fields.Float('Paid Amount')
    memo = fields.Text('Memo')
    paid_date = fields.Date('Paid Date')
    type = fields.Selection([
        ('cash', 'Cash'),
        ('check', 'Check'),
    ], 'Type', select=True)
    state = fields.Selection([
        ('draft', 'Draft'),
        ('assigned', 'Assigned'),
        ('submitted', 'Submitted'),
        ('pending_payment', 'Pending Payment'),
        ('partially_paid', 'Partially Paid'),
        ('paid', 'Paid'),
        ('cancel', 'Cancel'),
    ], 'Status', select=True)

    @api.model
    def default_get(self,fields):

        record = super(BillWizPaymentCollection, self).default_get(fields)

        ids = self.env.context['active_ids']
        bill_pro_obj = self.env['bill.collection.billing.process']
        bill_pro_data = bill_pro_obj.browse(ids)

        # for bill_pro in bill_pro_data:
        #
        #     for line in bill_pro.assignee:
        #         if line.assignee_status:
        #             # record['name'] = str(bill_pro.name)
        #             record['assigned_rm'] = line.assigned_rm.id
        #             record['mag_name'] = str(bill_pro.mag_name)
        #             record['order_amount'] = bill_pro.order_amount

        return record

    def save_payment_collection(self):

        # pay_obj = self.env["bill.payment.line")

        ids = self.env.context['active_ids']
        bill_pro_obj = self.env['bill.collection.billing.process']
        bill_pro_data = bill_pro_obj.browse(ids)

        bill_payment_line_obj = self.env['bill.payment.line']
        pay_line_list = bill_payment_line_obj.search([('col_bill_pro_id', '=', self.env.context['active_ids'][0])])

        total_paid_amount = 0.0
        for pay_line in pay_line_list:
            total_paid_amount += pay_line.paid_amount

        if total_paid_amount == 0.0:
            total_paid_amount = self.paid_amount
        else:
            total_paid_amount = total_paid_amount + self.paid_amount

        payment_line_status = ""
        if total_paid_amount == bill_pro_data.order_amount:
            payment_line_status = 'paid'
        else:
            payment_line_status = 'partially_paid'

        # payment_line_status need to save
        state_update_query = "UPDATE bill_collection_billing_process SET state='{0}', pending_payment_by={1}," \
                             "pending_payment_time='{2}', paid_amount='{3}' WHERE id={4}".format\
            (payment_line_status, self._uid, str(fields.datetime.now()), total_paid_amount, self.env.context['active_ids'][0])
        self._cr.execute(state_update_query)
        self._cr.commit()

        # payment_update_query = "UPDATE bill_payment_line SET state='{0}', paid_amount='{1}', memo='{2}', paid_date='{3}',type='{4}' WHERE col_bill_pro_id={5}".format(payment_line_status, save_payment.paid_amount, save_payment.memo, save_payment.paid_date, save_payment.type, context['active_ids'][0])
        # cr.execute(payment_update_query)
        # cr.commit()

        assignee_line_obj = self.env['bill.assignee.line']

        zero_paid = bill_payment_line_obj.search([('paid_amount','=', 0.00),('col_bill_pro_id','=', bill_pro_data.id)])

        if zero_paid:
            zero_paid.unlink()

        values = {
            'state': payment_line_status,
            'paid_amount': self.paid_amount,
            'memo': self.memo,
            'paid_date': self.paid_date,
            'type': self.type,
            'col_bill_pro_id': ids[0],
            'assignee_line_id': assignee_line_obj.search([('col_bill_pro_id','=', bill_pro_data.id),
                                                                   ('assignee_status', '=', True)]).id,

         }

        bill_payment_line_obj.create(values)

        bill_log_obj = self.env['coll.bill.process.log']
        log_id = bill_log_obj.search([('bill_process_id', '=', bill_pro_data.id)])
        bill_log_data = {
            'paid_amount': total_paid_amount,
            'unpaid_amount': bill_pro_data.order_amount - total_paid_amount,
            'state': payment_line_status,
        }
        log_id.write(bill_log_data)

        return True


class CancelBillProcessReason(models.Model):
    _name = "cancel.bill.process.reason"
    _description = "Cancel Bill Process Reason"

    cancel_bill_process_date = fields.Datetime('Cancel Bill Process Date')
    cancel_bill_process_by = fields.Many2one('res.users', 'Cancel Bill Process By')
    cancel_bill_process_reason = fields.Text('Cancel Bill Process Reason', required=True)

    def cancel_bill_process_reason_def(self):
        context = self.env.context
        ids = context['active_ids']
        cancel_bill_process_reason = str(context['cancel_bill_process_reason'])
        cancel_bill_process_by = self._uid
        cancel_bill_process_date = str(fields.datetime.now())

        bill_obj = self.env['bill.collection.billing.process']

        for s_id in ids:
            cancel_bill_proces_query = "UPDATE bill_collection_billing_process SET cancel_bill_process_reason='{0}', " \
                                       "cancel_bill_process_by={1}, cancel_bill_process_date='{2}' WHERE id={3}".format(
                cancel_bill_process_reason, cancel_bill_process_by, cancel_bill_process_date, s_id)
            self._cr.execute(cancel_bill_proces_query)
            self._cr.commit()

            bill_obj.cancel_bill(s_id)

            bill_log_obj = self.env['coll.bill.process.log']
            log_id = bill_log_obj.search([('bill_process_id', '=', s_id)])
            bill_log_data = {
                'state': 'cancel',
                'cancel_by': self._uid,
                'cancel_time': datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'),

            }
            log_id.write(bill_log_data)

        return True


class CollBillProcessLog(models.Model):
    _name = "coll.bill.process.log"
    _description = "Collection Billing Process Log"

    name = fields.Char('Name')
    mag_name = fields.Char('Magento Order No')
    partner_id = fields.Many2one('res.partner', 'Customer')
    warehouse_name = fields.Char('Warehouse Name')
    order_amount = fields.Float('Order Amount')
    billing_amount = fields.Float('Final Amount')
    return_amount = fields.Float('Total Return Amount')
    delivered_amount = fields.Float('Total Delivered Amount')
    unpaid_amount = fields.Float('Unpaid Amount')
    paid_amount = fields.Float('Paid Amount')
    order_id = fields.Many2one('sale.order', 'Sale Order')
    bill_process_id = fields.Many2one('bill.collection.billing.process', 'Bill Process ID')
    state = fields.Char('Status')
    assigned_to = fields.Many2one('res.users', 'Assigned To')
    assigned_time = fields.Datetime('Assigned Date')
    actual_sub_date = fields.Date('Actual Submitted Date')
    submitted_to = fields.Many2one('res.users', 'Submitted To')
    submitted_time = fields.Datetime('Submitted Date')
    pending_payment_by = fields.Many2one('res.users', 'Pending Payment By')
    pending_payment_time = fields.Datetime('Pending Payment Date')
    paid_by = fields.Many2one('res.users', 'Paid By')
    paid_time = fields.Datetime('Paid Date')
    cancel_by = fields.Many2one('res.users', 'Cancel By')
    cancel_time = fields.Datetime('Cancel Date')
    partially_paid_by = fields.Many2one('res.users', 'Partially Paid By')
    partially_paid_time = fields.Datetime('Partially Paid Date')


class BillWizAccountsApprove(models.Model):
    _name = "bill.wiz.accounts.approve"
    _description = "Accounts Approve For Bill"

    reason = fields.Text('Reason')

    def save_account_approve(self):

        if self.env.context.get('active_id'):
            billing_id = self.env.context.get('active_id')
            # cr.execute("update account_invoice set state='partially_paid' where id=%s", ([billing_id]))
            self._cr.execute("update bill_collection_billing_process set state='paid' where id=%s", ([billing_id]))
        self._cr.commit()
        return True
