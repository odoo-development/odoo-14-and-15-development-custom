from openerp import models, api


class BillingCollectionReport(models.Model):
    _inherit = 'bill.collection.billing.process'

    @api.model
    def _report_xls_fields(self):

        # return [
        #     'customer_name', 'company_code', 'credit_limit', 'credit_days', 'classification', 'salesperson', 'assigned_to', 'client_order_ref', 'warehouse_name',
        #     'po_ref', 'date_order', 'date_confirm', 'amount_total',
        #     'final_delivery_date', 'delivery_challan_uploaded', 'invoice_nos', 'invoice_dates', 'invoice_values', 'delivery_dates', 'final_inv_date',
        #     'final_inv_no', 'delivered_amount', 'return_amount', 'order_status',
        #     'bill_sub_target_date', 'actual_sub_date', 'sub_comment', 'bill_sub_date', 'bill_rec_copy_uploaded',
        #     'collection_aging',
        #
        # ]

        return [
            'customer_name', 'company_code', 'credit_limit', 'credit_days', 'classification', 'salesperson',
            'assigned_to', 'client_order_ref', 'warehouse_name', 'date_confirm', 'amount_total',
            'final_delivery_date', 'delivery_challan_uploaded', 'invoice_nos', 'invoice_dates', 'invoice_values',
            'delivery_dates', 'final_inv_date',
            'final_inv_no', 'delivered_amount', 'return_amount', 'order_status',
            'bill_sub_target_date', 'actual_sub_date', 'sub_comment', 'bill_sub_date', 'bill_rec_copy_uploaded',
            'collection_aging', 'final_amount', 'expected_payment_date','total_paid_amount','unpaid_amount', 'paid_amount', 'cq_number',
             'cq_money_receipt_attach','partial_paid_amount', 'payment_type',
            'billing_pattern','billing_status', 'cancel_date', 'cancel_amount', 'cancel_reason'

        ]

    # Change/Add Template entries
    @api.model
    def _report_xls_template(self):

        return {}
