{
    'name': 'Sales Collection Billing Process',
    'version': '14.0',
    'author': "Shuvarthi",
    'category': 'Sale',
    'summary': 'Sales Collection Billing Process',
    'depends': ['base', 'account', 'sale', 'purchase', 'sale_order_custom_fields', 'mail'],
    'data': [
        'security/billing_process_access.xml',
        'security/ir.model.access.csv',

        'wizard/assign_rm.xml',
        'wizard/re_assign_rm.xml',
        'wizard/assign_rm_submission.xml',
        'wizard/reschedule_rm_submission.xml',
        'wizard/payment.xml',
        'wizard/accounts_approve_view.xml',
        'wizard/cancel_view.xml',

        'report/billing_invoice_report.xml',
        #'report/billing_assignee_report.xml',
        # 'report/billing_payment_report.xml',
        # 'report/billing_reattempt_report.xml',
        'report/billing_process_print_menu.xml',
        # 'report/billing_collection_report_xls.xml',
        'views/billing_process_log_view.xml',

        'views/pending_for_assignment_view.xml',
        'views/assigned_to_rm_view.xml',
        'views/submitted_list_view.xml',
        'views/over_due_submitted_list_view.xml',
        'views/pending_for_payment_view.xml',
        'views/over_due_payment_view.xml',
        'views/partially_paid_view.xml',
        'views/paid_list_view.xml',
        'views/all_billing_status_view.xml',

        'views/billing_process_menu.xml',
        'views/collection_billing_process.xml',

    ],
    'installable': True,
    'auto_install': False,
}
