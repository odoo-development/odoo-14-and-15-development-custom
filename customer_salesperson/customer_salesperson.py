# Author Rocky 2021

from odoo import fields, models


class ResPartner(models.Model):
    _inherit = 'res.partner'

    sales_representative_name = fields.Char(string="Salesperson Name")
    sales_representative_email = fields.Char(string="Salesperson Email")
    sales_representative_mobile = fields.Char(string="Salesperson Phone")
