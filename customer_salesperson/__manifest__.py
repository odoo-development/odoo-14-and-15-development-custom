# Copyright 2021 Rocky

{
    "name": "Salesperson info in Partner",
    "version": "14.0",
    "author": "Sindabad",
    "category": "Sales Management",
    "license": "AGPL-3",
    "depends": ["base"],
    "data": [
        "customer_salesperson_view.xml"
    ],
    "installable": True,
}
