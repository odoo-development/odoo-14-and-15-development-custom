from odoo import api, fields, models, _


class SaleOrder(models.Model):
    _inherit = 'sale.order'

    # @api.model
    # def order_resync_check(self, data):
    #
    #     mag_order_id = None
    #     if data.get('order_id'):
    #         mag_order_id = data['order_id']
    #
    #     select_so_query = "Select id from sale_order WHERE client_order_ref='{0}'".format(str(mag_order_id))
    #     self.env.cr.execute(select_so_query)
    #     so_id = self.env.cr.fetchone()
    #
    #     if so_id:
    #         result = {
    #             'found': True,
    #             'odoo_id': so_id[0]
    #         }
    #     else:
    #         result = {
    #             'found': False,
    #             'odoo_id': ''
    #         }
    #
    #     return result

    @api.model
    def order_cancel_from_magento(self, data):
        if data.get('order_id'):
            mag_order_id = data['order_id']

            so_obj = self.env['sale.order'].search([('client_order_ref','=',mag_order_id)])
            try:
                if so_obj:
                    total_sum = 0.00
                    for line in so_obj.order_line:
                        total_sum = total_sum + (line.qty_delivered * line.price_unit) - (
                            line.qty_return * line.price_unit)
                    delivered_amount = float(total_sum)
                    if delivered_amount == float(0):
                        so_obj.action_cancel()
                        result = {
                            'cancel': True,
                            'msg': 'Order Cancelled Successfully'
                        }

                    else:
                        result = {
                            'cancel': False,
                            'msg': 'Already invoiced'
                        }
                else:
                    result = {
                    'cancel': False,
                    'msg': 'Not found'
                }
            except:
                result = {
                    'cancel': False,
                    'msg': 'Not found'
                }

            return result

    @api.model
    def order_pay_later_check_from_magento(self, data):
        if data.get('order_id'):
            mag_order_id = data['order_id']

            so_obj = self.env['sale.order'].search([('client_order_ref', '=', mag_order_id)])
            try:
                if so_obj:
                    if so_obj.state in ['sale','cancel']:
                        result = {
                            'payment':False,
                            'msg': 'Already Confirmed'
                        }
                    else:
                        update_so_query = "UPDATE sale_order SET x_pay_later=TRUE WHERE id='{0}'".format(so_obj.id, )
                        self.env.cr.execute(update_so_query)
                        self.env.cr.commit()
                        result = {
                            'payment':True,
                            'msg': 'Please go proceed'
                        }
                else:
                    result = {
                        'payment': False,
                        'msg': 'Not found'
                    }
            except:
                result = {
                    'payment': False,
                    'msg': 'Not found'
                }

            return result