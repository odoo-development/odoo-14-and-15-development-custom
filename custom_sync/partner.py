from odoo import api, fields, models, _


class ResPartner(models.Model):
    _inherit = "res.partner"

    legal_name = fields.Char('Legal Name')
    status = fields.Char('Status')
    customer = fields.Boolean('Customer')
    wk_address = fields.Boolean('Address')
    account_manager = fields.Boolean('Account Manager')
    company_admin = fields.Boolean('Company Admin')
    # magento_company_id = fields.Integer('Magento Company ID', required=False)
    mage_company_id = fields.Integer('Magento Company ID', required=False)
    mage_customer_id = fields.Integer('Magento Customer ID', required=False)
    tin_no = fields.Char('TIN No')
    fax = fields.Char('Fax')
    trade_license = fields.Char('Trade License')
    magento_superuser_id = fields.Integer('Magento Superuser ID', required=False)
    magento_sales_rep_id = fields.Integer('Magento Sales Representative ID', required=False)
    odoo_super_customer_id = fields.Integer('Company admin odoo id', required=False)
    customer_ids = fields.Char('It is for bridge Only')

    @api.model
    def map_rm(self, vals_list):
        if 'email' in vals_list:
            res_user_env = self.env['res.users']
            res_user_list = res_user_env.search([('login', '=', vals_list['email'])])
            done = 1
            return done

    @api.model
    def create(self,vals):
        company_code = vals.get('company_code')

        customer_id = super(ResPartner, self).create(vals)

        if vals.get('name') == 'Account Manager':
            try:
                partner_obj = self.search([('company_code', '=', company_code), ('is_company', '=', True)])
                sms_text = "Company " + str(partner_obj.name)+", " \
                                                              "created successfully,You can place order now. Email: " \
                                                              ""+str(partner_obj.email)+" A/C Manager: " \
                                                                                        ""+str(vals.get('email'))
                name = 'Account Manager'

                if company_code:
                    mobile_numbers = partner_obj.sales_representative_mobile
                    self.env["send.sms.on.demand"].send_sms_on_demand(sms_text, mobile_numbers, name)
            except:
                pass

        ## Ends Here

        return customer_id

    # def write(self, vals):
    #
    #     record = super(ResPartner, self).write(vals)
    #     if self.name == 'Account Manager' and self.account_manager :
    #         if self.parent_id and self.parent_id.sales_representative_mobile :
    #             sms_text = "Company " + str(self.parent_id.name)+", created successfully,You can place order now. Email: "+str(self.parent_id.email)+" A/C Manager: "+str(self.email)
    #             name = 'Account Manager'
    #             mobile_numbers = self.parent_id.sales_representative_mobile
    #             ## Send SMS
    #             self.env["send.sms.on.demand"].send_sms_on_demand(sms_text, mobile_numbers, name)
    #
    #     return record


    @api.model
    def customer_email_check(self,vals):
        email = vals.get('email')

        res_part_parent_id_update_query = "select res_partner.id,connector_partner_mapping.id from res_partner," \
                                          "connector_partner_mapping  WHERE res_partner.id = " \
                                          "connector_partner_mapping.odoo_id and res_partner.customer=True and " \
                                          "res_partner.email=%s order by res_partner.id asc"
        self.env.cr.execute(res_part_parent_id_update_query, ([email]))
        abc = self.env.cr.fetchall()

        if len(abc)>0:
            exist_customer_id=abc[0][0]
            exist_mapping_id = abc[0][1]

        else:
            exist_customer_id = 0
            exist_mapping_id = 0

        data = {
            'mapping_id': exist_mapping_id,
            'customer_id': exist_customer_id
        }

        return data

    @api.model
    def company_email_check(self,vals):
        email = vals.get('email')
        res_part_parent_id_update_query = "select res_partner.id,connector_partner_mapping.id from res_partner," \
                                          "connector_partner_mapping  WHERE res_partner.id = " \
                                          "connector_partner_mapping.odoo_id and res_partner.is_company=True and " \
                                          "res_partner.email=%s order by res_partner.id asc"
        self.env.cr.execute(res_part_parent_id_update_query, ([email]))
        abc = self.env.cr.fetchall()

        if len(abc)>0:
            exist_customer_id=abc[0][0]
            exist_mapping_id=abc[0][1]
        else:
            exist_customer_id = 0
            exist_mapping_id = 0

        data  = {
            'mapping_id':exist_mapping_id,
            'company_id':exist_customer_id,
        }

        return data
