import json
import requests
from ..odoo_to_magento_api_connect.api_connect import get_magento_token
from odoo import api, fields, models, _

XMLRPC_API = '/index.php/api/xmlrpc'


class ProductProduct(models.Model):
    _inherit = "product.product"

    prod_uom = fields.Char('UOM')
    product_name = fields.Char('Product Name')
    warranty_applicable = fields.Boolean('Warranty Applicable')
    warranty_duration = fields.Integer("Warranty (Month)")
    warranty_text = fields.Text('Warranty Rule')
    uttara_reorder_level = fields.Float('Uttara Re Order Qty Level')
    badda_reorder_level = fields.Float('Badda Re Order Qty Level')

    @api.model
    def product_barcode_sync_to_magento(self, product_id):

        try:
            product_data_list =  self.env['product.product'].browse(product_id)
            mapping_objects = self.env['connector.product.mapping'].search([('odoo_id', 'in', [product_id])])

            odoo_magento_dict = {}  # keys are odoo id and values are magento ID

            for items in mapping_objects:
                odoo_magento_dict[items.odoo_id] = items.ecomm_id

            final_list = []

            for prod_item in product_data_list:
                final_list.append(
                    {
                        "product_id": odoo_magento_dict[prod_item.id],
                        "barcode": str(prod_item.barcode),
                        # "weight": float(prod_item.weight),
                        # "product_size": float(prod_item.volume)
                    }
                )

            sale_order_ids = []

            ### magrnto problem
            token, url_root = get_magento_token(self, sale_order_ids)

            token = token.replace('"', "")
            headers = {'Authorization': token, 'Content-Type': 'application/json'}

            product_data = {"productItems": final_list}

            url = url_root + "/index.php/rest/V1/odoomagentoconnect/productAttributeBulk/"
            data = json.dumps(product_data)
            resp = ''
            try:
                resp = requests.post(url, data=data, headers=headers)
            except:
                pass
                # By getting rsponses data ends here
        except:
            pass

        return True