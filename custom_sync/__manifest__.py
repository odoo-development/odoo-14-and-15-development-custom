{
    'name': 'Custom Sync',
    'version': '1.0',
    'category': 'WMS',
    'author': 'Rocky',
    'summary': 'Custom Sync',
    'description': 'Custom Sync Module',
    # 'depends': ['base', 'product','partner_custom_fields','odoo_magento_connect','odoo_to_magento_api_connect'],
    'depends': ['base','product', 'odoo_magento_connect', 'partner_custom_fields', 'sale_management', 'odoo_to_magento_api_connect'],
    'installable': True,
    'application': True,
    'auto_install': False,
}
