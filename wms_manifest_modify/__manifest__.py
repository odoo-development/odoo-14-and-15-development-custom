{
    'name': 'WMS Manifest Last update custom module',
    'version': '14.0.0',
    'category': 'WMS Manifest',
    'description': """
WMS Manifest Last update custom module which is develop sindabad odoo team.
===========================================================================

""",
    'author': 'Sindabad',
    'depends': ['wms_manifest','wms_manifest_extends','wms_manifest_inherit','loading_master', 'cash_collection'],
    'data': [
        'report/wms_manifest_report_modify.xml',
        'views/wms_manifest_last_view.xml',
        'views/loading_master_last_view.xml',
    ],

    'installable': True,
    'application': True,
    'auto_install': False,
}

