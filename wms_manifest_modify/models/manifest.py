from odoo import api, fields, models


class WmsManifestProcess(models.Model):
    _inherit = "wms.manifest.process"

    warehouse_id = fields.Many2one('stock.warehouse', string='Warehouse', index=True)
    courier_wh = fields.Selection(selection=[
        ('in_house_uttara', 'In House Uttara'),
        ('in_house_badda', 'In House Badda'),
        ('in_house_signboard', 'In House Signboard'),
        ('in_house_savar', 'In House Savar'),
        ('in_house_badda_horeca', 'In House Badda Horeca'),
        ('in_house_uttara_damage', 'In House Uttara Damage'),
        ('in_house_nodda_damage', 'In House Nodda Damage'),
        ('in_house_signboard_damage', 'In House Signboard Damage'),
        ('in_house_badda_damage', 'In House Badda Damage'),
        ('in_house_savar_damage', 'In House Savar Damage'),
    ], string='Courier Name', required=True, copy=False, tracking=True, index=True)

    @api.onchange('courier_wh')
    def warehouse_name(self):
        if self.courier_wh == 'in_house_uttara':
            warehouse = self.env['stock.warehouse']
            warehouse_info = warehouse.get_warehouse_id('UttWH')
            self.warehouse_id = warehouse_info.id
            self.courier_name = 'N/A'

        elif self.courier_wh == 'in_house_badda':
            warehouse = self.env['stock.warehouse']
            warehouse_info = warehouse.get_warehouse_id('BddWH')
            self.warehouse_id = warehouse_info.id
            self.courier_name = 'N/A'

        elif self.courier_wh == 'in_house_signboard':
            warehouse = self.env['stock.warehouse']
            warehouse_info = warehouse.get_warehouse_id('SinWH')
            self.warehouse_id = warehouse_info.id
            self.courier_name = 'N/A'

        elif self.courier_wh == 'in_house_savar':
            warehouse = self.env['stock.warehouse']
            warehouse_info = warehouse.get_warehouse_id('SavWH')
            self.warehouse_id = warehouse_info.id
            self.courier_name = 'N/A'

        elif self.courier_wh == 'in_house_badda_horeca':
            warehouse = self.env['stock.warehouse']
            warehouse_info = warehouse.get_warehouse_id('BdHoW')
            self.warehouse_id = warehouse_info.id
            self.courier_name = 'N/A'

        elif self.courier_wh == 'in_house_uttara_damage':
            warehouse = self.env['stock.warehouse']
            warehouse_info = warehouse.get_warehouse_id('UttDW')
            self.warehouse_id = warehouse_info.id
            self.courier_name = 'N/A'

        elif self.courier_wh == 'in_house_nodda_damage':
            warehouse = self.env['stock.warehouse']
            warehouse_info = warehouse.get_warehouse_id('NodDW')
            self.warehouse_id = warehouse_info.id
            self.courier_name = 'N/A'

        elif self.courier_wh == 'in_house_signboard_damage':
            warehouse = self.env['stock.warehouse']
            warehouse_info = warehouse.get_warehouse_id('SinDW')
            self.warehouse_id = warehouse_info.id
            self.courier_name = 'N/A'

        elif self.courier_wh == 'in_house_badda_damage':
            warehouse = self.env['stock.warehouse']
            warehouse_info = warehouse.get_warehouse_id('BddDW')
            self.warehouse_id = warehouse_info.id
            self.courier_name = 'N/A'

        elif self.courier_wh == 'in_house_savar_damage':
            warehouse = self.env['stock.warehouse']
            warehouse_info = warehouse.get_warehouse_id('SavDW')
            self.warehouse_id = warehouse_info.id
            self.courier_name = 'N/A'

        else:
            self.warehouse_id = ''


class WmsManifestLine(models.Model):
    _inherit = "wms.manifest.line"

    warehouse_id = fields.Many2one('stock.warehouse', string='Warehouse')
    state = fields.Selection([
        ('pending', 'Pending'),
        ('confirm', 'Confirmation'),
        ('delivered', 'Delivered & Not Paid'),
        ('closed', 'Paid & Closed'),
        ('cancel', 'Cancelled'),
    ], 'Status', related='wms_manifest_id.state', readonly=True, copy=False, help="Gives the status of the Manifest", index=True, store=True)


class StockWarehouse(models.Model):
    _inherit = 'stock.warehouse'

    def get_warehouse_id(self, code):
        warehouse_info = self.search([('code', '=', code)])
        wh_id = warehouse_info

        return wh_id

