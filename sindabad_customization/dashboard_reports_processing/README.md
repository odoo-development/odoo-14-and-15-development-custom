# Report DB preparation -

1. Create a new database named "reports" with role or owner "odoo"
2. Create tables -

```
Connection String :
DB: reports
IP: 
user: odoo
pass: admin1234 or postgres

ALTER USER odoo PASSWORD 'admin1234';
```
    A.
    ```SQL
    # Pending Procurement (canceled)
    CREATE TABLE pending_procurement(
        id serial PRIMARY KEY,
        sku VARCHAR (250),
        uttara_pending_qty NUMERIC,
        nodda_pending_qty NUMERIC,
        moti_pending_qty NUMERIC,
        uttara_free_stock NUMERIC,
        nodda_free_stock NUMERIC,
        moti_free_stock NUMERIC,
        pending_orders NUMERIC,
        order_number TEXT
    	)

    # Pending Procurement
    CREATE TABLE pending_procurement(
        id serial PRIMARY KEY,
        sku VARCHAR (250),
        warehouse VARCHAR (250),
        pending_qty NUMERIC,
        free_stock_qty NUMERIC,
        pending_order_number TEXT
    	)

    # Excess Inventory by Category # 60 days
    CREATE table excess_inventory_by_category(
        id serial PRIMARY KEY,
        category VARCHAR(250),
        sku VARCHAR(250),
        order_qty NUMERIC,
        sale_value NUMERIC,
        order_date DATE,
        order_no VARCHAR(250)
    )

    # Procurement Cost
    CREATE table procurement_cost(
        id serial PRIMARY KEY,
        category VARCHAR(250),
        sku VARCHAR(250),
        previous_cost NUMERIC,
        previous_po VARCHAR(250),
        recent_cost NUMERIC,
        recent_po VARCHAR(250),
        reduced_cost NUMERIC, /* (previous_cost - recent_cost) < 0 */
        increased_cost NUMERIC, /* (previous_cost - recent_cost) > 0 */
        cost_changed BOOLEAN /* (previous_cost - recent_cost) == 0 */
    )

    # COGS Report
    CREATE table cogs_report(
        id serial PRIMARY KEY,
        category VARCHAR(250),
        sku VARCHAR(250),
        prev_mon_deli_amount NUMERIC,
        cur_mon_deli_amount NUMERIC,
        prev_mon_margin NUMERIC,
        cur_mon_margin NUMERIC
    )

    # product sales
    CREATE TABLE product_sales(
        id serial PRIMARY KEY,
        line_id NUMERIC,
        sku VARCHAR(250),
        product_name VARCHAR(250),
        category VARCHAR(250),
        unit_price NUMERIC,
        quantity NUMERIC,
        order_date DATE,
        order_status VARCHAR(250),
        subtotal NUMERIC
    	)
    ```