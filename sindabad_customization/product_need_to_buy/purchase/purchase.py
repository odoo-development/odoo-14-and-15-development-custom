from odoo import fields, models


class PurchaseOrder(models.Model):
    _inherit = 'purchase.order'

    system_generated_po = fields.Boolean('System Generated PO', default=False)

    ### Block code for V-14
    # _columns = {
    #     'system_generated_po': fields.boolean('System Generated PO'),
    # }
    #
    # _defaults = {
    #     'system_generated_po': False,
    # }
