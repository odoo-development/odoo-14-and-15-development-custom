{
    "name": "Employee Custom Fields",
    "version": "15.0.0",
    "author": "Ashif",
    "category": "Human Resources",
    "license": "AGPL-3",
    "depends": [
        'bi_hr_payroll',
        'hr_contract'
    ],
    "data": [
         "views/employee_view.xml",
         "views/payslip_view.xml",
    ],
    "installable": True,
    "auto_install": False,
}
