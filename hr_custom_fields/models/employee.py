from odoo import api, fields, models,_


class Employee(models.Model):
    _inherit = 'hr.contract'

    cash_allowance = fields.Float(string="Other Allowance", default=0)
    emintax_sin01 = fields.Float(string="Employee income Tax")
    gross_salary = fields.Float(string="Gross")
    laptop_allowance= fields.Float(string="Laptop Allowance")
    lunch_allowance = fields.Float(string="Lunch Allowance")
    lunch_bill= fields.Float(string="Lunch Bill")
    mobile_allowance= fields.Float(string="Mobile Allowance")
    employee_identification_code=fields.Char(string='Employee ID')


class HrPayslip(models.Model):
    _inherit = 'hr.payslip'

    emintax_sin =fields.Float(string="Employee Income Tax")
    fest_bonus_sin=fields.Float(string="Festival Bonus", default=0)
    incentive_bon_cus =fields.Float(string="Incentive Bonuse")
    inpur_sin = fields.Float(string="In House Purchase")
    laptop_allowance_cus=fields.Float(string="Laptop Allowance")
    late_sin=fields.Float(string="No. of Day(s) LE")
    lunch_allowance_cus=fields.Float(string="Lunch Allowance")
    lunch_bill_cus=fields.Float(string="Lunch Bill")
    lunch_days=fields.Float(string="Lunch Days")
    lunch_rate=fields.Float(string="Lunch Rate")
    lwp_sin=fields.Float(string="No. of Day(s) LWP")
    mobile_allowance_cus=fields.Float(string="Mobile Allowance")
    mob_sin=fields.Float(string="Mobile bill")
    nods_sin = fields.Float(string="No. of Days")
    over_time_hour=fields.Float(string="Total Over Time Hour")
    over_time_rate=fields.Float(string="Over Time Rate")
    working_days=fields.Float(string="New Employee Working Days")
    arrer_cus=fields.Float(string="Arrer")
    delivery_incentive=fields.Float(string="Delivery Incentive", default=0)
    advance_sa=fields.Float(string="Advance", default=0)
    attend_bonus=fields.Float(string="Attendance Bonus", default=0)
    additional_service = fields.Float(string="Additional service", default=0)





