{
    'name': 'Customer Name in Delivery Challan and Invoice List Page',
    "category": "Purchase",
    'summary': """Customer Name in Delivery Challan List Page
    """,
    'author': 'Odoo Bangladesh',

    'website': '',
    'depends': ['stock', 'account'],
    'data': [
        "views/stock_picking_form_view.xml",
    ],
}