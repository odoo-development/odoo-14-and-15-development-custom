from odoo import api, fields, models, _


class StockPicking(models.Model):
    _inherit = 'stock.picking'

    @api.depends('partner_id')
    def _get_customer_name(self):
        for picking in self:
            picking.customer_name = picking.partner_id.parent_id.name if picking.partner_id.parent_id else picking.partner_id.name

    customer_name = fields.Char(compute='_get_customer_name', string='Customer Name', store=True)


class AccountMove(models.Model):
    _inherit = 'account.move'

    @api.depends('partner_id')
    def _get_customer_name(self):
        for move in self:
            move.customer_name = move.partner_id.parent_id.name if move.partner_id.parent_id else move.partner_id.name

    customer_name = fields.Char(compute='_get_customer_name', string='Customer Name', store=True)
