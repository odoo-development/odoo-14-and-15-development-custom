{
    'name': 'Product Weight and Size',
    'version': '14.0.1',
    'summary': 'Insert data and check from product table Weight and Size.',
    'category': 'Inventory',
    'author': 'Ashif Raihun',
    'website': 'https://www.sindabad.com',
    'depends': ['stock',
                'product',
                'stock_sync',
                'wms_inbound'
                ],
    'data': [
        'security/product_weight_size_sa.xml',
        'security/ir.model.access.csv',
        'wizard/irn_with_weight_size_wizard.xml',
        'views/inherit_view_from_irn.xml',
    ],
    'installable': True,
    'application': False,
    'auto_install': False,
}
