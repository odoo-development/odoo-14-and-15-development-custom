from odoo import api, fields, models, _
from odoo.tools.translate import _
from odoo.exceptions import except_orm, Warning, RedirectWarning, UserError


class IrnWithWeightSize(models.Model):
    _name = "irn.with.weight.size"
    _description = "IRN With Weight and Size"

    product = fields.Char('Product', readonly=True)
    product_id = fields.Integer('Product ID')
    product_weight = fields.Float(string="Weight")
    product_size = fields.Float(string="Size")

#save function for wizard
    def save_irn_with_product_weight_size(self):
        context = None

        # import pdb;pdb.set_trace()

        product_id = self.env.context['product_id']
        product_weight = self.product_weight
        product_size = self.product_size

        if self.product_weight:

            for pp in self.env['product.product'].browse(product_id):
                if pp.weight == 0 or pp.weight < 0:
                    pp.write({'weight': product_weight})

            irn_line_query = "UPDATE irn_control_line SET product_weight='{0}' WHERE product_id='{1}'".format(
                product_weight, product_id)
            self.env.cr.execute(irn_line_query)
            self.env.cr.commit()
        else:
            pass

        if self.product_size:
            for pp in self.env['product.product'].browse(product_id):

                if pp.volume == 0 or pp.volume < 0:
                    pp.write({'volume': product_size})


            irn_line_query="UPDATE irn_control_line SET product_size='{0}' where product_id='{1}'".format(product_size,product_id)
            self.env.cr.execute(irn_line_query)
            self.env.cr.commit
        else:
            pass

        return {
            'type': 'ir.actions.client',
            'tag': 'reload',
        }

    def cancel_irn_with_product_weight_size(self):
        return {
            'type': 'ir.actions.client',
            'tag': 'reload',

        }

    @api.model
    def default_get(self, fields):
        res = super(IrnWithWeightSize, self).default_get(fields)


        product_obj = self.env['product.product'].browse(res['product_id'])
        res['product_weight'] =product_obj.weight
        res['product_size'] =product_obj.volume
        return res


class IrnControlLine(models.Model):
    _inherit = "irn.control.line"

    product_weight = fields.Float(string="Weight")
    product_size = fields.Float(string="Size")

