{
    "name": "WMS Accounting",
    "version": "14.0.1.0.0",
    "category": "WMS",
    "author": "Odoo Bangladesh",
    "summary": "WMS Accounting for warehouse team",
    "description": "WMS Accounting for warehouse team",
    "depends": [
        "account",
        "wms_base",
    ],
    "data": [
        "security/wms_accounting_security.xml",
        "views/wms_accounting_view.xml",
    ],
    "installable": True,
    "application": True,
    "auto_install": False,
}
