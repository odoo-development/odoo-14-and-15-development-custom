from datetime import datetime
from datetime import date
from odoo import SUPERUSER_ID
from odoo import api, models, fields
from odoo.tools.translate import _
from odoo.exceptions import UserError, ValidationError, Warning
from odoo.osv import osv
import datetime


class WmsManifestProcess(models.Model):
    _name = "wms.manifest.process"
    _description = "WMS Manifest Process"
    _order = 'id desc'

    def _get_manifest_total_amount(self):

        for process in self:
            total_amount = 0

            for p_line in process.picking_line:
                total_amount = total_amount + p_line.amount

            process.manifest_total_amount = total_amount

    name = fields.Char("Reference")
    courier_name = fields.Char('Courier Name', required=True)
    vehicle_number = fields.Char('Vehicle Number')
    assigned_to = fields.Char('Assigned To')
    Assignee = fields.Many2one('res.users', 'Assignee', default=lambda self: self.env.user)
    vehicle_from = fields.Float('Start Km', required=False)
    vehicle_to = fields.Float('End Km', required=False)
    remark = fields.Text('Remark')
    scan = fields.Char('Scan')
    picking_line = fields.One2many('wms.manifest.line', 'wms_manifest_id', 'Assign to Courier', required=True)
    confirm_time = fields.Datetime('Confirmation Date', readonly=True, index=True,
                                   help="Date on which sales order is confirmed.")
    confirm_by = fields.Many2one('res.users', 'Confirm By')
    cancel_time = fields.Datetime('Cancelletion Date', readonly=True, index=True,
                                  help="Date on which sales order is cancelled.")
    cancel_by = fields.Many2one('res.users', 'Cancel By')
    manifest_total_amount = fields.Float(compute='_get_manifest_total_amount', string='Total Amount')
    state = fields.Selection([
        ('pending', 'Pending'),
        ('confirm', 'Confirmation'),
        ('cancel', 'Cancelled'),

    ], 'Status', default='pending', readonly=True, copy=False, help="Gives the status of the Mainfest", index=True)

    # ## Block code for V-14
    # _columns = {
    #     'courier_name': fields.char('Courier Name', required=True),
    #     'vehicle_number': fields.char('Vehicle Number'),
    #     'assigned_to': fields.char('Assigned To'),
    #     'Assignee': fields.many2one('res.users', 'Assignee'),
    #     'vehicle_from': fields.float('Start Km', required=False),
    #     'vehicle_to': fields.float('End Km', required=False),
    #     'remark': fields.text('Remark'),
    #     'scan': fields.char('Scan'),
    #     'picking_line': fields.one2many('wms.manifest.line', 'wms_manifest_id', 'Assign to Courier', required=True),
    #     'confirm_time': fields.datetime('Confirmation Date', readonly=True, index=True,
    #                                    help="Date on which sales order is confirmed."),
    #     'confirm_by': fields.many2one('res.users', 'Confirm By'),
    #     'cancel_time': fields.datetime('Cancelletion Date', readonly=True, index=True,
    #                                    help="Date on which sales order is cancelled."),
    #     'cancel_by': fields.many2one('res.users', 'Cancel By'),
    #     'manifest_total_amount': fields.function(_get_manifest_total_amount, string='Total Amount', type='float'),
    #     'state': fields.selection([
    #         ('pending', 'Pending'),
    #         ('confirm', 'Confirmation'),
    #         ('cancel', 'Cancelled'),
    #
    #     ], 'Status', readonly=True, copy=False, help="Gives the status of the Mainfest", index=True),
    #
    # }
    #
    # _defaults = {
    #     'user_id': lambda obj, cr, uid, context: uid,
    #     'state': 'pending',
    #     'Assignee': lambda obj, cr, uid, context: uid,
    # }

    def _check_packing_line_for_same_invoice(self, packing_line, invoice_id):
        same_invoice = False

        for line in packing_line:
            if line['invoice_id'] == invoice_id:
                same_invoice = True

        return same_invoice

    def check_invoice_assigned(self, invoice_id):
        invoice = self.env['account.move'].browse([invoice_id])
        if invoice:
            return invoice.assigned
        else:
            return False

    def check_invoice_delivered(self, invoice_id):
        invoice = self.env['account.move'].browse([invoice_id])
        if invoice:
            return invoice.delivered
        else:
            return False

    def check_invoice_return(self, invoice_id):
        invoice = self.env['account.move'].browse([invoice_id])
        if invoice:
            if invoice.move_type == 'out_refund':
                return True
            else:
                return False
        return False

    @api.onchange('scan')
    def invoice_barcode_onchange(self):
        # try:
        if self.scan:
            outbound_line_list = list()

            if int(self.scan):
                invoice = self.env['account.move'].search([('id', '=', int(self.scan)),('state','!=','cancel')], limit=1)
            else:
                invoice = self.env['account.move'].search([('name', '=', self.scan),('state','!=','cancel')], limit=1)

            if invoice:
                invoice_id = invoice.id

                if invoice.warehouse_id != self.warehouse_id:
                    self.scan = ""
                    raise Warning(_("This invoice is not %s, please scan same warehouse invoices." % self.warehouse_id.name))
                elif invoice.delivered:
                    self.scan = ""
                    raise Warning(_("This invoice is already delivered."))
                elif invoice.move_type == 'out_refund':
                    self.scan = ""
                    raise Warning(_("This order is already returned."))
                elif invoice.assigned:
                    self.scan = ""
                    raise Warning(_("This invoice is already assigned another manifest."))
                elif invoice.x_loading_assign:
                    self.scan = ""
                    raise Warning(_("This invoice is already assigned another loading sheet."))
                else:

                    invoice_obj = invoice
                    deli_area = invoice_obj.partner_shipping_id.state_id.name or ""
                    deli_address = invoice_obj.partner_shipping_id.street or ""
                    deli_phone = invoice_obj.partner_shipping_id.mobile or invoice_obj.partner_shipping_id.phone or ""

                    new_picking_line = []

                    if self.hub_id:
                        hub_area_list_obj = self.env['hub.area.line'].search([('area_hub_id', '=', self.hub_id.id)])
                        area_list = [hub_area_name.state_id.name for hub_area_name in hub_area_list_obj]
                        delivery_area = invoice.partner_shipping_id.state_id.name
                        if delivery_area not in area_list:
                            raise Warning(
                                _("This invoice is not %s, please scan same hub invoices." % self.hub_id.name))

                    if self.picking_line:
                        if invoice_obj:

                            if self._check_packing_line_for_same_invoice(self.picking_line, invoice_id):
                                self.scan = ""
                            else:
                                sale_object = self.env["sale.order"].search([("client_order_ref", "=", invoice_obj.ref)],
                                                                            limit=1)
                                customer_name = sale_object.partner_id.parent_id.name \
                                    if sale_object.partner_id.parent_id.name else sale_object.partner_id.name
                                new_picking_line.append((0, 0, {
                                    "invoice_id": invoice_id,
                                    "invoice_number": invoice_obj.name,
                                    "warehouse_id": invoice_obj.warehouse_id.id,
                                    "hub_id": self.hub_id.id if self.hub_id.id else None,
                                    "hub_invoice": True if self.hub_id.id else False,
                                    "magento_no": invoice_obj.ref,
                                    "priority_customer": '',
                                    # "priority_customer": invoice_obj.x_priority_customer,
                                    "customer_name": str(customer_name),
                                    "phone": str(deli_phone),
                                    "address": str(deli_address),
                                    "payment_type": str(invoice_obj.narration).replace("Payment Information:-", ""),
                                    "amount": float(invoice_obj.amount_total),
                                    "status": str(invoice_obj.state),
                                    "area": str(deli_area),
                                }))
                                self.picking_line = new_picking_line
                                self.scan = ""
                        else:
                            self.scan = ""
                    else:
                        new_line = []
                        if invoice_obj:

                            new_line.append((0, 0, {
                                'invoice_id': invoice_id,
                                'magento_no': invoice_obj.ref,
                                'warehouse_id': invoice_obj.warehouse_id.id,
                                'hub_id': self.hub_id.id if self.hub_id.id else None,
                                'hub_invoice': True if self.hub_id.id else False,
                                'customer_name': str(invoice_obj.partner_id.name),
                                'phone': str(deli_phone),
                                'address': str(deli_address),
                                'payment_type': str(invoice_obj.narration).replace("Payment Information:-", ""),
                                'amount': float(invoice_obj.amount_total),
                                'status': str(invoice_obj.state),
                                'area': str(deli_area)
                            }))

                        self.picking_line = new_line
                        self.scan = ""
            else:
                raise Warning(_("This invoice is already canceled."))

    @api.model
    def create(self, vals):
        record = super(WmsManifestProcess, self).create(vals)
        loading_number = "Manifest-0" + str(record.id)
        record.name = loading_number

        invoice_ids = []
        for line in record.picking_line:
            invoice_ids.append(line.invoice_id)

            invoice = self.env["account.move"].browse(line.invoice_id)
            if invoice.warehouse_id.id != vals["warehouse_id"]:
                raise Warning(_("This manifest warehouse has not set properly, please check!"))

        self.env['account.move'].browse(invoice_ids).write({'assigned': True})

        return record

    def write(self, vals):
        # if not context:
        #     context = {}

        deleted_picking_line = []
        if vals.get('picking_line'):
            for items in vals.get('picking_line'):
                if items[0] == 2:
                    deleted_picking_line.append(items[1])

        invoice_ids_obj = list()
        if deleted_picking_line:
            self.env.cr.execute("select invoice_id from  wms_manifest_line where id=%s", (deleted_picking_line))

            invoice_ids_obj = self.env.cr.fetchall()
        invoice_ids = []

        for it in invoice_ids_obj:
            invoice_ids.append(it[0])

        res = super(WmsManifestProcess, self).write(vals)
        if res:
            for inv_id in invoice_ids:
                self.env.cr.execute("update account_move set assigned=FALSE where id=%s", ([inv_id]))
                self.env.cr.commit()

        # return res

    def confirm_wms_manifest(self):
        self.ensure_one()
        # data = self.browse(self.ids[0])

        for items in self.picking_line:
            # magento_no = items.magento_no
            sales = self.env['sale.order'].search([('client_order_ref', '=', items.magento_no),('state', '!=','cancel')])

            if len(sales) > 0:
                # sales = query_for_SO
                for so in sales:
                    if so.state != 'cancel':
                        if so.delivered_amount == so.amount_total:
                            so.write({
                                'invoiced_dispatch': True,
                            })
                            # dispatch_wms_query = "UPDATE sale_order SET invoiced_dispatch=TRUE  WHERE id='{1}'".format(
                            #     so_item.id)
                            # self.env.cr.execute(dispatch_wms_query)
                            # self.env.cr.commit()
                        else:
                            so.write({
                                'partially_invoiced_dispatch': True,
                            })
                            # dispatch_wms_query = "UPDATE sale_order SET partially_invoiced_dispatch=TRUE  WHERE id='{1}'".format(
                            #     so_item.id)
                            # self.env.cr.execute(dispatch_wms_query)
                            # self.env.cr.commit()

        self.write({
            'state': 'confirm',
            'confirm_by': self.env.uid,
            'confirm_time': fields.datetime.now(),
        })

    def cancel_wms_manifest(self):
        for items in self.picking_line:
            self.env.cr.execute("update account_move set assigned=FALSE where id=%s", ([items.invoice_id]))
            self.env.cr.commit()

            self.env.cr.execute("update wms_manifest_line set status='cancel' and reschedule = TRUE where id=%s", ([items.id]))
            self.env.cr.commit()

        self.write({
            'state': 'cancel',
            'cancel_by': self.env.uid,
        })


class WmsManifestLine(models.Model):
    _name = "wms.manifest.line"
    _description = "Manifest Line"

    def _get_invoice_number(self):

        for line in self:
            move = self.env['account.move'].browse(line.invoice_id)
            line.invoice_number = str(move.name)

    wms_manifest_id = fields.Many2one('wms.manifest.process', 'Manifest Line ID', required=True, ondelete='cascade',
                                      index=True, readonly=True)
    invoice_id = fields.Integer('Invoice ID')
    invoice_number = fields.Char(compute='_get_invoice_number', string='Invoice No.')
    magento_no = fields.Char('Order No')
    customer_name = fields.Char('Customer Name')
    phone = fields.Char('Phone')
    address = fields.Char('Address')
    payment_type = fields.Char('Payment Type')
    amount = fields.Float('Amount')
    status = fields.Char('Status')
    area = fields.Char('Area')

#

class AccountInvoice(models.Model):
    _inherit = 'account.move'

    delivery_date = fields.Date('Delivery Date')
    delivered = fields.Boolean('Delivered', default=False)


class WmsManifestOutbound(models.Model):
    _name = "wms.manifest.outbound"
    _description = "WMS Manifest Process Outbound"
    _order = 'id desc'

    name = fields.Char('Order Number', required=True)
    customer_name = fields.Char('Customer Name')
    scan = fields.Char('Scan')
    outbound_line = fields.One2many('wms.manifest.outbound.line', 'wms_manifest_outbound_id', 'WMS Outbound Line',
                                    required=True)
    confirm = fields.Boolean('Confirm')
    confirm_by = fields.Many2one('res.users', 'Confirm By')
    cancel = fields.Boolean('Cancel')
    cancel_by = fields.Many2one('res.users', 'Cancel By')
    has_return = fields.Boolean('Has Return')
    delivered = fields.Boolean('Delivered')
    stock_return_id = fields.Integer('Stock Return ID')
    invoice_return_id = fields.Integer('Invoice Return ID')
    delivery_date = fields.Date('Delivery Date', required=True)
    state = fields.Selection([
        ('pending', 'Pending'),
        ('confirm', 'Confirmation'),
        ('cancel', 'Cancelled'),
    ], 'Status', readonly=True, copy=False, help="Gives the status of the Mainfest", index=True)


    @api.model
    def create(self, vals):

        order_number = None
        today_date = fields.date.today()

        has_return = False
        inv_id = None
        for items in vals.get('outbound_line'):
            if items:
                for items2 in items:
                    if isinstance(items2, dict):
                        inv_id = items2.get('invoice_id')
                        order_number = items2.get('magento_no')
                        if float(items2.get('quantity')) > float(items2.get('delivered')):
                            items2['return_qty'] = str(float(items2.get('quantity')) - float(items2.get('delivered')))
                            has_return = True
                        else:
                            items2['return_qty'] = '0.00'

                        today_date = items2.get('delivery_date')

        vals['has_return'] = has_return
        vals['name'] = order_number
        vals['state'] = 'pending'
        today_date = vals.get('delivery_date')

        inv_data = self.env['account.move'].browse([inv_id])
        if inv_data:
            if inv_data.delivered:
                raise Warning(_('This Order Already Delivered'))
        # if context is None:
        #     context = {}

        section_id = super(WmsManifestOutbound, self).create(vals)
        inv_data.write({'delivery_date': today_date,
                        'delivered': True})

        # self.env.cr.execute("update account_invoice set delivery_date=%s,delivered=TRUE where id=%s",
        #                     ([today_date, inv_id]))
        #
        # self.env.cr.commit()

        if order_number is not None:
            sales = self.env['sale.order'].search([('client_order_ref', '=', order_number),('state', '!=','cancel')])

            if sales.delivered_amount == sales.amount_total:
                sales.write({'delivery_dispatch': True,
                             'partially_invoiced_dispatch': False,
                             })
                # self.env.cr.execute("update sale_order set delivery_dispatch=TRUE where id=%s", ([sales.id]))
                # self.env.cr.commit()
                # self.env.cr.execute("update sale_order set partially_invoiced_dispatch=FALSE where id=%s", ([sales.id]))
                # self.env.cr.commit()
            elif sales.delivered_amount < sales.amount_total and sales.delivered_amount > 0:
                sales.write({'partially_invoiced_dispatch': True})
                # self.env.cr.execute("update sale_order set partially_invoiced_dispatch=TRUE where id=%s", ([sales.id]))
                # self.env.cr.commit()

        # self.env.cr.commit()
        return section_id


class WmsManifestOutboundLine(models.Model):
    _name = "wms.manifest.outbound.line"
    _description = "Manifest Outbound Line"

    # delivered amount
    def _get_delivered_amount(self):
        res = False
        res = {}

        for manifest_line in self.browse():

            total_sum = ''
            inv_id = manifest_line.invoice_id
            self.env.cr.execute("select name from  account_move where id=%s", ([inv_id]))

            invoice_ids_obj = self.env.cr.fetchall()
            if len(invoice_ids_obj) > 0:
                total_sum = invoice_ids_obj[0]

            res[manifest_line.id] = total_sum
        return res

    wms_manifest_outbound_id = fields.Many2one('wms.manifest.outbound', 'Manifest Outbound Line ID', required=True,
                                                ondelete='cascade', index=True, readonly=True)
    invoice_id = fields.Integer('Invoice ID', readonly=True)
    magento_no = fields.Char('Order No', readonly=True)
    description = fields.Char('Description', readonly=True)
    product_id = fields.Integer('Product ID', readonly=True)
    amount = fields.Float('Amount', readonly=True)
    quantity = fields.Float('Quantity', readonly=True)
    delivered = fields.Float('Delivered')
    return_qty = fields.Float('Return', readonly=True)

    # ## Block code code V-14
    # _columns = {
    #     'wms_manifest_outbound_id': fields.many2one('wms.manifest.outbound', 'Manifest Outbound Line ID', required=True,
    #                                                 ondelete='cascade', index=True, readonly=True),
    #     'invoice_id': fields.integer('Invoice ID', readonly=True),
    #     'magento_no': fields.char('Order No', readonly=True),
    #     'description': fields.char('Description', readonly=True),
    #     'product_id': fields.integer('Product ID', readonly=True),
    #     'amount': fields.float('Amount', readonly=True),
    #     'quantity': fields.float('Quantity', readonly=True),
    #     'delivered': fields.float('Delivered'),
    #     'return_qty': fields.float('Return', readonly=True)
    # }
