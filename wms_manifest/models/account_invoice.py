from odoo import models, fields


class AccountInvoice(models.Model):
    _inherit = "account.move"

    assigned = fields.Boolean(help="Assigned")


class SaleOrder(models.Model):
    _inherit = "sale.order"

    partially_invoiced_dispatch = fields.Boolean(help="Partially Dispatched")
