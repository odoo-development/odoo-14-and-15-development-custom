from odoo import api, fields, models, _


class COGSReportWizard(models.TransientModel):
    _name = "cogs.report.wizard"
    _description = "COGS Report"

    date_from = fields.Date(string="Date From", default=fields.Datetime.now())
    date_to = fields.Date(string="Date To", default=fields.Datetime.now())
    customer_classification = fields.Selection([('All', 'All'),
                                                 ('Ananta', 'Ananta'),
                                                 ('Corporate', 'Corporate'),
                                                 ('Retail', 'Retail'),
                                                 ('Employee', 'Employee'),
                                                 ('General', 'General'),
                                                 ('SOHO', 'SOHO'),
                                                 ], string='Customer Classification')

    customer = fields.Many2one('res.partner', string='Customer')

    def print_report_xls(self):
        # order_ids = self.env['account.move'].search([('state', '!=', 'cancel')])
        data = {
            'date_from': self.date_from,
            'date_to': self.date_to,
            'customer_classification': self.customer_classification,
            'customer': self.customer.id,

            # 'form_data': self.read()[0],
            # 'order_list': order_ids
        }
        return self.env.ref('sales_cogs_margin.action_cogs_report_xlsx').report_action(self, data=data)

    

