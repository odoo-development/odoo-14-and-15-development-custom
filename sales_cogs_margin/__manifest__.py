{
    'name': 'Sales Cogs Margin Report',
    'version': '14.0.1',
    'author': "Sindabad",
    'category': 'Sales',
    'summary': 'Details Summary of Delievred quantity with profit and Loss Statement',
    'depends': ['base', 'sale', 'report_xlsx'],
    'data': [
        'security/sales_cogs_security.xml',
        'security/ir.model.access.csv',
        'wizard/cogs_report_wizard_view.xml',
        'report/action_cogs_report.xml',
        'views/cogs_report_menu.xml',
        'views/action_manager.xml',
    ],
}
