import base64
import io
from odoo import models
import datetime

def _unescape(text):
    ##
    # Replaces all encoded characters by urlib with plain utf8 string.
    #
    # @param text source text.
    # @return The plain text.
    from urllib.parse import unquote_plus
    try:
        text = unquote_plus(text)
        return text
    except Exception as e:
        return text

# def date_plus_one(date_str):
#
#     date_obj = datetime.datetime.strptime(date_str, '%Y-%m-%d') + datetime.timedelta(days=1)
#
#     increased_date = date_obj.strftime('%Y-%m-%d')
#
#     return increased_date


class COGSReportXlsx(models.AbstractModel):
    _name = 'report.sales_cogs_margin.cogs_report_xls'
    _description = 'COGS Report'
    _inherit = 'report.report_xlsx.abstract'

    def generate_xlsx_report(self, workbook, data, objects):
        # domain = [('state', '!=', 'cancel')]
        #
        #
        # if data.get('date_from'):
        #     domain.append(('date', '>=', data.get('date_from')))
        # if data.get('date_to'):
        #     domain.append(('date', '<=', data.get('date_to')))
        # if data.get('customer_classification'):
        #     domain.append(('customer_classification', '=', data.get('customer_classification')))
        # if data.get('customer'):
        #     domain.append(('customer', '=', data.get('customer')))

        from_date = data.get('date_from')
        to_date = data.get('date_to')
        # to_date = date_plus_one(to_date)

        customer_classification = data.get('customer_classification')
        customer = data.get('customer')

        report_name = "COGS Report"
        sheet = workbook.add_worksheet(report_name)
        bold = workbook.add_format({'bold': True, 'align': 'center', 'bg_color': '#fffbed', 'border': True})
        title = workbook.add_format({'bold': True, 'align': 'center', 'bg_color': '#f2eee4', 'border': True})
        header_row_style = workbook.add_format({'bold': True, 'align': 'center', 'border': True})

        sheet.merge_range('A1:F1', 'COGS Report', title)

        invoices = "SELECT account_move_line.move_id AS invoice_id,account_move_line.product_id,account_move_line.quantity,account_move_line.unit_cost AS cost_rate,(account_move_line.quantity * account_move_line.unit_cost) AS total_cost, account_move_line.price_unit AS sale_rate, account_move_line.price_subtotal AS total_sale, (account_move_line.price_subtotal - (account_move_line.quantity * account_move_line.unit_cost)) AS profit_loss, sol.x_custom_discount as custom_discount,sol.x_discount as campaign_promotoion_discount,so.date_order,so.customer_classification,so.client_order_ref,so.name as odoo_order,so.customer_classification,so.note,wh.name as wh_name,inv.name,inv.move_type,inv.payment_state,inv.create_date,inv.delivery_date,child.name as customer,child.email as customer_email,(select parent.name from res_partner as parent where child.parent_id=parent.id) as company_name,(select res_partner.email from res_partner where child.parent_id=res_partner.id) as company_email,(select parent1.parent_code from res_partner as parent1 where  child.parent_id=parent1.id) as company_code,product.product_name,product.default_code,product_category.name as product_category  FROM account_move_line,account_move as inv,sale_order as so,sale_order_line as sol,stock_warehouse as wh, res_partner as child,product_product as product,product_template as product_template,product_category as product_category WHERE account_move_line.move_id=inv.id and inv.invoice_origin=so.name and so.id=sol.order_id and account_move_line.product_id=sol.product_id and so.warehouse_id=wh.id and so.partner_id=child.id and account_move_line.product_id=product.id and product.product_tmpl_id=product_template.id and product_template.categ_id= product_category.id AND account_move_line.product_id IS NOT NULL AND account_move_line.price_subtotal IS NOT NULL AND account_move_line.move_id IN (SELECT id FROM account_move WHERE account_move.state IN ('posted') AND account_move.move_type IN ('out_invoice', 'out_refund')) and CAST((inv.create_date + INTERVAL '6 hour') as DATE ) >= %s and CAST((inv.create_date + INTERVAL '6 hour') as DATE )<= %s"

        self.env.cr.execute(invoices, (from_date, to_date))
        objects = self.env.cr.dictfetchall()

        row = 3
        col = 0

        sheet.write(row, col, 'Date Range: ', header_row_style)
        sheet.write(row, col+1, data.get('date_from'), header_row_style)
        sheet.write(row, col+2, 'To', header_row_style)
        sheet.write(row, col+3, data.get('date_to'), header_row_style)

        row += 2

        # Header Row
        sheet.set_column(0, 25, 25)
        sheet.write(row, col, 'Order Date', header_row_style)
        sheet.write(row, col + 1, 'Order Number', header_row_style)
        sheet.write(row, col + 2, 'Wharehouse', header_row_style)
        sheet.write(row, col + 3, 'Return', header_row_style)
        sheet.write(row, col + 4, 'Odoo Number', header_row_style)
        sheet.write(row, col + 5, 'Customer Name ', header_row_style)
        sheet.write(row, col + 6, 'Company Name', header_row_style)
        sheet.write(row, col + 7, 'Company Code', header_row_style)
        sheet.write(row, col + 8, 'Classification', header_row_style)
        sheet.write(row, col + 9, 'Customer Email', header_row_style)
        sheet.write(row, col + 10, 'Company Email', header_row_style)
        sheet.write(row, col + 11, 'Product Name', header_row_style)
        sheet.write(row, col + 12, 'SKU', header_row_style)
        sheet.write(row, col + 13, 'Product Category', header_row_style)
        sheet.write(row, col + 14, 'Quantity', header_row_style)
        sheet.write(row, col + 15, 'Cost Rate', header_row_style)
        sheet.write(row, col + 16, 'Total Cost', header_row_style)
        sheet.write(row, col + 17, 'Sale Rate', header_row_style)
        sheet.write(row, col + 18, 'Total Sale', header_row_style)
        sheet.write(row, col + 19, 'Profit Loss', header_row_style)
        sheet.write(row, col + 20, 'Custom Discount', header_row_style)
        sheet.write(row, col + 21, 'Campaign/Promotoion Discount', header_row_style)
        sheet.write(row, col + 22, 'Invoice Number', header_row_style)
        sheet.write(row, col + 23, 'Create Date', header_row_style)
        sheet.write(row, col + 24, 'Delivery Date', header_row_style)
        sheet.write(row, col + 25, 'Note', header_row_style)
        sheet.write(row, col + 26, 'Payment State', header_row_style)

        row += 2

        for line in objects:
            # for line in inv.invoice_line_ids:
            sheet.write(row, col, line.get('date_order'))
            sheet.write(row, col + 1, line.get('client_order_ref'))
            sheet.write(row, col + 2, line.get('wh_name'))
            if line.get('move_type') and line.get('move_type') =='out_refund':
                sheet.write(row, col + 3, 'Return')
            else:
                sheet.write(row,col + 3,'')
            sheet.write(row, col + 4, line.get('odoo_order'))
            sheet.write(row, col + 5,  _unescape(line.get('customer')))
            sheet.write(row, col + 6,  _unescape(line.get('company_name')))
            sheet.write(row, col + 7, line.get('company_code'))
            sheet.write(row, col + 8, line.get('customer_classification'))
            sheet.write(row, col + 9, line.get('customer_email'))
            sheet.write(row, col + 10, line.get('company_email'))
            sheet.write(row, col + 11, _unescape(line.get('product_name')))
            sheet.write(row, col + 12, line.get('default_code'))
            sheet.write(row, col + 13, line.get('product_category'))
            sheet.write(row, col + 14, line.get('quantity'))
            sheet.write(row, col + 15, line.get('cost_rate'))
            if line.get('move_type') and line.get('move_type') == 'out_refund':
                total_cost = line.get('total_cost') if line.get('total_cost') else 0
                sheet.write(row, col + 16, (total_cost * (-1)))
            else:
                total_cost = line.get('total_cost') if line.get('total_cost') else 0
                sheet.write(row, col + 16, total_cost)
            sheet.write(row, col + 17, line.get('sale_rate'))
            if line.get('move_type') and line.get('move_type') =='out_refund':
                sheet.write(row, col + 18, (line.get('total_sale')*(-1)))
            else:
                sheet.write(row, col + 18, line.get('total_sale'))
            if line.get('move_type') and line.get('move_type') =='out_refund':
                profit_loss = line.get('profit_loss') if line.get('profit_loss') else 0
                sheet.write(row, col + 19, round(((profit_loss)*(-1)), 2))
            else:
                profit_loss = line.get('profit_loss') if line.get('profit_loss') else 0
                sheet.write(row, col + 19, round(profit_loss, 2))
            if line.get('custom_discount'):
                sheet.write(row, col + 20, (line.get('quantity')*line.get('custom_discount')))
            else:
                sheet.write(row, col + 20, 0)
            if line.get('campaign_promotoion_discount'):
                sheet.write(row, col + 21, round(line.get('campaign_promotoion_discount'), 2))
            else:
                sheet.write(row, col + 21, 0)
            sheet.write(row, col + 22, line.get('name'))
            sheet.write(row, col + 23, line.get('create_date'))
            sheet.write(row, col + 24, line.get('delivery_date'))
            sheet.write(row, col + 25, line.get('note'))
            sheet.write(row, col + 26, line.get('payment_state'))

            row += 1
