# -*- coding: utf-8 -*-

from odoo import api, fields, models,_


class AccountMoveLine(models.Model):
    _inherit = "account.move.line"

    image_128 = fields.Image(string="Image",compute="_onchange_product_images")
    invoice_line_prod_uom = fields.Char(string='UOM')

    @api.depends('product_id')
    def _onchange_product_images(self):
        for line in self:
            line.image_128 = line.product_id.image_128
            line.invoice_line_prod_uom = line.product_id.prod_uom
