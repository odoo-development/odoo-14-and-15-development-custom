# -*- coding: utf-8 -*-

from odoo import api, fields, models,_


class PurchaseOrderLine(models.Model):
    _inherit = "purchase.order.line"

    image_128 = fields.Image(string="Image", compute="onchange_purchase_product_image")
    po_line_prod_uom = fields.Char(string='UOM')

    @api.onchange('product_id')
    def onchange_purchase_product_image(self):
        for product in self:
            product.image_128 = product.product_id.image_128
            product.po_line_prod_uom = product.product_id.prod_uom
