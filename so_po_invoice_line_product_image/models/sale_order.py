# -*- coding: utf-8 -*-

from odoo import api, fields, models,_


class SaleOrderLine(models.Model):
    _inherit = "sale.order.line"

    image_128 = fields.Image(string="Image", compute="onchange_sake_product_image")
    so_line_prod_uom = fields.Char(string='UOM')

    @api.onchange('product_id')
    def onchange_sake_product_image(self):
        for product in self:
            product.image_128 = product.product_id.image_128
            product.so_line_prod_uom = product.product_id.prod_uom
