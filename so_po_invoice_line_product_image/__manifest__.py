# -*- coding: utf-8 -*-

{
    'name': "Product Image On Sale Order Line, Purchase Order Line and Invoice/Vendor Bill Line",
    'version': "14.0.0.1",
    'category': "Sales",
    'summary': "Display Product Image On Sale Order Line, Purchase Order Line and Invoice/Vendor Bill Line and also display in PDF report",
    'description': """
	
			Display product image on sale order line, purchase order line and invoice/vendor bill line. It will also display product image on sale order, purchase and invoice/vendor bill report. 

	""",
    'author': "Sindabad",
    'depends': ['base', 'sale_management', 'purchase', 'account'],
    'data': [
        'report/sale_order_report.xml',
        'report/purchase_order_report.xml',
        'report/invoice_report.xml',
        'views/view_sale_order.xml',
        'views/view_purchase_order.xml',
        'views/view_invoice.xml',
    ],
    'installable': True,
    'auto_install': False,
    'application': False,
}
