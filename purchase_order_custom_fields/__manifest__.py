# Copyright 2021 Rocky

{
    "name": "Purchase Order Custom Fields",
    "version": "14.0",
    "author": "Rocky",
    "category": "Inventory/Purchase",
    "license": "AGPL-3",
    "depends": ['purchase'],
    "data": [
        "views/purchase_order_view.xml",
    ],
    "installable": True,
}
