# Copyright 2021 Rocky

from odoo import fields, models, api, _


class PurchaseOrder(models.Model):
    _inherit = 'purchase.order'

    x_credit_days = fields.Integer(string="Credit Days")
    x_not_pick_up_reason = fields.Char(string="Not Pick Up Reason")
    x_payment_validity = fields.Char(string="Payment Validity")
    x_sale_classification_type = fields.Selection(
        [('b2r', 'B2R'),
         ('b2e', 'B2E'),
         ('b2c', 'B2C'),
         ('b2re', 'B2R & B2E'),
         ('undefined', 'Undefined')],
        string="Sale Classification Type", default='b2r')

