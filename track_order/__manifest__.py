# -*- coding: utf-8 -*-
{
    'name': "Sale Order Delivery Tracking Information",
    'summary': """ This module will help you to Sale Order Delivery Tracking Information based on model """,
    'description': """ Sale Order Delivery Tracking Information based on model """,
    'author': "Sindabad",
    'category': 'Customise',
    'depends': [],
    'license': 'AGPL-3',
    'data': [
        'security/track_order_sa.xml',
        'security/ir.model.access.csv',
        'views/track_order_view.xml'
    ],
    'installable': True,
    'application': True,
}
