from odoo import api, fields, models, _
from odoo import tools


class TrackOrder(models.Model):
    _name = 'track.order'
    _auto = False
    description = "Track Order"

    name = fields.Many2one('sale.order', string="Sale Order ID", index=True)
    odoo_order_number = fields.Char(string="Odoo Order Number", index=True)
    magento_order_number = fields.Char(string="Magento Order Number", index=True)
    order_date = fields.Datetime(string="Order Date", index=True)
    order_amount = fields.Float(string="Order Amount", index=True)
    expected_delivery_date = fields.Date(string="Expected Delivery Date", index=True)
    delivery_slot = fields.Char(string="Delivery Slot", index=True)
    sale_status = fields.Char(string="SO Status", index=True)
    invoice_status = fields.Char(string="Invoice Status", index=True)
    customer_classification = fields.Char(string="Customer Classification", index=True)
    no_of_product = fields.Integer(string="Number of Product", index=True)
    total_order_qty = fields.Float(string="Total Order QTY", index=True)
    invoice_id = fields.Many2one('account.move', string="Invoice ID", index=True)
    invoice_amount = fields.Float(string="Invoice Amount", index=True)
    manifest_id = fields.Many2one('wms.manifest.process', string="Manifest ID", index=True)
    manifest_number = fields.Char(string="Manifest Number", index=True)
    vehicle_number = fields.Char(string="Vehicle Number", index=True)
    assigned_to = fields.Char(string="Delivery Man", index=True)
    confirm_time = fields.Datetime(string="Dispatch Time", index=True)

    def init(self):
        tools.drop_view_if_exists(self._cr, 'track_order')
        self._cr.execute("""
                CREATE OR REPLACE VIEW track_order AS (
                SELECT row_number() OVER () AS id, 
                order_pick.id as name, 
                odoo_order_number,
                order_pick.magento_order_number as magento_order_number,
                order_pick.order_date as order_date,
                order_pick.expected_delivery_date as expected_delivery_date,
                order_pick.delivery_slot as delivery_slot,
                order_pick.amount as order_amount,
                order_pick.state as sale_status,
                order_pick.invoice_status as invoice_status,
                order_pick.customer_classification as customer_classification,
                COUNT(distinct(product)) as no_of_product,
                sum(order_qty) as total_order_qty,
                invoice.id as invoice_id,
                sum(invoice.amount_total) as invoice_amount,
                man_process.id as manifest_id,
                man_process.name as manifest_number,
                man_process.vehicle_number as vehicle_number,
                man_process.assigned_to as assigned_to,
                man_process.confirm_time as confirm_time
                from
                (select so.id,
                so.name as odoo_order_number,
                so.client_order_ref as magento_order_number, 
                so.date_order as order_date,
                so.x_expected_delivery_date as expected_delivery_date,
                so.x_delivery_slot as delivery_slot,
                so.amount_total as amount,
                so.state,
                so.invoice_status,
                so.customer_classification, 
                sol.name as product,
                sol.product_uom_qty as order_qty
                from sale_order as so,sale_order_line as sol
                Where so.id=sol.Order_id and sol.product_id<>1 
                and so.date_order >= (CURRENT_DATE -7)) as order_pick
                left join account_move as invoice on  order_pick.odoo_order_number=invoice.invoice_origin
left join wms_manifest_line as man_line on invoice.id=man_line.invoice_id left join wms_manifest_process as man_process
on  man_line.wms_manifest_id=man_process.id       
                group by order_pick.id,order_pick.odoo_order_number,order_pick.magento_order_number,order_pick.order_date,
                order_pick.expected_delivery_date,order_pick.delivery_slot,order_pick.amount,order_pick.state,order_pick.invoice_status,
                order_pick.customer_classification,invoice.id,man_process.id,man_process.name,man_process.vehicle_number,
                man_process.assigned_to,man_process.confirm_time  
                order by order_pick.order_date
    )""")
