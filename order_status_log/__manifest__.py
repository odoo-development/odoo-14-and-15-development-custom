{
    'name': 'Bulk Order Status Synchronization',
    'version': '14.0',
    'category': 'Order',
    'author': 'Sindabad',
    'summary': 'Order Status Synchronization',
    'description': 'Order Status Synchronization',
    'depends': ['odoo_magento_connect','sale','stock','wms_manifest'],
    'data': [

    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}