from datetime import datetime, timedelta
import time, datetime, json
import requests
import re
import logging
from odoo import api, fields, models, _
from ...odoo_to_magento_api_connect.api_connect import get_magento_token




XMLRPC_API = '/index.php/api/xmlrpc'

_logger = logging.getLogger(__name__)


class OrderStatus_synch(models.Model):
    _name = "order.status.synch.log"

    odoo_order_id = fields.Integer(string="Odoo Order ID")
    magento_id = fields.Char('Magento No')
    order_state = fields.Char('Order Status')
    state_time = fields.Char('State Date and Time')
    sent_to_megento_flag = fields.Boolean('Sent To Magento')

    def bulk_order_status_synch_log(self):

        context = self.env.context
        if context is None:
            context = {
                'lang': 'en_US',
                'params': {'action': 404},
                'tz': 'Asia/Dhaka',
                'uid': self.env.uid
            }

        processing_list=[]
        p_dis_list=[]
        dis_list=[]
        p_del_list=[]
        del_list=[]
        complt_list=[]
        point_gain_list=[]
        invoiced_list=[]
        picked_list=[]

        status_log = self.env['order.status.synch.log']
        so_obj = self.env['sale.order']

        status_log_obj = status_log.search([('sent_to_megento_flag', '=', False)])
        statu_log_ids = status_log_obj.mapped('id')

        # status_log_obj = status_log.browse(cr, uid, statu_log_ids, context=context)
        for st_items in status_log_obj:
            tmp_dict={
                'orderId':st_items.magento_id,
                'comment':'',
                'createdAt':st_items.state_time,
            }
            if str(st_items.order_state) == str('processing'):
                tmp_dict['status']='processing'
                processing_list.append(tmp_dict)

            if str(st_items.order_state) == str('partially_dispatched'):
                tmp_dict['status']='partially_dispatched'
                p_dis_list.append(tmp_dict)

            if str(st_items.order_state) == str('dispatched'):
                tmp_dict['status']='dispatched' ## Later it will reflect live synch, if we required changes
                dis_list.append(tmp_dict)

            if str(st_items.order_state) == str('partially_delivered'):
                tmp_dict['status']='partially_delivered'
                p_del_list.append(tmp_dict)

            if str(st_items.order_state) == str('delivered'):
                tmp_dict['status']='delivered'
                del_list.append(tmp_dict)

            # if str(st_items.order_state) == str('complete'):
            #     tmp_dict['status']='complete'
            #     complt_list.append(tmp_dict)

            if str(st_items.order_state) == str('point_gain'):
                tmp_dict['status']='point_gain'
                point_gain_list.append(tmp_dict)

            if str(st_items.order_state) == str('invoiced'):
                tmp_dict['status']='invoiced'
                invoiced_list.append(tmp_dict)

            if str(st_items.order_state) == str('picked'):
                tmp_dict['status']='picked'
                picked_list.append(tmp_dict)

            tmp_dict={}

        final_list= processing_list + p_dis_list + dis_list + p_del_list + del_list + complt_list + point_gain_list

        if len(final_list) >0:

            magento_style_data = {
                      "statusData": final_list
                    }

            try:

                sale_order_ids = so_obj.search([('client_order_ref', '!=', None)], limit=3)
                sale_order_id = sale_order_ids.mapped('id')
                token, url_root = get_magento_token(self, sale_order_id)

                if token:
                    token = token.replace('"', "")
                    headers = {'Authorization': token, 'Content-Type': 'application/json'}

                    post_url = url_root + "/index.php/rest/V1/odoomagentoconnect/OrderStatusBulk"
                    data = json.dumps(magento_style_data)

                    post_resp = requests.post(post_url, data=data, headers=headers)

                    self.env.cr.execute("UPDATE order_status_synch_log SET sent_to_megento_flag=TRUE WHERE id in %s", (tuple(statu_log_ids),))
                    self.env.cr.commit()
            except:
                pass
        count =1

        for p_items in processing_list:

            magento_no = str(p_items.get('orderId'))
            try:
                so = so_obj.search([('client_order_ref', '=',magento_no),('state', '!=','cancel')],limit=1)
                # so = self.pool.get('sale.order').browse(cr, uid, ids, context=context)
                parent = so.partner_invoice_id
                inv_address_mobile=''
                inv_address_phone=''

                # for i in range(5):
                #     if len(parent.parent_id) == 1:
                #         parent = parent.parent_id
                #     else:
                #         parent = parent
                #
                #         break

                inv_address_mobile = parent.mobile
                inv_address_phone = parent.phone

                if len(inv_address_mobile) >10:
                    inv_address_phone=inv_address_mobile

                """
                phone_number = str(so.partner_id.mobile) if so.partner_id.mobile else company_phone
                if not phone_number:
                    phone_number = str(so.partner_id.phone)
                """
                so_placed_date = datetime.datetime.now().strftime('%A, %d-%m-%Y')
                payment_type = str(so.note).split(":-")[-1]
                email = str(so.partner_id.email)

                sms_text = ""
                if len(inv_address_phone)>10:
                    sms_text = "Your order {0} has been confirmed. You will get updates once items has been shipped.".format(
                        str(so.client_order_ref))

                    self.env["send.sms.on.demand"].send_sms_on_demand(sms_text, inv_address_phone,
                                                                           str(parent.name))
                try:
                    # send email
                    if not email[0].isdigit():

                        if re.search("^(\d+)@sindabad.com$", email) :
                            sin_email_match = re.search("^(\d+)@sindabad.com$", email)
                        elif re.search("^sin(\d+)@sindabad.com$", email):
                            sin_email_match = re.search("^sin(\d+)@sindabad.com$", email)
                        elif re.search("@sindabadretail.com$", email):
                            sin_email_match = re.search("@sindabadretail.com$", email)
                        else:
                            sin_email_match = None

                        if sin_email_match is None:
                            # e_log_obj = self.env['send.email.on.demand.log']
                            #
                            # email_data = {
                            #     'mail_text': sms_text,
                            #     'model': "sale.order",
                            #     'so_id': so.id,
                            #     'email': email
                            # }

                            # email_log_id = e_log_obj.create(email_data)
                            # tmp_data = e_log_obj.browse(cr, uid, email_log_id, context=context)

                            # send email
                            email_template_obj = self.env['mail.template']
                            # template_ids = email_template_obj.search(cr, uid, [('name', '=', 'SO process mail')], context=context)
                            template_ids = email_template_obj.sudo().search([('name', '=', 'Order Confirmed')])
                            template_ids.send_mail(so.id, force_send=True)
                            # template_ids.send_mail(template_ids[0], so.id, force_send=True)
                            # self.env['mail.template'].browse(template_id).send_mail(attendee.id, force_send=force_send)

                except:
                    pass
            except:
                pass
            # try:
            #     status = 'processing'
            #     self.offer_notification_api(magento_no, status)
            # except:
            #     pass

        return True

    def offer_notification_api(self, order, status):
        try:
            so_obj = self.env["sale.order"]
            so = so_obj.search([('client_order_ref', '=', str(order))])
            status = status
            headers = {'Authorization': 'jl8t%4Re1C74r7hjWQZv#$Zl9v7dYuz', 'Content-Type': 'application/json'}

            obj_magento = self.env['connector.instance'].browse([1])

            if obj_magento.name == 'https://sindabad.com':

                get_url = "https://offers.sindabad.com/api/customer-notification?" \
                          "customer_id=" + str(so.partner_id.email) + "&order_id=" + \
                          str(so.client_order_ref) + "&order_status=" + str(status) + "&type=order"
            else:
                get_url = "https://offersdev.sindabad.com/api/customer-notification?" \
                          "customer_id=" + str(so.partner_id.email) + "&order_id=" + \
                          str(so.client_order_ref) + "&order_status=" + str(status) + "&type=order"
            # 1323
            # get_url = "https://offers.sindabad.com/api/customer-notification?" \
            #           "customer_id=1323&order_id=" + \
            #           str(1000179140) + "&order_status=" + str('delivered') + "&type=order"

            get_resp = requests.get(get_url, headers=headers)
            order_data = json.loads(get_resp.text)
        except:
            pass
        return True
