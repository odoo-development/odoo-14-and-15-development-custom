import datetime
import requests
from odoo import api, fields, models, _
import logging

XMLRPC_API = '/index.php/api/xmlrpc'

_logger = logging.getLogger(__name__)


class SaleOrder(models.Model):
    _inherit = "sale.order"

    def action_confirm(self):

        res = super(SaleOrder, self).action_confirm()
        for so in self:
            so.write({'date_order':fields.Datetime.now()})
            try:
                data = {
                    'odoo_order_id': so.id,
                    'magento_id': str(so.client_order_ref),
                    'order_state': str('processing'),
                    'state_time': fields.datetime.now(),
                }
                new_r = self.env["order.status.synch.log"].sudo().create(data)
            except:
                pass

            return True


class WmsManifestProcess(models.Model):
    _inherit = "wms.manifest.process"

    def confirm_wms_manifest(self):

        # data = self.browse(cr, uid, ids[0], context=context)
        super(WmsManifestProcess, self).confirm_wms_manifest()

        for data in self:
            for items in data.picking_line:
                magento_no = str(items.magento_no)
                sales = self.env['sale.order'].search([('client_order_ref', '=', magento_no),('state', '!=','cancel')])

                if len(sales) > 0:

                    for so_item in sales:
                        if so_item.state != 'cancel':
                            if int(so_item.delivered_amount) == int(so_item.amount_total):
                                dispatch_wms_query = "UPDATE sale_order SET invoiced_dispatch=TRUE  WHERE id='{0}'".format(so_item.id)
                                self.env.cr.execute(dispatch_wms_query)
                                self.env.cr.commit()

                                try:
                                    s_uid = 1
                                    data = {
                                        'odoo_order_id': so_item.id,
                                        'magento_id': str(magento_no),
                                        'order_state': str('dispatched'),
                                        'state_time': fields.datetime.now(),
                                    }
                                    new_r = self.env["order.status.synch.log"].sudo().create(data)
                                except:
                                    pass

                            else:
                                dispatch_wms_query = "UPDATE sale_order SET partially_invoiced_dispatch=TRUE  WHERE id='{0}'".format(so_item.id)
                                self.env.cr.execute(dispatch_wms_query)
                                self.env.cr.commit()

                                try:
                                    data = {
                                        'odoo_order_id': so_item.id,
                                        'magento_id': str(magento_no),
                                        'order_state': str('partially_dispatched'),
                                        'state_time': fields.datetime.now(),
                                    }
                                    new_r = self.env["order.status.synch.log"].sudo().create(data)
                                except:
                                    pass


        return True


class WmsManifestOutbound(models.Model):
    _inherit = "wms.manifest.outbound"


    def create(self,vals):
        outbound = super(WmsManifestOutbound, self).create(vals)
        today_date = datetime.date.today()
        if outbound:
            inv_id = outbound.outbound_line[0].invoice_id
            self.env.cr.execute("update account_move set delivery_date=%s,delivered=TRUE where id=%s",
                                ([today_date, inv_id]))
            self.env.cr.commit()
        # if context.has_key('manifest_line_id') and context['manifest_line_id']:
        #
        #     cr.execute("update wms_manifest_line set manifest_out_status='assigned' where id=%s", ([context['manifest_line_id']]))
        #
        #     cr.commit()

        # self.env['wms.manifest.outbound.line'].search([('wms_manifest_outbound_id','=',outbound.id)])
        order_number = outbound.name
        sales = self.env['sale.order'].search([('client_order_ref', '=', order_number),('state', '!=','cancel')])

        if order_number is not None:
            invoiced_amount = int(sales.invoiced_amount) + int(sales.canceled_amount)

            if int(invoiced_amount) == int(sales.amount_total):
                self.env.cr.execute("update sale_order set delivery_dispatch=TRUE where id=%s", ([sales.id]))
                self.env.cr.commit()
                self.env.cr.execute("update sale_order set partially_invoiced_dispatch=FALSE where id=%s", ([sales.id]))
                self.env.cr.commit()
                try:
                    s_uid = 1

                    data = {
                        'odoo_order_id': sales.id,
                        'magento_id': str(order_number),
                        'order_state': str('point_gain'),
                        'state_time': fields.datetime.now(),
                    }
                    new_r = self.env["order.status.synch.log"].sudo().create(data)
                except:
                    pass
                # try:
                #     sale_order.action_status(cr, uid, [sales.id, ], 'delivered', context=context)
                # except:
                #     pass
            elif int(invoiced_amount) < int(sales.amount_total) and int(invoiced_amount) > 0:
                self.env.cr.execute("update sale_order set partially_invoiced_dispatch=TRUE where id=%s", ([sales.id]))
                self.env.cr.commit()

                try:
                    s_uid = 1

                    data = {
                        'odoo_order_id': sales.id,
                        'magento_id': str(order_number),
                        'order_state': str('delivered'),
                        'state_time': fields.datetime.now(),
                    }
                    new_r = self.env["order.status.synch.log"].sudo().create(data)
                except:
                    pass

        return outbound
