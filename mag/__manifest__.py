# Copyright 2021 Rocky

{
    "name": "Mag",
    "version": "14.0",
    "author": "Sindabad",
    "category": "Sales Management",
    "license": "AGPL-3",
    "depends": ["sale"],
    "data": [
        'security/ir.model.access.csv',
        'views/mag_view.xml',
    ],
    "installable": True,
}
