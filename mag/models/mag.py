# Author Rocky 2021

from odoo import fields, models


class Mag(models.Model):
    _name = "mag"

    odoo_id = fields.Integer('Odoo ID')
    address = fields.Char('Address')

