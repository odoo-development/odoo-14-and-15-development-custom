import datetime
from datetime import datetime, date
from odoo.exceptions import UserError, ValidationError, Warning
from odoo import api, fields, models, _

class SaleOrder(models.Model):
    _inherit = ["sale.order"]

    def action_button_confirm(self):

        context = {}
        ids = self.ids
        assert len(ids) == 1, 'This option should only be used for a single id at a time.'
        self.signal_workflow('order_confirm')
        if context.get('send_email'):
            self.force_quotation_send()

        self.action_view_delivery()

        ###  ai code age off  kora
        ## Insert Confirmation Log

        # try:
        #     s_uid=1
        #     abc=self.browse(cr, uid, ids[0], context=context)
        #     data = {
        #         'odoo_order_id': ids[0],
        #         'magento_id': str(abc.client_order_ref),
        #         'order_state': str('processing'),
        #         'state_time': fields.datetime.now(),
        #     }
        #     new_r = self.env["order.status.synch.log"].create(cr, s_uid, data)
        # except:
        #     pass

        ## Ends Here

        return True

    def action_view_delivery(self):
        '''
        This function returns an action that display existing delivery orders
        of given sales order ids. It can either be a in a list or in a form
        view, if there is only one delivery order to show.
        '''

        mod_obj = self.env['ir.model.data']
        # act_obj = self.env['ir.actions.act_window']
        #
        # result = mod_obj.get_object_reference('stock', 'action_picking_tree_all')
        # id = result and result[1] or False
        # result = act_obj.read([id])[0]

        action = self.env["ir.actions.actions"]._for_xml_id("stock.action_picking_tree_all")

        # compute the number of delivery orders to display
        pick_ids = []
        for so in self:
            pick_ids += [picking.id for picking in so.picking_ids]

        # choose the view_mode accordingly
        if len(pick_ids) > 1:
            action['domain'] = "[('id','in',[" + ','.join(map(str, pick_ids)) + "])]"
        else:
            form_view = [(self.env.ref('stock.view_picking_form').id, 'form')]
            if 'views' in action:
                action['views'] = form_view + [(state, view) for state, view in action['views'] if view != 'form']
            else:
                action['views'] = form_view
            action['res_id'] = pick_ids and pick_ids[0] or False

            # res = mod_obj.get_object_reference('stock', 'view_picking_form')
            # action['views'] = [(res and res[1] or False, 'form')]
            # action['res_id'] = pick_ids and pick_ids[0] or False

        if len(pick_ids) == 1 and (str(so.state) == "draft" or str(so.state) == "progress"):
            self.env['stock.picking'].action_assign(pick_ids)

        return action


# class StockMove(models.Model):
#     _inherit = "stock.move"
#
#     quant_ids = fields.Many2many('stock.quant', "move_quant_rel", 'move_id', 'quant_id', 'Moved Quants')


class StockPicking(models.Model):
    _inherit = "stock.picking"

    def waiting_refresh_action(self):

        last_few_days_order_query = "select id,origin,state from stock_picking where state='waiting'"

        self.env.cr.execute(last_few_days_order_query)
        all_order_origin_data = self.env.cr.fetchall()
        uid = self.env.uid

        if len(all_order_origin_data) > 0:

            all_so_id_list = list()
            for all_order_origin in all_order_origin_data:

                if str(all_order_origin[1]).startswith('SO'):
                    all_so_id_list.append(all_order_origin)

            context = {
                'lang': 'en_US',
                'tz': 'Asia/Dhaka',
                'uid': uid,
                'active_model': 'stock.picking.type',
                'search_default_picking_type_id': [2],
                'default_picking_type_id': 2,
                'search_default_waiting': 1,
                'params': {'action': 379},
                'search_disable_custom_filters': True,
                'contact_display': 'partner_address',
                'active_ids': [2],
                'active_id': 2
            }

            for so_id in all_so_id_list:
                stock_picking_id = int(so_id[0])
                stock_obj = self.env['stock.picking'].browse(int(so_id[0]))

                if str(so_id[2]) == 'partially_available' or str(so_id[2]) == 'confirmed':
                    #     # unreserve
                    stock_obj.with_context(context).do_unreserve()

                    # action cancel
                    stock_obj.with_context(context).action_assign()

                if str(so_id[2]) == 'waiting':
                    # Rereservation
                    stock_obj.with_context(context).rereserve_pick()
        return True

    def action_refresh(self):
        self.refresh_action()

    def refresh_action(self):

        try:
            with open('/var/log/odoo/cron-run-time.log', 'a+') as f:
                right_now = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
                f.write("Auto Refresh Action: Start time: " + right_now + "\n")
        except:
            pass

        # 90 days but not before 1st July
        date_threshold = 90

        now = date.today()
        first_july = date(2018, 7, 1)
        date_delta = now - first_july
        uid = self.env.uid

        if int(date_delta.days) > date_threshold:

            last_few_days_order_query = "SELECT DISTINCT ON (origin) id, origin, state FROM stock_picking WHERE date > current_date - interval '" + str(
                date_threshold) + "' day AND state != 'done' AND state != 'cancel';"
        else:
            last_few_days_order_query = "SELECT DISTINCT ON (origin) id, origin, state FROM stock_picking WHERE date > '" + str(
                first_july) + "' AND state != 'done' AND state != 'cancel';"

        self.env.cr.execute(last_few_days_order_query)
        all_order_origin_data = self.env.cr.fetchall()

        if len(all_order_origin_data) > 0:

            all_so_id_list = list()
            for all_order_origin in all_order_origin_data:

                if str(all_order_origin[1]).startswith('S'):
                    all_so_id_list.append(all_order_origin)

            context = {
                'lang': 'en_US',
                'tz': 'Asia/Dhaka',
                'uid': uid,
                'active_model': 'stock.picking.type',
                'search_default_picking_type_id': [2],
                'default_picking_type_id': 2,
                'search_default_waiting': 1,
                'params': {'action': 379},
                'search_disable_custom_filters': True,
                'contact_display': 'partner_address',
                'active_ids': [2],
                'active_id': 2
            }

            for so_id in all_so_id_list:
                stock_picking_id = int(so_id[0])
                stock_obj = self.env['stock.picking'].browse(int(so_id[0]))

                if str(so_id[2]) == 'partially_available' or str(so_id[2]) == 'confirmed':
                    #     # unreserve
                    stock_obj.with_context(context).do_unreserve()

                    # action cancel
                    stock_obj.with_context(context).action_assign()

                if str(so_id[2]) == 'waiting':
                    # Rereservation
                    stock_obj.with_context(context).rereserve_pick()

        try:
            with open('/var/log/odoo/cron-run-time.log', 'a+') as f:
                right_now = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
                f.write("Auto Refresh Action: End time: " + right_now + "\n")
        except:
            pass

        return True

    def force_calculation(self):
        uid = self.env.uid
        if uid != 2:
            raise Warning(_('You do not have any access. Only IT team can do it'))

        active_id_list = self.env.context.get('active_ids')
        stock_list = self.browse(active_id_list)

        quant_ids = []
        for abc in stock_list:
            for move_line in abc.move_lines:
                for quant in move_line:
                    quant_ids.append(quant.id)

        for quant_id in quant_ids:
            cancel_expense_query_line = "UPDATE stock_quant SET total_calculated_cost=0 WHERE id={0}".format(quant_id)
            self.env.cr.execute(cancel_expense_query_line)
            self.env.cr.commit()

        for abc in stock_list:
            for move_line in abc.move_lines:
                for quant in move_line:
                    data = quant
                    # data = quant.inventory_value

        return True
