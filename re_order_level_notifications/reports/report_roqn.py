import logging
import re
from datetime import datetime
from odoo import api, fields, models, _

_logger = logging.getLogger(__name__)


class report_roqn(models.AbstractModel):
    _name = 'report.re_order_level_notifications.report_roqn'
    _description = 'Reorder Quantity Notification Report'

    @api.model
    def _get_report_values(self, docids, data=None):
        docs = self.env['product.product'].browse(docids[0])

        return {
            'doc_ids': docids,
            'doc_model': 'product.product',
            'docs': docs,
            'data': data,
            'get_reorder_qty': self.get_reorder_qty,
        }

    def get_reorder_qty(self, obj):

        product_obj = self.env['product.product']
        reo_ps_lst = product_obj.search([('reorder_qty_level', '>', 0)], order="categ_id desc")

        reorder_qty_level_ids = list()

        for rel_product in reo_ps_lst:

            if rel_product.reorder_qty_level >= rel_product.synch_qty:
                reorder_qty_level_ids.append(rel_product.id)

        all_products = product_obj.browse(reorder_qty_level_ids)

        return all_products if all_products else '-'
