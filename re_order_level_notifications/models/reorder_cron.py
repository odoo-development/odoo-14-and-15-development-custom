import logging
import re
from datetime import datetime
from odoo import api, fields, models, _

_logger = logging.getLogger(__name__)


class ProductProduct(models.Model):
    _inherit = 'product.product'

    @api.model
    def send_reorder_qty_level_mail(self):

        if self.env.context is None:
            context = {
                'lang': 'en_US',
                'params': {'action': 404},
                'tz': 'Asia/Dhaka',
                'uid': self.env.uid
            }

        product_ids = self.env['product.product'].search([('reorder_qty_level', '>', 0)])

        for product_id in product_ids:
            p_id = product_id.id

        email = "rockibul.alam@sindabad.com"

        email_template_obj = self.env['mail.template']
        template_ids = email_template_obj.sudo().search([('name', '=', 'Reorder Level - Send by Email')])
        template = template_ids.with_context({'email_to': email}).send_mail(p_id, force_send=True)

        return template
