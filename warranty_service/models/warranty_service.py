
from odoo import api, fields, models, _


class PackingList(models.Model):
    _inherit = "packing.list"

    def print_packing_warranty_report(self):

        # context = dict(context or {}, active_ids=ids)
        return self.env["report"].get_action('warranty_service.report_packings_warranty_layout')

    def print_packing_mushak_report(self):

        # context = dict(context or {}, active_ids=ids)
        return self.env["report"].get_action('warranty_service.report_packings_mushak_layout')


class ProductProduct(models.Model):
    _inherit = 'product.product'

    warranty_applicable = fields.Boolean('Warranty Applicable')
    warranty_duration = fields.Integer('Warranty (Month)')
    warranty_text = fields.Text('Warranty Rule')

    # ## Block code for V-14
    # _columns = {
    #     'warranty_applicable': fields.boolean('Warranty Applicable'),
    #     'warranty_duration': fields.integer('Warranty (Month)'),
    #     'warranty_text': fields.text('Warranty Rule'),
    #
    # }

class PackingListLine(models.Model):
    _inherit = 'packing.list.line'

    warranty_serials = fields.Boolean('Warranty Serials')

    # ## Block code for V-14
    # _columns = {
    #     'warranty_serials': fields.boolean('Warranty Serials')
    # }

# class account_invoice_line(osv.osv):  ai code age thake close
#     _inherit = 'account.invoice.line'
#
#
#     _columns = {
#         'product_serials': fields.char('Product Serials')
#     }


class WarrantyServiceSerialAdd(models.Model):
    _name = 'warranty.service.serial.add'
    _description = "Warranty Service Serial Add"

    warranty_serials = fields.Boolean('Warranty Serials')
    invoice_id = fields.Many2one('account.move', 'Invoice Number')
    warranty_service_serial_add_line = fields.One2many('warranty.service.serial.add.line',
                                                        'warranty_service_serial_add_id', 'Serial Line',
                                                        required=True)

    #### Block code V-14
    # _columns = {
    #     'warranty_serials': fields.boolean('Warranty Serials'),
    #     'invoice_id': fields.many2one('account.invoice', 'Invoice Number'),
    #     'warranty_service_serial_add_line': fields.one2many('warranty.service.serial.add.line', 'warranty_service_serial_add_id', 'Serial Line',
    #                                        required=True),
    #
    # }

    def default_get(self, fields):
        # product_pricelist = self.env['product.pricelist']
        # if context is None:
        #     context = {}
        res = super(WarrantyServiceSerialAdd, self).default_get(fields)

        if self.env.context.get('active_model') == 'packing.list.line':
            packing_line_obj = self.env["packing.list.line"]
            packing_line = packing_line_obj.browse(self._context['active_id'])
            invoice = packing_line.invoice_id
        else:
            inv_obj = self.env["account.move"]
            inv_data = inv_obj.browse(self._context['active_id'])
            invoice = inv_data

        items = list()

        for line in invoice.invoice_line_ids:
            if line.quantity>0.00 and line.product_id.warranty_applicable == True:
                item = {
                    'product_id': line.product_id.id,
                    'quantity': line.quantity,
                    'invoice_line_id': line.id,
                    'serial': line.x_product_serial
                }

                if line.product_id.default_code and line.product_id.default_code != '':
                    items.append(item)
        res['invoice_id'] = invoice.id

        res.update(warranty_service_serial_add_line=items)

        return res

    def serial_add(self):

        warranty_service = self.browse()

        ## update invoice Line

        for wt_items in warranty_service.warranty_service_serial_add_line:

            try:
                if wt_items.serial is not False:
                    self.env.cr.execute("UPDATE account_move_line set x_product_serial=%s where id = %s", (str(wt_items.serial), wt_items.invoice_line_id.id))
                    self.env.cr.commit()
            except:
                pass
        return True


class WarrantyServiceSerialAddLine(models.Model):
    _name = 'warranty.service.serial.add.line'
    _description = "Warranty Service Serial Add Line"

    warranty_service_serial_add_id = fields.Many2one('warranty.service.serial.add', 'Warranty Service Serial Add',
                                                      required=True,
                                                      ondelete='cascade', index=True, readonly=True)
    product_id = fields.Many2one('product.product', 'Product', required=True)
    invoice_line_id = fields.Many2one('account.move.line', 'Invoice Line')
    quantity = fields.Float('Quantity')
    serial = fields.Text('Serial')

    # ## Block code for V-14
    # _columns = {
    #     'warranty_service_serial_add_id': fields.many2one('warranty.service.serial.add', 'Warranty Service Serial Add', required=True,
    #                                       ondelete='cascade', index=True, readonly=True),
    #     'product_id': fields.many2one('product.product', 'Product', required=True),
    #     'invoice_line_id': fields.many2one('account.invoice.line', 'Invoice Line'),
    #     'quantity': fields.float('Quantity'),
    #     'serial': fields.text('Serial'),
    #
    # }













