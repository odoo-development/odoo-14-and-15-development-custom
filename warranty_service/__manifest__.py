{
    'name': 'Warranty Service Print and service',
    'version': "14.0.1.0.0",
    'category': 'Sales',
    'description': """
        """,
    'author': 'Odoo Bangladesh',
    'depends': ['product', 'packings', 'order_delivery_dispatch'],
    'data': [
        'security/ir.model.access.csv',
        # 'views/report_packings_warranty_layout.xml',
        # 'views/report_packings_mushak_layout.xml',
        # 'views/report_invoice_warranty_layout.xml',
        'views/warranty_service_serial_add.xml',
        'views/warranty_service_packing.xml',
        # 'warranty_serial_report.xml',
        # 'invoice_warranty_menu.xml',
        'views/account_invoice.xml',
        # 'packing_warranty_button.xml',
        # 'packing_mushak_menu.xml',
        # 'packing_mushak_button.xml',
    ],

    'installable': True,
    'auto_install': False,
}

