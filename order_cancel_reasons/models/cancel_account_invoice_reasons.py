from odoo import models, fields, api, _


class AccountInvoice(models.Model):
    _inherit = "account.move"

    cancel_account_invoice_date = fields.Datetime('Cancel Account Invoice Date')
    cancel_account_invoice_by = fields.Many2one('res.users', 'Cancel Account Invoice By')
    cancel_account_invoice_reason = fields.Text('Cancel Account Invoice Reason')

    # ### Block code for V-14
    # _columns = {
    #     'cancel_account_invoice_date': fields.datetime('Cancel Account Invoice Date'),
    #     'cancel_account_invoice_by': fields.many2one('res.users', 'Cancel Account Invoice By'),
    #     'cancel_account_invoice_reason': fields.text('Cancel Account Invoice Reason'),
    # }
    def cancel_account_invoice(self, ids):
        super(AccountInvoice, self).button_cancel()
        return True



### ai model V-14 ta nai

# class AccountInvoiceCancel(models.Model):
#     _inherit = "account.invoice.cancel"
#
#     def cancel_account_invoice(self, cr, uid, ids, context=None):
#
#         super(account_invoice_cancel, self).invoice_cancel(cr, uid, ids, context)
#
#         return True


class CancelAccountInvoiceReason(models.Model):
    _name = "cancel.account.invoice.reason"
    _description = "Cancel Account Invoice Reason"

    cancel_account_invoice_date = fields.Datetime('Cancel Account Invoice Date')
    cancel_account_invoice_by = fields.Many2one('res.users', 'Cancel Account Invoice By')
    cancel_account_invoice_reason = fields.Text('Cancel Account Invoice Reason', required=True)

    # ## Block code for V-14
    # _columns = {
    #     'cancel_account_invoice_date': fields.datetime('Cancel Account Invoice Date'),
    #     'cancel_account_invoice_by': fields.many2one('res.users', 'Cancel Account Invoice By'),
    #     'cancel_account_invoice_reason': fields.text('Cancel Account Invoice Reason', required=True),
    # }

    def cancel_invoice_reason(self):
        ids = self.env.context['active_ids']
        cancel_account_invoice_reason = str(self.env.context['cancel_account_invoice_reason'])
        # cancel_account_invoice_by = self.env.uid
        # cancel_account_invoice_date = str(fields.datetime.now())

        invoice_obj = self.env['account.move'].browse(ids)
        invoice_obj.write({
            'cancel_account_invoice_reason': cancel_account_invoice_reason,
            'cancel_account_invoice_by': self.env.uid,
            'cancel_account_invoice_date': fields.datetime.now(),
        })
        invoice_obj.cancel_account_invoice(ids)

        # for s_id in ids:
        #     cancel_invoice_query = "UPDATE account_invoice SET cancel_account_invoice_reason='{0}', cancel_account_invoice_by={1}, cancel_account_invoice_date='{2}' WHERE id={3}".format(
        #         cancel_account_invoice_reason, cancel_account_invoice_by, cancel_account_invoice_date, s_id)
        #     self.env.cr.execute(cancel_invoice_query)
        #     self.env.cr.commit()
        #
        #     invoice_obj.cancel_account_invoice([s_id], )
        #
        # return True
