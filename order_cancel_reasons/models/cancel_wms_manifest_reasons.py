from odoo import models, fields


class WmsManifestProcess(models.Model):
    _inherit = 'wms.manifest.process'

    cancel_manifest_date = fields.Datetime('Cancel Manifest Date')
    cancel_manifest_by = fields.Many2one('res.users', 'Cancel Manifest By')
    cancel_manifest_reason = fields.Text('Cancel Manifest Reason')

    ### Block code for v-14
    # _columns = {
    #     'cancel_manifest_date': fields.datetime('Cancel Manifest Date'),
    #     'cancel_manifest_by': fields.many2one('res.users', 'Cancel Manifest By'),
    #     'cancel_manifest_reason': fields.text('Cancel Manifest Reason'),
    # }

    def cancel_wms_manifest_with_reason(self, ids):
        super(WmsManifestProcess, self).cancel_wms_manifest()

        return True


class CancelWmsManifestReasons(models.Model):
    _name = "cancel.wms.manifest.reasons"
    _description = "Cancel WMS Manifest Reasons"

    cancel_manifest_date = fields.Datetime('Cancel Manifest Date')
    cancel_manifest_by = fields.Many2one('res.users', 'Cancel Manifest By')
    cancel_manifest_reason = fields.Text('Cancel Manifest Reason', required=True)

    ### Block code for v-14
    # _columns = {
    #     'cancel_manifest_date': fields.datetime('Cancel Manifest Date'),
    #     'cancel_manifest_by': fields.many2one('res.users', 'Cancel Manifest By'),
    #     'cancel_manifest_reason': fields.text('Cancel Manifest Reason', required=True),
    # }

    def cancel_wms_manifest_reason(self):
        ids = self.env.context['active_ids']
        cancel_manifest_reason = str(self.env.context['cancel_manifest_reason'])
        # cancel_manifest_by = self.env.uid
        # cancel_manifest_date = str(fields.datetime.now())

        manifest_obj = self.env['wms.manifest.process'].browse(ids)
        manifest_obj.write({
            'cancel_manifest_reason': cancel_manifest_reason,
            'cancel_manifest_by': self.env.uid,
            'cancel_manifest_date': fields.datetime.now(),
        })
        manifest_obj.cancel_wms_manifest_with_reason(ids)

        # for s_id in ids:
        #     cancel_manifest_query = "UPDATE wms_manifest_process SET cancel_manifest_reason='{0}', cancel_manifest_by={1}, cancel_manifest_date='{2}' WHERE id={3}".format(
        #         cancel_manifest_reason, cancel_manifest_by, cancel_manifest_date, s_id)
        #     self.env.cr.execute(cancel_manifest_query)
        #     self.env.cr.commit()
        #
        #     manifest_obj.cancel_wms_manifest_with_reason([s_id])

        # return True
