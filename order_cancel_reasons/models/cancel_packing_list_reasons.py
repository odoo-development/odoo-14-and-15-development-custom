from odoo import models, fields


class PackingList(models.Model):
    _inherit = 'packing.list'

    cancel_packing_date = fields.Datetime('Cancel Packing Date')
    cancel_packing_by = fields.Many2one('res.users', 'Cancel Packing By')
    cancel_packing_reason = fields.Selection([
        ("product_not_found", "Product not found"),
        ("sku_mismatch", "SKU mismatch"),
        ("not_all_qty_available", "Not all quantity available"),
        ("sku_damaged", "SKU Damaged")

    ], 'Cancel Packing Reason', copy=False, help="Reasons", index=True)

    # ## Block code for v-14
    # _columns = {
    #     'cancel_packing_date': fields.datetime('Cancel Packing Date'),
    #     'cancel_packing_by': fields.many2one('res.users', 'Cancel Packing By'),
    #     'cancel_packing_reason': fields.selection([
    #         ("product_not_found", "Product not found"),
    #         ("sku_mismatch", "SKU mismatch"),
    #         ("not_all_qty_available", "Not all quantity available"),
    #         ("sku_damaged", "SKU Damaged")
    #
    #
    #     ], 'Cancel Packing Reason', copy=False, help="Reasons", index=True),
    #
    # }

    def cancel_packing_list(self, ids):
        super(PackingList, self).make_cancel()

        return True


class CancelPackingListReason(models.Model):
    _name = "cancel.packing.list.reason"
    _description = "Cancel Packing Reason"

    cancel_packing_date = fields.Datetime('Cancel Packing Date')
    cancel_packing_by = fields.Many2one('res.users', 'Cancel Packing By')
    cancel_packing_reason = fields.Selection([
        ("product_not_found", "Product not found"),
        ("sku_mismatch", "SKU mismatch"),
        ("not_all_qty_available", "Not all quantity available"),
        ("sku_damaged", "SKU Damaged")

    ], 'Cancel Packing Reason', copy=False, help="Reasons", index=True)

    ### Block code for v-14
    # _columns = {
    #     'cancel_packing_date': fields.datetime('Cancel Packing Date'),
    #     'cancel_packing_by': fields.many2one('res.users', 'Cancel Packing By'),
    #     'cancel_packing_reason': fields.selection([
    #         ("product_not_found", "Product not found"),
    #         ("sku_mismatch", "SKU mismatch"),
    #         ("not_all_qty_available", "Not all quantity available"),
    #         ("sku_damaged", "SKU Damaged")
    #
    #
    #     ], 'Cancel Packing Reason', copy=False, help="Reasons", index=True),
    #
    # }

    def cancel_packing_list_reason(self):
        ids = self.env.context['active_ids']
        cancel_packing_reason = str(self.env.context['cancel_packing_reason'])
        # cancel_packing_by = self.env.uid
        # cancel_packing_date = str(fields.datetime.now())

        packing_obj = self.env['packing.list'].browse(ids)
        packing_obj.write({
            'cancel_packing_reason': cancel_packing_reason,
            'cancel_packing_by': self.env.uid,
            'cancel_packing_date': fields.datetime.now(),
        })
        packing_obj.cancel_packing_list(ids)

        # for s_id in ids:
        #     cancel_co_query = "UPDATE packing_list SET cancel_packing_reason='{0}', cancel_packing_by={1}, cancel_packing_date='{2}' WHERE id={3}".format(
        #         cancel_packing_reason, cancel_packing_by, cancel_packing_date, s_id)
        #     self.env.cr.execute(cancel_co_query)
        #     self.env.cr.commit()
        #
        #     packing_obj.cancel_packing_list([s_id])
        #
        # return True
