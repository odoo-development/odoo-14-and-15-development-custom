from odoo import models, fields


class WmsManifestOutbound(models.Model):
    _inherit = 'wms.manifest.outbound'

    cancel_manifest_outbound_date = fields.Datetime('Cancel Manifest Outbound Date')
    cancel_manifest_outbound_by = fields.Many2one('res.users', 'Cancel Manifest Outbound By')
    cancel_manifest_outbound_reason = fields.Text('Cancel Manifest Outbound Reason')

    ### Block code for V-14
    # _columns = {
    #     'cancel_manifest_outbound_date': fields.datetime('Cancel Manifest Outbound Date'),
    #     'cancel_manifest_outbound_by': fields.many2one('res.users', 'Cancel Manifest Outbound By'),
    #     'cancel_manifest_outbound_reason': fields.text('Cancel Manifest Outbound Reason'),
    # }

    def cancel_wms_manifest_outbound_with_reason(self, ids):
        # super(wms_manifest_process, self).cancel_wms_manifest(cr, uid, ids, context)
        # if confirm is FALSE then state = cancel

        outbound_obj = self.browse(ids)
        if not outbound_obj.confirm:
            outbound_obj.write({'state': 'cancel'})

        #     state_manifest_outbound_query = "UPDATE wms_manifest_outbound SET state='cancel' WHERE id={0}".format(
        #         outbound_obj.id)
        #     self.env.cr.execute(state_manifest_outbound_query)
        #     self.env.cr.commit()
        #
        # return True


class CancelWmsManifestOutboundReasons(models.Model):
    _name = "cancel.wms.manifest.outbound.reasons"
    _description = "Cancel WMS Manifest Outbound Reasons"

    cancel_manifest_outbound_date = fields.Datetime('Cancel Manifest Outbound Date')
    cancel_manifest_outbound_by = fields.Many2one('res.users', 'Cancel Manifest Outbound By')
    cancel_manifest_outbound_reason = fields.Text('Cancel Manifest Outbound Reason', required=True)

    # ### Block code for V-14
    # _columns = {
    #     'cancel_manifest_outbound_date': fields.datetime('Cancel Manifest Outbound Date'),
    #     'cancel_manifest_outbound_by': fields.many2one('res.users', 'Cancel Manifest Outbound By'),
    #     'cancel_manifest_outbound_reason': fields.text('Cancel Manifest Outbound Reason', required=True),
    # }

    def cancel_wms_manifest_outbound_reason(self):
        ids = self.env.context['active_ids']
        cancel_manifest_outbound_reason = str(self.env.context['cancel_manifest_outbound_reason'])
        # cancel_manifest_outbound_by = self.env.uid
        # cancel_manifest_outbound_date = str(fields.datetime.now())

        manifest_outbound_obj = self.env['wms.manifest.outbound'].browse(ids)
        manifest_outbound_obj.write({
            'cancel_manifest_outbound_reason': cancel_manifest_outbound_reason,
            'cancel_manifest_outbound_by': self.env.uid,
            'cancel_manifest_outbound_date': fields.datetime.now(),
        })
        manifest_outbound_obj.cancel_wms_manifest_outbound_with_reason(ids)

        # for s_id in ids:
            # cancel_manifest_outbound_query = "UPDATE wms_manifest_outbound SET cancel_manifest_outbound_reason='{0}', cancel_manifest_outbound_by={1}, cancel_manifest_outbound_date='{2}' WHERE id={3}".format(
            #     cancel_manifest_outbound_reason, cancel_manifest_outbound_by, cancel_manifest_outbound_date, s_id)
            # self.env.cr.execute(cancel_manifest_outbound_query)
            # self.env.cr.commit()



        # return True
