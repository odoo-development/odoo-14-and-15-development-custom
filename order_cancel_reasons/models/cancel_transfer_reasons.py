from odoo import models, fields


class StockPicking(models.Model):
    _inherit = "stock.picking"

    cancel_transfer_date = fields.Datetime('Cancel Transfer Date')
    cancel_transfer_by = fields.Many2one('res.users', 'Cancel Transfer By')
    cancel_transfer_reason = fields.Text('Cancel Transfer Reason')

    ### Block code for V-14
    # _columns = {
    #     'cancel_transfer_date': fields.datetime('Cancel Transfer Date'),
    #     'cancel_transfer_by': fields.many2one('res.users', 'Cancel Transfer By'),
    #     'cancel_transfer_reason': fields.text('Cancel Transfer Reason'),
    # }

    def cancel_stock_picking_transfer(self, ids):
        super(StockPicking, self).action_cancel()

        return True


class StockPickingCancelTransfer(models.Model):
    _name = "stock.picking.cancel.transfer"
    _description = "Stock Picking Cancel Transfer"

    cancel_transfer_date = fields.Datetime('Cancel Transfer Date')
    cancel_transfer_by = fields.Many2one('res.users', 'Cancel Transfer By')
    cancel_transfer_reason = fields.Text('Cancel Transfer Reason', required=True)

    ### Block code for V-14
    # _columns = {
    #     'cancel_transfer_date': fields.datetime('Cancel Transfer Date'),
    #     'cancel_transfer_by': fields.many2one('res.users', 'Cancel Transfer By'),
    #     'cancel_transfer_reason': fields.text('Cancel Transfer Reason', required=True),
    # }

    def stock_picking_cancel_transfer(self):
        ids = self.env.context['active_ids']
        cancel_transfer_reason = str(self.env.context['cancel_transfer_reason'])
        # cancel_transfer_by = self.env.uid
        # cancel_transfer_date = str(fields.datetime.now())

        sp_obj = self.env['stock.picking'].browse(ids)
        sp_obj.write({
            'cancel_transfer_reason': cancel_transfer_reason,
            'cancel_transfer_by': self.env.uid,
            'cancel_transfer_date': fields.datetime.now(),
        })
        sp_obj.cancel_stock_picking_transfer(ids)

        # for s_id in ids:
            # cancel_co_query = "UPDATE stock_picking SET cancel_transfer_reason='{0}', cancel_transfer_by={1}, cancel_transfer_date='{2}' WHERE id={3}".format(
            #     cancel_transfer_reason, cancel_transfer_by, cancel_transfer_date, s_id)
            # self.env.cr.execute(cancel_co_query)
            # self.env.cr.commit()



        # return True
