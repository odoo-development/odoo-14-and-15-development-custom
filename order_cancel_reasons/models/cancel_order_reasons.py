import datetime
from datetime import datetime

from odoo import models, fields


class SaleOrder(models.Model):
    _inherit = "sale.order"

    # co: Confirmed Order
    cancel_co_date = fields.Datetime('Confirmed Order Cancel Date')
    cancel_co_by = fields.Many2one('res.users', 'Confirmed Order Cancel By')
    cancel_co_reason = fields.Selection([
        ("product_unavailability", "Product unavailability"),
        ("delay_delivery", "Delay delivery"),
        ("pricing_issue", "Pricing issue"),
        ("wrong_order", "Wrong order"),
        ("cash_problem", "Cash problem"),
        ("damage_product", "Damage product"),
        ("double_ordered", "Double ordered"),
        ("sample_not_approved", "Sample not approved"),
        ("wrong_sku", "Wrong SKU"),
        ("not_interested", "Not interested"),
        ("wrong_product", "Wrong product"),
        ("quality_issue", "Quality issue"),
        ("test_order", "Test Order"),
        ("payment_not_done", "Payment not done"),
        ("apnd", "Advance payment not done"),
        ("invalid_information", "Invalid Information"),
        ("customer_unreachable", "Customer unreachable"),
        ("same_day_delivery", "Same day delivery"),
        ("fake_order", "Fake Order"),
        ("sourcing_delay", "Sourcing Delay"),
        ("reorder", "Reorder"),
        ("discount_issue", "Discount Issue"),
        ("product_not_available_customer_will_take_alternative_product",
         "Product not available, Customer will take alternative product"),
    ], 'Confirmed Order Cancel Reason', copy=False, help="Reasons", index=True)

    # ## Block code for V-14
    # # co: Confirmed Order
    # _columns = {
    #     'cancel_co_date': fields.datetime('Confirmed Order Cancel Date'),
    #     'cancel_co_by': fields.many2one('res.users', 'Confirmed Order Cancel By'),
    #     'cancel_co_reason': fields.selection([
    #
    #         ("product_unavailability", "Product unavailability"),
    #         ("delay_delivery", "Delay delivery"),
    #         ("pricing_issue", "Pricing issue"),
    #         ("wrong_order", "Wrong order"),
    #         ("cash_problem", "Cash problem"),
    #         ("damage_product", "Damage product"),
    #         ("double_ordered", "Double ordered"),
    #         ("sample_not_approved", "Sample not approved"),
    #         ("wrong_sku", "Wrong SKU"),
    #         ("not_interested", "Not interested"),
    #         ("wrong_product", "Wrong product"),
    #         ("quality_issue", "Quality issue"),
    #         ("test_order", "Test Order"),
    #         ("payment_not_done", "Payment not done"),
    #         ("apnd", "Advance payment not done"),
    #         ("invalid_information", "Invalid Information"),
    #         ("customer_unreachable", "Customer unreachable"),
    #         ("same_day_delivery", "Same day delivery"),
    #         ("fake_order", "Fake Order"),
    #         ("sourcing_delay","Sourcing Delay"),
    #         ("reorder","Reorder"),
    #         ("discount_issue","Discount Issue"),
    #         ("product_not_available_customer_will_take_alternative_product", "Product not available, Customer will take alternative product"),
    #
    #     ], 'Confirmed Order Cancel Reason', copy=False, help="Reasons", index=True)
    #
    # }

    def action_cancel_confirmed_order(self, ids):
        super(SaleOrder, self).action_cancel()
        # so = self.browse(ids)
        #
        # parent = so.partner_id
        # for i in range(5):
        #     if len(parent.parent_id) == 1:
        #         parent = parent.parent_id
        #     else:
        #         parent = parent
        #
        #         break
        #
        # company_phone = str(parent.mobile)
        # phone_number = str(so.partner_id.mobile) if so.partner_id.mobile else company_phone
        # so_cancel_date = fields.datetime.now()
        #
        # if not phone_number:
        #     phone_number = str(so.partner_id.phone)
        #
        # email = str(parent.email)
        # cancel_reason = ""
        # if so.cancel_co_reason:
        #     cancel_reason += str(so.cancel_co_reason).replace("_", " ")
        # ### cancel_quotation_reason ai field missing in so
        # # if so.cancel_quotation_reason:
        # #     cancel_reason += str(so.cancel_quotation_reason).replace("_", " ")
        #
        # sms_text = ''
        # if cancel_reason == 'apnd':
        #     cancel_reason = "Advance payment not done"
        #
        # if phone_number:
        #     # sms_text = "Hello str(parent.name), we are sorry to inform that your order #str(so.client_order_ref) has been canceled.ui due to cancel_reason"
        #
        #     sms_text = "Hello {0}, we are sorry to inform that your order #{1} has been canceled.ui due to '{2}'".format(
        #         str(parent.name), str(so.client_order_ref), cancel_reason)
        #
        #     self.env["send.sms.on.demand"].send_sms_on_demand(sms_text, phone_number, str(parent.name))
        #
        # # send email
        # if email:
        #     e_log_obj = self.env['send.email.on.demand.log']
        #
        #     email_data = {
        #         'mail_text': sms_text,
        #         'model': "sale.order",
        #         'so_id': so.id,
        #         'email': email
        #     }
        #
        #     email_log_id = e_log_obj.create(email_data)
        #
        #     # send email
        #     email_template_obj = self.env['mail.template']
        #     template_ids = email_template_obj.search([('name', '=', 'Order Canceled')], limit=1)
        #
        #     # email_template_obj.send_mail(template_ids[0], email_log_id, force_send=True)
        #
        # # try:
        # #     so = self.browse()
        # #
        # #     parent = so.partner_id
        # #
        # #     for i in range(5):
        # #         if len(parent.parent_id) == 1:
        # #             parent = parent.parent_id
        # #         else:
        # #             parent = parent
        # #
        # #             break
        # #
        # #     company_phone = str(parent.mobile)
        # #     phone_number = str(so.partner_id.mobile) if so.partner_id.mobile else company_phone
        # #     so_cancel_date = datetime.datetime.now().strftime('%A, %d-%m-%Y')
        # #
        # #     if not phone_number:
        # #         phone_number = str(so.partner_id.phone)
        # #
        # #     email = str(parent.email)
        # #     cancel_reason = ""
        # #     if so.cancel_co_reason:
        # #         cancel_reason += str(so.cancel_co_reason).replace("_", " ")
        # #     if so.cancel_quotation_reason:
        # #         cancel_reason += str(so.cancel_quotation_reason).replace("_", " ")
        # #
        # #     sms_text = ''
        # #     if cancel_reason == 'apnd':
        # #         cancel_reason = "Advance payment not done"
        # #
        # #     if phone_number:
        # #         sms_text = "Hello {0}, we are sorry to inform that your order #{1} has been canceled.ui due to '{2}'".format(
        # #             str(parent.name), str(so.client_order_ref), cancel_reason)
        # #
        # #         self.env["send.sms.on.demand"].send_sms_on_demand(sms_text, phone_number, str(parent.name))
        # #
        # #     # send email
        # #     if email:
        # #         e_log_obj = self.env['send.email.on.demand.log']
        # #
        # #         email_data = {
        # #             'mail_text': sms_text,
        # #             'model': "sale.order",
        # #             'so_id': self.ids[0],
        # #             'email': email
        # #         }
        # #
        # #         email_log_id = e_log_obj.create(email_data)
        # #         # tmp_data = e_log_obj.browse(cr, uid, email_log_id, context=context)
        # #
        # #         # send email
        # #         email_template_obj = self.env['email.template']
        # #         template_ids = email_template_obj.search([('name', '=', 'Order Canceled')])
        # #
        # #         email_template_obj.send_mail(template_ids[0], force_send=True)
        # #
        # # except:
        # #     pass
        # #
        return True


class SoCancelConfirmedOrder(models.Model):
    _name = "so.cancel.confirmed.order"
    _description = "SO cancel confirmed order"

    cancel_co_date = fields.Datetime('Confirmed Order Cancel Date')
    cancel_co_by = fields.Many2one('res.users', 'Confirmed Order Cancel By')
    cancel_co_reason = fields.Selection([

        ("product_unavailability", "Product unavailability"),
        ("delay_delivery", "Delay delivery"),
        ("pricing_issue", "Pricing issue"),
        ("wrong_order", "Wrong order"),
        ("cash_problem", "Cash problem"),
        ("damage_product", "Damage product"),
        ("double_ordered", "Double ordered"),
        ("sample_not_approved", "Sample not approved"),
        ("wrong_sku", "Wrong SKU"),
        ("not_interested", "Not interested"),
        ("wrong_product", "Wrong product"),
        ("quality_issue", "Quality issue"),
        ("test_order", "Test Order"),
        ("payment_not_done", "Payment not done"),
        ("apnd", "Advance payment not done"),
        ("invalid_information", "Invalid Information"),
        ("customer_unreachable", "Customer unreachable"),
        ("same_day_delivery", "Same day delivery"),
        ("fake_order", "Fake Order"),
        ("sourcing_delay", "Sourcing Delay"),
        ("reorder", "Reorder"),
        ("discount_issue", "Discount Issue"),
        ("product_not_available_customer_will_take_alternative_product",
         "Product not available, Customer will take alternative product"),

    ], 'Confirmed Order Cancel Reason', copy=False, help="Reasons", index=True)

    # ## Block code for V-14
    # # co: Confirmed Order
    # _columns = {
    #     'cancel_co_date': fields.datetime('Confirmed Order Cancel Date'),
    #     'cancel_co_by': fields.many2one('res.users', 'Confirmed Order Cancel By'),
    #     'cancel_co_reason': fields.selection([
    #
    #         ("product_unavailability", "Product unavailability"),
    #         ("delay_delivery", "Delay delivery"),
    #         ("pricing_issue", "Pricing issue"),
    #         ("wrong_order", "Wrong order"),
    #         ("cash_problem", "Cash problem"),
    #         ("damage_product", "Damage product"),
    #         ("double_ordered", "Double ordered"),
    #         ("sample_not_approved", "Sample not approved"),
    #         ("wrong_sku", "Wrong SKU"),
    #         ("not_interested", "Not interested"),
    #         ("wrong_product", "Wrong product"),
    #         ("quality_issue", "Quality issue"),
    #         ("test_order", "Test Order"),
    #         ("payment_not_done", "Payment not done"),
    #         ("apnd", "Advance payment not done"),
    #         ("invalid_information", "Invalid Information"),
    #         ("customer_unreachable", "Customer unreachable"),
    #         ("same_day_delivery", "Same day delivery"),
    #         ("fake_order", "Fake Order"),
    #         ("sourcing_delay","Sourcing Delay"),
    #         ("reorder","Reorder"),
    #         ("discount_issue","Discount Issue"),
    #         ("product_not_available_customer_will_take_alternative_product", "Product not available, Customer will take alternative product"),
    #
    #     ], 'Confirmed Order Cancel Reason', copy=False, help="Reasons", index=True)
    #
    # }

    def so_cancel_confirmed_order(self):
        ids = self.env.context['active_ids']
        cancel_co_reason = str(self.env.context['cancel_co_reason'])
        # cancel_co_by = self.env.uid
        # cancel_co_date = str(fields.datetime.now())

        so_obj = self.env['sale.order'].browse(ids)
        so_obj.write({
            'cancel_co_reason': cancel_co_reason,
            'cancel_co_by': self.env.uid,
            'cancel_co_date': fields.datetime.now(),
        })
        # so_obj.action_cancel_confirmed_order(ids)

        so_obj.action_cancel()

        # for s_id in ids:
        #     cancel_co_query = "UPDATE sale_order SET cancel_co_reason='{0}', cancel_co_by={1}, cancel_co_date='{2}' WHERE id={3}".format(
        #         cancel_co_reason, cancel_co_by, cancel_co_date, s_id)
        #     self.env.cr.execute(cancel_co_query)
        #     self.env.cr.commit()
        #
        #     so_obj.action_cancel_confirmed_order([s_id])
        #
        # return True
