from odoo import models, fields
from odoo.tools.translate import _


class PickingList(models.Model):
    _inherit = 'picking.list'

    cancel_picking_date = fields.Datetime('Cancel Picking Date')
    cancel_picking_by = fields.Many2one('res.users', 'Cancel Picking By')
    cancel_picking_reason = fields.Selection([
        ("product_not_found", "Product not found"),
        ("sku_mismatch", "SKU mismatch"),
        ("not_all_qty_available", "Not all quantity available"),
        ("sku_damaged", "SKU Damaged"),
        ("wrongly_pick_for_other_warehouse", "Wrongly pick for other warehouse")

    ], 'Cancel Picking Reason', copy=False, help="Reasons", index=True)

    ### Block Code for V-14
    # _columns = {
    #     'cancel_picking_date': fields.datetime('Cancel Picking Date'),
    #     'cancel_picking_by': fields.many2one('res.users', 'Cancel Picking By'),
    #     'cancel_picking_reason': fields.selection([
    #         ("product_not_found","Product not found"),
    #         ("sku_mismatch","SKU mismatch"),
    #         ("not_all_qty_available","Not all quantity available"),
    #         ("sku_damaged","SKU Damaged"),
    #         ("wrongly_pick_for_other_warehouse","Wrongly pick for other warehouse")
    #
    #         ], 'Cancel Picking Reason', copy=False, help="Reasons", index=True),
    #
    # }

    def cancel_picking_list(self, ids):
        super(PickingList, self).make_cancel()

        return True


class CancelPickingListReason(models.Model):
    _name = "cancel.picking.list.reason"
    _description = "Cancel Picking Reason"

    cancel_picking_date = fields.Datetime('Cancel Picking Date')
    cancel_picking_by = fields.Many2one('res.users', 'Cancel Picking By')
    cancel_picking_reason = fields.Selection([
        ("product_not_found", "Product not found"),
        ("sku_mismatch", "SKU mismatch"),
        ("not_all_qty_available", "Not all quantity available"),
        ("sku_damaged", "SKU Damaged"),
        ("wrongly_pick_for_other_warehouse", "Wrongly pick for other warehouse")

    ], 'Cancel Picking Reason', copy=False, help="Reasons", index=True)

    ### Block code for V-14
    # _columns = {
    #     'cancel_picking_date': fields.datetime('Cancel Picking Date'),
    #     'cancel_picking_by': fields.many2one('res.users', 'Cancel Picking By'),
    #     'cancel_picking_reason': fields.selection([
    #         ("product_not_found","Product not found"),
    #         ("sku_mismatch","SKU mismatch"),
    #         ("not_all_qty_available","Not all quantity available"),
    #         ("sku_damaged","SKU Damaged"),
    #         ("wrongly_pick_for_other_warehouse","Wrongly pick for other warehouse")
    #
    #         ], 'Cancel Picking Reason', copy=False, help="Reasons", index=True),
    #
    # }

    def cancel_picking_list_reason(self):
        ids = self.env.context['active_ids']
        cancel_picking_reason = str(self.env.context['cancel_picking_reason'])
        # cancel_picking_by = self.env.uid
        # cancel_picking_date = str(fields.datetime.now())

        picking_obj = self.env['picking.list'].browse(ids)
        picking_obj.write({
            'cancel_picking_reason': cancel_picking_reason,
            'cancel_picking_by': self.env.uid,
            'cancel_picking_date': fields.datetime.now(),
        })
        picking_obj.cancel_picking_list(ids)

        # for s_id in ids:
        #     cancel_co_query = "UPDATE picking_list SET cancel_picking_reason='{0}', cancel_picking_by={1}, cancel_picking_date='{2}' WHERE id={3}".format(
        #         cancel_picking_reason, cancel_picking_by, cancel_picking_date, s_id)
        #     self.env.cr.execute(cancel_co_query)
        #     self.env.cr.commit()
        #
        #     picking_obj.cancel_picking_list([s_id])
        #
        # return True
