{
    'name': 'Custom Fields in Base Models',
    'version': "14.0.1.0.0",
    'category': 'Tools',
    'author': 'Rocky',
    'description': '',
    'depends': [
        'partner_custom_fields',
        'product_custom_fields',
        'purchase_order_custom_fields',
        'sale_order_custom_fields',
        'stock_picking_custom_fields',
                ],
    'data': [],
    'installable': True,
}
