from odoo import models

def _unescape(text):
    ##
    # Replaces all encoded characters by urlib with plain utf8 string.
    #
    # @param text source text.
    # @return The plain text.
    from urllib.parse import unquote_plus
    try:
        text = unquote_plus(text)
        return text
    except Exception as e:
        return text


class ProductSummaryForSTNReportXlsx(models.AbstractModel):
    _name = 'report.stn_log.product_summary_for_stn_report_xls'
    _description = 'STN Summary Report'
    _inherit = 'report.report_xlsx.abstract'

    def generate_xlsx_report(self, workbook, data, objects):

        warehouse_id = data.get('warehouse_id')

        report_name = "STN Summary Report"
        sheet = workbook.add_worksheet(report_name)
        bold = workbook.add_format({'bold': True, 'align': 'center', 'bg_color': '#fffbed', 'border': True})
        title = workbook.add_format({'bold': True, 'align': 'center', 'bg_color': '#f2eee4', 'border': True})
        header_row_style = workbook.add_format({'bold': True, 'align': 'center', 'border': True})

        sheet.merge_range('A1:F1', 'Product Summary For STN Report', title)

        data_query = "SELECT * from stock_order_wise_product_summary where warehouse_id=%s order by remarks,category,product_name"
        self.env.cr.execute(data_query, (warehouse_id,))
        objects = self.env.cr.dictfetchall()

        row = 3
        col = 0

        sheet.write(row, col, 'Date: ', header_row_style)
        sheet.write(row, col + 2, 'Warehouse', header_row_style)
        if objects:
            sheet.write(row, col + 1, objects[0]['report_date'], header_row_style)

            sheet.write(row, col + 3, objects[0]['warehouse'], header_row_style)
        row += 2

        # Header Row
        sheet.set_column(0, 25, 25)
        # sheet.write(row, col, 'Report Date', header_row_style)
        sheet.write(row, col, 'Remarks', header_row_style)
        # sheet.write(row, col + 2, 'Warehouse', header_row_style)
        sheet.write(row, col + 1, 'Category', header_row_style)
        sheet.write(row, col + 2, 'SKU', header_row_style)
        sheet.write(row, col + 3, 'Product Name', header_row_style)
        sheet.write(row, col + 4, 'Propose STN Qty', header_row_style)
        sheet.write(row, col + 5, 'Reorder Level', header_row_style)
        sheet.write(row, col + 6, 'W/H Stock Qty', header_row_style)
        sheet.write(row, col + 7, 'Company Stock Qty', header_row_style)
        sheet.write(row, col + 8, 'Todays Sales QTY', header_row_style)
        sheet.write(row, col + 9, 'In Qty 0_to_7', header_row_style)
        sheet.write(row, col + 10, 'Out Qty 0_to_7', header_row_style)
        sheet.write(row, col + 11, 'Closing Qty 0_to_7', header_row_style)
        sheet.write(row, col + 12, 'In Qty 8_to_14', header_row_style)
        sheet.write(row, col + 13, 'Out Qty 8_to_14', header_row_style)
        sheet.write(row, col + 14, 'Closing Qty 8_to_14', header_row_style)
        sheet.write(row, col + 15, 'In Qty 15_to_21', header_row_style)
        sheet.write(row, col + 16, 'Out Qty 15_to_21', header_row_style)
        sheet.write(row, col + 17, 'Closing Qty 15_to_21', header_row_style)
        sheet.write(row, col + 18, 'In Qty 22_to_30', header_row_style)
        sheet.write(row, col + 19, 'Out Qty 22_to_30', header_row_style)
        sheet.write(row, col + 20, 'Closing Qty 22_to_30', header_row_style)
        sheet.write(row, col + 21, 'In Qty 31_to_60', header_row_style)
        sheet.write(row, col + 22, 'Out Qty 31_to_60', header_row_style)
        sheet.write(row, col + 23, 'Closing Qty 31_to_60', header_row_style)
        sheet.write(row, col + 24, 'In Qty Before 60', header_row_style)
        sheet.write(row, col + 25, 'Out Qty Before 60', header_row_style)
        sheet.write(row, col + 26, 'Closing Qty Before 60', header_row_style)

        row += 2

        for line in objects:

            # sheet.write(row, col, line.get('report_date'))
            sheet.write(row, col, line.get('remarks'))
            # sheet.write(row, col + 2, line.get('warehouse'))
            sheet.write(row,col + 1,line.get('category'))
            sheet.write(row, col + 2, line.get('product_code'))
            sheet.write(row, col + 3,  _unescape(line.get('product_name')))
            sheet.write(row, col + 4, line.get('propose_stn_qty'))
            sheet.write(row, col + 5, line.get('product_reorder_level'))
            sheet.write(row, col + 6, line.get('wh_stock_qty'))
            sheet.write(row, col + 7, line.get('company_stock_qty'))
            sheet.write(row, col + 8, line.get('sales_qty'))
            sheet.write(row, col + 9, line.get('in_qty_0_to_7'))
            sheet.write(row, col + 10, line.get('out_qty_0_to_7'))
            sheet.write(row, col + 11,line.get('closing_qty_0_to_7'))
            sheet.write(row, col + 12, line.get('in_qty_8_to_14'))
            sheet.write(row, col + 13, line.get('out_qty_8_to_14'))
            sheet.write(row, col + 14, line.get('closing_qty_8_to_14'))
            sheet.write(row, col + 15, line.get('in_qty_15_to_21'))
            sheet.write(row, col + 16, line.get('out_qty_15_to_21'))
            sheet.write(row, col + 17, line.get('closing_qty_15_to_21'))
            sheet.write(row, col + 18, line.get('in_qty_22_to_30'))
            sheet.write(row, col + 19, line.get('out_qty_22_to_30'))
            sheet.write(row, col + 20, line.get('closing_qty_22_to_30'))
            sheet.write(row, col + 21, line.get('in_qty_31_to_60'))
            sheet.write(row, col + 22, line.get('out_qty_31_to_60'))
            sheet.write(row, col + 23, line.get('closing_qty_31_to_60'))
            sheet.write(row, col + 24, line.get('in_qty_before_60'))
            sheet.write(row, col + 25, line.get('out_qty_before_60'))
            sheet.write(row, col + 26, line.get('closing_qty_before_60'))

            row += 1
