{
    'name': 'STN Log Entry',
    'version': "14.0.1.0.0",
    'category': 'wms',
    'author': 'Odoo Bangladesh/Sindabad',
    'summary': 'Stock Transfer Note Process Start to End Log Entry',
    'description': 'Stock Transfer Note Process Start to End Log Entry',
    'depends': ['stn_process'],
    'data': [
        'security/stn_log_security.xml',
        'security/ir.model.access.csv',
        'wizard/product_summary_for_stn_report_wizard_view.xml',
        'report/action_product_summary_report.xml',
        'views/stock_order_wise_product_summary_view.xml',

    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}
