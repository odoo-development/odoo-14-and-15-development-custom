from odoo import api, fields, models, _


class ProductSummaryForSTNReportWizard(models.TransientModel):
    _name = "product.summary.for.stn.report.wizard"
    _description = "Product Summary For STN Report"

    warehouse_id = fields.Many2one('stock.warehouse',default=1,  string='Warehouse')

    def print_report_xls(self):
        data = {
            'warehouse_id': self.warehouse_id.id,
        }
        return self.env.ref('stn_log.action_product_summary_for_stn_report_xlsx').report_action(self, data=data)

    

