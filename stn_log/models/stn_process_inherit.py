from odoo import models, fields
from odoo.tools.translate import _


class StnProcess(models.Model):
    _inherit = 'stn.process'

    warehouse_id_to = fields.Selection([
        ('1', "Uttara Warehouse"),
        ('2', "Nodda Warehouse"),
        ('3', "Signboard Warehouse"),
        ('7', "Badda Warehouse"),
        ('4', "Uttara Damage Warehouse"),
        ('5', "Nodda Damage Warehouse"),
        ('6', "Signboard Damage Warehouse"),
        ('9', "Badda Damage Warehouse"),
        ('10', "Badda HORECA Warehouse"),
        ('11', "Savar Warehouse"),
        ('12', "Savar Damage Warehouse"),
    ], "Requested To", readonly=False, copy=False,
        help="Gives the List of the Warehouses",
        index=True, )

    def send_stn(self):
        new_id = super(StnProcess, self).send_stn()

        self.message_post(body=_("STN Confirmed"))

        return new_id

    def cancel_stn(self):
        new_id = super(StnProcess, self).cancel_stn()

        self.message_post(body=_("STN Cancelled"))

        return new_id

    def receive_stn(self):
        new_id = super(StnProcess, self).receive_stn()

        self.message_post(body=_("STN Received"))

        return new_id

    def request_stn(self):
        new_id = super(StnProcess, self).request_stn()

        self.message_post(body=_("STN Requested"))

        return new_id


class StockOrderWiseProductSummary(models.Model):

    _name = "stock.order.wise.product.summary"
    _description = "StockOrderWiseProductSummary"

    report_date = fields.Datetime('Report Date', default=fields.Datetime.now())
    remarks = fields.Text('Remarks')
    product_id = fields.Many2one('product.product', string='Product', required=True)
    warehouse_id = fields.Many2one('stock.warehouse', string='Warehouse ID', required=True)
    warehouse = fields.Char('Warehouse', required=True)
    category = fields.Char('Category')
    product_code = fields.Char('SKU')
    product_name = fields.Char('Product Name')
    product_reorder_level = fields.Float('Reorder Level')
    total_stock_qty = fields.Float('Total Stock Qty')
    sales_qty = fields.Float('Sales QTY')
    in_qty_0_to_7 = fields.Float('In Qty 0_to_7')
    out_qty_0_to_7 = fields.Float('Out Qty 0_to_7')
    closing_qty_0_to_7 = fields.Float('Closing Qty 0_to_7')
    in_qty_8_to_14 = fields.Float('In Qty 8_to_14')
    out_qty_8_to_14 = fields.Float('Out Qty 8_to_14')
    closing_qty_8_to_14 = fields.Float('Closing Qty 8_to_14')
    in_qty_15_to_21 = fields.Float('In Qty 15_to_21')
    out_qty_15_to_21 = fields.Float('Out Qty 15_to_21')
    closing_qty_15_to_21 = fields.Float('Closing Qty 15_to_21')
    in_qty_22_to_30 = fields.Float('In Qty 22_to_30')
    out_qty_22_to_30 = fields.Float('Out Qty 22_to_30')
    closing_qty_22_to_30 = fields.Float('Closing Qty 22_to_30')
    in_qty_31_to_60 = fields.Float('In Qty 31_to_60')
    out_qty_31_to_60 = fields.Float('Out Qty 31_to_60')
    closing_qty_31_to_60 = fields.Float('Closing Qty 31_to_60')
    in_qty_before_60 = fields.Float('In Qty Before 60')
    out_qty_before_60 = fields.Float('Out Qty Before 60')
    closing_qty_before_60 = fields.Float('Closing Qty Before 60')
    propose_stn_qty = fields.Float('Propose STN Qty')
    wh_stock_qty = fields.Float('W/H Stock Qty')
    company_stock_qty = fields.Float('Company Stock Qty')