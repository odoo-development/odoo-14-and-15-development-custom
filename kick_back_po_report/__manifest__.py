{
    'name': 'KickBack Purchase Order and Bills Report',
    'version': '14.0.0',
    'category': 'Purchase',
    'description': """
KickBack Offer Voucher added in Purchase Order and Bills Report
==============================================================

""",
    'author': 'Sindabad',
    'depends': ['product','purchase','account'],
    'data': [
        "security/kickback_access.xml",
        'report/kickback_po_report.xml',
        'report/kickback_po_bill_report.xml',
        'views/report_menu.xml',
        # 'views/kick_back_view.xml',
    ],

    'installable': True,
    'application': True,
    'auto_install': False,
}

