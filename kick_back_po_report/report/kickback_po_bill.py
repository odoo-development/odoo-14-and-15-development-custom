from odoo import api, models, _
from datetime import datetime


class KickBackReport(models.AbstractModel):
    _name = 'report.kick_back_po_report.kickback_po_bill_report'
    _description = 'Print Kickback Bill Report'

    @api.model
    def _get_report_values(self, docids, data=None):
        docs = self.env['account.move'].browse(docids[0])

        return {
            'doc_ids': docids,
            'doc_model': 'account.move',
            'docs': docs,
            'data': data,

            'get_invoices_data': self.get_invoices_data,
            'total_sum': self.total_sum,
        }

    def get_invoices_data(self, obj):

        final_list = []

        try:
            order_numbers = [obj.split(',')[0]]
        except:
            order_numbers = [str(obj)]

        group_id = None
        data_list = []

        self.env.cr.execute("SELECT id FROM account_move WHERE invoice_origin=%s", (order_numbers))
        for item in self.env.cr.fetchall():
            group_id = [item[0]]

        if group_id is not None:

            order_data = self.env['account.move'].browse(group_id)

            kickback_data = self.env['account.move'].search([('ref', '=', order_data.invoice_origin), ('kickback', '=', True), ('state', '!=', 'cancel')])

            if kickback_data  and str(order_data.state) != 'cancel':

                account_invoice_ids = order_data.invoice_line_ids
                list_limit_counter = 0
                sl = 0
                for items in account_invoice_ids:

                    if items.product_id.default_code:
                        list_limit_counter += 1

                        try:
                            sale_price_per_unit = items.price_unit
                        except ZeroDivisionError:

                            sale_price_per_unit = items.price_unit

                        quantity = round(items.quantity, 2)
                        kickback_amount = kickback_data.amount_total
                        kickback_with_unit_price = kickback_amount / quantity

                        unit_price = round((items.price_unit + kickback_with_unit_price), 2)

                        sl += 1
                        data_list.append({'name': 'cool',
                                          'sl': sl,
                                          'product_name': items.product_id.name,
                                          'quantity': quantity,
                                          'unit_price': unit_price,
                                          'amount': round((unit_price * items.quantity), 2),
                                          'uom': items.product_id.prod_uom,
                                          'sale_price_per_unit': round(sale_price_per_unit, 2),
                                          })


        final_list = data_list

        return final_list

    def total_sum(self, obj):

        try:
            order_numbers = [obj.split(',')[0]]
        except:
            order_numbers = [str(obj)]

        total_subtotal = 0.00
        total_amount = 0.00

        group_id = None

        self.env.cr.execute("SELECT id FROM account_move WHERE invoice_origin=%s", (order_numbers))
        for item in self.env.cr.fetchall():
            group_id = [item[0]]

        if group_id is not None:

            order_data = self.env['account.move'].browse(group_id)
            kickback_data = self.env['account.move'].search(
                [('ref', '=', order_data.invoice_origin), ('kickback', '=', True), ('state', '!=', 'cancel')])

            if kickback_data and str(order_data.state) != 'cancel':

                account_invoice_ids = order_data.invoice_line_ids

                for items in account_invoice_ids:

                    quantity = items.quantity
                    kickback_amount = kickback_data.amount_total
                    kickback_with_unit_price = kickback_amount / quantity

                    unit_price = items.price_unit + kickback_with_unit_price
                    amount = unit_price * items.quantity

                    total_amount += amount

        return [{
                 'total_amount': round(total_amount, 2),
                 }]

