from odoo import models, fields, api, _
from odoo.tools.translate import _
from odoo.exceptions import UserError, ValidationError, Warning


class AccountMove(models.Model):
    _inherit = "account.move"

    kickback = fields.Boolean('Kick Back Offer')

