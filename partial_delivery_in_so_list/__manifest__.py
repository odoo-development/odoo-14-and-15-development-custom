
{
    'name': 'Bring Partial Delivery flag and amount for Sales Order',
    'version': "14.0.1.0.0",
    'category': 'Sales',
    'description': """
        """,
    'author': 'Odoo Bangladesh',
    'depends': ['sale_management',
                'stock_account',
                'delivery_quantity_in_SO'],
    'data': [
        # 'views/sale.xml',
        "views/sale_order_view.xml",
    ],

    'installable': True,
    'auto_install': False,
}