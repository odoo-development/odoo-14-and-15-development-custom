from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta
from collections import defaultdict
import json

from odoo import api, fields, models, _, SUPERUSER_ID
from odoo.exceptions import UserError
from odoo.tools import float_compare, float_round, format_datetime


class SaleOrderLine(models.Model):
    _inherit = 'sale.order.line'

    partial_delivery_flag = fields.Boolean(string='Partially ', compute="_get_counted", store=True,
                                           help="Quantity Partially Delivered")

    @api.model
    def check_move(self, move):
        if move.state != 'done':
            return False
        if all([self._context.get('date_start'), self._context.get('date_stop')]):
            if not (move.date >= self._context['date_start'] and move.date <= self._context['date_stop']):
                return False
        return True

    def calculate_delivered_amount(self):
        delivered_qty = None
        qty_return = None
        product_id = None
        order_id = None

        self.env.cr.execute("select product_id, product_uom_qty,qty_delivered, price_unit, qty_return from "
                            " sale_order_line where order_id=%s", ([order_id.id]))

        all_data = self.env.cr.fetchall()

        res = False
        total_sum = 0.00

        for items in all_data:
            if items[0] == product_id:
                total_sum = total_sum + (items[3] * delivered_qty) - (items[3] * qty_return)
            else:
                total_sum = total_sum + (items[2] * items[3]) - (items[3] * items[4])

        self.env.cr.execute("update sale_order set delivered_amount=%s where id=%s", ([total_sum, order_id.id]))

        return res

    def calculate_result_data(self):

        order_id = None
        self.env.cr.execute(
            "select product_uom_qty,qty_delivered, partial_delivery_flag from  sale_order_line where order_id=%s",
            ([order_id.id]))

        all_data = self.env.cr.fetchall()

        all_products_delivered = False

        count_delivered_items = 1
        res = False

        for items in all_data:
            if items[0] == items[1]:
                count_delivered_items += 1

        if len(all_data) != count_delivered_items:
            res = True

        return res

    @api.model
    def _get_real_move_qty(self, move):
        src = move.location_id.usage
        dst = move.location_dest_id.usage
        # pp = self.product_uom._compute_quantity(move.product_qty, self.product_uom)

        if src == dst:
            return 0.0
        elif src == 'internal':
            return self.product_uom._compute_quantity(move.product_qty, self.product_uom)
        elif dst == 'internal':
            return -self.product_uom._compute_quantity(move.product_qty, self.product_uom)
        else:
            return 0.0

    # @api.depends('move_ids.state')
    def _get_counted(self):

        qty_delivered = 0
        qty_return = 0
        for move in self.move_ids:
            if self.check_move(move):
                # Add Quantity
                if self._get_real_move_qty(move) > 0:
                    qty_delivered += self._get_real_move_qty(move)
                else:
                    qty_return -= self._get_real_move_qty(move)

        self.partial_delivery_flag = False

        if qty_delivered > 0:
            for line in self:
                ordered_qty = line.product_uom_qty
                if ordered_qty > (qty_delivered - abs(qty_return)):
                    self.partial_delivery_flag = True
