from datetime import datetime

from odoo import fields
from odoo import models
from odoo.exceptions import Warning
from odoo.osv import osv
from odoo.tools.translate import _


class partial_waiting_process_class(object):

    def partially_available_process(self, client_order_ref):

        """
        stock_picking any picking state "partially_available"
        """

        try:
            if client_order_ref != 'None':
                so_query = "SELECT name FROM sale_order WHERE client_order_ref='{0}'".format(client_order_ref)
                self.env.cr.execute(so_query)
                sale_order_data = self.env.cr.fetchall()
                so_name = str(sale_order_data[0][0])

                stock_picking_status = list()
                stock_picking_query = "SELECT state FROM stock_picking WHERE origin='{0}'".format(so_name)
                self.env.cr.execute(stock_picking_query)
                for sp in self.env.cr.fetchall():
                    stock_picking_status.append(str(sp[0]))

                partially_available = False
                if "partially_available" in stock_picking_status:
                    partially_available = True
                else:
                    partially_available = False

                query_string = "UPDATE sale_order SET partially_available='{0}' WHERE name='{1}'".format(
                    partially_available, so_name)

                self.env.cr.execute(query_string)
                self.env.cr.commit()

        except:
            pass

        return True

    def partially_delivered_prcess(self, sale_order_pool, client_order_ref):
        if client_order_ref.isdigit():
            # partially delivered amount
            so_query = "SELECT id, amount_total FROM sale_order WHERE client_order_ref='{0}'".format(client_order_ref)
            self.env.cr.execute(so_query)
            sale_order_data = self.env.cr.fetchall()
            so_id = sale_order_data[0][0]
            so_amount_total = sale_order_data[0][1]

            res = {}
            for sale in sale_order_pool.browse([so_id]):
                total_sum = 0.00

                for obj in sale.order_line:
                    total_sum = total_sum + (obj.qty_delivered * obj.price_unit) - (obj.qty_return * obj.price_unit)

                res[sale.id] = total_sum

            partially_delivered = False
            if res[so_id] < so_amount_total and res[so_id] > 0:
                partially_delivered = True
            else:
                partially_delivered = False

            query_string = "UPDATE sale_order SET partial_delivered='{0}' WHERE id='{1}'".format(partially_delivered,
                                                                                                 so_id)

            self.env.cr.execute(query_string)
            self.env.cr.commit()

        return True

    def waiting_availability_process(self, client_order_ref):

        if client_order_ref.isdigit():

            sale_order_data_query = "SELECT id, client_order_ref, state, amount_total, name FROM sale_order WHERE client_order_ref='{0}'".format(
                client_order_ref)

            self.env.cr.execute(sale_order_data_query)
            sale_order_data = self.env.cr.fetchall()
            so_id = sale_order_data[0][0]
            so_mg_ref = str(sale_order_data[0][1])
            so_state = str(sale_order_data[0][2])
            so_amount_total = sale_order_data[0][3]
            so_name = str(sale_order_data[0][4])

            # for partially available
            stock_move_query = "SELECT state FROM stock_move WHERE origin = '{0}'".format(so_name)
            self.env.cr.execute(stock_move_query)
            stock_move_ref = self.env.cr.fetchall()
            move_state_list = [str(sm[0]) for sm in stock_move_ref]

            waiting_available = False
            if "confirmed" in move_state_list:
                waiting_available = True
            else:
                waiting_available = False

            query_string = "UPDATE sale_order SET waiting_availability='{0}' WHERE name='{1}'".format(waiting_available,
                                                                                                      so_name)

            self.env.cr.execute(query_string)
            self.env.cr.commit()

        return True


class SaleOrder(models.Model):
    _inherit = "sale.order"

    # partial delivered status
    def _get_partial_delivered_status(self):
        res = False
        res = {}
        """
        for sale in self.browse(cursor, user, ids, context=context):
            res[sale.id]=False
            if  sale.shipped == False:
                for obj in sale.order_line:
                    if obj.partial_delivery_flag == True:
                        res[sale.id] = True
                        break
        """
        so_delivery_amount = self._get_delivered_amount()

        # Raw query to set res
        sale_order_amount_query = "SELECT id, amount_total FROM sale_order WHERE id IN {0}".format(tuple(self.ids))
        self.env.cr.execute(sale_order_amount_query)
        for sale in self.env.cr.fetchall():
            if so_delivery_amount[int(sale[0])] < sale[1] and so_delivery_amount[int(sale[0])] > 0:
                res[int(sale[0])] = True
            else:
                res[int(sale[0])] = False

        # ORM to set res
        """
        for sale in self.browse(cursor, user, ids, context=context):
            
            if so_delivery_amount[sale.id] < sale.amount_total and so_delivery_amount[sale.id] > 0:
                res[sale.id] = True
            else:
                res[sale.id] = False
        """

        return res

    # partial delivered search
    def _partial_delivery_search(self, name, args):
        if not len(args):
            return []

        # select all so where state is not draft
        confirmed_so_list = list()
        so_not_draft_query = "SELECT id FROM sale_order WHERE state IN ('progress', 'done')"
        self.env.cr.execute(so_not_draft_query)
        for so in self.env.cr.fetchall():
            confirmed_so_list.append(so[0])

        so_delivery_amount = self._get_delivered_amount()
        res = dict()

        # Raw query to set res
        sale_order_amount_query = "SELECT id, amount_total FROM sale_order WHERE id IN {0}".format(
            tuple(confirmed_so_list))
        self.env.cr.execute(sale_order_amount_query)
        for sale in self.env.cr.fetchall():
            if so_delivery_amount[int(sale[0])] < sale[1] and so_delivery_amount[int(sale[0])] > 0:
                res[int(sale[0])] = True
            else:
                res[int(sale[0])] = False

        # ORM to set res
        """
        for sale in self.browse(cursor, user, confirmed_so_list, context=context):
            if so_delivery_amount[sale.id] < sale.amount_total and so_delivery_amount[sale.id] > 0:
                res[sale.id] = True
            else:
                res[sale.id] = False
        """

        # ('partial_delivered', '!=', True)
        args_str = eval(str(args[0]))

        arg_status = False
        if args_str[1] == '!=':
            arg_status = False
        else:
            arg_status = True

        filtered_ids = [('id', 'in', [item for item in res if res[item] == arg_status])]

        return filtered_ids

    # waiting availability status
    def _get_waiting_availability_status(self):

        res = dict()
        ids = self.ids

        if len(ids) == 1:
            all_ids = "(" + str(ids[0]) + ")"
        else:
            all_ids = tuple(ids)

        sale_order_names_query = "SELECT name FROM sale_order WHERE id IN {0}".format(all_ids)
        self.env.cr.execute(sale_order_names_query)
        sale_order_names_ref = self.env.cr.fetchall()
        sale_order_names = [str(son[0]) for son in sale_order_names_ref]

        if len(sale_order_names) == 1:
            all_sale_order_names = "('" + str(sale_order_names[0]) + "')"
        else:
            all_sale_order_names = tuple(sale_order_names)

        # stock_move confirmed -> waiting_availability
        stock_move_query = "SELECT DISTINCT(origin) FROM stock_move WHERE state='confirmed' AND origin IN {0}".format(
            all_sale_order_names)
        self.env.cr.execute(stock_move_query)
        stock_move_ref = self.env.cr.fetchall()
        stock_picking_partial = [str(sm[0]) for sm in stock_move_ref]

        for so in self.browse(ids):

            if str(so.name) in stock_picking_partial:
                res[int(str(so.id))] = True

            else:
                res[int(str(so.id))] = False

        return res

    # waiting availability search
    def _waiting_availability_search(self, args):
        if not len(args):
            return []

        # select all so where state is not draft
        confirmed_so_list = list()
        confirmed_so_query = "SELECT id, name FROM sale_order WHERE state IN ('progress', 'done')"
        self.env.cr.execute(confirmed_so_query)
        confirmed_so_ref = self.env.cr.fetchall()
        confirmed_so_list = [int(cso[0]) for cso in confirmed_so_ref]
        confirmed_so_name_list = [str(cso[1]) for cso in confirmed_so_ref]

        stock_picking_query = "SELECT origin FROM stock_picking WHERE state = 'partially_available' AND origin IN {0}".format(
            tuple(confirmed_so_name_list))
        self.env.er.execute(stock_picking_query)
        stock_picking_ref = self.env.cr.fetchall()
        stock_picking_partial_origin_list = [str(spr[0]) for spr in stock_picking_ref]

        res = dict()
        for sale in self.browse(confirmed_so_list):
            if str(sale.name) in stock_picking_partial_origin_list:
                res[sale.id] = True
            else:
                res[sale.id] = False

        # ('waiting_availability', '!=', True)
        args_str = eval(str(args[0]))

        arg_status = False
        if args_str[1] == '!=':
            arg_status = False
        else:
            arg_status = True

        filtered_ids = [('id', 'in', [item for item in res if res[item] == arg_status])]
        return filtered_ids

    # delivered amount
    def _get_delivered_amount(self):
        for so in self:
            total_sum = 0.00
            for line in so.order_line:
                total_sum = total_sum + (line.qty_delivered * line.price_unit) - (line.qty_return * line.price_unit)
                if line.product_id.default_code in ['Delivery_007','CSC_007','DS_007'] :
                    total_sum = total_sum + line.price_subtotal
            so.delivered_amount = total_sum

    def _get_confirmed_so_list(self):

        # select all so where state is not draft
        confirmed_so_list = list()
        so_not_draft_query = "SELECT id FROM sale_order WHERE state IN ('progress', 'done')"
        self.env.cr.execute(so_not_draft_query)
        for so in self.env.cr.fetchall():
            confirmed_so_list.append(so[0])

        return confirmed_so_list

    def _partial_delivered_computation(self):

        confirmed_so_list = self._get_confirmed_so_list()

        partial_delivered_res = self._get_partial_delivered_status()

        for res_id in partial_delivered_res:
            query_string = "UPDATE sale_order SET partial_delivered='{0}' WHERE id='{1}'".format(
                partial_delivered_res[res_id], res_id)

            self.env.cr.execute(query_string)
            self.env.cr.commit()

        return True

    def _waiting_availability_computation(self):

        confirmed_so_list = self._get_confirmed_so_list()

        waiting_availability_res = self._get_waiting_availability_status(confirmed_so_list, "waiting_availability")

        for res_id in waiting_availability_res:
            query_string = "UPDATE sale_order SET waiting_availability='{0}' WHERE id='{1}'".format(
                waiting_availability_res[res_id], res_id)

            self.env.cr.execute(query_string)
            self.env.cr.commit()

        return True

    def _partial_available_computation(self):

        so_query = "SELECT client_order_ref FROM sale_order WHERE state IN ('progress', 'done')"
        self.env.cr.execute(so_query)
        # so_client_order_ref = cr.fetchall()
        pwp = partial_waiting_process_class()
        for client_order_ref in self.env.cr.fetchall():
            pwp.partially_available_process(str(client_order_ref[0]))

        return True

    def _get_full_partially_available(self):
        for so in self:
            full_partially_available = False
            for picking in so.picking_ids:
                for move in picking.move_lines:
                    if move.state == "assigned" and so.partially_available:
                        full_partially_available = True
                        break
                    else:
                        full_partially_available = False

                if full_partially_available:
                    break

            so.full_partially_available = full_partially_available


        # res = {}
        # full_partially_available = False
        #
        # for so in self.browse():
        #
        #     stock_move_obj = self.env["stock.move"]
        #
        #     stock_move = stock_move_obj.search([('origin', '=', str(so.name))])
        #     stock_move_line = stock_move_obj.browse(stock_move)
        #
        #     for sm in stock_move_line:
        #         if str(sm.state) == "assigned" and so.partially_available == True:
        #             full_partially_available = True
        #             break
        #         else:
        #             full_partially_available = False
        #
        #     res[so.id] = full_partially_available
        #
        # return res

    def _get_full_partially_available_search(self, args):
        if not len(args):
            return []
        res = dict()
        count = 1

        for so in self.browse(self.search([])):
            count += 1
            if count == 3000:
                break

            stock_move_obj = self.env["stock.move"]

            stock_move = stock_move_obj.search([('origin', '=', str(so.name))])
            stock_move_line = stock_move_obj.browse(stock_move)

            for sm in stock_move_line:
                if str(sm.state) == "assigned" and so.partially_available == True:

                    res[so.id] = True
                    break
                else:
                    res[so.id] = False

        args_str = eval(str(args[0]))
        arg_status = False
        if args_str[1] == '!=':
            arg_status = False
        else:
            arg_status = True

        filtered_ids = [('id', 'in', [item for item in res if res[item] == arg_status])]

        return filtered_ids

    delivered_amount = fields.Float(string='Delivered Amount', compute='_get_delivered_amount')

    ### Commented By Matiar Rahman
    ### Need to implement the logic to set value for the below attributes
    partial_delivered = fields.Boolean('Partial Delivered')
    waiting_availability = fields.Boolean('Waiting Availability')
    partially_available = fields.Boolean('Partially Available')

    full_partially_available = fields.Boolean(string='Full Partially Available', compute='_get_full_partially_available')


class StockPicking(models.Model):
    _inherit = "stock.picking"

    def rereserve_pick(self):
        """
        This can be used to provide a button that rereserves taking into account the existing pack operations
        """
        for pick in self.browse():
            self.rereserve_quants(pick, move_ids=[x.id for x in pick.move_lines
                                                           if x.state not in ('done', 'cancel')])

        if str(pick.origin).startswith("SO"):
            so_query = "SELECT client_order_ref FROM sale_order WHERE name='{0}'".format(str(pick.origin))
            self.env.cr.execute(so_query)
            sale_order_data = self.env.cr.fetchall()
            client_order_ref = str(sale_order_data[0][0])

            pwp = partial_waiting_process_class()
            pwp.waiting_availability_process(client_order_ref)
            pwp.partially_available_process(client_order_ref)
