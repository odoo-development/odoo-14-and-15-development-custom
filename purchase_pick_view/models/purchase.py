from odoo import api, fields, models
from odoo.osv import osv
from odoo.tools.translate import _


class PurchaseOrder(models.Model):
    _inherit = "purchase.order"

    def view_picking(self):
        '''
        This function returns an action that display existing picking orders of given purchase order ids.
        '''
        mod_obj = self.env['ir.model.data']
        dummy, action_id = tuple(mod_obj.get_object_reference('stock', 'action_picking_tree'))
        action = self.env['ir.actions.act_window'].read(action_id)

        pick_ids = []
        origin_list = []

        for po in self.browse():
            pick_ids += [picking.id for picking in po.picking_ids]

        pick_ids_new = self.env['stock.picking'].search([('id', 'in', pick_ids)])
        abc_data = self.env['stock.picking'].browse(pick_ids_new)

        group_id = []
        for it_n in abc_data:
            origin_list.append(str(it_n.name))
            group_id = it_n.group_id.id

        final_pick_ids_new = self.env['stock.picking'].search([('origin', 'in', origin_list)])

        for pic_id in final_pick_ids_new:
            if pic_id not in pick_ids:
                pick_ids.append(pic_id)

        g_final_pick_ids_new = self.env['stock.picking'].search([('group_id', 'in', [group_id])], )
        for pic_id in g_final_pick_ids_new:
            if pic_id not in pick_ids:
                pick_ids.append(pic_id)

        # override the context to get rid of the default filtering on picking type
        action['context'] = {}
        # choose the view_mode accordingly
        if len(pick_ids) > 1:
            action['domain'] = "[('id','in',[" + ','.join(map(str, pick_ids)) + "])]"
        else:
            res = mod_obj.get_object_reference('stock', 'view_picking_form')
            action['views'] = [(res and res[1] or False, 'form')]
            action['res_id'] = pick_ids and pick_ids[0] or False

        return action

    def _len_of_picking(self):
        '''
        This function returns an action that display existing picking orders of given purchase order ids.
        '''
        mod_obj = self.env['ir.model.data']
        dummy, action_id = tuple(mod_obj.get_object_reference('stock', 'action_picking_tree'))
        action = self.env['ir.actions.act_window'].read(action_id)

        pick_ids = []
        origin_list = []
        inv_ids = []

        for po in self.browse():
            pick_ids += [picking.id for picking in po.picking_ids]
            inv_ids = [items.id for items in po.invoice_ids]

        pick_ids_new = self.env['stock.picking'].search([('id', 'in', pick_ids)])
        abc_data = self.env['stock.picking'].browse(pick_ids_new)

        group_id = []
        for it_n in abc_data:
            origin_list.append(str(it_n.name))
            group_id = it_n.group_id.id

        final_pick_ids_new = self.env['stock.picking'].search([('origin', 'in', origin_list)])

        for pic_id in final_pick_ids_new:
            if pic_id not in pick_ids:
                pick_ids.append(pic_id)

        g_final_pick_ids_new = self.env['stock.picking'].search([('group_id', 'in', [group_id])])
        for pic_id in g_final_pick_ids_new:
            if pic_id not in pick_ids:
                pick_ids.append(pic_id)
        # Folowing code for Invoice counting

        pick_ids_new = self.env['stock.picking'].search([('id', 'in', pick_ids)])
        new_abc_data = self.env['stock.picking'].browse(pick_ids_new)
        new_origin_list = []

        for stk_obj in new_abc_data:
            new_origin_list.append(str(stk_obj.name))

        final_invoice_ids_new = self.env['account.invoice'].search([('origin', 'in', new_origin_list)])

        for inv_id in final_invoice_ids_new:
            if inv_id not in inv_ids:
                inv_ids.append(inv_id)

        res = {}
        for po in self.browse():
            res[po.id] = {
                'shipment_count': len(pick_ids),
                'invoice_count': len(inv_ids),
            }

        return res

    def invoice_open(self):
        mod_obj = self.env['ir.model.data']
        act_obj = self.env['ir.actions.act_window']

        result = mod_obj.get_object_reference('account', 'action_invoice_tree2')
        id = result and result[1] or False
        result = act_obj.read([id])[0]
        inv_ids = []
        ## Custom INvoice counts ends here
        pick_ids = []
        origin_list = []

        for po in self.browse():
            inv_ids += [invoice.id for invoice in po.invoice_ids]
            pick_ids += [picking.id for picking in po.picking_ids]

        pick_ids_new = self.env['stock.picking'].search([('id', 'in', pick_ids)])
        abc_data = self.env['stock.picking'].browse(pick_ids_new)

        group_id = []
        for it_n in abc_data:
            origin_list.append(str(it_n.name))
            group_id = it_n.group_id.id

        final_pick_ids_new = self.env['stock.picking'].search([('origin', 'in', origin_list)])

        for pic_id in final_pick_ids_new:
            if pic_id not in pick_ids:
                pick_ids.append(pic_id)

        g_final_pick_ids_new = self.env['stock.picking'].search([('group_id', 'in', [group_id])])

        for pic_id in g_final_pick_ids_new:
            if pic_id not in pick_ids:
                pick_ids.append(pic_id)

        pick_ids_new = self.env['stock.picking'].search([('id', 'in', pick_ids)])
        new_abc_data = self.env['stock.picking'].browse(pick_ids_new)
        new_origin_list = []

        for stk_obj in new_abc_data:
            new_origin_list.append(str(stk_obj.name))

        final_invoice_ids_new = self.env['account.invoice'].search([('origin', 'in', new_origin_list)])

        for inv_id in final_invoice_ids_new:
            if inv_id not in inv_ids:
                inv_ids.append(inv_id)
        ## Custom INvoice counts ends here

        if not inv_ids:
            raise osv.except_osv(_('Error!'), _('Please create Invoices.'))
        # choose the view_mode accordingly
        if len(inv_ids) > 1:
            result['domain'] = "[('id','in',[" + ','.join(map(str, inv_ids)) + "])]"
        else:
            res = mod_obj.get_object_reference('account', 'invoice_supplier_form')
            result['views'] = [(res and res[1] or False, 'form')]
            result['res_id'] = inv_ids and inv_ids[0] or False
        return result

    shipment_count = fields.Integer(compute='_len_of_picking', string='Incoming Shipments', multi=True)

    # _columns = {
    #     'shipment_count': fields.function(_len_of_picking, type='integer', string='Incoming Shipments', multi=True),
    # }
