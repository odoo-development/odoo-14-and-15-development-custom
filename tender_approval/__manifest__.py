# Copyright 2022 Rocky

{
    "name": "Tender Approval Process",
    "version": "14.0",
    "author": "Sindabad",
    "category": "Sales Management",
    "license": "AGPL-3",
    "depends": ['sale','purchase', 'mail'],
    "data": [
        'security/tender_approval_access.xml',
        'security/ir.model.access.csv',
        'data/ir_sequence_data.xml',
        'wizard/approval_remark_view.xml',
        'wizard/confirmed_remark_view.xml',
        'wizard/cancel_view.xml',
        'views/tender_approval_view.xml',
    ],
    "installable": True,
}
