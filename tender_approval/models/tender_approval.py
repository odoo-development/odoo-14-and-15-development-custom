# Author Rocky 2021

from odoo import api, models, fields,_
from odoo.exceptions import UserError


class TenderApproval(models.Model):
    _name = 'tender.approval'
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _description = 'Tender Approval Process'

    name = fields.Char('Name')
    sequence = fields.Integer(string='Sequence')

    partner_id = fields.Many2one('res.partner', 'Company', change_default=True, tracking=True,
                                 domain=[('is_company', '=', True),('x_classification', '=', 'Corporate')])
    total_amount = fields.Float('Amount',change_default=True, tracking=True)
    po_number = fields.Many2one('purchase.order','PO Number', tracking=True)
    description = fields.Text('Description')
    state = fields.Selection([
        ('draft', 'Pending'),
        ('approved', 'Approved'),
        ('confirmed', 'Confirmed'),
        ('done', 'Create PO & Close'),
        ('cancel', 'Cancelled'),
    ], string='Status', readonly=True, copy=False, index=True, tracking=True, default='draft')
    confirm_date = fields.Datetime('Confirmed Date', tracking=True)
    confirm_by = fields.Many2one('res.users', 'Confirmed By', tracking=True)

    approve_date = fields.Datetime('Approve Date', tracking=True)
    approve_by = fields.Many2one('res.users', 'Approve By', tracking=True)
    approve_remark = fields.Text('Approval Remark', tracking=True)
    confirm_remark = fields.Text('Confirm Remark', tracking=True)

    cancel_date = fields.Datetime('Cancelled Date', tracking=True)
    cancel_by = fields.Many2one('res.users', 'Cancelled By', tracking=True)
    cancel_reason = fields.Char('Reason', tracking=True)

    done_date = fields.Datetime('Approve Date', tracking=True)
    done_by = fields.Many2one('res.users', 'Approve By', tracking=True)

    credit_limit = fields.Float(string="Credit Limit")
    receivable_amount = fields.Float(string="Receivable Amount")
    payable_amount = fields.Float(string="Payable Amount")

    @api.onchange('partner_id')
    def _onchange_credit_limit(self):
        if self.partner_id:
            self.credit_limit = self.partner_id.credit_limit

    @api.onchange('partner_id')
    def _onchange_receivable_amount(self):
        if self.partner_id:
            self.receivable_amount = self.partner_id.credit

    @api.onchange('partner_id')
    def _onchange_payable_amount(self):
        if self.partner_id:
            self.payable_amount = self.partner_id.debit

    @api.model
    def create(self, vals):
        if vals['total_amount']<=float(0):
            raise UserError(_('Amount has been greater than zero'))
        record = super(TenderApproval, self).create(vals)
        tender_name = self.env['ir.sequence'].next_by_code('tender.approval')
        record.name = tender_name
        record.credit_limit = record.partner_id.credit_limit
        record.receivable_amount = record.partner_id.credit
        record.payable_amount = record.partner_id.debit

        return record

    def tender_approved(self,reason):
        self.write({
            'state': 'approved',
            'approve_by': self.env.uid,
            'approve_date': fields.datetime.now(),
            'approve_remark': reason
        })

    def tender_confirmed(self,reson):
        self.write({
            'state': 'confirmed',
            'confirm_by': self.env.uid,
            'confirm_date': fields.datetime.now(),
            'confirm_remark': reson
        })

    def tender_done(self):
        if not self.po_number:
            raise UserError(_('Please enter the PO number'))
        self.write({
            'state': 'done',
            'done_by': self.env.uid,
            'done_date': fields.datetime.now(),
        })

    def tender_cancel(self,reason):
        self.write({
            'state': 'cancel',
            'cancel_by': self.env.uid,
            'cancel_date': fields.datetime.now(),
            'cancel_reason': reason
        })


class CancelTender(models.Model):
    _name = "tender.cancel"
    _description = "Cancel tender"

    cancel_by = fields.Many2one('res.users', 'Cancel By')
    cancel_reason = fields.Char('Reject Reason',)

    def cancel_tender(self):
        cancel_reason = self.env.context['cancel_reason']
        ids = self.env.context['active_ids']

        for id in ids:
            try:
                if cancel_reason:
                    self.env['tender.approval'].browse(id).tender_cancel(cancel_reason)
            except:
                pass

        return True


class ApprovalCFOTender(models.Model):
    _name = "approval.cfo.tender"
    _description = "Approval Tender"

    approve_by = fields.Many2one('res.users', 'Approve By')
    approve_remark = fields.Text('Remark')

    def approve_tender(self):
        approve_remark = self.env.context['approve_remark']
        ids = self.env.context['active_ids']

        for id in ids:
            try:
                if approve_remark:
                    self.env['tender.approval'].browse(id).tender_approved(approve_remark)
            except:
                pass

        return True


class ConfirmedApprovalTender(models.Model):
    _name = "confirmed.approval.tender"
    _description = "Confirmed Approval Tender"

    confirm_by = fields.Many2one('res.users', 'Confirmed By')
    confirm_remark = fields.Text('Remark',)

    def confirmed_tender(self):
        confirm_remark = self.env.context['confirm_remark']
        ids = self.env.context['active_ids']

        for id in ids:
            try:
                if confirm_remark:
                    self.env['tender.approval'].browse(id).tender_confirmed(confirm_remark)
            except:
                pass

        return True