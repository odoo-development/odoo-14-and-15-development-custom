{
    'name': 'Stock Restricts Zero Quantity Return',
    'version': "14.0.1.0.0",
    'category': 'Stock Management',
    'author': 'Odoo Bangladesh',
    'summary': 'Inventory, Logistic, Storage',
    'description': 'Stock Restricts Zero Quantity Return',
    'depends': ['stock'],
    'installable': True,
    'data': [
        'wizard/stock_return_picking_view.xml',
    ],
    'application': True,
    'auto_install': False,
}
