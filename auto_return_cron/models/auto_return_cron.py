import json
import datetime
from odoo import api, fields, models, _
from odoo.exceptions import UserError, ValidationError, Warning


class AutoReturnCron(models.Model):
    _name = "auto.return.cron"

    name = fields.Char('Name')

    def cancel_inv_return_stock(self,hour=None,range=1):
        hour = hour
        start_time = fields.datetime.now()

        actual_create_date = datetime.datetime.now()
        create_date = actual_create_date - datetime.timedelta(hours=hour)

        invoices = self.env['account.move'].search([('assigned', '=', False),('x_loading_assign', '=', False),('customer_classification','like','Retail%'),('delivered', '=', False),('state', '!=', 'cancel'),('create_date','<',create_date),('move_type','=','out_invoice'),('ndr','!=',True),('payment_state','=','not_paid'),('amount_total','>',0),('x_auto_cancel','!=',True)],limit=range)
        out_product_id = []
        invoice_name_list = []
        return_picking_list = []
        order_list = []
        return_picking_id = None
        print (invoices)
        print ('*'*50)
        for invoice in invoices:
            try:
                start_time1 = fields.datetime.now()
                stock_objects = self.env['stock.picking'].search([('origin', '=', invoice.invoice_origin),('state','=','done')])
                service_product_query = "select id from product_product WHERE default_code in ('Delivery_007','CSC_007','DS_007')"
                self.env.cr.execute(service_product_query)
                service_product_id = [product_id['id'] for product_id in self.env.cr.dictfetchall()]

                if stock_objects and invoice.ref:
                    product_return_moves = [[5, False, False]]

                    invoice.button_draft()

                    invoice.button_cancel()
                    for stock_item in stock_objects:
                        stock_object = stock_item
                        product_id_return = []
                        for items in invoice.invoice_line_ids:
                            if items.product_id.id not in service_product_id:
                                out_product_id.append(items.product_id.id)

                            for st_items in stock_item.move_lines:
                                if items.product_id.id == st_items.product_id.id and items.quantity == st_items.product_uom_qty:
                                    product_id_return.append(st_items.product_id.id)
                                    product_dict = {
                                        'product_id': st_items.product_id.id,
                                        'quantity': st_items.product_uom_qty,
                                        'move_id': st_items.id
                                    }
                                    product_return_moves.append([0, False, product_dict])

                        if product_return_moves and len(out_product_id) == len(product_id_return):
                            stock_object = stock_item
                            break

                    ###########################################################################

                    prepare_data = {'product_return_moves': product_return_moves,
                                    'location_id': stock_object.location_id.id,
                                    }

                    return_picking = self.env['stock.return.picking'].with_context(active_model='stock.picking',
                                                                                   active_ids=[stock_object.id],
                                                                                   active_id=stock_object.id, refund=True).create(prepare_data)

                    result = return_picking.with_context(active_id=stock_object.id).create_returns()

                    if result and result.get('res_id', False):
                        return_picking_id = result.get('res_id', False)
                        # Immediate Transfer
                        immediate_transfer_line_ids = [[0, False, {
                            'picking_id': return_picking_id,
                            'to_immediate': True
                        }]]

                        stock_immediate_trn = self.env['stock.immediate.transfer'].create({
                            'pick_ids': [(4, return_picking_id)],
                            'show_transfers': False,
                            'immediate_transfer_line_ids': immediate_transfer_line_ids
                        })

                        stock_immediate_trn.with_context(button_validate_picking_ids=stock_immediate_trn.pick_ids.ids).process()

                        return_picking_list.append(return_picking_id)
                        self.env.cr.execute("UPDATE account_move SET cancel_account_invoice_reason='Auto Cancel',cancel_account_invoice_date=%s WHERE id = %s",(fields.datetime.now(),invoice.id,))
                        self.env.cr.commit()

                        self.env.cr.execute(
                            "UPDATE sale_order SET order_close_reason='Auto Cancel',cl_cancel_reason='Auto Invoice Cancel' WHERE name = %s",
                            (invoice.invoice_origin,))
                        self.env.cr.commit()

                    else:
                        print ("&"*50)
                        self.env.cr.rollback()

                    try:
                        log_data = {
                            'entry_log_start_time': start_time1,
                            'entry_log_time': fields.datetime.now(),
                            'invoice_name_list': str(invoice.name),
                            'return_challan_list': str(return_picking_id),
                            'order_list': str(order_list),
                        }
                        stock_log = self.env['auto.cancel.invoice.synch.log'].sudo().create(log_data)

                        data = {
                            'magento_id': str(invoice.ref),
                            'order_state': str('return'),
                            'state_time': fields.datetime.now(),
                        }
                        # new_r = self.env["order.status.synch.log"].sudo().create(data)

                    except:
                        pass
                self.env.cr.execute(
                    "UPDATE account_move SET x_auto_cancel= TRUE WHERE id = %s",
                    (invoice.id,))
                self.env.cr.commit()
                invoice_name_list.append(invoice.name)
                order_list.append(invoice.ref)
            except:
                self.env.cr.rollback()
        try:
            self.env['auto.cancel.synch.log'].sudo().create({
                'entry_log_start_time': start_time,
                'entry_log_time': fields.datetime.now(),
                'invoice_name_list': str(invoice_name_list),
                'return_challan_list': str(return_picking_list),
                'order_list': str(order_list),
                'total_cancel_invoice': str(len(invoice_name_list)),
                'total_return_challan': str(len(return_picking_list))
            })
        except:
            pass

    def cancel_order(self,hour=None,range=1):
        hour = hour
        start_time = fields.datetime.now()
        created_date = datetime.datetime.strftime(start_time - datetime.timedelta(hours=hour),'%Y-%m-%d %H:%M:%S')

        pending_order_query = "Select sale_order.id from sale_order where id not in (Select so.id from sale_order as so where so.name in (select invoice_origin from account_move WHERE account_move.customer_classification LIKE 'Retail%') and so.state!= 'cancel' and so.customer_classification LIKE 'Retail%') and sale_order.state!= 'cancel' and sale_order.customer_classification LIKE 'Retail%'"
        self.env.cr.execute(pending_order_query)
        pending_order_id_list = [res[0] for res in self.env.cr.fetchall()]
        so_objects = self.env['sale.order'].search([('id', 'in', pending_order_id_list),
                                                    ('create_date', '<', created_date),
                                                    ('x_auto_cancel', '=', False)
                                                    ], limit=range)
        for so in so_objects:
            invoice = so.invoice_ids
            if len(invoice)==0:
                done_stock_query = "Select count(*) from stock_picking where state = 'done' and origin = %s"
                self.env.cr.execute(done_stock_query, (so.name,))
                done_stock = [rest[0] for rest in self.env.cr.fetchall()]
                if done_stock:
                    done_stock = list(set(done_stock))
                    if len(done_stock) == 1 and done_stock[0]==0:
                        auto_cancel = True
                    else:
                        auto_cancel = False
                else:
                    auto_cancel = True

                if auto_cancel :
                    so.action_cancel()
                    print ('^^^' * 50)
                    self.env.cr.execute(
                        "UPDATE sale_order SET cl_cancel_reason='Auto Cancel',x_auto_cancel= TRUE WHERE id = %s",
                        (so.id,))
                    self.env.cr.commit()

                    try:
                        self.env['auto.cancel.synch.log'].sudo().create({
                            'entry_log_start_time': start_time,
                            'entry_log_time': fields.datetime.now(),
                            'order_list': so.name
                        })
                    except:
                        pass


class AutoCancelInvoiceSynchLog(models.Model):
    _name = "auto.cancel.invoice.synch.log"
    _description = 'Auto Cancel Invoice Stock Synch Log'

    entry_log_time = fields.Datetime("Entry Log Time")
    entry_log_start_time = fields.Datetime("Entry Log Start Time")
    invoice_name_list = fields.Text("Invoice Name")
    return_challan_list = fields.Text("Return Challan")
    order_list = fields.Text("Order List")


class AutoCancelSynchLog(models.Model):
    _name = "auto.cancel.synch.log"
    _description = 'Auto Cancel Stock Synch Log'

    entry_log_time = fields.Datetime("Entry Log Time")
    entry_log_start_time = fields.Datetime("Entry Log Start Time")
    invoice_name_list = fields.Text("Invoice Name")
    return_challan_list = fields.Text("Return Challan")
    order_list = fields.Text("Order List")
    total_cancel_invoice = fields.Text("Total # Cancel Invoice")
    total_return_challan = fields.Text("Total # Cancel Challan")
