{
    'name': 'Auto Invoice Return/Refund & Order Cancel Cron',
    'version': '14.0',
    'category': 'Accounting/Accounting',
    'author': 'Sindabad',
    'summary': 'Auto Invoice Return/Refund & Order Cancel Cron',
    'description': 'Auto Invoice Return/Refund & Order Cancel Cron',
    'depends': ['base','product', 'sale','purchase','stock','account','send_sms_on_demand','vat_calculation_custom'],
    'data': [
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}
