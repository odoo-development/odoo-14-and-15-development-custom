import json
import datetime
from datetime import datetime

from odoo import api, fields, models, _
from odoo.exceptions import UserError, ValidationError, Warning
from odoo.osv import osv
from odoo.tools.translate import _


class WmsManifestOutbound(models.Model):
    _name = 'wms.manifest.outbound'
    _inherit = ['wms.manifest.outbound', 'mail.thread', 'mail.activity.mixin']


class WmsManifestProcess(models.Model):
    _name = 'wms.manifest.process'
    _inherit = ['wms.manifest.process', 'mail.thread', 'mail.activity.mixin']


class WmsManifestLine(models.Model):
    _inherit = "wms.manifest.line"

    sl_no = fields.Integer(string='Sl. No.', compute='_compute_serial_number', store=True)

    @api.depends('wms_manifest_id')
    def _compute_serial_number(self):
        for order_line in self:
            if not order_line.sl_no:
                serial_no = 1
                for line in order_line.mapped('wms_manifest_id').picking_line:
                    line.sl_no = serial_no
                    serial_no += 1

