from odoo import api, fields, models, _


class BulkOrderPayment(models.Model):
    _name = 'bulk.order.payment'
    _inherit = ['bulk.order.payment', 'mail.thread', 'mail.activity.mixin']


class WmsManifestLine(models.Model):
    _inherit = "bulk.order.payment.line"

    sl_no = fields.Integer(string='Sl. No.', compute='_compute_serial_number', store=True)

    @api.depends('bulk_order_payment_id')
    def _compute_serial_number(self):
        for order_line in self:
            if not order_line.sl_no:
                serial_no = 1
                for line in order_line.mapped('bulk_order_payment_id').bulk_order_payment_line:
                    line.sl_no = serial_no
                    serial_no += 1