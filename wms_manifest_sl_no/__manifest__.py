{
    'name': 'WMS Manifest Serial Number',
    'description': 'WMS Manifest Serial Number',
    'author': 'sindabad.com',
    'depends': [
        'wms_manifest',
        'wms_manifest_extends',
        'wms_manifest_inherit',
        'cash_collection',
        'mail'
    ],
    'data': [
        'views/wms_manifest_view.xml',
        'views/wms_outbound_view.xml',
        'views/serial_number_view.xml',
        'views/serial_no_bulk_payment_view.xml',
    ],

    'installable': True,
    'application': True,
    'auto_install': False,

}
