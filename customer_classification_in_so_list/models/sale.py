from odoo import fields, models,api


class SaleOrder(models.Model):
    _inherit = "sale.order"

    customer_classification = fields.Char(compute="_compute_customer_classification", string="Customer Classification",
                                          store=True)
    customer_industry = fields.Char(compute="_compute_customer_industry", string="Customer Industry", store=True)

    @api.depends('partner_id')
    def _compute_customer_classification(self):
        for sale in self.browse(self.ids):
            sale.customer_classification = sale.partner_id.x_classification

    @api.depends('partner_id')
    def _compute_customer_industry(self):
        for sale in self.browse(self.ids):
            sale.customer_industry = sale.partner_id.x_industry