from odoo import fields, models


class StockInvoiceOnshipping(models.Model):
    _inherit = "stock.invoice.onshipping"

    invoice_date = fields.Datetime('Invoice Date', default=fields.Datetime.now)

    ### Block code for V-14
    # _defaults = {
    #     "invoice_date": date.today().strftime("%Y-%m-%d"),
    # }
