{
    "name": "Default Invoice Date",
    "version": "14.0.1.0.0",
    "category": "Default Invoice Date",
    "author": "Odoo Bangladesh",
    "summary": "Stock",
    "description": "Default Invoice Date",
    "depends": ["base", "sale_management", "sale_stock", "stock", "stock_account"],
    "installable": True,
    "application": True,
    "auto_install": False,
}
