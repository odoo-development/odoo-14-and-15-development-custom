
from odoo import api, fields, models, _

class StockPicking(models.Model):
    _inherit = "stock.picking"

    partial_available = fields.Boolean(string="Partial Available")

    def partial_so_delivery_challan(self):
        return True


