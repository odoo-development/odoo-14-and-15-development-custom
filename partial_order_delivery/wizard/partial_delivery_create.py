import logging
from datetime import datetime
from odoo import api, models, fields
from odoo.osv import osv
from odoo.exceptions import Warning
from odoo.tools.float_utils import float_compare, float_round
from odoo.tools.translate import _

from odoo.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT, DATETIME_FORMATS_MAP

_logger = logging.getLogger(__name__)


class PartialDeliveryCreate(models.TransientModel):
    _name = "partial.delivery.create"
    _description = "Partial Delivery Creation"

    def _get_picking_in(self):
        obj_data = self.env['ir.model.data']
        type_obj = self.env['stock.picking.type']
        user_obj = self.env['res.users']
        company_id = user_obj.company_id.id
        types = type_obj.search([('code', '=', 'incoming'), ('warehouse_id.company_id', '=', company_id)])
        if not types:
            types = type_obj.search([('code', '=', 'incoming'), ('warehouse_id', '=', False)])
            if not types:
                raise Warning(_("Make sure you have at least an incoming picking type defined"))
        return types[0]

    def get_min_max_date(self):
        """ Finds minimum and maximum dates for picking.
        @return: Dictionary of values
        """
        res = {}
        for id in self:
            res[id] = {'min_date': False, 'max_date': False, 'priority': '1'}
        if not self:
            return res
        self.env.cr.execute("""select
                picking_id,
                min(date_expected),
                max(date_expected),
                max(priority)
            from
                stock_move
            where
                picking_id IN %s
            group by
                picking_id""", (tuple(self.ids),))
        for pick, dt1, dt2, prio in self.env.cr.fetchall():
            res[pick]['min_date'] = dt1
            res[pick]['max_date'] = dt2
            res[pick]['priority'] = prio
        return res

    def _set_min_date(self, value):
        move_obj = self.env["stock.move"]
        if value:
            move_ids = [move.id for move in self.line_ids]
            move_obj.write(move_ids, {'date_expected': value})

    def _get_pickings_dates_priority(self):
        res = set()
        for move in self:
            if move.picking_id and (not (
                    move.picking_id.min_date < move.date_expected < move.picking_id.max_date) or move.priority > move.picking_id.priority):
                res.add(move.picking_id.id)
        return list(res)

    def _state_get(self):
        '''The state of a picking depends on the state of its related stock.move
            draft: the picking has no line or any one of the lines is draft
            done, draft, cancel: all lines are done / draft / cancel
            confirmed, waiting, assigned, partially_available depends on move_type (all at once or partial)
        '''
        res = {}
        for pick in self:
            if (not pick.line_ids) or any([x.state == 'draft' for x in pick.line_ids]):
                res[pick.id] = 'draft'
                continue
            if all([x.state == 'cancel' for x in pick.line_ids]):
                res[pick.id] = 'cancel'
                continue
            if all([x.state in ('cancel', 'done') for x in pick.line_ids]):
                res[pick.id] = 'done'
                continue

            order = {'confirmed': 0, 'waiting': 1, 'assigned': 2}
            order_inv = {0: 'confirmed', 1: 'waiting', 2: 'assigned'}
            lst = [order[x.state] for x in pick.line_ids if x.state not in ('cancel', 'done')]
            if pick.move_type == 'one':
                res[pick.id] = order_inv[min(lst)]
            else:
                # we are in the case of partial delivery, so if all move are assigned, picking
                # should be assign too, else if one of the move is assigned, or partially available, picking should be
                # in partially available state, otherwise, picking is in waiting or confirmed state
                res[pick.id] = order_inv[max(lst)]
                if not all(x == 2 for x in lst):
                    if any(x == 2 for x in lst):
                        res[pick.id] = 'partially_available'
                    else:
                        # if all moves aren't assigned, check if we have one product partially available
                        for move in pick.line_ids:
                            if move.partially_available:
                                res[pick.id] = 'partially_available'
                                break
        return res

    def _get_pickings(self):
        res = set()
        for move in self:
            if move.picking_id:
                res.add(move.picking_id.id)
        return list(res)

    def _get_pack_operation_exist(self):
        res = {}
        for pick in self:
            res[pick.id] = False
            if pick.pack_operation_ids:
                res[pick.id] = True
        return res

    def _set_priority(self, value):
        move_obj = self.env["stock.move"]
        if value:
            move_ids = [move.id for move in self.line_ids]
            move_obj.write(move_ids, {'priority': value})

    def _get_quant_reserved_exist(self):
        res = {}
        for pick in self:
            res[pick.id] = False
            for move in pick.line_ids:
                if move.reserved_quant_ids:
                    res[pick.id] = True
                    continue
        return res

    # @api.onchange('partner_id')
    def onchange_partner_id(self):
        partner = self.env['res.partner']
        if not self.partner_id:
            return {}
        supplier = partner.browse(self.partner_id)
        return {'value': {
            'pricelist_id': supplier.property_product_pricelist_purchase.id,
        }}

    partner_id = fields.Many2one('res.partner', 'Partner',
                                 states={'done': [('readonly', True)], 'cancel': [('readonly', True)]})
    state = fields.Char('Status')
    date = fields.Datetime('Creation Date', help="Creation Date, usually the time of the order", index=True)

    min_date = fields.Datetime('Scheduled Date', help="Creation Date, usually the time of the order", index=True)
    origin = fields.Char('Source Document')

    magento_shipment = fields.Char('Magento Shipment', help="Contains Magento Order Shipment Number (eg. 300000008)")

    product_id = fields.Many2one('product.product', string='Product', related='line_ids.product_id')
    # move_lines = fields.One2many("partial.delivery.create.line", "partial_delivery_create_id", "Order Lines", required=True)
    line_ids = fields.One2many("partial.delivery.create.line", "move_line_id", "Order Lines",
                               required=True)

    # ## Block code for V-14
    # _columns = {
    #
    #     'partner_id': fields.many2one('res.partner', 'Partner',
    #                                   states={'done': [('readonly', True)], 'cancel': [('readonly', True)]}),
    #     'state': fields.char('Status'),
    #     'date': fields.datetime('Creation Date', help="Creation Date, usually the time of the order", index=True),
    #
    #     'min_date': fields.datetime('Scheduled Date', help="Creation Date, usually the time of the order", index=True),
    #     'origin': fields.char('Source Document'),
    #
    #     'magento_shipment': fields.char('Magento Shipment',
    #                                     help="Contains Magento Order Shipment Number (eg. 300000008)"),
    #
    #     'product_id': fields.related('move_lines', 'product_id', type='many2one', relation='product.product',
    #                                  string='Product'),
    #     'move_lines': fields.one2many('partial.delivery.create.line', 'move_line_id', 'Order Lines', required=True),
    #     # --------------------------------------------
    #
    # }

    def default_get(self, fields):
        product_pricelist = self.env['product.pricelist']
        # if context is None:
        #     context = {}
        res = super(PartialDeliveryCreate, self).default_get(fields)
        items = []
        date_order = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        picking_data = self.env['stock.picking'].browse(self._context['active_id'])

        for line in picking_data.move_lines:
            _logger.info('-------line-----------%r', line)

            # - determine price_unit and taxes_id

            price = 0.0
            if 'pricelist_id' in res:
                if res['pricelist_id']:
                    date_order_str = datetime.strptime(date_order, DEFAULT_SERVER_DATETIME_FORMAT).strftime(
                        DEFAULT_SERVER_DATE_FORMAT)
                    price = product_pricelist.price_get([res['pricelist_id']],
                                                        line.product_id.id, line.product_uom_qty or 1.0,
                                                        line.partner_id.id or False,
                                                        {'uom': line.product_id.uom_po_id.id, 'date': date_order_str})[
                        res['pricelist_id']]
                else:
                    price = line.product_id.standard_price
            else:
                price = line.product_id.standard_price

            item = {
                'product_id': line.product_id.id,
                'qty': line.product_uom_qty,
                # 'product_uom': line.product_id.uom_id.name,
                'price': price,
                'subtotal': price * line.product_uom_qty,
                'location_dest_id': line.location_dest_id.id,

            }

            if line.product_id:
                if line.state == "assigned":
                    item['state'] = 'Available'
                    items.append((0, 0, item))

        res['partner_id'] = picking_data[0].partner_id.id
        res['date'] = picking_data[0].date
        res['origin'] = picking_data[0].origin
        res['min_date'] = picking_data[0].scheduled_date
        # res['magento_shipment'] = picking_data[0].magento_shipment

        res.update(line_ids=items)

        return res

    def partial_order_delivery_challan(self):

        # saved_data = self.env['partial.delivery.create'].browse(self.ids)

        product_list = []

        for items in self.line_ids:
            product_list.append(items.product_id)

        stock_move_ids = []

        picking_data = self.env['stock.picking'].browse(self._context['active_id'])
        so_id = self.env['sale.order'].search([('name', '=', picking_data.origin)]).id

        avail_move_line_ids = list()
        for line in picking_data.move_lines:
            if line.state == "assigned":
                avail_move_line_ids.append(line.id)

                if line.product_id in product_list:
                    stock_move_ids.append(line)

        if stock_move_ids:
            if picking_data.carrier_id:
                carrier_id = picking_data.carrier_id.id
            else:
                carrier_id = None

            new_stock_picking = {
                'sale_id': so_id,
                'origin': picking_data.origin,
                'create_date': picking_data.create_date,
                'date_done': picking_data.date_done,
                'write_uid': picking_data.write_uid,
                'partner_id': picking_data.partner_id.id,
                'priority': picking_data.priority,
                'backorder_id': picking_data.backorder_id.id,
                'picking_type_id': picking_data.picking_type_id.id,
                # 'message_last_post': picking_data.message_last_post,
                'company_id': picking_data.company_id.id,
                'note': picking_data.note,
                'state': 'draft',
                'owner_id': picking_data.owner_id.id,
                'create_uid': picking_data.create_uid,
                'scheduled_date': picking_data.scheduled_date,
                'write_date': picking_data.write_date,
                'date': picking_data.date,
                # 'recompute_pack_op': picking_data.recompute_pack_op,
                # 'max_date': picking_data.max_date,
                'group_id': picking_data.group_id.id,
                'carrier_tracking_ref': picking_data.carrier_tracking_ref,
                # 'number_of_packages': picking_data.number_of_packages,
                'weight': picking_data.weight,
                'carrier_id': carrier_id,
                # 'weight_uom_id': picking_data.weight_uom_id.id,
                # 'weight_net': picking_data.weight_net,
                # 'volume': picking_data.volume,
                'carrier_code': picking_data.carrier_code,
                'packed': False,
                'picked': False,
                'location_id': picking_data.location_id.id,
                'location_dest_id': picking_data.location_dest_id.id,

            }

            new_stock_id = self.env['stock.picking'].create(new_stock_picking)

            if new_stock_id:
                picking_data.do_unreserve()

                for move in stock_move_ids:
                    move.state = "draft"
                    move.picking_id = new_stock_id.id

                new_stock_id.action_confirm()
                new_stock_id.action_assign()

            picking_data.action_assign()


class PartialDeliveryCreateLine(models.TransientModel):
    _name = "partial.delivery.create.line"
    _description = "Back to Back Order"

    def _amount_line(self):
        cur_obj = self.env['res.currency']
        tax_obj = self.env['account.tax']
        for line in self:
            taxes = tax_obj.compute_all(line.taxes_id, line.price, line.qty, line.product_id,
                                        line.back_order_id.partner_id)
            cur = line.back_order_id.pricelist_id.currency_id
            line.subtotal = cur_obj.round(cur, taxes['total'])

    def _get_string_qty_information(self):
        # settings_obj = self.env['stock.config.settings']   ai modules missing v-14
        # uom_obj = self.env['product.uom']  ai modules missing v-14
        res = dict.fromkeys(self.ids, '')
        precision = self.env['decimal.precision'].precision_get('Product Unit of Measure')
        for move in self:
            if move.state in ('draft', 'done', 'cancel') or move.location_id.usage != 'internal':
                res[move.id] = ''  # 'not applicable' or 'n/a' could work too
                continue
            total_available = min(move.product_qty, move.reserved_availability + move.availability)
            # total_available = uom_obj._compute_qty_obj(move.product_id.uom_id, total_available, move.product_uom, round=False)
            total_available = float_round(total_available, precision_digits=precision)
            info = str(total_available)
            # look in the settings if we need to display the UoM name or not
            # config_ids = settings_obj.search([], limit=1, order='id DESC')
            # if config_ids:
            #     stock_settings = settings_obj.browse(config_ids[0])
            #     if stock_settings.group_uom:
            #         info += ' ' + move.product_uom.name
            if move.reserved_availability:
                if move.reserved_availability != total_available:
                    # some of the available quantity is assigned and some are available but not reserved
                    # reserved_available = uom_obj._compute_qty_obj(move.product_id.uom_id, move.reserved_availability, move.product_uom, round=False)
                    reserved_available = float_round(reserved_available, precision_digits=precision)
                    info += _(' (%s reserved)') % str(reserved_available)
                else:
                    # all available quantity is assigned
                    info += _(' (reserved)')
            move.string_availability_info = info

    move_line_id = fields.Many2one('partial.delivery.create', 'Move Line', required=True)
    product_id = fields.Many2one('product.product', 'Product')
    partial_delivery_create_id = fields.Many2one('partial.delivery.create', 'Back Order')
    qty = fields.Float('Quantity')
    price = fields.Float('Unit Price')
    subtotal = fields.Float(compute='_amount_line', string='Subtotal', digits=0)
    taxes_id = fields.Many2many('account.tax', 'purchase_order_tax', 'ord_id', 'tax_id', 'Taxes')
    product_uom = fields.Many2one('product.uom', 'Product Unit of Measure')
    location_dest_id = fields.Many2one('stock.location', 'Destination Location', states={'done': [('readonly', True)]},
                                       index=True, auto_join=True,
                                       help="Location where the system will stock the finished products.")
    string_availability_info = fields.Text(compute='_get_string_qty_information', string='Availability', readonly=True,
                                           help='Show various information on stock availability for this move')
    state = fields.Char('Status', readonly=True)

#### Block codee for V-14
#
#     _columns = {
#
# #        'location_destination_id': fields.many2one('stock.location', 'Stock Destination Location'),
# #        'location_id': fields.many2one('stock.location', 'Stock Source Location'),
#         'move_line_id': fields.many2one('partial.delivery.create', 'Move Line', required=True),
#         'product_id': fields.many2one('product.product', 'Product'),
#         'partial_delivery_create_id': fields.many2one('partial.delivery.create', 'Back Order'),
#         'qty': fields.float('Quantity'),
#         'price': fields.float('Unit Price'),
#         'subtotal': fields.function(_amount_line, string='Subtotal', digits_compute= dp.get_precision('Account')),
#         'taxes_id': fields.many2many('account.tax', 'purchase_order_taxe', 'ord_id', 'tax_id', 'Taxes'),
#         'product_uom': fields.many2one('product.uom', 'Product Unit of Measure'),
#         'location_dest_id': fields.many2one('stock.location', 'Destination Location',
#                                             states={'done': [('readonly', True)]}, index=True,
#                                             auto_join=True,
#                                             help="Location where the system will stock the finished products."),
#         'string_availability_info': fields.function(_get_string_qty_information, type='text', string='Availability',
#                                                     readonly=True,
#                                                     help='Show various information on stock availability for this move'),
#         'state': fields.char('Status', readonly=True),
#
#
#     }
