from odoo import api, fields, models
from odoo.osv import osv
from odoo.tools.translate import _
from odoo.exceptions import UserError, ValidationError, Warning


class IrnControl(models.Model):
    _name = "irn.control"
    _description = "IRN Control"
    _order = 'id desc'
    _rec_name = "irn_number"

    scan = fields.Char('PO Scan')
    product_scan = fields.Char('Product Scan')
    warehouse_id = fields.Many2one('stock.warehouse', string='Warehouse Location', required=True)
    irn_number = fields.Char('IRN Number')
    po_number = fields.Char('PO Number')
    po_id = fields.Integer('Purchase ID')
    remark = fields.Text('Remark')
    received_by = fields.Many2one('res.users', 'Received By', default=lambda self: self.env.uid)
    qc_pushed = fields.Boolean('QC Pushed')
    date_confirm = fields.Datetime('Confirmation Date', readonly=True, select=False)
    state = fields.Selection([
        ('full_received', 'Full Received'),
        ('partial_received', 'Partial Received'),
    ], 'Status', readonly=True, copy=False, help="Gives the status of the IRN control", index=True)
    irn_line = fields.One2many('irn.control.line', 'irn_line_id', 'IRN Line', required=True)

    #
    #
    # _columns = {
    #     'scan': fields.char('PO Scan'),
    #     'product_scan': fields.char('Product Scan'),
    #     'warehouse_id': fields.many2one('stock.warehouse', string='Warehouse Location', required=True),
    #     'irn_number': fields.char('IRN Number'),
    #     'po_number': fields.char('PO Number'),
    #     'po_id': fields.integer('Purchase ID'),
    #     'remark': fields.text('Remark'),
    #     'received_by': fields.many2one('res.users', 'Received By'),
    #     'qc_pushed': fields.boolean('QC Pushed'),
    #     'date_confirm': fields.datetime('Confirmation Date', readonly=True, select=False),
    #     'state': fields.selection([
    #         ('full_received', 'Full Received'),
    #         ('partial_received', 'Partial Received'),
    #
    #     ], 'Status', readonly=True, copy=False, help="Gives the status of the IRN control", index=True),
    #     'irn_line': fields.one2many('irn.control.line', 'irn_line_id', 'IRN Line', required=True),
    # }
    #
    # _defaults = {
    #     'user_id': lambda obj, cr, uid, context: uid,
    #     'received_by': lambda obj, cr, uid, context: uid,
    # }

    def confirm_wms_inbound_irn(self):
        self.ensure_one()
        if not self.qc_pushed:
            qc_data = {
                'irn_number': self.irn_number,
                'irn_id': self.id,
                'warehouse_id': self.warehouse_id.id,
                'po_number': self.po_number,
                'po_id': self.po_id,
                'remark': self.remark,
                'received_by': self.received_by.id,
            }

            qc_line_list = list()

            for line_item in self.irn_line:
                qc_line_list.append([0, False,
                                     {
                                         'product': line_item.product,
                                         'product_id': line_item.product_id.id,
                                         'product_ean': line_item.product_ean if line_item.product_ean else "",
                                         'product_weight': line_item.product_weight if line_item.product_weight else "",
                                         'product_size': line_item.product_size if line_item.product_size else "",
                                         'description': line_item.remark if line_item.remark else "",
                                         'purchase_quantity': line_item.ordered_quantity,
                                         'received_quantity': line_item.received_quantity,
                                         'accepted_quantity': 0,
                                         'pending_quantity': line_item.received_quantity,
                                     }])

            qc_data['qc_line'] = qc_line_list

            saved_qc_id = self.env['quality.control'].create(qc_data)

            values = {'qc_pushed': True,
                      'date_confirm': fields.Datetime.now()}
            self.write(values)
            self.irn_line.write({'qc_pushed': True})
            po = self.env['purchase.order'].browse([self.po_id])
            po.write({'irn_created': True})

            # update irn end-to-end
            self.env['inb.end.to.end'].irn_confirm_update(po_number=self.po_number,
                                                          po_id=self.po_id)

        return True

    def _check_if_already_irned(self, po_number):
        # need to check on the po already IRNed

        irn_state = 'new'
        for irn in self.search([('po_number', '=', po_number)]):
            if str(irn.state) == 'full_received':
                irn_state = 'full_received'
                break
            elif str(irn.state) == 'partial_received':
                irn_state = 'partial_received'
            else:
                irn_state = 'new'

        return irn_state

    def _po_confirmation(self, po):
        po_obj = self.env['purchase.order'].search([('name', '=', po)])
        po_state = po_obj.state if po_obj else False

        return po_state

    def _check_full_or_partial_received(self):
        full_or_partial_rec = False

        for line_item in self.irn_line:
            # line_received_qty = self._check_irn_line_received_quantity(line_item.product_id)
            if line_item.ordered_quantity != line_item.received_quantity:
                full_or_partial_rec = False
                break
            else:
                full_or_partial_rec = True

        return full_or_partial_rec

    # # -------------------------------------------------------------
    def _process_partial_received_order(self, po_number):

        partial_received_product_id_list = list()
        partial_rec_prod_list = list()

        irn_obj = self.search([('po_number', '=', po_number), ('state', '=', 'partial_received')], limit=1)

        for irn in irn_obj:
            for product in irn.irn_line:
                if product.state == 'partially_received' and product.product_id not in partial_received_product_id_list:
                    partial_received_product_id_list.append(product.product_id)

        for pr_product_id in partial_received_product_id_list:
            pr_product = self._process_partial_received_product(po_number, pr_product_id.id)
            if len(pr_product.keys()) > 0:
                partial_rec_prod_list.append([0, False, pr_product])

        # return list of dictionary
        return partial_rec_prod_list

    def _process_partial_received_product(self, po_number, product_id):

        irn_obj = self.search([('po_number', '=', po_number)])
        irn_line_env = self.env['irn.control.line']
        product_received_quantity = self._check_irn_line_received_quantity(po_number, product_id)
        product_name = ''
        ordered_quantity = 0

        ###########################################
        for irn in irn_obj:
            irn_line_obj = irn_line_env.search([('irn_line_id', '=', irn.id), ('product_id', '=', product_id)])
            for product in irn_line_obj:
                product_name = product.product
                # product_id = product_id
                ordered_quantity = product.ordered_quantity
        ###########################################

        if ordered_quantity > product_received_quantity:

            product_env = self.env['product.product']
            product_obj = product_env.search([('id', '=', product_id)])

            product_dict = {
                'product': product_name,
                'product_id': product_id,
                'product_ean': str(product_obj.barcode) if product_obj.barcode else '',
                'product_weight': float(product_obj.weight) if product_obj.weight ==0 or product_obj.weight<0 else '',
                'product_size': float(product_obj.volume) if product_obj.volume == 0 or product_obj.volume < 0 else '',
                'ordered_quantity': ordered_quantity,
                'received_quantity': ordered_quantity - product_received_quantity,
                'remark': ''
            }
            return product_dict
        else:
            return {}

    @api.onchange('scan')
    def po_irn_barcode_onchange(self):
    # try:
        self.ensure_one()
        self.irn_line = None
        po_number = str(self.scan)
        if self.scan:
            irn_state = self._check_if_already_irned(po_number)
            po_state = self._po_confirmation(po_number)

            # if po_state == 'cancel' or po_state == 'draft':
            if po_state and po_state in ['cancel', 'draft', 'bid', 'category_head_approval_pending', 'category_vp_approval_pending', 'coo_approval_pending', 'ceo_approval_pending']:
                raise Warning(_('This PO is in Draft/Approval Pending/Cancel mode'))
            else:

                if irn_state == 'full_received':
                    # raise exception
                    raise Warning(_('IRN already done for this PO!!!'))

                    self.scan = ""
                elif irn_state == 'partial_received':
                    # load only left products
                    # search in irn_control with po_number

                    self.irn_line = self._process_partial_received_order(po_number)
                    self.po_number = po_number
                    po_env = self.env['purchase.order']
                    po_obj = po_env.search([('name', '=', po_number)])
                    self.warehouse_id = po_obj.picking_type_id.warehouse_id.id
                    self.scan = ""

                else:

                    self.po_number = po_number

                    # po_env = self.env['purchase.order']
                    po_obj = self.env['purchase.order'].search([('name', '=', po_number)])

                    # po_line_env = self.env['purchase.order.line']
                    # po_line_obj = po_line_env.search([('order_id', '=', po_obj.id)])

                    self.warehouse_id = po_obj.picking_type_id.warehouse_id

                    po_line_list = []

                    for po_line in po_obj.order_line:
                        po_line_list.append([0, 0, {
                            'product': po_line.name,
                            'product_id': po_line.product_id.id,
                            'product_ean': str(po_line.product_id.barcode) if po_line.product_id.barcode else '',
                            'product_weight': float(po_line.product_id.weight) if po_line.product_id.weight else '',
                            'product_size': float(po_line.product_id.volume) if po_line.product_id.volume else '',
                            'ordered_quantity': po_line.product_qty,
                            'received_quantity': 0,
                            'remark': ''
                        }])

                    self.irn_line = po_line_list
                    self.scan = ""


        # except Exception as e:
        #     self.scan = ""


    def _check_irn_line_received_quantity(self, po_number, product_id):

        received_quantity = 0

        irn_env = self.env['irn.control']
        irn_obj = irn_env.search([('po_number', '=', po_number)])

        irn_line_env = self.env['irn.control.line']

        for irn in irn_obj:
            irn_line_obj = irn_line_env.search([('irn_line_id', '=', irn.id), ('product_id', '=', product_id)])
            for irn_line in irn_line_obj:
                received_quantity += irn_line.received_quantity

        return received_quantity

    #
    def _set_irn_line_state(self):
        for line_item in self.irn_line:
            # line_received_qty = self._check_irn_line_received_quantity(line_item.product_id)
            if line_item.ordered_quantity == line_item.received_quantity:
                line_item.state = 'fully_received'
            else:
                line_item.state = 'partially_received'

        # return record

    #
    def _set_po_id_in_line(self):
        for line_item in self.irn_line:
            line_item.po_id = self.po_id

        # return record

    #
    @api.model
    def create(self, vals):
        # generate IRN number
        # IRN0123456

        record = super(IrnControl, self).create(vals)
        # irn_number = "IRN0" + str(record.id)
        po_obj = self.env['purchase.order'].search([('name', '=', record.po_number)])

        record.irn_number = "IRN0" + str(record.id)
        record.po_id = po_obj.id
        record.state = 'full_received' if record._check_full_or_partial_received() else 'partial_received'

        record._set_irn_line_state()
        record._set_po_id_in_line()

        # ---------------------------
        create_permission = list()

        if vals.get('irn_line', False):
            for single_line in vals['irn_line']:

                if not not single_line[2]:

                    if single_line[2]['received_quantity'] > single_line[2]['ordered_quantity']:
                        create_permission.append(False)
                    else:
                        create_permission.append(True)


        # if vals.get('irn_line', False):
        # for single_line in record.irn_line:
        #     if single_line.received_quantity > single_line.ordered_quantity:
        #         create_permission.append(False)
        #     else:
        #         create_permission.append(True)

        if False in create_permission:
            raise ValidationError(_('IRN line adjustment ERROR!   '
                                    'Received quantity should be less then or equal to (requested quantity)!!!'))

        else:
            # self.env['inb.end.to.end'].irn_data_create(record)
            self.env['inb.end.to.end'].irn_data_create(po_number=record.po_number,
                                                       po_id=record.po_id,
                                                       irn_id=record.id,
                                                       irn_number=record.irn_number,
                                                       irn_date_create=record.create_date)
            # ---------------------------

        return record

    #
    # # calls at the time of update record
    def write(self, vals):

        # only update only if (received_quantity less or equal to ordered_quantity)
        update_permission = list()

        if vals.get('irn_line', False):
            for single_line in vals['irn_line']:
                if not not single_line[2]:
                    for irn_l in self.irn_line:

                        if irn_l.id == single_line[1]:

                            received_quantity = single_line[2]['received_quantity'] if single_line[2].get(
                                'received_quantity') else irn_l.received_quantity
                            if received_quantity < irn_l.ordered_quantity:
                                vals['state'] = 'partial_received'
                                single_line[2]['state'] = 'partially_received'
                            else:
                                vals['state'] = 'full_received'
                                single_line[2]['state'] = 'fully_received'

                            if received_quantity > irn_l.ordered_quantity:
                                update_permission.append(False)
                            else:
                                update_permission.append(True)

        if False in update_permission:
            raise Warning(_('Received quantity should be less then or equal to (ordered quantity)!!!'))

        record = super(IrnControl, self).write(vals)

        return record

class IrnControlLine(models.Model):
    _name = "irn.control.line"
    _description = "IRN Control Line"

    irn_line_id = fields.Many2one('irn.control', 'IRN Line ID', required=True,
                                  ondelete='cascade', index=True)
    po_id = fields.Many2one('purchase.order', 'PO ID', required=False,
                            ondelete='cascade', index=True)
    product = fields.Char('Product Name')
    product_id = fields.Many2one('product.product', string='Product')
    product_ean = fields.Char('Product EAN')
    ordered_quantity = fields.Float('Ordered Qty')
    received_quantity = fields.Float('Received Qty')
    remark = fields.Char('Remark')
    qc_pushed = fields.Boolean('QC Pushed')
    state = fields.Selection([
        ('fully_received', 'Fully Received'),
        ('partially_received', 'Partially Received'),
        ('cancel', 'Cancelled')
    ], 'Status', help="Gives the status of the IRN control line", index=True)
    is_ean = fields.Boolean("Is EAN?", default=False)

    ####Block code for V-14
    #
    # _columns = {
    #     'irn_line_id': fields.many2one('irn.control', 'IRN Line ID', required=True,
    #                                   ondelete='cascade', index=True, readonly=True),
    #     'po_id': fields.many2one('purchase.order', 'PO ID', required=False,
    #                              ondelete='cascade', index=True, readonly=True),
    #     'product': fields.char('Product', readonly=True),
    #     'product_id': fields.integer('Product ID'),
    #     'product_ean': fields.char('Product EAN'),
    #     'ordered_quantity': fields.float('Ordered Qty', readonly=True),
    #     'received_quantity': fields.float('Received Qty'),
    #     'remark': fields.char('Remark'),
    #     'qc_pushed': fields.boolean('QC Pushed'),
    #     'state': fields.selection([
    #         ('fully_received', 'Fully Received'),
    #         ('partially_received', 'Partially Received'),
    #         ('cancel', 'Cancelled'),
    #     ], 'Status', help="Gives the status of the IRN control line", index=True),
    # }


class IrnWithEan(models.Model):
    _name = "irn.with.ean"
    _description = "IRN With EAN"

    product = fields.Char('Product', readonly=True)
    product_id = fields.Integer('Product ID')
    product_ean = fields.Char('EAN')

    #### Block code for V-14
    # _columns = {
    #     'product': fields.char('Product', readonly=True),
    #     'product_id': fields.integer('Product ID'),
    #     'product_ean': fields.char('EAN'),
    #
    # }

    def save_irn_with_product_ean(self):
        context = None
        product_id = self.env.context['product_id']
        product_ean = self.env.context['product_ean']

        if len(product_ean) > 13 or " " in product_ean:
            # show error. not a valid ean
            raise Warning(_('Not a valid EAN number!!!'))
        else:
            for pp in self.env['product.product'].browse(product_id):
                pp.write({'barcode': product_ean})

            # for icl in self.env['irn.control.line'].browse(product_id):
            #     icl.write({'product_ean': product_ean})

            # ean_update_query = "UPDATE product_product SET ean13='{0}' WHERE id='{1}'".format(product_ean, product_id)
            # self.env.cr.execute(ean_update_query)
            # self.env.cr.commit()
            #
            irn_line_query = "UPDATE irn_control_line SET is_ean=TRUE, product_ean='{0}' WHERE product_id='{1}'".format(product_ean,
                                                                                                           product_id)
            self.env.cr.execute(irn_line_query)
            self.env.cr.commit()

        # return True
        return {
            'type': 'ir.actions.client',
            'tag': 'reload',
        }

    def cancel_irn_with_product_ean(self):
        return {
            'type': 'ir.actions.client',
            'tag': 'reload',
        }
