from odoo import fields, models


class PurchaseOrder(models.Model):
    _inherit = 'purchase.order'

    irn_line = fields.One2many('irn.control.line', 'po_id', 'IRN Line', required=False)
    qc_line = fields.One2many('quality.control.line', 'po_id', 'QC Line', required=False)

    # ## Block code for V-14
    # _columns = {
    #     'irn_line': fields.one2many('irn.control.line', 'po_id', 'IRN Line', required=False),
    #     'qc_line': fields.one2many('quality.control.line', 'po_id', 'QC Line', required=False),
    #
    # }
