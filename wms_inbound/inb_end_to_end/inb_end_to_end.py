from odoo import fields, models


class InbEndToEnd(models.Model):
    _name = "inb.end.to.end"
    _description = "Inbound End-to-End"

    po_number = fields.Char('PO Number')
    po_id = fields.Integer('Purchase ID')
    irn_id = fields.Char('IRN IDs')
    irn_number = fields.Char('IRN Numbers')
    irn_date_create = fields.Text('IRN Created Date')
    irn_date_confirm = fields.Text('IRN Confirm Date')
    qc_id = fields.Char('QC IDs')
    qc_number = fields.Char('QC Numbers')
    qc_date_create = fields.Text('QC Created Date')
    qc_date_confirm = fields.Text('QC Confirm Date')
    aqc_id = fields.Char('AQC IDs')
    aqc_number = fields.Char('AQC Number')
    aqc_date_create = fields.Text('AQC Created Date')
    aqc_date_confirm = fields.Text('AQC Confirm Date')
    pqc_id = fields.Char('PQC IDs')
    pqc_number = fields.Char('PQC Number')
    pqc_date_create = fields.Text('PQC Created Date')
    pqc_date_confirm = fields.Text('PQC Confirm Date')
    putaway_id = fields.Char('Putaway IDs')
    putaway_number = fields.Char('Putaway Number')
    putaway_date_create = fields.Text('Putaway Created Date')
    putaway_date_confirm = fields.Text('Putaway Confirm Date')

    # ## Block code for V-14
    # _columns = {
    #     'po_number': fields.char('PO Number'),
    #     'po_id': fields.integer('Purchase ID'),
    #     'irn_id': fields.char('IRN IDs'),
    #     'irn_number': fields.char('IRN Numbers'),
    #     'irn_date_create': fields.text('IRN Created Date'),
    #     'irn_date_confirm': fields.text('IRN Confirm Date'),
    #     'qc_id': fields.char('QC IDs'),
    #     'qc_number': fields.char('QC Numbers'),
    #     'qc_date_create': fields.text('QC Created Date'),
    #     'qc_date_confirm': fields.text('QC Confirm Date'),
    #     'aqc_id': fields.char('AQC IDs'),
    #     'aqc_number': fields.char('AQC Number'),
    #     'aqc_date_create': fields.text('AQC Created Date'),
    #     'aqc_date_confirm': fields.text('AQC Confirm Date'),
    #     'pqc_id': fields.char('PQC IDs'),
    #     'pqc_number': fields.char('PQC Number'),
    #     'pqc_date_create': fields.text('PQC Created Date'),
    #     'pqc_date_confirm': fields.text('PQC Confirm Date'),
    #     'putaway_id': fields.char('Putaway IDs'),
    #     'putaway_number': fields.char('Putaway Number'),
    #     'putaway_date_create': fields.text('Putaway Created Date'),
    #     'putaway_date_confirm': fields.text('Putaway Confirm Date'),
    # }

    def irn_data_create(self, po_number, po_id, irn_id=None, irn_number=None, irn_date_create=None):
        # check po_number and po_id already exists
        inb_etoe_id = self.search([('po_number', '=', po_number), ('po_id', '=', po_id)])

        if len(inb_etoe_id) == 0:
            created_id = self.create({
                'po_number': po_number,
                'po_id': po_id,
                'irn_id': irn_id,
                'irn_number': irn_number,
                'irn_date_create': irn_date_create,
            })
        else:
            # inb_etoe_id = self.search([('po_number', '=', self.po_number), ('po_id', '=', self.po_id)])

            # browse and then update
            for etoe in inb_etoe_id:
                etoe.write({
                    'irn_id': (str(etoe.irn_id) + "," + str(irn_id)) if etoe.irn_id else str(irn_id),
                    'irn_number': (str(etoe.irn_number) + "," + str(irn_number)) if etoe.irn_number else str(irn_number),
                    'irn_date_create': (str(etoe.irn_date_create) + "," + str(irn_date_create)) if etoe.irn_date_create else str(irn_date_create),
                })

        # return True

    def irn_confirm_update(self, po_number, po_id):

        inb_etoe_ids = self.search([('po_number', '=', po_number), ('po_id', '=', po_id)])
        if len(inb_etoe_ids) > 0:
            # browse and then update
            for etoe in inb_etoe_ids:
                etoe.write({
                    'irn_date_confirm': (str(etoe.irn_date_confirm) + "," + str(fields.Datetime.now()))
                    if etoe.irn_date_confirm else str(fields.Datetime.now()),
                })

        # return True

    def qc_data_create(self, po_number=None, po_id=None, qc_id=None, qc_number=None, qc_date_create=None):
        inb_etoe_ids = self.search([('po_number', '=', po_number), ('po_id', '=', po_id)])
        if len(inb_etoe_ids) > 0:
            # browse and then update
            for etoe in inb_etoe_ids:
                if etoe.qc_id:
                    qc_id = etoe.qc_id if qc_id is None else str(etoe.qc_id) + "," + str(qc_id)
                else:
                    qc_id = etoe.qc_id if qc_id is None else str(etoe.qc_id)

                if etoe.qc_number:
                    qc_number = etoe.qc_number if qc_number is None else str(etoe.qc_number) + "," + str(qc_number)
                else:
                    qc_number = etoe.qc_number if qc_number is None else str(etoe.qc_number)

                if etoe.qc_date_create:
                    qc_date_create = etoe.qc_date_create if qc_date_create is None else str(etoe.qc_date_create) + "," + str(qc_date_create)
                else:
                    qc_date_create = etoe.qc_date_create if qc_date_create is None else str(etoe.qc_date_create)

                values = {'qc_id': qc_id,
                          'qc_number': qc_number,
                          'qc_date_create': qc_date_create
                          }
                etoe.write(values)
        else:
            values = {'po_number': po_number,
                      'po_id': po_id,
                      'qc_id': qc_id,
                      'qc_number': qc_number,
                      'qc_date_create': qc_date_create
                      }
            self.create(values)

        # return True

    def qc_confirm_update(self, po_number=None, po_id=None):
        inb_etoe_ids = self.search([('po_number', '=', po_number), ('po_id', '=', po_id)])
        if len(inb_etoe_ids) > 0:
            # browse and then update
            for etoe in inb_etoe_ids:
                etoe.write({
                    'qc_date_confirm': str(etoe.qc_date_confirm) + "," + str(fields.Datetime.now())
                    if etoe.qc_date_confirm else str(fields.Datetime.now()),
                })

        # return True

    def aqc_data_create(self, po_number, po_id, aqc_id=None, aqc_number=None, aqc_date_create=None):
        inb_etoe_id = self.search([('po_number', '=', po_number), ('po_id', '=', po_id)])
        if len(inb_etoe_id) > 0:
            # browse and then update
            for etoe in inb_etoe_id:
                etoe.write({
                    'aqc_id': str(etoe.aqc_id) + "," + str(aqc_id) if etoe.aqc_id else str(aqc_id),
                    'aqc_number': (str(etoe.aqc_number) + "," + aqc_number) if etoe.aqc_number else aqc_number,
                    'aqc_date_create': str(etoe.aqc_date_create) + "," + str(aqc_date_create)
                    if etoe.aqc_date_create else str(aqc_date_create)
                })

        # return True

    def aqc_confirm_update(self, po_number, po_id):

        inb_etoe_id = self.search([('po_number', '=', po_number), ('po_id', '=', po_id)])
        if len(inb_etoe_id) > 0:
            # browse and then update
            for etoe in inb_etoe_id:
                etoe.write({
                    'aqc_date_confirm': (str(etoe.aqc_date_confirm) + "," + str(fields.Datetime.now()))
                    if etoe.aqc_date_confirm else str(fields.Datetime.now()),
                })

        # return True

    def pqc_data_create(self, po_number, po_id, pqc_id=None, pqc_number=None, pqc_date_create=None):
        inb_etoe_id = self.search([('po_number', '=', po_number), ('po_id', '=', po_id)])
        if len(inb_etoe_id) > 0:
            # browse and then update
            for etoe in inb_etoe_id:
                etoe.write({
                    'pqc_id': (str(etoe.pqc_id) + "," + str(pqc_id)) if etoe.pqc_id else str(pqc_id),
                    'pqc_number': (str(etoe.pqc_number) + "," + str(pqc_number)) if etoe.pqc_number else pqc_number,
                    'pqc_date_create': (str(etoe.pqc_date_create) + "," + str(pqc_date_create)) if etoe.pqc_date_create else str(pqc_id),
                })

        # return True

    def pqc_confirm_update(self, po_number, po_id):

        inb_etoe_id = self.search([('po_number', '=', po_number), ('po_id', '=', po_id)])
        if len(inb_etoe_id) > 0:
            # browse and then update
            for etoe in inb_etoe_id:
                etoe.write({
                    'pqc_date_confirm': (str(etoe.pqc_date_confirm) + "," + str(fields.Datetime.now()))
                    if etoe.pqc_date_confirm else str(fields.Datetime.now()),
                })

    def putaway_data_create(self, po_number, po_id, putaway_id=None, putaway_number=None, putaway_date_create=None):
        inb_etoe_id = self.search([('po_number', '=', po_number), ('po_id', '=', po_id)])
        if len(inb_etoe_id) > 0:
            # browse and then update
            for etoe in inb_etoe_id:
                etoe.write({
                    'putaway_id': (str(etoe.putaway_id) + "," + str(putaway_id)) if etoe.putaway_id else str(putaway_id),
                    'putaway_number': (str(etoe.putaway_number) + "," + str(putaway_number)) if etoe.putaway_number else putaway_number,
                    'putaway_date_create': (str(etoe.putaway_date_create) + "," + str(putaway_date_create))
                    if etoe.putaway_date_create else str(putaway_date_create),
                })

    def putaway_confirm_update(self, po_number, po_id):
        inb_etoe_id = self.search([('po_number', '=', po_number), ('po_id', '=', po_id)])
        if len(inb_etoe_id) > 0:
            # browse and then update
            for etoe in inb_etoe_id:
                self.write({
                    'putaway_date_confirm': str(etoe.putaway_date_confirm) + "," + str(fields.Datetime.now())
                    if etoe.putaway_date_confirm else str(fields.Datetime.now())
                })
