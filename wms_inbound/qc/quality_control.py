from odoo import api, fields, models
from odoo.osv import osv
from odoo.tools.translate import _
from odoo.exceptions import Warning


class QualityControl(models.Model):
    _name = "quality.control"
    _description = "Quality Control"
    _order = 'id desc'
    _rec_name = "qc_number"

    scan = fields.Char('Scan')
    product_scan = fields.Char('Product Scan')
    warehouse_id = fields.Many2one('stock.warehouse', string='Warehouse Location', required=True)
    qc_number = fields.Char('QC Number')
    irn_number = fields.Char('IRN Number')
    irn_id = fields.Integer('IRN ID')
    po_number = fields.Char('PO Number')
    po_id = fields.Integer('Purchase ID')
    remark = fields.Text('Remark')
    received_by = fields.Many2one('res.users', 'Received By', default=lambda self: self.env.user)
    apqc_pushed = fields.Boolean('A/P QC Pushed')
    date_confirm = fields.Datetime('Confirmation Date', readonly=True, select=False)
    state = fields.Selection([
        ('pending', 'Pending'),
        ('confirmed', 'Confirmed'),
        ('cancel', 'Cancelled'),
    ], 'Status', default='pending', readonly=True, copy=False, help="Gives the status of the WMS inbound", index=True)
    qc_line = fields.One2many('quality.control.line', 'qc_line_id', 'QC Line', required=True)
    aqc_line = fields.One2many('accepted.quality.control.line', 'qc_id', 'AQC Line', required=False)
    pqc_line = fields.One2many('pending.quality.control.line', 'qc_id', 'PQC Line', required=False)

    # ##Block code V-14
    # _columns = {
    #     'scan': fields.char('Scan'),
    #     'product_scan': fields.char('Product Scan'),
    #     'warehouse_id': fields.many2one('stock.warehouse', string='Warehouse Location', required=True),
    #     'qc_number': fields.char('QC Number'),
    #     'irn_number': fields.char('IRN Number'),
    #     'irn_id': fields.integer('IRN ID'),
    #     'po_number': fields.char('PO Number'),
    #     'po_id': fields.integer('Purchase ID'),
    #     'remark': fields.text('Remark'),
    #     'received_by': fields.many2one('res.users', 'Received By'),
    #     'apqc_pushed': fields.boolean('A/P QC Pushed'),
    #     'date_confirm': fields.datetime('Confirmation Date', readonly=True, select=False),
    #     'state': fields.selection([
    #         ('pending', 'Pending'),
    #         ('confirmed', 'Confirmed'),
    #         ('cancel', 'Cancelled'),
    #
    #     ], 'Status', readonly=True, copy=False, help="Gives the status of the WMS inbound", index=True),
    #     'qc_line': fields.one2many('quality.control.line', 'qc_line_id', 'QC Line', required=True),
    #     'aqc_line': fields.one2many('accepted.quality.control.line', 'qc_id', 'AQC Line', required=False),
    #     'pqc_line': fields.one2many('pending.quality.control.line', 'qc_id', 'PQC Line', required=False),
    # }
    #
    # _defaults = {
    #     'user_id': lambda obj, cr, uid, context: uid,
    #     'received_by': lambda obj, cr, uid, context: uid,
    #     'state': 'pending',
    # }

    def _set_po_id_in_line(self):
        for line_item in self.qc_line:
            line_item.po_id = self.po_id

        # return record

    # calls at the time of create new record
    @api.model
    def create(self, vals):
        # generate QC number
        # QC0123456

        record = super(QualityControl, self).create(vals)

        qc_number = "QC0" + str(record.id)
        record.qc_number = qc_number
        record._set_po_id_in_line()

        self.env['inb.end.to.end'].qc_data_create(po_number=record.po_number,
                                                  po_id=record.po_id,
                                                  qc_id=record.id,
                                                  qc_number=qc_number,
                                                  qc_date_create=record.create_date)

        return record

    # calls at the time of update record
    def write(self, vals):

        # only update only if (received_quantity = accepted_quantity + pending_quantity)
        # qc_env = self.env['quality.control']
        # qc_obj = qc_env.browse(self._cr, self._uid, [self.id], context=self._context)
        update_permission = list()

        if vals.get('qc_line', False):
            for single_line in vals['qc_line']:
                if not not single_line[2]:
                    for qc_l in self.qc_line:
                        if qc_l.id == single_line[1]:
                            accepted_quantity = single_line[2]['accepted_quantity'] if single_line[2].get(
                                'accepted_quantity') else qc_l.accepted_quantity
                            pending_quantity = single_line[2]['pending_quantity'] if single_line[2].get(
                                'pending_quantity') else 0

                            pending_quantity = pending_quantity * -1 if pending_quantity < 0 else pending_quantity

                            if qc_l.received_quantity == accepted_quantity + pending_quantity:
                                update_permission.append(True)
                            else:
                                update_permission.append(False)

        if False in update_permission:
            raise Warning('Received quantity should be equal to (accepted quantity + pending quantity)!!!')
        else:
            record = super(QualityControl, self).write(vals)

        # return record

    def confirm_wms_inbound_qc(self):
        self.ensure_one()
        # qc_env = self.env['quality.control']
        # qc_obj = qc_env.browse()

        # 1. AQC will create with accepted quantity
        put_data = {
            'qc_number': self.qc_number,
            'qc_id': self.id,
            'warehouse_id': self.warehouse_id.id,
            'irn_number': self.irn_number,
            'irn_id': self.irn_id,
            'po_number': self.po_number,
            'po_id': self.po_id,
            'remark': self.remark,
            'received_by': self.received_by.id,
        }

        aqc_line_list = list()

        for aqc_line_item in self.qc_line:
            if aqc_line_item.accepted_quantity > 0:
                aqc_line_list.append([0, False,
                                      {
                                          'qc_id': self.id,
                                          'product': aqc_line_item.product,
                                          'product_id': aqc_line_item.product_id,
                                          'product_ean': aqc_line_item.product_ean,
                                          'description': aqc_line_item.description,
                                          'purchase_quantity': aqc_line_item.purchase_quantity,
                                          'received_quantity': aqc_line_item.received_quantity,
                                          'accepted_quantity': aqc_line_item.accepted_quantity,
                                          'pending_quantity': aqc_line_item.pending_quantity,
                                          # 'product_exp_date': aqc_line_item.product_exp_date,
                                      }])

        put_data['aqc_line'] = aqc_line_list

        if len(aqc_line_list) > 0:
            # Create aqc
            aqc_env = self.env['accepted.quality.control']
            saved_aqc_id = aqc_env.create(put_data)

            # update qc status and apqc_pushed to true
            # qc_state_update_query = "UPDATE quality_control SET state='confirmed', apqc_pushed=TRUE, date_confirm='{0}' WHERE id={1}".format(
            #     str(fields.datetime.now()), self.id)
            # self.env.cr.execute(qc_state_update_query)
            # self.env.cr.commit()
            values = {
                'state': 'confirmed',
                'apqc_pushed': True,
                'date_confirm': fields.Datetime.now()
            }
            self.write(values)

            # apqc_pushed_qc_line_update_query = "UPDATE quality_control_line SET apqc_pushed=TRUE WHERE qc_line_id={0}".format(
            #     self.id)
            # self.env.cr.execute(apqc_pushed_qc_line_update_query)
            # self.env.cr.commit()

            self.qc_line.write({'apqc_pushed': True})

            # Confirm the aqc
            # aqc_confirm_query = "UPDATE accepted_quality_control SET state='confirm', date_confirm='{0}' WHERE id={1}".format(
            #     str(fields.datetime.now()), saved_aqc_id)
            # self.env.cr.execute(aqc_confirm_query)
            # self.env.cr.commit()

            saved_aqc_id.write({'state': 'confirm',
                                'date_confirm': fields.Datetime.now()
                                })

            # aqc_confirm_query_line = "UPDATE accepted_quality_control_line SET state='confirm', date_confirm='{0}' WHERE aqc_id={1}".format(
            #     str(fields.datetime.now()), saved_aqc_id)
            # self.env.cr.execute(aqc_confirm_query_line)
            # self.env.cr.commit()

            saved_aqc_id.aqc_line.write({'state': 'confirm',
                                         'date_confirm': fields.Datetime.now()
                                         })

            # import pdb
            # pdb.set_trace()

            # Generate Put away Process
            put_line_list = []

            for aqc_line_item in self.qc_line:
                if aqc_line_item.accepted_quantity > 0:
                    put_line_list.append([0, False,
                                          {
                                              'product_id': aqc_line_item.product_id,
                                              'qc_id': saved_aqc_id,
                                              'location': '',
                                              'product': aqc_line_item.product,
                                              'remarks': '',
                                              'quantity': aqc_line_item.accepted_quantity,
                                              # 'product_exp_date': aqc_line_item.product_exp_date,
                                              'po_number': self.po_number
                                          }])

            put_data = {
                # 'qc_number': self.qc_number,
                # 'qc_id': self.id,
                'warehouse_id': self.warehouse_id.id,
                # 'irn_number': self.irn_number,
                # 'irn_id': self.irn_id,
                # 'po_number': self.po_number,
                # 'po_id': self.po_id,
                # 'remark': self.remark,
                # 'received_by': self.received_by.id
            }
            put_data['aqc_id'] = saved_aqc_id.id
            put_data['putaway_line_view'] = put_line_list
            saved_put_id = self.env['putaway.process'].create(put_data)

        # 2. PQC will create with pending quantity
        pqc_data = {
            'qc_number': str(self.qc_number),
            'qc_id': self.id,
            'warehouse_id': self.warehouse_id.id,
            'irn_number': str(self.irn_number),
            'irn_id': self.irn_id,
            'po_number': str(self.po_number),
            'po_id': self.po_id,
            'remark': str(self.remark),
            'received_by': self.received_by.id,
        }
        pqc_line_list = list()

        for pqc_line_item in self.qc_line:

            if pqc_line_item.pending_quantity > 0:
                pqc_line_list.append([0, False,
                                      {
                                          'qc_id': self.id,
                                          'product': pqc_line_item.product,
                                          'product_id': pqc_line_item.product_id,
                                          'product_ean': pqc_line_item.product_ean,
                                          'description': pqc_line_item.description,
                                          'purchase_quantity': pqc_line_item.purchase_quantity,
                                          'received_quantity': pqc_line_item.received_quantity,
                                          'accepted_quantity': pqc_line_item.accepted_quantity,
                                          'pending_quantity': pqc_line_item.pending_quantity,
                                          'pqc_reject_quantity': pqc_line_item.pending_quantity,
                                      }])

        pqc_data['pqc_line'] = pqc_line_list

        if len(pqc_line_list) > 0:
            # Create pqc
            pqc_env = self.env['pending.quality.control']
            saved_pqc_id = pqc_env.create(pqc_data)

            saved_pqc_id.write({'state': 'pending',
                                })
            saved_pqc_id.pqc_line.write({'state': 'pending',
                                         })

        # update inbound end-to-end
        self.env['inb.end.to.end'].qc_confirm_update(po_number=self.po_number,
                                                     po_id=self.po_id
                                                     )

        return True

    """
    @api.onchange('scan')
    def po_barcode_onchange(self):

        import pdb;pdb.set_trace()

        return "xXxXxXxXxX"
    """

    @api.onchange('product_scan')
    def po_qc_product_scan(self):

        po_number = str(self.po_number)
        product_scan = str(self.product_scan)

        if product_scan != 'False':
            qc_env = self.env['quality.control']
            qc_obj = qc_env.search([('qc_number', '=', self.qc_number)])

            for single_line in qc_obj.qc_line:
                if str(single_line.product_ean) == product_scan:
                    if single_line.accepted_quantity < single_line.received_quantity:
                        single_line.accepted_quantity += 1
                    else:
                        raise osv.except_osv(_('QC line adjustment ERROR!'), _(
                            'Received quantity should be equal to (accepted quantity + pending quantity)!!!'))
                    if single_line.pending_quantity > 0:
                        single_line.pending_quantity -= 1
                    else:
                        raise osv.except_osv(_('QC line adjustment ERROR!'), _(
                            'Received quantity should be equal to (accepted quantity + pending quantity)!!!'))
                    break
            """
            if self.pending_quantity < 0:
            # raise error
                raise osv.except_osv(_('QC line adjustment ERROR!'), _(
                    'Received quantity should be equal to (accepted quantity + pending quantity)!!!'))
            """
            self.product_scan = ""


    #     try:
    #
    #         po_number = str(self.po_number)
    #
    #         product_scan = str(self.product_scan)
    #
    #         if product_scan != 'False':
    #             qc_env = self.env['quality.control']
    #             qc_obj = qc_env.search([('qc_number', '=', self.qc_number)])
    #
    #             # po_line_env = self.env['purchase.order.line']
    #             # po_line_obj = po_line_env.search([('order_id', '=', po_obj.id)])
    #
    #             # po_line_obj[0].product_id.ean13
    #             # self.irn_line[0].product_ean
    #
    #             for single_line in qc_obj.qc_line:
    #
    #                 if str(single_line.product_ean) == product_scan:
    #                     if single_line.accepted_quantity < single_line.received_quantity:
    #                         single_line.accepted_quantity += 1
    #                     else:
    #                         raise osv.except_osv(_('QC line adjustment ERROR!'), _(
    #                             'Received quantity should be equal to (accepted quantity + pending quantity)!!!'))
    #                     if single_line.pending_quantity > 0:
    #                         single_line.pending_quantity -= 1
    #                     else:
    #                         raise osv.except_osv(_('QC line adjustment ERROR!'), _(
    #                             'Received quantity should be equal to (accepted quantity + pending quantity)!!!'))
    #                     break
    #             # self.irn_line[0].received_quantity
    #             """
    #             if self.pending_quantity < 0:
    #                 # raise error
    #                 raise osv.except_osv(_('QC line adjustment ERROR!'), _(
    #                     'Received quantity should be equal to (accepted quantity + pending quantity)!!!'))
    #             """
    #             self.product_scan = ""
    #
    #     except:
    #
    #         self.product_scan = ""
    #
    #     return "xXxXxXxXxX"


class QualityControlLine(models.Model):
    _name = "quality.control.line"
    _description = "Quality Control Line"

    qc_line_id = fields.Many2one('quality.control', 'QC Line ID', required=True,
                                 ondelete='cascade', index=True, readonly=True)
    po_id = fields.Many2one('purchase.order', 'PO ID', required=False,
                            ondelete='cascade', index=True, readonly=True)
    product = fields.Char('Product')
    product_id = fields.Integer('Product ID')
    product_ean = fields.Char('Product EAN')
    description = fields.Char('Description')
    purchase_quantity = fields.Float('Purchase Qty')
    received_quantity = fields.Float('Received Qty')
    accepted_quantity = fields.Float('Accepted Qty')
    pending_quantity = fields.Float('Pending Qty')
    apqc_pushed = fields.Boolean('A/P QC Pushed')
    state = fields.Selection([
        ('accept', 'Accept'),
        ('reject', 'Reject'),
        ('cancel', 'Cancelled'),
    ], 'Status', help="Gives the status of the WMS inbound line", index=True)

    # ## Block code for V-14
    # _columns = {
    #     'qc_line_id': fields.many2one('quality.control', 'QC Line ID', required=True,
    #                                        ondelete='cascade', index=True, readonly=True),
    #     'po_id': fields.many2one('purchase.order', 'PO ID', required=False,
    #                              ondelete='cascade', index=True, readonly=True),
    #     'product': fields.char('Product'),
    #     'product_id': fields.integer('Product ID'),
    #     'product_ean': fields.char('Product EAN'),
    #     'description': fields.char('Description'),
    #     'purchase_quantity': fields.float('Purchase Qty'),
    #     'received_quantity': fields.float('Received Qty'),
    #     'accepted_quantity': fields.float('Accepted Qty'),
    #     'pending_quantity': fields.float('Pending Qty'),
    #     'apqc_pushed': fields.boolean('A/P QC Pushed'),
    #     'state': fields.selection([
    #         ('accept', 'Accept'),
    #         ('reject', 'Reject'),
    #         ('cancel', 'Cancelled'),
    #     ], 'Status', help="Gives the status of the WMS inbound line", index=True),
    # }

    @api.onchange('accepted_quantity')
    def po_qc_product_scan(self):

        try:

            if self.accepted_quantity > 0:

                # po_line_obj[0].product_id.ean13
                # self.irn_line[0].product_ean

                # self.pending_quantity = self.pending_quantity - self.accepted_quantity
                self.pending_quantity = self.received_quantity - self.accepted_quantity
                # self.save()
                # self.irn_line[0].received_quantity

                if self.pending_quantity < 0:
                    # raise error
                    raise osv.except_osv(_('QC line adjustment ERROR!'), _(
                        'Received quantity should be equal to (accepted quantity + pending quantity)!!!'))

                self.qc_line_id.product_scan = ""

        except Exception as e:

            self.qc_line_id.product_scan = ""

        # return "xXxXxXxXxX"
