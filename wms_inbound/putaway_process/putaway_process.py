from odoo import api, fields, models, _
from odoo.tools.translate import _
from odoo.osv import osv
from odoo.exceptions import except_orm, Warning, RedirectWarning


class RelationPutLineToAqcLine(models.Model):
    _name = 'relation.put.line.to.aqc.line'
    _description = 'Relation Put Line To Aqc Line'

    product_id = fields.Integer('Product ID')
    put_line_id = fields.Integer('Put Line ID')
    aqc_line_id = fields.Integer('AQC Line ID')
    assigned_qty = fields.Float('Assigned Quantity')
    location = fields.Char('Location')
    grn_id = fields.Integer('GRN ID')

    #### Block code for V-14
    # _columns = {
    #     'product_id': fields.integer('Product ID'),
    #     'put_line_id': fields.integer('Put Line ID'),
    #     'aqc_line_id': fields.integer('AQC Line ID'),
    #     'assigned_qty': fields.float('Assigned Quantity'),
    #     'location': fields.char('Location'),
    #     'grn_id': fields.integer('GRN ID'),
    #
    # }


class PutawayProcess(models.Model):
    _name = 'putaway.process'

    _description = "Putaway Process For Inbound"
    _order = 'id desc'

    scan = fields.Char('Scan')
    location_scan = fields.Char('Product Location Scan')
    location = fields.Char('Your Current Location')
    product_scan = fields.Char('Product Scan')
    product_location = fields.Char('Product Location')
    name = fields.Char('Name')
    warehouse_id = fields.Many2one('stock.warehouse', string='Warehouse ID', required=True)
    aqc_id = fields.Many2one('accepted.quality.control', string='AQC ID')

    assign_id = fields.Char('Assignee')
    description = fields.Char('Description')
    confirmation_date = fields.Datetime('Confirmation Date')
    state = fields.Selection([
        ('pending', 'Pending'),
        ('confirmed', 'Confirmed'),
        ('cancel', 'Cancelled'),
    ], 'Status', default='pending', help="Gives the status of the Put Away control", index=True)

    putaway_line_view = fields.One2many('putaway.process.details.view', 'putaway_line_ids', 'Putaway Line',
                                        required=True)

    ### Block code V-14
    # _columns = {
    #     'scan': fields.char('Scan'),
    #     'location_scan': fields.char('Product Location Scan'),
    #     'location': fields.char('Your Current Location'),
    #     'product_scan': fields.char('Product Scan'),
    #     'product_location': fields.char('Product Location'),
    #     'name': fields.char('Name'),
    #     'warehouse_id': fields.many2one('stock.warehouse', string='Warehouse ID', required=True),
    #     'aqc_id': fields.many2one('accepted.quality.control', string='AQC ID'),
    #
    #     'assign_id': fields.char('Assignee'),
    #     'description': fields.char('Description'),
    #     'confirmation_date': fields.datetime('Confirmation Date'),
    #     'state': fields.selection([
    #         ('confirmed', 'Confirmed'),
    #         ('pending', 'Pending'),
    #         ('cancel', 'Cancelled'),
    #     ], 'Status', help="Gives the status of the Put Away control", index=True),
    #
    #     'putaway_line_view': fields.one2many('putaway.process.details.view', 'putaway_line_ids', 'Putaway Line',
    #                                          required=True),
    #
    #
    #
    # }
    #
    # _defaults = {
    #     'state': 'pending',
    #
    # }

    def put_cancel(self):
        id_list = self.ids
        put_data = self.browse()

        if put_data.state == 'confirmed':
            raise Warning(_('This Put Away already confirmed. You can not cancel it'))

        if put_data.state == 'cancel':
            raise Warning(_('This Put Away already Cancelled'))

        put_line_id = [item.id for item in put_data.putaway_line_view]
        put_line_ids = self.env['relation.put.line.to.aqc.line'].search([('put_line_id', 'in', put_line_id)])
        assigned_put = self.env['relation.put.line.to.aqc.line'].browse(put_line_ids)

        for rel_item in assigned_put:
            self.env.cr.execute(
                "select accepted_quality_control.id,accepted_quality_control_line.remaining_quantity from  accepted_quality_control,accepted_quality_control_line where accepted_quality_control.id =accepted_quality_control_line.aqc_id and accepted_quality_control_line.id=%s",
                ([rel_item.aqc_line_id]))
            qc_ids = self.env.cr.fetchall()
            if len(qc_ids) > 0:
                remaining_qty = qc_ids[0][1] + rel_item.assigned_qty
                self.env.cr.execute("update accepted_quality_control set assigned=FALSE where id=%s", ([qc_ids[0][0]]))
                self.env.cr.commit()
                self.env.cr.execute("update accepted_quality_control_line set remaining_quantity=%s where id=%s",
                                    ([remaining_qty, rel_item.aqc_line_id]))
                self.env.cr.commit()

        for element in id_list:
            ids = [element]

            if ids is not None:
                self.env.cr.execute("update putaway_process set state='cancel' where id=%s", (ids))
                self.env.cr.commit()

        return ''

    def put_confirm(self):
        id_list = self.ids
        aqc_list = []
        put_data = self.browse()

        product_dict = {}

        for items in put_data.putaway_line_view:
            product_dict[items.product_id] = [items.quantity, items.location]

        for item in put_data.putaway_line_view:
            if len(item.location) < 2:
                raise Warning(_('You have to specify the location.'))

        if put_data.state == 'confirmed':
            raise Warning(_('This Put Away already confirmed'))
        if put_data.state == 'cancel':
            raise Warning(_('This Put Away already Cancelled. You can not confirm it. Please create new one.'))

        from datetime import date, datetime
        current_time = datetime.now()

        for element in id_list:
            ids = [element]

            if ids is not None:
                try:
                    for p_id in ids:
                        confirm_wms_query = "UPDATE putaway_process SET state='confirmed', confirmation_date='{0}' WHERE id='{1}'".format(
                            str(fields.datetime.now()), p_id)
                        self.env.cr.execute(confirm_wms_query)
                        self.env.cr.commit()
                except:
                    pass

        update_grn_id_dict = {}

        created_id = None

        for item in put_data.putaway_line_view:

            po_id = item.po_number

            po_id = self.env['purchase.order'].search([('name', '=', item.po_number)])

            purchase_order = self.env['purchase.order'].browse(po_id)

            picking_ids = []

            for po in purchase_order:
                for picking in po.picking_ids:
                    if picking.state in ['assigned', 'partially_available']:
                        picking_ids.append(picking.id)

            if len(picking_ids) > 0:
                picking = [picking_ids[0]]

            context = []
            context.update({

                'active_model': 'stock.picking',
                'active_ids': picking,
                'active_id': len(picking) and picking[0] or False
            })
            created_id = self.env['stock.transfer_details'].create({
                'picking_id': len(picking) and picking[0] or False})

            if created_id is not None:
                break

        if created_id:
            created_id = self.env['stock.transfer_details'].browse(created_id)
            if created_id.picking_id.state in ['assigned', 'partially_available']:
                processed_ids = []

                # Create new and update existing pack operations
                for lstits in [created_id.item_ids, created_id.packop_ids]:
                    for prod in lstits:
                        if product_dict.get(prod.product_id.id):

                            pack_datas = {
                                'product_id': prod.product_id.id,
                                'product_uom_id': prod.product_uom_id.id,
                                'product_qty': product_dict.get(prod.product_id.id)[0],
                                'package_id': prod.package_id.id,
                                'lot_id': prod.lot_id.id,
                                'location_id': prod.sourceloc_id.id,
                                'location_dest_id': prod.destinationloc_id.id,
                                'result_package_id': prod.result_package_id.id,
                                'date': prod.date if prod.date else datetime.now(),
                                'owner_id': prod.owner_id.id,
                                'product_location': product_dict.get(prod.product_id.id)[1]
                            }

                            if prod.packop_id:
                                prod.packop_id.with_context(no_recompute=True).write(pack_datas)
                                processed_ids.append(prod.packop_id.id)
                            else:
                                pack_datas['picking_id'] = created_id.picking_id.id
                                packop_id = created_id.env['stock.pack.operation'].create(pack_datas)
                                processed_ids.append(packop_id.id)
                # Delete the others
                packops = created_id.env['stock.pack.operation'].search(
                    ['&', ('picking_id', '=', created_id.picking_id.id), '!', ('id', 'in', processed_ids)])
                packops.unlink()

                # Execute the transfer of the picking
                created_id.picking_id.do_transfer()

        self.env['inb.end.to.end'].putaway_confirm_update(str(put_data.aqc_id.po_number), put_data.aqc_id.po_id)

        return ''

    @api.onchange('location_scan')
    def put_away_location_onchange(self):
        location = str(self.location_scan)
        self.location = location
        self.location_scan = ""
        # return 'Cool'

    def check_total_qty_put_line(self):
        get_all_pending_aqc_list = []
        submitted_put_line_value = []

        product_list = [items[2].get('product_id') for items in submitted_put_line_value]

        product_list = list(set(product_list))

        submitted_dict = {}

        for p_id in product_list:
            qty = 0
            for items in submitted_put_line_value:
                if items[2].get('product_id') == p_id:
                    qty += items[2].get('quantity')
        submitted_dict[p_id] = qty

        query_product_list = [items[2] for items in get_all_pending_aqc_list]
        query_product_list = list(set(query_product_list))

        query_dict = {}

        for qp_id in query_product_list:
            qty = 0
            for item in get_all_pending_aqc_list:
                if qp_id == item[2]:
                    qty += item[4]
            query_dict[qp_id] = qty

        message = 0

        for key, item in submitted_dict.items():

            if query_dict.get(key) is not None:
                if item > query_dict.get(key):
                    message = 1
                    break
            else:
                message = 2
                break
        return message

    @api.onchange('product_scan')
    def put_away_product_onchange(self):
        ean_number = str(self.product_scan)
        location = str(self.location)
        self.product_scan = ""
        if location == 'False':
            raise Warning(_('At first scan the location then product'))

        found_product_id = None

        if self.putaway_line_view:
            new_line = []
            for items in self.putaway_line_view:
                new_line.append({
                    'qc_id': items['qc_id'],
                    'product_id': items['product_id'],
                    'product': items['product'],
                    'location': items['location'],
                    'quantity': items['quantity'],
                    'remarks': items['remarks'],
                    'po_number': items['po_number'],
                })
        else:
            new_line = []

        if ean_number != 'False':
            qty = 1
            if 'Prod' not in ean_number:

                product_env = self.env['product.product']
                product_obj = product_env.search(
                    ['|', '|', '|', '|', ('ean13', '=', str(ean_number)), ('gift_ean', '=', str(ean_number)),
                     ('box_ean', '=', str(ean_number)), ('case_ean', '=', str(ean_number)),
                     ('pallet_ean', '=', str(ean_number))])
                if not product_obj.id:
                    raise Warning(
                        _('Product Not found.Please add EAN number in Product Configuration. Or contact with Your department head'))

                # if str(product_obj.gift_ean) == str(ean_number):
                #     qty = 1
                # elif str(product_obj.box_ean) == str(ean_number):
                #     qty = product_obj.holding_box_qty
                # elif str(product_obj.case_ean) == str(ean_number):
                #     qty = product_obj.holding_case_qty
                # elif str(product_obj.pallet_ean) == str(ean_number):
                #     qty = product_obj.holding_pallet_qty
            else:
                get_list = ean_number.split('Prod')
                product_id = int(get_list[1])

                product_env = self.env['product.product']
                product_obj = product_env.search([('id', '=', product_id)])

                if product_obj.id:
                    qty = 1
                else:
                    raise Warning(_('Product Not found.Please scan properly with proper product'))

            same_location_product_found = False

            for items in self.putaway_line_view:
                if items['product_id'] == product_obj.id:
                    items['location'] = location

                    # items['quantity'] +=qty
            #         same_location_product_found=True
            #         break
            # if same_location_product_found == False:
            #     if product_obj.id:
            #
            #
            #         new_line.append({
            #             'product_id': product_obj.id,
            #             'product': product_obj.name,
            #             'location': self.location,
            #             'quantity': qty,
            #             'remarks': '',
            #             'po_number': '',
            #         })
            # self.putaway_line_view = new_line
        self.product_scan = ""

        # return 'Cool'

    @api.onchange('scan')
    def put_away_barcode_onchange(self):

        qc_number = str(self.scan)
        qc_ids = []

        if self.putaway_line_view:
            new_putaway_line = []
            for items in self.putaway_line_view:
                qc_ids.append(items['qc_id'])
                new_putaway_line.append({

                    'qc_id': items['qc_id'],
                    'product_id': items['product_id'],
                    'product': items['product'],
                    'location': items['location'],
                    'quantity': items['quantity'],
                    'remarks': items['remarks'],
                    'po_number': items['po_number'],
                })
        else:
            new_putaway_line = []

        if qc_number != 'False':
            qc_number = str(qc_number.upper())
            mod_obj = self.env['accepted.quality.control']
            model_data_ids = mod_obj.search([('aqc_number', '=', str(qc_number))])

            for items in model_data_ids:
                if not items.assigned:
                    for qc_items in items.aqc_line:

                        # Assign Product Location By Default

                        p_loc = ''
                        if qc_items.product_id.x_products_location:
                            p_loc = qc_items.product_id.x_products_location

                        if qc_items.aqc_id not in qc_ids:
                            new_putaway_line.append({
                                'qc_id': qc_items.aqc_id,
                                'product_id': qc_items.product_id,
                                'product': qc_items.product,
                                'po_number': items.po_number,
                                'location': p_loc,
                                'remarks': '',
                                'quantity': qc_items.accepted_quantity,
                            })
                    self.putaway_line_view = new_putaway_line
            self.scan = ""

        # return "xXxXxXxXxX"

    def create(self, values):
        # Generate PUT AWAY Number

        # Get All data which are accepted in AQC Line

        # cr.execute("select id,aqc_id,product_id,remaining_quantity,accepted_quantity from accepted_quality_control_line WHERE state='confirm' and remaining_quantity > 0")
        #
        # get_all_pending_aqc_list = cr.fetchall()
        # submitted_put_line_value = vals.get('putaway_line_view')

        # message = self.check_total_qty_put_line(get_all_pending_aqc_list,submitted_put_line_value)
        # if message == 1:
        #     raise Warning(_('Quantity Is Greater Than Accepeted Quanitity'))
        # elif message == 2:
        #     raise Warning(_('Product you have scanned is still in the process bucket / pending state.'))

        qc_ids = []

        # Warehouse ID

        record = super(PutawayProcess, self).create(values)
        put_number = "PUT0" + str(record.id)
        record.name = put_number

        # if record:
        #     cr.execute(
        #         "select id,product_id,quantity,location,qc_id from  putaway_process_details_view where putaway_line_ids = %s",
        #         ([record]))
        #     get_all_put_line_list = cr.fetchall()
        #     new_get_all_pending_aqc_list = [list(item) for item in get_all_pending_aqc_list]
        #     data_pool = self.env['relation.put.line.to.aqc.line']
        #     for items in get_all_put_line_list:
        #         product_dir_id = data_pool.create(cr, uid, {
        #             'put_line_id': items[0],
        #             'aqc_line_id': items[4],
        #             'assigned_qty': items[2],
        #             'location': items[3],
        #             'product_id': items[1]
        #         })
        #         qty=0
        #         # cr.execute("update putaway_process_details_view set qc_id=%s where id=%s", (aq_item[0], items[0]))
        #         for aq_items in new_get_all_pending_aqc_list:
        #             qty=0
        #             cr.execute("update accepted_quality_control_line set remaining_quantity=%s where id=%s",
        #                        (qty, aq_items[0]))
        #             cr.commit()

        # Following condition has been commented due to reduce the allocation of aqc data. From now (21-01-19 with gauroV) it will be auto

        # if record:
        #     cr.execute("select id,product_id,quantity,location from  putaway_process_details_view where putaway_line_ids = %s",([record]))
        #     get_all_put_line_list = cr.fetchall()
        #     new_get_all_pending_aqc_list =[list(item) for item in get_all_pending_aqc_list]
        #     data_pool = self.env['relation.put.line.to.aqc.line']
        #
        #
        #     for items in get_all_put_line_list:
        #         qty = items[2]
        #         for aq_item in new_get_all_pending_aqc_list:
        #             if aq_item[3] >0:
        #                 if qty ==0:
        #                     break
        #                 elif items[1] == aq_item[2] and qty == aq_item[3]:
        #                     assigned_qty = qty
        #                     qty=0
        #                     aq_item[3]=0
        #                     product_dir_id = data_pool.create(cr, uid, {
        #                         'put_line_id': items[0],
        #                         'aqc_line_id': aq_item[0],
        #                         'assigned_qty': assigned_qty,
        #                         'location': items[3],
        #                         'product_id': items[1]
        #                     })
        #
        #
        #
        #                     cr.execute("update putaway_process_details_view set qc_id=%s where id=%s", (aq_item[0],items[0]))
        #                     cr.execute("update accepted_quality_control_line set remaining_quantity=%s where id=%s", (qty,aq_item[0]))
        #                     cr.commit()
        #
        #                     break
        #                 elif items[1] == aq_item[2] and qty > aq_item[3]:
        #                     assigned_qty = aq_item[3]
        #                     qty = qty-aq_item[3]
        #                     remaining_qty=0
        #                     aq_item[3] = 0
        #                     product_dir_id = data_pool.create(cr, uid, {
        #                         'put_line_id': items[0],
        #                         'aqc_line_id': aq_item[0],
        #                         'assigned_qty': assigned_qty,
        #                         'location': items[3],
        #                         'product_id': items[1]
        #                     })
        #
        #                     cr.execute("update putaway_process_details_view set qc_id=%s where id=%s",
        #                                (aq_item[0], items[0]))
        #                     cr.execute("update accepted_quality_control_line set remaining_quantity=%s where id=%s",
        #                                (remaining_qty, aq_item[0]))
        #                     cr.commit()
        #
        #                 elif items[1] == aq_item[2] and qty < aq_item[3]:
        #                     assigned_qty = qty
        #                     remaining_qty=aq_item[3] - qty
        #                     aq_item[3] = remaining_qty
        #                     qty = 0
        #                     product_dir_id = data_pool.create(cr, uid, {
        #                         'put_line_id': items[0],
        #                         'aqc_line_id': aq_item[0],
        #                         'assigned_qty': assigned_qty,
        #                         'location': items[3],
        #                         'product_id': items[1]
        #                     })
        #
        #                     cr.execute("update putaway_process_details_view set qc_id=%s where id=%s",
        #                                (aq_item[0], items[0]))
        #                     cr.execute("update accepted_quality_control_line set remaining_quantity=%s where id=%s",
        #                                (remaining_qty, aq_item[0]))
        #                     cr.commit()
        #

        # putaway_obj = self.browse([record])
        self.env['inb.end.to.end'].putaway_data_create(str(record.aqc_id.po_number),
                                                       record.aqc_id.po_id, record.id, put_number, record.create_date)

        # for qc_id in qc_ids:
        #
        #     cr.execute("update accepted_quality_control set assigned=TRUE where id=%s", ([qc_id]))
        #     cr.commit()

        return record


class PutawayProcessDetails(models.Model):
    _name = 'putaway.process.details'
    _description = 'Putaway Process Details'

    putaway_line_ids = fields.Many2one('putaway.process', 'Put Away Process ID', required=True,
                                       ondelete='cascade', index=True, readonly=True)
    qc_id = fields.Many2one('accepted.quality.control', string='AQC ID')
    product_id = fields.Integer('Product ID')
    location = fields.Char('Location')
    product = fields.Char('Product')
    remarks = fields.Char('Remarks')
    po_number = fields.Char('PO Number')
    quantity = fields.Float('Quantity')

    # ##Block code for V-14
    # _columns = {
    #     'putaway_line_ids': fields.many2one('putaway.process', 'Put Away Process ID', required=True,
    #                                                 ondelete='cascade', index=True, readonly=True),
    #     'qc_id': fields.many2one('accepted.quality.control', string='AQC ID'),
    #     'product_id': fields.integer('Product'),
    #     'location': fields.char('Location'),
    #     'product': fields.char('Product'),
    #     'remarks': fields.char('Remarks'),
    #     'po_number': fields.char('PO Number'),
    #     'quantity': fields.float('Quantity'),
    # }


class PutawayProcessDetailsView(models.Model):
    _name = 'putaway.process.details.view'
    _description = 'Putaway Process Details View'

    putaway_line_ids = fields.Many2one('putaway.process', 'Put Away Process ID', required=True,
                                       ondelete='cascade', index=True, readonly=True)
    product_id = fields.Integer('Product ID', required=True)
    qc_id = fields.Integer('AQC ID')
    location = fields.Char('Location')
    product = fields.Char('Product')
    remarks = fields.Char('Remarks')
    po_number = fields.Char('PO Number')
    quantity = fields.Float('Quantity', required=True)

    # ## Block code for V-14
    # _columns = {
    #     'putaway_line_ids': fields.many2one('putaway.process', 'Put Away Process ID', required=True,
    #                                                 ondelete='cascade', index=True, readonly=True),
    #     'product_id': fields.integer('Product',required=True),
    #     'qc_id': fields.integer('AQC ID'),
    #     'location': fields.char('Location'),
    #     'product': fields.char('Product'),
    #     'remarks': fields.char('Remarks'),
    #     'po_number': fields.char('PO Number'),
    #     'quantity': fields.float('Quantity',required=True),
    #
    #
    # }
