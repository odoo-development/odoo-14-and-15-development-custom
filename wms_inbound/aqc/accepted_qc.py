from odoo import api, fields, models, _
import datetime


class AcceptedQualityControl(models.Model):
    _name = "accepted.quality.control"
    _description = "Accepted Quality Control"
    _order = 'id desc'
    _rec_name = "aqc_number"

    scan = fields.Char('Scan')
    product_scan = fields.Char('Product Scan')
    warehouse_id = fields.Many2one('stock.warehouse', string='Warehouse Location', required=True)
    aqc_number = fields.Char('AQC Number')
    qc_number = fields.Char('QC Number')
    qc_id = fields.Char('QC ID')
    pqc_number = fields.Char('PQC Number')
    pqc_id = fields.Integer('PQC ID')
    irn_number = fields.Char('IRN Number')
    irn_id = fields.Integer('IRN ID')
    po_number = fields.Char('PO Number')
    po_id = fields.Integer('Purchase ID')
    remark = fields.Text('Remark')
    assigned = fields.Boolean('Assigned')
    received_by = fields.Many2one('res.users', 'Received By')
    date_confirm = fields.Datetime('Confirmation Date', readonly=True, select=False)
    state = fields.Selection([
        ('pending', 'Pending'),
        ('confirm', 'Confirmed'),
        ('cancel', 'Cancelled'),
    ], 'Status', readonly=True, copy=False, help="Gives the status of the WMS inbound", index=True)
    aqc_line = fields.One2many('accepted.quality.control.line', 'aqc_id', 'AQC Line', required=True)

    # ## Block code for V-14
    # _columns = {
    #     'scan': fields.char('Scan'),
    #     'product_scan': fields.char('Product Scan'),
    #     'warehouse_id': fields.many2one('stock.warehouse', string='Warehouse Location', required=True),
    #     'aqc_number': fields.char('AQC Number'),
    #     'qc_number': fields.char('QC Number'),
    #     'qc_id': fields.char('QC ID'),
    #     'pqc_number': fields.char('PQC Number'),
    #     'pqc_id': fields.integer('PQC ID'),
    #     'irn_number': fields.char('IRN Number'),
    #     'irn_id': fields.integer('IRN ID'),
    #     'po_number': fields.char('PO Number'),
    #     'po_id': fields.integer('Purchase ID'),
    #     'remark': fields.text('Remark'),
    #     'assigned': fields.boolean('Assigned'),
    #     'received_by': fields.many2one('res.users', 'Received By'),
    #     'date_confirm': fields.datetime('Confirmation Date', readonly=True, select=False),
    #     'state': fields.selection([
    #         ('pending', 'Pending'),
    #         ('confirm', 'Confirmed'),
    #         ('cancel', 'Cancelled'),
    #
    #     ], 'Status', readonly=True, copy=False, help="Gives the status of the WMS inbound", index=True),
    #     'aqc_line': fields.one2many('accepted.quality.control.line', 'aqc_id', 'AQC Line', required=True),
    # }
    #
    # _defaults = {
    #     'user_id': lambda obj, cr, uid, context: uid,
    #     'received_by': lambda obj, cr, uid, context: uid,
    #     'state': 'pending',
    # }

    @api.model
    def create(self, vals):
        # generate AQC number
        # AQC0123456

        record = super(AcceptedQualityControl, self).create(vals)
        record.aqc_number = "AQC0" + str(record.id)

        self.env['inb.end.to.end'].aqc_data_create(po_number=record.po_number,
                                                   po_id=record.po_id,
                                                   aqc_id=record.id,
                                                   aqc_number=record.aqc_number,
                                                   aqc_date_create=record.create_date)

        return record

    def confirm_wms_inbound_aqc(self):
        for aqc in self:
            aqc.write({
                'state': 'confirm',
                'date_confirm': str(fields.datetime.now()),
            })

            aqc.pqc_line.write({
                'state': 'confirm',
                'date_confirm': str(fields.datetime.now()),
            })

        # ids = self.ids
        # for single_id in ids:
        #     aqc_confirm_query = "UPDATE accepted_quality_control SET state='confirm', date_confirm='{0}' WHERE id={1}".format(
        #         str(fields.datetime.now()), single_id)
        #     self.env.cr.execute(aqc_confirm_query)
        #     self.env.cr.commit()
        #
        #     aqc_confirm_query_line = "UPDATE accepted_quality_control_line SET state='confirm', date_confirm='{0}' WHERE aqc_id={1}".format(
        #         str(fields.datetime.now()), single_id)
        #     self.env.cr.execute(aqc_confirm_query_line)
        #     self.env.cr.commit()

        # update inbound end-to-end
        self.env['inb.end.to.end'].aqc_confirm_update(po_number=self.po_number,
                                                      po_id=self.po_id)
        #
        # return True


class AcceptedQualityControlLine(models.Model):
    _name = "accepted.quality.control.line"
    _description = "Accepted Quality Control Line"

    aqc_id = fields.Many2one('accepted.quality.control', 'AQC ID', required=True,
                              ondelete='cascade', index=True, readonly=True)
    qc_id = fields.Many2one('quality.control', 'QC ID', required=False,
                             ondelete='cascade', index=True, readonly=True)
    product = fields.Char('Product Name')
    product_id = fields.Many2one('product.product', 'Product', required=True)
    product_ean = fields.Char('Product EAN')
    description = fields.Char('Description')
    purchase_quantity = fields.Float('Purchase Qty')
    received_quantity = fields.Float('Received Qty')
    accepted_quantity = fields.Float('Accepted Qty')
    date_confirm = fields.Datetime('Confirmation Date', readonly=True, select=False)
    pending_quantity = fields.Float('Pending Qty')
    remaining_quantity = fields.Float('Remaining GRN Received Qty')
    state = fields.Selection([
        ('pending', 'Pending'),
        ('confirm', 'Confirmed'),
        ('cancel', 'Cancelled'),
    ], 'Status', help="Gives the status of the WMS inbound line", index=True)

    # ## Block code for V-14
    # _columns = {
    #     'aqc_id': fields.many2one('accepted.quality.control', 'AQC ID', required=True,
    #                               ondelete='cascade', index=True, readonly=True),
    #     'qc_id': fields.many2one('quality.control', 'QC ID', required=False,
    #                              ondelete='cascade', index=True, readonly=True),
    #     'product': fields.char('Product'),
    #     # 'product_id': fields.integer('Product ID'),
    #     'product_id': fields.many2one('product.product', 'Product', required=True),
    #     'product_ean': fields.char('Product EAN'),
    #     'description': fields.char('Description'),
    #     'purchase_quantity': fields.float('Purchase Qty'),
    #     'received_quantity': fields.float('Received Qty'),
    #     'accepted_quantity': fields.float('Accepted Qty'),
    #     'date_confirm': fields.datetime('Confirmation Date', readonly=True, select=False),
    #     'pending_quantity': fields.float('Pending Qty'),
    #     'remaining_quantity': fields.float('Remaining GRN Received Qty'),
    #     # This column added by mufti. It will calculate how many quantities are pending for GRN Receive. And This column is filling up from Put away process.
    #
    #     'state': fields.selection([
    #         ('pending', 'Pending'),
    #         ('confirm', 'Confirmed'),
    #         ('cancel', 'Cancelled'),
    #     ], 'Status', help="Gives the status of the WMS inbound line", index=True),
    # }
