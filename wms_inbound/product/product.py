from odoo import api, fields, models, _
from datetime import datetime, timedelta
from odoo.tools import ustr, DEFAULT_SERVER_DATE_FORMAT as DF


class ProductConfig(models.Model):
    _inherit = 'product.product'

    def _get_60_days_lowest_purchase_price(self):
        # pol_obj = self.env['purchase.order.line']
        res = {}

        compare_date = datetime.now() - timedelta(days=61)

        compare_date = compare_date.strftime(DF)

        for product_id in self:

            #get all purchase order lines with current product product_id
            # res[product_id] = qty = subtot = others = others_qty = 0.0
            lowest_price = 999999999.0
                
            line_ids = self.env['purchase.order.line'].search([('product_id', '=', product_id.id),('create_date', '>', compare_date)])
            # pol = pol_obj.browse(line_ids)
            for line in line_ids:
                if line.state == 'purchase' or line.state == 'done':
                    if line.price_unit < lowest_price:
                        lowest_price = line.price_unit

            product_id.last_60_days_lowest_price = lowest_price if lowest_price != 999999999.0 else 0

    gift_ean = fields.Char('EAN 2')
    prod_uom = fields.Char('UOM')
    has_box = fields.Boolean('Has Box')
    holding_box_qty = fields.Float('Box Qty')
    box_ean = fields.Char('BOX EAN')
    has_case = fields.Boolean('Has Case')
    holding_case_qty = fields.Float('Pallet Qty')
    case_ean = fields.Char('Case EAN')
    has_pallet = fields.Boolean('Has Pallet')
    holding_pallet_qty = fields.Float('Pallet Qty')
    pallet_ean = fields.Char('Pallet EAN')
    reorder_qty_level = fields.Float('Re Order Qty Level')
    uttwh_location = fields.Char('Uttara Location')
    nodda_location = fields.Char('Nodda Location')
    motijil_location = fields.Char('Motijil Location')
    retail_location = fields.Char('Retail Location')
    auto_po_generation = fields.Boolean('Auto PO Genration')
    last_60_days_lowest_price = fields.Float(compute='_get_60_days_lowest_purchase_price', string='Last 60 Days Lowest Purchase Price')

    # ## Block code for V-14
    # _columns = {
    #     'gift_ean': fields.char('EAN 2'),
    #     'prod_uom': fields.char('UOM'),
    #     'has_box': fields.boolean('Has Box'),
    #     'holding_box_qty': fields.float('Box Qty'),
    #     'box_ean': fields.char('BOX EAN'),
    #     'has_case': fields.boolean('Has Case'),
    #     'holding_case_qty': fields.float('Pallet Qty'),
    #     'case_ean': fields.char('Case EAN'),
    #     'has_pallet': fields.boolean('Has Pallet'),
    #     'holding_pallet_qty': fields.float('Pallet Qty'),
    #     'pallet_ean': fields.char('Pallet EAN'),
    #     'reorder_qty_level': fields.float('Re Order Qty Level'),
    #     'uttwh_location': fields.char('Uttara Location'),
    #     'nodda_location': fields.char('Nodda Location'),
    #     'motijil_location': fields.char('Motijil Location'),
    #     'retail_location': fields.char('Retail Location'),
    #     'auto_po_generation': fields.boolean('Auto PO Genration'),
    #     'last_60_days_lowest_price': fields.function(_get_60_days_lowest_purchase_price, type='float', string='Last 60 Days Lowest Purchase Price'),
    #
    #
    # }