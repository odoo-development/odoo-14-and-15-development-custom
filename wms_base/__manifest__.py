# -*- coding: utf-8 -*-

{
    'name': 'WMS Base',
    'version': '1.0.0',
    'summary': 'Manage your inbound and outbound activities',
    'description': "",
    'author': 'Odoo Bangladesh',
    'depends': [
        'stock',
        # 'partner_custom_fields',
        # 'product_custom_fields',
        'stock_picking_custom_fields',
        'purchase_order_custom_fields',
        'sale_order_custom_fields'
    ],
    'category': 'Inventory/Inventory',
    'sequence': 25,
    'demo': [],
    'data': [
        'views/menu_views.xml',
    ],
    'qweb': [],
    'installable': True,
    'application': True,
    'auto_install': False,
}
