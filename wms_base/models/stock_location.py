from odoo import models


class StockLocation(models.Model):
    _inherit = 'stock.location'

    def _get_parent_path(self, location):
        parent_path = ''
        if location.location_id:
            parent_path = self._get_parent_path(location.location_id)

        return parent_path + str(location.id) + '/'

    def fix_location_parent_path(self):
        for loc in self.search([]):
            parent_path = ''
            if not loc.parent_path:
                if loc.location_id:
                    parent_path = self._get_parent_path(loc.location_id)

                loc.update({'parent_path': parent_path + str(loc.id) + '/'})

        return True
