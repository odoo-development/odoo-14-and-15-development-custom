from time import gmtime, strftime

from odoo import api
from odoo import fields, models
from odoo.exceptions import UserError, ValidationError, Warning
from odoo.exceptions import except_orm
from odoo.tools.translate import _


class StnProcess(models.Model):
    _name = "stn.process"
    _inherit = ["mail.thread"]
    _description = "STN Process"

    warehouse_id_from = fields.Many2one("stock.warehouse", string="Requested From", required=True)
    warehouse_id_to = fields.Selection([
        ('1', "Uttara Warehouse"),
        ('2', "Nodda Warehouse"),
        ('3', "Signboard Warehouse"),
        ('7', "Badda Warehouse"),
        ('4', "Uttara Damage Warehouse"),
        ('5', "Nodda Damage Warehouse"),
        ('6', "Signboard Damage Warehouse"),
        ('9', "Badda Damage Warehouse"),
    ], "Requested To", readonly=True, copy=False,
        help="Gives the List of the Warehouses",
        index=True, )
    remark = fields.Text("Remark")
    received = fields.Boolean("Received")
    received_by = fields.Many2one("res.users", "Received By")
    received_time = fields.Datetime("Received Time")
    send = fields.Boolean("Send")
    send_by = fields.Many2one("res.users", "Sending By")
    send_time = fields.Datetime("Sending Time")
    cancel = fields.Boolean("Cancel")
    cancel_by = fields.Many2one("res.users", "Cancel By")
    cancel_time = fields.Datetime("Cancel Time")
    requested = fields.Boolean("Requested")
    requested_by = fields.Many2one("res.users", "Requested By")
    requested_time = fields.Datetime("Requested Time")
    name = fields.Char("STN Number")

    state = fields.Selection([
        ("draft", "Created"),
        ("requested", "Requested"),
        ("send", "Confirmed"),
        ("receive", "Received"),
        ("cancel", "Cancelled"),
    ], "Status", default='draft', tracking=True, copy=False,
        help="Gives the status of the STN Process",
        index=True, readonly=True)
    stn_process_line = fields.One2many(
        "stn.process.line",
        "stn_process_id",
        "STN Process Line",
        required=True,
    )
    cancel_stn_process_date = fields.Datetime("Cancel STN Process Date")
    cancel_stn_process_by = fields.Many2one("res.users", "Cancel STN Process By")
    stock_id = fields.Many2one("stock.picking", "Stock Number")
    cancel_stn_process_reason = fields.Text("Cancel STN Process Reason")

    # ## Block code for V-14
    # _columns = {
    #     "warehouse_id_from": fields.many2one(
    #         "stock.warehouse", string="Requested From", required=True
    #     ),
    #     "warehouse_id_to": fields.selection(
    #         [
    #             (1, "Central Warehouse Uttara"),
    #             (2, "Nodda Warehouse"),
    #             (3, "Nodda Retail Warehouse"),
    #             (4, "Motijheel Warehouse"),
    #             (6, "Uttara Retail Warehouse"),
    #             (7, "Motijheel Retail Warehouse"),
    #             (8, "Damage Warehouse"),
    #             (19, "Comilla Warehouse"),
    #         ],
    #         "Requested To",
    #         readonly=True,
    #         copy=False,
    #         help="Gives the List of the Warehouses",
    #         index=True,
    #     ),
    #     "remark": fields.text("Remark"),
    #     "received": fields.boolean("Received"),
    #     "received_by": fields.many2one("res.users", "Received By"),
    #     "received_time": fields.datetime("Received Time"),
    #     "send": fields.boolean("Send"),
    #     "send_by": fields.many2one("res.users", "Sending By"),
    #     "send_time": fields.datetime("Sending Time"),
    #     "cancel": fields.boolean("Cancel"),
    #     "cancel_by": fields.many2one("res.users", "Cancel By"),
    #     "cancel_time": fields.datetime("Cancel Time"),
    #     "requested": fields.boolean("Requested"),
    #     "requested_by": fields.many2one("res.users", "Requested By"),
    #     "requested_time": fields.datetime("Requested Time"),
    #     "name": fields.char("STN Number"),
    #     "state": fields.selection(
    #         [
    #             ("draft", "Created"),
    #             ("requested", "Requested"),
    #             ("send", "Confirmed"),
    #             ("receive", "Received"),
    #             ("cancel", "Cancelled"),
    #         ],
    #         "Status",
    #         readonly=True,
    #         copy=False,
    #         help="Gives the status of the STN Process",
    #         index=True,
    #     ),
    #     "stn_process_line": fields.one2many(
    #         "stn.process.line",
    #         "stn_process_id",
    #         "STN Process Line",
    #         required=True,
    #     ),
    #     "cancel_stn_process_date": fields.datetime("Cancel STN Process Date"),
    #     "cancel_stn_process_by": fields.many2one(
    #         "res.users", "Cancel STN Process By"
    #     ),
    #     "stock_id": fields.many2one("stock.picking", "Stock Number"),
    #     "cancel_stn_process_reason": fields.text("Cancel STN Process Reason"),
    # }
    #
    # _defaults = {
    #     "user_id": lambda obj, cr, uid, context: uid,
    #     "state": "draft",
    # }

    @api.model
    def create(self, vals):

        warehouse_code_dict = {
            1: "UttWH",
            2: "NodWH",
            3: "SinWH",
            7: "BddWH",
            4: "UttDW",
            5: "NodDW",
            6: "SinDW",
            9: "BddDW",
            10: "BdHoW",
            11: "SavWH",
            12: "SavDW"
        }

        if vals["warehouse_id_from"] == vals["warehouse_id_to"]:
            raise Warning(_(
                "Source Location and Destination Location will be different. \n They will not be the same."
            ),
            )

        warehouse_id_to = vals["warehouse_id_to"]

        for items in vals.get("stn_process_line"):

            values = dict()

            product_id = items[2]["product_id"]

            prod_obj = self.env["product.product"]
            uid = 1
            prod_search = prod_obj.search([("id", "=", product_id)])
            prod_browse = self.env["product.product"].browse(prod_search)

            if warehouse_id_to == 1:  # Uttara
                items[2]["available_qty"] = prod_browse.uttara_free_quantity
            elif warehouse_id_to == 2:  # Nodda
                items[2]["available_qty"] = prod_browse.nodda_free_quantity
            elif warehouse_id_to == 3:  # Signboard
                items[2]["available_qty"] = prod_browse.signboard_free_quantity
            elif warehouse_id_to == 7:  # Badda
                items[2]["available_qty"] = prod_browse.badda_free_quantity
            elif warehouse_id_to == 4:  # Uttara Damage
                items[2]["available_qty"] = prod_browse.uttara_damage_quantity
            elif warehouse_id_to == 5:  # Nodda Damage
                items[2]["available_qty"] = prod_browse.nodda_damage_quantity
            elif warehouse_id_to == 6:  # Signboard Damage
                items[2]["available_qty"] = prod_browse.signboard_damage_quantity
            elif warehouse_id_to == 9: # Badda Damage
                values['available_qty'] = prod_browse.badda_damage_quantity
            elif warehouse_id_to == 10: # Badda horeca
                values['available_qty'] = prod_browse.badda_horeca_free_quantity
            elif warehouse_id_to == 11: # Savar
                values['available_qty'] = prod_browse.savar_free_quantity
            elif warehouse_id_to == 12: # Savar Damage
                values['available_qty'] = prod_browse.savar_damage_quantity



            if items[2].get("transfer_quantity") > items[2]["available_qty"]:
                raise Warning(_(
                    "Requested/ Transferred Quantity can not greater than Free Quantity"
                ),
                )
            else:
                items[2]["requested_qty"] = items[2].get("transfer_quantity")

        record = super(StnProcess, self).create(vals)

        stn_number = (
                "STN/"
                + str(warehouse_code_dict.get(int(record.warehouse_id_to)))
                + "/"
                + str(warehouse_code_dict.get(record.warehouse_id_from.id))
                + "/0"
                + str(record.id)
        )
        record.name = stn_number

        return record

    # calls at the time of update record

    def write(self, vals):

        if (
                vals.get("warehouse_id_from") is not None
                and vals.get("warehouse_id_to") is not None
        ):
            if vals.get("warehouse_id_from") == vals.get("warehouse_id_to"):
                raise Warning(_(
                    "Source Location and Destination Location will be different. \n They will not be the same."))

        record = super(StnProcess, self).write(vals)

        stn_info = self.browse(self.ids[0])

        if (
                vals.get("stn_process_line") is not None
                and len(vals.get("stn_process_line")) > 0
                and str(stn_info.state) != str("draft")
        ):
            if stn_info.stock_id:
                #     # unreserve
                stn_info.stock_id.do_unreserve()

                # Cancel Action
                stn_info.stock_id.action_cancel()

            ## Generate Challan From Here
            id = self.ids[0] if len(self.ids) > 0 else None
            stn_data = self.browse(id)
            # stock_picking_type_ids = self.env["stock.picking.type"].search([("name", "like", "Delivery Orders")])
            stock_picking_type_data = self.env["stock.picking.type"].search([("name", "like", "Delivery Orders")])
            location_id = None
            location_dest_id = None
            picking_type_id = None

            for items in stock_picking_type_data:

                if int(items.warehouse_id.id) == int(stn_data.warehouse_id_to):
                    location_id = items.default_location_src_id.id
                    picking_type_id = items.id

                elif items.warehouse_id.id == stn_data.warehouse_id_from.id:
                    location_dest_id = items.default_location_src_id.id
            move_line = []

            stock_data = {}

            if location_id is not None and location_dest_id is not None:
                stock_data = {
                    "origin": stn_data.name,
                    "message_follower_ids": False,
                    "location_id": location_id,
                    # "carrier_tracking_ref": False,
                    # "number_of_packages": 0,
                    "date_done": False,
                    # "carrier_id": False,
                    "partner_id": False,
                    "message_ids": False,
                    "note": False,
                    "picking_type_id": picking_type_id,
                    "move_type": "one",
                    "company_id": 1,
                    "priority": "1",
                    "picking_type_code": False,
                    "owner_id": False,
                    "location_dest_id": location_dest_id,
                    # "min_date": False,
                    "date": strftime("%Y-%m-%d %H:%M:%S", gmtime()),
                    # "pack_operation_ids": [],
                    # "carrier_code": "custom",
                    # "invoice_state": "none",
                }
                tmp_dict = {}
                for stn_line in stn_data.stn_process_line:
                    move_line.append(
                        [
                            0,
                            False,
                            {
                                # "product_uos_qty": stn_line.transfer_quantity,
                                # "date_expected": strftime(
                                #     "%Y-%m-%d %H:%M:%S", gmtime()
                                # ),
                                "date": strftime(
                                    "%Y-%m-%d %H:%M:%S", gmtime()
                                ),
                                "product_id": stn_line.product_id.id,
                                "product_uom": stn_line.product_id.uom_id.id,
                                "picking_type_id": picking_type_id,
                                "product_uom_qty": stn_line.transfer_quantity,
                                # "invoice_state": "none",
                                 "product_tmpl_id": stn_line.product_id.product_tmpl_id.id,
                                # "product_uos": False,
                                # "reserved_quant_ids": [],
                                "location_dest_id": location_dest_id,
                                "procure_method": "make_to_stock",
                                # "product_packaging": False,
                                "group_id": False,
                                "location_id": location_id,
                                "name": str(stn_line.product_id.name),
                            },
                        ]
                    )

                stock_data["move_lines"] = move_line

            stock_obj = self.env["stock.picking"]
            save_the_data = stock_obj.create(stock_data)

            ## End Challan From Here

            ## Assign Challan From Here
            if save_the_data:
                save_the_data.action_confirm()
                save_the_data.action_assign()
                # if save_the_data:
                #     stock_obj_confirmation = stock_obj.action_confirm([save_the_data])
                #     stock_obj = stock_obj.action_assign([save_the_data])

                stn_data.write({'stock_id': save_the_data})

                # requested_stn_query = "UPDATE StnProcess SET stock_id='{0}' WHERE id={1}".format(
                #     save_the_data, id
                # )
                # self.env.cr.execute(requested_stn_query)
                # self.env.cr.commit()

        return record

    def send_stn(self):
        uid = self.env.user.id
        ids = self.ids
        stn_obj = self.browse(ids)
        sender = uid

        if stn_obj.create_uid.id == uid:
            raise Warning(_("You can not send it. Only Other warehouse people can do it."))

        for stn in stn_obj:
            values = {'state': 'send',
                      'send_time': fields.datetime.now(),
                      'send_by': self.env.user.id,
                      }
            stn.write(values)
            for spl in stn.stn_process_line:
                values = {'state': 'send',
                          'send_time': fields.Datetime.now()
                          }
                spl.write(values)

        # for single_id in ids:
        #     send_stn_query = "UPDATE stn_process SET state='send', send_time='{0}',send_by='{1}'  WHERE id={2}".format(
        #         str(fields.datetime.now()), sender, single_id
        #     )
        #     self.env.cr.execute(send_stn_query)
        #     self.env.cr.commit()
        #
        #     send_stn_query_line = "UPDATE stn_process_line SET state='send', send_time='{0}' WHERE stn_process_id={1}".format(
        #         str(fields.datetime.now()), single_id
        #     )
        #     self.env.cr.execute(send_stn_query_line)
        #     self.env.cr.commit()
        #
        # return True

    def cancel_stn(self):
        # uid = self.env.user.id
        ids = self.ids

        stn_obj = self.browse(ids)

        if stn_obj["state"] != "draft":

            if stn_obj.stock_id.id is not None or False:
                # Cancel Action
                stock_obj = self.env["stock.picking"].browse(int(stn_obj.stock_id.id))
                stock_obj.action_cancel()

        stn_obj.write({
            'state': 'cancel',
            'cancel_time': fields.datetime.now(),
        })
        stn_obj.stn_process_line.write({
            'state': 'cancel',
            'cancel_time': fields.datetime.now(),
        })

        # for single_id in ids:
        #     cancel_stn_query = "UPDATE stn_process SET state='cancel', cancel_time='{0}' WHERE id={1}".format(
        #         str(fields.datetime.now()), single_id
        #     )
        #     self.env.cr.execute(cancel_stn_query)
        #     self.env.cr.commit()
        #
        #     cancel_stn_query_line = "UPDATE stn_process_line SET state='cancel', cancel_time='{0}' WHERE stn_process_id={1}".format(
        #         str(fields.datetime.now()), single_id
        #     )
        #     self.env.cr.execute(cancel_stn_query_line)
        #     self.env.cr.commit()
        #
        # return True

    def receive_stn(self):
        self.ensure_one()
        # uid = self.env.uid
        # ids = self.ids
        #
        # uid = 1
        #
        # context = []
        # call transfer
        stn_obj = self

        # do_enter_transfer_details ai method missing v-14
        # stock_picking = self.env["stock.picking"].do_enter_transfer_details([stn_obj.stock_id.id])
        self.received_stn_stock_transfer(stn_obj)

        # picking_obj = self.stock_id if self.stock_id else None
        # # immediate_transfer_line_ids = [[0, False, {
        # #     'picking_id': picking_obj.id,
        # #     'to_immediate': True
        # # }]]
        # # # for picking_id in stock_list:
        # #
        # # res = self.env['stock.immediate.transfer'].create({
        # #     'pick_ids': [(4, picking_obj.id)],
        # #     'show_transfers': False,
        # #     'immediate_transfer_line_ids': immediate_transfer_line_ids
        # # })
        # #
        # # res.with_context(button_validate_picking_ids=res.pick_ids.ids).process()
        # # picking_obj.button_validate()
        #
        # if (
        #         str(stn_obj.stock_id.state) == "draft"
        #         or str(stn_obj.stock_id.state) == "confirmed"
        # ):
        #     # unreserve
        #     picking_obj.do_unreserve()
        #
        #     # action cancel
        #     picking_obj.action_assign()
        #
        # if str(stn_obj.stock_id.state) == "waiting":
        #     # Rereservation
        #     picking_obj.rereserve_pick()
        #
        # if stn_obj.stock_id.state != "assigned":
        #     raise Warning(_("Your Product is not reserved for this stn. It is already assigned to other orders"))
        # else:
        #     immediate_transfer_line_ids = [[0, False, {
        #         'picking_id': picking_obj.id,
        #         'to_immediate': True
        #     }]]
        #     # for picking_id in stock_list:
        #
        #     res = self.env['stock.immediate.transfer'].create({
        #         'pick_ids': [(4, picking_obj.id)],
        #         'show_transfers': False,
        #         'immediate_transfer_line_ids': immediate_transfer_line_ids
        #     })
        #
        #     res.with_context(button_validate_picking_ids=res.pick_ids.ids).process()

        #### "stock.transfer_details" model nai
        # trans_obj = self.env["stock.transfer_details"]
        # trans_search = trans_obj.search([("picking_id", "=", stn_obj.stock_id.id)])
        #
        # trans_search = (
        #     [trans_search[len(trans_search) - 1]]
        #     if len(trans_search) > 1
        #     else trans_search
        # )
        #
        # trans_browse = self.env["stock.transfer_details"].browse(trans_search)
        #
        # trans_browse.do_detailed_transfer()

        # for stn in stn_obj:
        values = {'state': 'receive',
                  'received_time': fields.datetime.now(),
                  'received_by': self.env.user.id,
                  }
        self.write(values)
        # for spl in stn.stn_process_line:
        # values =
        self.stn_process_line.write({'state': 'receive',
                                     'received_time': fields.Datetime.now()
                                     })

        # for single_id in ids:
        #     received_stn_query = "UPDATE stn_process SET state='receive', received_time='{0}', received_by='{1}' WHERE id={2}".format(
        #         str(fields.datetime.now()), receiver, single_id
        #     )
        #     self.env.cr.execute(received_stn_query)
        #     self.env.cr.commit()
        #
        #     received_stn_query_line = "UPDATE stn_process_line SET state='receive', received_time='{0}' WHERE stn_process_id={1}".format(
        #         str(fields.datetime.now()), single_id
        #     )
        #     self.env.cr.execute(received_stn_query_line)
        #     self.env.cr.commit()
        #
        # return True

    def received_stn_stock_transfer(self,stn_obj):
        self.ensure_one()

        # stn_obj = self

        picking_obj = self.stock_id if self.stock_id else None

        if (
                str(stn_obj.stock_id.state) == "draft"
                or str(stn_obj.stock_id.state) == "confirmed"
        ):
            # unreserve
            picking_obj.do_unreserve()

            # action cancel
            picking_obj.action_assign()

        if str(stn_obj.stock_id.state) == "waiting":
            # Rereservation
            picking_obj.rereserve_pick()

        if stn_obj.stock_id.state != "assigned":
            raise Warning(_("Your Product is not reserved for this stn. It is already assigned to other orders"))
        else:
            immediate_transfer_line_ids = [[0, False, {
                'picking_id': picking_obj.id,
                'to_immediate': True
            }]]
            # for picking_id in stock_list:

            res = self.env['stock.immediate.transfer'].create({
                'pick_ids': [(4, picking_obj.id)],
                'show_transfers': False,
                'immediate_transfer_line_ids': immediate_transfer_line_ids
            })

            res.with_context(button_validate_picking_ids=res.pick_ids.ids).process()

        return True

    def request_stn(self):
        id = self.ids[0] if len(self.ids) > 0 else None
        stn_data = self.browse(id)

        # stock_picking_type_ids = self.env["stock.picking.type"].search([("name", "like", "Delivery Orders")])
        # stock_picking_type_data = self.env["stock.picking.type"].browse(stock_picking_type_ids)

        stock_picking_type_data = self.env["stock.picking.type"].search([("name", "like", "Delivery Orders")])

        location_id = None
        location_dest_id = None
        picking_type_id = None

        for items in stock_picking_type_data:
            ## nichar 3line test korar jonno likachi
            # location_id = items.default_location_src_id.id
            # location_dest_id = items.default_location_src_id.id
            # picking_type_id = items.id

            if items.warehouse_id.id == int(stn_data.warehouse_id_to):
                location_id = items.default_location_src_id.id
                picking_type_id = items.id

            elif items.warehouse_id.id == stn_data.warehouse_id_from.id:
                location_dest_id = items.default_location_src_id.id
                # location_id = items.default_location_src_id.id
                # picking_type_id = items.id
        move_line = []

        stock_data = {}

        if location_id is not None and location_dest_id is not None:
            stock_data = {
                "origin": stn_data.name,
                "message_follower_ids": False,
                "carrier_tracking_ref": False,
                # "number_of_packages": 0,
                "date_done": False,
                "carrier_id": False,
                "write_uid": False,
                "partner_id": False,
                "message_ids": False,
                "note": False,
                "picking_type_id": picking_type_id,
                "move_type": "one",
                "company_id": self.env.company.id,
                "priority": "1",
                "picking_type_code": False,
                "owner_id": False,
                # "min_date": False,
                "date": strftime("%Y-%m-%d %H:%M:%S", gmtime()),
                "location_id": location_id,
                "location_dest_id": location_dest_id,
                # "pack_operation_ids": [],
                "carrier_code": "custom",
                # "invoice_state": "none",
            }
            tmp_dict = {}
            for stn_line in stn_data.stn_process_line:
                move_line.append(
                    [
                        0,
                        False,
                        {
                            # "product_uos_qty": stn_line.transfer_quantity,
                            # "date_expected": strftime(
                            #     "%Y-%m-%d %H:%M:%S", gmtime()
                            # ),
                            "date": strftime("%Y-%m-%d %H:%M:%S", gmtime()),
                            "product_id": stn_line.product_id.id,
                            "product_uom": stn_line.product_id.uom_id.id,
                            "picking_type_id": picking_type_id,
                            "product_uom_qty": stn_line.transfer_quantity,
                            # "invoice_state": "none",
                            "product_tmpl_id": stn_line.product_id.product_tmpl_id.id,
                            # "product_uos": False,
                            # "reserved_quant_ids": [],
                            "location_dest_id": location_dest_id,
                            "procure_method": "make_to_stock",
                            # "product_packaging": False,
                            "group_id": False,
                            "location_id": location_id,
                            "name": str(stn_line.product_id.name),
                        },
                    ]
                )

            stock_data["move_lines"] = move_line

        stock_obj = self.env["stock.picking"]
        save_the_data = stock_obj.create(stock_data)
        if save_the_data:
            save_the_data.action_confirm()
            save_the_data.action_assign()

        for stn in stn_data:
            values = {'state': 'requested',
                      'requested_time': fields.datetime.now(),
                      'stock_id': save_the_data,
                      'requested_by': self.env.user.id,
                      }
            stn.write(values)
            for spl in stn.stn_process_line:
                values = {'state': 'requested',
                          'requested_time': fields.Datetime.now()
                          }
                spl.write(values)

        # for single_id in ids:
        #     requested_stn_query = "UPDATE stn_process SET state='requested', requested_time='{0}', stock_id='{1}', requested_by='{2}' WHERE id={3}".format(
        #         str(fields.datetime.now()),
        #         save_the_data,
        #         request_user,
        #         single_id,
        #     )
        #     self.env.cr.execute(requested_stn_query)
        #     self.env.cr.commit()
        #
        #     requested_stn_query_line = "UPDATE stn_process_line SET state='requested', requested_time='{0}' WHERE stn_process_id={1}".format(
        #         str(fields.datetime.now()), single_id
        #     )
        #     self.env.cr.execute(requested_stn_query_line)
        #     self.env.cr.commit()
        #
        # return True


class StnProcessLine(models.Model):
    _name = "stn.process.line"
    _description = "STN Process Line"

    stn_process_id = fields.Many2one("stn.process", "STN Process ID", required=True, ondelete="cascade", index=True,
                                     readonly=True, )
    product_id = fields.Many2one("product.product", "Product", required=True)
    send_time = fields.Datetime("Sending Time")
    cancel_time = fields.Datetime("Cancel Time")
    received_time = fields.Datetime("Received Time")
    requested_time = fields.Datetime("Requested Time")
    transfer_quantity = fields.Float("Transfer Qty", required=True)
    available_qty = fields.Float("Free Qty")
    requested_qty = fields.Float("Requested Qty")

    state = fields.Selection([
        ("draft", "Created"),
        ("requested", "Requested"),
        ("send", "Confirmed"),
        ("receive", "Received"),
        ("cancel", "Cancelled"),
    ], "Status", default='draft', help="Gives the status of the STN Process line", index=True, )

    ### Block code for V-14
    # _columns = {
    #     "stn_process_id": fields.many2one(
    #         "stn.processs",
    #         "STN Process ID",
    #         required=True,
    #         ondelete="cascade",
    #         index=True,
    #         readonly=True,
    #     ),
    #     "product_id": fields.many2one(
    #         "product.product", "Product", required=True
    #     ),
    #     "send_time": fields.datetime("Sending Time"),
    #     "cancel_time": fields.datetime("Cancel Time"),
    #     "received_time": fields.datetime("Received Time"),
    #     "requested_time": fields.datetime("Requested Time"),
    #     "transfer_quantity": fields.float("Transfer Qty", required=True),
    #     "available_qty": fields.float("Free Qty"),
    #     "requested_qty": fields.float("Requested Qty"),
    #     "state": fields.selection(
    #         [
    #             ("draft", "Created"),
    #             ("requested", "Requested"),
    #             ("send", "Confirmed"),
    #             ("receive", "Received"),
    #             ("cancel", "Cancelled"),
    #         ],
    #         "Status",
    #         help="Gives the status of the STN Process line",
    #         index=True,
    #     ),
    # }
    @api.onchange('product_id')
    def onchange_product_id(self):
        warehouse_id_to = self._context.get("warehouse_id_to")

        if not warehouse_id_to:
            raise Warning(_("You must first select a Destination Warehouse"))

        values = {}
        # product_id = False

        if self.product_id:
            prod_obj = self.env["product.product"].search([("id", "=", self.product_id.id)])
            # uid = 1
            # prod_search = prod_obj.search([("id", "=", self.product_id.id)])
            # prod_browse = self.env["product.product"].browse(prod_search)
            # free = prod_browse.uttwh_free_quantity
            # bb = free

            if int(warehouse_id_to) == 1:  # Uttara
                values["available_qty"] = prod_obj.uttara_free_quantity
            elif int(warehouse_id_to) == 2:  # Nodda
                values["available_qty"] = prod_obj.nodda_free_quantity
            elif int(warehouse_id_to) == 3:  # Signboard
                values["available_qty"] = prod_obj.signboard_free_quantity
            elif int(warehouse_id_to) == 7:  # Badda
                values["available_qty"] = prod_obj.badda_free_quantity
            elif int(warehouse_id_to) == 4:  # Uttara Damage
                values["available_qty"] = prod_obj.uttara_damage_quantity
            elif int(warehouse_id_to) == 5:  # Nodda Damage
                values["available_qty"] = prod_obj.nodda_damage_quantity
            elif int(warehouse_id_to) == 6:  # Signboard Damage
                values["available_qty"] = prod_obj.signboard_damage_quantity
            elif int(warehouse_id_to) == 9: # Badda Damage
                values['available_qty'] = prod_obj.badda_damage_quantity
            elif int(warehouse_id_to) == 10: # Badda horeca
                values['available_qty'] = prod_obj.badda_horeca_free_quantity
            elif int(warehouse_id_to) == 11: # Savar
                values['available_qty'] = prod_obj.savar_free_quantity
            elif int(warehouse_id_to) == 12: # Savar Damage
                values['available_qty'] = prod_obj.savar_damage_quantity



        return {"value": values}


class CancelStnProcessReason(models.Model):
    _name = "cancel.stn.process.reason"
    _description = "Cancel STN Process Reason"

    cancel_stn_process_date = fields.Datetime('Cancel STN Process Date')
    cancel_stn_process_by = fields.Many2one("res.users", "Cancel STN Process By")
    cancel_stn_process_reason = fields.Text("Cancel STN Process Reason", required=True)

    ### Block code for V-14
    # _columns = {
    #     "cancel_stn_process_date": fields.datetime("Cancel STN Process Date"),
    #     "cancel_stn_process_by": fields.many2one(
    #         "res.users", "Cancel STN Process By"
    #     ),
    #     "cancel_stn_process_reason": fields.text(
    #         "Cancel STN Process Reason", required=True
    #     ),
    # }

    def cancel_stn_process_reason_def(self):
        context = []
        uid = self.env.user.id
        ids = self.env.context["active_ids"]
        cancel_stn_process_reason = str(self.env.context["cancel_stn_process_reason"])
        # cancel_stn_process_by = uid
        # cancel_stn_process_date = fields.datetime.now()

        stn_obj = self.env["stn.process"].browse(ids)
        stn_obj.write({
            'cancel_stn_process_reason': cancel_stn_process_reason,
            'cancel_stn_process_by': self.env.user.id,
            'cancel_stn_process_date': fields.datetime.now(),
        })
        stn_obj.cancel_stn()

        # for s_id in ids:
        #     cancel_stn_proces_query = "UPDATE stn_process SET cancel_stn_process_reason='{0}', cancel_stn_process_by={1}, cancel_stn_process_date='{2}' WHERE id={3}".format(
        #         cancel_stn_process_reason,
        #         cancel_stn_process_by,
        #         cancel_stn_process_date,
        #         s_id,
        #     )
        #     self.env.cr.execute(cancel_stn_proces_query)
        #     self.env.cr.commit()
        #
        #     stn_obj.cancel_stn([s_id])
        #
        # return True


class ProductProduct(models.Model):
    _inherit = "product.product"

    def _get_stn_intransit_qty(self):

        stn_search = self.env["stn.process"].search([("state", "in", ["requested", "send"])])

        for p_id in self:
            t_qty = 0
            for items in stn_search:
                for stn_line in items.stn_process_line:
                    if p_id == stn_line.product_id:
                        t_qty += stn_line.transfer_quantity
            p_id.stn_in_transit_qty = t_qty

    stn_in_transit_qty = fields.Float(string="In transit Qty", compute='_get_stn_intransit_qty')

    # _columns = {
    #     "stn_in_transit_qty": fields.function(
    #         _get_stn_intransit_qty, string="In transit Qty", type="float"
    #     ),
    # }


class Users(models.Model):
    _name = 'res.users'
    _inherit = ['res.users']

    # @api.model
    # def _default_warehouse_id(self):
    #     return self.env['stock.warehouse'].search([('company_id', '=', self.env.company.id)], limit=1)
    #
    # warehouse_id = fields.Many2one('stock.warehouse', 'Warehouse', default=lambda self: self._default_warehouse_id())
