{
    "name": "Delivered and Return Quantity In Sale Order",
    "version": "14.0.1.0.0",
    "summary": """Quantity Delivered and Invoiced on Sale Line
    """,
    "author": "Odoo Bangladesh",
    "website": "",
    "depends": ["sale_stock", "odoo_magento_connect"],
    "data": [
            "views/sale_order.xml"
            ],
}
