import logging
from odoo import models, fields, api, _

_logger = logging.getLogger(__name__)


class StockMove(models.Model):
    _inherit = "stock.move"

    ###  'sale_line_id'  field alrady acha, inherit korachi field

    sale_line_id = fields.Many2one('sale.order.line', string="Sale Order Line", readonly=True, ondelete="set null")

    ### 'procurement_id' ai field nai, tai error dicha, tai close
    # sale_line_id = fields.Many2one(
    #     related="procurement_id.sale_line_id",
    #     string="Sale Order Line",
    #     readonly=True,
    #     store=True,
    #     ondelete="set null",
    # )
