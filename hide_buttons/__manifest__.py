# -*- coding: utf-8 -*-
{
    'name': "Hide buttons",
    'summary': """ This module will help you to Hide buttons based on model """,
    'description': """ Hide buttons based on model """,
    'author': "Sindabad",
    'category': 'Customise',
    'depends': ['base','sale','auto_confirm_resync_recalc_order'],
    'license': 'AGPL-3',
    'data': [
        'security/hide_buttons.xml',
        'views/hide_buttons_view.xml',
    ],
    'installable': True,
    'application': True,
}
