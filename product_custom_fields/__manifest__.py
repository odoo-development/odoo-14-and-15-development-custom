# Copyright 2021 Rocky

{
    "name": "Products Custom Fields",
    "version": "14.0",
    "author": "Rocky",
    "category": "Sales Management",
    "license": "AGPL-3",
    "depends": ['product'],
    "data": [
        "views/product_view.xml",
    ],
    "installable": True,
}
