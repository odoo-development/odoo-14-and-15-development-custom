# Copyright 2021 Rocky

from odoo import fields, models, api, _


class ProductProduct(models.Model):
    _name = 'product.product'
    _inherit = 'product.product'

    x_products_location = fields.Char(string="Products Location")
    x_odoo_magento_mismatch = fields.Boolean(string="Odoo Magento Mismatch")


class ProductTemplate(models.Model):
    _inherit = "product.template"

    prod_uom = fields.Char('UOM')
    product_name = fields.Char('Product Name')
    warranty_applicable = fields.Boolean('Warranty Applicable')
    warranty_duration = fields.Integer("Warranty (Month)")
    warranty_text = fields.Text('Warranty Rule')

