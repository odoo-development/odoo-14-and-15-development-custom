import json
import requests

TOKEN_API = '/integration/admin/token'


# passes magento.configure object
def get_magento_token(self, sale_order_ids):

    instance_id = 1
    # import pdb;pdb.set_trace()
    if sale_order_ids:
        for mag_ord_id in self.env['connector.order.mapping'].search([('odoo_order_id', '=', sale_order_ids[0])]):
            # for mag_obj in self.env['connector.order.mapping'].browse([mag_ord_id]):
            instance_id = mag_ord_id.instance_id.id
                # instance_id = mag_obj.id

    obj = self.env['connector.instance'].browse([instance_id])

    """
    obj
    magento.configure(1, )
    """
    url = obj.name + "/index.php/rest/V1" + TOKEN_API
    user = obj.user
    pwd = obj.pwd

    cred_dict = {"username": user, "password": pwd}
    cred = json.dumps(cred_dict)

    header = {"Content-type": "application/json"}
    res = requests.post(str(url), data=cred, headers=header)

    token = None
    if res.status_code == 200:
        token = "Bearer " + str(res.json())

    return token, obj.name


# def get_magento_token(self_obj, cr, uid, order_ids):
    ## order_ids = [13673, 13672, 13671, 13670, 13669, 13668]
    # instance_id = 1
    #
    # if order_ids:
    #     for mag_ord_id in self_obj.pool.get('magento.orders').search(cr, uid, [('order_ref', '=', order_ids[0])]):
    #         for mag_obj in self_obj.pool.get('magento.orders').browse(cr, uid, [mag_ord_id]):
    #             instance_id = mag_obj.instance_id.id
    #
    # obj = self_obj.pool.get('magento.configure').browse(cr, uid, [instance_id])
    #
    # """
    # obj
    # magento.configure(1, )
    # """
    # url = obj.name + "/index.php/rest/V1" + TOKEN_API
    # user = obj.user
    # pwd = obj.pwd
    #
    # cred_dict = {"username": user, "password": pwd}
    # cred = json.dumps(cred_dict)
    #
    # header = {"Content-type": "application/json"}
    # res = requests.post(str(url), data=cred, headers=header)
    #
    # token = None
    # if res.status_code == 200:
    #     token = "Bearer " + str(res.json())
    #
    # return token, obj.name


def submit_request(url, token, data, obj=None):
    token = token.replace('"', "")
    headers = {'Authorization': token, 'Content-Type': 'application/json'}

    res = requests.post(url, data=data, headers=headers)

    return res.status_code
