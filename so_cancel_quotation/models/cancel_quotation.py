import datetime
from odoo import _,api, fields, models
from odoo.exceptions import UserError, ValidationError

class SaleOrder(models.Model):
    _inherit = 'sale.order'

    cancel_quotation_date = fields.Datetime(string='Quotation Cancel Date')
    cancel_quotation_by = fields.Many2one('res.users', string='Quotation Cancel By')
    cancel_quotation_reason = fields.Selection([

        ("product_unavailability", "Product unavailability"),
        ("delay_delivery", "Delay delivery"),
        ("pricing_issue", "Pricing issue"),
        ("wrong_order", "Wrong order"),
        ("cash_problem", "Cash problem"),
        ("damage_product", "Damage product"),
        ("double_ordered", "Double ordered"),
        ("sample_not_approved", "Sample not approved"),
        ("wrong_sku", "Wrong SKU"),
        ("not_interested", "Not interested"),
        ("wrong_product", "Wrong product"),
        ("quality_issue", "Quality issue"),
        ("test_order", "Test Order"),
        ("payment_not_done", "Payment not done"),
        ("apnd", "Advance payment not done"),
        ("invalid_information", "Invalid Information"),
        ("customer_unreachable", "Customer unreachable"),
        ("same_day_delivery", "Same day delivery"),
        ("fake_order", "Fake Order"),
        ("sourcing_delay", "Sourcing Delay"),
        ("reorder", "Reorder"),
        ("discount_issue", "Discount Issue"),
        ("product_not_available_customer_will_take_alternative_product",
         "Product not available, Customer will take alternative product"),

    ], string='Quotation Cancel Reason', copy=False, help="Reasons")


class SOCancelQuotationProcess(models.Model):
    _name = "so.cancel.quotation.process"
    _description = "SO Cancel Quotation Process"

    cancel_quotation_date = fields.Datetime(string='Quotation Cancel Date')
    cancel_quotation_by = fields.Many2one('res.users', string='Quotation Cancel By')
    cancel_quotation_reason = fields.Selection([

        ("product_unavailability", "Product unavailability"),
        ("delay_delivery", "Delay delivery"),
        ("pricing_issue", "Pricing issue"),
        ("wrong_order", "Wrong order"),
        ("cash_problem", "Cash problem"),
        ("damage_product", "Damage product"),
        ("double_ordered", "Double ordered"),
        ("sample_not_approved", "Sample not approved"),
        ("wrong_sku", "Wrong SKU"),
        ("not_interested", "Not interested"),
        ("wrong_product", "Wrong product"),
        ("quality_issue", "Quality issue"),
        ("test_order", "Test Order"),
        ("payment_not_done", "Payment not done"),
        ("apnd", "Advance payment not done"),
        ("invalid_information", "Invalid Information"),
        ("customer_unreachable", "Customer unreachable"),
        ("same_day_delivery", "Same day delivery"),
        ("fake_order", "Fake Order"),
        ("sourcing_delay", "Sourcing Delay"),
        ("reorder", "Reorder"),
        ("discount_issue", "Discount Issue"),
        ("product_not_available_customer_will_take_alternative_product",
         "Product not available, Customer will take alternative product"),

    ], string='Quotation Cancel Reason', copy=False, help="Reasons")

    def so_cancel_quotation(self):

        ids = self.env.context['active_ids']
        cancel_quotation_date = self._context.get('date') or fields.Date.today()
        cancel_quotation_by = self.env.uid
        cancel_quotation_reason = self.env.context['cancel_quotation_reason']

        for id in ids:

            try:
                if cancel_quotation_reason:

                    cancel_quotation_query = "UPDATE sale_order SET cancel_quotation_date='{0}', cancel_quotation_by='{1}', cancel_quotation_reason='{2}' WHERE id='{3}'".format(cancel_quotation_date, cancel_quotation_by, cancel_quotation_reason, id)
                    self.env.cr.execute(cancel_quotation_query)
                    self.env.cr.commit()
                    self.env['sale.order'].browse(id).action_cancel()
                # else:
                #     raise except_orm('Cancel Reason', 'Can not submit blank reason!!!')

            except:
                pass

        return True
