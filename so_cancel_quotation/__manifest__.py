{
    'name': 'SO Cancel Quotation',
    'version': '1.0',
    'category': 'SO Cancel Quotation',
    'author': 'sindabad',
    'summary': 'Sales Order, Cancel Quotation',
    'description': 'SO Cancel Quotation',
    'depends': ['sale'],
    'data': [
        'security/so_cancel_quatation_security.xml',
        'security/ir.model.access.csv',
        'wizard/order_cancel.xml',
        'views/cancel_quotation.xml',
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}
