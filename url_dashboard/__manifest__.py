# Copyright 2021 Rocky

{
    "name": "All Business Dashboard ",
    "version": "13.0.1",
    "author": "Rocky",
    "category": "Sales Management",
    "license": "AGPL-3",
    "depends": ['base', 'sale_management'],
    "data": [
         "views/url_dashboard.xml",
    ],
    "installable": True,
}
