{
    'name': "Stock Location in Move_line",
    'version': "14.0.1.0.0",
    'summary': """
        This module will help to link stock location in each stock move """,
    'description': """
        This module will help to link stock location in each stock move
    """,
    'author': "Odoo Bangladesh",
    'website': " ",
    'category': 'Uncategorized',
    # any module necessary for this one to work correctly
    'depends': [
            'odoo_magento_connect', 'stock',
    ],

    # always loaded
    'data': [
        'security/stock_location_in_moveline.xml',
        # 'security/ir.model.access.csv',
        # 'views/stock_location_in_moveline.xml',
    ],

}
