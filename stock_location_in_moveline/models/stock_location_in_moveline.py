from datetime import date, datetime

from odoo import fields
from odoo.tools.translate import _
from odoo.exceptions import Warning
from odoo import SUPERUSER_ID, api, models


# reload(sys)
# sys.setdefaultencoding('utf8')


# class StockPackOperation(models.Model):
#     _inherit = "stock.pack.operation"
#
#     product_location = fields.Char('Product Location')
#
#     # _columns = {
#     #     'product_location': fields.char('Product Location'),
#     # }


class StockMove(models.Model):
    _inherit = "stock.move"

    def _get_product_location(self):
        res = dict.fromkeys(self.ids, 0)
        data = []
        for move in self.browse():

            data = []
            for q in move.reserved_quant_ids:
                for r in q.history_ids:
                    if r.origin and 'PO' in r.origin:
                        smol = self.env['stock.move.operation.link']
                        smol_obj = smol.search([('move_id', '=', r.id)])
                        po_location = smol.browse(smol_obj[0]).operation_id.product_location
                        if po_location:
                            data.append(po_location)

            # res[move.id] = len(data) > 0 and ",".join(str(r) for r in data)
            res[move.id] = ", ".join(map(str, data)) if len(data) > 0 else ''
            # print '_get_product_location', res[move.id]
        return res

    product_location_list = fields.Char(compute='_get_product_location', string='Product Location', )

    # ## Block code for V-14
    # _columns = {
    #     'product_location_list': fields.function(_get_product_location, string='Product Location', type='char'),
    # }


# ----------------------------------------------------
# Stock Move Details
# ----------------------------------------------------
#
#
# class StockTransferDetails(models.Model):
#     _inherit = 'stock.transfer_details'
#
#     def default_get(self, fields):
#         # if context is None:
#         #     context = {}
#         res = super(StockTransferDetails, self).default_get(fields)
#         picking_ids = self._context.get('active_ids', [])
#         active_model = self._context.get('active_model')
#
#         if not picking_ids or len(picking_ids) != 1:
#             # Partial Picking Processing may only be done for one picking at a time
#             return res
#         assert active_model in ('stock.picking'), 'Bad context propagation'
#         picking_id, = picking_ids
#         picking = self.env['stock.picking'].browse(picking_id)
#         items = []
#         packs = []
#         if not picking.pack_operation_ids:
#             picking.do_prepare_partial()
#         for op in picking.pack_operation_ids:
#             if op.product_location:
#                 product_location = op.product_location
#             else:
#                 product_location = op.product_id.x_products_location
#             item = {
#                 'packop_id': op.id,
#                 'product_id': op.product_id.id,
#                 'product_uom_id': op.product_uom_id.id,
#                 'quantity': op.product_qty,
#                 'product_location': product_location,
#                 'package_id': op.package_id.id,
#                 'lot_id': op.lot_id.id,
#                 'sourceloc_id': op.location_id.id,
#                 'destinationloc_id': op.location_dest_id.id,
#                 'result_package_id': op.result_package_id.id,
#                 'date': op.date,
#                 'owner_id': op.owner_id.id,
#             }
#             if op.product_id:
#                 items.append(item)
#             elif op.package_id:
#                 packs.append(item)
#         res.update(item_ids=items)
#         res.update(packop_ids=packs)
#         return res
#
#     def do_detailed_transfer(self):
#         if self.picking_id.state not in ['assigned', 'partially_available']:
#             raise Warning(_('You cannot transfer a picking in state \'%s\'.') % self.picking_id.state)
#         processed_ids = []
#         # Create new and update existing pack operations
#         for lstits in [self.item_ids, self.packop_ids]:
#             for prod in lstits:
#                 pack_datas = {
#                     'product_id': prod.product_id.id,
#                     'product_uom_id': prod.product_uom_id.id,
#                     'product_qty': prod.quantity,
#                     'product_location': prod.product_location,
#                     'package_id': prod.package_id.id,
#                     'lot_id': prod.lot_id.id,
#                     'location_id': prod.sourceloc_id.id,
#                     'location_dest_id': prod.destinationloc_id.id,
#                     'result_package_id': prod.result_package_id.id,
#                     'date': prod.date if prod.date else datetime.now(),
#                     'owner_id': prod.owner_id.id,
#                 }
#                 if prod.packop_id:
#                     prod.packop_id.with_context(no_recompute=True).write(pack_datas)
#                     processed_ids.append(prod.packop_id.id)
#                 else:
#                     pack_datas['picking_id'] = self.picking_id.id
#                     packop_id = self.env['stock.pack.operation'].create(pack_datas)
#                     processed_ids.append(packop_id.id)
#         # Delete the others
#         packops = self.env['stock.pack.operation'].search(
#             ['&', ('picking_id', '=', self.picking_id.id), '!', ('id', 'in', processed_ids)])
#         packops.unlink()
#
#         # Execute the transfer of the picking
#         self.picking_id.do_transfer()
#
#
# class StockTransferDetailsItems(models.Model):
#     _inherit = 'stock.transfer_details_items'
#
#     product_location = fields.Char('Product Location')
#
#     # _columns = {
#     #     'product_location': fields.char('Product Location'),
#     # }
