{
    'name': 'WMS Cash management',
    'version': "14.0.1.0.0",
    'author': "Odoo Bangladesh",
    'category': 'Sale',
    'summary': 'WMS Cash Management/ Warehouse Management System',
    'depends': ['base', 'odoo_magento_connect', 'account', 'sale_management', 'purchase', 'wms_manifest', 'send_sms_on_demand','wms_manifest_extends'],
    'data': [
        'security/bulk_security.xml',
        'security/ir.model.access.csv',
        'wizard/bulk_payment_data_load.xml',
        'views/bulk_order_payment_view.xml',
        'views/wms_manifest_add_button.xml',
        'views/wms_delivered_menu.xml',

    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}
