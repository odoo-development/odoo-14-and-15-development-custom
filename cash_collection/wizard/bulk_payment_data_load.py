import json
import logging
from odoo import api
from odoo import api, fields, models, _
from odoo.exceptions import RedirectWarning, UserError, ValidationError, AccessError


_logger = logging.getLogger(__name__)


class BulkPaymentDataLoad(models.Model):
    _name = "bulk.payment.data.load"
    _description = "Bulk Payment Data Load"

    total_cash_amount = fields.Float("Total Cash Amount")
    total_bank_amount = fields.Float("Total Advance/Paid Amount")
    total_receiving_amount = fields.Float("Total Receiving Amount")
    due_amount = fields.Float('Total Due Amount')
    cash_payment_method = fields.Many2one('account.journal', 'Cash Payment Method', required=True)
    bank_payment_method = fields.Many2one('account.journal', 'Bank Payment Method', required=True)
    # period_id = fields.Many2one('account.period', 'Period', required=True)
    manifest_id = fields.Many2one('wms.manifest.process', 'Manifest ID', required=True)
    line_ids = fields.One2many("bulk.payment.data.load.line", "bulk_order_payment_id", "Bulk Invoice payment line", required=True)
    # bulk_order_payment_line_ids = fields.One2many("bulk.payment.data.load.line", "bulk_order_payment_id",
    #                                            "Bulk Invoice payment line",
    #                                            required=True)

    # ### Block code for v-14
    # _columns = {
    #     'total_cash_amount': fields.float("Total Cash Amount"),
    #     'total_bank_amount': fields.float("Total Advance/Paid Amount"),
    #     'total_receiving_amount': fields.float("Total Receiving Amount"),
    #     'due_amount': fields.float('Total Due Amount'),
    #     'cash_payment_method': fields.many2one('account.journal', 'Cash Payment Method', required=True),
    #     'bank_payment_method': fields.many2one('account.journal', 'Bank Payment Method', required=True),
    #     'period_id': fields.many2one('account.period', 'Period', required=True),
    #     'manifest_id': fields.many2one('wms.manifest.process', 'Manifest ID', required=True),
    #     'bulk_order_payment_line': fields.one2many('bulk.payment.data.load.line', 'bulk_order_payment_id',
    #                                                'Bulk Invoice payment line',
    #                                                required=True),
    #
    # }

    # def _get_period(self):
    #     context = None
    #     if context is None: context = {}
    #     if context.get('period_id', False):
    #         return context.get('period_id')
    #     periods = self.env['account.period'].find(context=context)
    #     return periods and periods[0] or False

    def default_get(self, fields):
        # context=None
        res = super(BulkPaymentDataLoad, self).default_get(fields)
        manifest_data = self.env['wms.manifest.process'].browse(self._context['active_id'])
        res['manifest_id'] = manifest_data.id
        res['total_cash_amount'] = manifest_data.total_cash_amount
        res['total_receiving_amount'] = manifest_data.total_cash_amount
        res['total_bank_amount'] = manifest_data.total_paid_amount
        res['due_amount'] = 0

        # periods = self.env['account.period'].find()
        # if len(periods) > 0:
        #     res['period_id'] = periods[0]

        line_vals = []

        for man_item in manifest_data.picking_line:
            inv_type = str(man_item.payment_type)

            if man_item.amount > 0 and (man_item.full_delivered == True or man_item.partial_delivered == True):

                # if 'Cash On Delivery' in inv_type or 'PAID' in inv_type or 'Mobile Banking' in inv_type or 'Credit/Debit Cards' in inv_type:
                if 'Cash On Delivery' in inv_type:
                    line_vals.append([0, False, {
                        'invoice_id': man_item.invoice_id,
                        'mag_no': man_item.magento_no,
                        'invoiced_amount': man_item.amount,
                        'due_amount': 0,
                        'receiving_amount': man_item.delivered_amount,
                        'manifest_line_id': man_item.id,
                        'inv_pay_type': man_item.payment_type
                    }])

        res['line_ids'] = line_vals

        return res

    def wkf_confirm_order(self):
        for items in self:
            payments_vals = {}
            line_vals = []
            for man_item in items.line_ids:
                line_vals.append([0, False, {
                    'invoice_id': man_item.invoice_id.id,
                    'mag_no': man_item.mag_no,
                    'invoiced_amount': man_item.invoiced_amount,
                    'receiving_amount': man_item.receiving_amount,
                    'manifest_line_id': man_item.manifest_line_id.id,
                    'due_amount': man_item.due_amount,
                    'inv_pay_type': man_item.inv_pay_type
                }])
            if len(line_vals) > 0:
                # vals['period_id'] = items.period_id.id
                payments_vals['cash_receiving_amount'] = items.total_cash_amount
                payments_vals['bank_receiving_amount'] = items.total_bank_amount

                payments_vals['receiving_amount'] = items.total_receiving_amount
                payments_vals['cash_payment_method'] = items.cash_payment_method.id
                payments_vals['bank_payment_method'] = items.bank_payment_method.id
                payments_vals['due_amount'] = items.due_amount
                payments_vals['bulk_order_payment_line'] = line_vals

                statement_id = self.env['bulk.order.payment'].create(payments_vals)
                statement_id.confirm_the_payment()

                items.manifest_id.write({
                    'bulk_payment_id': statement_id.id,
                    'state': 'closed',
                })

        for line in self.line_ids:
            invoice = line.invoice_id
            try:
                vals = {}
                ###########################################################################
                # Check suggested outstanding payments.
                to_reconcile_payments_widget_vals = json.loads(invoice.invoice_outstanding_credits_debits_widget)
                current_amounts = {vals['move_id']: vals['amount'] for vals in
                                   to_reconcile_payments_widget_vals['content']}

                # Reconcile
                pay_term_lines = invoice.line_ids.filtered(lambda line: line.account_id.user_type_id.type in 'receivable')
                to_reconciles = self.env['account.move'].browse(list(current_amounts.keys())) \
                    .line_ids \
                    .filtered(lambda line: line.account_id == pay_term_lines.account_id)

                for to_reconcile in to_reconciles:
                    if pay_term_lines.move_id.ref in to_reconcile.move_id.ref:
                        (pay_term_lines + to_reconcile).reconcile()
            except:
                pass
                # raise UserError(
                #     _("You are trying to reconcile some entries that are already reconciled. \n %s and %s and %s") % (
                #     invoice.ref, invoice.name, line))
            ###########################################################################

    # @api.onchange('bulk_order_payment_line_ids')
    def test_code(self):
        total_recv_sum = 0
        total_due_sum = 0
        total_cash_sum = 0
        total_bank_sum = 0
        for line_item in self.line_ids:
            if line_item.inv_pay_type and 'Cash On Delivery' in line_item.inv_pay_type:
                total_cash_sum = total_cash_sum + line_item.receiving_amount
            if 'PAID' in line_item.inv_pay_type or 'Mobile Banking' in line_item.inv_pay_type or 'Credit/Debit Cards' in line_item.inv_pay_type:
                total_bank_sum = total_bank_sum + line_item.receiving_amount

            total_recv_sum = total_recv_sum + line_item.receiving_amount
            total_due_sum = total_due_sum + line_item.due_amount
        self.total_cash_amount = total_cash_sum - total_due_sum
        self.total_receiving_amount = total_recv_sum - total_due_sum
        self.total_bank_amount = total_bank_sum

        self.due_amount = total_due_sum
        return 'ss'


class BulkPaymentDataLoadLine(models.Model):
    _name = "bulk.payment.data.load.line"
    _description = "Bulk Payment Data Load Line"

    bulk_order_payment_id = fields.Many2one("bulk.payment.data.load", 'Bulk Order Payment ID', required=True,
                                             ondelete='cascade', index=True, readonly=True)

    invoice_id = fields.Many2one('account.move', string="Invoice No")
    mag_no = fields.Char('Magento No')
    order_id = fields.Many2one('sale.order', string="Order No.")
    invoiced_amount = fields.Float('Invoiced Amount')
    receiving_amount = fields.Float('Receiving Amount')
    due_amount = fields.Float('Due Amount')
    journal_id = fields.Many2one('account.move', 'Journal ')
    manifest_line_id = fields.Many2one('wms.manifest.line', 'Manifest Line ')
    inv_pay_type = fields.Char('Invoice Payment Type')

    # ### Block code for v-14
    # _columns = {
    #
    #     'bulk_order_payment_id': fields.many2one('bulk.payment.data.load', 'Bulk Order Payment ID', required=True,
    #                                              ondelete='cascade', index=True, readonly=True),
    #
    #     'invoice_id': fields.many2one('account.invoice', string="Invoice No"),
    #     'mag_no': fields.char('Magento No'),
    #     'order_id': fields.many2one('sale.order', string="Order No."),
    #     'invoiced_amount': fields.float('Invoiced Amount'),
    #     'receiving_amount': fields.float('Receiving Amount'),
    #     'due_amount': fields.float('Due Amount'),
    #     'journal_id': fields.many2one('account.move', 'Journal '),
    #     'manifest_line_id': fields.many2one('wms.manifest.line', 'Manifest Line '),
    #     'inv_pay_type': fields.char('Invoice Payment Type'),
    #
    # }

    @api.onchange('receiving_amount')
    def mag_on_select(self):
        if self.receiving_amount:
            self.due_amount = float(self.invoiced_amount) - float(self.receiving_amount)

        return "xXxXxXxXxX"
