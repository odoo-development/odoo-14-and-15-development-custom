from odoo import api, fields, models
from odoo.osv import osv
from odoo.tools.translate import _
from odoo.exceptions import Warning,UserError, ValidationError


class wms_manifest_process(models.Model):
    _inherit = "wms.manifest.process"

    total_receivable_amount = fields.Float('Total Delivered Amount')
    total_return_amount = fields.Float('Total Return Amount')
    total_ndr_amount = fields.Float('Total NDR Amount')
    total_received_amount = fields.Float('Total Receiving Amount')
    total_cash_amount = fields.Float('Total Cash Amount')
    total_credit_amount = fields.Float('Total Credit Amount')
    total_paid_amount = fields.Float('Total Advance/Paid Amount')
    bulk_payment_id = fields.Many2one('bulk.order.payment', 'Bulk Payment ID')
    state = fields.Selection([
        ('pending', 'Pending'),
        ('confirm', 'Confirmation'),
        ('delivered', 'Delivered & Not Paid'),
        ('closed', 'Paid & Closed'),
        ('cancel', 'Cancelled'),

    ], 'Status', readonly=True, copy=False, help="Gives the status of the Manifest", index=True)

    ####Block code for V-14
    # _columns = {
    #     'total_receivable_amount': fields.float('Total Delivered Amount'),
    #     'total_return_amount': fields.float('Total Return Amount'),
    #     'total_ndr_amount': fields.float('Total NDR Amount'),
    #     'total_received_amount': fields.float('Total Receiving Amount'),
    #     'total_cash_amount': fields.float('Total Cash Amount'),
    #     'total_credit_amount': fields.float('Total Credit Amount'),
    #     'total_paid_amount': fields.float('Total Advance/Paid Amount'),
    #     'bulk_payment_id': fields.many2one('bulk.order.payment', 'Bulk Payment ID'),
    #     'state': fields.selection([
    #         ('pending', 'Pending'),
    #         ('confirm', 'Confirmation'),
    #         ('delivered', 'Delivered & Not Paid'),
    #         ('closed', 'Paid & Closed'),
    #         ('cancel', 'Cancelled'),
    #
    #     ], 'Status', readonly=True, copy=False, help="Gives the status of the Manifest", index=True),
    #
    # }

    def receive_payment(self):
        return True

    def calculate_total(self):
        self.ensure_one()

        # abc = self.browse(self.ids)[0]
        total_delivered = 0
        total_return_amount = 0
        total_ndr_amount = 0
        total_advance_amount = 0
        total_credit_amount = 0
        total_cash_amount = 0
        total_received_amount = 0
        all_manifest_did_not_taken_action = False

        for items in self.picking_line:
            if items.full_delivered:
                # inv_id = items.invoice_id
                inv_data = self.env['account.move'].browse([items.invoice_id])

                total_delivered = total_delivered + inv_data.amount_residual
                if items.payment_type and 'Cash On Delivery' in items.payment_type:
                    total_cash_amount = total_cash_amount + inv_data.amount_residual
                elif items.payment_type and ('Credit/Debit Cards' in items.payment_type or 'Mobile Banking' in items.payment_type or 'PAID' in items.payment_type):
                    total_advance_amount = total_advance_amount + inv_data.amount_residual
                else:
                    total_credit_amount = total_credit_amount + inv_data.amount_residual

                total_return_amount = total_return_amount + (inv_data.amount_total - inv_data.amount_residual)

                items.write({'delivered_amount': inv_data.amount_residual,
                             'return_amount': (inv_data.amount_total - inv_data.amount_residual)})

            elif items.partial_delivered:
                inv_data = self.env['account.move'].browse([items.invoice_id])
                if inv_data.invoice_payments_widget == 'false':
                    raise ValidationError(_(' %s this invoice still in pending return bucket.',inv_data.name))
                total_delivered = total_delivered + inv_data.amount_residual
                if items.payment_type and 'Cash On Delivery' in items.payment_type:
                    total_cash_amount = total_cash_amount + inv_data.amount_residual
                elif items.payment_type and ('Credit/Debit Cards' in items.payment_type or 'Mobile Banking' in items.payment_type or 'PAID' in items.payment_type):
                    total_advance_amount = total_advance_amount + inv_data.amount_residual
                else:
                    total_credit_amount = total_credit_amount + inv_data.amount_residual

                total_return_amount = total_return_amount + (inv_data.amount_total - inv_data.amount_residual)

                items.write({'delivered_amount': inv_data.amount_residual,
                             'return_amount': (inv_data.amount_total - inv_data.amount_residual)})

            elif items.full_return:
                total_return_amount = total_return_amount + items.amount
                items.write({'return_amount': items.amount})
                # self.env.cr.execute("UPDATE wms_manifest_line SET return_amount=%s WHERE id=%s",
                #                     ([items.amount, items.id]))
                self.env.cr.commit()

            elif items.reschedule:
                total_ndr_amount = total_ndr_amount + items.amount
                items.write({'ndr_amount': items.amount})
                # self.env.cr.execute("UPDATE wms_manifest_line SET ndr_amount=%s WHERE id=%s",
                #                     ([items.amount, items.id]))
                # self.env.cr.commit()
            else:
                all_manifest_did_not_taken_action = True

        self.write({'total_receivable_amount': total_delivered, 'total_cash_amount': total_cash_amount,
                    'total_credit_amount': total_credit_amount, 'total_paid_amount': total_advance_amount,
                    'total_return_amount': total_return_amount, 'total_received_amount': total_received_amount,
                    'total_ndr_amount': total_ndr_amount})
        # self.env.cr.execute(
        #     "UPDATE wms_manifest_process SET total_receivable_amount=%s,total_cash_amount=%s,total_credit_amount=%s,total_paid_amount=%s,total_return_amount=%s,total_received_amount=%s,total_ndr_amount=%s WHERE id=%s",
        #     ([total_delivered, total_cash_amount, total_credit_amount, total_advance_amount, total_return_amount,
        #       total_received_amount, total_ndr_amount, self.id]))
        # self.env.cr.commit()

        if not all_manifest_did_not_taken_action:
            self.write({'state': 'delivered'})

        if float(self.total_receivable_amount) == float(0):
            self.write({'state': 'closed'})
        #     stat = 'delivered'
        #     self.env.cr.execute(
        #         "UPDATE wms_manifest_process SET state=%s WHERE id=%s", ([stat, self.id]))
        #     self.env.cr.commit()
        #
        # return True


class WmsManifestLine(models.Model):
    _inherit = "wms.manifest.line"

    delivered_amount = fields.Float('Delivered Amount')
    return_amount = fields.Float('Return Amount')
    ndr_amount = fields.Float('NDR Amount')
    journal_id = fields.Many2one('account.move', 'Journal ID')

    #
    # _columns = {
    #     'delivered_amount': fields.float('Delivered Amount'),
    #     'return_amount': fields.float('Return Amount'),
    #     'ndr_amount': fields.float('NDR Amount'),
    #     'journal_id': fields.many2one('account.move', 'Journal ID'),
    #
    # }


class BulkOrderPayment(models.Model):
    _name = 'bulk.order.payment'
    _description = 'Bulk Order Payment'
    _order = 'id desc'

    scan = fields.Char('Scan Invoice Here')
    name = fields.Char('Name')
    payment_date = fields.Date('Date Of Received', default=fields.Datetime.now)
    # period_id = fields.Many2one('account.period', 'Period')
    cash_receiving_amount = fields.Float('Total Cash Amount')
    bank_receiving_amount = fields.Float('Total Advance Paid Amount')
    receiving_amount = fields.Float('Total Receiving Amount')
    due_amount = fields.Float('Total Due Amount')
    vat_amount = fields.Float('Total Vat Amount')
    cash_payment_method = fields.Many2one('account.journal', 'Cash Payment Method', required=True)
    bank_payment_method = fields.Many2one('account.journal', 'Bank Payment Method', required=True)
    vat_payment_method = fields.Many2one('account.journal', 'Vat Payment Method')
    description = fields.Text('Remarks')
    bulk_order_payment_line = fields.One2many('bulk.order.payment.line', 'bulk_order_payment_id',
                                              'Bulk Invoice payment line',
                                              required=True)

    confirm_time = fields.Datetime('Confirmation Time')
    cancel_time = fields.Datetime('Cancel Time')
    state = fields.Selection([
        ('pending', 'Pending'),
        ('confirm', 'Confirmed'),
        ('cancel', 'Cancelled'),

    ], 'Status', default='pending', readonly=True, copy=False,
        help="Gives the status of the Customer Order Payment Voucher",
        index=True)

    # ## Block code for v-14
    # _columns = {
    #     'scan': fields.char('Scan Invoice Here'),
    #     'name': fields.char('Name'),
    #     'payment_date': fields.date('Date Of Received'),
    #     'period_id': fields.many2one('account.period', 'Period', required=True),
    #     'cash_receiving_amount': fields.float('Total Cash Amount'),
    #     'bank_receiving_amount': fields.float('Total Advance Paid Amount'),
    #     'receiving_amount': fields.float('Total Receiving Amount'),
    #     'due_amount': fields.float('Total Due Amount'),
    #     'vat_amount': fields.float('Total Vat Amount'),
    #     'cash_payment_method': fields.many2one('account.journal', 'Cash Payment Method', required=True),
    #     'bank_payment_method': fields.many2one('account.journal', 'Bank Payment Method', required=True),
    #     'vat_payment_method': fields.many2one('account.journal', 'Vat Payment Method'),
    #     'description': fields.text('Remarks'),
    #     'bulk_order_payment_line': fields.one2many('bulk.order.payment.line', 'bulk_order_payment_id',
    #                                                'Bulk Invoice payment line',
    #                                                required=True),
    #
    #     'confirm_time': fields.datetime('Confirmation Time'),
    #     'cancel_time': fields.datetime('Cancel Time'),
    #     'state': fields.selection([
    #         ('pending', 'Pending'),
    #         ('confirm', 'Confirmed'),
    #         ('cancel', 'Cancelled'),
    #
    #     ], 'Status', readonly=True, copy=False, help="Gives the status of the Customer Order Payment Voucher",
    #         index=True),
    #
    # }

    # def _get_period(self):
    #     # if context is None: context = {}
    #     if self.context.get('period_id', False):
    #         return self.context.get('period_id')
    #     periods = self.env['account.period'].find()
    #     return periods and periods[0] or False

    # _defaults = {
    #     'payment_date': datetime.datetime.today(),
    #     'period_id': _get_period,
    #     'state': 'pending'
    # }

    def write(self, vals):

        data = self.browse()

        if data.state == 'confirm':
            raise Warning(_('It is already confirmed. You can not change the value'))

        return super(BulkOrderPayment, self).write(vals)

    @api.model
    def create(self, vals):

        # if context is None:
        #     context = {}

        stored = super(BulkOrderPayment, self).create(vals)  # return ID int object

        name_text = 'BOR-0' + str(stored.id)
        stored.name = name_text

        # if stored is not None:
        #     name_text = 'BOR-0' + str(stored.id)
        #     self.env.cr.execute('update bulk_order_payment set name=%s where id=%s', (name_text, stored))
        #     self.env.cr.commit()

        return stored

    def _check_line_for_same_invoice(self, packing_line, invoice_id):
        same_invoice = False

        for line in packing_line:
            if str(line.invoice_id.id) == invoice_id:
                same_invoice = True

        return same_invoice

    def cancel_the_payment(self):
        for data in self:
            if data.state == 'confirm':
                raise Warning(_('It is already confirmed. You can not cancel.'))

            data.write({'state': 'cancel'})

    def confirm_the_payment(self):

        data = self.browse(self.ids)

        if data.state == 'confirm':
            raise Warning(_('It is already confirmed'))

        cash_payment_method = data.cash_payment_method.payment_debit_account_id.id
        bank_payment_method = data.bank_payment_method.payment_credit_account_id.id

        for items in data.bulk_order_payment_line:

            if items.invoice_id.state == 'posted':
                j_vals = {}
                line_ids = []
                if 'Cash On Delivery' in items.inv_pay_type:
                    line_ids.append((0, 0, {
                        'name': data.name,
                        'account_id': cash_payment_method,
                        'partner_id': items.invoice_id.commercial_partner_id.id,
                        'debit': items.receiving_amount,
                    }))
                else:
                    line_ids.append((0, 0, {
                        'name': data.name,
                        'account_id': bank_payment_method,
                        'partner_id': items.invoice_id.commercial_partner_id.id,
                        'debit': items.receiving_amount,
                    }))

                line_ids.append((0, 0, {
                    'name': data.name,
                    'account_id': items.invoice_id.partner_id.property_account_receivable_id.id,
                    'partner_id': items.invoice_id.commercial_partner_id.id,
                    'credit': items.receiving_amount,
                }))

                j_vals = {'name': '/',
                          'journal_id': data.cash_payment_method.id,
                          'date': data.payment_date,
                          # 'period_id': data.period_id.id,
                          'ref': items.invoice_id.name + '(' + str(items.mag_no) + ')',
                          'line_ids': line_ids

                          }
                inv_id = items.invoice_id.id

                jv_entry = self.env['account.move']
                saved_jv_id = self.env['account.move'].create(j_vals)
                if saved_jv_id:
                    journal_id = saved_jv_id
                saved_jv_id.action_post()

                if journal_id:
              # #     ### nichar quary tik korta hoba ####################
                    # self.env.cr.execute(
                    #     'INSERT INTO invoice_journal_payment_relation (journal_id, invoice_id,invoice_amount) values (%s, %s,%s)',
                    #     (journal_id, items.invoice_id.id, items.receiving_amount))
                    # self.env.cr.commit()
                    new_residual = 0
                    new_residual = items.due_amount
                    # if new_residual > 0:
                    #     inv_status = 'open'
                    # else:
                    #     inv_status = 'paid'

                    # inv_id.write({
                    #     'new_residual': new_residual,
                    #     # 'state': inv_status,
                    # })
                    ##############################

                    items.write({'journal_id': journal_id})

                    # update_refund_residual_amount = "UPDATE account_invoice set new_residual='{0}',state='{1}' where id='{2}'".format(
                    #     new_residual, inv_status, inv_id)
                    #
                    # self.env.cr.execute(update_refund_residual_amount)
                    # self.env.cr.commit()

                    # update_refund_residual_amount = "UPDATE bulk_order_payment_line set journal_id='{0}'where id='{1}'".format(
                    #     journal_id, items.id)
                    #
                    # self.env.cr.execute(update_refund_residual_amount)
                    # self.env.cr.commit()
        data.write({'state': 'confirm'})
        # update_refund_residual_amount = "UPDATE bulk_order_payment set state='confirm' where id='{0}'".format(data.id)
        #
        # self.env.cr.execute(update_refund_residual_amount)
        # self.env.cr.commit()
        #
        # return True


##### error a jonno ami close rakachi
# @api.onchange('scan')
# def invoice_barcode_onchange(self):
#     invoice_id = str(self.scan)
#
#     bulk_payment_line_list = []
#     cash_receiving_amount = 0
#     bank_receiving_amount = 0
#
#     if invoice_id != 'False':
#
#         if self._check_line_for_same_invoice(self.bulk_order_payment_line, invoice_id):
#             self.scan = ''
#
#
#         else:
#
#             bulk_payment_line_list = []
#
#             for invoice_line in self.bulk_order_payment_line:
#
#                 inv_type = invoice_line.inv_pay_type
#                 if 'Cash On Delivery' in inv_type:
#                     cash_receiving_amount = cash_receiving_amount + invoice_line.receiving_amount
#                 if 'PAID' in inv_type or 'Mobile Banking' in inv_type or 'Credit/Debit Cards' in inv_type:
#                     bank_receiving_amount = bank_receiving_amount + invoice_line.receiving_amount
#
#                 bulk_payment_line_list.append({
#                     'invoice_id': invoice_line.invoice_id.id,
#                     'mag_no': invoice_line.mag_no,
#                     'order_id': '',
#                     'invoiced_amount': invoice_line.invoiced_amount,
#                     'receiving_amount': invoice_line.receiving_amount,
#                     'journal_id': '',
#                     'manifest_line_id': '',
#                     'inv_pay_type': inv_type,
#                 })
#
#             invoice_line_env = self.env['account.move']
#             invoice_line_obj = invoice_line_env.search([('id', '=', invoice_id)])
#             for invoice_line in invoice_line_obj:
#                 inv_type = invoice_line.comment if invoice_line.comment is not False else ''
#
#                 if 'Cash On Delivery' in inv_type:
#                     cash_receiving_amount = cash_receiving_amount + invoice_line.residual
#                 if 'PAID' in inv_type or 'Mobile Banking' in inv_type or 'Credit/Debit Cards' in inv_type:
#                     bank_receiving_amount = bank_receiving_amount + invoice_line.residual
#
#                 bulk_payment_line_list.append({
#                     'invoice_id': invoice_line.id,
#                     'mag_no': invoice_line.name,
#                     'order_id': '',
#                     'invoiced_amount': invoice_line.amount_total,
#                     'receiving_amount': invoice_line.residual,
#                     'journal_id': '',
#                     'manifest_line_id': '',
#                     'inv_pay_type': invoice_line.comment,
#
#                 })
#
#             self.scan = ""
#
#             self.bulk_order_payment_line = bulk_payment_line_list
#
#             self.cash_receiving_amount = cash_receiving_amount
#             self.bank_receiving_amount = bank_receiving_amount
#             self.receiving_amount = bank_receiving_amount + cash_receiving_amount
#
#     return "xXxXxXxXxX"

# @api.onchange('bulk_order_payment_line')
# def test_code(self):
#     total_recv_sum = 0
#     total_due_sum = 0
#     total_cash_sum = 0
#     total_bank_sum = 0
#     for line_item in self.bulk_order_payment_line:
#         if 'Cash On Delivery' in line_item.inv_pay_type:
#             total_cash_sum = total_cash_sum + line_item.receiving_amount
#         if 'PAID' in line_item.inv_pay_type or 'Mobile Banking' in line_item.inv_pay_type or 'Credit/Debit Cards' in line_item.inv_pay_type:
#             total_bank_sum = total_bank_sum + line_item.receiving_amount
#
#         total_recv_sum = total_recv_sum + line_item.receiving_amount
#         total_due_sum = total_due_sum + line_item.due_amount
#     self.cash_receiving_amount = total_cash_sum
#     self.bank_receiving_amount = total_bank_sum
#     self.receiving_amount = total_recv_sum
#     self.due_amount = total_due_sum
#     return 'ss'


class BulkOrderPaymentLine(models.Model):
    _name = 'bulk.order.payment.line'
    _description = "Bulk Order Payment Line"

    bulk_order_payment_id = fields.Many2one('bulk.order.payment', 'Bulk Order Payment ID', required=True,
                                            ondelete='cascade', index=True, readonly=True)

    invoice_id = fields.Many2one('account.move', string="Invoice No")
    mag_no = fields.Char('Magento No')
    order_id = fields.Many2one('sale.order', string="Order No.")
    invoiced_amount = fields.Float('Invoiced Amount')
    receiving_amount = fields.Float('Receiving Amount')
    due_amount = fields.Float('Due Amount')
    journal_id = fields.Many2one('account.move', 'Journal ')
    manifest_line_id = fields.Many2one('wms.manifest.line', 'Manifest Line ')
    inv_pay_type = fields.Char('Invoice Payment Type')

    # ### Block code for v-14
    # _columns = {
    #
    #     'bulk_order_payment_id': fields.many2one('bulk.order.payment', 'Bulk Order Payment ID', required=True,
    #                                              ondelete='cascade', index=True, readonly=True),
    #
    #     'invoice_id': fields.many2one('account.move', string="Invoice No"),
    #     'mag_no': fields.char('Magento No'),
    #     'order_id': fields.many2one('sale.order', string="Order No."),
    #     'invoiced_amount': fields.float('Invoiced Amount'),
    #     'receiving_amount': fields.float('Receiving Amount'),
    #     'due_amount': fields.float('Due Amount'),
    #     'journal_id': fields.many2one('account.move', 'Journal '),
    #     'manifest_line_id': fields.many2one('wms.manifest.line', 'Manifest Line '),
    #     'inv_pay_type': fields.char('Invoice Payment Type'),
    #
    # }

    @api.onchange('receiving_amount')
    def mag_on_select(self):
        if self.receiving_amount:
            self.due_amount = float(self.invoiced_amount) - float(self.receiving_amount)

        # return "xXxXxXxXxX"
