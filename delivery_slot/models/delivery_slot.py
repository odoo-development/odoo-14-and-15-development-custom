from odoo import api, fields, models, _


class StockPicking(models.Model):
    _inherit = 'stock.picking'

    @api.model
    def create(self, vals):

        delivery_slot = ''
        delivery_comments = ''
        expected_delivery_date = ''
        expected_delivery_datetime = False

        result = super(StockPicking, self).create(vals)
        try:
            order_origin = vals.get('origin')
            if str(order_origin).startswith("S"):
                so_obj = self.env['sale.order']
                so_list = so_obj.search([('name', '=', str(order_origin))])
                delivery_slot = so_list.x_delivery_slot
                expected_delivery_date = so_list.x_expected_delivery_date
                delivery_comments = str(so_list.x_delivery_comments)
        except:
            pass

        result.x_delivery_slot = delivery_slot
        result.x_delivery_comments = delivery_comments
        result.x_expected_delivery_date = expected_delivery_date
        # vals['x_expected_delivery_datetime'] = expected_delivery_datetime

        return result



