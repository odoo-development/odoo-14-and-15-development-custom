{
    'name': 'Delivery Slot',
    'version': '14.0',
    'category': 'Order',
    'author': 'Sindabad',
    'summary': 'Delivery Slot',
    'description': 'Delivery Slot',
    'depends': [
        'pickings',
        'sale_stock',
        'order_cancel_reasons'
    ],
    "data": [
        "views/rfp_delivery_slot_view.xml",
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}
