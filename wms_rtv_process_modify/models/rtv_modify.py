import datetime
from dateutil.relativedelta import relativedelta

from odoo import api, fields, models, _
from odoo.osv import osv
from odoo.exceptions import UserError, ValidationError, Warning


from odoo import api, fields, models


class RtvProcess(models.Model):
    _name = "rtv.process"
    _inherit = ['rtv.process','mail.thread']

    warehouse_id = fields.Many2one(tracking=True)

    rtv_process_type = fields.Selection(selection=[('po', 'Purchase Order'), ('other', 'Other')], string='RTV Type Name', required=True, index=True, tracking=True, default="po")

    damage_vendor_id = fields.Many2one('res.partner', 'Damage Supplier', tracking=True, domain=[('damage_supplier', '=', True)])

    grn_id = fields.Many2one('stock.picking', 'GRN', tracking=True, domain=[('location_id','=',4),('state', '!=', 'cancel')])

    @api.onchange('grn_id')
    def grn_id_details(self):
        grn_list_id = self.grn_id
        if grn_list_id:
            self.grn_id = grn_list_id

            if self.grn_id:
                grn_data_browse = self.env['stock.picking'].browse(self.grn_id.id)
                grn_product_list = self.env['stock.move.line'].search([('picking_id', '=', grn_data_browse.id)])

                move_line = []

                po_obj = self.env['purchase.order'].search([('name', '=', grn_product_list.picking_id.origin)], limit=1)
                self.po_id = po_obj.id
                self.vendor_id = po_obj.partner_id.id
                self.vendor_address = po_obj.partner_id.contact_address_complete if po_obj.partner_id.contact_address_complete else ''
                self.vendor_contact = po_obj.partner_id.phone or po_obj.partner_id.mobile if po_obj.partner_id.phone or po_obj.partner_id.mobile else None
                self.area = po_obj.partner_id.state_id.id if po_obj.partner_id.state_id.id else None

                self.warehouse_id = po_obj.picking_type_id.warehouse_id.id
                self.rtv_type = ''
                self.pqc = ''
                self.pqc_id = ''
                self.pqc_number = ''
                self.po_number = po_obj.name
                self.pqc_processed = ''
                self.remark = ''
                self.state = 'pending'

                for rtv_line in grn_product_list:

                    warehouse_id = po_obj.picking_type_id.warehouse_id.id
                    values = {}
                    if rtv_line.product_id:

                        prod_browse = self.env["product.product"].search([("id", "=", rtv_line.product_id.id)])

                        if warehouse_id == 1:  # Uttara
                            values['available_qty'] = prod_browse.uttara_free_quantity
                        elif warehouse_id == 2:  # Nodda
                            values['available_qty'] = prod_browse.nodda_free_quantity
                        elif warehouse_id == 3:  # Signboard
                            values['available_qty'] = prod_browse.signboard_free_quantity
                        elif warehouse_id == 7:  # Badda
                            values['available_qty'] = prod_browse.badda_free_quantity
                        elif warehouse_id == 4:  # Uttara Damage
                            values['available_qty'] = prod_browse.uttara_damage_quantity
                        elif warehouse_id == 5:  # Nodda Damage
                            values['available_qty'] = prod_browse.nodda_damage_quantity
                        elif warehouse_id == 6:  # Signboard Damage
                            values['available_qty'] = prod_browse.signboard_damage_quantity
                        elif warehouse_id == 9:  # Badda Damage
                            values['available_qty'] = prod_browse.badda_damage_quantity
                        elif warehouse_id == 10:  # Badda horeca
                            values['available_qty'] = prod_browse.badda_horeca_free_quantity
                        elif warehouse_id == 11:  # Savar
                            values['available_qty'] = prod_browse.savar_free_quantity
                        elif warehouse_id == 12:  # Savar Damage
                            values['available_qty'] = prod_browse.savar_damage_quantity

                        values['free_qty'] = values['available_qty']

                    move_line.append([0, False, {
                        'product_id': rtv_line.product_id.id,
                        'po_number': po_obj.name,
                        'free_qty': values['free_qty'],
                        'grn_receive_quantity': rtv_line.qty_done,
                        'available_qty': 0,
                        # 'requested_qty': 0,
                        # 'return_qty': 0,
                        'unit_cost': rtv_line.product_id.standard_price,
                        # 'amount': 0,
                        # 'cal_amount': 0,
                        'state': 'pending',
                    }])

                self.rtv_process_line = move_line

            else:
                self.po_id = None
                self.vendor_id = None
                self.vendor_address = None
                self.vendor_contact = None
                self.area = None
                self.warehouse_id = None
                self.rtv_type = None
                self.pqc = None
                self.pqc_id = None
                self.pqc_number = None
                self.po_number = None
                self.pqc_processed = None
                self.remark = None
                self.state = 'pending'

    def dynamic_selection(self):

        three_months = datetime.datetime.now() - relativedelta(months=3)

        stock_picking_ids = self.env['stock.picking'].search([('location_id','=',4),('state','=','done'),('date_done', '>=', three_months)])

        grn_list = []
        for i in stock_picking_ids:
            grn_list.append((i.id, str(i.name+' '+i.origin)))

        return grn_list

    @api.onchange('damage_vendor_id')
    def damage_vendor_details(self):
        damage_supplier = self.damage_vendor_id
        if damage_supplier:
            self.vendor_id = damage_supplier.id
            self.vendor_address = damage_supplier.contact_address_complete if damage_supplier.contact_address_complete else ''
            self.vendor_contact = damage_supplier.phone or damage_supplier.mobile if damage_supplier.phone or damage_supplier.mobile else None
            self.area = damage_supplier.state_id.id if damage_supplier.state_id.id else None

        else:
            self.po_id = None
            self.vendor_id = None
            self.vendor_address = None
            self.vendor_contact = None
            self.area = None
            self.warehouse_id = None
            self.rtv_type = None
            self.pqc = None
            self.pqc_id = None
            self.pqc_number = None
            self.po_number = None
            self.pqc_processed = None
            self.remark = None
            self.state = 'pending'


class RtvProcessLine(models.Model):
    _inherit = "rtv.process.line"

    return_qty = fields.Float(tracking=True)
    unit_cost = fields.Float(tracking=True)
    grn_receive_quantity = fields.Float('GRN Qty')


class ResPartner(models.Model):
    _inherit = 'res.partner'

    damage_supplier = fields.Boolean(string='Damage Supplier', default=False)




