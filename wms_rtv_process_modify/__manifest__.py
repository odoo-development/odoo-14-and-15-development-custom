{
    'name': 'WMS RTV Process Last update custom module',
    'version': '14.0.0',
    'category': 'WMS RTV Process',
    'description': """
WMS RTV Process Last update custom module which is develop sindabad odoo team.
==============================================================================

""",
    'author': 'Sindabad',
    'depends': ['wms_rtv_process'],
    'data': [
        'views/wms_rtv_modify_view.xml',
    ],

    'installable': True,
    'application': True,
    'auto_install': False,
}

