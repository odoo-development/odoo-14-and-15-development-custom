# -*- coding : utf-8 -*-

from odoo import api, fields, models, tools, _


class BackToBackOrder(models.TransientModel):
    _inherit = 'back.to.back.order'

    x_sale_classification_type = fields.Selection(string="Sale Classification Type", selection='_populate_choice', default='b2r')

    @api.model
    def _populate_choice(self):
        choices = [
            ('b2r', 'B2R'), ('b2e', 'B2E'), ('b2c', 'B2C'), ('horeca', 'HORECA')
        ]
        return choices

