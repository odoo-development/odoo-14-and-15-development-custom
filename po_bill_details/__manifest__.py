# -*- coding : utf-8 -*-
# Part of Rocky See LICENSE file for full copyright and licensing details.

{
	'name'			: 'Bill Details on Purchase order',
	'version'		: '14.0.0.0',
	'category'		: 'Purchase',
	'author'		: 'Rocky',
	'summary'		: 'You can see Bill Amount, Bill Amount Due, Bill Paid Amount in purchase order',
	'description'	: """You can see Bill Amount, Bill Amount Due, Bill Paid Amount in purchase order. 
	Bill Amount Details For Purchase Order
	Bill Amount Details For Purchase
	Purchase bill details
	Purchase order bill details
	Purchase billd details
	Purchase bill details
	Bill amount on Purchase order
	Bill amount details on Purchase order
	Bill details for Purchase order
	Bill detail for Purchase order
	Bill detail on Purchases
	Bill detail in Purchase order
	Bill amount details on Purchases order

	""",
	'depends'		: ['base','purchase','purchase_stock','sgeede_b2b','stock_account'],
	'data'			: ['views/po_bill_details_view.xml'],
	'installable'	: True,
	'auto_install'	: False,
	'live_test_url':'https://youtu.be/kjFQSg5URcw',
	"images":['static/description/Banner.png'],
}
