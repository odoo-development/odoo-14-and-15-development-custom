# -*- coding : utf-8 -*-

from odoo import api, fields, models, _


class PurchaseOrder(models.Model):
	_inherit = 'purchase.order'

	bill_amount = fields.Float(String='Bill Amount', compute='_compute_bill_amount', index=True)
	bill_amount_due = fields.Float(String='Bill Amount Due', compute='_compute_bill_amount_due', index=True)
	bill_paid_amount = fields.Float(String='Bill Paid Amount', compute='_compute_bill_amount_paid', index=True)
	adv_paid_amount = fields.Float(String='Advance Paid Amount', compute='_compute_amount_adv_paid', index=True)
	po_paid_amount = fields.Float(String='PO Paid Amount', compute='_compute_po_amount_paid', index=True)

	x_sale_classification_type = fields.Selection(string="Sale Classification Type", selection='_populate_choice', default='b2r')

	@api.model
	def _populate_choice(self):
		choices = [
			('b2r', 'B2R'), ('b2e', 'B2E'), ('b2c', 'B2C'), ('horeca', 'HORECA')
		]
		return choices

	def _compute_bill_amount(self):
		for record in self:
			bill_id = self.env['account.move'].search(['&', ('invoice_origin', '=', record.name), '|', ('state', '=', 'draft'), ('state', '=', 'posted'), ('payment_state', 'not in', ['reversed', 'invoicing_legacy'])])
			total = 0

			if bill_id:
				for bill in bill_id:
					total -= bill.amount_total_signed
					record.bill_amount = total
			else:
				record.bill_amount = total

	@api.depends('bill_paid_amount', 'bill_amount', 'bill_amount_due')
	def _compute_bill_amount_due(self):
		for record in self:
			bill_ids = self.env['account.move'].search(['&', ('invoice_origin', '=', record.name), '|', ('state', '=', 'draft'), ('state', '=', 'posted'), ('payment_state', 'not in', ['reversed', 'invoicing_legacy'])])
			amount = 0

			if bill_ids:
				for inv in bill_ids:
					amount += inv.amount_residual
					record.bill_amount_due = amount
			else:
				record.bill_amount_due = amount

	def _compute_amount_adv_paid(self):
		for record in self:
			adv_pay_ids = self.env['account.move'].search([('ref', '=like', '%' + record.name), ('state', '=', 'posted'), ('journal_id', 'in', [3, 45]), ('kickback', '!=', True)])
			amount = 0

			if adv_pay_ids:
				for inv in adv_pay_ids:
					for inv_line in inv.line_ids:
						if inv_line.partner_id.id == record.partner_id.id:
							amount += inv_line.debit
							record.adv_paid_amount = amount
			else:
				record.adv_paid_amount = amount

	@api.onchange('bill_amount', 'bill_amount_due')
	def _compute_bill_amount_paid(self):
		for record in self:
			record.bill_paid_amount = float(record.bill_amount) - float(record.bill_amount_due)

	@api.onchange('bill_paid_amount', 'adv_paid_amount')
	def _compute_po_amount_paid(self):
		for record in self:
			if record.adv_paid_amount == record.amount_total:
				record.po_paid_amount = float(record.adv_paid_amount)
			elif record.bill_paid_amount == record.amount_total:
				record.po_paid_amount = float(record.bill_paid_amount)
			else:
				record.po_paid_amount = float(record.bill_paid_amount)

