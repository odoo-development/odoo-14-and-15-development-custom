# -*- coding : utf-8 -*-

from odoo import api, fields, models, _


class StockValuationLayer(models.Model):
    _inherit = 'stock.valuation.layer'

    product_category = fields.Char(related='product_id.categ_id.name', string="Product Category", readonly=True, index=True, store=True)

